@echo off
TITLE --- Load Scan Data - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Export Claim Error procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

D:


call SetEnv.bat


IF EXIST ..\Logs\EXCEP_CLAIMERR.log MOVE ..\Logs\EXCEP_CLAIMERR.log ..\Logs\Archive\LOADSCAN\EXCEP_CLAIMERR.log_%Timestamp%.log 

IF EXIST ..\Logs\EXCEP_CLAIMERR.log DEL ..\Logs\EXCEP_CLAIMERR.log

set DPLOG=..\Logs\EXCEP_CLAIMERR.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load CLAIMERR Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @CLAIMERR.sql


ECHO. >> %DPLOG%
ECHO Finished CLAIMERRProcedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%