@echo off
TITLE --- Unlock Users BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Unlock Users 

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\unlock_users.log MOVE ..\Logs\test_la.log ..\Logs\Archive\unlock_users\test_la.log_%Timestamp%.log 

IF EXIST ..\Logs\unlock_users.log DEL ..\Logs\test_la.log 

set DPLOG=..\Logs\test_la.log 

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Unlocking Demantra users >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running unlock_users.sql 
REM sqlplus %USER%/%PASSWD%@%TNS%   @unlock_users.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_unlock_users_err

ECHO Running Simulation Stater
ECHO start /D "%ENGINEROOT%\" EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 2
start /D "%ENGINEROOT%\" EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 2


REM timeout /t 10
ping 127.0.0.1 -n 60 > nul

ECHO Running Simulation
start /D "%ENGINEROOT%\" Start_Simulation2K.bat 99 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD%


ECHO. >> %DPLOG%
ECHO Finished Locking out Demantra users 
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_unlock_users_err
ECHO Demantra Unlocking  Demantra users procedures failed
goto end_error

:end_error
ECHO Demantra Unlocking users failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Unlocking users successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
