@echo off
TITLE --- Load Scan Data - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load Scan Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\LOADSCAN.log MOVE ..\Logs\LOADSCAN.log ..\Logs\Archive\LOADSCAN\LOADSCAN.log_%Timestamp%.log 

IF EXIST ..\Logs\LOADSCAN.log DEL ..\Logs\LOADSCAN.log

set DPLOG=..\Logs\LOADSCAN.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load Scan Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO Running rr_pkg_data_load.prc_rrloadscan 
sqlplus %USER%/%PASSWD%@%TNS%   @loadscan.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_rrloadscan_err

ECHO. >> %DPLOG%
ECHO Finished Load Scan Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_rrloadscan_err
ECHO Procedure to load scan data failed
goto end_error

:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 
:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT