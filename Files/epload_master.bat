@echo off
TITLE --- EP LOAD - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo EP LOAD procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\EPLOAD_master.log MOVE ..\Logs\EPLOAD_master.log ..\Logs\Archive\EPLOAD_master\EPLOAD_master.log_%Timestamp%.log 

IF EXIST ..\Logs\EPLOAD_master.log DEL ..\Logs\EPLOAD_master.log

set DPLOG=..\Logs\EPLOAD_master.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Master Data Load Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running EPLOAD_master.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @EPLOAD_master.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Master Data Load Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Master Data Load Procedure failed
goto end_error



:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 

:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT