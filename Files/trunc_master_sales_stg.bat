@echo off
TITLE --- Clear Master Staging table BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Forecast Export procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\trunc_master_sales_stg.log MOVE ..\Logs\trunc_master_sales_stg.log ..\Logs\Archive\trunc_master_sales_stg\trunc_master_sales_stg.log_%Timestamp%.log 

IF EXIST ..\Logs\trunc_master_sales_stg.log DEL ..\Logs\trunc_master_sales_stg.log

set DPLOG=..\Logs\trunc_master_sales_stg.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Clear Master Staging table  >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running trunc_master_sales_stg.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @trunc_master_sales_stg.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Clear Master Staging table  >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Clear Master Staging table failed
goto end_error



:end_error
ECHO Demantra Clear Master Staging table failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Clear Master Staging table successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT