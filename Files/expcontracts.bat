@echo off
TITLE --- Export Contracts BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Contracts Export procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\expcontracts.log MOVE ..\Logs\expcontracts.log ..\Logs\Archive\expcontracts\expcontracts.log_%Timestamp%.log 

IF EXIST ..\Logs\expcontracts.log DEL ..\Logs\expcontracts.log

set DPLOG=..\Logs\expcontracts.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Export CSV Files for Notifications >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

start /D "%CURRFOLDER%" /WAIT err_notif.bat

ECHO. >> %DPLOG%
ECHO Start Demantra Contracts Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO Running expcontracts.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @expcontracts.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Contracts Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Contracts Export Procedures  failed
goto end_error



:end_error
ECHO Demantra Contracts Export Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Contracts Export Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT