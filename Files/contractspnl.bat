@echo off
TITLE --- Contracts P AND L SYNC BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Promotions Accrual Overspend procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\contractspnl.log MOVE ..\Logs\contractspnl.log ..\Logs\Archive\contractspnl\contractspnl.log_%Timestamp%.log 

IF EXIST ..\Logs\contractspnl.log DEL ..\Logs\contractspnl.log

set DPLOG=..\Logs\contractspnl.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Contracts P AND L Calculation and Sync Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running contractspnl.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @contractspnl.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Contracts P AND L Calculation and Sync Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Contracts P AND L Calculation and Sync Procedures  failed
goto end_error



:end_error
ECHO Demantra Contracts P AND L Calculation and Sync Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Contracts P AND L Calculation and Sync Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT