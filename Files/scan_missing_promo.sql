set heading off

select 'Starting to write EDI Claim Errors' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 150

spool ..\CSV\Missing_Promos_AU.csv

select 'Region,Customer Level 4,Promoted Group,Sale Date,Base,Incremental,Perc Difference'
 from dual;

select distinct 
   country||','||Account_state||','||promo_group||','||to_char(sales_date, 'dd/mm/yyyy')||','||round(base,2)||','||round(incr,2)||','||round(diff*100,2) 
from tbl_missing_promos where upper(country) like 'AUS%';
spool off;

set termout on;

exit;