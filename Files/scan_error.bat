@echo off
TITLE --- Scan Error - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Export Scan Error procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\EXCEP_SCANERR.log MOVE ..\Logs\EXCEP_SCANERR.log ..\Logs\Archive\LOADSCAN\EXCEP_SCANERR.log_%Timestamp%.log 

IF EXIST ..\Logs\EXCEP_SCANERR.log DEL ..\Logs\EXCEP_SCANERR.log

set DPLOG=..\Logs\EXCEP_SCANERR.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load SCANERR Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @SCANERR.sql


ECHO. >> %DPLOG%
ECHO Finished SCANERR Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%