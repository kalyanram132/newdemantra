@echo off
TITLE --- EP LOAD - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo EP LOAD procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\EPLOAD.log MOVE ..\Logs\EPLOAD.log ..\Logs\Archive\EPLOAD\EPLOAD.log_%Timestamp%.log 

IF EXIST ..\Logs\EPLOAD.log DEL ..\Logs\EPLOAD.log

set DPLOG=..\Logs\EPLOAD.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Master Data Load Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running epload.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @epload.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Master Data Load Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Master Data Load Procedure failed
goto end_error



:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 

:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT