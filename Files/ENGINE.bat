@echo off
TITLE --- Engine Batch - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv_eng.bat

IF EXIST ..\..\Logs\ENGINE.log MOVE ..\..\Logs\ENGINE.log ..\..\Logs\Archive\Engine\ENGINE.log_%Timestamp%.log 

COPY ..\..\Logs\Engine2k\%ENGINEFOLDER%\*.* ..\..\Logs\Engine2k\Archive\

DEL ..\..\Logs\Engine2k\%ENGINEFOLDER%\*.*

IF EXIST ..\Logs\ENGINE.log DEL ..\Logs\ENGINE.log

set DPLOG=..\Logs\ENGINE.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Full Proport Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running rr_pkg_common_utils.prc_run_full_proport
sqlplus %USER%/%PASSWD%@%TNS%   @proport.sql  >> %DPLOG%
echo IF %ERRORLEVEL% NEQ 0 goto prc_run_full_proport_err

ECHO. >> %DPLOG%
ECHO Finished Full Proport Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Kill Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Killing Simulation Engine
start /D "%CURRFOLDER%" kill_sim_engine.bat

ping 127.0.0.1 -n 60 > nul


ECHO IF %ERRORLEVEL% NEQ 0 goto kill_sim_engine_err

ECHO. >> %DPLOG%
ECHO Finished Kill Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO. >> %DPLOG%
ECHO Start Engine Run >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Changing folder to Engine root folder
ECHO Changing folder to Engine root folder>> %DPLOG%
ECHO CD %ENGINEROOT%

ECHO Running Engine
ECHO %ENGINEROOT%\EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 12 >> %DPLOG%
start /D "%ENGINEROOT%\" EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 23
ECHO IF %ERRORLEVEL% NEQ 0 goto engine_starter_err

ping 127.0.0.1 -n 60 > nul

ECHO Running Engine
ECHO call %ENGINEROOT%\Start_Engine2K.bat 1 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% >> %DPLOG%
call "%ENGINEROOT%\Start_Engine2K.bat" 1 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD%
 IF %ERRORLEVEL% NEQ 0 goto engine_err

ECHO. >> %DPLOG%
ECHO Finished Engine Run >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Start Simulation Engine
ECHO call %ENGINEROOT%\Start_Simulation2K.bat 99 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% >> %DPLOG%
ECHO IF %ERRORLEVEL% NEQ 0 goto start_sim_engine_err

ECHO. >> %DPLOG%
ECHO Finished Start Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Post Engine run Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running pkg_wf.prc_wf_post_engine
 sqlplus %USER%/%PASSWD%@%TNS%   @post_engine.sql  >> %DPLOG%
 IF %ERRORLEVEL% NEQ 0 goto prc_post_engine_err

ECHO. >> %DPLOG%
ECHO Finished Post Engine run Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_run_full_proport_err
ECHO Procedure prc_run_full_proport failed
goto end_error

:prc_post_engine_err
ECHO Procedure post_engine failed
goto end_error

:kill_sim_engine_err
ECHO Kill Simulation Engine failed
goto end_error

:engine_starter_err
ECHO Engine starter failed
goto end_error

:engine_err
ECHO Engine run failed
goto end_error

:start_sim_engine_err
ECHO Start Simulation Engine failed
goto end_error

:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 
:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
