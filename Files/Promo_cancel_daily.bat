@echo off
TITLE ---  Promo Cancel Daily  BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Promotions Accrual cancel daily procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\Promo_cancel_daily.log MOVE ..\Logs\Promo_cancel_daily.log ..\Logs\Archive\Promocanceldaily\Promo_cancel_daily.log_%Timestamp%.log 

IF EXIST ..\Logs\Promo_cancel_daily.log DEL ..\Logs\Promo_cancel_daily.log

set DPLOG=..\Logs\Promo_cancel_daily.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Promo Cancel Daily Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Promo_cancel_daily.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @Promo_cancel_daily.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Promo Cancel Daily Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Promo Cancel Daily Calculation Procedure failed
goto end_error



:end_error
ECHO Promo Cancel Daily Calculation Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Promo Cancel Daily Calculation successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT