spool "%LOGFOLDER%\Audit_reorg.sql.log"
 
SELECT COUNT (*) from DEMANTRA.AUDIT_VALUES where AUDIT_ID in (select AUDIT_ID from DEMANTRA.AUDIT_TRAIL where USER_ID = '6920' and TRUNC(change_date) BETWEEN TRUNC(sysdate - 60) and TRUNC(sysdate - 30));

DELETE from DEMANTRA.AUDIT_VALUES where AUDIT_ID in (select AUDIT_ID from DEMANTRA.AUDIT_TRAIL where USER_ID = '6920' and TRUNC(change_date) BETWEEN TRUNC(sysdate - 60) and TRUNC(sysdate - 30));

exec table_reorg.reorg('DEMANTRA','AUDIT_VALUES','C',20,1); 

EXIT;

Spool off ;