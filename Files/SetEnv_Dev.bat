SET PATH=D:\Oracle\DemantraDev\Demand Planner\DeskTop;%PATH%
SET PATH=D:\Oracle\DemantraDev\Demand Planner\Security Management;%PATH%
SET PATH=D:\Oracle\DemantraDev\Demand Planner\Analytical Engines\bin;%PATH%
SET ENGINEROOT=D:\Oracle\DemantraDev\Demand Planner\Analytical Engines

SET TNS=DMTRDEV
SET USER=demantra
SET PASSWD=demantra

SET ENGINEFOLDER=pdcaap58_DEMANTRA

SET CURRFOLDER=D:\Oracle\DemantraDev\Demantra Batch\Files

SET APO_INBOUND_PATH=D:\Oracle\DemantraDev\Demantra Batch\APO_Inbound\
SET APO_OUTBOUND_PATH=D:\Oracle\DemantraDev\Demantra Batch\APO_Outbound\

D:

CD %CURRFOLDER%

Rem ---------------------------------------------------------------------------------
Rem Assemble a date using DOS commands
Rem ---------------------------------------------------------------------------------

for /F "tokens=1-4 delims=/ " %%i in ('date /t') do (
   set DayOfWeek=%%i
   set Month=%%j
   set Day=%%k
   set Year=%%l
   set Date=%%i %%j/%%k/%%l
)

for /F "tokens=1-2 delims=: " %%i in ('time /t') do (
   set hours=%%i
   set minutes=%%j
   set Tim=%%i%%j
)

Rem ---------------------------------------------------------------------------------
Rem Set up a timestamp variable in YYYYMMDD_HHMM format
Rem Comment it or remove the comments (Rem) to use this format
Rem ---------------------------------------------------------------------------------

set Timestamp=%Year%%Month%%Day%_%Tim%