SET TNS=DMTRPRD
SET USER=demantra
SET PASSWD=d

SET DBSEVERPORT=pdcaap59:1521

SET APPSERVER=pdcaap57

SET CURRFOLDER=D:\Demantra_Batch

echo %CURRFOLDER%

D:

CD %CURRFOLDER%

Rem ---------------------------------------------------------------------------------
Rem - Assemble a date using DOS commands
Rem ---------------------------------------------------------------------------------

for /F "tokens=1-4 delims=/ " %%i in ('date /t') do (
   set DayOfWeek=%%i
   set Month=%%j
   set Day=%%k
   set Year=%%l
   set Date=%%i %%j/%%k/%%l
)

for /F "tokens=1-2 delims=: " %%i in ('time /t') do (
   set hours=%%i
   set minutes=%%j
   set Tim=%%i%%j
)

Rem ---------------------------------------------------------------------------------
Rem Set up a timestamp variable in YYYYMMDD_HHMM format
Rem Comment it or remove the comments (Rem) to use this format
Rem ---------------------------------------------------------------------------------

set Timestamp=%Year%%Month%%Day%_%Tim%

Rem - Set the environment variables if already not set
Rem - call SetEnv.bat

SET SOURCE=D:\Demantra_Batch\ECC_Inbound\New

SET DEST=D:\Demantra_Batch\ECC_Inbound\Old

set DPLOG=%CURRFOLDER%\PriceUploadDetails.log

ECHO Start. >> %DPLOG%

Rem - SQL Loader to load the csv into RR_PRICE_GSV staging table
sqlldr %USER%/%PASSWD%@%TNS% control=%CURRFOLDER%\T_IMP_PRICE_GSV.ctl log=%CURRFOLDER%\T_IMP_PRICE_GSV_SQLLDR_LOG.log data=%CURRFOLDER%\ECC_Inbound\New\%Year%_GSV.txt

Rem - SQL Loader to load the csv into RR_PRICE_SDC staging table
sqlldr %USER%/%PASSWD%@%TNS% control=%CURRFOLDER%\T_IMP_PRICE_SDC.ctl log=%CURRFOLDER%\T_IMP_PRICE_SDC_SQLLDR_LOG.log data=%CURRFOLDER%\ECC_Inbound\New\%Year%_SDC.txt

Rem - SQL Loader to load the csv into RR_PRICE_LP staging table
sqlldr %USER%/%PASSWD%@%TNS% control=%CURRFOLDER%\T_IMP_PRICE_LP.ctl log=%CURRFOLDER%\T_IMP_PRICE_LP_SQLLDR_LOG.log data=%CURRFOLDER%\ECC_Inbound\New\%Year%_PRC.txt

Rem - Move the CSV file from the DemantraTPM\RedRock\BulkUpload\New to DemantraTPM\RedRock\BulkUpload\Old to folder
IF EXIST %SOURCE%\%Year%_GSV.txt MOVE %SOURCE%\%Year%_GSV.txt %DEST%\%Year%_GSV_%Timestamp%.txt 

Rem - Move the CSV file from the DemantraTPM\RedRock\BulkUpload\New to DemantraTPM\RedRock\BulkUpload\Old to folder
IF EXIST %SOURCE%\%Year%_SDC.txt MOVE %SOURCE%\%Year%_SDC.txt %DEST%\%Year%_SDC_%Timestamp%.txt 

Rem - Move the CSV file from the DemantraTPM\RedRock\BulkUpload\New to DemantraTPM\RedRock\BulkUpload\Old to folder
IF EXIST %SOURCE%\%Year%_PRC.txt MOVE %SOURCE%\%Year%_PRC.txt %DEST%\%Year%_PRC_%Timestamp%.txt 

ECHO Loading CSV into Staging and MOVE done:  %DATE%  -  %TIME% >> %DPLOG%

ECHO %CURRFOLDER% >> %DPLOG%

Rem - Call script1 to load the price into Demantra tables
sqlplus %USER%/%PASSWD%@%TNS% @launch_price_import.sql

ECHO Import done:  %DATE%  -  %TIME% >> %DPLOG%

ECHO End. >> %DPLOG%

ECHO -------------------------------------------- >> %DPLOG%