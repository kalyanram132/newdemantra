@echo off
TITLE --- Load APO Forecast Data(weekly) - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load APO Forecast(Weekly) Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\LOADFCST.log MOVE ..\Logs\LOADFCST.log ..\Logs\Archive\LOADFCST\LOADFCST_%Timestamp%.log 

IF EXIST ..\Logs\LOADFCST.log DEL ..\Logs\LOADFCST.log

set DPLOG=..\Logs\LOADFCST.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load APO Forecast Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Import APO Forecast Workflow 
sqlplus %USER%/%PASSWD%@%TNS%   @loadfcst_weekly.sql >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_rrloadfcst_err

ECHO. >> %DPLOG%
ECHO Finished Load APO Forecast Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Creating APO Import Master data Errors list
ECHO Creating APO Import Master data Errors list >> %DPLOG% 
call apo_import_err.bat >> %DPLOG%
ECHO IF %ERRORLEVEL% NEQ 0 goto notif_err

ECHO Completed Creating csv with Master data error in APO Load
ECHO Completed Creating csv with Master data error in APO Load >> %DPLOG%

goto end_success

:prc_rrloadfcst_err
ECHO Procedure prc_load_apo_fcst failed
:notif_err
ECHO Failed to create error list 
goto end_error

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT