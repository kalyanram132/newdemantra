set heading off

select 'Starting to write EDI Claims errors' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 250

spool ..\CSV\EDI_CLAIM_ERR.csv

select 'CLAIM_NO,CUSTOMER_CLAIM_NO,START_DATE ,END_DATE ,CLAIM_TYPE ,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT  ,TTL_GST ,PAY_TO_CUSTOMER ,CURRENCY ,LINK_UNIT_DEAL ,LINK_CASE_DEAL,CUSTOMER_PROMO_NO,INVOICE_NO ,INVOICE_DATE,BRAND2 ,TAX_CLASS_MAT,MATERIAL ,BANNER  ,CUSTOMER ,CUSTOMER_LEVEL3  ,CUSTOMER_LEVEL4 ,SALES_OFFICE,PROMO_GROUP,ERR_DESC'
 from dual;

select
CLAIM_NO|| ',' ||
CUSTOMER_CLAIM_NO|| ',' ||
START_DATE|| ',' ||         
END_DATE|| ',' ||          
CLAIM_TYPE  || ',' ||
CD_DEAL_TYPE|| ',' ||
COOP_DEAL_TYPE|| ',' ||
CASE_DEAL_AMOUNT|| ',' ||  
COOP_AMOUNT|| ',' ||
TTL_GST || ',' ||   
PAY_TO_CUSTOMER|| ',' ||
CURRENCY || ',' ||
LINK_UNIT_DEAL|| ',' ||
LINK_CASE_DEAL || ',' || 
CUSTOMER_PROMO_NO || ',' ||
INVOICE_NO || ',' ||
INVOICE_DATE|| ',' ||           
BRAND2    || ',' ||
TAX_CLASS_MAT || ',' ||
MATERIAL     || ',' ||
BANNER   || ',' ||
CUSTOMER    || ',' ||
CUSTOMER_LEVEL3  || ',' || 
CUSTOMER_LEVEL4  || ',' ||
SALES_OFFICE  || ',' ||
PROMO_GROUP || ',' ||
ERR_DESC              
from t_Src_claims_err;


spool off;

set termout on;

set heading on;

exit;