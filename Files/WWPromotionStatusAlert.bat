@echo off
TITLE --- Load PromotionStatusAlertWW - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo PromotionStatusAlertWW procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------


call SetEnv.bat


IF EXIST ..\Logs\PromotionStatusAlertWW.log MOVE ..\Logs\PromotionStatusAlertWW.log ..\Logs\Archive\Logs\Archive\PromotionStatusAlert\WW\PromotionStatusAlertWW.log_%Timestamp%.log 

IF EXIST ..\Logs\PromotionStatusAlertWW.log DEL ..\PromotionStatusAlertWW.log

set DPLOG=..\Logs\PromotionStatusAlertWW.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load PromotionStatusAlertWW Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @WWPromotionStatusAlert.sql


ECHO. >> %DPLOG%
ECHO Finished Promotion Status Alert WW Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%