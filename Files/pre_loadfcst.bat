@echo off
TITLE --- Job to run before Loading APO Forecast Data - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load APO Forecast Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\pre_LOADFCST.log MOVE ..\Logs\pre_LOADFCST.log ..\Logs\Archive\LOADFCST\pre_LOADFCST_%Timestamp%.log 

IF EXIST ..\Logs\pre_LOADFCST.log DEL ..\Logs\pre_LOADFCST.log

set DPLOG=..\Logs\pre_LOADFCST.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load APO Forecast Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Import APO Forecast Workflow 
sqlplus %USER%/%PASSWD%@%TNS%   @pre_loadfcst.sql >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_rrloadfcst_err

ECHO. >> %DPLOG%
ECHO Finished Load APO Forecast Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Restarting Demantra Application server
ECHO Restarting Demantra Application server >> %DPLOG% 
REM call Restart.bat >> %DPLOG%
REM IF %ERRORLEVEL% NEQ 0 goto app_server_restart_err

ECHO Completed Restarting Application Server
ECHO Completed Restarting Application Server >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

REM ping 192.0.2.2 -n 1 -w 120000 > nul

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_rrloadfcst_err
ECHO Procedure prc_load_apo_fcst failed
goto end_error

:app_server_restart_err
ECHO Restarting Application Server failed
goto end_error

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT