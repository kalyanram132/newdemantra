@echo off
TITLE --- Error Notifications  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Error Notifications to users

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\error_notifs1.log MOVE ..\Logs\error_notifs1.log ..\Logs\Archive\error_notifs\error_notifs1.log_%Timestamp%.log 

IF EXIST ..\Logs\error_notifs1.log DEL ..\Logs\error_notifs1.log

set DPLOG=..\Logs\error_notifs1.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO Claim Import Errors
ECHO Claim Import Errors >> %DPLOG% 
call claim_import_err.bat >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto claim_import_err

ECHO Completed Claim Import Errors
ECHO Completed Claim Import Errors >> %DPLOG%



ECHO Non Export Claims
ECHO Non Export Claims >> %DPLOG% 
call non_export_claims.bat >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto non_export_claims_err

ECHO Completed Non Export Claims
ECHO Completed Non Export Claims >> %DPLOG%



ECHO Scan Errors
ECHO Scan Errors >> %DPLOG% 
call scan_error.bat >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto scan_err

ECHO Completed Scan Errors
ECHO Completed Scan Errors >> %DPLOG%



ECHO GL Transactions
ECHO Start GL Transactions >> %DPLOG% 
call gl_transactions.bat >> %DPLOG%

ECHO Completed GL Transactions
ECHO Completed GL Transactions >> %DPLOG%


goto end_success

:apo_import_err
ECHO APO Import Error batch Failed
goto prc_notif_err

:claim_import_err
ECHO Claim Import Error batch Failed
goto prc_notif_err

:non_export_claims_err
ECHO Not exported Claims batch Failed
goto prc_notif_err

:scan_err
ECHO Scan Error batch Failed
goto prc_notif_err

:prc_notif_err  
ECHO Error Notification Failed
goto end_error

:end_error
ECHO Demantra Error Notification failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Error Notification batch successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

exit;