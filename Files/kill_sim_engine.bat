@echo off
TITLE --- Kill Engine Batch - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv_eng.bat

IF EXIST ..\Logs\ENGINE.log MOVE ..\Logs\ENGINE.log ..\Logs\Archive\Engine\ENGINE.log_%Timestamp%.log 

COPY ..\Logs\Engine2k\%ENGINEFOLDER%\*.* ..\Logs\Engine2k\Archive\

DEL ..\Logs\Engine2k\%ENGINEFOLDER%\*.*

IF EXIST ..\Logs\ENGINE.log DEL ..\Logs\ENGINE.log

set DPLOG=..\Logs\ENGINE.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%


ECHO. >> %DPLOG%
ECHO Start Kill Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Killing Simulation Engine

killprocess iexplore.exe

killprocess enginemanager.exe

killprocess Engine2k_122.exe

killprocess EngineStarter.exe

taskkill /f /im iexplore.exe

taskkill /f /im enginemanager.exe

taskkill /f /im Engine2k_122.exe

taskkill /f /im EngineStarter.exe

ECHO. >> %DPLOG%
ECHO Finished Kill Simulation Engine >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%



goto end_success


:kill_sim_engine_err
ECHO Kill Simulation Engine failed
goto end_error

:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 
:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
