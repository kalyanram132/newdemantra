@echo off
TITLE --- Promo Accrual BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Promotions Accrual procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\PROMOACCRUAL.log MOVE ..\Logs\PROMOACCRUAL.log ..\Logs\Archive\PROMOACCRUAL\PROMOACCRUAL.log_%Timestamp%.log 

IF EXIST ..\Logs\PROMOACCRUAL.log DEL ..\Logs\PROMOACCRUAL.log

set DPLOG=..\Logs\PROMOACCRUAL.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Promotion Accrual Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running PROMOACCRUAL.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @PROMOACCRUAL.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Promotion Accrual Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Promotion Accrual Calculation Procedure failed
goto end_error



:end_error
ECHO Accrual Calculation Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Accrual Calculation successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT