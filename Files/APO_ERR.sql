set heading off

select 'Starting to generate CSV file for APO Forecast Errors ' from dual;

set feedback off

set verify off

set termout off

set pages 0

set feed off

set flush off

set linesize 150

spool "%APOERRCSVFOLDER%\apo_imp_err.csv"

select 'KeyValue1,KeyValue2,ErrorDetail,ErrorDate
' from dual; 



select 	 field_value						||','||
	 field_value2   						||','||
	 err_desc				    			||','||
	 err_date					 
from 	 RR_APO_ERR_T;	 


set termout on;

select 'CSV file for APO Forecast Errors created' from dual;

exit;