@echo off
TITLE --- Import Forecast Errors - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Master Data Errors in APO Forecast Import

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------


call SetEnv.bat


IF EXIST ..\Logs\APO_IMPORT_ERR_CSV.log MOVE ..\Logs\APO_IMPORT_ERR_CSV.log ..\Logs\Archive\APO_IMPORT_ERR_CSV\APO_IMPORT_ERR_CSV.log_%Timestamp%.log 

IF EXIST ..\Logs\APO_IMPORT_ERR_CSV.log DEL ..\Logs\APO_IMPORT_ERR_CSV.log

IF EXIST ..\CSV\apo_imp_err.csv MOVE ..\CSV\apo_imp_err.csv ..\CSV\Archive\apo_imp_err.csv_%Timestamp%.csv 

IF EXIST ..\CSV\apo_imp_err.csv DEL ..\CSV\apo_imp_err.csv

set DPLOG=..\Logs\APO_IMPORT_ERR_CSV.log 

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load for APO IMPORT Errors to CSV  >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @APO_ERR.sql



ECHO. >> %DPLOG%
ECHO Finished Load for APO IMPORT Errors to CSV >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%
goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end
ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT