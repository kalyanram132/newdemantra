set heading off

select 'Starting to write Promo Commit Status Check report' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 150

spool ..\CSV\WW_Promocommit_Check.csv

select 'Promotion,Promoted Group,Promotion ID,Evt Status,Start Event,End Event, Case Deal $, Ttl Evt Vol, Unit Deal, Promo Offset Start date, Promo Offset  End date'
 from dual;

SELECT P.PROMOTION_DESC||','||  t.p2b_desc ||','|| P.PROMOTION_ID ||','|| P.PROMOTION_STAT_ID ||','|| PD.FROM_DATE
||','|| PD.UNTIL_DATE ||','|| MAX(PA.CASE_BUYDOWN)
FROM PROMOTION P, PROMOTION_DATES PD, PROMOTION_DATA PA, mdp_matrix M, t_ep_p2b T
WHERE 
P.PROMOTION_ID = PD.PROMOTION_ID AND
P.PROMOTION_ID = PA.PROMOTION_ID AND
PA.ITEM_ID = M.ITEM_ID AND
PA.LOCATION_ID = M.LOCATION_ID AND
M. t_ep_p2b_ep_id = t.t_ep_p2b_ep_id AND
P.PROMOTION_STAT_ID = '2'
GROUP BY P.PROMOTION_DESC, P.PROMOTION_ID, P.PROMOTION_STAT_ID, PD.FROM_DATE, PD.UNTIL_DATE,  t.p2b_desc;


spool off;

set termout on;

exit;