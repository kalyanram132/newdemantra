@echo off
TITLE --- Job to delete more than 5  days old APO Forecast Data files - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Delete more than 5  days old APO Forecast files

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\DEL_OLD_APO_FILES.log MOVE ..\Logs\DEL_OLD_APO_FILES.log ..\Logs\Archive\LOADFCST\DEL_OLD_APO_FILES_%Timestamp%.log 

IF EXIST ..\Logs\DEL_OLD_APO_FILES.log DEL ..\Logs\DEL_OLD_APO_FILES.log

set DPLOG=..\Logs\DEL_OLD_APO_FILES.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Deleting more than 5  days old APO Baseline Forecast Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

del_old_files.vbs "%APO_INBOUND_PATH%Archive" ".TXT" "5"
IF %ERRORLEVEL% NEQ 0 goto prc_archive_err

ECHO. >> %DPLOG%
ECHO Finished Deleting more than 5  days old APO Baseline Forecast Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO. >> %DPLOG%
ECHO Start Deleting more than 5 days old APO Baseline load error Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

del_old_files.vbs "%APOERRCSVFOLDER%\Archive" ".csv" "5"
IF %ERRORLEVEL% NEQ 0 goto prc_archive2_err

ECHO. >> %DPLOG%
ECHO Finished Deleting more than 5  days old APO Baseline load error Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Deleting more than 5 days old APO Baseline load error Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

CD %LOGFOLDER%
del_nested_logs.vbs "."
IF %ERRORLEVEL% NEQ 0 goto prc_archive3_err

ECHO. >> %DPLOG%
ECHO Finished Deleting more than 5  days old APO Baseline load error Files >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success


:prc_archive_err
ECHO Job to delete Old APO forecast data failed
goto end_error

:prc_archive2_err
ECHO Job to delete Old APO baseline load error Files failed
goto end_error

:prc_archive3_err
ECHO Job to delete Old Demantra log Files failed
goto end_error

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed

goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed

goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT