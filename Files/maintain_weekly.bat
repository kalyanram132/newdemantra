@echo off
TITLE --- Weekly  Maitenance Batch  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Weekly Maintenance Batch

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\maintain_weekly.log MOVE ..\Logs\maintain_weekly.log ..\Logs\Archive\maintain_weekly\maintain_weekly.log_%Timestamp%.log 

IF EXIST ..\Logs\maintain_weekly.log DEL ..\Logs\maintain_weekly.log

set DPLOG=..\Logs\maintain_weekly.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Weekly Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running maintain_weekly.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @maintain_weekly.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Weekly Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Demantra Weekly Maitenance Batch Procedures  failed
goto end_error



:end_error
ECHO Demantra Weekly Maitenance Batch Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Demantra Weekly Maitenance Batch Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT