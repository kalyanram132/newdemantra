@echo off
TITLE --- Restart Demantra APP Server --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat

IF EXIST ..\Logs\Restart.log MOVE ..\Logs\Restart.log ..\Logs\Archive\Restart\Restart.log_%Timestamp%.log 

IF EXIST ..\Logs\Restart.log DEL ..\Logs\Restart.log

set DPLOG=..\Logs\Restart.log


ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Appserver restart >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Stopping Appserver
ECHO net stop tomcat7 >> %DPLOG%

IF %ERRORLEVEL% NEQ 0 goto stop_app_server

ECHO. >> %DPLOG%
ECHO Finished Stopping Appserver >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO Starting Appserver
ECHO net start tomcat7 >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto start_app_server

ECHO. >> %DPLOG%
ECHO Starting Demantra Appserver >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO. >> %DPLOG%
ECHO Finished Appserver restart >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success


:stop_app_server
ECHO Stopping app server failed
goto end_error

:start_app_server
ECHO Starting app server failed
goto end_error

:end_error
ECHO Appserver restart Failed  >> %DPLOG%
goto end 
:end_success 
ECHO Restart of Demantra Appserver successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
