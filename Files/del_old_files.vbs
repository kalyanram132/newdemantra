Option Explicit 
On Error Resume Next 
Dim oFSO, oFolder, sDirectoryPath  
Dim oFileCollection, oFile, sDir, oFileExt  
Dim iDaysOld 

' Specify Directory Path From Where You want to clear the old files 

		sDirectoryPath = WScript.Arguments(0)


Set oFSO = CreateObject("Scripting.FileSystemObject") 
Set oFolder = oFSO.GetFolder(sDirectoryPath) 
Set oFileCollection = oFolder.Files 

For each oFile in oFileCollection

'This section will filter the text file as i have used for for test 
'Specify the Extension of file that you want to delete 
'and the number with Number of character in the file extension  
     oFileExt =   WScript.Arguments(1)      

' Specify Number of Days Old File to Delete

iDaysOld = WScript.Arguments(2)    
      
    If LCase(instr(oFile.Name,oFileExt)) > 1 Then 

        If oFile.DateLastModified < (Date() - iDaysOld) Then 
        oFile.Delete(True) 
        End If 

    End If     
Next 

Set oFSO = Nothing 
Set oFolder = Nothing 
Set oFileCollection = Nothing 
Set oFile = Nothing
