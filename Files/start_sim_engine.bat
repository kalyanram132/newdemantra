@echo off
TITLE ---  Sim Engine Batch - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv_eng.bat

IF EXIST ..\Logs\ENGINE.log MOVE ..\Logs\ENGINE.log ..\Logs\Archive\Engine\start_sim_engine.log_%Timestamp%.log 

COPY ..\Logs\Engine2k\%ENGINEFOLDER%\*.* ..\Logs\Engine2k\Archive\

DEL ..\Logs\Engine2k\%ENGINEFOLDER%\*.*

IF EXIST ..\Logs\start_sim_engine.log DEL ..\Logs\start_sim_engine.log

set DPLOG=..\Logs\start_sim_engine.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO Running Engine
ECHO %ENGINEROOT%\EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 12 >> %DPLOG%
ECHO call %ENGINEROOT%\Start_Simulation2K.bat 99 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% >> %DPLOG%

call %ENGINEROOT%\EngineStarter.bat %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% %APPSERVER% 12 >> %DPLOG%

ECHO call %ENGINEROOT%\Start_Simulation2K.bat 99 %DBSEVER%:%DBPORT%/%TNS% %USER% %PASSWD% >> %DPLOG%
 IF %ERRORLEVEL% NEQ 0 goto start_sim_engine_err

ECHO. >> %DPLOG%
ECHO Finished Simulation Engine Run >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success


:start_sim_engine_err
ECHO Start Simulation Engine failed
goto end_error

:end_error
ECHO Load of data failed  >> %DPLOG%
goto end 
:end_success 
ECHO Load of data successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
