@echo off
TITLE --- Export Forecast BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Forecast Export procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\expfcst.log MOVE ..\Logs\expfcst.log ..\Logs\Archive\expfcst\expfcst.log_%Timestamp%.log 

IF EXIST ..\Logs\expfcst.log DEL ..\Logs\expfcst.log

set DPLOG=..\Logs\expfcst.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Forecast Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running expfcst.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @expfcst_weekly.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Forecast Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Sending APO Import Error Notification Email
ECHO Sending APO Import Error Notification Email >> %DPLOG% 
call apo_import_err.bat >> %DPLOG%
ECHO IF %ERRORLEVEL% NEQ 0 goto notif_err

ECHO Completed Sending APO Import Error Notification Email
ECHO Completed Sending APO Import Error Notification Email >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_data_load_err
ECHO Demantra Forecast Export Procedures  failed
goto end_error

:notif_err
ECHO Sending APO Import Error Notification Email
goto end_error

:end_error
ECHO Demantra Forecast Export Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Forecast Export Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT