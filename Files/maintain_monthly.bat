@echo off
TITLE --- Monthly Maitenance Batch  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Monthly Maintenance Batch

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\maintain_Monthly.log MOVE ..\Logs\maintain_Monthly.log ..\Logs\Archive\maintain_Monthly\maintain_Monthly.log_%Timestamp%.log 

IF EXIST ..\Logs\maintain_Monthly.log DEL ..\Logs\maintain_Monthly.log

set DPLOG=..\Logs\maintain_Monthly.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Monthly Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running maintain_Monthly.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @maintain_Monthly.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Monthly Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Demantra Monthly Maitenance Batch Procedures  failed
goto end_error



:end_error
ECHO Demantra Monthly Maitenance Batch Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Demantra Monthly Maitenance Batch Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT