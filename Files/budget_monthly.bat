@echo off
TITLE --- Budget Snapshot Monthly  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Budget monthly snapshot

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\budget_monthly.log MOVE ..\Logs\budget_monthly.log ..\Logs\Archive\budget_monthly\budget_monthly.log_%Timestamp%.log 

IF EXIST ..\Logs\budget_monthly.log DEL ..\Logs\budget_monthly.log

set DPLOG=..\Logs\budget_monthly.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Budget monthly snapshot Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running budget_monthly.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @budget_monthly.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Budget monthly snapshot Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Budget monthly snapshot Procedures  failed
goto end_error



:end_error
ECHO Demantra Budget monthly snapshot Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Budget monthly snapshot successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT