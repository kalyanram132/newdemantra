@echo off
TITLE --- start Demantra APP Server --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat

IF EXIST ..\Logs\Starttomcat7.log MOVE ..\Logs\Starttomcat7.log ..\Logs\Archive\Start\Starttomcat7.log_%Timestamp%.log 

IF EXIST ..\Logs\Starttomcat7.log DEL ..\Logs\Starttomcat7.log

set DPLOG=..\Logs\Starttomcat7.log


ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Appserver start >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Starting Appserver
net start tomcat7 >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto start_app_server


ECHO. >> %DPLOG%
ECHO Finished Appserver start >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success


:start_app_server
ECHO Starting app server failed
goto end_error


:end_success 
ECHO start of Demantra Appserver successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
