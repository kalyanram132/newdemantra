@echo off
TITLE --- Stop Demantra APP Server --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Engine Batch procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat

IF EXIST ..\Logs\Stoptomcat7.log MOVE ..\Logs\Stoptomcat7.log ..\Logs\Archive\stop\Stoptomcat7.log_%Timestamp%.log 

IF EXIST ..\Logs\Stoptomcat7.log DEL ..\Logs\Stoptomcat7.log

set DPLOG=..\Logs\Stoptomcat.log


ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Appserver stop >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Stopping Appserver
net stop tomcat7 >> %DPLOG%

IF %ERRORLEVEL% NEQ 0 goto stop_app_server

ECHO. >> %DPLOG%
ECHO Finished Stopping Appserver >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:stop_app_server
ECHO Stopping app server failed
goto end_error

:end_success 
ECHO Stop of Demantra Appserver successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
