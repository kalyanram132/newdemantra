@echo off
TITLE --- Budget Snapshot qtrly  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Budget qtrly snapshot

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\budget_qtrly.log MOVE ..\Logs\budget_qtrly.log ..\Logs\Archive\budget_qtrly\budget_qtrly.log_%Timestamp%.log 

IF EXIST ..\Logs\budget_qtrly.log DEL ..\Logs\budget_qtrly.log

set DPLOG=..\Logs\budget_qtrly.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Budget qtrly snapshot Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running budget_qtrly.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @budget_qtrly.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Budget qtrly snapshot Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Budget qtrly snapshot Procedures  failed
goto end_error



:end_error
ECHO Demantra Budget qtrly snapshot Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Budget qtrly snapshot successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT