@echo off
TITLE --- Promo Accrual Overspend BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Promotions Accrual Overspend procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\PROMOACCRUALOS.log MOVE ..\Logs\PROMOACCRUALOS.log ..\Logs\Archive\PROMOACCRUALOS\PROMOACCRUALOS.log_%Timestamp%.log 

IF EXIST ..\Logs\PROMOACCRUALOS.log DEL ..\Logs\PROMOACCRUALOS.log

set DPLOG=..\Logs\PROMOACCRUALOS.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Promotion Accrual Overspend Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running PROMOACCRUALOS.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @PROMOACCRUALOS.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Promotion Accrual Overspend Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Promotion Accrual Overspend Calculation Procedure failed
goto end_error



:end_error
ECHO Accrual Overspend Calculation Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Accrual Overspend Calculation successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT