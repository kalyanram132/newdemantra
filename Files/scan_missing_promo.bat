@echo off
TITLE --- Load Scan Data - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load Scan Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\EXCEP_SCANMISSINGPROMO.log MOVE ..\Logs\EXCEP_SCANMISSINGPROMO.log ..\Logs\Archive\LOADSCAN\EXCEP_SCANMISSINGPROMO.log_%Timestamp%.log 

IF EXIST ..\Logs\EXCEP_SCANMISSINGPROMO.log DEL ..\Logs\EXCEP_SCANMISSINGPROMO.log

set DPLOG=..\Logs\EXCEP_SCANMISSINGPROMO.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Export AU PromosProcedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @scan_missing_promo.sql


ECHO. >> %DPLOG%
ECHO Finished Export AU Promo Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT