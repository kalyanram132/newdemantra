@echo off
TITLE --- Process Claims BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Process Claim procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\PROCESSCLAIM.log MOVE ..\Logs\PROCESSCLAIM.log ..\Logs\Archive\PROCESSCLAIM\PROCESSCLAIM.log_%Timestamp%.log 

IF EXIST ..\Logs\PROCESSCLAIM.log DEL ..\Logs\PROCESSCLAIM.log

set DPLOG=..\Logs\PROCESSCLAIM.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Claim Processing Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running PROCESSCLAIM.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @PROCESSCLAIM.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Claim Processing Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Claim Processing Procedure failed
goto end_error



:end_error
ECHO Claim Processing failed  >> %DPLOG%
goto end 

:end_success 
ECHO Claim Processing successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT