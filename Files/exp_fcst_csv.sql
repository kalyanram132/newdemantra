set heading off

select 'Starting to generate CSV file for Export to APO' from dual;

set feedback off

set verify off

set termout off

set pages 0

set feed off

set flush off

set linesize 150

spool "%APO_OUTBOUND_PATH%\t_exp_fcst_daily.txt"

select 'SKU_CODE|CL3_CODE|SALES_OFFICE|SDATE|DMTRA_OFFSET_BASE_FCST|DMTRA_INCR_FCST|SCAN_ACTUALS_OFFSET' from dual; 


select 	 SKU_CODE   						||'|'||
	 CL3_CODE				    			||'|'||
   SALES_OFFICE           	||'|'||
   to_char(SDATE,'YYYYMMDD')						||'|'||
	 round(DMTRA_OFFSET_BASE_FCST,3)		||'|'||
   round(DMTRA_INCR_FCST,3)		||'|'||
	 round(SCAN_ACTUALS_OFFSET,3)			 
from 	 t_exp_fcst_daily
order by sku_code,cl3_code,sales_office,sdate;	 

set termout ON;


exit;