set heading off

select 'Starting to write Scan errors' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 150

spool ..\CSV\SCAN_ERR.csv

select 'MATERIAL - Level1,CL3 - Level2,BANNER - Level3,SALES_OFFICE - Level4,SALES_DATE,SHELF_PRICE,PROMO_PRICE,BASE_VOL,INCREMENTAL_VOL,TOTAL_VOL,ERROR_MSG'
 from dual;

select
level1|| ',' ||
level2|| ',' ||
level3|| ',' ||
level4|| ',' ||
sdate|| ',' ||
shelf_price_sd|| ',' ||
sdata15|| ',' ||
DEMAND_BASE|| ',' ||
actuals_incr|| ',' ||
ERR_Message              
from biio_scan_err;
spool off;

set termout on;

exit;