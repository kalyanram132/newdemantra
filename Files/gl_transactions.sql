set heading off

select 'Starting to write GL Transactions' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 200

spool ..\CSV\GL_TRANSACTIONS.csv

select 'GL Post Date,CL3,Promoted Group,GL Account,Transaction Type,Promotion ID,Promo Start Date, Promo End Date,Sales Date,Case Deal $, Lumpsum $'
 from dual;


select gl_post_date ||','|| c3.ebs_customer_DESC ||','|| pg.P2B_DESC ||','|| decode(cd_deal_type,'ZT01','Case/Lump','ZT03','Margin Sup','ZT07','Other',cd_deal_type) ||','|| TRANSACTION_TYPE ||','|| g.promotion_id ||','|| d.from_date ||','|| d.until_date
||','|| sales_date ||','|| sum(case_deal_amount) ||','|| sum(coop_amount) 
from t_Exp_gl g, mdp_matrix m, t_ep_site s,t_ep_item i,t_ep_ebs_customer c3,t_ep_p2b pg, promotion_dates d
where gl_post_date > sysdate -32
and m.t_ep_site_ep_id = s.t_ep_site_ep_id
and m.t_ep_item_ep_id = i.t_ep_item_ep_id
and m.t_ep_ebs_customer_ep_id = c3.t_ep_ebs_customer_ep_id 
and m.t_ep_p2b_ep_id = pg.t_ep_p2b_ep_id
and g.customer_no || '-30110101'= s.site
and g.material = i.item
and g.promotion_id = d.promotion_id
group by gl_post_date,c3.ebs_customer_DESC,pg.P2B_DESC,cd_Deal_type,sales_date,g.promotion_id,d.from_date,d.until_date,TRANSACTION_TYPE
order by gl_post_date,c3.ebs_customer_DESC,pg.P2B_DESC ,g.promotion_id,sales_date;

spool off;

set termout on;

exit;

