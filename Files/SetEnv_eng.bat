SET PATH=D:\Oracle Demantra Spectrum\Demand Planner\DeskTop;%PATH%
SET PATH=D:\Oracle Demantra Spectrum\Demand Planner\Security Management;%PATH%
SET PATH=D:\Oracle Demantra Spectrum\Demand Planner\Analytical Engines\bin;%PATH%
SET ENGINEROOT=D:\Oracle Demantra Spectrum\Demand Planner\Analytical Engines
SET PATH=D:\Oracle\app\client\product\12.1.0\client_1\BIN;%PATH%
SET ORACLE_HOME=D:\Oracle\app\client\product\12.1.0\client_1


SET TNS=DMTRPRD
SET USER=demantra
SET PASSWD=Asahi2017

SET DBSEVER=pdcaap59
SET DBPORT=1521
SET APPSERVER=pdcaap57

SET ENGINEFOLDER=pdcaap59_DEMANTRA

SET CURRFOLDER=D:\Demantra_Batch\Files

SET LOGFOLDER=D:\Demantra_Batch\Logs
SET APO_INBOUND_PATH=D:\Demantra_Batch\APO_Inbound\
SET APO_OUTBOUND_PATH=D:\Demantra_Batch\APO_Outbound\
SET APOERRCSVFOLDER=D:\Demantra_Batch\CSV



D:

CD %CURRFOLDER%

Rem ---------------------------------------------------------------------------------
Rem Assemble a date using DOS commands
Rem ---------------------------------------------------------------------------------

for /F "tokens=1-4 delims=/ " %%i in ('date /t') do (
   set DayOfWeek=%%i
   set Month=%%j
   set Day=%%k
   set Year=%%l
   set Date=%%i %%j/%%k/%%l
)

for /F "tokens=1-2 delims=: " %%i in ('time /t') do (
   set hours=%%i
   set minutes=%%j
   set Tim=%%i%%j
)

Rem ---------------------------------------------------------------------------------
Rem Set up a timestamp variable in YYYYMMDD_HHMM format
Rem Comment it or remove the comments (Rem) to use this format
Rem ---------------------------------------------------------------------------------

set Timestamp=%Year%%Month%%Day%_%Tim%