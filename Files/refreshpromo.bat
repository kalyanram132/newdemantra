@echo off
TITLE --- Refresh Promo Population BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Refresh Promotions Population

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\REFRESHPROMO.log MOVE ..\Logs\REFRESHPROMO.log ..\Logs\Archive\PROMOACCRUAL\REFRESHPROMO.log_%Timestamp%.log 

IF EXIST ..\Logs\REFRESHPROMO.log DEL ..\Logs\REFRESHPROMO.log

set DPLOG=..\Logs\REFRESHPROMO.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Refresh Promotion Population Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running REFRESHPROMO.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @REFRESHPROMO.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Refresh Promotion PopulationProcedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Refresh Promotion Population Procedure failed
goto end_error



:end_error
ECHO Refresh Promotion Population Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Refresh Promotion Population successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT