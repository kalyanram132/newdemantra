@echo off
TITLE --- Job to load APO Forecast Data to Demantra staging table - BATCH LOAD --- --- ---
@echo on
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load APO Forecast Data to Demantra staging procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\LOAD_APO_DATA.log MOVE ..\Logs\LOAD_APO_DATA.log ..\Logs\Archive\LOADFCST\LOAD_APO_DATA_%Timestamp%.log 

IF EXIST ..\Logs\LOAD_APO_DATA.log DEL ..\Logs\LOAD_APO_DATA.log

set DPLOG=..\Logs\LOAD_APO_DATA.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load APO Forecast Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Import APO Forecast Workflow 
sqlldr %USER%/%PASSWD%@%TNS%   control=load_data_2.ctl, ERRORS=999999 >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_rrloadfcst_err

ECHO. >> %DPLOG%
ECHO Finished Load APO Forecast Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_rrloadfcst_err
ECHO Job to load APO forecast data to staging failed
goto end_error

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed

goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed

goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT