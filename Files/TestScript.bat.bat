
TITLE --- Test Script - DEMANTRA BATCH --- --- ---

echo Batch File Called From Scheduler OR Workflow (dp)
echo EP LOAD procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------


call SetEnv.bat


IF EXIST ..\Logs\TestScript.log MOVE ..\Logs\TestScript.log ..\Logs\Archive\TestScript\TestScript.log_%Timestamp%.log 

IF EXIST ..\Logs\TestScript.log DEL ..\Logs\TestScript.log

set DPLOG=..\Logs\TestScript.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Sample proc >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running rr_test_batch_run 
sqlplus %USER%/%PASSWD%@%TNS%   @TestProc.sql >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_run_err

ECHO. >> %DPLOG%
ECHO Finished Sample PL SQL Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_run_err
ECHO Procedure sample proc failed
goto end_error


:end_error
ECHO Procedure run failed  >> %DPLOG%
goto end 
:end_success 
ECHO Procedure run successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT