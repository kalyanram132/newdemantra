@echo off
TITLE --- Audit table delete and reorg BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Audit table delete and reorg procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat 


IF EXIST ..\Logs\Audittabledeleteandreorg.log MOVE ..\Logs\Audittabledeleteandreorg.log ..\Logs\Archive\Audit\Audittabledeleteandreorg.log_%Timestamp%.log 

IF EXIST ..\Logs\Audittabledeleteandreorg.log DEL ..\Logs\Audittabledeleteandreorg.log

set DPLOG=..\Logs\Audittabledeleteandreorg.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Audittabledeleteandreorg Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Audit_reorg.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @Audit_reorg.sql >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_delete_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Audittabledeleteandreorg Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_delete_err
ECHO Demantra Audittabledeleteandreorg failed
goto end_error



:end_error
ECHO Audittabledeleteandreorg failed  >> %DPLOG%
goto end 

:end_success 
ECHO Audittabledeleteandreorg completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT