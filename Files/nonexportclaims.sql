set heading off

select 'Starting to write Non Exported Claims' from dual;

set feedback off
set verify off
set termout off
set pages 0
set feed off
set flush off
set linesize 150

spool ..\CSV\Claim_not_exported.csv

select 
'Region,CL2,Sales Office,CLAIM_DESC,AP_AR,INVALID,NO_LINES,PERCENTAGE,TOTAL_CLAIM,CREATED_DATE'
 from dual;

SELECT  r.l_att_10_desc||','||p.EBS_CUSTOMER_DESC||','||c.L_ATT_6_desc||','||settlement_desc||','||DECODE (GL_CODE_ID, 0, 'AR', 'AP') ||','||(SELECT COUNT (*)
            FROM settlement s1
           WHERE s.settlement_desc = s1.settlement_desc
             AND gl_exp_claim IN (0, 2, 3)) ||','||
         COUNT (*) ||','||
           (SELECT COUNT (*)
              FROM settlement s1
             WHERE s.settlement_desc = s1.settlement_desc
               AND gl_exp_claim IN (0, 2, 3))
         / COUNT (*)
         * 100 ||','||
         SUM (settlement_amount) ||','|| to_char(min(date_posted),'dd/mm/yyyy') 
    FROM settlement s, t_ep_l_att_10 r, t_ep_L_ATT_6 c, t_ep_ebs_customer p
   WHERE 1=1
     AND s.t_ep_L_ATT_10_ep_id = r.t_ep_l_att_10_ep_id 
     AND s.t_ep_L_ATT_6_ep_id = c.t_ep_L_ATT_6_ep_id
     AND s.key_account = p.t_ep_ebs_customer_ep_id
     AND gl_exp_claim <> 1
     AND s.settlement_Desc not like '%Default%'
GROUP BY r.l_att_10_desc,c.l_att_6_desc,settlement_desc, GL_CODE_ID,p.EBS_CUSTOMER_DESC; 

/* 
-- new code adding bill to -- SR -- 


SELECT  r.l_att_10_desc||','||c.e1_cust_cat_2_desc||','||settlement_desc||','||DECODE (GL_CODE_ID, 0, 'AR', 'AP') ||','||(SELECT COUNT (*)
            FROM settlement s1
           WHERE s.settlement_desc = s1.settlement_desc
             AND gl_exp_claim IN (0, 2, 3)) ||','||
         COUNT (*) ||','||
           (SELECT COUNT (*)
              FROM settlement s1
             WHERE s.settlement_desc = s1.settlement_desc
               AND gl_exp_claim IN (0, 2, 3))
         / COUNT (*)
         * 100 ||','||
         SUM (settlement_amount) ||','|| to_char(min(date_posted),'dd/mm/yyyy') ||','|| v.customer_desc  
  FROM hz.settlement s full outer join t_src_vendor v on s.bill_to = v.vendor_id
   ,t_ep_l_att_10 r, t_ep_e1_cust_cat_2 c
   WHERE 1=1
    AND s.t_ep_l_att_10_ep_id = r.t_ep_l_att_10_ep_id 
     AND s.t_ep_e1_cust_cat_2_ep_id = c.t_ep_e1_cust_cat_2_ep_id
     AND gl_exp_claim <> 1
     AND s.settlement_Desc not like '%Default%'
GROUP BY r.l_att_10_desc,c.e1_cust_cat_2_desc,settlement_desc, GL_CODE_ID, v.customer_Desc;

*/


spool off;

set termout on;

exit;

