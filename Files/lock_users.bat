@echo off
TITLE --- Lock Users BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Lock Users 

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\lock_users.log MOVE ..\Logs\lock_users.log ..\Logs\Archive\lock_users\lock_users.log_%Timestamp%.log 

IF EXIST ..\Logs\lock_users.log DEL ..\Logs\lock_users.log

set DPLOG=..\Logs\lock_users.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Locking out Demantra users >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running lock_users.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @lock_users.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_lock_users_err

ECHO Killing Simulation Engine
start /D "%CURRFOLDER%" kill_sim_engine.bat

ECHO. >> %DPLOG%
ECHO Finished Locking out Demantra users >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_lock_users_err
ECHO Demantra Locking out Demantra users procedures failed
goto end_error

:end_error
ECHO Demantra Locking users failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Locking users successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT