@echo off
TITLE --- Load claims Data - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load claims Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\LOADCLAIMS.log MOVE ..\Logs\LOADCLAIMS.log ..\Logs\Archive\LOADCLAIMS\LOADCLAIMS_%Timestamp%.log 

IF EXIST ..\Logs\LOADCLAIMS.log DEL ..\Logs\LOADCLAIMS.log

set DPLOG=..\Logs\LOADCLAIMS.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load claims Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Import EDI Claims Workflow 
sqlplus %USER%/%PASSWD%@%TNS%   @loadclaims.sql >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_rrloadclaims_err

ECHO. >> %DPLOG%
ECHO Finished Load claims Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_rrloadclaims_err
ECHO Procedure prc_rrloadclaims failed
goto end_error

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT