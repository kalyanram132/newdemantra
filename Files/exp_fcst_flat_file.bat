@echo off
TITLE --- Export Demantra Incrementals to APO - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Export Demantra Incrementals to APO

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\EXP_TO_APO_CSV.log MOVE ..\Logs\EXP_TO_APO_CSV.log ..\Logs\Archive\EXP_TO_APO_CSV\EXP_TO_APO_CSV.log_%Timestamp%.log 

IF EXIST ..\Logs\EXP_TO_APO_CSV.log DEL ..\Logs\EXP_TO_APO_CSV.log

IF EXIST "%APO_OUTBOUND_PATH%\t_exp_fcst_daily.csv" MOVE "%APO_OUTBOUND_PATH%\t_exp_fcst_daily.csv" "%APO_OUTBOUND_PATH%\Archive\t_exp_fcst_daily.csv"%Timestamp%.csv 

IF EXIST "%APO_OUTBOUND_PATH%\t_exp_fcst_daily.csv" DEL "%APO_OUTBOUND_PATH%\t_exp_fcst_daily.csv"

set DPLOG=..\Logs\EXP_TO_APO_CSV.log 

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load for APO Export Table to CSV  >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @exp_fcst_csv.sql



ECHO. >> %DPLOG%
ECHO Finished Load for APO IMPORT Errors to CSV >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%
goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end
ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%

EXIT