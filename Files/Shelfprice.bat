@echo off
TITLE --- Shelf Price BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Shelf Price procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\Shelfprice.log MOVE ..\Logs\Shelfprice.log ..\Logs\Archive\Shelfprice\Shelfprice.log_%Timestamp%.log 

IF EXIST ..\Logs\Shelfprice.log DEL ..\Logs\Shelfprice.log

set DPLOG=..\Logs\Shelfprice.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Shelf Price Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running Shelfprice.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @Shelfprice.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Shelf Price Calculation Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Shelf Price Calculation Procedure failed
goto end_error



:end_error
ECHO Shelf Price Calculation Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Shelf Price Calculation successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT