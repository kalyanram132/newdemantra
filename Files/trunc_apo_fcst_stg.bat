@echo off
TITLE --- Clear Export Forecast staging BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo clearing Forecast staging table 

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\trunc_apo_fcst_stg.log MOVE ..\Logs\trunc_apo_fcst_stg.log ..\Logs\Archive\trunc_apo_fcst_stg\trunc_apo_fcst_stg.log_%Timestamp%.log 

IF EXIST ..\Logs\trunc_apo_fcst_stg.log DEL ..\Logs\trunc_apo_fcst_stg.log

set DPLOG=..\Logs\trunc_apo_fcst_stg.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Forecast Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running trunc_apo_fcst_stg.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @trunc_apo_fcst_stg.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra clearing Forecast staging table >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

goto end_success

:prc_data_load_err
ECHO Demantra clearing Forecast staging table  failed
goto end_error

:app_server_restart_err
ECHO Demantra Application Serve Restart Failed
goto end_error

:end_error
ECHO Demantra clearing Forecast staging table failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra clearing Forecast staging table successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT