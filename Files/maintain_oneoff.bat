@echo off
TITLE --- Monthly Maitenance Batch  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo oneoff Maintenance Batch

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\maintain_oneoff.log MOVE ..\Logs\maintain_oneoff.log ..\Logs\Archive\maintain_Monthly\maintain_oneoff.log_%Timestamp%.log 

IF EXIST ..\Logs\maintain_oneoff.log DEL ..\Logs\maintain_oneoff.log

set DPLOG=..\Logs\maintain_oneoff.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra oneoff Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running maintain_oneoff.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @maintain_oneoff.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra oneoff Maitenance Batch Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Demantra oneoff Maitenance Batch Procedures  failed
goto end_error



:end_error
ECHO Demantra oneoff Maitenance Batch Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Demantra oneoff Maitenance Batch Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT