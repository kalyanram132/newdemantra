@echo off
TITLE --- Load Export Claims - BATCH LOAD --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Load Scan Data procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------


call SetEnv.bat


IF EXIST ..\Logs\EXCEP_NONEXPORTCLAIM.log MOVE ..\Logs\EXCEP_NONEXPORTCLAIM.log ..\Logs\Archive\LOADSCAN\EXCEP_NONEXPORTCLAIM.log_%Timestamp%.log 

IF EXIST ..\Logs\EXCEP_NONEXPORTCLAIM.log DEL ..\Logs\EXCEP_NONEXPORTCLAIM.log

set DPLOG=..\Logs\EXCEP_NONEXPORTCLAIM.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Load Non Export Claims Workflow Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


sqlplus %USER%/%PASSWD%@%TNS% @NONEXPORTCLAIMS.sql


ECHO. >> %DPLOG%
ECHO Finished Load Scan Workflow Procedure >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:end_error
ECHO Batch failed  >> %DPLOG%
ECHO Batch failed
ECHO BATCH FAILED  >> %DPLOG%
ECHO BATCH FAILED
goto end 
:end_success 
ECHO Batch successfuly completed >> %DPLOG%
ECHO Batch successfuly completed
ECHO BATCH SUCCEEDED >> %DPLOG%
ECHO BATCH SUCCEEDED
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%