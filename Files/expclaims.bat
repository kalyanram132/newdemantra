@echo off
TITLE --- Export Claims BATCH  --- --- ---
@echo off
echo Batch File Called From Scheduler OR Workflow (dp)
echo Claims Export procedures

echo

Rem ---------------------------------------------------------------------------------
Rem Set the environment variables if already not set
Rem ---------------------------------------------------------------------------------

call SetEnv.bat


IF EXIST ..\Logs\expclaims.log MOVE ..\Logs\expclaims.log ..\Logs\Archive\expclaims\expclaims.log_%Timestamp%.log 

IF EXIST ..\Logs\expclaims.log DEL ..\Logs\expclaims.log

set DPLOG=..\Logs\expclaims.log

ECHO Started:    %Day%-%Month%-%Year%-%Time% >> %DPLOG%

ECHO. >> %DPLOG%
ECHO Start Demantra Claims Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%

ECHO Running expclaims.sql 
sqlplus %USER%/%PASSWD%@%TNS%   @expclaims.sql  >> %DPLOG%
IF %ERRORLEVEL% NEQ 0 goto prc_data_load_err

ECHO. >> %DPLOG%
ECHO Finished Demantra Claims Export Procedures >> %DPLOG%
ECHO =================================== >> %DPLOG%
ECHO %DATE% - %TIME% >> %DPLOG%
ECHO. >> %DPLOG%


goto end_success

:prc_data_load_err
ECHO Demantra Claims Export Procedures  failed
goto end_error



:end_error
ECHO Demantra Claims Export Procedures failed  >> %DPLOG%
goto end 

:end_success 
ECHO Demantra Claims Export Procedures successfuly completed >> %DPLOG%
goto end 

:end

ECHO =================================== >> %DPLOG%

ECHO Completed:  %DATE%  -  %TIME% >> %DPLOG%
EXIT