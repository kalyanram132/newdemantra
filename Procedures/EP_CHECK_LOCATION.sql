--------------------------------------------------------
--  DDL for Procedure EP_CHECK_LOCATION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."EP_CHECK_LOCATION" -- $Revision: 1.200.14.3.2.2 $

      (dis_append IN VARCHAR2 DEFAULT NULL)
IS
   vs_sql                     VARCHAR2(32000);
   vs_msg                     VARCHAR2(2000);
   vs_proc                    VARCHAR2(30) := 'EP_CHECK_LOCATION';
 
   vi_error_code              NUMBER;
   vs_error_text              VARCHAR2(4000);
 
   dvi_exists                 INTEGER;
   dvs_sqlstr                 VARCHAR2(32000);
   dvs_sqltmp                 VARCHAR2(32000);
   dvi_err_code               INTEGER;
   dvs_err_msg                VARCHAR2(2000);
   dvs_field_name             VARCHAR2(30);
   dvs_proc_name              VARCHAR2(30) :='EP_CHECK_LOCATION';
   dvd_sysdate                DATE         := CAST(SYSTIMESTAMP AS DATE);
   dv_max_id                  INTEGER;
 
   vi_data_err_count          INTEGER := 0;
   vi_data_err_chk_01         INTEGER := 0;
   vi_data_err_chk_02         INTEGER := 0;
   vi_data_err_chk_03         INTEGER := 0;
   vi_data_err_chk_12         INTEGER := 0;
   vi_data_errors_tot         INTEGER := 0;
 
 
   vi_logging_level           INTEGER;
   vs_log_table               VARCHAR2(30);
   vs_proc_source             VARCHAR2(60);
   vi_log_it_commit           INTEGER;
   vs_log_msg                 VARCHAR2(2000);
   vs_temp_proc_name          VARCHAR2(30);
 
   vd_start_time_proc         NUMBER;
   vd_start_time              NUMBER;
   vd_end_time                NUMBER;
 
   vt_procedure               NUMBER := 0;
 
BEGIN
 
   pre_logon;
 
   set_module ( 'S' );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_start_time_proc := DBMS_UTILITY.get_time;
 
   --------------------------------------------------------------------------
   -- Record the Parameters passed to DB_CALL_LOG
   --------------------------------------------------------------------------
   vs_msg := 'dis_append => '  || NVL( dis_append  ,'NULL');
 
   DBEX(vs_msg, vs_proc, 'C');
 
   --------------------------------------------------------------------------
   -- Initialize LOG_IT
   --------------------------------------------------------------------------
   vs_proc_source   := 'EP_CHECK_LOCATION';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level, vi_log_it_commit);

   vs_log_msg := RPAD('Logging Level',31) ||': '|| vi_logging_level;                                               LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'SP', vs_log_msg , 'T' , 'C', NULL);


 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
 
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL                           INSERT to _ERR table
   --------------------------------------------------------------------------
   vs_log_msg := 'Check 01 : Code IS NULL       Insert to _ERR table';                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   vs_sql := 'INSERT INTO t_src_loc_tmpl_err
              SELECT T_SRC_LOC_TMPL.*,1,'''|| dvd_sysdate ||''',''Code IS NULL - ''
                     || DECODE (t_ep_lr2                      , NULL,''t_ep_lr2; '','''')
                     || DECODE (t_ep_ls3                      , NULL,''t_ep_ls3; '','''')
                     || DECODE (t_ep_l_att_4                  , NULL,''t_ep_l_att_4; '','''')
                     || DECODE (e1_branch_category_5          , NULL,''e1_branch_category_5; '','''')
                     || DECODE (e1_customer_category_2        , NULL,''e1_customer_category_2; '','''')
                     || DECODE (ebs_tp_zone_code              , NULL,''ebs_tp_zone_code; '','''')
                     || DECODE (e1_branch_city                , NULL,''e1_branch_city; '','''')
                     || DECODE (ebs_zone_code                 , NULL,''ebs_zone_code; '','''')
                     || DECODE (ebs_account_code              , NULL,''ebs_account_code; '','''')
                     || DECODE (t_ep_lr1                      , NULL,''t_ep_lr1; '','''')
                     || DECODE (t_ep_ls2                      , NULL,''t_ep_ls2; '','''')
                     || DECODE (t_ep_ls4                      , NULL,''t_ep_ls4; '','''')
                     || DECODE (t_ep_l_att_6                  , NULL,''t_ep_l_att_6; '','''')
                     || DECODE (t_ep_l_att_9                  , NULL,''t_ep_l_att_9; '','''')
                     || DECODE (e1_customer_category_7        , NULL,''e1_customer_category_7; '','''')
                     || DECODE (ebs_customer_class_code       , NULL,''ebs_customer_class_code; '','''')
                     || DECODE (ebs_sales_channel_code        , NULL,''ebs_sales_channel_code; '','''')
                     || DECODE (e1_branch_state               , NULL,''e1_branch_state; '','''')
                     || DECODE (e1_customer_category_1        , NULL,''e1_customer_category_1; '','''')
                     || DECODE (t_ep_lr2a                     , NULL,''t_ep_lr2a; '','''')
                     || DECODE (t_ep_ls1                      , NULL,''t_ep_ls1; '','''')
                     || DECODE (t_ep_l_att_1                  , NULL,''t_ep_l_att_1; '','''')
                     || DECODE (dm_org_code                   , NULL,''dm_org_code; '','''')
                     || DECODE (e1_branch_category_4          , NULL,''e1_branch_category_4; '','''')
                     || DECODE (e1_customer_country           , NULL,''e1_customer_country; '','''')
                     || DECODE (ebs_business_group_code       , NULL,''ebs_business_group_code; '','''')
                     || DECODE (ebs_legal_entity_code         , NULL,''ebs_legal_entity_code; '','''')
                     || DECODE (t_ep_l_att_5                  , NULL,''t_ep_l_att_5; '','''')
                     || DECODE (t_ep_l_att_10                 , NULL,''t_ep_l_att_10; '','''')
                     || DECODE (dm_site_code                  , NULL,''dm_site_code; '','''')
                     || DECODE (e1_branch_country             , NULL,''e1_branch_country; '','''')
                     || DECODE (e1_branch_category_3          , NULL,''e1_branch_category_3; '','''')
                     || DECODE (ebs_customer_code             , NULL,''ebs_customer_code; '','''')
                     || DECODE (t_ep_l_att_2                  , NULL,''t_ep_l_att_2; '','''')
                     || DECODE (t_ep_l_att_3                  , NULL,''t_ep_l_att_3; '','''')
                     || DECODE (t_ep_l_att_7                  , NULL,''t_ep_l_att_7; '','''')
                     || DECODE (e1_branch_category_1          , NULL,''e1_branch_category_1; '','''')
                     || DECODE (e1_customer_state             , NULL,''e1_customer_state; '','''')
                     || DECODE (e1_customer_category_3        , NULL,''e1_customer_category_3; '','''')
                     || DECODE (e1_customer_category_5        , NULL,''e1_customer_category_5; '','''')
                     || DECODE (e1_customer_category_6        , NULL,''e1_customer_category_6; '','''')
                     || DECODE (e1_customer_city              , NULL,''e1_customer_city; '','''')
                     || DECODE (t_ep_ls5                      , NULL,''t_ep_ls5; '','''')
                     || DECODE (t_ep_ls6                      , NULL,''t_ep_ls6; '','''')
                     || DECODE (t_ep_l_att_8                  , NULL,''t_ep_l_att_8; '','''')
                     || DECODE (e1_branch_category_2          , NULL,''e1_branch_category_2; '','''')
                     || DECODE (e1_customer_category_4        , NULL,''e1_customer_category_4; '','''')
                     || DECODE (ebs_operation_unit_code       , NULL,''ebs_operation_unit_code; '','''')
              FROM   T_SRC_LOC_TMPL
              WHERE  1 = 2
              OR     T_SRC_LOC_TMPL.t_ep_lr2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls3 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_4 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_5 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_2 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_tp_zone_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_city IS NULL
              OR     T_SRC_LOC_TMPL.ebs_zone_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_account_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_lr1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls4 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_6 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_9 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_7 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_customer_class_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_sales_channel_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_state IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_lr2a IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_1 IS NULL
              OR     T_SRC_LOC_TMPL.dm_org_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_4 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_country IS NULL
              OR     T_SRC_LOC_TMPL.ebs_business_group_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_legal_entity_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_5 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_10 IS NULL
              OR     T_SRC_LOC_TMPL.dm_site_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_country IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_3 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_customer_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_3 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_7 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_1 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_state IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_3 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_5 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_6 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_city IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls5 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls6 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_8 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_2 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_4 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_operation_unit_code IS NULL';
 
   vi_data_err_chk_01 := 0;
 
   BEGIN
      EXECUTE IMMEDIATE vs_sql;
 
      vi_data_err_chk_01 := SQL%ROWCOUNT;
      vi_data_errors_tot := vi_data_errors_tot + vi_data_err_chk_01;
 
      EXCEPTION
         WHEN OTHERS THEN
 
         vi_error_code := SQLCODE;
         vs_error_text := SUBSTR(SQLERRM,1,4000);
 
         vs_log_msg    := vs_error_text;                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
         vs_log_msg    := vs_sql;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
         vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';            DBEX ( vs_msg , vs_proc_source, 'E');
         vs_msg        := vs_log_msg;                                                                                  DBEX ( vs_msg , vs_proc_source, 'E');
   END;
 
 
   vs_log_msg  := 'Check  01 : Code IS NULL errors found : '|| vi_data_err_chk_01;                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL                          DELETE from SRC_ table
   --------------------------------------------------------------------------
   IF vi_data_err_chk_01 > 0 THEN
 
      vs_log_msg := 'Check 01 : Code IS NULL       DELETE from SRC_ table';                                          LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      vs_sql := 'DELETE T_SRC_LOC_TMPL
                 WHERE  1 = 2
              OR     T_SRC_LOC_TMPL.t_ep_lr2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls3 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_4 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_5 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_2 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_tp_zone_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_city IS NULL
              OR     T_SRC_LOC_TMPL.ebs_zone_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_account_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_lr1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls4 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_6 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_9 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_7 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_customer_class_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_sales_channel_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_state IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_lr2a IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls1 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_1 IS NULL
              OR     T_SRC_LOC_TMPL.dm_org_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_4 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_country IS NULL
              OR     T_SRC_LOC_TMPL.ebs_business_group_code IS NULL
              OR     T_SRC_LOC_TMPL.ebs_legal_entity_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_5 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_10 IS NULL
              OR     T_SRC_LOC_TMPL.dm_site_code IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_country IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_3 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_customer_code IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_2 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_3 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_7 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_1 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_state IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_3 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_5 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_6 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_city IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls5 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_ls6 IS NULL
              OR     T_SRC_LOC_TMPL.t_ep_l_att_8 IS NULL
              OR     T_SRC_LOC_TMPL.e1_branch_category_2 IS NULL
              OR     T_SRC_LOC_TMPL.e1_customer_category_4 IS NULL
              OR     T_SRC_LOC_TMPL.ebs_operation_unit_code IS NULL';
 
      BEGIN
 
         EXECUTE IMMEDIATE vs_sql;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SUBSTR(SQLERRM,1,4000);
 
            vs_log_msg    := vs_error_text;                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_log_msg    := vs_sql;                                                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';         DBEX ( vs_msg , vs_proc_source, 'E');
            vs_msg        := vs_log_msg;                                                                               DBEX ( vs_msg , vs_proc_source, 'E');

      END;

   END IF;

   COMMIT;

 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Check 02 : Duplicate description value
   -- Check 03 : Duplicate relation value
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   vi_data_err_count  := 0;
   vi_data_err_chk_02 := 0;
   vi_data_err_chk_03 := 0;
 
   FOR rec IN (SELECT field_name, ems.table_name, e_plan_node_id
               FROM   EP_MODEL_SYNTAX ems,
                      E_PLAN_TREE     ept
               WHERE  LOWER(LTRIM(RTRIM( ems.table_name ))) = LOWER(LTRIM(RTRIM( ept.e_plan_table_name )))
               AND    model_version = 15
               AND    dim_type      = 2)
   LOOP
 
      vi_data_err_count := 0;
 
      SELECT DISTINCT field_name
      INTO   dvs_field_name
      FROM   E_PLAN_TREE
      WHERE  model_version      = 15
      AND    LOWER(e_plan_type) = LOWER('DESC')
      AND    e_plan_child_node  = rec.e_plan_node_id;
 
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
 
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value             INSERT to _ERR table
      --------------------------------------------------------------------------
      vs_log_msg := 'Check 02 : Duplicate description value : '|| dvs_field_name ||'   INSERT to _ERR table';      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      dvs_err_msg:='Duplicate description value - '||dvs_field_name;
 
      vs_sql := 'INSERT INTO t_src_loc_tmpl_err
                 SELECT T_SRC_LOC_TMPL.*,2 error_code_record,'''|| dvd_sysdate ||''','''|| dvs_err_msg ||''' error_message_record
                 FROM   T_SRC_LOC_TMPL
                 WHERE UPPER('||rec.field_name||') IN ( SELECT  UPPER('||rec.field_name||')
                                                          FROM T_SRC_LOC_TMPL
                                                          WHERE '|| dvs_field_name ||' IS NOT NULL
                                                          GROUP BY  '|| rec.field_name ||'
                                                          HAVING  COUNT ( DISTINCT '|| dvs_field_name ||') > 1 )';
 
      BEGIN
         EXECUTE IMMEDIATE vs_sql;
 
         vi_data_err_count  := SQL%ROWCOUNT;
         vi_data_err_chk_02 := vi_data_err_chk_02 + vi_data_err_count;
         vi_data_errors_tot := vi_data_errors_tot + vi_data_err_count;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SUBSTR(SQLERRM,1,4000);
 
            vs_log_msg    := vs_error_text;                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';         DBEX ( vs_msg , vs_proc_source, 'E');
            vs_msg        := vs_log_msg;                                                                               DBEX ( vs_msg , vs_proc_source, 'E');
 
      END;
 
      vs_log_msg  := 'Check 02 : Duplicate description value -  Errors found : '|| vi_data_err_count;                LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value           DELETE from SRC_ table
      --------------------------------------------------------------------------
      IF vi_data_err_count > 0 THEN
 
         vs_log_msg := 'Check  2 : Duplicate description value : '|| dvs_field_name ||'   DELETE from SRC_ table'; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         vs_sql := 'DELETE T_SRC_LOC_TMPL
                     WHERE UPPER('|| rec.field_name ||') IN ( SELECT UPPER('|| rec.field_name ||')
                                                                FROM  T_SRC_LOC_TMPL
                                                                WHERE '|| dvs_field_name ||' IS NOT NULL
                                                                GROUP BY  '|| rec.field_name ||'
                                                                HAVING  COUNT ( DISTINCT '|| dvs_field_name||') > 1 )';
         BEGIN
 
            EXECUTE IMMEDIATE vs_sql;
 
         EXCEPTION
            WHEN OTHERS THEN
 
               vi_error_code := SQLCODE;
               vs_error_text := SUBSTR(SQLERRM,1,4000);
 
               vs_log_msg    := vs_error_text;                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';      DBEX ( vs_msg , vs_proc_source, 'E');
               vs_msg        := vs_log_msg;                                                                            DBEX ( vs_msg , vs_proc_source, 'E');
 
         END;
 
      END IF;
 
      COMMIT;
 
 
 
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      -- Check 03 : Duplicate relation value
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      vi_data_err_count := 0;
      dvs_err_msg       := 'Duplicate relation value - '||rec.field_name;
 
      FOR rec2 IN ( SELECT field_name
                    FROM   E_PLAN_TREE
                    WHERE  model_version = 15
                    AND    LOWER( e_plan_type     ) = LOWER('code')
                    AND    LOWER( e_plan_base_dim ) = 'location'
                    AND    e_plan_child_node IN ( SELECT E_PLAN_NODE_ID
                                                  FROM   E_PLAN_TREE
                                                  WHERE  model_version       = 15
                                                  AND    UPPER( field_name ) = UPPER( rec.field_name )))
      LOOP
         dvs_sqltmp := dvs_sqltmp ||' COUNT(DISTINCT '||rec2.field_name||' ) > 1 OR ';
      END LOOP;
 
      IF dvs_sqltmp IS NOT NULL THEN
 
         dvs_sqltmp := RTRIM( dvs_sqltmp, 'OR ');
 
         --------------------------------------------------------------------------
         -- Check 03 : Duplicate relation value                INSERT to _ERR table
         --------------------------------------------------------------------------
         vs_log_msg := 'Check 03 : Duplicate relation value : '|| rec.field_name ||'   INSERT to _ERR table';      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         vs_sql := 'INSERT INTO t_src_loc_tmpl_err
             SELECT T_SRC_LOC_TMPL.*,3 error_code_record,'''|| dvd_sysdate ||''','''|| dvs_err_msg ||''' error_message_record
             FROM   T_SRC_LOC_TMPL
             WHERE  UPPER('|| rec.field_name ||') <> ''N/A''
             AND    UPPER('|| rec.field_name ||') IN ( SELECT UPPER('|| rec.field_name ||')
                                                       FROM   T_SRC_LOC_TMPL
                                                       GROUP BY '|| rec.field_name ||'
                                                       HAVING   '|| dvs_sqltmp ||')';
         BEGIN
            EXECUTE IMMEDIATE vs_sql;
 
            vi_data_err_count  := SQL%ROWCOUNT;
            vi_data_err_chk_03 := vi_data_err_chk_03 + vi_data_err_count;
            vi_data_errors_tot := vi_data_errors_tot + vi_data_err_count;
 
         EXCEPTION
            WHEN OTHERS THEN
 
               vi_error_code := SQLCODE;
               vs_error_text := SUBSTR(SQLERRM,1,4000);
 
               vs_log_msg    := vs_error_text;                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_log_msg    := vs_sql;                                                                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';      DBEX ( vs_msg , vs_proc_source, 'E');
               vs_msg        := vs_log_msg;                                                                            DBEX ( vs_msg , vs_proc_source, 'E');
 
         END;
 
         vs_log_msg  := 'Check 03 : Duplicate relation value -  Errors found : '|| vi_data_err_count;                LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
         --------------------------------------------------------------------------
         -- Check  3 : Duplicate relation value              DELETE from SRC_ table
         --------------------------------------------------------------------------
         IF vi_data_err_count > 0 THEN
 
            vs_log_msg := 'Check  3 : Duplicate relation value : '|| rec.field_name ||'   DELETE from SRC_ table'; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
            vs_sql := 'DELETE T_SRC_LOC_TMPL
                        WHERE  UPPER('|| rec.field_name ||') <> ''N/A''
                        AND    UPPER('|| rec.field_name ||')  IN ( SELECT UPPER('||rec.field_name||')
                                                                     FROM   T_SRC_LOC_TMPL
                                                                     GROUP BY '|| rec.field_name ||'
                                                                     HAVING   '|| dvs_sqltmp ||')';
 
            BEGIN
 
               EXECUTE IMMEDIATE vs_sql;
 
            EXCEPTION
               WHEN OTHERS THEN
 
                  vi_error_code := SQLCODE;
                  vs_error_text := SUBSTR(SQLERRM,1,4000);
 
                  vs_log_msg    := vs_error_text;                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
                  vs_log_msg    := vs_sql;                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
                  vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';   DBEX ( vs_msg , vs_proc_source, 'E');
                  vs_msg        := vs_log_msg;                                                                         DBEX ( vs_msg , vs_proc_source, 'E');
 
            END;
 
         END IF;
 
         COMMIT;
 
      END IF;
 
      dvs_sqltmp:='';
 
   END LOOP;
 
   vs_log_msg  := 'Check 02 : Duplicate description value -  Errors found : '|| vi_data_err_chk_02;                  LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
   vs_log_msg  := 'Check 03 : Duplicate relation value    -  Errors found : '|| vi_data_err_chk_03;                  LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);

 
 
 
   --------------------------------------------------------------------------
   -- Rebuild the location sequence.
   --------------------------------------------------------------------------
 
   --------------------------------------------------------------------------
   -- DROP SEQUENCE
   --------------------------------------------------------------------------
   IF get_is_sequence_exists('location_seq') = 1 THEN
 
      vs_sql     := 'DROP SEQUENCE location_seq';
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      dynamic_ddl( vs_sql );
 
   END IF;
 
   dv_max_id := dynamic_number('SELECT MAX(NVL(location_id,0))  FROM location');
   dv_max_id := dv_max_id + 1;
   dv_max_id := NVL(dv_max_id,1);
 
   --------------------------------------------------------------------------
   -- CREATE SEQUENCE
   --------------------------------------------------------------------------
   vs_sql     := 'CREATE SEQUENCE location_seq START WITH '|| dv_max_id;
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl( vs_sql );
 
   --------------------------------------------------------------------------
   -- ALTER PROCEDURE    COMPILE
   --------------------------------------------------------------------------
   vs_sql     := 'ALTER PROCEDURE EP_LOAD_LOCATIONS COMPILE';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl( vs_sql );
 
 
 
   vs_log_msg := 'End of procedure';                                                                                 LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_end_time  := DBMS_UTILITY.get_time;
   vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
   vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - ';
      vs_msg := vs_msg ||'Data validation errors :';
      vs_msg := vs_msg ||'Total  ['|| vi_data_errors_tot ||'] ';
      vs_msg := vs_msg ||'Chk 01 ['|| vi_data_err_chk_01 ||'] ';
      vs_msg := vs_msg ||'Chk 02 ['|| vi_data_err_chk_02 ||'] ';
      vs_msg := vs_msg ||'Chk 03 ['|| vi_data_err_chk_03 ||'] ';
      vs_msg := vs_msg ||'Chk 12 ['|| vi_data_err_chk_12 ||'] ';                                                   DBEX( vs_msg, vs_proc, 'T' );

   set_module ('E');

 
EXCEPTION
   WHEN OTHERS THEN
 
      send_err_message (dvs_proc_name,'Failed TO complete data checking - '||dvs_sqlstr);
 
      vs_log_msg :=  'Completed with errors';                                                                        LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_TIMING_LOG
      --------------------------------------------------------------------------
      vd_end_time  := DBMS_UTILITY.get_time;
      vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
      vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds with errors - ';
      vs_msg := vs_msg ||'Data validation errors :';
      vs_msg := vs_msg ||'Total  ['|| vi_data_errors_tot ||'] ';
      vs_msg := vs_msg ||'Chk 01 ['|| vi_data_err_chk_01 ||'] ';
      vs_msg := vs_msg ||'Chk 02 ['|| vi_data_err_chk_02 ||'] ';
      vs_msg := vs_msg ||'Chk 03 ['|| vi_data_err_chk_03 ||'] ';
      vs_msg := vs_msg ||'Chk 12 ['|| vi_data_err_chk_12 ||'] ';                                                   DBEX( vs_msg, vs_proc, 'T' );


END  EP_CHECK_LOCATION;

/
