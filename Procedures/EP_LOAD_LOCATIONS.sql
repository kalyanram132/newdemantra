--------------------------------------------------------
--  DDL for Procedure EP_LOAD_LOCATIONS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."EP_LOAD_LOCATIONS" -- $Revision: 1.200.14.3.2.2 $
  
    (ii_dummy INTEGER DEFAULT NULL)
IS
   vs_sql                            VARCHAR2(32000);
   vi_exists                         INTEGER;
   dv_is_exists                      INTEGER;
   sqlstr                            VARCHAR2(32000);
   vs_proc                           VARCHAR2(30) := 'EP_LOAD_LOCATIONS';
   vs_msg                            VARCHAR2(2000);
   vi_seq                            INTEGER;
 
   vi_error_code                     NUMBER;
   vs_error_text                     VARCHAR2(4000);
   vs_status                         VARCHAR2(10) := 'SUCCESS';
 
   dv_rowcounter                     INTEGER;
   dv_sys_rowcounter                 INTEGER;
   dv_max_id                         INTEGER;
   dv_pval_max_recs                  SYS_PARAMS.pval%TYPE;
   vs_pval                           VARCHAR2(100);
   vd_load_date                      DATE;
   vd_update_date                    DATE;
   vs_dim_merge_count                INTEGER;
   vs_mdp_update_count               INTEGER;
   vs_dim_level_update_count         INTEGER;
   vi_stagging_count                 INTEGER;

   vi_level_id                       INTEGER;
   vs_level_id                       VARCHAR2(20);

 
 
   dv_location                       INTEGER;
   dv_t_ep_L_ATT_1                   INTEGER;
   dv_t_ep_L_ATT_10                  INTEGER;
   dv_t_ep_L_ATT_2                   INTEGER;
   dv_t_ep_L_ATT_3                   INTEGER;
   dv_t_ep_L_ATT_4                   INTEGER;
   dv_t_ep_L_ATT_5                   INTEGER;
   dv_t_ep_L_ATT_6                   INTEGER;
   dv_t_ep_L_ATT_7                   INTEGER;
   dv_t_ep_L_ATT_8                   INTEGER;
   dv_t_ep_L_ATT_9                   INTEGER;
   dv_t_ep_e1_br_cat_1               INTEGER;
   dv_t_ep_e1_br_cat_2               INTEGER;
   dv_t_ep_e1_br_cat_3               INTEGER;
   dv_t_ep_e1_br_cat_4               INTEGER;
   dv_t_ep_e1_br_cat_5               INTEGER;
   dv_t_ep_e1_br_city                INTEGER;
   dv_t_ep_e1_br_country             INTEGER;
   dv_t_ep_e1_br_state               INTEGER;
   dv_t_ep_e1_cust_cat_1             INTEGER;
   dv_t_ep_e1_cust_cat_2             INTEGER;
   dv_t_ep_e1_cust_cat_3             INTEGER;
   dv_t_ep_e1_cust_cat_4             INTEGER;
   dv_t_ep_e1_cust_cat_5             INTEGER;
   dv_t_ep_e1_cust_cat_6             INTEGER;
   dv_t_ep_e1_cust_cat_7             INTEGER;
   dv_t_ep_e1_cust_city              INTEGER;
   dv_t_ep_e1_cust_ctry              INTEGER;
   dv_t_ep_e1_cust_state             INTEGER;
   dv_t_ep_ebs_account               INTEGER;
   dv_t_ep_ebs_bus_group             INTEGER;
   dv_t_ep_ebs_cust_class            INTEGER;
   dv_t_ep_ebs_customer              INTEGER;
   dv_t_ep_ebs_legal_entity          INTEGER;
   dv_t_ep_ebs_oper_unit             INTEGER;
   dv_t_ep_ebs_sales_ch              INTEGER;
   dv_t_ep_ebs_tp_zone               INTEGER;
   dv_t_ep_ebs_zone                  INTEGER;
   dv_t_ep_lr1                       INTEGER;
   dv_t_ep_lr2                       INTEGER;
   dv_t_ep_lr2a                      INTEGER;
   dv_t_ep_ls1                       INTEGER;
   dv_t_ep_ls2                       INTEGER;
   dv_t_ep_ls3                       INTEGER;
   dv_t_ep_ls4                       INTEGER;
   dv_t_ep_ls5                       INTEGER;
   dv_t_ep_ls6                       INTEGER;
   dv_t_ep_organization              INTEGER;
   dv_t_ep_site                      INTEGER;
 
   TYPE rec_load_cursor              IS REF CURSOR;
   rec_load                          rec_load_cursor;
 
   vs_t_ep_lr1_570                   VARCHAR2(500);
   vs_t_ep_lr2_573                   VARCHAR2(500);
   vs_t_ep_lr2a_576                  VARCHAR2(500);
   vs_t_ep_ls1_581                   VARCHAR2(500);
   vs_t_ep_ls2_584                   VARCHAR2(500);
   vs_t_ep_ls3_587                   VARCHAR2(500);
   vs_t_ep_ls4_590                   VARCHAR2(500);
   vs_t_ep_ls5_617                   VARCHAR2(500);
   vs_t_ep_ls6_620                   VARCHAR2(500);
   vs_t_ep_l_att_1_762               VARCHAR2(500);
   vs_t_ep_l_att_2_765               VARCHAR2(500);
   vs_t_ep_l_att_3_768               VARCHAR2(500);
   vs_t_ep_l_att_4_772               VARCHAR2(500);
   vs_t_ep_l_att_5_775               VARCHAR2(500);
   vs_t_ep_l_att_6_778               VARCHAR2(500);
   vs_t_ep_l_att_7_781               VARCHAR2(500);
   vs_t_ep_l_att_8_784               VARCHAR2(500);
   vs_t_ep_l_att_9_787               VARCHAR2(500);
   vs_t_ep_l_att_10_790              VARCHAR2(500);
   vs_dm_org_code_838                VARCHAR2(500);
   vs_dm_site_code_841               VARCHAR2(500);
   vs_e1_branch_city_891             VARCHAR2(500);
   vs_e1_branch_state_894            VARCHAR2(500);
   vs_e1_branch_country_897          VARCHAR2(500);
   vs_e1_branch_category_1_901       VARCHAR2(500);
   vs_e1_branch_category_2_904       VARCHAR2(500);
   vs_e1_branch_category_3_907       VARCHAR2(500);
   vs_e1_branch_category_4_910       VARCHAR2(500);
   vs_e1_branch_category_5_913       VARCHAR2(500);
   vs_e1_customer_state_991          VARCHAR2(500);
   vs_e1_customer_country_994        VARCHAR2(500);
   vs_e1_customer_category_1174      VARCHAR2(500);
   vs_e1_customer_category_1191      VARCHAR2(500);
   vs_e1_customer_category_1194      VARCHAR2(500);
   vs_e1_customer_category_1197      VARCHAR2(500);
   vs_e1_customer_category_1200      VARCHAR2(500);
   vs_e1_customer_category_1203      VARCHAR2(500);
   vs_e1_customer_category_1206      VARCHAR2(500);
   vs_ebs_tp_zone_code_1251          VARCHAR2(500);
   vs_ebs_zone_code_1254             VARCHAR2(500);
   vs_ebs_account_code_1257          VARCHAR2(500);
   vs_ebs_customer_code_1260         VARCHAR2(500);
   vs_ebs_customer_class_c_1263      VARCHAR2(500);
   vs_ebs_operation_unit_c_1271      VARCHAR2(500);
   vs_ebs_business_group_c_1274      VARCHAR2(500);
   vs_ebs_legal_entity_cod_1277      VARCHAR2(500);
   vs_ebs_sales_channel_co_1280      VARCHAR2(500);
   vs_e1_customer_city_1311          VARCHAR2(500);
 
   vs_p_t_ep_lr1_570                  VARCHAR2(500) := NULL;
   vs_p_t_ep_lr2_573                  VARCHAR2(500) := NULL;
   vs_p_t_ep_lr2a_576                 VARCHAR2(500) := NULL;
   vs_p_t_ep_ls1_581                  VARCHAR2(500) := NULL;
   vs_p_t_ep_ls2_584                  VARCHAR2(500) := NULL;
   vs_p_t_ep_ls3_587                  VARCHAR2(500) := NULL;
   vs_p_t_ep_ls4_590                  VARCHAR2(500) := NULL;
   vs_p_t_ep_ls5_617                  VARCHAR2(500) := NULL;
   vs_p_t_ep_ls6_620                  VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_1_762              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_2_765              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_3_768              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_4_772              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_5_775              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_6_778              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_7_781              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_8_784              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_9_787              VARCHAR2(500) := NULL;
   vs_p_t_ep_l_att_10_790             VARCHAR2(500) := NULL;
   vs_p_dm_org_code_838               VARCHAR2(500) := NULL;
   vs_p_dm_site_code_841              VARCHAR2(500) := NULL;
   vs_p_e1_branch_city_891            VARCHAR2(500) := NULL;
   vs_p_e1_branch_state_894           VARCHAR2(500) := NULL;
   vs_p_e1_branch_country_897         VARCHAR2(500) := NULL;
   vs_p_e1_branch_category_1_901      VARCHAR2(500) := NULL;
   vs_p_e1_branch_category_2_904      VARCHAR2(500) := NULL;
   vs_p_e1_branch_category_3_907      VARCHAR2(500) := NULL;
   vs_p_e1_branch_category_4_910      VARCHAR2(500) := NULL;
   vs_p_e1_branch_category_5_913      VARCHAR2(500) := NULL;
   vs_p_e1_customer_state_991         VARCHAR2(500) := NULL;
   vs_p_e1_customer_country_994       VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1174     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1191     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1194     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1197     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1200     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1203     VARCHAR2(500) := NULL;
   vs_p_e1_customer_category_1206     VARCHAR2(500) := NULL;
   vs_p_ebs_tp_zone_code_1251         VARCHAR2(500) := NULL;
   vs_p_ebs_zone_code_1254            VARCHAR2(500) := NULL;
   vs_p_ebs_account_code_1257         VARCHAR2(500) := NULL;
   vs_p_ebs_customer_code_1260        VARCHAR2(500) := NULL;
   vs_p_ebs_customer_class_c_1263     VARCHAR2(500) := NULL;
   vs_p_ebs_operation_unit_c_1271     VARCHAR2(500) := NULL;
   vs_p_ebs_business_group_c_1274     VARCHAR2(500) := NULL;
   vs_p_ebs_legal_entity_cod_1277     VARCHAR2(500) := NULL;
   vs_p_ebs_sales_channel_co_1280     VARCHAR2(500) := NULL;
   vs_p_e1_customer_city_1311         VARCHAR2(500) := NULL;
 
   vi_t_ep_lr1_ep_id                 NUMBER;
   vi_t_ep_lr2_ep_id                 NUMBER;
   vi_t_ep_lr2a_ep_id                NUMBER;
   vi_t_ep_ls1_ep_id                 NUMBER;
   vi_t_ep_ls2_ep_id                 NUMBER;
   vi_t_ep_ls3_ep_id                 NUMBER;
   vi_t_ep_ls4_ep_id                 NUMBER;
   vi_t_ep_ls5_ep_id                 NUMBER;
   vi_t_ep_ls6_ep_id                 NUMBER;
   vi_t_ep_l_att_1_ep_id             NUMBER;
   vi_t_ep_l_att_2_ep_id             NUMBER;
   vi_t_ep_l_att_3_ep_id             NUMBER;
   vi_t_ep_l_att_4_ep_id             NUMBER;
   vi_t_ep_l_att_5_ep_id             NUMBER;
   vi_t_ep_l_att_6_ep_id             NUMBER;
   vi_t_ep_l_att_7_ep_id             NUMBER;
   vi_t_ep_l_att_8_ep_id             NUMBER;
   vi_t_ep_l_att_9_ep_id             NUMBER;
   vi_t_ep_l_att_10_ep_id            NUMBER;
   vi_t_ep_organization_ep_id        NUMBER;
   vi_t_ep_site_ep_id                NUMBER;
   vi_t_ep_e1_br_city_ep_id          NUMBER;
   vi_t_ep_e1_br_state_ep_id         NUMBER;
   vi_t_ep_e1_br_country_ep_id       NUMBER;
   vi_t_ep_e1_br_cat_1_ep_id         NUMBER;
   vi_t_ep_e1_br_cat_2_ep_id         NUMBER;
   vi_t_ep_e1_br_cat_3_ep_id         NUMBER;
   vi_t_ep_e1_br_cat_4_ep_id         NUMBER;
   vi_t_ep_e1_br_cat_5_ep_id         NUMBER;
   vi_t_ep_e1_cust_state_ep_id       NUMBER;
   vi_t_ep_e1_cust_ctry_ep_id        NUMBER;
   vi_t_ep_e1_cust_cat_1_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_2_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_3_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_4_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_5_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_6_ep_id       NUMBER;
   vi_t_ep_e1_cust_cat_7_ep_id       NUMBER;
   vi_t_ep_ebs_tp_zone_ep_id         NUMBER;
   vi_t_ep_ebs_zone_ep_id            NUMBER;
   vi_t_ep_ebs_account_ep_id         NUMBER;
   vi_t_ep_ebs_customer_ep_id        NUMBER;
   vi_t_ep_ebs_cust_class_ep_id      NUMBER;
   vi_t_ep_ebs_oper_unit_ep_id       NUMBER;
   vi_t_ep_ebs_bus_group_ep_id       NUMBER;
   vi_t_ep_ebs_legal_entity_ep_id    NUMBER;
   vi_t_ep_ebs_sales_ch_ep_id        NUMBER;
   vi_t_ep_e1_cust_city_ep_id        NUMBER;
 
   vs_dm_org_desc_839                VARCHAR2(2000);
   vs_dm_site_desc_842               VARCHAR2(2000);
   vs_e1_br_cat_desc_1_902           VARCHAR2(2000);
   vs_e1_br_cat_desc_2_905           VARCHAR2(2000);
   vs_e1_br_cat_desc_3_908           VARCHAR2(2000);
   vs_e1_br_cat_desc_4_911           VARCHAR2(2000);
   vs_e1_br_cat_desc_5_914           VARCHAR2(2000);
   vs_e1_br_city_desc_892            VARCHAR2(2000);
   vs_e1_br_country_desc_898         VARCHAR2(2000);
   vs_e1_br_state_desc_895           VARCHAR2(2000);
   vs_e1_cust_cat_1_desc_1175        VARCHAR2(2000);
   vs_e1_cust_cat_2_desc_1192        VARCHAR2(2000);
   vs_e1_cust_cat_3_desc_1195        VARCHAR2(2000);
   vs_e1_cust_cat_4_desc_1198        VARCHAR2(2000);
   vs_e1_cust_cat_5_desc_1201        VARCHAR2(2000);
   vs_e1_cust_cat_6_desc_1204        VARCHAR2(2000);
   vs_e1_cust_cat_7_desc_1207        VARCHAR2(2000);
   vs_e1_cust_city_desc_1312         VARCHAR2(2000);
   vs_e1_cust_ctry_desc_995          VARCHAR2(2000);
   vs_e1_cust_state_desc_992         VARCHAR2(2000);
   vs_ebs_account_desc_1258          VARCHAR2(2000);
   vs_ebs_bus_group_desc_1275        VARCHAR2(2000);
   vs_ebs_cust_class_desc_1264       VARCHAR2(2000);
   vs_ebs_customer_desc_1261         VARCHAR2(2000);
   vs_ebs_legal_ent_desc_1278        VARCHAR2(2000);
   vs_ebs_oper_unit_desc_1272        VARCHAR2(2000);
   vs_ebs_sales_ch_desc_1281         VARCHAR2(2000);
   vs_ebs_tp_zone_desc_1252          VARCHAR2(2000);
   vs_ebs_zone_desc_1255             VARCHAR2(2000);
   vs_l_att_1_desc_763               VARCHAR2(2000);
   vs_l_att_10_desc_791              VARCHAR2(2000);
   vs_l_att_2_desc_766               VARCHAR2(2000);
   vs_l_att_3_desc_771               VARCHAR2(2000);
   vs_l_att_4_desc_773               VARCHAR2(2000);
   vs_l_att_5_desc_776               VARCHAR2(2000);
   vs_l_att_6_desc_779               VARCHAR2(2000);
   vs_l_att_7_desc_782               VARCHAR2(2000);
   vs_l_att_8_desc_785               VARCHAR2(2000);
   vs_l_att_9_desc_788               VARCHAR2(2000);
   vs_lr1_desc_571                   VARCHAR2(2000);
   vs_lr2_desc_574                   VARCHAR2(2000);
   vs_lr2a_desc_577                  VARCHAR2(2000);
   vs_ls1_desc_582                   VARCHAR2(2000);
   vs_ls2_desc_585                   VARCHAR2(2000);
   vs_ls3_desc_588                   VARCHAR2(2000);
   vs_ls4_desc_591                   VARCHAR2(2000);
   vs_ls5_desc_618                   VARCHAR2(2000);
   vs_ls6_desc_621                   VARCHAR2(2000);

   vs_description                    VARCHAR2(32000);

 
   vi_logging_level                  INTEGER;
   vs_log_table                      VARCHAR2(30);
   vs_proc_source                    VARCHAR2(60);
   vi_log_it_commit                  INTEGER;
   vs_log_msg                        VARCHAR2(2000);
   vs_temp_proc_name                 VARCHAR2(30);
 
   vd_start_time_proc                NUMBER;
   vd_start_time                     NUMBER;
   vd_end_time                       NUMBER;
 
   vt_procedure                      NUMBER := 0;
   vt_duration                       NUMBER := 0;
   vs_duration                       VARCHAR2(10)   := '';
 
   vs_section                        VARCHAR2(3000) := '';
   vi_sec_log_id                     INTEGER := 0;
   vs_sec_log_id                     VARCHAR2(10);
 
BEGIN
 
   pre_logon;
 
   set_module ( 'S' );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_start_time_proc := DBMS_UTILITY.get_time;
 
   --------------------------------------------------------------------------
   -- Record the Parameters passed to DB_CALL_LOG
   --------------------------------------------------------------------------
   vs_msg := 'ii_dummy => '|| NVL( TO_CHAR( ii_dummy ) ,'NULL');
 
   DBEX(vs_msg, vs_proc, 'C');
 
   --------------------------------------------------------------------------
   -- Initialize LOG_IT
   --------------------------------------------------------------------------
 
   vs_proc_source   := 'EP_LOAD_LOCATIONS';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level, vi_log_it_commit);
 
   vs_log_msg := 'Logging Level ('|| vi_logging_level ||')';                                                       LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'SP', vs_log_msg, 'T' , 'C', NULL);

   vd_update_date := SYSTIMESTAMP;

   vs_log_msg := 'vd_update_date : '|| vd_update_date;                                                               LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg , NULL, NULL);

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Rebuild all the dimension level sequences
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Rebuild all the dimension level sequences';                                                        LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- REBUILD ALL THE DIMENSION LEVEL SEQUENCES
   --------------------------------------------------------------------------
   vs_log_msg := 'REBUILD ALL THE DIMENSION LEVEL SEQUENCES';                                                        LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   FOR rec IN (SELECT DISTINCT e_plan_table_name, sequence_name
               FROM   e_plan_tree, user_sequences
               WHERE  sequence_name = UPPER(e_plan_table_name)||'_SEQ'
               AND    model_version = 15)
   LOOP
      dynamic_ddl('DROP SEQUENCE '|| rec.sequence_name);
 
      dv_max_id := dynamic_number('SELECT MAX('|| rec.e_plan_table_name ||'_ep_id) + 1  FROM   '|| rec.e_plan_table_name);

      dynamic_ddl('CREATE SEQUENCE '|| rec.sequence_name ||' START WITH '|| dv_max_id);
   END LOOP;

   get_param('SYS_PARAMS','max_records_for_commit', dv_pval_max_recs, 10000);

   dv_sys_rowcounter := dv_pval_max_recs;


 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Load dimension level tables
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Load dimension level tables';                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- LOOP FOR ALL ITEMS LEVELS - CREATE AND RUN EP_LOAD_TMP
   --------------------------------------------------------------------------
   vs_log_msg := 'LOOP FOR ALL dim LEVELS - CREATE AND RUN EP_LOAD_TMP';                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   FOR rec IN (SELECT syntax, node_id, UPPER(table_name) table_name
               FROM   ep_model_syntax
               WHERE  dim_type = 2
               ORDER BY node_id)
   LOOP
 
      vs_sql := 'SELECT COUNT(*)
                  FROM   group_tables
                  WHERE  UPPER(gtable) = '''|| rec.table_name || '''';
 
      vi_exists := dynamic_number ( vs_sql );
 
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
      vi_level_id := NULL;
 
      IF vi_exists = 1 THEN
 
         vs_sql := 'SELECT group_table_id
                     FROM   group_tables
                     WHERE  UPPER(gtable) = '''|| rec.table_name || '''';
 
         vs_log_msg := vs_sql;                                                                                         LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
         vi_level_id := dynamic_number ( vs_sql );
      END IF;
 
      vs_level_id := NVL(TO_CHAR(vi_level_id) ,'NULL');
 
      vs_temp_proc_name := 'ep_load_tmp';
 
      IF vi_logging_level >= 4 THEN
         vs_temp_proc_name := 'EP_LD_' || SUBSTR(rec.table_name,1,24);
      END IF;
 
      sqlstr := 'CREATE OR REPLACE PROCEDURE '|| vs_temp_proc_name ||'
IS
   vs_sql                VARCHAR2(32000);
   vi_seq                INTEGER;
   vi_exists             INTEGER;
 
   vi_logging_level      INT;
   vs_log_table          VARCHAR2(30);
   vs_proc_source        VARCHAR2(60);
   vi_log_it_commit      INT;
   vs_log_msg            VARCHAR2(2000);
 
   vd_update_date        DATE;
 
   vi_level_id           INTEGER := '|| vs_level_id ||';
 
   vi_insert_count       INTEGER := 0;
   vi_update_count       INTEGER := 0;
 
   dv_is_exists          INTEGER;
   dv_rowcounter         INTEGER;
   vi_fictive_child      INTEGER;
   vs_father_level_id    INTEGER;
   vs_id_display_field   VARCHAR2(30);
   dv_sys_rowcounter     INTEGER := '|| dv_pval_max_recs ||';
 
   vs_content            VARCHAR2(4000);
   os_status             VARCHAR2(100);
   os_result             VARCHAR2(4000);
 
BEGIN
 
   pre_logon;
 
   set_module ( ''S'' );
 
   vs_proc_source   := ''EP_LOAD_LOCATIONS'';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level);
 
   vs_proc_source   := '''|| vs_temp_proc_name ||''';
 
   vs_log_msg := ''Start of procedure'';                                                                           LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,''SP'', vs_log_msg , NULL ,NULL);
 
   vd_update_date   := '''|| vd_update_date ||''';
 
   vi_level_id := NVL( vi_level_id , -54321 );
 
   '|| rec.syntax ||'
 
   vs_log_msg := ''End of procedure'';                                                                             LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,''EP'',vs_log_msg , NULL ,NULL);
 
   set_module (''E'');
 
 
END;';
      dynamic_ddl(sqlstr);
      dynamic_ddl('BEGIN '|| vs_temp_proc_name ||'; END;');
 
      --IF get_is_table_stats_exists   ( UPPER(rec.table_name) ) = 0 OR
      --   get_is_table_stats_rows_low ( UPPER(rec.table_name) ) = 1 THEN

         analyze_table( UPPER(rec.table_name) ,0);

      --END IF;

      IF vi_logging_level < 4 THEN
         check_and_drop( vs_temp_proc_name );
      END IF;

   END LOOP;

   COMMIT;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
   vs_log_msg := 'ALTER SESSION SET SKIP_UNUSABLE_INDEXES=TRUE';                                                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);

   execute immediate ('ALTER SESSION SET SKIP_UNUSABLE_INDEXES=TRUE');


 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Stagging table count
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Stagging table count';                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- Stagging table count
   --------------------------------------------------------------------------
   vs_sql := 'SELECT COUNT(*) FROM t_src_loc_tmpl';

   vi_stagging_count := dynamic_number ( vs_sql );
   vi_stagging_count := NVL(vi_stagging_count,0);

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MERGE INTO dimension table - Cursor Loop
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MERGE INTO dimension table - Cursor Loop';                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- LOOP - SELECT THE SRC DIMENSION TABLE ROWS
   --        Use 2 cursor loops instead of 1 to avoid large use of TEMP TS
   --------------------------------------------------------------------------
   vs_dim_merge_count := 0;
 
   vs_log_msg := 'LOOP - SELECT THE SRC DIMENSION TABLE ROWS';                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   vd_load_date := SYSTIMESTAMP;
 
   vs_log_msg := 'vd_load_date : '|| vd_load_date;                                                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg , NULL, NULL);
 
   vs_sql := 'SELECT DISTINCT
                     LOWER(t_ep_lr1),
                     LOWER(t_ep_lr2),
                     LOWER(t_ep_lr2a),
                     LOWER(t_ep_ls1),
                     LOWER(t_ep_ls2),
                     LOWER(t_ep_ls3),
                     LOWER(t_ep_ls4),
                     LOWER(t_ep_ls5),
                     LOWER(t_ep_ls6),
                     LOWER(t_ep_l_att_1),
                     LOWER(t_ep_l_att_2),
                     LOWER(t_ep_l_att_3),
                     LOWER(t_ep_l_att_4),
                     LOWER(t_ep_l_att_5),
                     LOWER(t_ep_l_att_6),
                     LOWER(t_ep_l_att_7),
                     LOWER(t_ep_l_att_8),
                     LOWER(t_ep_l_att_9),
                     LOWER(t_ep_l_att_10),
                     LOWER(dm_org_code),
                     LOWER(dm_site_code),
                     LOWER(e1_branch_city),
                     LOWER(e1_branch_state),
                     LOWER(e1_branch_country),
                     LOWER(e1_branch_category_1),
                     LOWER(e1_branch_category_2),
                     LOWER(e1_branch_category_3),
                     LOWER(e1_branch_category_4),
                     LOWER(e1_branch_category_5),
                     LOWER(e1_customer_state),
                     LOWER(e1_customer_country),
                     LOWER(e1_customer_category_1),
                     LOWER(e1_customer_category_2),
                     LOWER(e1_customer_category_3),
                     LOWER(e1_customer_category_4),
                     LOWER(e1_customer_category_5),
                     LOWER(e1_customer_category_6),
                     LOWER(e1_customer_category_7),
                     LOWER(ebs_tp_zone_code),
                     LOWER(ebs_zone_code),
                     LOWER(ebs_account_code),
                     LOWER(ebs_customer_code),
                     LOWER(ebs_customer_class_code),
                     LOWER(ebs_operation_unit_code),
                     LOWER(ebs_business_group_code),
                     LOWER(ebs_legal_entity_code),
                     LOWER(ebs_sales_channel_code),
                     LOWER(e1_customer_city)
              FROM   t_src_loc_tmpl';
 
   OPEN rec_load FOR
        vs_sql;
   LOOP
      FETCH rec_load INTO
            vs_t_ep_lr1_570,
            vs_t_ep_lr2_573,
            vs_t_ep_lr2a_576,
            vs_t_ep_ls1_581,
            vs_t_ep_ls2_584,
            vs_t_ep_ls3_587,
            vs_t_ep_ls4_590,
            vs_t_ep_ls5_617,
            vs_t_ep_ls6_620,
            vs_t_ep_l_att_1_762,
            vs_t_ep_l_att_2_765,
            vs_t_ep_l_att_3_768,
            vs_t_ep_l_att_4_772,
            vs_t_ep_l_att_5_775,
            vs_t_ep_l_att_6_778,
            vs_t_ep_l_att_7_781,
            vs_t_ep_l_att_8_784,
            vs_t_ep_l_att_9_787,
            vs_t_ep_l_att_10_790,
            vs_dm_org_code_838,
            vs_dm_site_code_841,
            vs_e1_branch_city_891,
            vs_e1_branch_state_894,
            vs_e1_branch_country_897,
            vs_e1_branch_category_1_901,
            vs_e1_branch_category_2_904,
            vs_e1_branch_category_3_907,
            vs_e1_branch_category_4_910,
            vs_e1_branch_category_5_913,
            vs_e1_customer_state_991,
            vs_e1_customer_country_994,
            vs_e1_customer_category_1174,
            vs_e1_customer_category_1191,
            vs_e1_customer_category_1194,
            vs_e1_customer_category_1197,
            vs_e1_customer_category_1200,
            vs_e1_customer_category_1203,
            vs_e1_customer_category_1206,
            vs_ebs_tp_zone_code_1251,
            vs_ebs_zone_code_1254,
            vs_ebs_account_code_1257,
            vs_ebs_customer_code_1260,
            vs_ebs_customer_class_c_1263,
            vs_ebs_operation_unit_c_1271,
            vs_ebs_business_group_c_1274,
            vs_ebs_legal_entity_cod_1277,
            vs_ebs_sales_channel_co_1280,
            vs_e1_customer_city_1311;
      EXIT WHEN rec_load%NOTFOUND;


      -----------------------------------------------------------------------------
      -- (1) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_lr1_570 IS NULL AND vs_t_ep_lr1_570 IS NOT NULL ) OR vs_p_t_ep_lr1_570 <> vs_t_ep_lr1_570 THEN
 
         vs_sql := 'SELECT t_ep_lr1_ep_id, lr1_desc FROM   t_ep_lr1 WHERE  LOWER(lr1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_lr1_ep_id, vs_lr1_desc_571 USING vs_t_ep_lr1_570;

         vs_p_t_ep_lr1_570 := vs_t_ep_lr1_570;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_lr1_570 := NULL;
         END;
      END IF;
      vi_t_ep_lr1_ep_id := NVL( vi_t_ep_lr1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (2) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_lr2_573 IS NULL AND vs_t_ep_lr2_573 IS NOT NULL ) OR vs_p_t_ep_lr2_573 <> vs_t_ep_lr2_573 THEN
 
         vs_sql := 'SELECT t_ep_lr2_ep_id, lr2_desc FROM   t_ep_lr2 WHERE  LOWER(lr2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_lr2_ep_id, vs_lr2_desc_574 USING vs_t_ep_lr2_573;

         vs_p_t_ep_lr2_573 := vs_t_ep_lr2_573;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_lr2_573 := NULL;
         END;
      END IF;
      vi_t_ep_lr2_ep_id := NVL( vi_t_ep_lr2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (3) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_lr2a_576 IS NULL AND vs_t_ep_lr2a_576 IS NOT NULL ) OR vs_p_t_ep_lr2a_576 <> vs_t_ep_lr2a_576 THEN
 
         vs_sql := 'SELECT t_ep_lr2a_ep_id, lr2a_desc FROM   t_ep_lr2a WHERE  LOWER(lr2a) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_lr2a_ep_id, vs_lr2a_desc_577 USING vs_t_ep_lr2a_576;

         vs_p_t_ep_lr2a_576 := vs_t_ep_lr2a_576;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_lr2a_576 := NULL;
         END;
      END IF;
      vi_t_ep_lr2a_ep_id := NVL( vi_t_ep_lr2a_ep_id,0);

      -----------------------------------------------------------------------------
      -- (4) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls1_581 IS NULL AND vs_t_ep_ls1_581 IS NOT NULL ) OR vs_p_t_ep_ls1_581 <> vs_t_ep_ls1_581 THEN
 
         vs_sql := 'SELECT t_ep_ls1_ep_id, ls1_desc FROM   t_ep_ls1 WHERE  LOWER(ls1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls1_ep_id, vs_ls1_desc_582 USING vs_t_ep_ls1_581;

         vs_p_t_ep_ls1_581 := vs_t_ep_ls1_581;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls1_581 := NULL;
         END;
      END IF;
      vi_t_ep_ls1_ep_id := NVL( vi_t_ep_ls1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (5) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls2_584 IS NULL AND vs_t_ep_ls2_584 IS NOT NULL ) OR vs_p_t_ep_ls2_584 <> vs_t_ep_ls2_584 THEN
 
         vs_sql := 'SELECT t_ep_ls2_ep_id, ls2_desc FROM   t_ep_ls2 WHERE  LOWER(ls2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls2_ep_id, vs_ls2_desc_585 USING vs_t_ep_ls2_584;

         vs_p_t_ep_ls2_584 := vs_t_ep_ls2_584;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls2_584 := NULL;
         END;
      END IF;
      vi_t_ep_ls2_ep_id := NVL( vi_t_ep_ls2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (6) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls3_587 IS NULL AND vs_t_ep_ls3_587 IS NOT NULL ) OR vs_p_t_ep_ls3_587 <> vs_t_ep_ls3_587 THEN
 
         vs_sql := 'SELECT t_ep_ls3_ep_id, ls3_desc FROM   t_ep_ls3 WHERE  LOWER(ls3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls3_ep_id, vs_ls3_desc_588 USING vs_t_ep_ls3_587;

         vs_p_t_ep_ls3_587 := vs_t_ep_ls3_587;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls3_587 := NULL;
         END;
      END IF;
      vi_t_ep_ls3_ep_id := NVL( vi_t_ep_ls3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (7) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls4_590 IS NULL AND vs_t_ep_ls4_590 IS NOT NULL ) OR vs_p_t_ep_ls4_590 <> vs_t_ep_ls4_590 THEN
 
         vs_sql := 'SELECT t_ep_ls4_ep_id, ls4_desc FROM   t_ep_ls4 WHERE  LOWER(ls4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls4_ep_id, vs_ls4_desc_591 USING vs_t_ep_ls4_590;

         vs_p_t_ep_ls4_590 := vs_t_ep_ls4_590;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls4_590 := NULL;
         END;
      END IF;
      vi_t_ep_ls4_ep_id := NVL( vi_t_ep_ls4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (8) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls5_617 IS NULL AND vs_t_ep_ls5_617 IS NOT NULL ) OR vs_p_t_ep_ls5_617 <> vs_t_ep_ls5_617 THEN
 
         vs_sql := 'SELECT t_ep_ls5_ep_id, ls5_desc FROM   t_ep_ls5 WHERE  LOWER(ls5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls5_ep_id, vs_ls5_desc_618 USING vs_t_ep_ls5_617;

         vs_p_t_ep_ls5_617 := vs_t_ep_ls5_617;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls5_617 := NULL;
         END;
      END IF;
      vi_t_ep_ls5_ep_id := NVL( vi_t_ep_ls5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (9) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_ls6_620 IS NULL AND vs_t_ep_ls6_620 IS NOT NULL ) OR vs_p_t_ep_ls6_620 <> vs_t_ep_ls6_620 THEN
 
         vs_sql := 'SELECT t_ep_ls6_ep_id, ls6_desc FROM   t_ep_ls6 WHERE  LOWER(ls6) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ls6_ep_id, vs_ls6_desc_621 USING vs_t_ep_ls6_620;

         vs_p_t_ep_ls6_620 := vs_t_ep_ls6_620;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_ls6_620 := NULL;
         END;
      END IF;
      vi_t_ep_ls6_ep_id := NVL( vi_t_ep_ls6_ep_id,0);

      -----------------------------------------------------------------------------
      -- (10) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_1_762 IS NULL AND vs_t_ep_l_att_1_762 IS NOT NULL ) OR vs_p_t_ep_l_att_1_762 <> vs_t_ep_l_att_1_762 THEN
 
         vs_sql := 'SELECT t_ep_l_att_1_ep_id, l_att_1_desc FROM   t_ep_l_att_1 WHERE  LOWER(l_att_1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_1_ep_id, vs_l_att_1_desc_763 USING vs_t_ep_l_att_1_762;

         vs_p_t_ep_l_att_1_762 := vs_t_ep_l_att_1_762;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_1_762 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_1_ep_id := NVL( vi_t_ep_l_att_1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (11) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_2_765 IS NULL AND vs_t_ep_l_att_2_765 IS NOT NULL ) OR vs_p_t_ep_l_att_2_765 <> vs_t_ep_l_att_2_765 THEN
 
         vs_sql := 'SELECT t_ep_l_att_2_ep_id, l_att_2_desc FROM   t_ep_l_att_2 WHERE  LOWER(l_att_2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_2_ep_id, vs_l_att_2_desc_766 USING vs_t_ep_l_att_2_765;

         vs_p_t_ep_l_att_2_765 := vs_t_ep_l_att_2_765;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_2_765 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_2_ep_id := NVL( vi_t_ep_l_att_2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (12) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_3_768 IS NULL AND vs_t_ep_l_att_3_768 IS NOT NULL ) OR vs_p_t_ep_l_att_3_768 <> vs_t_ep_l_att_3_768 THEN
 
         vs_sql := 'SELECT t_ep_l_att_3_ep_id, l_att_3_desc FROM   t_ep_l_att_3 WHERE  LOWER(l_att_3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_3_ep_id, vs_l_att_3_desc_771 USING vs_t_ep_l_att_3_768;

         vs_p_t_ep_l_att_3_768 := vs_t_ep_l_att_3_768;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_3_768 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_3_ep_id := NVL( vi_t_ep_l_att_3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (13) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_4_772 IS NULL AND vs_t_ep_l_att_4_772 IS NOT NULL ) OR vs_p_t_ep_l_att_4_772 <> vs_t_ep_l_att_4_772 THEN
 
         vs_sql := 'SELECT t_ep_l_att_4_ep_id, l_att_4_desc FROM   t_ep_l_att_4 WHERE  LOWER(l_att_4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_4_ep_id, vs_l_att_4_desc_773 USING vs_t_ep_l_att_4_772;

         vs_p_t_ep_l_att_4_772 := vs_t_ep_l_att_4_772;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_4_772 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_4_ep_id := NVL( vi_t_ep_l_att_4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (14) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_5_775 IS NULL AND vs_t_ep_l_att_5_775 IS NOT NULL ) OR vs_p_t_ep_l_att_5_775 <> vs_t_ep_l_att_5_775 THEN
 
         vs_sql := 'SELECT t_ep_l_att_5_ep_id, l_att_5_desc FROM   t_ep_l_att_5 WHERE  LOWER(l_att_5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_5_ep_id, vs_l_att_5_desc_776 USING vs_t_ep_l_att_5_775;

         vs_p_t_ep_l_att_5_775 := vs_t_ep_l_att_5_775;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_5_775 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_5_ep_id := NVL( vi_t_ep_l_att_5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (15) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_6_778 IS NULL AND vs_t_ep_l_att_6_778 IS NOT NULL ) OR vs_p_t_ep_l_att_6_778 <> vs_t_ep_l_att_6_778 THEN
 
         vs_sql := 'SELECT t_ep_l_att_6_ep_id, l_att_6_desc FROM   t_ep_l_att_6 WHERE  LOWER(l_att_6) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_6_ep_id, vs_l_att_6_desc_779 USING vs_t_ep_l_att_6_778;

         vs_p_t_ep_l_att_6_778 := vs_t_ep_l_att_6_778;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_6_778 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_6_ep_id := NVL( vi_t_ep_l_att_6_ep_id,0);

      -----------------------------------------------------------------------------
      -- (16) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_7_781 IS NULL AND vs_t_ep_l_att_7_781 IS NOT NULL ) OR vs_p_t_ep_l_att_7_781 <> vs_t_ep_l_att_7_781 THEN
 
         vs_sql := 'SELECT t_ep_l_att_7_ep_id, l_att_7_desc FROM   t_ep_l_att_7 WHERE  LOWER(l_att_7) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_7_ep_id, vs_l_att_7_desc_782 USING vs_t_ep_l_att_7_781;

         vs_p_t_ep_l_att_7_781 := vs_t_ep_l_att_7_781;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_7_781 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_7_ep_id := NVL( vi_t_ep_l_att_7_ep_id,0);

      -----------------------------------------------------------------------------
      -- (17) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_8_784 IS NULL AND vs_t_ep_l_att_8_784 IS NOT NULL ) OR vs_p_t_ep_l_att_8_784 <> vs_t_ep_l_att_8_784 THEN
 
         vs_sql := 'SELECT t_ep_l_att_8_ep_id, l_att_8_desc FROM   t_ep_l_att_8 WHERE  LOWER(l_att_8) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_8_ep_id, vs_l_att_8_desc_785 USING vs_t_ep_l_att_8_784;

         vs_p_t_ep_l_att_8_784 := vs_t_ep_l_att_8_784;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_8_784 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_8_ep_id := NVL( vi_t_ep_l_att_8_ep_id,0);

      -----------------------------------------------------------------------------
      -- (18) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_9_787 IS NULL AND vs_t_ep_l_att_9_787 IS NOT NULL ) OR vs_p_t_ep_l_att_9_787 <> vs_t_ep_l_att_9_787 THEN
 
         vs_sql := 'SELECT t_ep_l_att_9_ep_id, l_att_9_desc FROM   t_ep_l_att_9 WHERE  LOWER(l_att_9) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_9_ep_id, vs_l_att_9_desc_788 USING vs_t_ep_l_att_9_787;

         vs_p_t_ep_l_att_9_787 := vs_t_ep_l_att_9_787;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_9_787 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_9_ep_id := NVL( vi_t_ep_l_att_9_ep_id,0);

      -----------------------------------------------------------------------------
      -- (19) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_l_att_10_790 IS NULL AND vs_t_ep_l_att_10_790 IS NOT NULL ) OR vs_p_t_ep_l_att_10_790 <> vs_t_ep_l_att_10_790 THEN
 
         vs_sql := 'SELECT t_ep_l_att_10_ep_id, l_att_10_desc FROM   t_ep_l_att_10 WHERE  LOWER(l_att_10) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_l_att_10_ep_id, vs_l_att_10_desc_791 USING vs_t_ep_l_att_10_790;

         vs_p_t_ep_l_att_10_790 := vs_t_ep_l_att_10_790;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_l_att_10_790 := NULL;
         END;
      END IF;
      vi_t_ep_l_att_10_ep_id := NVL( vi_t_ep_l_att_10_ep_id,0);

      -----------------------------------------------------------------------------
      -- (20) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_dm_org_code_838 IS NULL AND vs_dm_org_code_838 IS NOT NULL ) OR vs_p_dm_org_code_838 <> vs_dm_org_code_838 THEN
 
         vs_sql := 'SELECT t_ep_organization_ep_id, dm_org_desc FROM   t_ep_organization WHERE  LOWER(organization) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_organization_ep_id, vs_dm_org_desc_839 USING vs_dm_org_code_838;

         vs_p_dm_org_code_838 := vs_dm_org_code_838;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_dm_org_code_838 := NULL;
         END;
      END IF;
      vi_t_ep_organization_ep_id := NVL( vi_t_ep_organization_ep_id,0);

      -----------------------------------------------------------------------------
      -- (21) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_dm_site_code_841 IS NULL AND vs_dm_site_code_841 IS NOT NULL ) OR vs_p_dm_site_code_841 <> vs_dm_site_code_841 THEN
 
         vs_sql := 'SELECT t_ep_site_ep_id, dm_site_desc FROM   t_ep_site WHERE  LOWER(site) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_site_ep_id, vs_dm_site_desc_842 USING vs_dm_site_code_841;

         vs_p_dm_site_code_841 := vs_dm_site_code_841;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_dm_site_code_841 := NULL;
         END;
      END IF;
      vi_t_ep_site_ep_id := NVL( vi_t_ep_site_ep_id,0);

      -----------------------------------------------------------------------------
      -- (22) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_city_891 IS NULL AND vs_e1_branch_city_891 IS NOT NULL ) OR vs_p_e1_branch_city_891 <> vs_e1_branch_city_891 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_city_ep_id, e1_br_city_desc FROM   t_ep_e1_br_city WHERE  LOWER(e1_br_city) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_city_ep_id, vs_e1_br_city_desc_892 USING vs_e1_branch_city_891;

         vs_p_e1_branch_city_891 := vs_e1_branch_city_891;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_city_891 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_city_ep_id := NVL( vi_t_ep_e1_br_city_ep_id,0);

      -----------------------------------------------------------------------------
      -- (23) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_state_894 IS NULL AND vs_e1_branch_state_894 IS NOT NULL ) OR vs_p_e1_branch_state_894 <> vs_e1_branch_state_894 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_state_ep_id, e1_br_state_desc FROM   t_ep_e1_br_state WHERE  LOWER(e1_br_state) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_state_ep_id, vs_e1_br_state_desc_895 USING vs_e1_branch_state_894;

         vs_p_e1_branch_state_894 := vs_e1_branch_state_894;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_state_894 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_state_ep_id := NVL( vi_t_ep_e1_br_state_ep_id,0);

      -----------------------------------------------------------------------------
      -- (24) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_country_897 IS NULL AND vs_e1_branch_country_897 IS NOT NULL ) OR vs_p_e1_branch_country_897 <> vs_e1_branch_country_897 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_country_ep_id, e1_br_country_desc FROM   t_ep_e1_br_country WHERE  LOWER(e1_br_country) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_country_ep_id, vs_e1_br_country_desc_898 USING vs_e1_branch_country_897;
 
         vs_p_e1_branch_country_897 := vs_e1_branch_country_897;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_country_897 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_country_ep_id := NVL( vi_t_ep_e1_br_country_ep_id,0);

      -----------------------------------------------------------------------------
      -- (25) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_category_1_901 IS NULL AND vs_e1_branch_category_1_901 IS NOT NULL ) OR vs_p_e1_branch_category_1_901 <> vs_e1_branch_category_1_901 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_cat_1_ep_id, e1_br_cat_desc_1 FROM   t_ep_e1_br_cat_1 WHERE  LOWER(e1_br_cat_1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_cat_1_ep_id, vs_e1_br_cat_desc_1_902 USING vs_e1_branch_category_1_901;
 
         vs_p_e1_branch_category_1_901 := vs_e1_branch_category_1_901;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_category_1_901 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_cat_1_ep_id := NVL( vi_t_ep_e1_br_cat_1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (26) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_category_2_904 IS NULL AND vs_e1_branch_category_2_904 IS NOT NULL ) OR vs_p_e1_branch_category_2_904 <> vs_e1_branch_category_2_904 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_cat_2_ep_id, e1_br_cat_desc_2 FROM   t_ep_e1_br_cat_2 WHERE  LOWER(e1_br_cat_2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_cat_2_ep_id, vs_e1_br_cat_desc_2_905 USING vs_e1_branch_category_2_904;
 
         vs_p_e1_branch_category_2_904 := vs_e1_branch_category_2_904;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_category_2_904 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_cat_2_ep_id := NVL( vi_t_ep_e1_br_cat_2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (27) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_category_3_907 IS NULL AND vs_e1_branch_category_3_907 IS NOT NULL ) OR vs_p_e1_branch_category_3_907 <> vs_e1_branch_category_3_907 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_cat_3_ep_id, e1_br_cat_desc_3 FROM   t_ep_e1_br_cat_3 WHERE  LOWER(e1_br_cat_3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_cat_3_ep_id, vs_e1_br_cat_desc_3_908 USING vs_e1_branch_category_3_907;
 
         vs_p_e1_branch_category_3_907 := vs_e1_branch_category_3_907;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_category_3_907 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_cat_3_ep_id := NVL( vi_t_ep_e1_br_cat_3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (28) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_category_4_910 IS NULL AND vs_e1_branch_category_4_910 IS NOT NULL ) OR vs_p_e1_branch_category_4_910 <> vs_e1_branch_category_4_910 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_cat_4_ep_id, e1_br_cat_desc_4 FROM   t_ep_e1_br_cat_4 WHERE  LOWER(e1_br_cat_4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_cat_4_ep_id, vs_e1_br_cat_desc_4_911 USING vs_e1_branch_category_4_910;
 
         vs_p_e1_branch_category_4_910 := vs_e1_branch_category_4_910;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_category_4_910 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_cat_4_ep_id := NVL( vi_t_ep_e1_br_cat_4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (29) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_branch_category_5_913 IS NULL AND vs_e1_branch_category_5_913 IS NOT NULL ) OR vs_p_e1_branch_category_5_913 <> vs_e1_branch_category_5_913 THEN
 
         vs_sql := 'SELECT t_ep_e1_br_cat_5_ep_id, e1_br_cat_desc_5 FROM   t_ep_e1_br_cat_5 WHERE  LOWER(e1_br_cat_5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_br_cat_5_ep_id, vs_e1_br_cat_desc_5_914 USING vs_e1_branch_category_5_913;
 
         vs_p_e1_branch_category_5_913 := vs_e1_branch_category_5_913;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_branch_category_5_913 := NULL;
         END;
      END IF;
      vi_t_ep_e1_br_cat_5_ep_id := NVL( vi_t_ep_e1_br_cat_5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (30) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_state_991 IS NULL AND vs_e1_customer_state_991 IS NOT NULL ) OR vs_p_e1_customer_state_991 <> vs_e1_customer_state_991 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_state_ep_id, e1_cust_state_desc FROM   t_ep_e1_cust_state WHERE  LOWER(e1_cust_state) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_state_ep_id, vs_e1_cust_state_desc_992 USING vs_e1_customer_state_991;
 
         vs_p_e1_customer_state_991 := vs_e1_customer_state_991;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_state_991 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_state_ep_id := NVL( vi_t_ep_e1_cust_state_ep_id,0);

      -----------------------------------------------------------------------------
      -- (31) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_country_994 IS NULL AND vs_e1_customer_country_994 IS NOT NULL ) OR vs_p_e1_customer_country_994 <> vs_e1_customer_country_994 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_ctry_ep_id, e1_cust_ctry_desc FROM   t_ep_e1_cust_ctry WHERE  LOWER(e1_cust_ctry) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_ctry_ep_id, vs_e1_cust_ctry_desc_995 USING vs_e1_customer_country_994;
 
         vs_p_e1_customer_country_994 := vs_e1_customer_country_994;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_country_994 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_ctry_ep_id := NVL( vi_t_ep_e1_cust_ctry_ep_id,0);

      -----------------------------------------------------------------------------
      -- (32) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1174 IS NULL AND vs_e1_customer_category_1174 IS NOT NULL ) OR vs_p_e1_customer_category_1174 <> vs_e1_customer_category_1174 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_1_ep_id, e1_cust_cat_1_desc FROM   t_ep_e1_cust_cat_1 WHERE  LOWER(e1_cust_cat_1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_1_ep_id, vs_e1_cust_cat_1_desc_1175 USING vs_e1_customer_category_1174;
 
         vs_p_e1_customer_category_1174 := vs_e1_customer_category_1174;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1174 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_1_ep_id := NVL( vi_t_ep_e1_cust_cat_1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (33) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1191 IS NULL AND vs_e1_customer_category_1191 IS NOT NULL ) OR vs_p_e1_customer_category_1191 <> vs_e1_customer_category_1191 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_2_ep_id, e1_cust_cat_2_desc FROM   t_ep_e1_cust_cat_2 WHERE  LOWER(e1_cust_cat_2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_2_ep_id, vs_e1_cust_cat_2_desc_1192 USING vs_e1_customer_category_1191;
 
         vs_p_e1_customer_category_1191 := vs_e1_customer_category_1191;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1191 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_2_ep_id := NVL( vi_t_ep_e1_cust_cat_2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (34) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1194 IS NULL AND vs_e1_customer_category_1194 IS NOT NULL ) OR vs_p_e1_customer_category_1194 <> vs_e1_customer_category_1194 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_3_ep_id, e1_cust_cat_3_desc FROM   t_ep_e1_cust_cat_3 WHERE  LOWER(e1_cust_cat_3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_3_ep_id, vs_e1_cust_cat_3_desc_1195 USING vs_e1_customer_category_1194;
 
         vs_p_e1_customer_category_1194 := vs_e1_customer_category_1194;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1194 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_3_ep_id := NVL( vi_t_ep_e1_cust_cat_3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (35) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1197 IS NULL AND vs_e1_customer_category_1197 IS NOT NULL ) OR vs_p_e1_customer_category_1197 <> vs_e1_customer_category_1197 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_4_ep_id, e1_cust_cat_4_desc FROM   t_ep_e1_cust_cat_4 WHERE  LOWER(e1_cust_cat_4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_4_ep_id, vs_e1_cust_cat_4_desc_1198 USING vs_e1_customer_category_1197;
 
         vs_p_e1_customer_category_1197 := vs_e1_customer_category_1197;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1197 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_4_ep_id := NVL( vi_t_ep_e1_cust_cat_4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (36) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1200 IS NULL AND vs_e1_customer_category_1200 IS NOT NULL ) OR vs_p_e1_customer_category_1200 <> vs_e1_customer_category_1200 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_5_ep_id, e1_cust_cat_5_desc FROM   t_ep_e1_cust_cat_5 WHERE  LOWER(e1_cust_cat_5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_5_ep_id, vs_e1_cust_cat_5_desc_1201 USING vs_e1_customer_category_1200;
 
         vs_p_e1_customer_category_1200 := vs_e1_customer_category_1200;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1200 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_5_ep_id := NVL( vi_t_ep_e1_cust_cat_5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (37) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1203 IS NULL AND vs_e1_customer_category_1203 IS NOT NULL ) OR vs_p_e1_customer_category_1203 <> vs_e1_customer_category_1203 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_6_ep_id, e1_cust_cat_6_desc FROM   t_ep_e1_cust_cat_6 WHERE  LOWER(e1_cust_cat_6) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_6_ep_id, vs_e1_cust_cat_6_desc_1204 USING vs_e1_customer_category_1203;
 
         vs_p_e1_customer_category_1203 := vs_e1_customer_category_1203;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1203 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_6_ep_id := NVL( vi_t_ep_e1_cust_cat_6_ep_id,0);

      -----------------------------------------------------------------------------
      -- (38) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_category_1206 IS NULL AND vs_e1_customer_category_1206 IS NOT NULL ) OR vs_p_e1_customer_category_1206 <> vs_e1_customer_category_1206 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_cat_7_ep_id, e1_cust_cat_7_desc FROM   t_ep_e1_cust_cat_7 WHERE  LOWER(e1_cust_cat_7) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_cat_7_ep_id, vs_e1_cust_cat_7_desc_1207 USING vs_e1_customer_category_1206;
 
         vs_p_e1_customer_category_1206 := vs_e1_customer_category_1206;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_category_1206 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_cat_7_ep_id := NVL( vi_t_ep_e1_cust_cat_7_ep_id,0);

      -----------------------------------------------------------------------------
      -- (39) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_tp_zone_code_1251 IS NULL AND vs_ebs_tp_zone_code_1251 IS NOT NULL ) OR vs_p_ebs_tp_zone_code_1251 <> vs_ebs_tp_zone_code_1251 THEN
 
         vs_sql := 'SELECT t_ep_ebs_tp_zone_ep_id, ebs_tp_zone_desc FROM   t_ep_ebs_tp_zone WHERE  LOWER(ebs_tp_zone) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_tp_zone_ep_id, vs_ebs_tp_zone_desc_1252 USING vs_ebs_tp_zone_code_1251;
 
         vs_p_ebs_tp_zone_code_1251 := vs_ebs_tp_zone_code_1251;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_tp_zone_code_1251 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_tp_zone_ep_id := NVL( vi_t_ep_ebs_tp_zone_ep_id,0);

      -----------------------------------------------------------------------------
      -- (40) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_zone_code_1254 IS NULL AND vs_ebs_zone_code_1254 IS NOT NULL ) OR vs_p_ebs_zone_code_1254 <> vs_ebs_zone_code_1254 THEN
 
         vs_sql := 'SELECT t_ep_ebs_zone_ep_id, ebs_zone_desc FROM   t_ep_ebs_zone WHERE  LOWER(ebs_zone) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_zone_ep_id, vs_ebs_zone_desc_1255 USING vs_ebs_zone_code_1254;

         vs_p_ebs_zone_code_1254 := vs_ebs_zone_code_1254;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_zone_code_1254 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_zone_ep_id := NVL( vi_t_ep_ebs_zone_ep_id,0);

      -----------------------------------------------------------------------------
      -- (41) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_account_code_1257 IS NULL AND vs_ebs_account_code_1257 IS NOT NULL ) OR vs_p_ebs_account_code_1257 <> vs_ebs_account_code_1257 THEN
 
         vs_sql := 'SELECT t_ep_ebs_account_ep_id, ebs_account_desc FROM   t_ep_ebs_account WHERE  LOWER(ebs_account) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_account_ep_id, vs_ebs_account_desc_1258 USING vs_ebs_account_code_1257;
 
         vs_p_ebs_account_code_1257 := vs_ebs_account_code_1257;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_account_code_1257 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_account_ep_id := NVL( vi_t_ep_ebs_account_ep_id,0);

      -----------------------------------------------------------------------------
      -- (42) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_customer_code_1260 IS NULL AND vs_ebs_customer_code_1260 IS NOT NULL ) OR vs_p_ebs_customer_code_1260 <> vs_ebs_customer_code_1260 THEN
 
         vs_sql := 'SELECT t_ep_ebs_customer_ep_id, ebs_customer_desc FROM   t_ep_ebs_customer WHERE  LOWER(ebs_customer) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_customer_ep_id, vs_ebs_customer_desc_1261 USING vs_ebs_customer_code_1260;
 
         vs_p_ebs_customer_code_1260 := vs_ebs_customer_code_1260;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_customer_code_1260 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_customer_ep_id := NVL( vi_t_ep_ebs_customer_ep_id,0);

      -----------------------------------------------------------------------------
      -- (43) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_customer_class_c_1263 IS NULL AND vs_ebs_customer_class_c_1263 IS NOT NULL ) OR vs_p_ebs_customer_class_c_1263 <> vs_ebs_customer_class_c_1263 THEN
 
         vs_sql := 'SELECT t_ep_ebs_cust_class_ep_id, ebs_cust_class_desc FROM   t_ep_ebs_cust_class WHERE  LOWER(ebs_cust_class) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_cust_class_ep_id, vs_ebs_cust_class_desc_1264 USING vs_ebs_customer_class_c_1263;
 
         vs_p_ebs_customer_class_c_1263 := vs_ebs_customer_class_c_1263;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_customer_class_c_1263 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_cust_class_ep_id := NVL( vi_t_ep_ebs_cust_class_ep_id,0);

      -----------------------------------------------------------------------------
      -- (44) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_operation_unit_c_1271 IS NULL AND vs_ebs_operation_unit_c_1271 IS NOT NULL ) OR vs_p_ebs_operation_unit_c_1271 <> vs_ebs_operation_unit_c_1271 THEN
 
         vs_sql := 'SELECT t_ep_ebs_oper_unit_ep_id, ebs_oper_unit_desc FROM   t_ep_ebs_oper_unit WHERE  LOWER(ebs_oper_unit) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_oper_unit_ep_id, vs_ebs_oper_unit_desc_1272 USING vs_ebs_operation_unit_c_1271;
 
         vs_p_ebs_operation_unit_c_1271 := vs_ebs_operation_unit_c_1271;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_operation_unit_c_1271 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_oper_unit_ep_id := NVL( vi_t_ep_ebs_oper_unit_ep_id,0);

      -----------------------------------------------------------------------------
      -- (45) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_business_group_c_1274 IS NULL AND vs_ebs_business_group_c_1274 IS NOT NULL ) OR vs_p_ebs_business_group_c_1274 <> vs_ebs_business_group_c_1274 THEN
 
         vs_sql := 'SELECT t_ep_ebs_bus_group_ep_id, ebs_bus_group_desc FROM   t_ep_ebs_bus_group WHERE  LOWER(ebs_bus_group) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_bus_group_ep_id, vs_ebs_bus_group_desc_1275 USING vs_ebs_business_group_c_1274;
 
         vs_p_ebs_business_group_c_1274 := vs_ebs_business_group_c_1274;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_business_group_c_1274 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_bus_group_ep_id := NVL( vi_t_ep_ebs_bus_group_ep_id,0);

      -----------------------------------------------------------------------------
      -- (46) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_legal_entity_cod_1277 IS NULL AND vs_ebs_legal_entity_cod_1277 IS NOT NULL ) OR vs_p_ebs_legal_entity_cod_1277 <> vs_ebs_legal_entity_cod_1277 THEN
 
         vs_sql := 'SELECT t_ep_ebs_legal_entity_ep_id, ebs_legal_ent_desc FROM   t_ep_ebs_legal_entity WHERE  LOWER(ebs_legal_entity) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_legal_entity_ep_id, vs_ebs_legal_ent_desc_1278 USING vs_ebs_legal_entity_cod_1277;
 
         vs_p_ebs_legal_entity_cod_1277 := vs_ebs_legal_entity_cod_1277;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_legal_entity_cod_1277 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_legal_entity_ep_id := NVL( vi_t_ep_ebs_legal_entity_ep_id,0);

      -----------------------------------------------------------------------------
      -- (47) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_sales_channel_co_1280 IS NULL AND vs_ebs_sales_channel_co_1280 IS NOT NULL ) OR vs_p_ebs_sales_channel_co_1280 <> vs_ebs_sales_channel_co_1280 THEN
 
         vs_sql := 'SELECT t_ep_ebs_sales_ch_ep_id, ebs_sales_ch_desc FROM   t_ep_ebs_sales_ch WHERE  LOWER(ebs_sales_ch) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_sales_ch_ep_id, vs_ebs_sales_ch_desc_1281 USING vs_ebs_sales_channel_co_1280;
 
         vs_p_ebs_sales_channel_co_1280 := vs_ebs_sales_channel_co_1280;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_sales_channel_co_1280 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_sales_ch_ep_id := NVL( vi_t_ep_ebs_sales_ch_ep_id,0);

      -----------------------------------------------------------------------------
      -- (48) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_customer_city_1311 IS NULL AND vs_e1_customer_city_1311 IS NOT NULL ) OR vs_p_e1_customer_city_1311 <> vs_e1_customer_city_1311 THEN
 
         vs_sql := 'SELECT t_ep_e1_cust_city_ep_id, e1_cust_city_desc FROM   t_ep_e1_cust_city WHERE  LOWER(e1_cust_city) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_cust_city_ep_id, vs_e1_cust_city_desc_1312 USING vs_e1_customer_city_1311;
 
         vs_p_e1_customer_city_1311 := vs_e1_customer_city_1311;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_customer_city_1311 := NULL;
         END;
      END IF;
      vi_t_ep_e1_cust_city_ep_id := NVL( vi_t_ep_e1_cust_city_ep_id,0);


      vs_description := vs_lr1_desc_571
                             ||vs_ls1_desc_582
                             ||vs_dm_org_desc_839
                             ||vs_dm_site_desc_842
                             ||vs_ebs_sales_ch_desc_1281
;

 
      --------------------------------------------------------------------------
      -- LOOP - USE THE ABOVE CURSOR VALUES TO GET THE LATEST VALUES (DESCRIPTION)
      --        FROM THE DIMENSIONS LEVELS TABLES
      --
      -- MERGE INTO dimension table
      --------------------------------------------------------------------------
 
 
      vs_sql := 'MERGE INTO location l
                 USING (SELECT DISTINCT
                               ''2'' loc_status,
                               0  is_fictive,
                               '|| vi_t_ep_lr1_ep_id ||' t_ep_lr1_ep_id,
                               '|| vi_t_ep_lr2_ep_id ||' t_ep_lr2_ep_id,
                               '|| vi_t_ep_lr2a_ep_id ||' t_ep_lr2a_ep_id,
                               '|| vi_t_ep_ls1_ep_id ||' t_ep_ls1_ep_id,
                               '|| vi_t_ep_ls2_ep_id ||' t_ep_ls2_ep_id,
                               '|| vi_t_ep_ls3_ep_id ||' t_ep_ls3_ep_id,
                               '|| vi_t_ep_ls4_ep_id ||' t_ep_ls4_ep_id,
                               '|| vi_t_ep_ls5_ep_id ||' t_ep_ls5_ep_id,
                               '|| vi_t_ep_ls6_ep_id ||' t_ep_ls6_ep_id,
                               '|| vi_t_ep_L_ATT_1_ep_id ||' t_ep_L_ATT_1_ep_id,
                               '|| vi_t_ep_L_ATT_2_ep_id ||' t_ep_L_ATT_2_ep_id,
                               '|| vi_t_ep_L_ATT_3_ep_id ||' t_ep_L_ATT_3_ep_id,
                               '|| vi_t_ep_L_ATT_4_ep_id ||' t_ep_L_ATT_4_ep_id,
                               '|| vi_t_ep_L_ATT_5_ep_id ||' t_ep_L_ATT_5_ep_id,
                               '|| vi_t_ep_L_ATT_6_ep_id ||' t_ep_L_ATT_6_ep_id,
                               '|| vi_t_ep_L_ATT_7_ep_id ||' t_ep_L_ATT_7_ep_id,
                               '|| vi_t_ep_L_ATT_8_ep_id ||' t_ep_L_ATT_8_ep_id,
                               '|| vi_t_ep_L_ATT_9_ep_id ||' t_ep_L_ATT_9_ep_id,
                               '|| vi_t_ep_L_ATT_10_ep_id ||' t_ep_L_ATT_10_ep_id,
                               '|| vi_t_ep_organization_ep_id ||' t_ep_organization_ep_id,
                               '|| vi_t_ep_site_ep_id ||' t_ep_site_ep_id,
                               '|| vi_t_ep_e1_br_city_ep_id ||' t_ep_e1_br_city_ep_id,
                               '|| vi_t_ep_e1_br_state_ep_id ||' t_ep_e1_br_state_ep_id,
                               '|| vi_t_ep_e1_br_country_ep_id ||' t_ep_e1_br_country_ep_id,
                               '|| vi_t_ep_e1_br_cat_1_ep_id ||' t_ep_e1_br_cat_1_ep_id,
                               '|| vi_t_ep_e1_br_cat_2_ep_id ||' t_ep_e1_br_cat_2_ep_id,
                               '|| vi_t_ep_e1_br_cat_3_ep_id ||' t_ep_e1_br_cat_3_ep_id,
                               '|| vi_t_ep_e1_br_cat_4_ep_id ||' t_ep_e1_br_cat_4_ep_id,
                               '|| vi_t_ep_e1_br_cat_5_ep_id ||' t_ep_e1_br_cat_5_ep_id,
                               '|| vi_t_ep_e1_cust_state_ep_id ||' t_ep_e1_cust_state_ep_id,
                               '|| vi_t_ep_e1_cust_ctry_ep_id ||' t_ep_e1_cust_ctry_ep_id,
                               '|| vi_t_ep_e1_cust_cat_1_ep_id ||' t_ep_e1_cust_cat_1_ep_id,
                               '|| vi_t_ep_e1_cust_cat_2_ep_id ||' t_ep_e1_cust_cat_2_ep_id,
                               '|| vi_t_ep_e1_cust_cat_3_ep_id ||' t_ep_e1_cust_cat_3_ep_id,
                               '|| vi_t_ep_e1_cust_cat_4_ep_id ||' t_ep_e1_cust_cat_4_ep_id,
                               '|| vi_t_ep_e1_cust_cat_5_ep_id ||' t_ep_e1_cust_cat_5_ep_id,
                               '|| vi_t_ep_e1_cust_cat_6_ep_id ||' t_ep_e1_cust_cat_6_ep_id,
                               '|| vi_t_ep_e1_cust_cat_7_ep_id ||' t_ep_e1_cust_cat_7_ep_id,
                               '|| vi_t_ep_ebs_tp_zone_ep_id ||' t_ep_ebs_tp_zone_ep_id,
                               '|| vi_t_ep_ebs_zone_ep_id ||' t_ep_ebs_zone_ep_id,
                               '|| vi_t_ep_ebs_account_ep_id ||' t_ep_ebs_account_ep_id,
                               '|| vi_t_ep_ebs_customer_ep_id ||' t_ep_ebs_customer_ep_id,
                               '|| vi_t_ep_ebs_cust_class_ep_id ||' t_ep_ebs_cust_class_ep_id,
                               '|| vi_t_ep_ebs_oper_unit_ep_id ||' t_ep_ebs_oper_unit_ep_id,
                               '|| vi_t_ep_ebs_bus_group_ep_id ||' t_ep_ebs_bus_group_ep_id,
                               '|| vi_t_ep_ebs_legal_entity_ep_id ||' t_ep_ebs_legal_entity_ep_id,
                               '|| vi_t_ep_ebs_sales_ch_ep_id ||' t_ep_ebs_sales_ch_ep_id,
                               '|| vi_t_ep_e1_cust_city_ep_id ||' t_ep_e1_cust_city_ep_id,
                               :1 description
                        FROM   DUAL
                       ) s
                 ON 
      (    l.t_ep_organization_ep_id       = s.t_ep_organization_ep_id
       AND l.t_ep_lr1_ep_id                = s.t_ep_lr1_ep_id
       AND l.t_ep_ls1_ep_id                = s.t_ep_ls1_ep_id
       AND l.t_ep_site_ep_id               = s.t_ep_site_ep_id
       AND l.t_ep_ebs_sales_ch_ep_id       = s.t_ep_ebs_sales_ch_ep_id)
                 WHEN MATCHED THEN UPDATE SET
             l.last_update_date          = '''|| vd_update_date ||''',
             l.is_fictive                = s.is_fictive,
             l.description = s.description
,             l.t_ep_lr2_ep_id                  = s.t_ep_lr2_ep_id,
             l.t_ep_lr2a_ep_id                 = s.t_ep_lr2a_ep_id,
             l.t_ep_ls2_ep_id                  = s.t_ep_ls2_ep_id,
             l.t_ep_ls3_ep_id                  = s.t_ep_ls3_ep_id,
             l.t_ep_ls4_ep_id                  = s.t_ep_ls4_ep_id,
             l.t_ep_ls5_ep_id                  = s.t_ep_ls5_ep_id,
             l.t_ep_ls6_ep_id                  = s.t_ep_ls6_ep_id,
             l.t_ep_l_att_1_ep_id              = s.t_ep_l_att_1_ep_id,
             l.t_ep_l_att_2_ep_id              = s.t_ep_l_att_2_ep_id,
             l.t_ep_l_att_3_ep_id              = s.t_ep_l_att_3_ep_id,
             l.t_ep_l_att_4_ep_id              = s.t_ep_l_att_4_ep_id,
             l.t_ep_l_att_5_ep_id              = s.t_ep_l_att_5_ep_id,
             l.t_ep_l_att_6_ep_id              = s.t_ep_l_att_6_ep_id,
             l.t_ep_l_att_7_ep_id              = s.t_ep_l_att_7_ep_id,
             l.t_ep_l_att_8_ep_id              = s.t_ep_l_att_8_ep_id,
             l.t_ep_l_att_9_ep_id              = s.t_ep_l_att_9_ep_id,
             l.t_ep_l_att_10_ep_id             = s.t_ep_l_att_10_ep_id,
             l.t_ep_e1_br_city_ep_id           = s.t_ep_e1_br_city_ep_id,
             l.t_ep_e1_br_state_ep_id          = s.t_ep_e1_br_state_ep_id,
             l.t_ep_e1_br_country_ep_id        = s.t_ep_e1_br_country_ep_id,
             l.t_ep_e1_br_cat_1_ep_id          = s.t_ep_e1_br_cat_1_ep_id,
             l.t_ep_e1_br_cat_2_ep_id          = s.t_ep_e1_br_cat_2_ep_id,
             l.t_ep_e1_br_cat_3_ep_id          = s.t_ep_e1_br_cat_3_ep_id,
             l.t_ep_e1_br_cat_4_ep_id          = s.t_ep_e1_br_cat_4_ep_id,
             l.t_ep_e1_br_cat_5_ep_id          = s.t_ep_e1_br_cat_5_ep_id,
             l.t_ep_e1_cust_state_ep_id        = s.t_ep_e1_cust_state_ep_id,
             l.t_ep_e1_cust_ctry_ep_id         = s.t_ep_e1_cust_ctry_ep_id,
             l.t_ep_e1_cust_cat_1_ep_id        = s.t_ep_e1_cust_cat_1_ep_id,
             l.t_ep_e1_cust_cat_2_ep_id        = s.t_ep_e1_cust_cat_2_ep_id,
             l.t_ep_e1_cust_cat_3_ep_id        = s.t_ep_e1_cust_cat_3_ep_id,
             l.t_ep_e1_cust_cat_4_ep_id        = s.t_ep_e1_cust_cat_4_ep_id,
             l.t_ep_e1_cust_cat_5_ep_id        = s.t_ep_e1_cust_cat_5_ep_id,
             l.t_ep_e1_cust_cat_6_ep_id        = s.t_ep_e1_cust_cat_6_ep_id,
             l.t_ep_e1_cust_cat_7_ep_id        = s.t_ep_e1_cust_cat_7_ep_id,
             l.t_ep_ebs_tp_zone_ep_id          = s.t_ep_ebs_tp_zone_ep_id,
             l.t_ep_ebs_zone_ep_id             = s.t_ep_ebs_zone_ep_id,
             l.t_ep_ebs_account_ep_id          = s.t_ep_ebs_account_ep_id,
             l.t_ep_ebs_customer_ep_id         = s.t_ep_ebs_customer_ep_id,
             l.t_ep_ebs_cust_class_ep_id       = s.t_ep_ebs_cust_class_ep_id,
             l.t_ep_ebs_oper_unit_ep_id        = s.t_ep_ebs_oper_unit_ep_id,
             l.t_ep_ebs_bus_group_ep_id        = s.t_ep_ebs_bus_group_ep_id,
             l.t_ep_ebs_legal_entity_ep_id     = s.t_ep_ebs_legal_entity_ep_id,
             l.t_ep_e1_cust_city_ep_id         = s.t_ep_e1_cust_city_ep_id
                 WHEN NOT MATCHED THEN INSERT    (
         location_id,
         loc_status,
         is_fictive, 
         t_ep_e1_br_cat_1_ep_id , 
         t_ep_ebs_legal_entity_ep_id , 
         t_ep_ebs_bus_group_ep_id , 
         t_ep_ebs_oper_unit_ep_id , 
         t_ep_e1_br_city_ep_id , 
         t_ep_e1_br_state_ep_id , 
         t_ep_e1_br_country_ep_id , 
         t_ep_e1_br_cat_2_ep_id , 
         t_ep_e1_br_cat_3_ep_id , 
         t_ep_e1_br_cat_4_ep_id , 
         t_ep_e1_br_cat_5_ep_id , 
         t_ep_organization_ep_id , 
         t_ep_lr1_ep_id , 
         t_ep_ls6_ep_id , 
         t_ep_ls5_ep_id , 
         t_ep_ls4_ep_id , 
         t_ep_ls3_ep_id , 
         t_ep_ls2_ep_id , 
         t_ep_ls1_ep_id , 
         t_ep_e1_cust_city_ep_id , 
         t_ep_l_att_1_ep_id , 
         t_ep_l_att_2_ep_id , 
         t_ep_l_att_3_ep_id , 
         t_ep_l_att_4_ep_id , 
         t_ep_l_att_5_ep_id , 
         t_ep_l_att_6_ep_id , 
         t_ep_l_att_7_ep_id , 
         t_ep_l_att_8_ep_id , 
         t_ep_l_att_9_ep_id , 
         t_ep_l_att_10_ep_id , 
         t_ep_lr2_ep_id , 
         t_ep_lr2a_ep_id , 
         t_ep_ebs_cust_class_ep_id , 
         t_ep_ebs_customer_ep_id , 
         t_ep_ebs_account_ep_id , 
         t_ep_ebs_zone_ep_id , 
         t_ep_ebs_tp_zone_ep_id , 
         t_ep_e1_cust_cat_7_ep_id , 
         t_ep_e1_cust_cat_6_ep_id , 
         t_ep_e1_cust_state_ep_id , 
         t_ep_e1_cust_ctry_ep_id , 
         t_ep_e1_cust_cat_1_ep_id , 
         t_ep_e1_cust_cat_2_ep_id , 
         t_ep_e1_cust_cat_3_ep_id , 
         t_ep_e1_cust_cat_4_ep_id , 
         t_ep_e1_cust_cat_5_ep_id , 
         t_ep_site_ep_id , 
         t_ep_ebs_sales_ch_ep_id , 
         description,
         last_update_date)
      VALUES    (
         location_seq.NEXTVAL,
         s.loc_status,
         s.is_fictive, 
         s.t_ep_e1_br_cat_1_ep_id , 
         s.t_ep_ebs_legal_entity_ep_id , 
         s.t_ep_ebs_bus_group_ep_id , 
         s.t_ep_ebs_oper_unit_ep_id , 
         s.t_ep_e1_br_city_ep_id , 
         s.t_ep_e1_br_state_ep_id , 
         s.t_ep_e1_br_country_ep_id , 
         s.t_ep_e1_br_cat_2_ep_id , 
         s.t_ep_e1_br_cat_3_ep_id , 
         s.t_ep_e1_br_cat_4_ep_id , 
         s.t_ep_e1_br_cat_5_ep_id , 
         s.t_ep_organization_ep_id , 
         s.t_ep_lr1_ep_id , 
         s.t_ep_ls6_ep_id , 
         s.t_ep_ls5_ep_id , 
         s.t_ep_ls4_ep_id , 
         s.t_ep_ls3_ep_id , 
         s.t_ep_ls2_ep_id , 
         s.t_ep_ls1_ep_id , 
         s.t_ep_e1_cust_city_ep_id , 
         s.t_ep_l_att_1_ep_id , 
         s.t_ep_l_att_2_ep_id , 
         s.t_ep_l_att_3_ep_id , 
         s.t_ep_l_att_4_ep_id , 
         s.t_ep_l_att_5_ep_id , 
         s.t_ep_l_att_6_ep_id , 
         s.t_ep_l_att_7_ep_id , 
         s.t_ep_l_att_8_ep_id , 
         s.t_ep_l_att_9_ep_id , 
         s.t_ep_l_att_10_ep_id , 
         s.t_ep_lr2_ep_id , 
         s.t_ep_lr2a_ep_id , 
         s.t_ep_ebs_cust_class_ep_id , 
         s.t_ep_ebs_customer_ep_id , 
         s.t_ep_ebs_account_ep_id , 
         s.t_ep_ebs_zone_ep_id , 
         s.t_ep_ebs_tp_zone_ep_id , 
         s.t_ep_e1_cust_cat_7_ep_id , 
         s.t_ep_e1_cust_cat_6_ep_id , 
         s.t_ep_e1_cust_state_ep_id , 
         s.t_ep_e1_cust_ctry_ep_id , 
         s.t_ep_e1_cust_cat_1_ep_id , 
         s.t_ep_e1_cust_cat_2_ep_id , 
         s.t_ep_e1_cust_cat_3_ep_id , 
         s.t_ep_e1_cust_cat_4_ep_id , 
         s.t_ep_e1_cust_cat_5_ep_id , 
         s.t_ep_site_ep_id , 
         s.t_ep_ebs_sales_ch_ep_id , 
         s.description,
         '''|| vd_load_date ||''' )';


 
      BEGIN
 
         EXECUTE IMMEDIATE vs_sql USING vs_description;
 
         vs_dim_merge_count := vs_dim_merge_count + sql%rowcount;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SQLERRM;
 
            vs_status := 'FAILED';
 
            IF vi_error_code = -30926 THEN
               /************************************************************************/
               /* ORA-30926: unable to get a stable set of rows in the source tables
               /************************************************************************/
 
               vs_msg     := 'The MERGE into location failed : '|| vs_error_text;                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
               vs_msg     := 'The MERGE into location failed : There are duplicate base levels with differing level codes in the staging table';
                                                                                                                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
            ELSE
 
               vs_msg     := 'The MERGE into location failed : '|| vs_error_text;                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);

               RAISE;

            END IF;
      END;


      COMMIT;

   END LOOP;
   CLOSE rec_load;

   COMMIT;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE
   ----------------------------------------------------------------------------------
   vs_log_msg := 'LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   ----------------------------------------------------------------------------------
   -- LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE
   ----------------------------------------------------------------------------------
   vs_dim_level_update_count := 0;
   dv_rowcounter := 0;
 
   FOR rec IN (
       SELECT 
          t_ep_e1_br_cat_1.t_ep_e1_br_cat_1_ep_id , 
          t_ep_ebs_legal_entity.t_ep_ebs_legal_entity_ep_id , 
          t_ep_ebs_bus_group.t_ep_ebs_bus_group_ep_id , 
          t_ep_ebs_oper_unit.t_ep_ebs_oper_unit_ep_id , 
          t_ep_e1_br_city.t_ep_e1_br_city_ep_id , 
          t_ep_e1_br_state.t_ep_e1_br_state_ep_id , 
          t_ep_e1_br_country.t_ep_e1_br_country_ep_id , 
          t_ep_e1_br_cat_2.t_ep_e1_br_cat_2_ep_id , 
          t_ep_e1_br_cat_3.t_ep_e1_br_cat_3_ep_id , 
          t_ep_e1_br_cat_4.t_ep_e1_br_cat_4_ep_id , 
          t_ep_e1_br_cat_5.t_ep_e1_br_cat_5_ep_id , 
          t_ep_organization.t_ep_organization_ep_id , 
          t_ep_lr1.t_ep_lr1_ep_id , 
          t_ep_ls6.t_ep_ls6_ep_id , 
          t_ep_ls5.t_ep_ls5_ep_id , 
          t_ep_ls4.t_ep_ls4_ep_id , 
          t_ep_ls3.t_ep_ls3_ep_id , 
          t_ep_ls2.t_ep_ls2_ep_id , 
          t_ep_ls1.t_ep_ls1_ep_id , 
          t_ep_e1_cust_city.t_ep_e1_cust_city_ep_id , 
          t_ep_l_att_1.t_ep_l_att_1_ep_id , 
          t_ep_l_att_2.t_ep_l_att_2_ep_id , 
          t_ep_l_att_3.t_ep_l_att_3_ep_id , 
          t_ep_l_att_4.t_ep_l_att_4_ep_id , 
          t_ep_l_att_5.t_ep_l_att_5_ep_id , 
          t_ep_l_att_6.t_ep_l_att_6_ep_id , 
          t_ep_l_att_7.t_ep_l_att_7_ep_id , 
          t_ep_l_att_8.t_ep_l_att_8_ep_id , 
          t_ep_l_att_9.t_ep_l_att_9_ep_id , 
          t_ep_l_att_10.t_ep_l_att_10_ep_id , 
          t_ep_lr2.t_ep_lr2_ep_id , 
          t_ep_lr2a.t_ep_lr2a_ep_id , 
          t_ep_ebs_cust_class.t_ep_ebs_cust_class_ep_id , 
          t_ep_ebs_customer.t_ep_ebs_customer_ep_id , 
          t_ep_ebs_account.t_ep_ebs_account_ep_id , 
          t_ep_ebs_zone.t_ep_ebs_zone_ep_id , 
          t_ep_ebs_tp_zone.t_ep_ebs_tp_zone_ep_id , 
          t_ep_e1_cust_cat_7.t_ep_e1_cust_cat_7_ep_id , 
          t_ep_e1_cust_cat_6.t_ep_e1_cust_cat_6_ep_id , 
          t_ep_e1_cust_state.t_ep_e1_cust_state_ep_id , 
          t_ep_e1_cust_ctry.t_ep_e1_cust_ctry_ep_id , 
          t_ep_e1_cust_cat_1.t_ep_e1_cust_cat_1_ep_id , 
          t_ep_e1_cust_cat_2.t_ep_e1_cust_cat_2_ep_id , 
          t_ep_e1_cust_cat_3.t_ep_e1_cust_cat_3_ep_id , 
          t_ep_e1_cust_cat_4.t_ep_e1_cust_cat_4_ep_id , 
          t_ep_e1_cust_cat_5.t_ep_e1_cust_cat_5_ep_id , 
          t_ep_site.t_ep_site_ep_id , 
          t_ep_ebs_sales_ch.t_ep_ebs_sales_ch_ep_id , 
              
           t_ep_organization.dm_org_desc ||
           t_ep_lr1.lr1_desc ||
           t_ep_ls1.ls1_desc ||
           t_ep_site.dm_site_desc ||
           t_ep_ebs_sales_ch.ebs_sales_ch_desc  description
       FROM location,t_ep_e1_br_cat_1,t_ep_ebs_legal_entity,t_ep_ebs_bus_group,t_ep_ebs_oper_unit,t_ep_e1_br_city,t_ep_e1_br_state,t_ep_e1_br_country,t_ep_e1_br_cat_2,t_ep_e1_br_cat_3,t_ep_e1_br_cat_4,t_ep_e1_br_cat_5,t_ep_organization,t_ep_lr1,t_ep_ls6,
t_ep_ls5,t_ep_ls4,t_ep_ls3,t_ep_ls2,t_ep_ls1,t_ep_e1_cust_city,t_ep_L_ATT_1,t_ep_L_ATT_2,t_ep_L_ATT_3,t_ep_L_ATT_4,t_ep_L_ATT_5,t_ep_L_ATT_6,t_ep_L_ATT_7,t_ep_L_ATT_8,t_ep_L_ATT_9,t_ep_L_ATT_10,t_ep_lr2,t_ep_lr2a,t_ep_ebs_cust_class,t_ep_ebs_customer,
t_ep_ebs_account,t_ep_ebs_zone,t_ep_ebs_tp_zone,t_ep_e1_cust_cat_7,t_ep_e1_cust_cat_6,t_ep_e1_cust_state,t_ep_e1_cust_ctry,t_ep_e1_cust_cat_1,t_ep_e1_cust_cat_2,t_ep_e1_cust_cat_3,t_ep_e1_cust_cat_4,t_ep_e1_cust_cat_5,t_ep_site,t_ep_ebs_sales_ch
       WHERE 1 = 1
         AND t_ep_organization.t_ep_e1_br_cat_1_ep_id           = t_ep_e1_br_cat_1.t_ep_e1_br_cat_1_ep_id
         AND t_ep_organization.t_ep_ebs_legal_entity_ep_id      = t_ep_ebs_legal_entity.t_ep_ebs_legal_entity_ep_id
         AND t_ep_ebs_oper_unit.t_ep_ebs_bus_group_ep_id        = t_ep_ebs_bus_group.t_ep_ebs_bus_group_ep_id
         AND t_ep_organization.t_ep_ebs_oper_unit_ep_id         = t_ep_ebs_oper_unit.t_ep_ebs_oper_unit_ep_id
         AND t_ep_organization.t_ep_e1_br_city_ep_id            = t_ep_e1_br_city.t_ep_e1_br_city_ep_id
         AND t_ep_organization.t_ep_e1_br_state_ep_id           = t_ep_e1_br_state.t_ep_e1_br_state_ep_id
         AND t_ep_organization.t_ep_e1_br_country_ep_id         = t_ep_e1_br_country.t_ep_e1_br_country_ep_id
         AND t_ep_organization.t_ep_e1_br_cat_2_ep_id           = t_ep_e1_br_cat_2.t_ep_e1_br_cat_2_ep_id
         AND t_ep_organization.t_ep_e1_br_cat_3_ep_id           = t_ep_e1_br_cat_3.t_ep_e1_br_cat_3_ep_id
         AND t_ep_organization.t_ep_e1_br_cat_4_ep_id           = t_ep_e1_br_cat_4.t_ep_e1_br_cat_4_ep_id
         AND t_ep_organization.t_ep_e1_br_cat_5_ep_id           = t_ep_e1_br_cat_5.t_ep_e1_br_cat_5_ep_id
         AND t_ep_organization.t_ep_organization_ep_id          = location.t_ep_organization_ep_id
         AND t_ep_lr1.t_ep_lr1_ep_id                            = location.t_ep_lr1_ep_id
         AND t_ep_ls5.t_ep_ls6_ep_id                            = t_ep_ls6.t_ep_ls6_ep_id
         AND t_ep_ls4.t_ep_ls5_ep_id                            = t_ep_ls5.t_ep_ls5_ep_id
         AND t_ep_ls3.t_ep_ls4_ep_id                            = t_ep_ls4.t_ep_ls4_ep_id
         AND t_ep_ls2.t_ep_ls3_ep_id                            = t_ep_ls3.t_ep_ls3_ep_id
         AND t_ep_ls1.t_ep_ls2_ep_id                            = t_ep_ls2.t_ep_ls2_ep_id
         AND t_ep_ls1.t_ep_ls1_ep_id                            = location.t_ep_ls1_ep_id
         AND t_ep_site.t_ep_e1_cust_city_ep_id                  = t_ep_e1_cust_city.t_ep_e1_cust_city_ep_id
         AND t_ep_site.t_ep_L_ATT_1_ep_id                       = t_ep_L_ATT_1.t_ep_L_ATT_1_ep_id
         AND t_ep_site.t_ep_L_ATT_2_ep_id                       = t_ep_L_ATT_2.t_ep_L_ATT_2_ep_id
         AND t_ep_site.t_ep_L_ATT_3_ep_id                       = t_ep_L_ATT_3.t_ep_L_ATT_3_ep_id
         AND t_ep_site.t_ep_L_ATT_4_ep_id                       = t_ep_L_ATT_4.t_ep_L_ATT_4_ep_id
         AND t_ep_site.t_ep_L_ATT_5_ep_id                       = t_ep_L_ATT_5.t_ep_L_ATT_5_ep_id
         AND t_ep_site.t_ep_L_ATT_6_ep_id                       = t_ep_L_ATT_6.t_ep_L_ATT_6_ep_id
         AND t_ep_site.t_ep_L_ATT_7_ep_id                       = t_ep_L_ATT_7.t_ep_L_ATT_7_ep_id
         AND t_ep_site.t_ep_L_ATT_8_ep_id                       = t_ep_L_ATT_8.t_ep_L_ATT_8_ep_id
         AND t_ep_site.t_ep_L_ATT_9_ep_id                       = t_ep_L_ATT_9.t_ep_L_ATT_9_ep_id
         AND t_ep_site.t_ep_L_ATT_10_ep_id                      = t_ep_L_ATT_10.t_ep_L_ATT_10_ep_id
         AND t_ep_site.t_ep_lr2_ep_id                           = t_ep_lr2.t_ep_lr2_ep_id
         AND t_ep_site.t_ep_lr2a_ep_id                          = t_ep_lr2a.t_ep_lr2a_ep_id
         AND t_ep_ebs_customer.t_ep_ebs_cust_class_ep_id        = t_ep_ebs_cust_class.t_ep_ebs_cust_class_ep_id
         AND t_ep_ebs_account.t_ep_ebs_customer_ep_id           = t_ep_ebs_customer.t_ep_ebs_customer_ep_id
         AND t_ep_site.t_ep_ebs_account_ep_id                   = t_ep_ebs_account.t_ep_ebs_account_ep_id
         AND t_ep_ebs_tp_zone.t_ep_ebs_zone_ep_id               = t_ep_ebs_zone.t_ep_ebs_zone_ep_id
         AND t_ep_site.t_ep_ebs_tp_zone_ep_id                   = t_ep_ebs_tp_zone.t_ep_ebs_tp_zone_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_7_ep_id                 = t_ep_e1_cust_cat_7.t_ep_e1_cust_cat_7_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_6_ep_id                 = t_ep_e1_cust_cat_6.t_ep_e1_cust_cat_6_ep_id
         AND t_ep_site.t_ep_e1_cust_state_ep_id                 = t_ep_e1_cust_state.t_ep_e1_cust_state_ep_id
         AND t_ep_site.t_ep_e1_cust_ctry_ep_id                  = t_ep_e1_cust_ctry.t_ep_e1_cust_ctry_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_1_ep_id                 = t_ep_e1_cust_cat_1.t_ep_e1_cust_cat_1_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_2_ep_id                 = t_ep_e1_cust_cat_2.t_ep_e1_cust_cat_2_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_3_ep_id                 = t_ep_e1_cust_cat_3.t_ep_e1_cust_cat_3_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_4_ep_id                 = t_ep_e1_cust_cat_4.t_ep_e1_cust_cat_4_ep_id
         AND t_ep_site.t_ep_e1_cust_cat_5_ep_id                 = t_ep_e1_cust_cat_5.t_ep_e1_cust_cat_5_ep_id
         AND t_ep_site.t_ep_site_ep_id                          = location.t_ep_site_ep_id
         AND t_ep_ebs_sales_ch.t_ep_ebs_sales_ch_ep_id          = location.t_ep_ebs_sales_ch_ep_id)
   LOOP

 
      UPDATE location
      SET    description = rec.description,
             
             t_ep_e1_br_cat_1_ep_id                             = rec.t_ep_e1_br_cat_1_ep_id,
             t_ep_ebs_legal_entity_ep_id                        = rec.t_ep_ebs_legal_entity_ep_id,
             t_ep_ebs_bus_group_ep_id                           = rec.t_ep_ebs_bus_group_ep_id,
             t_ep_ebs_oper_unit_ep_id                           = rec.t_ep_ebs_oper_unit_ep_id,
             t_ep_e1_br_city_ep_id                              = rec.t_ep_e1_br_city_ep_id,
             t_ep_e1_br_state_ep_id                             = rec.t_ep_e1_br_state_ep_id,
             t_ep_e1_br_country_ep_id                           = rec.t_ep_e1_br_country_ep_id,
             t_ep_e1_br_cat_2_ep_id                             = rec.t_ep_e1_br_cat_2_ep_id,
             t_ep_e1_br_cat_3_ep_id                             = rec.t_ep_e1_br_cat_3_ep_id,
             t_ep_e1_br_cat_4_ep_id                             = rec.t_ep_e1_br_cat_4_ep_id,
             t_ep_e1_br_cat_5_ep_id                             = rec.t_ep_e1_br_cat_5_ep_id,
             t_ep_organization_ep_id                            = rec.t_ep_organization_ep_id,
             t_ep_lr1_ep_id                                     = rec.t_ep_lr1_ep_id,
             t_ep_ls6_ep_id                                     = rec.t_ep_ls6_ep_id,
             t_ep_ls5_ep_id                                     = rec.t_ep_ls5_ep_id,
             t_ep_ls4_ep_id                                     = rec.t_ep_ls4_ep_id,
             t_ep_ls3_ep_id                                     = rec.t_ep_ls3_ep_id,
             t_ep_ls2_ep_id                                     = rec.t_ep_ls2_ep_id,
             t_ep_ls1_ep_id                                     = rec.t_ep_ls1_ep_id,
             t_ep_e1_cust_city_ep_id                            = rec.t_ep_e1_cust_city_ep_id,
             t_ep_L_ATT_1_ep_id                                 = rec.t_ep_L_ATT_1_ep_id,
             t_ep_L_ATT_2_ep_id                                 = rec.t_ep_L_ATT_2_ep_id,
             t_ep_L_ATT_3_ep_id                                 = rec.t_ep_L_ATT_3_ep_id,
             t_ep_L_ATT_4_ep_id                                 = rec.t_ep_L_ATT_4_ep_id,
             t_ep_L_ATT_5_ep_id                                 = rec.t_ep_L_ATT_5_ep_id,
             t_ep_L_ATT_6_ep_id                                 = rec.t_ep_L_ATT_6_ep_id,
             t_ep_L_ATT_7_ep_id                                 = rec.t_ep_L_ATT_7_ep_id,
             t_ep_L_ATT_8_ep_id                                 = rec.t_ep_L_ATT_8_ep_id,
             t_ep_L_ATT_9_ep_id                                 = rec.t_ep_L_ATT_9_ep_id,
             t_ep_L_ATT_10_ep_id                                = rec.t_ep_L_ATT_10_ep_id,
             t_ep_lr2_ep_id                                     = rec.t_ep_lr2_ep_id,
             t_ep_lr2a_ep_id                                    = rec.t_ep_lr2a_ep_id,
             t_ep_ebs_cust_class_ep_id                          = rec.t_ep_ebs_cust_class_ep_id,
             t_ep_ebs_customer_ep_id                            = rec.t_ep_ebs_customer_ep_id,
             t_ep_ebs_account_ep_id                             = rec.t_ep_ebs_account_ep_id,
             t_ep_ebs_zone_ep_id                                = rec.t_ep_ebs_zone_ep_id,
             t_ep_ebs_tp_zone_ep_id                             = rec.t_ep_ebs_tp_zone_ep_id,
             t_ep_e1_cust_cat_7_ep_id                           = rec.t_ep_e1_cust_cat_7_ep_id,
             t_ep_e1_cust_cat_6_ep_id                           = rec.t_ep_e1_cust_cat_6_ep_id,
             t_ep_e1_cust_state_ep_id                           = rec.t_ep_e1_cust_state_ep_id,
             t_ep_e1_cust_ctry_ep_id                            = rec.t_ep_e1_cust_ctry_ep_id,
             t_ep_e1_cust_cat_1_ep_id                           = rec.t_ep_e1_cust_cat_1_ep_id,
             t_ep_e1_cust_cat_2_ep_id                           = rec.t_ep_e1_cust_cat_2_ep_id,
             t_ep_e1_cust_cat_3_ep_id                           = rec.t_ep_e1_cust_cat_3_ep_id,
             t_ep_e1_cust_cat_4_ep_id                           = rec.t_ep_e1_cust_cat_4_ep_id,
             t_ep_e1_cust_cat_5_ep_id                           = rec.t_ep_e1_cust_cat_5_ep_id,
             t_ep_site_ep_id                                    = rec.t_ep_site_ep_id,
             t_ep_ebs_sales_ch_ep_id                            = rec.t_ep_ebs_sales_ch_ep_id,
              last_update_date = vd_update_date
      WHERE  1 = 1
     
         AND t_ep_organization_ep_id                            = rec.t_ep_organization_ep_id 
         AND t_ep_lr1_ep_id                                     = rec.t_ep_lr1_ep_id 
         AND t_ep_ls1_ep_id                                     = rec.t_ep_ls1_ep_id 
         AND t_ep_site_ep_id                                    = rec.t_ep_site_ep_id 
         AND t_ep_ebs_sales_ch_ep_id                            = rec.t_ep_ebs_sales_ch_ep_id 
      AND  (
          location.t_ep_e1_br_cat_1_ep_id                    <> rec.t_ep_e1_br_cat_1_ep_id
   OR     location.t_ep_ebs_legal_entity_ep_id               <> rec.t_ep_ebs_legal_entity_ep_id
   OR     location.t_ep_ebs_bus_group_ep_id                  <> rec.t_ep_ebs_bus_group_ep_id
   OR     location.t_ep_ebs_oper_unit_ep_id                  <> rec.t_ep_ebs_oper_unit_ep_id
   OR     location.t_ep_e1_br_city_ep_id                     <> rec.t_ep_e1_br_city_ep_id
   OR     location.t_ep_e1_br_state_ep_id                    <> rec.t_ep_e1_br_state_ep_id
   OR     location.t_ep_e1_br_country_ep_id                  <> rec.t_ep_e1_br_country_ep_id
   OR     location.t_ep_e1_br_cat_2_ep_id                    <> rec.t_ep_e1_br_cat_2_ep_id
   OR     location.t_ep_e1_br_cat_3_ep_id                    <> rec.t_ep_e1_br_cat_3_ep_id
   OR     location.t_ep_e1_br_cat_4_ep_id                    <> rec.t_ep_e1_br_cat_4_ep_id
   OR     location.t_ep_e1_br_cat_5_ep_id                    <> rec.t_ep_e1_br_cat_5_ep_id
   OR     location.t_ep_ls6_ep_id                            <> rec.t_ep_ls6_ep_id
   OR     location.t_ep_ls5_ep_id                            <> rec.t_ep_ls5_ep_id
   OR     location.t_ep_ls4_ep_id                            <> rec.t_ep_ls4_ep_id
   OR     location.t_ep_ls3_ep_id                            <> rec.t_ep_ls3_ep_id
   OR     location.t_ep_ls2_ep_id                            <> rec.t_ep_ls2_ep_id
   OR     location.t_ep_e1_cust_city_ep_id                   <> rec.t_ep_e1_cust_city_ep_id
   OR     location.t_ep_L_ATT_1_ep_id                        <> rec.t_ep_L_ATT_1_ep_id
   OR     location.t_ep_L_ATT_2_ep_id                        <> rec.t_ep_L_ATT_2_ep_id
   OR     location.t_ep_L_ATT_3_ep_id                        <> rec.t_ep_L_ATT_3_ep_id
   OR     location.t_ep_L_ATT_4_ep_id                        <> rec.t_ep_L_ATT_4_ep_id
   OR     location.t_ep_L_ATT_5_ep_id                        <> rec.t_ep_L_ATT_5_ep_id
   OR     location.t_ep_L_ATT_6_ep_id                        <> rec.t_ep_L_ATT_6_ep_id
   OR     location.t_ep_L_ATT_7_ep_id                        <> rec.t_ep_L_ATT_7_ep_id
   OR     location.t_ep_L_ATT_8_ep_id                        <> rec.t_ep_L_ATT_8_ep_id
   OR     location.t_ep_L_ATT_9_ep_id                        <> rec.t_ep_L_ATT_9_ep_id
   OR     location.t_ep_L_ATT_10_ep_id                       <> rec.t_ep_L_ATT_10_ep_id
   OR     location.t_ep_lr2_ep_id                            <> rec.t_ep_lr2_ep_id
   OR     location.t_ep_lr2a_ep_id                           <> rec.t_ep_lr2a_ep_id
   OR     location.t_ep_ebs_cust_class_ep_id                 <> rec.t_ep_ebs_cust_class_ep_id
   OR     location.t_ep_ebs_customer_ep_id                   <> rec.t_ep_ebs_customer_ep_id
   OR     location.t_ep_ebs_account_ep_id                    <> rec.t_ep_ebs_account_ep_id
   OR     location.t_ep_ebs_zone_ep_id                       <> rec.t_ep_ebs_zone_ep_id
   OR     location.t_ep_ebs_tp_zone_ep_id                    <> rec.t_ep_ebs_tp_zone_ep_id
   OR     location.t_ep_e1_cust_cat_7_ep_id                  <> rec.t_ep_e1_cust_cat_7_ep_id
   OR     location.t_ep_e1_cust_cat_6_ep_id                  <> rec.t_ep_e1_cust_cat_6_ep_id
   OR     location.t_ep_e1_cust_state_ep_id                  <> rec.t_ep_e1_cust_state_ep_id
   OR     location.t_ep_e1_cust_ctry_ep_id                   <> rec.t_ep_e1_cust_ctry_ep_id
   OR     location.t_ep_e1_cust_cat_1_ep_id                  <> rec.t_ep_e1_cust_cat_1_ep_id
   OR     location.t_ep_e1_cust_cat_2_ep_id                  <> rec.t_ep_e1_cust_cat_2_ep_id
   OR     location.t_ep_e1_cust_cat_3_ep_id                  <> rec.t_ep_e1_cust_cat_3_ep_id
   OR     location.t_ep_e1_cust_cat_4_ep_id                  <> rec.t_ep_e1_cust_cat_4_ep_id
   OR     location.t_ep_e1_cust_cat_5_ep_id                  <> rec.t_ep_e1_cust_cat_5_ep_id);

      vs_dim_level_update_count := vs_dim_level_update_count + sql%rowcount;


      IF dv_rowcounter >= dv_sys_rowcounter THEN
         COMMIT;
         dv_rowcounter := 0;
      ELSE
         dv_rowcounter := dv_rowcounter + 1;
      END IF;


   END LOOP;
   COMMIT;

   vs_log_msg := 'Rows Updated in the dimension : '|| vs_dim_level_update_count;                                     LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   ---------------------------------------------------------------------------------------------
   -- Total of the rows MERGED in to location
   ---------------------------------------------------------------------------------------------

   vs_log_msg := 'Rows merged (inserted or updated) in LOCATION : '|| vs_dim_merge_count;        LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   ---------------------------------------------------------------------------------------------
   -- BULK INSERT :
   -- Make a list of all updated dimension rows
   ---------------------------------------------------------------------------------------------
   vs_log_msg := 'BULK INSERT : GLOB_MDP_ADD_LOCATION';                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   vs_sql := '
         INSERT INTO GLOB_MDP_ADD_LOCATION (location_id,
               t_ep_L_ATT_1_ep_id,
               t_ep_L_ATT_10_ep_id,
               t_ep_L_ATT_2_ep_id,
               t_ep_L_ATT_3_ep_id,
               t_ep_L_ATT_4_ep_id,
               t_ep_L_ATT_5_ep_id,
               t_ep_L_ATT_6_ep_id,
               t_ep_L_ATT_7_ep_id,
               t_ep_L_ATT_8_ep_id,
               t_ep_L_ATT_9_ep_id,
               t_ep_e1_br_cat_1_ep_id,
               t_ep_e1_br_cat_2_ep_id,
               t_ep_e1_br_cat_3_ep_id,
               t_ep_e1_br_cat_4_ep_id,
               t_ep_e1_br_cat_5_ep_id,
               t_ep_e1_br_city_ep_id,
               t_ep_e1_br_country_ep_id,
               t_ep_e1_br_state_ep_id,
               t_ep_e1_cust_cat_1_ep_id,
               t_ep_e1_cust_cat_2_ep_id,
               t_ep_e1_cust_cat_3_ep_id,
               t_ep_e1_cust_cat_4_ep_id,
               t_ep_e1_cust_cat_5_ep_id,
               t_ep_e1_cust_cat_6_ep_id,
               t_ep_e1_cust_cat_7_ep_id,
               t_ep_e1_cust_city_ep_id,
               t_ep_e1_cust_ctry_ep_id,
               t_ep_e1_cust_state_ep_id,
               t_ep_ebs_account_ep_id,
               t_ep_ebs_bus_group_ep_id,
               t_ep_ebs_cust_class_ep_id,
               t_ep_ebs_customer_ep_id,
               t_ep_ebs_legal_entity_ep_id,
               t_ep_ebs_oper_unit_ep_id,
               t_ep_ebs_sales_ch_ep_id,
               t_ep_ebs_tp_zone_ep_id,
               t_ep_ebs_zone_ep_id,
               t_ep_lr1_ep_id,
               t_ep_lr2_ep_id,
               t_ep_lr2a_ep_id,
               t_ep_ls1_ep_id,
               t_ep_ls2_ep_id,
               t_ep_ls3_ep_id,
               t_ep_ls4_ep_id,
               t_ep_ls5_ep_id,
               t_ep_ls6_ep_id,
               t_ep_organization_ep_id,
               t_ep_site_ep_id )
   SELECT DISTINCT location_id,
               t_ep_L_ATT_1_ep_id,
               t_ep_L_ATT_10_ep_id,
               t_ep_L_ATT_2_ep_id,
               t_ep_L_ATT_3_ep_id,
               t_ep_L_ATT_4_ep_id,
               t_ep_L_ATT_5_ep_id,
               t_ep_L_ATT_6_ep_id,
               t_ep_L_ATT_7_ep_id,
               t_ep_L_ATT_8_ep_id,
               t_ep_L_ATT_9_ep_id,
               t_ep_e1_br_cat_1_ep_id,
               t_ep_e1_br_cat_2_ep_id,
               t_ep_e1_br_cat_3_ep_id,
               t_ep_e1_br_cat_4_ep_id,
               t_ep_e1_br_cat_5_ep_id,
               t_ep_e1_br_city_ep_id,
               t_ep_e1_br_country_ep_id,
               t_ep_e1_br_state_ep_id,
               t_ep_e1_cust_cat_1_ep_id,
               t_ep_e1_cust_cat_2_ep_id,
               t_ep_e1_cust_cat_3_ep_id,
               t_ep_e1_cust_cat_4_ep_id,
               t_ep_e1_cust_cat_5_ep_id,
               t_ep_e1_cust_cat_6_ep_id,
               t_ep_e1_cust_cat_7_ep_id,
               t_ep_e1_cust_city_ep_id,
               t_ep_e1_cust_ctry_ep_id,
               t_ep_e1_cust_state_ep_id,
               t_ep_ebs_account_ep_id,
               t_ep_ebs_bus_group_ep_id,
               t_ep_ebs_cust_class_ep_id,
               t_ep_ebs_customer_ep_id,
               t_ep_ebs_legal_entity_ep_id,
               t_ep_ebs_oper_unit_ep_id,
               t_ep_ebs_sales_ch_ep_id,
               t_ep_ebs_tp_zone_ep_id,
               t_ep_ebs_zone_ep_id,
               t_ep_lr1_ep_id,
               t_ep_lr2_ep_id,
               t_ep_lr2a_ep_id,
               t_ep_ls1_ep_id,
               t_ep_ls2_ep_id,
               t_ep_ls3_ep_id,
               t_ep_ls4_ep_id,
               t_ep_ls5_ep_id,
               t_ep_ls6_ep_id,
               t_ep_organization_ep_id,
               t_ep_site_ep_id
   FROM   location
   WHERE  last_update_date = '''|| vd_update_date ||'''';
 
   dynamic_ddl(vs_sql);
 
   vs_log_msg := 'Rows inserted in to GLOB_MDP_ADD_LOCATION : '|| sql%rowcount;
   COMMIT;
 
   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

   ----------------------------------------------------------------------------
   -- New MDP_MATRIX MERGE update code being driven only by GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   vs_log_msg := 'MDP_MATRIX MERGE update code being driven only by GLOB_MDP_ADD_LOCATION';                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   MERGE /*+ parallel(m,24)*/  INTO mdp_matrix m
   USING (SELECT /*+ parallel(a,24)*/  DISTINCT
                 location_id,
               t_ep_L_ATT_1_ep_id,
               t_ep_L_ATT_10_ep_id,
               t_ep_L_ATT_2_ep_id,
               t_ep_L_ATT_3_ep_id,
               t_ep_L_ATT_4_ep_id,
               t_ep_L_ATT_5_ep_id,
               t_ep_L_ATT_6_ep_id,
               t_ep_L_ATT_7_ep_id,
               t_ep_L_ATT_8_ep_id,
               t_ep_L_ATT_9_ep_id,
               t_ep_e1_br_cat_1_ep_id,
               t_ep_e1_br_cat_2_ep_id,
               t_ep_e1_br_cat_3_ep_id,
               t_ep_e1_br_cat_4_ep_id,
               t_ep_e1_br_cat_5_ep_id,
               t_ep_e1_br_city_ep_id,
               t_ep_e1_br_country_ep_id,
               t_ep_e1_br_state_ep_id,
               t_ep_e1_cust_cat_1_ep_id,
               t_ep_e1_cust_cat_2_ep_id,
               t_ep_e1_cust_cat_3_ep_id,
               t_ep_e1_cust_cat_4_ep_id,
               t_ep_e1_cust_cat_5_ep_id,
               t_ep_e1_cust_cat_6_ep_id,
               t_ep_e1_cust_cat_7_ep_id,
               t_ep_e1_cust_city_ep_id,
               t_ep_e1_cust_ctry_ep_id,
               t_ep_e1_cust_state_ep_id,
               t_ep_ebs_account_ep_id,
               t_ep_ebs_bus_group_ep_id,
               t_ep_ebs_cust_class_ep_id,
               t_ep_ebs_customer_ep_id,
               t_ep_ebs_legal_entity_ep_id,
               t_ep_ebs_oper_unit_ep_id,
               t_ep_ebs_sales_ch_ep_id,
               t_ep_ebs_tp_zone_ep_id,
               t_ep_ebs_zone_ep_id,
               t_ep_lr1_ep_id,
               t_ep_lr2_ep_id,
               t_ep_lr2a_ep_id,
               t_ep_ls1_ep_id,
               t_ep_ls2_ep_id,
               t_ep_ls3_ep_id,
               t_ep_ls4_ep_id,
               t_ep_ls5_ep_id,
               t_ep_ls6_ep_id,
               t_ep_organization_ep_id,
               t_ep_site_ep_id
          FROM
                GLOB_MDP_ADD_LOCATION a
         ) g
   ON    (m.location_id = g.location_id)
   WHEN MATCHED THEN UPDATE SET m.last_update_date          = SYSTIMESTAMP, 
             m.t_ep_L_ATT_1_ep_id             = g.t_ep_L_ATT_1_ep_id,
             m.t_ep_L_ATT_10_ep_id            = g.t_ep_L_ATT_10_ep_id,
             m.t_ep_L_ATT_2_ep_id             = g.t_ep_L_ATT_2_ep_id,
             m.t_ep_L_ATT_3_ep_id             = g.t_ep_L_ATT_3_ep_id,
             m.t_ep_L_ATT_4_ep_id             = g.t_ep_L_ATT_4_ep_id,
             m.t_ep_L_ATT_5_ep_id             = g.t_ep_L_ATT_5_ep_id,
             m.t_ep_L_ATT_6_ep_id             = g.t_ep_L_ATT_6_ep_id,
             m.t_ep_L_ATT_7_ep_id             = g.t_ep_L_ATT_7_ep_id,
             m.t_ep_L_ATT_8_ep_id             = g.t_ep_L_ATT_8_ep_id,
             m.t_ep_L_ATT_9_ep_id             = g.t_ep_L_ATT_9_ep_id,
             m.t_ep_e1_br_cat_1_ep_id         = g.t_ep_e1_br_cat_1_ep_id,
             m.t_ep_e1_br_cat_2_ep_id         = g.t_ep_e1_br_cat_2_ep_id,
             m.t_ep_e1_br_cat_3_ep_id         = g.t_ep_e1_br_cat_3_ep_id,
             m.t_ep_e1_br_cat_4_ep_id         = g.t_ep_e1_br_cat_4_ep_id,
             m.t_ep_e1_br_cat_5_ep_id         = g.t_ep_e1_br_cat_5_ep_id,
             m.t_ep_e1_br_city_ep_id          = g.t_ep_e1_br_city_ep_id,
             m.t_ep_e1_br_country_ep_id       = g.t_ep_e1_br_country_ep_id,
             m.t_ep_e1_br_state_ep_id         = g.t_ep_e1_br_state_ep_id,
             m.t_ep_e1_cust_cat_1_ep_id       = g.t_ep_e1_cust_cat_1_ep_id,
             m.t_ep_e1_cust_cat_2_ep_id       = g.t_ep_e1_cust_cat_2_ep_id,
             m.t_ep_e1_cust_cat_3_ep_id       = g.t_ep_e1_cust_cat_3_ep_id,
             m.t_ep_e1_cust_cat_4_ep_id       = g.t_ep_e1_cust_cat_4_ep_id,
             m.t_ep_e1_cust_cat_5_ep_id       = g.t_ep_e1_cust_cat_5_ep_id,
             m.t_ep_e1_cust_cat_6_ep_id       = g.t_ep_e1_cust_cat_6_ep_id,
             m.t_ep_e1_cust_cat_7_ep_id       = g.t_ep_e1_cust_cat_7_ep_id,
             m.t_ep_e1_cust_city_ep_id        = g.t_ep_e1_cust_city_ep_id,
             m.t_ep_e1_cust_ctry_ep_id        = g.t_ep_e1_cust_ctry_ep_id,
             m.t_ep_e1_cust_state_ep_id       = g.t_ep_e1_cust_state_ep_id,
             m.t_ep_ebs_account_ep_id         = g.t_ep_ebs_account_ep_id,
             m.t_ep_ebs_bus_group_ep_id       = g.t_ep_ebs_bus_group_ep_id,
             m.t_ep_ebs_cust_class_ep_id      = g.t_ep_ebs_cust_class_ep_id,
             m.t_ep_ebs_customer_ep_id        = g.t_ep_ebs_customer_ep_id,
             m.t_ep_ebs_legal_entity_ep_id    = g.t_ep_ebs_legal_entity_ep_id,
             m.t_ep_ebs_oper_unit_ep_id       = g.t_ep_ebs_oper_unit_ep_id,
             m.t_ep_ebs_sales_ch_ep_id        = g.t_ep_ebs_sales_ch_ep_id,
             m.t_ep_ebs_tp_zone_ep_id         = g.t_ep_ebs_tp_zone_ep_id,
             m.t_ep_ebs_zone_ep_id            = g.t_ep_ebs_zone_ep_id,
             m.t_ep_lr1_ep_id                 = g.t_ep_lr1_ep_id,
             m.t_ep_lr2_ep_id                 = g.t_ep_lr2_ep_id,
             m.t_ep_lr2a_ep_id                = g.t_ep_lr2a_ep_id,
             m.t_ep_ls1_ep_id                 = g.t_ep_ls1_ep_id,
             m.t_ep_ls2_ep_id                 = g.t_ep_ls2_ep_id,
             m.t_ep_ls3_ep_id                 = g.t_ep_ls3_ep_id,
             m.t_ep_ls4_ep_id                 = g.t_ep_ls4_ep_id,
             m.t_ep_ls5_ep_id                 = g.t_ep_ls5_ep_id,
             m.t_ep_ls6_ep_id                 = g.t_ep_ls6_ep_id,
             m.t_ep_organization_ep_id        = g.t_ep_organization_ep_id,
             m.t_ep_site_ep_id                = g.t_ep_site_ep_id,
             m.customer_type_lud              = SYSTIMESTAMP,
             m.t_ep_e1_br_cat_1_lud           = SYSTIMESTAMP,
             m.t_ep_e1_br_cat_2_lud           = SYSTIMESTAMP,
             m.t_ep_e1_br_cat_3_lud           = SYSTIMESTAMP,
             m.t_ep_e1_br_cat_4_lud           = SYSTIMESTAMP,
             m.t_ep_e1_br_cat_5_lud           = SYSTIMESTAMP,
             m.t_ep_e1_br_city_lud            = SYSTIMESTAMP,
             m.t_ep_e1_br_country_lud         = SYSTIMESTAMP,
             m.t_ep_e1_br_state_lud           = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_1_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_2_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_3_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_4_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_5_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_6_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_cat_7_lud         = SYSTIMESTAMP,
             m.t_ep_e1_cust_city_lud          = SYSTIMESTAMP,
             m.t_ep_e1_cust_ctry_lud          = SYSTIMESTAMP,
             m.t_ep_e1_cust_state_lud         = SYSTIMESTAMP,
             m.t_ep_ebs_account_lud           = SYSTIMESTAMP,
             m.t_ep_ebs_bus_group_lud         = SYSTIMESTAMP,
             m.t_ep_ebs_cust_class_lud        = SYSTIMESTAMP,
             m.t_ep_ebs_customer_lud          = SYSTIMESTAMP,
             m.t_ep_ebs_legal_entity_lud      = SYSTIMESTAMP,
             m.t_ep_ebs_oper_unit_lud         = SYSTIMESTAMP,
             m.t_ep_ebs_sales_ch_lud          = SYSTIMESTAMP,
             m.t_ep_ebs_tp_zone_lud           = SYSTIMESTAMP,
             m.t_ep_ebs_zone_lud              = SYSTIMESTAMP,
             m.t_ep_l_att_10_lud              = SYSTIMESTAMP,
             m.t_ep_l_att_1_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_2_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_3_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_4_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_5_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_6_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_7_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_8_lud               = SYSTIMESTAMP,
             m.t_ep_l_att_9_lud               = SYSTIMESTAMP,
             m.t_ep_lob_lud                   = SYSTIMESTAMP,
             m.t_ep_lr1_lud                   = SYSTIMESTAMP,
             m.t_ep_lr2_lud                   = SYSTIMESTAMP,
             m.t_ep_lr2a_lud                  = SYSTIMESTAMP,
             m.t_ep_ls1_lud                   = SYSTIMESTAMP,
             m.t_ep_ls2_lud                   = SYSTIMESTAMP,
             m.t_ep_ls3_lud                   = SYSTIMESTAMP,
             m.t_ep_ls4_lud                   = SYSTIMESTAMP,
             m.t_ep_ls5_lud                   = SYSTIMESTAMP,
             m.t_ep_ls6_lud                   = SYSTIMESTAMP,
             m.t_ep_organization_lud          = SYSTIMESTAMP,
             m.t_ep_site_lud                  = SYSTIMESTAMP,
             m.t_ep_terr_retailer_lud         = SYSTIMESTAMP;
 
   ---------------------------------------------------------------------------------------------
   -- Total of the rows MERGE (updated) in MDP_MATRIX
   ---------------------------------------------------------------------------------------------
   vs_mdp_update_count := sql%rowcount;
   COMMIT;
   vs_log_msg := 'Rows merged (updated) in MDP_MATRIX : '|| vs_mdp_update_count;                                     LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   -------------------------------------------------------------------------------------
   -- Clear down the Global temp table because the Apps Server may not close the session
   -------------------------------------------------------------------------------------
   vs_log_msg := 'TRUNCATE TABLE GLOB_MDP_ADD_LOCATION';
   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);

   dynamic_ddl('TRUNCATE TABLE GLOB_MDP_ADD_LOCATION');


   --------------------------------------------------------------------------
   -- Use Final COMMIT
   --------------------------------------------------------------------------
   COMMIT;

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - ANALYZE ALL THE LEVELS TABLES
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Analyze all the levels tables';                                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   --------------------------------------------------------------------------
   -- ANALYZE ALL THE LEVELS TABLES
   --------------------------------------------------------------------------
   vs_log_msg := 'ANALYZE ALL THE LEVELS TABLES';                                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   
   --IF get_is_table_stats_exists   ('location') = 0 OR
   --   get_is_table_stats_rows_low ('location') = 1 THEN
      analyze_table('location',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_1') = 1 THEN
      analyze_table('t_ep_L_ATT_1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_10') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_10') = 1 THEN
      analyze_table('t_ep_L_ATT_10',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_2') = 1 THEN
      analyze_table('t_ep_L_ATT_2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_3') = 1 THEN
      analyze_table('t_ep_L_ATT_3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_4') = 1 THEN
      analyze_table('t_ep_L_ATT_4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_5') = 1 THEN
      analyze_table('t_ep_L_ATT_5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_6') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_6') = 1 THEN
      analyze_table('t_ep_L_ATT_6',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_7') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_7') = 1 THEN
      analyze_table('t_ep_L_ATT_7',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_8') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_8') = 1 THEN
      analyze_table('t_ep_L_ATT_8',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_L_ATT_9') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_L_ATT_9') = 1 THEN
      analyze_table('t_ep_L_ATT_9',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_cat_1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_cat_1') = 1 THEN
      analyze_table('t_ep_e1_br_cat_1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_cat_2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_cat_2') = 1 THEN
      analyze_table('t_ep_e1_br_cat_2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_cat_3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_cat_3') = 1 THEN
      analyze_table('t_ep_e1_br_cat_3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_cat_4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_cat_4') = 1 THEN
      analyze_table('t_ep_e1_br_cat_4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_cat_5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_cat_5') = 1 THEN
      analyze_table('t_ep_e1_br_cat_5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_city') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_city') = 1 THEN
      analyze_table('t_ep_e1_br_city',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_country') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_country') = 1 THEN
      analyze_table('t_ep_e1_br_country',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_br_state') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_br_state') = 1 THEN
      analyze_table('t_ep_e1_br_state',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_1') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_2') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_3') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_4') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_5') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_6') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_6') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_6',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_cat_7') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_cat_7') = 1 THEN
      analyze_table('t_ep_e1_cust_cat_7',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_city') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_city') = 1 THEN
      analyze_table('t_ep_e1_cust_city',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_ctry') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_ctry') = 1 THEN
      analyze_table('t_ep_e1_cust_ctry',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_cust_state') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_cust_state') = 1 THEN
      analyze_table('t_ep_e1_cust_state',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_account') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_account') = 1 THEN
      analyze_table('t_ep_ebs_account',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_bus_group') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_bus_group') = 1 THEN
      analyze_table('t_ep_ebs_bus_group',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_cust_class') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_cust_class') = 1 THEN
      analyze_table('t_ep_ebs_cust_class',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_customer') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_customer') = 1 THEN
      analyze_table('t_ep_ebs_customer',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_legal_entity') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_legal_entity') = 1 THEN
      analyze_table('t_ep_ebs_legal_entity',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_oper_unit') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_oper_unit') = 1 THEN
      analyze_table('t_ep_ebs_oper_unit',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_sales_ch') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_sales_ch') = 1 THEN
      analyze_table('t_ep_ebs_sales_ch',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_tp_zone') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_tp_zone') = 1 THEN
      analyze_table('t_ep_ebs_tp_zone',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_zone') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_zone') = 1 THEN
      analyze_table('t_ep_ebs_zone',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_lr1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_lr1') = 1 THEN
      analyze_table('t_ep_lr1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_lr2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_lr2') = 1 THEN
      analyze_table('t_ep_lr2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_lr2a') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_lr2a') = 1 THEN
      analyze_table('t_ep_lr2a',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls1') = 1 THEN
      analyze_table('t_ep_ls1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls2') = 1 THEN
      analyze_table('t_ep_ls2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls3') = 1 THEN
      analyze_table('t_ep_ls3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls4') = 1 THEN
      analyze_table('t_ep_ls4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls5') = 1 THEN
      analyze_table('t_ep_ls5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ls6') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ls6') = 1 THEN
      analyze_table('t_ep_ls6',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_organization') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_organization') = 1 THEN
      analyze_table('t_ep_organization',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_site') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_site') = 1 THEN
      analyze_table('t_ep_site',0);
   --END IF;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
 
   vs_log_msg := 'End of procedure';                                                                                 LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
 
   set_module ('E');
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_end_time  := DBMS_UTILITY.get_time;
   vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
   IF vs_status = 'SUCCESS' THEN
 
      vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - location MERGE [ '|| TO_CHAR(NVL(vs_dim_merge_count,0)) ||'] mdp_matrix MERGE ['|| TO_CHAR(NVL( vs_mdp_update_count,
0)) ||'] location Level UPDATE ['|| TO_CHAR(NVL(vs_dim_level_update_count,0)) ||'] T_SRC_LOC_TMPL ['|| TO_CHAR(NVL(vi_stagging_count,0)) ||']';         DBEX( vs_msg, vs_proc, 'T' );
 
   ELSE
 
      vs_msg := 'Procedure Failed    in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - location MERGE [ '|| TO_CHAR(NVL(vs_dim_merge_count,0)) ||'] mdp_matrix MERGE ['|| TO_CHAR(NVL( vs_mdp_update_count,
0)) ||'] location Level UPDATE ['|| TO_CHAR(NVL(vs_dim_level_update_count,0)) ||'] T_SRC_LOC_TMPL ['|| TO_CHAR(NVL(vi_stagging_count,0)) ||']';         DBEX( vs_msg, vs_proc, 'T' );

   END IF;
 END;

/
