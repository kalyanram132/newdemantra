--------------------------------------------------------
--  DDL for Procedure PRC_FORMAT_APO_ERRORS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."PRC_FORMAT_APO_ERRORS" 
IS
/******************************************************************************
   NAME:       RR_DUPLICATE_PROMOTION
   PURPOSE:    Duplicates a specific promotion

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        16/10/2010  Richard Burton/Red Rock Ltd


******************************************************************************/
   SQL_STR                 VARCHAR2 (10000);
   v_prog_name             VARCHAR2 (100)   := 'PRC_FORMAT_APO_ERRORS';
   v_step                  VARCHAR2 (100);
   v_status                VARCHAR2 (100);
   v_proc_log_id           NUMBER;
   
BEGIN
 
   V_STATUS := 'start ';
   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_STATUS || SYSDATE);
   
   dynamic_ddl('truncate table RR_APO_ERR_T');
insert into RR_APO_ERR_T(FIELD_VALUE,ERR_DESC)
select distinct ltrim(replace(LEVEL1,'-30110101',''),'0'),'Material Missing' from BIIO_APO_FCST_ERR where ERR_MESSAGE like '%[ODPM-00075: Invalid Member LEVEL1-Material]%';
commit;

insert into RR_APO_ERR_T(FIELD_VALUE,ERR_DESC)
select distinct ltrim(LEVEL2,'0'),'CL3 missing' from BIIO_APO_FCST_ERR where LOWER(ERR_MESSAGE) like '%level2-%';
COMMIT;

insert into RR_APO_ERR_T(FIELD_VALUE,FIELD_VALUE2,ERR_DESC)
select distinct ltrim(LEVEL2,'0'),ltrim(LEVEL3,'0') ,'CL3-Sales Office Combination Missing' from BIIO_APO_FCST_ERR where LOWER(ERR_MESSAGE) like '%level2,level3%';
commit;

insert into RR_APO_ERR_T(FIELD_VALUE,ERR_DESC)
select distinct ltrim(LEVEL3,'0'),'Sales Office missing' from BIIO_APO_FCST_ERR where LOWER(ERR_MESSAGE) like '%level3-%';
commit;

V_STATUS := 'end ';
V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_STATUS || SYSDATE);

   
EXCEPTION
   WHEN OTHERS THEN
      BEGIN
         dbex ('Fatal Error in Step: '|| v_step|| '. SQLCODE = '|| SQLCODE|| ' '|| SQLERRM,v_prog_name);
         
         v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
         

   COMMIT;   
               
         RAISE;
      END;
      
      
      
END prc_format_apo_errors;

/
