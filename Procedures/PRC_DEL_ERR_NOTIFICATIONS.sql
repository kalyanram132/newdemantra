--------------------------------------------------------
--  DDL for Procedure PRC_DEL_ERR_NOTIFICATIONS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."PRC_DEL_ERR_NOTIFICATIONS" AS
 v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
begin
v_status := 'start ';
V_PROG_NAME := 'PRC_DEL_ERR_NOTIFICATIONS'; 

V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME,  V_STATUS || sysdate);
--dbex (v_step || SYSDATE, v_prog_name);

pre_logon; check_and_drop('rr_tmp_notif_t');

dynamic_ddl('create table rr_tmp_notif_t as select distinct TO_NUMBER(SUBSTR(DESCRIPTION,13,INSTR(DESCRIPTION,chr(11),1)+7)) process_id from portal_task pt where query_id = -1 and message like ''Failure in %''');

dynamic_ddl('create table rr_pt_bkup_'||TO_CHAR (SYSTIMESTAMP, 'mm_dd_yyyy_hh24_mi_ss')||' as select * from portal_task where process_id in (select process_id from rr_tmp_notif_t)');

dynamic_ddl('create table rr_wf_bkup_'||TO_CHAR (SYSTIMESTAMP, 'mm_dd_yyyy_hh24_mi_ss')||' as select * from wf_process_log where process_id in (select process_id from rr_tmp_notif_t)');

dynamic_ddl('delete from portal_task where process_id in (select process_id from rr_tmp_notif_t)');

dynamic_ddl('delete from wf_process_log where process_id in (select process_id from rr_tmp_notif_t)');

COMMIT;

v_status := 'end ';
      V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_STATUS || sysdate);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
             v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name,'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
end PRC_DEL_ERR_NOTIFICATIONS;

/
