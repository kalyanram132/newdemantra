--------------------------------------------------------
--  DDL for Procedure EP_LOAD_SALES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."EP_LOAD_SALES" -- $Revision: 1.200.14.3.2.2 $
   
    (is_tbl_name                 VARCHAR2 DEFAULT NULL,
     is_load_type                VARCHAR2 DEFAULT NULL)
IS
   vs_sql                        VARCHAR2(32766);
   va_sql                        DBMS_SQL.VARCHAR2S;
   vi_counter                    INTEGER;
   vi_result                     INTEGER;
   vs_proc                       VARCHAR2(30) := 'EP_LOAD_SALES';
   vs_msg                        VARCHAR2(2000);
 
   dv_is_exists                  INTEGER;
   dv_pval                       SYS_PARAMS.pval%TYPE;
   vs_pval                       SYS_PARAMS.pval%TYPE;
 
   dv_counter                    INTEGER;
   dv_updfld                     VARCHAR2(30);
   dv_sts                        INTEGER;
   dv_sale_flds_list             VARCHAR2(10000) := 'item_price,base_evt_d_rtl_sd,incr_evt_d_rtl_sd,ebs_sh_req_qty_rd,sdata15,sdata14,sdata13,sdata12,sdata11,sdata10,sdata9,sdata8,sdata7,sdata4,actual_quantity,shelf_price_sd,ebs_sh_ship_qty_rd,
ebs_sh_ship_qty_sd,ebs_bh_req_qty_rd,ebs_bh_book_qty_rd,ebs_bh_book_qty_bd,ebs_bh_req_qty_bd,';
   vs_load_table                 VARCHAR2(30)    := 't_src_sales_tmpl';
 
   dv_sqlupd_1                    VARCHAR2(10000);
   dv_sqlupd_2                    VARCHAR2(10000);
   dv_sqlupd_3                    VARCHAR2(10000);
   dv_sqlupd_4                    VARCHAR2(10000);
   dv_sqlupd_5                    VARCHAR2(10000);
   dv_sqlupd_6                    VARCHAR2(10000);
   dv_sqlupd_7                    VARCHAR2(10000);
 
   dv_updlist                     VARCHAR2(10000);
   dv_acclist                     VARCHAR2(10000);
   dv_rowcounter                  INTEGER;
   dv_sys_rowcounter              INTEGER;
   vs_do_commits                  VARCHAR2(5);
 
   dd_load_sig                    DATE      := SYSTIMESTAMP;
   vi_insert_flag                 NUMBER(1) := 0;
   vd_max_load_date               DATE      := TO_DATE('1/1/1900','mm/dd/yyyy');
   vd_min_load_date               DATE      := SYSTIMESTAMP + (365 * 4);
   vd_max_sales_date              DATE;
   vd_min_sales_date              DATE;
   vd_last_date_backup            DATE;
   vi_item_id                     INTEGER;
   vi_location_id                 INTEGER;
   vi_old_sd_rows                 INTEGER;
   TYPE str_type IS               TABLE OF VARCHAR2(32700) INDEX BY BINARY_INTEGER;
   str                            str_type;
   vi_index                       INTEGER   := 1;
   vi_exists                      INTEGER;
   vi_error_count_5               INTEGER;
   vi_error_count_6               INTEGER;
   vi_error_count_7               INTEGER;
   vi_error_count_8               INTEGER;
   vi_error_count_9               INTEGER;
 
   vs_row_id                      VARCHAR2(20);
 
   TYPE rec1_cursor               IS REF CURSOR;
   rec1                           rec1_cursor;
   vi_sales_merge_count           INTEGER;
   vi_commit_count                INTEGER;
   vi_stagging_count              INTEGER;
 
 
   TYPE cursor_type               IS REF CURSOR;
   rec_load                       cursor_type;
 
   vi_ld_item_id                  NUMBER(20,10);
 
   vi_logging_level               INTEGER;
   vs_log_table                   VARCHAR2(30);
   vs_proc_source                 VARCHAR2(60);
   vi_log_it_commit               INTEGER;
   vs_log_msg                     VARCHAR2(2000);
   vs_temp_proc_name              VARCHAR2(30);
 
   vi_row_count                   INTEGER := 0;
   vd_start_time_proc             NUMBER;
   vd_start_time                  NUMBER;
   vd_end_time                    NUMBER;
 
   vt_procedure                   NUMBER := 0;
   vt_duration                    NUMBER := 0;
   vs_duration                    VARCHAR2(10)   :='';
 
   vs_section                     VARCHAR2(3000) :='';
   vi_sec_log_id                  INTEGER := 0;
   vs_sec_log_id                  VARCHAR2(10);
 
   vs_part_level_column           VARCHAR2(30);
   vs_part_level_source           VARCHAR2(30);
   vs_part_level_column_select    VARCHAR2(20000);
   vs_row_movement                VARCHAR2(30);
 
BEGIN
 
   pre_logon;
 
   set_module ( 'S' );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_start_time_proc := DBMS_UTILITY.get_time;
 
   --------------------------------------------------------------------------
   -- Record the Parameters passed to DB_CALL_LOG
   --------------------------------------------------------------------------
   vs_msg := 'is_tbl_name => '  || NVL( is_tbl_name  ,'NULL') ||', '||
             'is_load_type => ' || NVL( is_load_type ,'NULL');
 
   DBEX(vs_msg, vs_proc, 'C');
 
   --------------------------------------------------------------------------
   -- Initialize LOG_IT
   --------------------------------------------------------------------------
   vs_proc_source   := 'EP_LOAD_SALES';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level, vi_log_it_commit);
 
   vs_log_msg := 'Start of procedure';                                                                               LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'SP', vs_log_msg , 'T' , 'C', NULL);
   vs_log_msg := RPAD('vi_logging_level',31) ||': '|| vi_logging_level;                                            LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg ,  NULL , NULL , NULL);


 
   ----------------------------------------------------------------------------------
   -- Check staging table is not empty
   ----------------------------------------------------------------------------------
   IF is_tbl_name IS NULL THEN
      vs_log_msg := 'Check staging table is not empty';                                                              LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      IF dynamic_number ('SELECT COUNT(*) FROM T_SRC_SALES_TMPL WHERE ROWNUM <= 1') = 0 THEN
         vs_log_msg := 'Sales staging table is empty';                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
         --------------------------------------------------------------------------
         -- Record the start and end times to be recorded in DB_TIMING_LOG
         --------------------------------------------------------------------------
         vd_end_time  := DBMS_UTILITY.get_time;
         vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
 
          vs_section    := vs_log_msg;
          vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
 
          vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - '|| vs_log_msg;        DBEX( vs_msg, vs_proc, 'T' );
          vs_msg := vs_sec_log_id ||'Section Completed in '|| vt_procedure ||' seconds - '|| vs_section;           DBEX( vs_msg, vs_proc, 'S' );
 
          vs_log_msg := 'End of procedure';                                                                          LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );

          set_module ('E');

         RETURN;
      END IF;
   END IF;

 
   ----------------------------------------------------------------------------------
   -- DISABLE triggers
   ----------------------------------------------------------------------------------
   vs_log_msg := 'DISABLE triggers';                                                                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   COMMIT;
   vs_sql     := 'ALTER SESSION ENABLE PARALLEL DML';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);

   dynamic_ddl ( vs_sql );



   IF is_tbl_name IS NOT NULL THEN

      vs_log_msg := RPAD('is_tbl_name',31) ||': '|| is_tbl_name;                                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg ,  NULL , NULL , NULL);

 
      ----------------------------------------------------------------------------------
      -- The load table is the name of the table passed in
      ----------------------------------------------------------------------------------
      vs_load_table := is_tbl_name ;

 
      IF get_is_table_exists ( vs_load_table ||'_err') = 0 THEN
 
         vs_log_msg := 'CREATE TABLE '|| vs_load_table ||'_err';                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
         dynamic_ddl('CREATE TABLE '|| vs_load_table ||'_err
                       AS
                       SELECT *
                       FROM   '|| is_tbl_name ||'
                       WHERE  1 = 2');
      END IF;

      FOR ind IN 2..10 LOOP
         str(ind):='';
      END LOOP;

 
 
      vs_log_msg := 'Building procedure ep_load_'|| is_tbl_name;                                                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      -- Initailise the SQL Array
      sql_array.clear(va_sql);
 
      str(1) := 'CREATE OR REPLACE PROCEDURE ep_load_'|| is_tbl_name ||'
';
      vi_index := 2;
 
      FOR rec IN (SELECT text,line
                  FROM   user_source
                  WHERE  name = 'EP_LOAD_SALES'
                  AND    line > 2
                  ORDER BY line)
      LOOP
         str(vi_index) := str(vi_index) || REPLACE(LOWER(rec.text),'t_src_sales_tmpl',LOWER(is_tbl_name));
 
         IF LENGTH(str(vi_index)) > 30000 THEN
            vi_index := vi_index + 1;
         END IF;
      END LOOP;
 
      FOR ind IN 1..10 LOOP
          sql_array.add(va_sql, str(ind));
      END LOOP;
 
      vs_log_msg := 'Create procedure ep_load_'|| is_tbl_name;                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      sql_array.run(va_sql);
 
      vs_log_msg := 'Compile procedure ep_load_'|| is_tbl_name;                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dynamic_ddl('ALTER PROCEDURE ep_load_'||is_tbl_name||' COMPILE ');
 
 
      vs_log_msg := 'Running procedure ep_load_'|| is_tbl_name;                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dynamic_ddl(' BEGIN ep_load_'||is_tbl_name||'; END; ');
 
      IF vs_load_table <> is_tbl_name THEN
 
         vs_log_msg := 'Dropping table '|| vs_load_table;                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
         check_and_drop ( vs_load_table );
 
      END IF;
      vs_log_msg := 'Dropping procedure ep_load_'|| is_tbl_name;                                                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
      check_and_drop ('ep_load_'||is_tbl_name);

      RETURN;
   END IF;

 
 
   --------------------------------------------------------------------------
   -- Stagging table count
   --------------------------------------------------------------------------
   vs_sql := 'SELECT COUNT(*) FROM T_SRC_SALES_TMPL';

   vi_stagging_count := dynamic_number ( vs_sql );
   vi_stagging_count := NVL(vi_stagging_count,0);

 
   add_index ( 'T_SRC_SALES_TMPL' , 'T_SRC_SALES_TMPL_SD_IND', 'sales_date' , 'NONUNIQUE', 'BTREE', null );
   add_index ( 'T_SRC_SALES_TMPL' , 'T_SRC_SALES_TMPL_Q_IND' , 'actual_qty' , 'NONUNIQUE', 'BTREE', null );
 
   ----------------------------------------------------------------------------------------------------
   -- Only get new T_SRC_ stats if there are none there to start with or NUM_ROWS are low
   ----------------------------------------------------------------------------------------------------
 
   IF get_is_table_stats_exists   ('T_SRC_SALES_TMPL') = 0 OR
      get_is_table_stats_rows_low ('T_SRC_SALES_TMPL') = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE TABLE T_SRC_SALES_TMPL';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('T_SRC_SALES_TMPL',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;


 
   ----------------------------------------------------------------------------------------------------
   -- Analyze the T_SRC table only when they have no stats to start with or NUM_ROWS are low
   ----------------------------------------------------------------------------------------------------

 
   IF get_is_table_stats_exists   ('T_SRC_LOC_TMPL') = 0 OR
      get_is_table_stats_rows_low ('T_SRC_LOC_TMPL') = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE TABLE T_SRC_LOC_TMPL';                                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('T_SRC_LOC_TMPL',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;

 
   IF get_is_table_stats_exists   ('T_SRC_ITEM_TMPL') = 0 OR
      get_is_table_stats_rows_low ('T_SRC_ITEM_TMPL') = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE TABLE T_SRC_ITEM_TMPL';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('T_SRC_ITEM_TMPL',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;
 
 
   -----------------------------------------------------------------------
   -- Set the commit control
   -----------------------------------------------------------------------
 
   get_param('SYS_PARAMS','dv_pval_max_recs',dv_sys_rowcounter,10000);
 
   vs_do_commits := 'TRUE';
 
   get_param('db_params','ep_load_do_commits',vs_pval);
 
   IF vs_pval IS NOT NULL THEN
 
      IF UPPER(vs_pval) = 'FALSE' THEN

         vs_do_commits := 'FALSE';

      END IF;
   END IF;

   vs_log_msg := 'vs_do_commits : '|| vs_do_commits ;     LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg ,  NULL , NULL , NULL);

 
 
 
   -----------------------------------------------------------------------
   -- Set the SALES_DATA update method to UPDATE or ACCUMULATE
   -----------------------------------------------------------------------
 
   get_param('SYS_PARAMS','accumulatedOrUpdate',dv_pval,'UPDATE');
 
   IF UPPER(dv_pval)!= 'UPDATE' THEN
 
      vs_log_msg := 'accumulatedOrUpdate : ACCUMULATE';                                                              LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dv_sale_flds_list := REPLACE(dv_sale_flds_list, CHR(10), '');
      dv_sale_flds_list := REPLACE(dv_sale_flds_list, CHR(13), '');
 
      dv_counter := 1;
      LOOP
         parsestringbydelimiter(dv_sale_flds_list,',',dv_counter,dv_updfld,dv_sts);
 
         IF dv_sts = 0 THEN
            EXIT;
         END IF;
 
         SELECT COUNT(*)
         INTO   dv_is_exists
         FROM   computed_fields
         WHERE  LOWER(LTRIM(RTRIM(dbname))) = LOWER(dv_updfld);
 
         IF dv_is_exists = 0 THEN
            dv_updlist := dv_updlist || dv_updfld ||',';
         ELSE
 
            -----------------------------------------------------------------------
            -- Check is the series has a 'proportion' flag set
            -----------------------------------------------------------------------
            SELECT SUM(NVL(is_proportion,0))
            INTO   dv_is_exists
            FROM   computed_fields
            WHERE  LOWER(LTRIM(RTRIM(dbname))) = LOWER(dv_updfld);
 
            IF dv_is_exists >= 1 THEN
 
               -----------------------------------------------------------------------
               -- Proportional Series Found : Add column to accumulative list
               -----------------------------------------------------------------------
               dv_acclist := dv_acclist || dv_updfld ||',';
            ELSE
 
               -----------------------------------------------------------------------
               -- No Proportional Series Found : Add column to update list
               -----------------------------------------------------------------------
               dv_updlist := dv_updlist || dv_updfld || ',';
            END IF;

         END IF;
         dv_counter := dv_counter + 1;
      END LOOP;
   END IF;
  
 
   --------------------------------------------------------------------------------
   -- Prepare the load table
   --------------------------------------------------------------------------------
   vs_log_msg := 'Prepare the load table - DROP PK COLUMNS';                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------------
   -- Remove the load table PK
   --------------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.drop_primary_key('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX','NO COLUMNS');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_FI'    ,'NO COLUMNS');
 
   helper.drop_column('t_src_sales_tmpl', 'LD_ITEM_ID');
   helper.drop_column('t_src_sales_tmpl', 'LD_LOCATION_ID');
   helper.drop_column('t_src_sales_tmpl', 'AGGRE_SD');
 
   -- If table is partitioned then add the partition level column
   IF vs_part_level_column IS NOT NULL THEN
      helper.drop_column('t_src_sales_tmpl', vs_part_level_column);
   END IF;
 
   --------------------------------------------------------------------------
   -- Drop unused columns from the load table
   --------------------------------------------------------------------------
   vs_sql := 'ALTER TABLE t_src_sales_tmpl DROP UNUSED COLUMNS';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level , 2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl(vs_sql);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;                   vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
 
   --------------------------------------------
   -- Create the load table functional index
   --------------------------------------------
 
   IF get_is_index_name_exists('T_SRC_SALES_TMPL','T_SRC_SALES_TMPL_FI') = 0 THEN
 
      vs_log_msg := 'CREATE INDEX T_SRC_SALES_TMPL_FI ON T_SRC_SALES_TMPL';                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
      vs_msg     := vs_log_msg;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'CREATE INDEX T_SRC_SALES_TMPL_FI ON T_SRC_SALES_TMPL (lower(dm_item_code),lower(ebs_demand_class_code),lower(t_ep_p1),lower(ebs_sales_channel_code),lower(dm_org_code),lower(dm_site_code),lower(t_ep_ls1),lower(t_ep_lr1))';
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dynamic_ddl(vs_sql, vs_proc);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_msg ;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );
 
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE TABLE t_src_sales_tmpl';                                                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('t_src_sales_tmpl',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;
 
 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Error Check : DATE IS OUT OF RANGE
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_log_msg := 'Error Check : DATE IS OUT OF RANGE - ERR INSERT';                                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_err
              SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,5,'''|| dd_load_sig ||''',''DATE IS OUT OF RANGE''
              FROM   T_SRC_SALES_TMPL
              WHERE  t_src_sales_tmpl.sales_date <(SELECT MIN(datet) FROM INPUTS)
              OR     t_src_sales_tmpl.sales_date >(SELECT MAX(datet) FROM INPUTS)';
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_error_count_5 := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' Rows ['|| vi_error_count_5 ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;                   vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_err : '|| vi_error_count_5;                       LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
   IF vi_error_count_5 > 0 THEN
 
      vs_log_msg := 'Error Check : DATE IS OUT OF RANGE - DELETE';                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'DELETE T_SRC_SALES_TMPL
                 WHERE  t_src_sales_tmpl.sales_date <(SELECT MIN(datet) FROM INPUTS)
                 OR     t_src_sales_tmpl.sales_date >(SELECT MAX(datet) FROM INPUTS)';
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' DELETE ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

      vs_log_msg := 'Rows deleted from T_SRC_SALES_TMPL : '|| vi_row_count;                              LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

   END IF;

   COMMIT;
   
 
   ---------------------------------------------------------------------------------------
   ---------------------------------------------------------------------------------------
   -- Error Check : ITEM OR LOCATION IN SALES source does NOT exist IN ITEMS/LOCATION
   ---------------------------------------------------------------------------------------
   ---------------------------------------------------------------------------------------
   EXECUTE IMMEDIATE 'TRUNCATE TABLE T_SRC_SALES_TMPL_TMP';
 
   ---------------------------------------------------------------------------------------
   -- Check Error Data : Sales levels must match with existing data in the level tables
   ---------------------------------------------------------------------------------------

 
   ---------------------------------------------------------------------------------------
   -- Insert to the _ERR table Invalid ITEMS
   ---------------------------------------------------------------------------------------
   vs_log_msg := 'Error Check : ITEM IN SALES source does NOT exist IN ITEMS - TMP INSERT';                          LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --======================================================================================================
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_TMP (
                 SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,6 ,'''|| dd_load_sig ||''', ''ITEM IN SALES source does NOT exist IN ITEMS'', ROWID
                 FROM   T_SRC_SALES_TMPL
              MINUS
                 SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,6 ,'''|| dd_load_sig ||''', ''ITEM IN SALES source does NOT exist IN ITEMS'', T_SRC_SALES_TMPL.ROWID
                 FROM   T_SRC_SALES_TMPL, ITEMS , t_ep_item, t_ep_ebs_demand_class, t_ep_p1
                 WHERE  1 = 1 
                 AND    items.t_ep_item_ep_id                = t_ep_item.t_ep_item_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.dm_item_code) = LOWER(t_ep_item.item)
                 AND    items.t_ep_ebs_demand_class_ep_id    = t_ep_ebs_demand_class.t_ep_ebs_demand_class_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.ebs_demand_class_code) = LOWER(t_ep_ebs_demand_class.ebs_demand_class)
                 AND    items.t_ep_p1_ep_id                  = t_ep_p1.t_ep_p1_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.t_ep_p1) = LOWER(t_ep_p1.p1))';
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_error_count_6  := sql%rowcount;
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' ['|| vi_error_count_6 ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_TMP : '|| vi_error_count_6;                       LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
 
   IF vi_error_count_6 > 0 THEN
 
      vs_log_msg := 'Error Check : ITEM IN SALES source does NOT exist IN ITEMS - ERR INSERT';                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_ERR
                 SELECT   T_EP_P1,T_EP_LR1,T_EP_LS1,ACTUAL_QTY,BASE_EVT_DOL_RTL,INCR_EVT_DOL_RTL,ITEM_PRICE,SALES_DATE,SDATA10,SDATA11,SDATA12,SDATA13,SDATA14,SDATA4,SDATA5,SDATA6,SDATA7,SDATA8,SDATA9,SHELF_PRICE_SD,T_EP_M1,T_EP_M2,SDATA15,DM_ITEM_CODE,
DM_ORG_CODE,DM_SITE_CODE,E1_ITEM_BRANCH_CATEGORY_1,E1_ITEM_BRANCH_CATEGORY_2,E1_ITEM_BRANCH_CATEGORY_3,E1_ITEM_BRANCH_CATEGORY_4,E1_ITEM_BRANCH_CATEGORY_5,E1_ITEM_BRANCH_CATEGORY_6,E1_ITEM_BRANCH_CATEGORY_7,E1_ITEM_BRANCH_CATEGORY_8,
E1_ITEM_BRANCH_CATEGORY_9,E1_ITEM_BRANCH_CATEGORY_10,E1_ITEM_BRANCH_CATEGORY_11,E1_ITEM_BRANCH_CATEGORY_12,E1_ITEM_BRANCH_CATEGORY_13,E1_ITEM_BRANCH_CATEGORY_14,E1_ITEM_BRANCH_CATEGORY_15,E1_ITEM_BRANCH_CATEGORY_16,E1_ITEM_BRANCH_CATEGORY_17,
E1_ITEM_BRANCH_CATEGORY_18,E1_ITEM_BRANCH_CATEGORY_19,E1_ITEM_BRANCH_CATEGORY_20,E1_ITEM_BRANCH_CATEGORY_21,E1_ITEM_BRANCH_CATEGORY_22,E1_ITEM_BRANCH_CATEGORY_23,EBS_DEMAND_CLASS_CODE,EBS_SALES_CHANNEL_CODE,EBS_BOOK_HIST_BOOK_QTY_BD,
EBS_BOOK_HIST_REQ_QTY_BD,EBS_BOOK_HIST_BOOK_QTY_RD,EBS_BOOK_HIST_REQ_QTY_RD,EBS_SHIP_HIST_SHIP_QTY_SD,EBS_SHIP_HIST_SHIP_QTY_RD,EBS_SHIP_HIST_REQ_QTY_RD,EBS_ITEM_SR_PK,EBS_ORG_SR_PK,EBS_SITE_SR_PK,EBS_DEMAND_CLASS_SR_PK,EBS_SALES_CHANNEL_SR_PK,
BDF_BASE_RATE_CORP,BDF_DEV_RATE_CORP,BDF_FIXED_FUNDS_CORP,EBSPRICELIST100,EBSPRICELIST101,EBSPRICELIST102,EBSPRICELIST104,ERROR_CODE_RECORD,LOAD_DATE,ERROR_MESSAGE_RECORD
                 FROM     T_SRC_SALES_TMPL_TMP';
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;                vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );
 
      vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_ERR : '|| vi_row_count;                        LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
      ---------------------------------------------------------------------------------------
      -- Delete the error data from the T_SRC table
      ---------------------------------------------------------------------------------------
      vs_log_msg := 'Error Check : ITEM IN SALES source does NOT exist IN ITEMS - DELETE';                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vi_row_count := 0;
 
      OPEN rec1 FOR
            'SELECT DISTINCT row_id FROM T_SRC_SALES_TMPL_TMP';
      LOOP
 
         FETCH rec1 INTO vs_row_id;
         EXIT WHEN rec1%NOTFOUND;
 
         dynamic_ddl('DELETE T_SRC_SALES_TMPL WHERE ROWID = '''|| vs_row_id ||'''');
 
         vi_row_count := vi_row_count + sql%rowcount;
 
      END LOOP;
      CLOSE rec1;
 
      COMMIT;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );
 
      vs_log_msg := 'Rows deleted from T_SRC_SALES_TMPL : '|| vi_row_count;                              LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


      EXECUTE IMMEDIATE 'TRUNCATE TABLE T_SRC_SALES_TMPL_TMP';

   END IF;

 
   ---------------------------------------------------------------------------------------
   -- Insert to the _ERR table Invalid LOCATIONS
   ---------------------------------------------------------------------------------------
   vs_log_msg := 'Error Check : LOCATION in SALES source do NOT exist IN LOCATION - TMP INSERT';                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --======================================================================================================
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_TMP (
                 SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,9 ,'''|| dd_load_sig ||''', ''LOCATION IN SALES source does NOT exist IN LOCATION'', ROWID
                 FROM   T_SRC_SALES_TMPL
              MINUS
                 SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,9 ,'''|| dd_load_sig ||''', ''LOCATION IN SALES source does NOT exist IN LOCATION'', T_SRC_SALES_TMPL.ROWID
                 FROM   T_SRC_SALES_TMPL, LOCATION , t_ep_ebs_sales_ch, t_ep_organization, t_ep_site, t_ep_ls1, t_ep_lr1
                 WHERE  1 = 1 
                 AND    location.t_ep_ebs_sales_ch_ep_id        = t_ep_ebs_sales_ch.t_ep_ebs_sales_ch_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.ebs_sales_channel_code) = LOWER(t_ep_ebs_sales_ch.ebs_sales_ch)
                 AND    location.t_ep_organization_ep_id        = t_ep_organization.t_ep_organization_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.dm_org_code) = LOWER(t_ep_organization.organization)
                 AND    location.t_ep_site_ep_id                = t_ep_site.t_ep_site_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.dm_site_code) = LOWER(t_ep_site.site)
                 AND    location.t_ep_ls1_ep_id                 = t_ep_ls1.t_ep_ls1_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.t_ep_ls1) = LOWER(t_ep_ls1.ls1)
                 AND    location.t_ep_lr1_ep_id                 = t_ep_lr1.t_ep_lr1_ep_id
                 AND    LOWER(T_SRC_SALES_TMPL.t_ep_lr1) = LOWER(t_ep_lr1.lr1))';
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_error_count_9  := sql%rowcount;
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' ['|| vi_error_count_9 ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_TMP : '|| vi_error_count_9;                       LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
 
   IF vi_error_count_9 > 0 THEN
 
      vs_log_msg := 'Error Check : LOCATION IN SALES source does NOT exist IN LOCATION - ERR INSERT';                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_ERR
                 SELECT   T_EP_P1,T_EP_LR1,T_EP_LS1,ACTUAL_QTY,BASE_EVT_DOL_RTL,INCR_EVT_DOL_RTL,ITEM_PRICE,SALES_DATE,SDATA10,SDATA11,SDATA12,SDATA13,SDATA14,SDATA4,SDATA5,SDATA6,SDATA7,SDATA8,SDATA9,SHELF_PRICE_SD,T_EP_M1,T_EP_M2,SDATA15,DM_ITEM_CODE,
DM_ORG_CODE,DM_SITE_CODE,E1_ITEM_BRANCH_CATEGORY_1,E1_ITEM_BRANCH_CATEGORY_2,E1_ITEM_BRANCH_CATEGORY_3,E1_ITEM_BRANCH_CATEGORY_4,E1_ITEM_BRANCH_CATEGORY_5,E1_ITEM_BRANCH_CATEGORY_6,E1_ITEM_BRANCH_CATEGORY_7,E1_ITEM_BRANCH_CATEGORY_8,
E1_ITEM_BRANCH_CATEGORY_9,E1_ITEM_BRANCH_CATEGORY_10,E1_ITEM_BRANCH_CATEGORY_11,E1_ITEM_BRANCH_CATEGORY_12,E1_ITEM_BRANCH_CATEGORY_13,E1_ITEM_BRANCH_CATEGORY_14,E1_ITEM_BRANCH_CATEGORY_15,E1_ITEM_BRANCH_CATEGORY_16,E1_ITEM_BRANCH_CATEGORY_17,
E1_ITEM_BRANCH_CATEGORY_18,E1_ITEM_BRANCH_CATEGORY_19,E1_ITEM_BRANCH_CATEGORY_20,E1_ITEM_BRANCH_CATEGORY_21,E1_ITEM_BRANCH_CATEGORY_22,E1_ITEM_BRANCH_CATEGORY_23,EBS_DEMAND_CLASS_CODE,EBS_SALES_CHANNEL_CODE,EBS_BOOK_HIST_BOOK_QTY_BD,
EBS_BOOK_HIST_REQ_QTY_BD,EBS_BOOK_HIST_BOOK_QTY_RD,EBS_BOOK_HIST_REQ_QTY_RD,EBS_SHIP_HIST_SHIP_QTY_SD,EBS_SHIP_HIST_SHIP_QTY_RD,EBS_SHIP_HIST_REQ_QTY_RD,EBS_ITEM_SR_PK,EBS_ORG_SR_PK,EBS_SITE_SR_PK,EBS_DEMAND_CLASS_SR_PK,EBS_SALES_CHANNEL_SR_PK,
BDF_BASE_RATE_CORP,BDF_DEV_RATE_CORP,BDF_FIXED_FUNDS_CORP,EBSPRICELIST100,EBSPRICELIST101,EBSPRICELIST102,EBSPRICELIST104,ERROR_CODE_RECORD,LOAD_DATE,ERROR_MESSAGE_RECORD
                 FROM     T_SRC_SALES_TMPL_TMP';
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );
 
      vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_ERR : '|| vi_row_count;                        LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
      ---------------------------------------------------------------------------------------
      -- Delete the error data from the T_SRC table
      ---------------------------------------------------------------------------------------
      vs_log_msg := 'Error Check : LOCATION IN SALES source does NOT exist IN LOCATION - DELETE';                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vi_row_count := 0;
 
      OPEN rec1 FOR
            'SELECT DISTINCT row_id FROM T_SRC_SALES_TMPL_TMP';
      LOOP
 
         FETCH rec1 INTO vs_row_id;
         EXIT WHEN rec1%NOTFOUND;
 
         dynamic_ddl('DELETE T_SRC_SALES_TMPL WHERE ROWID = '''|| vs_row_id ||'''');
 
         vi_row_count := vi_row_count + sql%rowcount;
 
      END LOOP;
      CLOSE rec1;
 
      COMMIT;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;      DBEX( vs_msg, vs_proc, 'S' );
 
      vs_log_msg := 'Rows deleted from T_SRC_SALES_TMPL : '|| vi_row_count;                           LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


      EXECUTE IMMEDIATE 'TRUNCATE TABLE T_SRC_SALES_TMPL_TMP';

   END IF;

 
   ---------------------------------------------------------------------------------------
   ---------------------------------------------------------------------------------------
   -- Error Check : SALES_DATE cannot be NULL
   ---------------------------------------------------------------------------------------
   ---------------------------------------------------------------------------------------
   vs_log_msg := 'Error Check : SALES_DATE cannot be NULL - ERR INSERT';                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'INSERT INTO T_SRC_SALES_TMPL_err (
              SELECT /*+ parallel(T_SRC_SALES_TMPL, 24) full(T_SRC_SALES_TMPL)*/ T_SRC_SALES_TMPL.*,7 ,'''|| dd_load_sig ||''', ''SALES_DATE cannot be NULL''
              FROM   T_SRC_SALES_TMPL
              WHERE  t_src_sales_tmpl.sales_date IS NULL)';
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_error_count_7 := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' ['|| vi_error_count_7 ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   vs_log_msg := 'Rows inserted in to T_SRC_SALES_TMPL_err : '|| vi_error_count_7;                       LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);
 
 
   IF vi_error_count_7 > 0 THEN
 
      vs_log_msg := 'Error Check : SALES_DATE cannot be NULL - DELETE';                                              LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'DELETE
                 FROM  T_SRC_SALES_TMPL
                 WHERE t_src_sales_tmpl.sales_date IS NULL';
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

      vs_log_msg := 'Rows deleted from T_SRC_SALES_TMPL : '||vi_row_count;                               LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

   END IF;
   COMMIT;
   
 
   -----------------------------------------------------------
   -- Add the internal PK columns to the load table
   -----------------------------------------------------------
 
   -- Get LIST partition column name if SALES_DATA is partitioned.
   -- and on which side the partitionin level is (item or location).
 
   BEGIN
 
      SELECT UPPER(id_field),      UPPER(search_table)
      INTO   vs_part_level_column, vs_part_level_source
      FROM   group_tables
      WHERE  UPPER(search_table) IN ('ITEMS','LOCATION')
      AND    group_table_id =
         (SELECT sub_level_id
          FROM   partition_profiles pp, partition_tables pt
          WHERE  pp.profile_id = pt.profile_id
          AND    pp.is_active  = 1
          AND    pt.table_name = 'BRANCH_DATA'
          AND    pt.is_data    = 1)
      AND get_is_column_exists( search_table  , id_field) = 1
      AND get_is_column_exists( 'SALES_DATA', id_field) = 1;
 
      IF vs_part_level_column IS NOT NULL THEN
 
         SELECT row_movement
         INTO   vs_row_movement
         FROM   user_tables
         WHERE  table_name = 'SALES_DATA';

         IF vs_row_movement = 'DISABLED' THEN
            EXECUTE IMMEDIATE 'ALTER TABLE SALES_DATA ENABLE ROW MOVEMENT';
         END IF;

      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   END;

 
   --------------------------------------------------------------------------------
   -- Prepare the load table
   --------------------------------------------------------------------------------
   vs_log_msg := 'Prepare the load table - DROP PK COLUMNS';                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   --------------------------------------------------------------------------------
   -- Remove the load table PK and indexes
   --------------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.drop_primary_key('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX','NO COLUMNS');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_FI'    ,'NO COLUMNS');
 
   helper.drop_column('t_src_sales_tmpl', 'LD_ITEM_ID');
   helper.drop_column('t_src_sales_tmpl', 'LD_LOCATION_ID');

   -- If table is partitioned then add the partition level column
   IF vs_part_level_column IS NOT NULL THEN
      helper.drop_column('t_src_sales_tmpl', vs_part_level_column);
   END IF;


 
   --------------------------------------------------------------------------------
   -- Remove the AGGRE_SD load table PK
   --------------------------------------------------------------------------------
   helper.drop_column('t_src_sales_tmpl', 'AGGRE_SD');

 
   --------------------------------------------------------------------------
   -- Drop unused columns from the load table
   --------------------------------------------------------------------------
   vs_sql := 'ALTER TABLE t_src_sales_tmpl DROP UNUSED COLUMNS';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level , 2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);

   dynamic_ddl(vs_sql);

 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;                   vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );


 
   -----------------------------------------------------------------------------------------------------
   -- Only get new stats if there are none there to start with or NUM_ROWS are low
   -----------------------------------------------------------------------------------------------------
 
   IF get_is_table_stats_exists   (vs_load_table) = 0 OR
      get_is_table_stats_rows_low (vs_load_table) = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE TABLE t_src_sales_tmpl';                                                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('t_src_sales_tmpl',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;


 
 
   -----------------------------------------------------------
   -- Add the AGGRE_SD column to the load table
   -----------------------------------------------------------
   vs_log_msg := 'Add the AGGRE_SD column to the load table';                                                        LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.add_column('t_src_sales_tmpl', 'AGGRE_SD'    , 'DATE',NULL,NULL);
 
   -----------------------------------------------------------
   -- Add the internal PK columns to the load table
   -----------------------------------------------------------
   vs_log_msg := 'Add the internal PK columns to the load table';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   -----------------------------------------------------------
   -- Add the LD_ITEM_ID column to the load table
   -----------------------------------------------------------
   vs_log_msg := 'Add the LD_ITEM_ID column to the load table';                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.add_column('t_src_sales_tmpl', 'LD_ITEM_ID'    , 'NUMBER(10)',NULL,NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   -----------------------------------------------------------
   -- Add the LD_LOCATION_ID column to the load table
   -----------------------------------------------------------
   vs_log_msg := 'Add the LD_LOCATION_ID column to the load table';                                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.add_column('t_src_sales_tmpl', 'LD_LOCATION_ID', 'NUMBER(10)',NULL,NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
 
   -----------------------------------------------------------
   -- If table is partitioned then add the partition level column
   -----------------------------------------------------------
   IF vs_part_level_column IS NOT NULL THEN
 
      vs_log_msg := 'Table is partitioned then add the partition level column';                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := 'Add the '|| vs_part_level_column ||' column to the load table';                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      helper.add_column('t_src_sales_tmpl', vs_part_level_column, 'NUMBER(10)',NULL,NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;


 
   --------------------------------------------------------------------------
   -- Analyze the load table if needed
   --------------------------------------------------------------------------
   IF get_is_table_stats_exists   ('t_src_sales_tmpl' ) = 0 OR
      get_is_table_stats_rows_low ('t_src_sales_tmpl' ) = 1 THEN
 
      vs_log_msg := 'analyze_table t_src_sales_tmpl';                                                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      analyze_table( UPPER('t_src_sales_tmpl') ,0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;

 
 
   -----------------------------------------------------------------------------
   -- Create table ld_item_base
   -----------------------------------------------------------------------------
   check_and_drop('ld_item_base');
 
   dynamic_ddl('CREATE TABLE ld_item_base ( item_id NUMBER(10), item VARCHAR2(240) , ebs_demand_class VARCHAR2(240) , p1 VARCHAR2(100)  )');
 
   vs_log_msg := 'Create table ld_item_base';                                                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'INSERT INTO ld_item_base (item_id, item, ebs_demand_class, p1)
              SELECT DISTINCT
                     items.item_id,
                     LOWER(t_ep_item.item),
                     LOWER(t_ep_ebs_demand_class.ebs_demand_class),
                     LOWER(t_ep_p1.p1)
              FROM   items, t_ep_item, t_ep_ebs_demand_class, t_ep_p1
              WHERE  1 = 1 
              AND    items.t_ep_item_ep_id                = t_ep_item.t_ep_item_ep_id
              AND    items.t_ep_ebs_demand_class_ep_id    = t_ep_ebs_demand_class.t_ep_ebs_demand_class_ep_id
              AND    items.t_ep_p1_ep_id                  = t_ep_p1.t_ep_p1_ep_id';
 
   dynamic_ddl ( vs_sql );
 
   vi_row_count := sql%rowcount;
   commit;
 
   dynamic_ddl('CREATE UNIQUE INDEX ld_item_base_uk ON ld_item_base (  item, ebs_demand_class, p1 )');
 
   analyze_table('ld_item_base',0);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows inserted in ld_item_base : '|| vi_row_count;                                          LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

 
   -----------------------------------------------------------------------------
   -- Create table ld_loc_base
   -----------------------------------------------------------------------------
   check_and_drop('ld_loc_base');
 
   dynamic_ddl('CREATE TABLE ld_loc_base ( location_id NUMBER(10), ebs_sales_ch VARCHAR2(240) , organization VARCHAR2(240) , site VARCHAR2(240) , ls1 VARCHAR2(100) , lr1 VARCHAR2(100)  )');
 
   vs_log_msg := 'Create table ld_loc_base';                                                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'INSERT INTO ld_loc_base (location_id, ebs_sales_ch, organization, site, ls1, lr1 )
              SELECT DISTINCT
                     location.location_id,
                     LOWER(t_ep_ebs_sales_ch.ebs_sales_ch),
                     LOWER(t_ep_organization.organization),
                     LOWER(t_ep_site.site),
                     LOWER(t_ep_ls1.ls1),
                     LOWER(t_ep_lr1.lr1)
              FROM   location, t_ep_ebs_sales_ch, t_ep_organization, t_ep_site, t_ep_ls1, t_ep_lr1
              WHERE  1 = 1 
              AND    location.t_ep_ebs_sales_ch_ep_id        = t_ep_ebs_sales_ch.t_ep_ebs_sales_ch_ep_id
              AND    location.t_ep_organization_ep_id        = t_ep_organization.t_ep_organization_ep_id
              AND    location.t_ep_site_ep_id                = t_ep_site.t_ep_site_ep_id
              AND    location.t_ep_ls1_ep_id                 = t_ep_ls1.t_ep_ls1_ep_id
              AND    location.t_ep_lr1_ep_id                 = t_ep_lr1.t_ep_lr1_ep_id';
 
   dynamic_ddl ( vs_sql );
 
   vi_row_count := sql%rowcount;
   commit;
 
   dynamic_ddl('CREATE UNIQUE INDEX ld_loc_base_uk ON ld_loc_base (  ebs_sales_ch, organization, site, ls1, lr1 )');
 
   analyze_table('ld_loc_base',0);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows inserted in ld_loc_base : '|| vi_row_count;                                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

 
   -----------------------------------------------------------------------------
   -- Update the LD_ITEM_ID column
   -----------------------------------------------------------------------------
   vs_log_msg := 'Update the LD_ITEM_ID column';                                                                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    LD_ITEM_ID     = (SELECT ld_item_base.item_id
                                       FROM   ld_item_base
                                       WHERE  1 = 1                                       
                                       AND    LOWER(t_src_sales_tmpl.dm_item_code ) = ld_item_base.item
                                       AND    LOWER(t_src_sales_tmpl.ebs_demand_class_code ) = ld_item_base.ebs_demand_class
                                       AND    LOWER(t_src_sales_tmpl.t_ep_p1 ) = ld_item_base.p1)';
 
   vs_log_msg := SUBSTR(vs_sql,1   ,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_log_msg := SUBSTR(vs_sql,2001,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows updated in t_src_sales_tmpl : '|| vi_row_count;                                          LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   -----------------------------------------------------------------------------
   -- Update the LD_LOCATION_ID column
   -----------------------------------------------------------------------------
   vs_log_msg := 'Update the LD_LOCATION_ID column';                                                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    LD_LOCATION_ID = (SELECT ld_loc_base.location_id
                                       FROM   ld_loc_base
                                       WHERE  1 = 1                                        
                                       AND    LOWER(t_src_sales_tmpl.ebs_sales_channel_code ) = ld_loc_base.ebs_sales_ch
                                       AND    LOWER(t_src_sales_tmpl.dm_org_code ) = ld_loc_base.organization
                                       AND    LOWER(t_src_sales_tmpl.dm_site_code ) = ld_loc_base.site
                                       AND    LOWER(t_src_sales_tmpl.t_ep_ls1 ) = ld_loc_base.ls1
                                       AND    LOWER(t_src_sales_tmpl.t_ep_lr1 ) = ld_loc_base.lr1)';
 
   vs_log_msg := SUBSTR(vs_sql,1   ,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_log_msg := SUBSTR(vs_sql,2001,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows updated in t_src_sales_tmpl : '|| vi_row_count;                                          LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   ---------------------------------------------------------------------------------------------------
   -- Update the AGGRE_SD column - UPDATE 1 for dates that exist in INPUTS
   ---------------------------------------------------------------------------------------------------
   vs_log_msg := 'Update the AGGRE_SD column - UPDATE 1';                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    AGGRE_SD       = TRUNC(t_src_sales_tmpl.sales_date)
              WHERE  EXISTS ( SELECT 1
                              FROM   INPUTS
                              WHERE  TRUNC(datet) = TRUNC(t_src_sales_tmpl.sales_date))';
 
   vs_log_msg := SUBSTR(vs_sql,1   ,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_log_msg := SUBSTR(vs_sql,2001,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows updated in t_src_sales_tmpl : '|| vi_row_count;                                          LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   ---------------------------------------------------------------------------------------------------
   -- Update the AGGRE_SD column - UPDATE 2 for dates that do not exist in INPUTS
   ---------------------------------------------------------------------------------------------------
   vs_log_msg := 'Update the AGGRE_SD column - UPDATE 2';                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    AGGRE_SD       = TRUNC( get_inputs_aggri_date(t_src_sales_tmpl.sales_date,''week'',2))
              WHERE  AGGRE_SD IS NULL';
 
   vs_log_msg := SUBSTR(vs_sql,1   ,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_log_msg := SUBSTR(vs_sql,2001,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   dynamic_ddl( vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'Rows updated in t_src_sales_tmpl : '|| vi_row_count;                                          LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   IF vs_part_level_column IS NOT NULL THEN
 
      vs_log_msg := 'Set the Partition column in the load table with actual values';                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      vs_part_level_column_select := 'SELECT '|| vs_part_level_column ||'  FROM '|| vs_part_level_source ||'  WHERE '|| vs_part_level_source ||'_id = '|| vs_load_table||'.ld_'||vs_part_level_source ||'_id';
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'UPDATE t_src_sales_tmpl SET '|| vs_part_level_column ||' = ('|| vs_part_level_column_select ||')';
 
      vs_log_msg := SUBSTR(vs_sql,1,2000);                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' - UPDATE ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

   END IF;


 
   -----------------------------------------------------------------------------
   -- LD_ITEM_ID - Any NULLs set to 0 (default)
   -----------------------------------------------------------------------------
   vs_log_msg := 'UPDATE load table - Any NULLs set to 0 (default) : LD_ITEM_ID';                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    LD_ITEM_ID     = 0
              WHERE  LD_ITEM_ID IS NULL';
 
   dynamic_ddl(vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
 
 
   -----------------------------------------------------------------------------
   -- LD_LOCATION_ID - Any NULLs set to 0 (default)
   -----------------------------------------------------------------------------
   vs_log_msg := 'UPDATE load table - Any NULLs set to 0 (default) : LD_LOCATION_ID';                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'UPDATE t_src_sales_tmpl
              SET    LD_LOCATION_ID = 0
              WHERE  LD_LOCATION_ID IS NULL';
 
   dynamic_ddl(vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - UPDATE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   IF vs_part_level_column IS NOT NULL THEN
 
      vs_log_msg := 'UPDATE load table - Any NULLs set to 0 (default) : vs_part_level_column';                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'UPDATE t_src_sales_tmpl
                 SET    '|| vs_part_level_column ||' = 0
                 WHERE  '|| vs_part_level_column ||' IS NULL';
 
      dynamic_ddl(vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' - UPDATE ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );
 
   END IF;
 
 
   vs_log_msg := 'CREATE INDEX t_src_sales_tmpl_SD_IDX LD_ITEM_ID,LD_LOCATION_ID,AGGRE_SD';                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   add_index ('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX','LD_ITEM_ID,LD_LOCATION_ID,AGGRE_SD','NONUNIQUE', 'BTREE','DEFAULT_TS');
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );


 
 
   vs_log_msg := 'CREATE INDEX t_src_sales_tmpl_CL_IDX LD_ITEM_ID';                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   add_index ('t_src_sales_tmpl','t_src_sales_tmpl_CL_IDX','LD_ITEM_ID','NONUNIQUE', 'BTREE','DEFAULT_TS');
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
 
 
  -- IF get_is_table_stats_exists   ('t_src_sales_tmpl' ) = 0 OR
  --    get_is_table_stats_rows_low ('t_src_sales_tmpl' ) = 1 THEN
 
      vs_log_msg := 'analyze_table t_src_sales_tmpl';                                                           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      analyze_table( UPPER('t_src_sales_tmpl') ,0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

  -- END IF;

 
 
 
   --------------------------------------------------------------------
   -- MERGE into MDP_LOAD_ASSIST where the sales quantity is NULL
   --------------------------------------------------------------------
 
   vs_log_msg := 'MERGE into MDP_LOAD_ASSIST where the sales quantity is NULL';                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'MERGE INTO mdp_load_assist m
   USING (SELECT /*+ parallel(t_src_sales_tmpl, 24) full(t_src_sales_tmpl)*/ 
                 ld_item_id     item_id,
                 ld_location_id location_id , max(t_src_sales_tmpl.e1_item_branch_category_1)  e1_it_br_cat_1, max(t_src_sales_tmpl.e1_item_branch_category_2)  e1_it_br_cat_2, max(t_src_sales_tmpl.e1_item_branch_category_5)  e1_it_br_cat_5,
 max(t_src_sales_tmpl.e1_item_branch_category_4)  e1_it_br_cat_4, max(t_src_sales_tmpl.e1_item_branch_category_3)  e1_it_br_cat_3
          FROM   t_src_sales_tmpl
          WHERE  ld_item_id     IS NOT NULL
          AND    ld_location_id IS NOT NULL
          AND    t_src_sales_tmpl.actual_qty IS NULL
          GROUP BY ld_item_id, ld_location_id
         ) s
   ON (m.item_id = s.item_id AND m.location_id = s.location_id)
   WHEN     MATCHED THEN UPDATE SET m.prop_changes = DECODE(m.prop_changes,1,1,0) ,
                                    m.is_fictive   = DECODE(m.is_fictive,0,0,2) , e1_it_br_cat_1 = s.e1_it_br_cat_1, e1_it_br_cat_2 = s.e1_it_br_cat_2, e1_it_br_cat_5 = s.e1_it_br_cat_5, e1_it_br_cat_4 = s.e1_it_br_cat_4, e1_it_br_cat_3 = s.e1_it_br_cat_3
   WHEN NOT MATCHED THEN INSERT VALUES (s.item_id, s.location_id, 0, 2 ,0,s.e1_it_br_cat_1,NULL,s.e1_it_br_cat_2,NULL,s.e1_it_br_cat_5,NULL,s.e1_it_br_cat_4,NULL,s.e1_it_br_cat_3,NULL)';
 
   dynamic_ddl(vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - MERGE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'MERGE into MDP_LOAD_ASSIST where the sales quantity is NULL rows : '|| vi_row_count;               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);


 
 
      --------------------------------------------------------------------------
      -- Delete ACTUAL_QUANTITY IS NULL  Rows from the staging table
      --------------------------------------------------------------------------
      vs_log_msg := 'Error Check : ACTUAL_QUANTITY cannot be NULL - T_SRC DELETE';                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_sql := 'DELETE
                 FROM  T_SRC_SALES_TMPL
                 WHERE t_src_sales_tmpl.actual_qty IS NULL';
 
      dynamic_ddl( vs_sql, vs_proc);
 
      vi_row_count := sql%rowcount;
 
      COMMIT;
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

      vs_log_msg := 'Rows deleted from T_SRC_SALES_TMPL : '|| vi_row_count;                              LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

 
 
   ---------------------------------------------------------------------------------------
   -- Gather MDP_LOAD_ASSIST stats ONLY when there are no stats found or NUM_ROWS are low
   ---------------------------------------------------------------------------------------
 
  -- IF get_is_table_stats_exists   ('MDP_LOAD_ASSIST') = 0 OR
  --    get_is_table_stats_rows_low ('MDP_LOAD_ASSIST') = 1 THEN
 
      vs_log_msg := 'ANALYZE TABLE MDP_LOAD_ASSIST';                                                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      analyze_table('MDP_LOAD_ASSIST',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

  -- END IF;


 
 
   --------------------------------------------------------------------
   -- MERGE into MDP_LOAD_ASSIST where the sales quantity is NOT NULL
   --------------------------------------------------------------------
 
   vs_log_msg := 'MERGE into MDP_LOAD_ASSIST where the sales quantity is NOT NULL';                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'MERGE INTO mdp_load_assist m
   USING (SELECT /*+ parallel(t_src_sales_tmpl, 24) full(t_src_sales_tmpl)*/ 
                 ld_item_id     item_id,
                 ld_location_id location_id,
                 DECODE(SUM(t_src_sales_tmpl.actual_qty),0,0,1) boolean_qty , max(t_src_sales_tmpl.e1_item_branch_category_1)  e1_it_br_cat_1, max(t_src_sales_tmpl.e1_item_branch_category_2)  e1_it_br_cat_2,
 max(t_src_sales_tmpl.e1_item_branch_category_5)  e1_it_br_cat_5, max(t_src_sales_tmpl.e1_item_branch_category_4)  e1_it_br_cat_4, max(t_src_sales_tmpl.e1_item_branch_category_3)  e1_it_br_cat_3
          FROM   t_src_sales_tmpl
          WHERE  ld_item_id     IS NOT NULL
          AND    ld_location_id IS NOT NULL
          AND    t_src_sales_tmpl.actual_qty IS NOT NULL
          GROUP BY ld_item_id, ld_location_id
         ) s
   ON (m.item_id = s.item_id AND m.location_id = s.location_id)
   WHEN MATCHED THEN UPDATE SET prop_changes = 1,
                                is_fictive   = DECODE(s.boolean_qty,1,0,DECODE(is_fictive,0,0,2)),
                                boolean_qty  = GREATEST(NVL(boolean_qty,0),s.boolean_qty)
                               , e1_it_br_cat_1 = s.e1_it_br_cat_1, e1_it_br_cat_2 = s.e1_it_br_cat_2, e1_it_br_cat_5 = s.e1_it_br_cat_5, e1_it_br_cat_4 = s.e1_it_br_cat_4, e1_it_br_cat_3 = s.e1_it_br_cat_3
   WHEN NOT MATCHED THEN INSERT
                         VALUES (s.item_id, s.location_id, 1, DECODE(s.boolean_qty,1,0,2), s.boolean_qty ,s.e1_it_br_cat_1,NULL,s.e1_it_br_cat_2,NULL,s.e1_it_br_cat_5,NULL,s.e1_it_br_cat_4,NULL,s.e1_it_br_cat_3,NULL)';
 
   dynamic_ddl(vs_sql, vs_proc);
 
   vi_row_count := sql%rowcount;
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - MERGE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

   vs_log_msg := 'MERGE into MDP_LOAD_ASSIST where the sales quantity is NOT NULL rows : '|| vi_row_count;           LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);

   COMMIT;

 
 
   ---------------------------------------------------------------------------------------
   -- Gather MDP_LOAD_ASSIST stats ONLY when there are no stats found or NUM_ROWS are low
   ---------------------------------------------------------------------------------------
 
 --  IF get_is_table_stats_exists   ('MDP_LOAD_ASSIST') = 0 OR
 --     get_is_table_stats_rows_low ('MDP_LOAD_ASSIST') = 1 THEN
 
      vs_log_msg := 'ANALYZE TABLE MDP_LOAD_ASSIST';                                                                 LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      analyze_table('MDP_LOAD_ASSIST',0);
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

  -- END IF;


 
 
   -------------------------------------------------------------------
   -- Construct MERGE - INSERT / INSERT for SALES_DATA
   -- There are 2 constructs
   -- 1. dv_pval  = 'UPDATE' Just update sales with the new value
   -- 2. dv_pval <> 'UPDATE' Add the new value to the previous one
   --
   -------------------------------------------------------------------
 
   ------------------
   -- MERGE header
   ------------------
 
   dv_sqlupd_1 := 'MERGE INTO SALES_DATA b
         USING (SELECT 
                       t_src_sales_tmpl.aggre_sd        aggre_sd,
                       t_src_sales_tmpl.ld_item_id      item_id,
                       t_src_sales_tmpl.ld_location_id  location_id,';
 
 
   IF vs_part_level_column IS NOT NULL THEN
      dv_sqlupd_1 := dv_sqlupd_1 ||'MAX(t_src_sales_tmpl.'|| vs_part_level_column ||') '|| vs_part_level_column ||',';
   END IF;
 
 
   dv_sqlupd_1 := dv_sqlupd_1 ||'avg(t_src_sales_tmpl.item_price) item_price , avg(t_src_sales_tmpl.base_evt_dol_rtl) base_evt_d_rtl_sd , avg(t_src_sales_tmpl.incr_evt_dol_rtl) incr_evt_d_rtl_sd ,
 sum(t_src_sales_tmpl.ebs_ship_hist_req_qty_rd) ebs_sh_req_qty_rd , avg(t_src_sales_tmpl.sdata15) sdata15 , avg(t_src_sales_tmpl.sdata14) sdata14 , avg(t_src_sales_tmpl.sdata13) sdata13 , avg(t_src_sales_tmpl.sdata12) sdata12 ,
 avg(t_src_sales_tmpl.sdata11) sdata11 , avg(t_src_sales_tmpl.sdata10) sdata10 , avg(t_src_sales_tmpl.sdata9) sdata9 , avg(t_src_sales_tmpl.sdata8) sdata8 , avg(t_src_sales_tmpl.sdata7) sdata7 , sum(t_src_sales_tmpl.sdata4) sdata4 ,
 sum(t_src_sales_tmpl.actual_qty) actual_quantity , avg(t_src_sales_tmpl.shelf_price_sd) shelf_price_sd , sum(t_src_sales_tmpl.ebs_ship_hist_ship_qty_rd) ebs_sh_ship_qty_rd , sum(t_src_sales_tmpl.ebs_ship_hist_ship_qty_sd) ebs_sh_ship_qty_sd ,
 sum(t_src_sales_tmpl.ebs_book_hist_req_qty_rd) ebs_bh_req_qty_rd , sum(t_src_sales_tmpl.ebs_book_hist_book_qty_rd) ebs_bh_book_qty_rd , sum(t_src_sales_tmpl.ebs_book_hist_book_qty_bd) ebs_bh_book_qty_bd ,
 sum(t_src_sales_tmpl.ebs_book_hist_req_qty_bd) ebs_bh_req_qty_bd';



   ------------------------
   -- MERGE SELECT - FROM
   ------------------------

   dv_sqlupd_2 := '
                FROM   t_src_sales_tmpl';


   ------------------------
   -- MERGE SELECT - WHERE
   ------------------------
   dv_sqlupd_3 := '
                WHERE  1 = 1
                AND    t_src_sales_tmpl.LD_ITEM_ID = ';

 
   ----------------------------
   -- MERGE SELECT - GROUP BY
   ----------------------------
   dv_sqlupd_5 := ' GROUP BY  LD_ITEM_ID,LD_LOCATION_ID,AGGRE_SD
   ORDER BY  LD_ITEM_ID,LD_LOCATION_ID,AGGRE_SD  ) s
   ON (b.item_id = s.item_id AND b.location_id = s.location_id AND b.sales_date = s.aggre_sd )';

 
 
   dv_sqlupd_6  := 'WHEN NOT MATCHED THEN INSERT (  item_id,   location_id,   sales_date,';
 
   IF vs_part_level_column IS NOT NULL THEN
      dv_sqlupd_6  := dv_sqlupd_6 || vs_part_level_column ||',';
   END IF;
 
   dv_sqlupd_6  := dv_sqlupd_6 ||'               load_sig, item_price , base_evt_d_rtl_sd , incr_evt_d_rtl_sd , ebs_sh_req_qty_rd , sdata15 , sdata14 , sdata13 , sdata12 , sdata11 , sdata10 , sdata9 , sdata8 , sdata7 , sdata4 , actual_quantity ,
 shelf_price_sd , ebs_sh_ship_qty_rd , ebs_sh_ship_qty_sd , ebs_bh_req_qty_rd , ebs_bh_book_qty_rd , ebs_bh_book_qty_bd , ebs_bh_req_qty_bd)
                             VALUES               (s.item_id, s.location_id, s.aggre_sd,';
 
   IF vs_part_level_column IS NOT NULL THEN
      dv_sqlupd_6  := dv_sqlupd_6 ||'s.'|| vs_part_level_column ||',';
   END IF;
 
   dv_sqlupd_6  := dv_sqlupd_6 ||'               '''|| dd_load_sig ||''',
                                                  s.item_price , s.base_evt_d_rtl_sd , s.incr_evt_d_rtl_sd , s.ebs_sh_req_qty_rd , s.sdata15 , s.sdata14 , s.sdata13 , s.sdata12 , s.sdata11 , s.sdata10 , s.sdata9 , s.sdata8 , s.sdata7 , s.sdata4 ,
 s.actual_quantity , s.shelf_price_sd , s.ebs_sh_ship_qty_rd , s.ebs_sh_ship_qty_sd , s.ebs_bh_req_qty_rd , s.ebs_bh_book_qty_rd , s.ebs_bh_book_qty_bd , s.ebs_bh_req_qty_bd)';
 
 
   --------------------------
   -- MERGE update section
   --------------------------
 
   IF UPPER(dv_pval) = 'UPDATE' THEN
 
      vs_log_msg := 'AccumulateOrUpdate : UPDATE';                                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      -------------------------------
      -- Update with the new value
      -------------------------------
 
      dv_sqlupd_7  := '
         WHEN MATCHED THEN UPDATE SET ';
 
      IF vs_part_level_column IS NOT NULL THEN
         dv_sqlupd_7  := dv_sqlupd_7 ||' '|| vs_part_level_column || ' = s.'|| vs_part_level_column ||',';
      END IF;
 
     dv_sqlupd_7  := dv_sqlupd_7 ||'item_price = s.item_price , base_evt_d_rtl_sd = s.base_evt_d_rtl_sd , incr_evt_d_rtl_sd = s.incr_evt_d_rtl_sd , ebs_sh_req_qty_rd = s.ebs_sh_req_qty_rd , sdata15 = s.sdata15 , sdata14 = s.sdata14 , sdata13 = s.sdata13 ,
 sdata12 = s.sdata12 , sdata11 = s.sdata11 , sdata10 = s.sdata10 , sdata9 = s.sdata9 , sdata8 = s.sdata8 , sdata7 = s.sdata7 , sdata4 = s.sdata4 , actual_quantity = s.actual_quantity , shelf_price_sd = s.shelf_price_sd ,
 ebs_sh_ship_qty_rd = s.ebs_sh_ship_qty_rd , ebs_sh_ship_qty_sd = s.ebs_sh_ship_qty_sd , ebs_bh_req_qty_rd = s.ebs_bh_req_qty_rd , ebs_bh_book_qty_rd = s.ebs_bh_book_qty_rd , ebs_bh_book_qty_bd = s.ebs_bh_book_qty_bd ,
 ebs_bh_req_qty_bd = s.ebs_bh_req_qty_bd,
                                       load_sig         = '''|| dd_load_sig ||''',
                                       syncro_sig       = -1,
                                       last_update_date = SYSTIMESTAMP';
 
   ELSE
 
      -------------------------------------------------------------
      -- Update Adding the new value to the previous one
      -------------------------------------------------------------
 
      vs_log_msg := 'AccumulateOrUpdate : ACCUMULATE';                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      dv_sqlupd_7  := '
         WHEN MATCHED THEN UPDATE SET   ';
 
      IF vs_part_level_column IS NOT NULL THEN
 
         dv_sqlupd_7  := dv_sqlupd_7 || vs_part_level_column ||' = s.'|| vs_part_level_column ||',';
 
      END IF;
 
      dv_counter  := 1;
 
      LOOP
         parsestringbydelimiter(dv_updlist,',', dv_counter, dv_updfld, dv_sts);
 
         IF dv_sts = 0 THEN
            EXIT;
         END IF;
 
         dv_sqlupd_7 := dv_sqlupd_7 || dv_updfld ||' = s.'|| dv_updfld ||',';
         dv_counter  := dv_counter + 1;
 
      END LOOP;
 
      dv_counter := 1;
      LOOP
         parsestringbydelimiter(dv_acclist,',', dv_counter, dv_updfld, dv_sts);
 
         IF dv_sts = 0 THEN
            EXIT;
         END IF;
 
         dv_sqlupd_7 := dv_sqlupd_7 || dv_updfld ||' = s.'|| dv_updfld  || ' + NVL('|| dv_updfld ||',0),';
         dv_counter  := dv_counter + 1;

      END LOOP;

      dv_sqlupd_7 := dv_sqlupd_7 ||'
                   load_sig         = '''|| dd_load_sig ||''',
                   syncro_sig       = -1,
                   last_update_date = SYSTIMESTAMP';

   END IF;


 
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_1,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_2,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_3,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_4,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_5,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_6,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_7,'NULL'),1,2000);                                                          LOG_IT (vs_log_table, vi_logging_level ,8 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);

 
 
   vs_log_msg := 'Main MERGE SALES_DATA LOOP';                                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   -----------------------------------------------------------------------
   -- Main MERGE SALES_DATA LOOP
   -----------------------------------------------------------------------
   vi_counter           := 0;
   vi_sales_merge_count := 0;
 
   vs_sql := 'SELECT DISTINCT LD_ITEM_ID
              FROM   t_src_sales_tmpl
              ORDER BY LD_ITEM_ID';
 
   OPEN rec_load FOR
        vs_sql;
   LOOP
      FETCH rec_load INTO vi_ld_item_id;
      EXIT WHEN rec_load%NOTFOUND;

      -----------------------------------------------------------------------
      -- Put the parameters in to strings to be run in the dynamic MERGE
      -----------------------------------------------------------------------


      dv_sqlupd_4 :=  ''|| vi_ld_item_id ||'';

 
      vs_log_msg := SUBSTR(NVL(dv_sqlupd_3 || dv_sqlupd_4,'NULL'),1,2000);                                           LOG_IT (vs_log_table, vi_logging_level ,9 , vs_proc_source,'LD', vs_log_msg ,  NULL , NULL , NULL);
 
 
      ----------------------------------------------------------
      -- Run the MERGE - INSERT / UPDATE on SALES_DATA
      ----------------------------------------------------------
 
      EXECUTE IMMEDIATE (dv_sqlupd_1 || dv_sqlupd_2 || dv_sqlupd_3 || dv_sqlupd_4 ||dv_sqlupd_5 || dv_sqlupd_6 || dv_sqlupd_7);
 
      vi_sales_merge_count := vi_sales_merge_count + sql%rowcount;
 
      COMMIT;
 
      vi_counter := vi_counter + 1;
 
      IF vi_logging_level >= 10 THEN
         DBEX('MERGE SALES_DATA loop count '|| vi_counter,'EP_LOAD_SALES','I');
      END IF;

   END LOOP;
   CLOSE rec_load;

   vs_log_msg := 'Main MERGE SALES_DATA - ITERATIONS using LD_ITEM_ID ['|| vi_counter ||']';

 
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - MERGE ['|| vi_sales_merge_count ||'] ' ;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );
 
   vs_log_msg := 'Rows merged (inserted or updated) in SALES_DATA : '|| TO_CHAR(NVL(vi_sales_merge_count,0));        LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

   IF vs_row_movement = 'DISABLED' THEN
      EXECUTE IMMEDIATE 'ALTER TABLE SALES_DATA DISABLE ROW MOVEMENT';
   END IF;


 
 
   ---------------------------------------------------------------------------
   -- Get the Min and Max sales dates from this data load.
   -- IF there is no data to load the use 1-1-1900 as the default for the max.
   ---------------------------------------------------------------------------
   vs_log_msg := 'Get the Min and Max sales dates from this data load.';                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vs_sql := 'SELECT  NVL(MAX(aggre_sd),TO_DATE(''1/1/1900'',''mm/dd/yyyy''))
              FROM    t_src_sales_tmpl
              WHERE   t_src_sales_tmpl.actual_qty IS NOT NULL';
 
   vd_max_load_date := dynamic_date ( vs_sql );
 
   vs_sql := 'SELECT  MIN(aggre_sd)
              FROM    t_src_sales_tmpl
              WHERE   t_src_sales_tmpl.actual_qty IS NOT NULL';
 
   vd_min_load_date := dynamic_date ( vs_sql );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
   --------------------------------------------------------------------------------
   -- Clean up the Load (Staging) table
   --------------------------------------------------------------------------------
   vs_log_msg := 'Clean up the Load (Staging) table';                                                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   helper.drop_primary_key('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_SD_IDX','NO COLUMNS');
   drop_index('t_src_sales_tmpl','t_src_sales_tmpl_FI'    ,'NO COLUMNS');
 
   helper.drop_column('t_src_sales_tmpl', 'LD_ITEM_ID');
   helper.drop_column('t_src_sales_tmpl', 'LD_LOCATION_ID');
   helper.drop_column('t_src_sales_tmpl', 'AGGRE_SD');
 
   --------------------------------------------------------------------------------
   -- If table is partitioned then drop the partition level column
   --------------------------------------------------------------------------------
   IF vs_part_level_column IS NOT NULL THEN
      helper.drop_column('t_src_sales_tmpl', vs_part_level_column);
   END IF;
 
   --------------------------------------------------------------------------
   -- Drop unused columns from the load table
   --------------------------------------------------------------------------
   vs_sql := 'ALTER TABLE t_src_sales_tmpl DROP UNUSED COLUMNS';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level , 2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl(vs_sql);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ;
   vd_end_time   := DBMS_UTILITY.get_time;                       vi_sec_log_id := vi_sec_log_id + 1;              vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );


 
 
   BEGIN
      get_param ('SYS_PARAMS','max_sales_date',vd_max_sales_date);
   EXCEPTION
      WHEN OTHERS THEN
 
         vd_max_sales_date := vd_max_load_date;
   END;
 
   -----------------------------------------------------------------------------------
   -- If the SYS_PARAMS value is not set then use the default 01/01/1900
   -----------------------------------------------------------------------------------
 
   IF vd_max_sales_date IS NULL THEN
      vd_max_sales_date := TO_DATE('01011900','MMDDYYYY');
   END IF;
 
   BEGIN
      SELECT TO_CHAR(GREATEST(vd_max_load_date,vd_max_sales_date),'mm-dd-yyyy hh24:mi:ss')
      INTO   vd_max_sales_date FROM dual;
   EXCEPTION
      WHEN OTHERS THEN
         vd_max_sales_date := vd_max_load_date;
   END;
 
   vs_log_msg := 'UPDATE SYS_PARAMS - max_sales_date with the newest sales date';                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   UPDATE SYS_PARAMS
   SET    pval  = vd_max_sales_date
   WHERE  pname = 'max_sales_date';
   COMMIT;
 
   -----------------------------------------------------------------------------------
   -- Set LAST_DATE_BACKUP
   -- If max_sales_date is a split date use the next time bucket
   -----------------------------------------------------------------------------------
   vd_last_date_backup := vd_max_sales_date;
 
   IF get_is_date_split(vd_max_sales_date) = 1 THEN
 
      vd_last_date_backup := get_date_via_time_buckets (vd_max_sales_date, 1);
 
   END IF;
 
   UPDATE INIT_PARAMS_0
   SET    value_date   = vd_max_sales_date
   WHERE  LOWER(pname) = 'last_date_backup';
   COMMIT;
 
   ------------------------------------------------------------------
   -- UPDATE SYS_PARAMS 'min_sales_date' with the oldest sales date
   ------------------------------------------------------------------
   vs_log_msg := 'UPDATE SYS_PARAMS - min_sales_date with the oldest sales date';                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   get_param ('SYS_PARAMS','min_sales_date',vd_min_sales_date);
 
   -------------------------------------------------------------------------------------
   -- If the SYS_PARAMS value is not set or it is set to the initial value of 01/01/1900
   -- then used the current load minimum sales date.
   -- Only change this one if we actually loaded sales data.
   -------------------------------------------------------------------------------------
 
   IF vd_min_load_date IS NOT NULL THEN
 
      IF vd_min_sales_date IS NULL                              OR
         vd_min_sales_date = TO_DATE('01011900','MMDDYYYY') THEN
 
         vd_min_sales_date := vd_min_load_date;
      END IF;
 
      SELECT TO_CHAR(LEAST(vd_min_load_date,vd_min_sales_date),'mm-dd-yyyy hh24:mi:ss')
      INTO   vd_min_sales_date FROM dual;
 
      UPDATE SYS_PARAMS
      SET    pval  = vd_min_sales_date
      WHERE  pname = 'min_sales_date';
 
   END IF;
 
   UPDATE SYS_PARAMS
   SET    pval  = (SELECT MIN(datet)
                   FROM   inputs
                   WHERE  datet > vd_max_sales_date)
   WHERE  pname = 'min_fore_sales_date';
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

    
 
   vs_log_msg := 'UPDATE the parent_id in the level tables.';
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
   -----------------------------------------------------------
   -- Remove duplicate rows from MDP_LOAD_ASSIST
   -----------------------------------------------------------
   vs_log_msg := 'Remove duplicate rows from MDP_LOAD_ASSIST';                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;
 
   vi_row_count := 0;
 
   FOR rec1 IN (
       SELECT item_id, location_id, COUNT (*) dup_count
       FROM   mdp_load_assist
       GROUP BY item_id, location_id
       HAVING COUNT (*) > 1)
   LOOP
 
      DELETE FROM mdp_load_assist
      WHERE  item_id     = rec1.item_id
      AND    location_id = rec1.location_id
      AND    ROWNUM < rec1.dup_count;
 
      vi_row_count := vi_row_count + sql%rowcount;
 
   END LOOP;
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vs_section    := vs_log_msg ||' - DELETE ['|| vi_row_count ||']';
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );


 
 
   ---------------------------------------------------------------------------------------
   -- Gather MDP_LOAD_ASSIST stats ONLY when there are no stats found or NUM_ROWS are low
   ---------------------------------------------------------------------------------------
 
  -- IF get_is_table_stats_exists   ('MDP_LOAD_ASSIST') = 0 OR
  --    get_is_table_stats_rows_low ('MDP_LOAD_ASSIST') = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE_TABLE : MDP_LOAD_ASSIST';                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL);
 
      analyze_table('MDP_LOAD_ASSIST',0);
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg ||' - DELETE ['|| vi_row_count ||']';
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

  -- END IF;

 
      vs_sql     := 'ALTER SESSION DISABLE PARALLEL DML';
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL);

   dynamic_ddl ( vs_sql );

 
   ----------------------------------------------------------------------------------
   -- ENABLE triggers
   ----------------------------------------------------------------------------------
   vs_log_msg := 'ENABLE triggers';                                                                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
 
   ---------------------------------------------------------------------------------------
   -- Gather SALES_DATA stats ONLY when there are no stats found or NUM_ROWS are low
   ---------------------------------------------------------------------------------------
   IF get_is_table_stats_exists   ('SALES_DATA') = 0 OR
      get_is_table_stats_rows_low ('SALES_DATA') = 1 THEN
 
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vd_start_time := DBMS_UTILITY.get_time;
 
      vs_log_msg := 'ANALYZE_TABLE : SALES_DATA';                                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
      analyze_table('SALES_DATA',0);
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_SECTION_LOG
      --------------------------------------------------------------------------
      vs_section    := vs_log_msg;
      vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
      vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
      vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

  END IF;

 
 
   vs_log_msg := 'End of procedure';                                                                                 LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_end_time  := DBMS_UTILITY.get_time;
   vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
   vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - sales_data MERGE ['|| TO_CHAR(NVL(vi_sales_merge_count,0)) ||']';

      vs_msg := vs_msg ||' - ITERATIONS using LD_ITEM_ID ['|| vi_counter ||'] T_SRC_SALES_TMPL ['|| vi_stagging_count ||']';

   DBEX( vs_msg, vs_proc, 'T' );

   set_module ('E');

END;

/
