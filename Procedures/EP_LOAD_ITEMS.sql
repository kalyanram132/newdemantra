--------------------------------------------------------
--  DDL for Procedure EP_LOAD_ITEMS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."EP_LOAD_ITEMS" -- $Revision: 1.200.14.3.2.2 $
  
    (ii_dummy INTEGER DEFAULT NULL)
IS
   vs_sql                            VARCHAR2(32000);
   vi_exists                         INTEGER;
   dv_is_exists                      INTEGER;
   sqlstr                            VARCHAR2(32000);
   vs_proc                           VARCHAR2(30) := 'EP_LOAD_ITEMS';
   vs_msg                            VARCHAR2(2000);
   vi_seq                            INTEGER;
 
   vi_error_code                     NUMBER;
   vs_error_text                     VARCHAR2(4000);
   vs_status                         VARCHAR2(10) := 'SUCCESS';
 
   dv_rowcounter                     INTEGER;
   dv_sys_rowcounter                 INTEGER;
   dv_max_id                         INTEGER;
   dv_pval_max_recs                  SYS_PARAMS.pval%TYPE;
   vs_pval                           VARCHAR2(100);
   vd_load_date                      DATE;
   vd_update_date                    DATE;
   vs_dim_merge_count                INTEGER;
   vs_mdp_update_count               INTEGER;
   vs_dim_level_update_count         INTEGER;
   vi_stagging_count                 INTEGER;

   vi_level_id                       INTEGER;
   vs_level_id                       VARCHAR2(20);

 
 
   dv_items                          INTEGER;
   dv_t_ep_I_ATT_1                   INTEGER;
   dv_t_ep_I_ATT_10                  INTEGER;
   dv_t_ep_I_ATT_2                   INTEGER;
   dv_t_ep_I_ATT_3                   INTEGER;
   dv_t_ep_I_ATT_4                   INTEGER;
   dv_t_ep_I_ATT_5                   INTEGER;
   dv_t_ep_I_ATT_6                   INTEGER;
   dv_t_ep_I_ATT_7                   INTEGER;
   dv_t_ep_I_ATT_8                   INTEGER;
   dv_t_ep_I_ATT_9                   INTEGER;
   dv_t_ep_e1_item_cat_1             INTEGER;
   dv_t_ep_e1_item_cat_2             INTEGER;
   dv_t_ep_e1_item_cat_3             INTEGER;
   dv_t_ep_e1_item_cat_4             INTEGER;
   dv_t_ep_e1_item_cat_5             INTEGER;
   dv_t_ep_e1_item_cat_6             INTEGER;
   dv_t_ep_e1_item_cat_7             INTEGER;
   dv_t_ep_ebs_demand_class          INTEGER;
   dv_t_ep_ebs_prod_cat              INTEGER;
   dv_t_ep_ebs_prod_family           INTEGER;
   dv_t_ep_item                      INTEGER;
   dv_t_ep_p1                        INTEGER;
   dv_t_ep_p2                        INTEGER;
   dv_t_ep_p2a                       INTEGER;
   dv_t_ep_p2a1                      INTEGER;
   dv_t_ep_p2a2                      INTEGER;
   dv_t_ep_p2b                       INTEGER;
   dv_t_ep_p3                        INTEGER;
   dv_t_ep_p4                        INTEGER;
 
   TYPE rec_load_cursor              IS REF CURSOR;
   rec_load                          rec_load_cursor;
 
   vs_t_ep_p1_596                    VARCHAR2(500);
   vs_t_ep_p2_623                    VARCHAR2(500);
   vs_t_ep_p3_624                    VARCHAR2(500);
   vs_t_ep_p4_626                    VARCHAR2(500);
   vs_t_ep_p2a_633                   VARCHAR2(500);
   vs_t_ep_p2a1_636                  VARCHAR2(500);
   vs_t_ep_p2a2_639                  VARCHAR2(500);
   vs_t_ep_p2b_642                   VARCHAR2(500);
   vs_dm_item_code_732               VARCHAR2(500);
   vs_t_ep_i_att_2_735               VARCHAR2(500);
   vs_t_ep_i_att_3_738               VARCHAR2(500);
   vs_t_ep_i_att_4_741               VARCHAR2(500);
   vs_t_ep_i_att_5_744               VARCHAR2(500);
   vs_t_ep_i_att_6_747               VARCHAR2(500);
   vs_t_ep_i_att_7_750               VARCHAR2(500);
   vs_t_ep_i_att_8_753               VARCHAR2(500);
   vs_t_ep_i_att_9_756               VARCHAR2(500);
   vs_t_ep_i_att_10_759              VARCHAR2(500);
   vs_dm_item_code_835               VARCHAR2(500);
   vs_e1_item_category_1_851         VARCHAR2(500);
   vs_e1_item_category_2_854         VARCHAR2(500);
   vs_e1_item_category_3_857         VARCHAR2(500);
   vs_e1_item_category_4_860         VARCHAR2(500);
   vs_e1_item_category_5_863         VARCHAR2(500);
   vs_e1_item_category_6_866         VARCHAR2(500);
   vs_e1_item_category_7_869         VARCHAR2(500);
   vs_ebs_product_category_1231      VARCHAR2(500);
   vs_ebs_product_family_c_1234      VARCHAR2(500);
   vs_ebs_demand_class_cod_1237      VARCHAR2(500);
 
   vs_p_t_ep_p1_596                   VARCHAR2(500) := NULL;
   vs_p_t_ep_p2_623                   VARCHAR2(500) := NULL;
   vs_p_t_ep_p3_624                   VARCHAR2(500) := NULL;
   vs_p_t_ep_p4_626                   VARCHAR2(500) := NULL;
   vs_p_t_ep_p2a_633                  VARCHAR2(500) := NULL;
   vs_p_t_ep_p2a1_636                 VARCHAR2(500) := NULL;
   vs_p_t_ep_p2a2_639                 VARCHAR2(500) := NULL;
   vs_p_t_ep_p2b_642                  VARCHAR2(500) := NULL;
   vs_p_dm_item_code_732              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_2_735              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_3_738              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_4_741              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_5_744              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_6_747              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_7_750              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_8_753              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_9_756              VARCHAR2(500) := NULL;
   vs_p_t_ep_i_att_10_759             VARCHAR2(500) := NULL;
   vs_p_dm_item_code_835              VARCHAR2(500) := NULL;
   vs_p_e1_item_category_1_851        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_2_854        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_3_857        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_4_860        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_5_863        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_6_866        VARCHAR2(500) := NULL;
   vs_p_e1_item_category_7_869        VARCHAR2(500) := NULL;
   vs_p_ebs_product_category_1231     VARCHAR2(500) := NULL;
   vs_p_ebs_product_family_c_1234     VARCHAR2(500) := NULL;
   vs_p_ebs_demand_class_cod_1237     VARCHAR2(500) := NULL;
 
   vi_t_ep_p1_ep_id                  NUMBER;
   vi_t_ep_p2_ep_id                  NUMBER;
   vi_t_ep_p3_ep_id                  NUMBER;
   vi_t_ep_p4_ep_id                  NUMBER;
   vi_t_ep_p2a_ep_id                 NUMBER;
   vi_t_ep_p2a1_ep_id                NUMBER;
   vi_t_ep_p2a2_ep_id                NUMBER;
   vi_t_ep_p2b_ep_id                 NUMBER;
   vi_t_ep_i_att_1_ep_id             NUMBER;
   vi_t_ep_i_att_2_ep_id             NUMBER;
   vi_t_ep_i_att_3_ep_id             NUMBER;
   vi_t_ep_i_att_4_ep_id             NUMBER;
   vi_t_ep_i_att_5_ep_id             NUMBER;
   vi_t_ep_i_att_6_ep_id             NUMBER;
   vi_t_ep_i_att_7_ep_id             NUMBER;
   vi_t_ep_i_att_8_ep_id             NUMBER;
   vi_t_ep_i_att_9_ep_id             NUMBER;
   vi_t_ep_i_att_10_ep_id            NUMBER;
   vi_t_ep_item_ep_id                NUMBER;
   vi_t_ep_e1_item_cat_1_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_2_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_3_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_4_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_5_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_6_ep_id       NUMBER;
   vi_t_ep_e1_item_cat_7_ep_id       NUMBER;
   vi_t_ep_ebs_prod_cat_ep_id        NUMBER;
   vi_t_ep_ebs_prod_family_ep_id     NUMBER;
   vi_t_ep_ebs_demand_class_ep_id    NUMBER;
 
   vs_i_att_1_desc_733               VARCHAR2(2000);
   vs_dm_item_desc_836               VARCHAR2(2000);
   vs_e1_i_cat_desc_1_852            VARCHAR2(2000);
   vs_e1_i_cat_desc_2_855            VARCHAR2(2000);
   vs_e1_i_cat_desc_3_858            VARCHAR2(2000);
   vs_e1_i_cat_desc_4_861            VARCHAR2(2000);
   vs_e1_i_cat_desc_5_864            VARCHAR2(2000);
   vs_e1_i_cat_desc_6_867            VARCHAR2(2000);
   vs_e1_i_cat_desc_7_870            VARCHAR2(2000);
   vs_ebs_demand_cls_desc_1238       VARCHAR2(2000);
   vs_ebs_prod_cat_desc_1232         VARCHAR2(2000);
   vs_ebs_prod_fmly_desc_1235        VARCHAR2(2000);
   vs_i_att_10_desc_760              VARCHAR2(2000);
   vs_i_att_2_desc_736               VARCHAR2(2000);
   vs_i_att_3_desc_739               VARCHAR2(2000);
   vs_i_att_4_desc_742               VARCHAR2(2000);
   vs_i_att_5_desc_745               VARCHAR2(2000);
   vs_i_att_6_desc_748               VARCHAR2(2000);
   vs_i_att_7_desc_751               VARCHAR2(2000);
   vs_i_att_8_desc_754               VARCHAR2(2000);
   vs_i_att_9_desc_757               VARCHAR2(2000);
   vs_p1_desc_597                    VARCHAR2(2000);
   vs_ps_desc_629                    VARCHAR2(2000);
   vs_p2a_desc_634                   VARCHAR2(2000);
   vs_p2a1_desc_637                  VARCHAR2(2000);
   vs_p2a2_desc_640                  VARCHAR2(2000);
   vs_p2b_desc_643                   VARCHAR2(2000);
   vs_p3_desc_630                    VARCHAR2(2000);
   vs_p4_desc_631                    VARCHAR2(2000);

   vs_description                    VARCHAR2(32000);

 
   vi_logging_level                  INTEGER;
   vs_log_table                      VARCHAR2(30);
   vs_proc_source                    VARCHAR2(60);
   vi_log_it_commit                  INTEGER;
   vs_log_msg                        VARCHAR2(2000);
   vs_temp_proc_name                 VARCHAR2(30);
 
   vd_start_time_proc                NUMBER;
   vd_start_time                     NUMBER;
   vd_end_time                       NUMBER;
 
   vt_procedure                      NUMBER := 0;
   vt_duration                       NUMBER := 0;
   vs_duration                       VARCHAR2(10)   := '';
 
   vs_section                        VARCHAR2(3000) := '';
   vi_sec_log_id                     INTEGER := 0;
   vs_sec_log_id                     VARCHAR2(10);
 
BEGIN
 
   pre_logon;
 
   set_module ( 'S' );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_start_time_proc := DBMS_UTILITY.get_time;
 
   --------------------------------------------------------------------------
   -- Record the Parameters passed to DB_CALL_LOG
   --------------------------------------------------------------------------
   vs_msg := 'ii_dummy => '|| NVL( TO_CHAR( ii_dummy ) ,'NULL');
 
   DBEX(vs_msg, vs_proc, 'C');
 
   --------------------------------------------------------------------------
   -- Initialize LOG_IT
   --------------------------------------------------------------------------
 
   vs_proc_source   := 'EP_LOAD_ITEMS';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level, vi_log_it_commit);
 
   vs_log_msg := 'Logging Level ('|| vi_logging_level ||')';                                                       LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'SP', vs_log_msg, 'T' , 'C', NULL);

   vd_update_date := SYSTIMESTAMP;

   vs_log_msg := 'vd_update_date : '|| vd_update_date;                                                               LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg , NULL, NULL);

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Rebuild all the dimension level sequences
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Rebuild all the dimension level sequences';                                                        LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- REBUILD ALL THE DIMENSION LEVEL SEQUENCES
   --------------------------------------------------------------------------
   vs_log_msg := 'REBUILD ALL THE DIMENSION LEVEL SEQUENCES';                                                        LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   FOR rec IN (SELECT DISTINCT e_plan_table_name, sequence_name
               FROM   e_plan_tree, user_sequences
               WHERE  sequence_name = UPPER(e_plan_table_name)||'_SEQ'
               AND    model_version = 15)
   LOOP
      dynamic_ddl('DROP SEQUENCE '|| rec.sequence_name);
 
      dv_max_id := dynamic_number('SELECT MAX('|| rec.e_plan_table_name ||'_ep_id) + 1  FROM   '|| rec.e_plan_table_name);

      dynamic_ddl('CREATE SEQUENCE '|| rec.sequence_name ||' START WITH '|| dv_max_id);
   END LOOP;

   get_param('SYS_PARAMS','max_records_for_commit', dv_pval_max_recs, 10000);

   dv_sys_rowcounter := dv_pval_max_recs;


 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Load dimension level tables
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Load dimension level tables';                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- LOOP FOR ALL ITEMS LEVELS - CREATE AND RUN EP_LOAD_TMP
   --------------------------------------------------------------------------
   vs_log_msg := 'LOOP FOR ALL dim LEVELS - CREATE AND RUN EP_LOAD_TMP';                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   FOR rec IN (SELECT syntax, node_id, UPPER(table_name) table_name
               FROM   ep_model_syntax
               WHERE  dim_type = 1
               ORDER BY node_id)
   LOOP
 
      vs_sql := 'SELECT COUNT(*)
                  FROM   group_tables
                  WHERE  UPPER(gtable) = '''|| rec.table_name || '''';
 
      vi_exists := dynamic_number ( vs_sql );
 
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
      vi_level_id := NULL;
 
      IF vi_exists = 1 THEN
 
         vs_sql := 'SELECT group_table_id
                     FROM   group_tables
                     WHERE  UPPER(gtable) = '''|| rec.table_name || '''';
 
         vs_log_msg := vs_sql;                                                                                         LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
         vi_level_id := dynamic_number ( vs_sql );
      END IF;
 
      vs_level_id := NVL(TO_CHAR(vi_level_id) ,'NULL');
 
      vs_temp_proc_name := 'ep_load_tmp';
 
      IF vi_logging_level >= 4 THEN
         vs_temp_proc_name := 'EP_LD_' || SUBSTR(rec.table_name,1,24);
      END IF;
 
      sqlstr := 'CREATE OR REPLACE PROCEDURE '|| vs_temp_proc_name ||'
IS
   vs_sql                VARCHAR2(32000);
   vi_seq                INTEGER;
   vi_exists             INTEGER;
 
   vi_logging_level      INT;
   vs_log_table          VARCHAR2(30);
   vs_proc_source        VARCHAR2(60);
   vi_log_it_commit      INT;
   vs_log_msg            VARCHAR2(2000);
 
   vd_update_date        DATE;
 
   vi_level_id           INTEGER := '|| vs_level_id ||';
 
   vi_insert_count       INTEGER := 0;
   vi_update_count       INTEGER := 0;
 
   dv_is_exists          INTEGER;
   dv_rowcounter         INTEGER;
   vi_fictive_child      INTEGER;
   vs_father_level_id    INTEGER;
   vs_id_display_field   VARCHAR2(30);
   dv_sys_rowcounter     INTEGER := '|| dv_pval_max_recs ||';
 
   vs_content            VARCHAR2(4000);
   os_status             VARCHAR2(100);
   os_result             VARCHAR2(4000);
 
BEGIN
 
   pre_logon;
 
   set_module ( ''S'' );
 
   vs_proc_source   := ''EP_LOAD_ITEMS'';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level);
 
   vs_proc_source   := '''|| vs_temp_proc_name ||''';
 
   vs_log_msg := ''Start of procedure'';                                                                           LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,''SP'', vs_log_msg , NULL ,NULL);
 
   vd_update_date   := '''|| vd_update_date ||''';
 
   vi_level_id := NVL( vi_level_id , -54321 );
 
   '|| rec.syntax ||'
 
   vs_log_msg := ''End of procedure'';                                                                             LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,''EP'',vs_log_msg , NULL ,NULL);
 
   set_module (''E'');
 
 
END;';
      dynamic_ddl(sqlstr);
      dynamic_ddl('BEGIN '|| vs_temp_proc_name ||'; END;');
 
      --IF get_is_table_stats_exists   ( UPPER(rec.table_name) ) = 0 OR
      --   get_is_table_stats_rows_low ( UPPER(rec.table_name) ) = 1 THEN

         analyze_table( UPPER(rec.table_name) ,0);

      --END IF;

      IF vi_logging_level < 4 THEN
         check_and_drop( vs_temp_proc_name );
      END IF;

   END LOOP;

   COMMIT;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
   vs_log_msg := 'ALTER SESSION SET SKIP_UNUSABLE_INDEXES=TRUE';                                                     LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);

   execute immediate ('ALTER SESSION SET SKIP_UNUSABLE_INDEXES=TRUE');


 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - Stagging table count
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Stagging table count';                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- Stagging table count
   --------------------------------------------------------------------------
   vs_sql := 'SELECT COUNT(*) FROM t_src_item_tmpl';

   vi_stagging_count := dynamic_number ( vs_sql );
   vi_stagging_count := NVL(vi_stagging_count,0);

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MERGE INTO dimension table - Cursor Loop
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MERGE INTO dimension table - Cursor Loop';                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   --------------------------------------------------------------------------
   -- LOOP - SELECT THE SRC DIMENSION TABLE ROWS
   --        Use 2 cursor loops instead of 1 to avoid large use of TEMP TS
   --------------------------------------------------------------------------
   vs_dim_merge_count := 0;
 
   vs_log_msg := 'LOOP - SELECT THE SRC DIMENSION TABLE ROWS';                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   vd_load_date := SYSTIMESTAMP;
 
   vs_log_msg := 'vd_load_date : '|| vd_load_date;                                                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source,'D', vs_log_msg , NULL, NULL);
 
   vs_sql := 'SELECT DISTINCT
                     LOWER(t_ep_p1),
                     LOWER(t_ep_p2),
                     LOWER(t_ep_p3),
                     LOWER(t_ep_p4),
                     LOWER(t_ep_p2a),
                     LOWER(t_ep_p2a1),
                     LOWER(t_ep_p2a2),
                     LOWER(t_ep_p2b),
                     LOWER(dm_item_code),
                     LOWER(t_ep_i_att_2),
                     LOWER(t_ep_i_att_3),
                     LOWER(t_ep_i_att_4),
                     LOWER(t_ep_i_att_5),
                     LOWER(t_ep_i_att_6),
                     LOWER(t_ep_i_att_7),
                     LOWER(t_ep_i_att_8),
                     LOWER(t_ep_i_att_9),
                     LOWER(t_ep_i_att_10),
                     LOWER(dm_item_code),
                     LOWER(e1_item_category_1),
                     LOWER(e1_item_category_2),
                     LOWER(e1_item_category_3),
                     LOWER(e1_item_category_4),
                     LOWER(e1_item_category_5),
                     LOWER(e1_item_category_6),
                     LOWER(e1_item_category_7),
                     LOWER(ebs_product_category_code),
                     LOWER(ebs_product_family_code),
                     LOWER(ebs_demand_class_code)
              FROM   t_src_item_tmpl';
 
   OPEN rec_load FOR
        vs_sql;
   LOOP
      FETCH rec_load INTO
            vs_t_ep_p1_596,
            vs_t_ep_p2_623,
            vs_t_ep_p3_624,
            vs_t_ep_p4_626,
            vs_t_ep_p2a_633,
            vs_t_ep_p2a1_636,
            vs_t_ep_p2a2_639,
            vs_t_ep_p2b_642,
            vs_dm_item_code_732,
            vs_t_ep_i_att_2_735,
            vs_t_ep_i_att_3_738,
            vs_t_ep_i_att_4_741,
            vs_t_ep_i_att_5_744,
            vs_t_ep_i_att_6_747,
            vs_t_ep_i_att_7_750,
            vs_t_ep_i_att_8_753,
            vs_t_ep_i_att_9_756,
            vs_t_ep_i_att_10_759,
            vs_dm_item_code_835,
            vs_e1_item_category_1_851,
            vs_e1_item_category_2_854,
            vs_e1_item_category_3_857,
            vs_e1_item_category_4_860,
            vs_e1_item_category_5_863,
            vs_e1_item_category_6_866,
            vs_e1_item_category_7_869,
            vs_ebs_product_category_1231,
            vs_ebs_product_family_c_1234,
            vs_ebs_demand_class_cod_1237;
      EXIT WHEN rec_load%NOTFOUND;


      -----------------------------------------------------------------------------
      -- (1) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p1_596 IS NULL AND vs_t_ep_p1_596 IS NOT NULL ) OR vs_p_t_ep_p1_596 <> vs_t_ep_p1_596 THEN
 
         vs_sql := 'SELECT t_ep_p1_ep_id, p1_desc FROM   t_ep_p1 WHERE  LOWER(p1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p1_ep_id, vs_p1_desc_597 USING vs_t_ep_p1_596;

         vs_p_t_ep_p1_596 := vs_t_ep_p1_596;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p1_596 := NULL;
         END;
      END IF;
      vi_t_ep_p1_ep_id := NVL( vi_t_ep_p1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (2) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p2_623 IS NULL AND vs_t_ep_p2_623 IS NOT NULL ) OR vs_p_t_ep_p2_623 <> vs_t_ep_p2_623 THEN
 
         vs_sql := 'SELECT t_ep_p2_ep_id, ps_desc FROM   t_ep_p2 WHERE  LOWER(p2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p2_ep_id, vs_ps_desc_629 USING vs_t_ep_p2_623;

         vs_p_t_ep_p2_623 := vs_t_ep_p2_623;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p2_623 := NULL;
         END;
      END IF;
      vi_t_ep_p2_ep_id := NVL( vi_t_ep_p2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (3) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p3_624 IS NULL AND vs_t_ep_p3_624 IS NOT NULL ) OR vs_p_t_ep_p3_624 <> vs_t_ep_p3_624 THEN
 
         vs_sql := 'SELECT t_ep_p3_ep_id, p3_desc FROM   t_ep_p3 WHERE  LOWER(p3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p3_ep_id, vs_p3_desc_630 USING vs_t_ep_p3_624;

         vs_p_t_ep_p3_624 := vs_t_ep_p3_624;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p3_624 := NULL;
         END;
      END IF;
      vi_t_ep_p3_ep_id := NVL( vi_t_ep_p3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (4) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p4_626 IS NULL AND vs_t_ep_p4_626 IS NOT NULL ) OR vs_p_t_ep_p4_626 <> vs_t_ep_p4_626 THEN
 
         vs_sql := 'SELECT t_ep_p4_ep_id, p4_desc FROM   t_ep_p4 WHERE  LOWER(p4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p4_ep_id, vs_p4_desc_631 USING vs_t_ep_p4_626;

         vs_p_t_ep_p4_626 := vs_t_ep_p4_626;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p4_626 := NULL;
         END;
      END IF;
      vi_t_ep_p4_ep_id := NVL( vi_t_ep_p4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (5) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p2a_633 IS NULL AND vs_t_ep_p2a_633 IS NOT NULL ) OR vs_p_t_ep_p2a_633 <> vs_t_ep_p2a_633 THEN
 
         vs_sql := 'SELECT t_ep_p2a_ep_id, p2a_desc FROM   t_ep_p2a WHERE  LOWER(p2a) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p2a_ep_id, vs_p2a_desc_634 USING vs_t_ep_p2a_633;

         vs_p_t_ep_p2a_633 := vs_t_ep_p2a_633;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p2a_633 := NULL;
         END;
      END IF;
      vi_t_ep_p2a_ep_id := NVL( vi_t_ep_p2a_ep_id,0);

      -----------------------------------------------------------------------------
      -- (6) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p2a1_636 IS NULL AND vs_t_ep_p2a1_636 IS NOT NULL ) OR vs_p_t_ep_p2a1_636 <> vs_t_ep_p2a1_636 THEN
 
         vs_sql := 'SELECT t_ep_p2a1_ep_id, p2a1_desc FROM   t_ep_p2a1 WHERE  LOWER(p2a1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p2a1_ep_id, vs_p2a1_desc_637 USING vs_t_ep_p2a1_636;

         vs_p_t_ep_p2a1_636 := vs_t_ep_p2a1_636;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p2a1_636 := NULL;
         END;
      END IF;
      vi_t_ep_p2a1_ep_id := NVL( vi_t_ep_p2a1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (7) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p2a2_639 IS NULL AND vs_t_ep_p2a2_639 IS NOT NULL ) OR vs_p_t_ep_p2a2_639 <> vs_t_ep_p2a2_639 THEN
 
         vs_sql := 'SELECT t_ep_p2a2_ep_id, p2a2_desc FROM   t_ep_p2a2 WHERE  LOWER(p2a2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p2a2_ep_id, vs_p2a2_desc_640 USING vs_t_ep_p2a2_639;

         vs_p_t_ep_p2a2_639 := vs_t_ep_p2a2_639;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p2a2_639 := NULL;
         END;
      END IF;
      vi_t_ep_p2a2_ep_id := NVL( vi_t_ep_p2a2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (8) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_p2b_642 IS NULL AND vs_t_ep_p2b_642 IS NOT NULL ) OR vs_p_t_ep_p2b_642 <> vs_t_ep_p2b_642 THEN
 
         vs_sql := 'SELECT t_ep_p2b_ep_id, p2b_desc FROM   t_ep_p2b WHERE  LOWER(p2b) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_p2b_ep_id, vs_p2b_desc_643 USING vs_t_ep_p2b_642;

         vs_p_t_ep_p2b_642 := vs_t_ep_p2b_642;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_p2b_642 := NULL;
         END;
      END IF;
      vi_t_ep_p2b_ep_id := NVL( vi_t_ep_p2b_ep_id,0);

      -----------------------------------------------------------------------------
      -- (9) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_dm_item_code_732 IS NULL AND vs_dm_item_code_732 IS NOT NULL ) OR vs_p_dm_item_code_732 <> vs_dm_item_code_732 THEN
 
         vs_sql := 'SELECT t_ep_i_att_1_ep_id, i_att_1_desc FROM   t_ep_i_att_1 WHERE  LOWER(i_att_1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_1_ep_id, vs_i_att_1_desc_733 USING vs_dm_item_code_732;

         vs_p_dm_item_code_732 := vs_dm_item_code_732;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_dm_item_code_732 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_1_ep_id := NVL( vi_t_ep_i_att_1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (10) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_2_735 IS NULL AND vs_t_ep_i_att_2_735 IS NOT NULL ) OR vs_p_t_ep_i_att_2_735 <> vs_t_ep_i_att_2_735 THEN
 
         vs_sql := 'SELECT t_ep_i_att_2_ep_id, i_att_2_desc FROM   t_ep_i_att_2 WHERE  LOWER(i_att_2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_2_ep_id, vs_i_att_2_desc_736 USING vs_t_ep_i_att_2_735;

         vs_p_t_ep_i_att_2_735 := vs_t_ep_i_att_2_735;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_2_735 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_2_ep_id := NVL( vi_t_ep_i_att_2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (11) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_3_738 IS NULL AND vs_t_ep_i_att_3_738 IS NOT NULL ) OR vs_p_t_ep_i_att_3_738 <> vs_t_ep_i_att_3_738 THEN
 
         vs_sql := 'SELECT t_ep_i_att_3_ep_id, i_att_3_desc FROM   t_ep_i_att_3 WHERE  LOWER(i_att_3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_3_ep_id, vs_i_att_3_desc_739 USING vs_t_ep_i_att_3_738;

         vs_p_t_ep_i_att_3_738 := vs_t_ep_i_att_3_738;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_3_738 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_3_ep_id := NVL( vi_t_ep_i_att_3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (12) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_4_741 IS NULL AND vs_t_ep_i_att_4_741 IS NOT NULL ) OR vs_p_t_ep_i_att_4_741 <> vs_t_ep_i_att_4_741 THEN
 
         vs_sql := 'SELECT t_ep_i_att_4_ep_id, i_att_4_desc FROM   t_ep_i_att_4 WHERE  LOWER(i_att_4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_4_ep_id, vs_i_att_4_desc_742 USING vs_t_ep_i_att_4_741;

         vs_p_t_ep_i_att_4_741 := vs_t_ep_i_att_4_741;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_4_741 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_4_ep_id := NVL( vi_t_ep_i_att_4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (13) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_5_744 IS NULL AND vs_t_ep_i_att_5_744 IS NOT NULL ) OR vs_p_t_ep_i_att_5_744 <> vs_t_ep_i_att_5_744 THEN
 
         vs_sql := 'SELECT t_ep_i_att_5_ep_id, i_att_5_desc FROM   t_ep_i_att_5 WHERE  LOWER(i_att_5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_5_ep_id, vs_i_att_5_desc_745 USING vs_t_ep_i_att_5_744;

         vs_p_t_ep_i_att_5_744 := vs_t_ep_i_att_5_744;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_5_744 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_5_ep_id := NVL( vi_t_ep_i_att_5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (14) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_6_747 IS NULL AND vs_t_ep_i_att_6_747 IS NOT NULL ) OR vs_p_t_ep_i_att_6_747 <> vs_t_ep_i_att_6_747 THEN
 
         vs_sql := 'SELECT t_ep_i_att_6_ep_id, i_att_6_desc FROM   t_ep_i_att_6 WHERE  LOWER(i_att_6) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_6_ep_id, vs_i_att_6_desc_748 USING vs_t_ep_i_att_6_747;

         vs_p_t_ep_i_att_6_747 := vs_t_ep_i_att_6_747;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_6_747 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_6_ep_id := NVL( vi_t_ep_i_att_6_ep_id,0);

      -----------------------------------------------------------------------------
      -- (15) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_7_750 IS NULL AND vs_t_ep_i_att_7_750 IS NOT NULL ) OR vs_p_t_ep_i_att_7_750 <> vs_t_ep_i_att_7_750 THEN
 
         vs_sql := 'SELECT t_ep_i_att_7_ep_id, i_att_7_desc FROM   t_ep_i_att_7 WHERE  LOWER(i_att_7) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_7_ep_id, vs_i_att_7_desc_751 USING vs_t_ep_i_att_7_750;

         vs_p_t_ep_i_att_7_750 := vs_t_ep_i_att_7_750;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_7_750 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_7_ep_id := NVL( vi_t_ep_i_att_7_ep_id,0);

      -----------------------------------------------------------------------------
      -- (16) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_8_753 IS NULL AND vs_t_ep_i_att_8_753 IS NOT NULL ) OR vs_p_t_ep_i_att_8_753 <> vs_t_ep_i_att_8_753 THEN
 
         vs_sql := 'SELECT t_ep_i_att_8_ep_id, i_att_8_desc FROM   t_ep_i_att_8 WHERE  LOWER(i_att_8) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_8_ep_id, vs_i_att_8_desc_754 USING vs_t_ep_i_att_8_753;

         vs_p_t_ep_i_att_8_753 := vs_t_ep_i_att_8_753;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_8_753 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_8_ep_id := NVL( vi_t_ep_i_att_8_ep_id,0);

      -----------------------------------------------------------------------------
      -- (17) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_9_756 IS NULL AND vs_t_ep_i_att_9_756 IS NOT NULL ) OR vs_p_t_ep_i_att_9_756 <> vs_t_ep_i_att_9_756 THEN
 
         vs_sql := 'SELECT t_ep_i_att_9_ep_id, i_att_9_desc FROM   t_ep_i_att_9 WHERE  LOWER(i_att_9) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_9_ep_id, vs_i_att_9_desc_757 USING vs_t_ep_i_att_9_756;

         vs_p_t_ep_i_att_9_756 := vs_t_ep_i_att_9_756;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_9_756 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_9_ep_id := NVL( vi_t_ep_i_att_9_ep_id,0);

      -----------------------------------------------------------------------------
      -- (18) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_t_ep_i_att_10_759 IS NULL AND vs_t_ep_i_att_10_759 IS NOT NULL ) OR vs_p_t_ep_i_att_10_759 <> vs_t_ep_i_att_10_759 THEN
 
         vs_sql := 'SELECT t_ep_i_att_10_ep_id, i_att_10_desc FROM   t_ep_i_att_10 WHERE  LOWER(i_att_10) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_i_att_10_ep_id, vs_i_att_10_desc_760 USING vs_t_ep_i_att_10_759;

         vs_p_t_ep_i_att_10_759 := vs_t_ep_i_att_10_759;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_t_ep_i_att_10_759 := NULL;
         END;
      END IF;
      vi_t_ep_i_att_10_ep_id := NVL( vi_t_ep_i_att_10_ep_id,0);

      -----------------------------------------------------------------------------
      -- (19) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_dm_item_code_835 IS NULL AND vs_dm_item_code_835 IS NOT NULL ) OR vs_p_dm_item_code_835 <> vs_dm_item_code_835 THEN
 
         vs_sql := 'SELECT t_ep_item_ep_id, dm_item_desc FROM   t_ep_item WHERE  LOWER(item) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_item_ep_id, vs_dm_item_desc_836 USING vs_dm_item_code_835;

         vs_p_dm_item_code_835 := vs_dm_item_code_835;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_dm_item_code_835 := NULL;
         END;
      END IF;
      vi_t_ep_item_ep_id := NVL( vi_t_ep_item_ep_id,0);

      -----------------------------------------------------------------------------
      -- (20) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_1_851 IS NULL AND vs_e1_item_category_1_851 IS NOT NULL ) OR vs_p_e1_item_category_1_851 <> vs_e1_item_category_1_851 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_1_ep_id, e1_i_cat_desc_1 FROM   t_ep_e1_item_cat_1 WHERE  LOWER(e1_item_cat_1) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_1_ep_id, vs_e1_i_cat_desc_1_852 USING vs_e1_item_category_1_851;
 
         vs_p_e1_item_category_1_851 := vs_e1_item_category_1_851;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_1_851 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_1_ep_id := NVL( vi_t_ep_e1_item_cat_1_ep_id,0);

      -----------------------------------------------------------------------------
      -- (21) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_2_854 IS NULL AND vs_e1_item_category_2_854 IS NOT NULL ) OR vs_p_e1_item_category_2_854 <> vs_e1_item_category_2_854 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_2_ep_id, e1_i_cat_desc_2 FROM   t_ep_e1_item_cat_2 WHERE  LOWER(e1_item_cat_2) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_2_ep_id, vs_e1_i_cat_desc_2_855 USING vs_e1_item_category_2_854;
 
         vs_p_e1_item_category_2_854 := vs_e1_item_category_2_854;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_2_854 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_2_ep_id := NVL( vi_t_ep_e1_item_cat_2_ep_id,0);

      -----------------------------------------------------------------------------
      -- (22) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_3_857 IS NULL AND vs_e1_item_category_3_857 IS NOT NULL ) OR vs_p_e1_item_category_3_857 <> vs_e1_item_category_3_857 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_3_ep_id, e1_i_cat_desc_3 FROM   t_ep_e1_item_cat_3 WHERE  LOWER(e1_item_cat_3) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_3_ep_id, vs_e1_i_cat_desc_3_858 USING vs_e1_item_category_3_857;
 
         vs_p_e1_item_category_3_857 := vs_e1_item_category_3_857;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_3_857 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_3_ep_id := NVL( vi_t_ep_e1_item_cat_3_ep_id,0);

      -----------------------------------------------------------------------------
      -- (23) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_4_860 IS NULL AND vs_e1_item_category_4_860 IS NOT NULL ) OR vs_p_e1_item_category_4_860 <> vs_e1_item_category_4_860 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_4_ep_id, e1_i_cat_desc_4 FROM   t_ep_e1_item_cat_4 WHERE  LOWER(e1_item_cat_4) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_4_ep_id, vs_e1_i_cat_desc_4_861 USING vs_e1_item_category_4_860;
 
         vs_p_e1_item_category_4_860 := vs_e1_item_category_4_860;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_4_860 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_4_ep_id := NVL( vi_t_ep_e1_item_cat_4_ep_id,0);

      -----------------------------------------------------------------------------
      -- (24) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_5_863 IS NULL AND vs_e1_item_category_5_863 IS NOT NULL ) OR vs_p_e1_item_category_5_863 <> vs_e1_item_category_5_863 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_5_ep_id, e1_i_cat_desc_5 FROM   t_ep_e1_item_cat_5 WHERE  LOWER(e1_item_cat_5) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_5_ep_id, vs_e1_i_cat_desc_5_864 USING vs_e1_item_category_5_863;
 
         vs_p_e1_item_category_5_863 := vs_e1_item_category_5_863;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_5_863 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_5_ep_id := NVL( vi_t_ep_e1_item_cat_5_ep_id,0);

      -----------------------------------------------------------------------------
      -- (25) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_6_866 IS NULL AND vs_e1_item_category_6_866 IS NOT NULL ) OR vs_p_e1_item_category_6_866 <> vs_e1_item_category_6_866 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_6_ep_id, e1_i_cat_desc_6 FROM   t_ep_e1_item_cat_6 WHERE  LOWER(e1_item_cat_6) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_6_ep_id, vs_e1_i_cat_desc_6_867 USING vs_e1_item_category_6_866;
 
         vs_p_e1_item_category_6_866 := vs_e1_item_category_6_866;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_6_866 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_6_ep_id := NVL( vi_t_ep_e1_item_cat_6_ep_id,0);

      -----------------------------------------------------------------------------
      -- (26) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_e1_item_category_7_869 IS NULL AND vs_e1_item_category_7_869 IS NOT NULL ) OR vs_p_e1_item_category_7_869 <> vs_e1_item_category_7_869 THEN
 
         vs_sql := 'SELECT t_ep_e1_item_cat_7_ep_id, e1_i_cat_desc_7 FROM   t_ep_e1_item_cat_7 WHERE  LOWER(e1_item_cat_7) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_e1_item_cat_7_ep_id, vs_e1_i_cat_desc_7_870 USING vs_e1_item_category_7_869;
 
         vs_p_e1_item_category_7_869 := vs_e1_item_category_7_869;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_e1_item_category_7_869 := NULL;
         END;
      END IF;
      vi_t_ep_e1_item_cat_7_ep_id := NVL( vi_t_ep_e1_item_cat_7_ep_id,0);

      -----------------------------------------------------------------------------
      -- (27) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_product_category_1231 IS NULL AND vs_ebs_product_category_1231 IS NOT NULL ) OR vs_p_ebs_product_category_1231 <> vs_ebs_product_category_1231 THEN
 
         vs_sql := 'SELECT t_ep_ebs_prod_cat_ep_id, ebs_prod_cat_desc FROM   t_ep_ebs_prod_cat WHERE  LOWER(ebs_prod_cat) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_prod_cat_ep_id, vs_ebs_prod_cat_desc_1232 USING vs_ebs_product_category_1231;
 
         vs_p_ebs_product_category_1231 := vs_ebs_product_category_1231;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_product_category_1231 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_prod_cat_ep_id := NVL( vi_t_ep_ebs_prod_cat_ep_id,0);

      -----------------------------------------------------------------------------
      -- (28) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_product_family_c_1234 IS NULL AND vs_ebs_product_family_c_1234 IS NOT NULL ) OR vs_p_ebs_product_family_c_1234 <> vs_ebs_product_family_c_1234 THEN
 
         vs_sql := 'SELECT t_ep_ebs_prod_family_ep_id, ebs_prod_fmly_desc FROM   t_ep_ebs_prod_family WHERE  LOWER(ebs_prod_family) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_prod_family_ep_id, vs_ebs_prod_fmly_desc_1235 USING vs_ebs_product_family_c_1234;
 
         vs_p_ebs_product_family_c_1234 := vs_ebs_product_family_c_1234;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_product_family_c_1234 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_prod_family_ep_id := NVL( vi_t_ep_ebs_prod_family_ep_id,0);

      -----------------------------------------------------------------------------
      -- (29) Performance - use the previous ID value if the CODE value did not change
      -----------------------------------------------------------------------------
      IF (vs_p_ebs_demand_class_cod_1237 IS NULL AND vs_ebs_demand_class_cod_1237 IS NOT NULL ) OR vs_p_ebs_demand_class_cod_1237 <> vs_ebs_demand_class_cod_1237 THEN
 
         vs_sql := 'SELECT t_ep_ebs_demand_class_ep_id, ebs_demand_cls_desc FROM   t_ep_ebs_demand_class WHERE  LOWER(ebs_demand_class) = :1';
         BEGIN
         EXECUTE IMMEDIATE vs_sql INTO vi_t_ep_ebs_demand_class_ep_id, vs_ebs_demand_cls_desc_1238 USING vs_ebs_demand_class_cod_1237;
 
         vs_p_ebs_demand_class_cod_1237 := vs_ebs_demand_class_cod_1237;

         EXCEPTION
            WHEN OTHERS THEN
            vs_p_ebs_demand_class_cod_1237 := NULL;
         END;
      END IF;
      vi_t_ep_ebs_demand_class_ep_id := NVL( vi_t_ep_ebs_demand_class_ep_id,0);


      vs_description := vs_p1_desc_597
                             ||vs_dm_item_desc_836
                             ||vs_ebs_demand_cls_desc_1238
;

 
      --------------------------------------------------------------------------
      -- LOOP - USE THE ABOVE CURSOR VALUES TO GET THE LATEST VALUES (DESCRIPTION)
      --        FROM THE DIMENSIONS LEVELS TABLES
      --
      -- MERGE INTO dimension table
      --------------------------------------------------------------------------
 
 
      vs_sql := 'MERGE INTO items i
                 USING (SELECT DISTINCT
                               ''2'' item_status,
                               3  is_fictive,
                               '|| vi_t_ep_p1_ep_id ||' t_ep_p1_ep_id,
                               '|| vi_t_ep_p2_ep_id ||' t_ep_p2_ep_id,
                               '|| vi_t_ep_p3_ep_id ||' t_ep_p3_ep_id,
                               '|| vi_t_ep_p4_ep_id ||' t_ep_p4_ep_id,
                               '|| vi_t_ep_p2a_ep_id ||' t_ep_p2a_ep_id,
                               '|| vi_t_ep_p2a1_ep_id ||' t_ep_p2a1_ep_id,
                               '|| vi_t_ep_p2a2_ep_id ||' t_ep_p2a2_ep_id,
                               '|| vi_t_ep_p2b_ep_id ||' t_ep_p2b_ep_id,
                               '|| vi_t_ep_I_ATT_1_ep_id ||' t_ep_I_ATT_1_ep_id,
                               '|| vi_t_ep_I_ATT_2_ep_id ||' t_ep_I_ATT_2_ep_id,
                               '|| vi_t_ep_I_ATT_3_ep_id ||' t_ep_I_ATT_3_ep_id,
                               '|| vi_t_ep_I_ATT_4_ep_id ||' t_ep_I_ATT_4_ep_id,
                               '|| vi_t_ep_I_ATT_5_ep_id ||' t_ep_I_ATT_5_ep_id,
                               '|| vi_t_ep_I_ATT_6_ep_id ||' t_ep_I_ATT_6_ep_id,
                               '|| vi_t_ep_I_ATT_7_ep_id ||' t_ep_I_ATT_7_ep_id,
                               '|| vi_t_ep_I_ATT_8_ep_id ||' t_ep_I_ATT_8_ep_id,
                               '|| vi_t_ep_I_ATT_9_ep_id ||' t_ep_I_ATT_9_ep_id,
                               '|| vi_t_ep_I_ATT_10_ep_id ||' t_ep_I_ATT_10_ep_id,
                               '|| vi_t_ep_item_ep_id ||' t_ep_item_ep_id,
                               '|| vi_t_ep_e1_item_cat_1_ep_id ||' t_ep_e1_item_cat_1_ep_id,
                               '|| vi_t_ep_e1_item_cat_2_ep_id ||' t_ep_e1_item_cat_2_ep_id,
                               '|| vi_t_ep_e1_item_cat_3_ep_id ||' t_ep_e1_item_cat_3_ep_id,
                               '|| vi_t_ep_e1_item_cat_4_ep_id ||' t_ep_e1_item_cat_4_ep_id,
                               '|| vi_t_ep_e1_item_cat_5_ep_id ||' t_ep_e1_item_cat_5_ep_id,
                               '|| vi_t_ep_e1_item_cat_6_ep_id ||' t_ep_e1_item_cat_6_ep_id,
                               '|| vi_t_ep_e1_item_cat_7_ep_id ||' t_ep_e1_item_cat_7_ep_id,
                               '|| vi_t_ep_ebs_prod_cat_ep_id ||' t_ep_ebs_prod_cat_ep_id,
                               '|| vi_t_ep_ebs_prod_family_ep_id ||' t_ep_ebs_prod_family_ep_id,
                               '|| vi_t_ep_ebs_demand_class_ep_id ||' t_ep_ebs_demand_class_ep_id,
                               :1 item_desc
                        FROM   DUAL
                       ) s
                 ON 
      (    i.t_ep_ebs_demand_class_ep_id   = s.t_ep_ebs_demand_class_ep_id
       AND i.t_ep_item_ep_id               = s.t_ep_item_ep_id
       AND i.t_ep_p1_ep_id                 = s.t_ep_p1_ep_id)
                 WHEN MATCHED THEN UPDATE SET
             i.last_update_date          = '''|| vd_update_date ||''',
             i.is_fictive                = s.is_fictive,
             i.item_desc = s.item_desc
,             i.t_ep_p2_ep_id                   = s.t_ep_p2_ep_id,
             i.t_ep_p3_ep_id                   = s.t_ep_p3_ep_id,
             i.t_ep_p4_ep_id                   = s.t_ep_p4_ep_id,
             i.t_ep_p2a_ep_id                  = s.t_ep_p2a_ep_id,
             i.t_ep_p2a1_ep_id                 = s.t_ep_p2a1_ep_id,
             i.t_ep_p2a2_ep_id                 = s.t_ep_p2a2_ep_id,
             i.t_ep_p2b_ep_id                  = s.t_ep_p2b_ep_id,
             i.t_ep_i_att_1_ep_id              = s.t_ep_i_att_1_ep_id,
             i.t_ep_i_att_2_ep_id              = s.t_ep_i_att_2_ep_id,
             i.t_ep_i_att_3_ep_id              = s.t_ep_i_att_3_ep_id,
             i.t_ep_i_att_4_ep_id              = s.t_ep_i_att_4_ep_id,
             i.t_ep_i_att_5_ep_id              = s.t_ep_i_att_5_ep_id,
             i.t_ep_i_att_6_ep_id              = s.t_ep_i_att_6_ep_id,
             i.t_ep_i_att_7_ep_id              = s.t_ep_i_att_7_ep_id,
             i.t_ep_i_att_8_ep_id              = s.t_ep_i_att_8_ep_id,
             i.t_ep_i_att_9_ep_id              = s.t_ep_i_att_9_ep_id,
             i.t_ep_i_att_10_ep_id             = s.t_ep_i_att_10_ep_id,
             i.t_ep_e1_item_cat_1_ep_id        = s.t_ep_e1_item_cat_1_ep_id,
             i.t_ep_e1_item_cat_2_ep_id        = s.t_ep_e1_item_cat_2_ep_id,
             i.t_ep_e1_item_cat_3_ep_id        = s.t_ep_e1_item_cat_3_ep_id,
             i.t_ep_e1_item_cat_4_ep_id        = s.t_ep_e1_item_cat_4_ep_id,
             i.t_ep_e1_item_cat_5_ep_id        = s.t_ep_e1_item_cat_5_ep_id,
             i.t_ep_e1_item_cat_6_ep_id        = s.t_ep_e1_item_cat_6_ep_id,
             i.t_ep_e1_item_cat_7_ep_id        = s.t_ep_e1_item_cat_7_ep_id,
             i.t_ep_ebs_prod_cat_ep_id         = s.t_ep_ebs_prod_cat_ep_id,
             i.t_ep_ebs_prod_family_ep_id      = s.t_ep_ebs_prod_family_ep_id
                 WHEN NOT MATCHED THEN INSERT    (
         item_id,
         item_status,
         is_fictive, 
         t_ep_ebs_demand_class_ep_id , 
         t_ep_p2b_ep_id , 
         t_ep_i_att_1_ep_id , 
         t_ep_i_att_2_ep_id , 
         t_ep_i_att_3_ep_id , 
         t_ep_i_att_4_ep_id , 
         t_ep_i_att_5_ep_id , 
         t_ep_i_att_6_ep_id , 
         t_ep_i_att_7_ep_id , 
         t_ep_i_att_8_ep_id , 
         t_ep_i_att_9_ep_id , 
         t_ep_i_att_10_ep_id , 
         t_ep_p4_ep_id , 
         t_ep_p3_ep_id , 
         t_ep_p2_ep_id , 
         t_ep_p2a2_ep_id , 
         t_ep_p2a1_ep_id , 
         t_ep_p2a_ep_id , 
         t_ep_ebs_prod_family_ep_id , 
         t_ep_ebs_prod_cat_ep_id , 
         t_ep_e1_item_cat_7_ep_id , 
         t_ep_e1_item_cat_6_ep_id , 
         t_ep_e1_item_cat_5_ep_id , 
         t_ep_e1_item_cat_4_ep_id , 
         t_ep_e1_item_cat_3_ep_id , 
         t_ep_e1_item_cat_2_ep_id , 
         t_ep_e1_item_cat_1_ep_id , 
         t_ep_item_ep_id , 
         t_ep_p1_ep_id , 
         item_desc,
         last_update_date)
      VALUES    (
         items_seq.NEXTVAL,
         s.item_status,
         s.is_fictive, 
         s.t_ep_ebs_demand_class_ep_id , 
         s.t_ep_p2b_ep_id , 
         s.t_ep_i_att_1_ep_id , 
         s.t_ep_i_att_2_ep_id , 
         s.t_ep_i_att_3_ep_id , 
         s.t_ep_i_att_4_ep_id , 
         s.t_ep_i_att_5_ep_id , 
         s.t_ep_i_att_6_ep_id , 
         s.t_ep_i_att_7_ep_id , 
         s.t_ep_i_att_8_ep_id , 
         s.t_ep_i_att_9_ep_id , 
         s.t_ep_i_att_10_ep_id , 
         s.t_ep_p4_ep_id , 
         s.t_ep_p3_ep_id , 
         s.t_ep_p2_ep_id , 
         s.t_ep_p2a2_ep_id , 
         s.t_ep_p2a1_ep_id , 
         s.t_ep_p2a_ep_id , 
         s.t_ep_ebs_prod_family_ep_id , 
         s.t_ep_ebs_prod_cat_ep_id , 
         s.t_ep_e1_item_cat_7_ep_id , 
         s.t_ep_e1_item_cat_6_ep_id , 
         s.t_ep_e1_item_cat_5_ep_id , 
         s.t_ep_e1_item_cat_4_ep_id , 
         s.t_ep_e1_item_cat_3_ep_id , 
         s.t_ep_e1_item_cat_2_ep_id , 
         s.t_ep_e1_item_cat_1_ep_id , 
         s.t_ep_item_ep_id , 
         s.t_ep_p1_ep_id , 
         s.item_desc,
         '''|| vd_load_date ||''' )';


 
      BEGIN
 
         EXECUTE IMMEDIATE vs_sql USING vs_description;
 
         vs_dim_merge_count := vs_dim_merge_count + sql%rowcount;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SQLERRM;
 
            vs_status := 'FAILED';
 
            IF vi_error_code = -30926 THEN
               /************************************************************************/
               /* ORA-30926: unable to get a stable set of rows in the source tables
               /************************************************************************/
 
               vs_msg     := 'The MERGE into items failed : '|| vs_error_text;                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
               vs_msg     := 'The MERGE into items failed : There are duplicate base levels with differing level codes in the staging table';
                                                                                                                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
 
            ELSE
 
               vs_msg     := 'The MERGE into items failed : '|| vs_error_text;                          DBEX( vs_msg , vs_proc, 'E' );
               vs_log_msg := vs_msg;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);

               RAISE;

            END IF;
      END;


      COMMIT;

   END LOOP;
   CLOSE rec_load;

   COMMIT;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE
   ----------------------------------------------------------------------------------
   vs_log_msg := 'LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   ----------------------------------------------------------------------------------
   -- LOOP - UPDATE ALL ROWS IN THE DIMENSION TABLE
   ----------------------------------------------------------------------------------
   vs_dim_level_update_count := 0;
   dv_rowcounter := 0;
 
   FOR rec IN (
       SELECT 
          t_ep_ebs_demand_class.t_ep_ebs_demand_class_ep_id , 
          t_ep_p2b.t_ep_p2b_ep_id , 
          t_ep_i_att_1.t_ep_i_att_1_ep_id , 
          t_ep_i_att_2.t_ep_i_att_2_ep_id , 
          t_ep_i_att_3.t_ep_i_att_3_ep_id , 
          t_ep_i_att_4.t_ep_i_att_4_ep_id , 
          t_ep_i_att_5.t_ep_i_att_5_ep_id , 
          t_ep_i_att_6.t_ep_i_att_6_ep_id , 
          t_ep_i_att_7.t_ep_i_att_7_ep_id , 
          t_ep_i_att_8.t_ep_i_att_8_ep_id , 
          t_ep_i_att_9.t_ep_i_att_9_ep_id , 
          t_ep_i_att_10.t_ep_i_att_10_ep_id , 
          t_ep_p4.t_ep_p4_ep_id , 
          t_ep_p3.t_ep_p3_ep_id , 
          t_ep_p2.t_ep_p2_ep_id , 
          t_ep_p2a2.t_ep_p2a2_ep_id , 
          t_ep_p2a1.t_ep_p2a1_ep_id , 
          t_ep_p2a.t_ep_p2a_ep_id , 
          t_ep_ebs_prod_family.t_ep_ebs_prod_family_ep_id , 
          t_ep_ebs_prod_cat.t_ep_ebs_prod_cat_ep_id , 
          t_ep_e1_item_cat_7.t_ep_e1_item_cat_7_ep_id , 
          t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id , 
          t_ep_e1_item_cat_5.t_ep_e1_item_cat_5_ep_id , 
          t_ep_e1_item_cat_4.t_ep_e1_item_cat_4_ep_id , 
          t_ep_e1_item_cat_3.t_ep_e1_item_cat_3_ep_id , 
          t_ep_e1_item_cat_2.t_ep_e1_item_cat_2_ep_id , 
          t_ep_e1_item_cat_1.t_ep_e1_item_cat_1_ep_id , 
          t_ep_item.t_ep_item_ep_id , 
          t_ep_p1.t_ep_p1_ep_id , 
              
           t_ep_ebs_demand_class.ebs_demand_cls_desc ||
           t_ep_item.dm_item_desc ||
           t_ep_p1.p1_desc  item_desc
       FROM items,t_ep_ebs_demand_class,t_ep_p2b,t_ep_I_ATT_1,t_ep_I_ATT_2,t_ep_I_ATT_3,t_ep_I_ATT_4,t_ep_I_ATT_5,t_ep_I_ATT_6,t_ep_I_ATT_7,t_ep_I_ATT_8,t_ep_I_ATT_9,t_ep_I_ATT_10,t_ep_p4,t_ep_p3,t_ep_p2,t_ep_p2a2,t_ep_p2a1,t_ep_p2a,t_ep_ebs_prod_family,
t_ep_ebs_prod_cat,t_ep_e1_item_cat_7,t_ep_e1_item_cat_6,t_ep_e1_item_cat_5,t_ep_e1_item_cat_4,t_ep_e1_item_cat_3,t_ep_e1_item_cat_2,t_ep_e1_item_cat_1,t_ep_item,t_ep_p1
       WHERE 1 = 1
         AND t_ep_ebs_demand_class.t_ep_ebs_demand_class_ep_id  = items.t_ep_ebs_demand_class_ep_id
         AND t_ep_item.t_ep_p2b_ep_id                           = t_ep_p2b.t_ep_p2b_ep_id
         AND t_ep_item.t_ep_I_ATT_1_ep_id                       = t_ep_I_ATT_1.t_ep_I_ATT_1_ep_id
         AND t_ep_item.t_ep_I_ATT_2_ep_id                       = t_ep_I_ATT_2.t_ep_I_ATT_2_ep_id
         AND t_ep_item.t_ep_I_ATT_3_ep_id                       = t_ep_I_ATT_3.t_ep_I_ATT_3_ep_id
         AND t_ep_item.t_ep_I_ATT_4_ep_id                       = t_ep_I_ATT_4.t_ep_I_ATT_4_ep_id
         AND t_ep_item.t_ep_I_ATT_5_ep_id                       = t_ep_I_ATT_5.t_ep_I_ATT_5_ep_id
         AND t_ep_item.t_ep_I_ATT_6_ep_id                       = t_ep_I_ATT_6.t_ep_I_ATT_6_ep_id
         AND t_ep_item.t_ep_I_ATT_7_ep_id                       = t_ep_I_ATT_7.t_ep_I_ATT_7_ep_id
         AND t_ep_item.t_ep_I_ATT_8_ep_id                       = t_ep_I_ATT_8.t_ep_I_ATT_8_ep_id
         AND t_ep_item.t_ep_I_ATT_9_ep_id                       = t_ep_I_ATT_9.t_ep_I_ATT_9_ep_id
         AND t_ep_item.t_ep_I_ATT_10_ep_id                      = t_ep_I_ATT_10.t_ep_I_ATT_10_ep_id
         AND t_ep_p3.t_ep_p4_ep_id                              = t_ep_p4.t_ep_p4_ep_id
         AND t_ep_p2.t_ep_p3_ep_id                              = t_ep_p3.t_ep_p3_ep_id
         AND t_ep_item.t_ep_p2_ep_id                            = t_ep_p2.t_ep_p2_ep_id
         AND t_ep_p2a1.t_ep_p2a2_ep_id                          = t_ep_p2a2.t_ep_p2a2_ep_id
         AND t_ep_p2a.t_ep_p2a1_ep_id                           = t_ep_p2a1.t_ep_p2a1_ep_id
         AND t_ep_item.t_ep_p2a_ep_id                           = t_ep_p2a.t_ep_p2a_ep_id
         AND t_ep_item.t_ep_ebs_prod_family_ep_id               = t_ep_ebs_prod_family.t_ep_ebs_prod_family_ep_id
         AND t_ep_item.t_ep_ebs_prod_cat_ep_id                  = t_ep_ebs_prod_cat.t_ep_ebs_prod_cat_ep_id
         AND t_ep_item.t_ep_e1_item_cat_7_ep_id                 = t_ep_e1_item_cat_7.t_ep_e1_item_cat_7_ep_id
         AND t_ep_item.t_ep_e1_item_cat_6_ep_id                 = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
         AND t_ep_item.t_ep_e1_item_cat_5_ep_id                 = t_ep_e1_item_cat_5.t_ep_e1_item_cat_5_ep_id
         AND t_ep_item.t_ep_e1_item_cat_4_ep_id                 = t_ep_e1_item_cat_4.t_ep_e1_item_cat_4_ep_id
         AND t_ep_item.t_ep_e1_item_cat_3_ep_id                 = t_ep_e1_item_cat_3.t_ep_e1_item_cat_3_ep_id
         AND t_ep_item.t_ep_e1_item_cat_2_ep_id                 = t_ep_e1_item_cat_2.t_ep_e1_item_cat_2_ep_id
         AND t_ep_item.t_ep_e1_item_cat_1_ep_id                 = t_ep_e1_item_cat_1.t_ep_e1_item_cat_1_ep_id
         AND t_ep_item.t_ep_item_ep_id                          = items.t_ep_item_ep_id
         AND t_ep_p1.t_ep_p1_ep_id                              = items.t_ep_p1_ep_id)
   LOOP

 
      UPDATE items
      SET    item_desc = rec.item_desc,
             
             t_ep_ebs_demand_class_ep_id                        = rec.t_ep_ebs_demand_class_ep_id,
             t_ep_p2b_ep_id                                     = rec.t_ep_p2b_ep_id,
             t_ep_I_ATT_1_ep_id                                 = rec.t_ep_I_ATT_1_ep_id,
             t_ep_I_ATT_2_ep_id                                 = rec.t_ep_I_ATT_2_ep_id,
             t_ep_I_ATT_3_ep_id                                 = rec.t_ep_I_ATT_3_ep_id,
             t_ep_I_ATT_4_ep_id                                 = rec.t_ep_I_ATT_4_ep_id,
             t_ep_I_ATT_5_ep_id                                 = rec.t_ep_I_ATT_5_ep_id,
             t_ep_I_ATT_6_ep_id                                 = rec.t_ep_I_ATT_6_ep_id,
             t_ep_I_ATT_7_ep_id                                 = rec.t_ep_I_ATT_7_ep_id,
             t_ep_I_ATT_8_ep_id                                 = rec.t_ep_I_ATT_8_ep_id,
             t_ep_I_ATT_9_ep_id                                 = rec.t_ep_I_ATT_9_ep_id,
             t_ep_I_ATT_10_ep_id                                = rec.t_ep_I_ATT_10_ep_id,
             t_ep_p4_ep_id                                      = rec.t_ep_p4_ep_id,
             t_ep_p3_ep_id                                      = rec.t_ep_p3_ep_id,
             t_ep_p2_ep_id                                      = rec.t_ep_p2_ep_id,
             t_ep_p2a2_ep_id                                    = rec.t_ep_p2a2_ep_id,
             t_ep_p2a1_ep_id                                    = rec.t_ep_p2a1_ep_id,
             t_ep_p2a_ep_id                                     = rec.t_ep_p2a_ep_id,
             t_ep_ebs_prod_family_ep_id                         = rec.t_ep_ebs_prod_family_ep_id,
             t_ep_ebs_prod_cat_ep_id                            = rec.t_ep_ebs_prod_cat_ep_id,
             t_ep_e1_item_cat_7_ep_id                           = rec.t_ep_e1_item_cat_7_ep_id,
             t_ep_e1_item_cat_6_ep_id                           = rec.t_ep_e1_item_cat_6_ep_id,
             t_ep_e1_item_cat_5_ep_id                           = rec.t_ep_e1_item_cat_5_ep_id,
             t_ep_e1_item_cat_4_ep_id                           = rec.t_ep_e1_item_cat_4_ep_id,
             t_ep_e1_item_cat_3_ep_id                           = rec.t_ep_e1_item_cat_3_ep_id,
             t_ep_e1_item_cat_2_ep_id                           = rec.t_ep_e1_item_cat_2_ep_id,
             t_ep_e1_item_cat_1_ep_id                           = rec.t_ep_e1_item_cat_1_ep_id,
             t_ep_item_ep_id                                    = rec.t_ep_item_ep_id,
             t_ep_p1_ep_id                                      = rec.t_ep_p1_ep_id,
              last_update_date = vd_update_date
      WHERE  1 = 1
     
         AND t_ep_ebs_demand_class_ep_id                        = rec.t_ep_ebs_demand_class_ep_id 
         AND t_ep_item_ep_id                                    = rec.t_ep_item_ep_id 
         AND t_ep_p1_ep_id                                      = rec.t_ep_p1_ep_id 
      AND  (
          items.t_ep_p2b_ep_id                               <> rec.t_ep_p2b_ep_id
   OR     items.t_ep_I_ATT_1_ep_id                           <> rec.t_ep_I_ATT_1_ep_id
   OR     items.t_ep_I_ATT_2_ep_id                           <> rec.t_ep_I_ATT_2_ep_id
   OR     items.t_ep_I_ATT_3_ep_id                           <> rec.t_ep_I_ATT_3_ep_id
   OR     items.t_ep_I_ATT_4_ep_id                           <> rec.t_ep_I_ATT_4_ep_id
   OR     items.t_ep_I_ATT_5_ep_id                           <> rec.t_ep_I_ATT_5_ep_id
   OR     items.t_ep_I_ATT_6_ep_id                           <> rec.t_ep_I_ATT_6_ep_id
   OR     items.t_ep_I_ATT_7_ep_id                           <> rec.t_ep_I_ATT_7_ep_id
   OR     items.t_ep_I_ATT_8_ep_id                           <> rec.t_ep_I_ATT_8_ep_id
   OR     items.t_ep_I_ATT_9_ep_id                           <> rec.t_ep_I_ATT_9_ep_id
   OR     items.t_ep_I_ATT_10_ep_id                          <> rec.t_ep_I_ATT_10_ep_id
   OR     items.t_ep_p4_ep_id                                <> rec.t_ep_p4_ep_id
   OR     items.t_ep_p3_ep_id                                <> rec.t_ep_p3_ep_id
   OR     items.t_ep_p2_ep_id                                <> rec.t_ep_p2_ep_id
   OR     items.t_ep_p2a2_ep_id                              <> rec.t_ep_p2a2_ep_id
   OR     items.t_ep_p2a1_ep_id                              <> rec.t_ep_p2a1_ep_id
   OR     items.t_ep_p2a_ep_id                               <> rec.t_ep_p2a_ep_id
   OR     items.t_ep_ebs_prod_family_ep_id                   <> rec.t_ep_ebs_prod_family_ep_id
   OR     items.t_ep_ebs_prod_cat_ep_id                      <> rec.t_ep_ebs_prod_cat_ep_id
   OR     items.t_ep_e1_item_cat_7_ep_id                     <> rec.t_ep_e1_item_cat_7_ep_id
   OR     items.t_ep_e1_item_cat_6_ep_id                     <> rec.t_ep_e1_item_cat_6_ep_id
   OR     items.t_ep_e1_item_cat_5_ep_id                     <> rec.t_ep_e1_item_cat_5_ep_id
   OR     items.t_ep_e1_item_cat_4_ep_id                     <> rec.t_ep_e1_item_cat_4_ep_id
   OR     items.t_ep_e1_item_cat_3_ep_id                     <> rec.t_ep_e1_item_cat_3_ep_id
   OR     items.t_ep_e1_item_cat_2_ep_id                     <> rec.t_ep_e1_item_cat_2_ep_id
   OR     items.t_ep_e1_item_cat_1_ep_id                     <> rec.t_ep_e1_item_cat_1_ep_id);

      vs_dim_level_update_count := vs_dim_level_update_count + sql%rowcount;


      IF dv_rowcounter >= dv_sys_rowcounter THEN
         COMMIT;
         dv_rowcounter := 0;
      ELSE
         dv_rowcounter := dv_rowcounter + 1;
      END IF;


   END LOOP;
   COMMIT;

   vs_log_msg := 'Rows Updated in the dimension : '|| vs_dim_level_update_count;                                     LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
   ---------------------------------------------------------------------------------------------
   -- Total of the rows MERGED in to items
   ---------------------------------------------------------------------------------------------

   vs_log_msg := 'Rows merged (inserted or updated) in ITEMS : '|| vs_dim_merge_count;        LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   ---------------------------------------------------------------------------------------------
   -- BULK INSERT :
   -- Make a list of all updated dimension rows
   ---------------------------------------------------------------------------------------------
   vs_log_msg := 'BULK INSERT : GLOB_MDP_ADD_ITEMS';                                                       LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   vs_sql := '
         INSERT INTO GLOB_MDP_ADD_ITEMS (item_id,
               t_ep_I_ATT_1_ep_id,
               t_ep_I_ATT_10_ep_id,
               t_ep_I_ATT_2_ep_id,
               t_ep_I_ATT_3_ep_id,
               t_ep_I_ATT_4_ep_id,
               t_ep_I_ATT_5_ep_id,
               t_ep_I_ATT_6_ep_id,
               t_ep_I_ATT_7_ep_id,
               t_ep_I_ATT_8_ep_id,
               t_ep_I_ATT_9_ep_id,
               t_ep_e1_item_cat_1_ep_id,
               t_ep_e1_item_cat_2_ep_id,
               t_ep_e1_item_cat_3_ep_id,
               t_ep_e1_item_cat_4_ep_id,
               t_ep_e1_item_cat_5_ep_id,
               t_ep_e1_item_cat_6_ep_id,
               t_ep_e1_item_cat_7_ep_id,
               t_ep_ebs_demand_class_ep_id,
               t_ep_ebs_prod_cat_ep_id,
               t_ep_ebs_prod_family_ep_id,
               t_ep_item_ep_id,
               t_ep_p1_ep_id,
               t_ep_p2_ep_id,
               t_ep_p2a_ep_id,
               t_ep_p2a1_ep_id,
               t_ep_p2a2_ep_id,
               t_ep_p2b_ep_id,
               t_ep_p3_ep_id,
               t_ep_p4_ep_id )
   SELECT DISTINCT item_id,
               t_ep_I_ATT_1_ep_id,
               t_ep_I_ATT_10_ep_id,
               t_ep_I_ATT_2_ep_id,
               t_ep_I_ATT_3_ep_id,
               t_ep_I_ATT_4_ep_id,
               t_ep_I_ATT_5_ep_id,
               t_ep_I_ATT_6_ep_id,
               t_ep_I_ATT_7_ep_id,
               t_ep_I_ATT_8_ep_id,
               t_ep_I_ATT_9_ep_id,
               t_ep_e1_item_cat_1_ep_id,
               t_ep_e1_item_cat_2_ep_id,
               t_ep_e1_item_cat_3_ep_id,
               t_ep_e1_item_cat_4_ep_id,
               t_ep_e1_item_cat_5_ep_id,
               t_ep_e1_item_cat_6_ep_id,
               t_ep_e1_item_cat_7_ep_id,
               t_ep_ebs_demand_class_ep_id,
               t_ep_ebs_prod_cat_ep_id,
               t_ep_ebs_prod_family_ep_id,
               t_ep_item_ep_id,
               t_ep_p1_ep_id,
               t_ep_p2_ep_id,
               t_ep_p2a_ep_id,
               t_ep_p2a1_ep_id,
               t_ep_p2a2_ep_id,
               t_ep_p2b_ep_id,
               t_ep_p3_ep_id,
               t_ep_p4_ep_id
   FROM   items
   WHERE  last_update_date = '''|| vd_update_date ||'''';
 
   dynamic_ddl(vs_sql);
 
   vs_log_msg := 'Rows inserted in to GLOB_MDP_ADD_ITEMS : '|| sql%rowcount;
   COMMIT;
 
   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);

   ----------------------------------------------------------------------------
   -- New MDP_MATRIX MERGE update code being driven only by GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim
   ----------------------------------------------------------------------------------
   vs_log_msg := 'MDP_MATRIX MERGE update from GLOB_MDP_ADD_dim';                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
 
   vs_log_msg := 'MDP_MATRIX MERGE update code being driven only by GLOB_MDP_ADD_ITEMS';                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   MERGE /*+ parallel(m,24)*/  INTO mdp_matrix m
   USING (SELECT /*+ parallel(a,24)*/  DISTINCT
                 item_id,
               t_ep_I_ATT_1_ep_id,
               t_ep_I_ATT_10_ep_id,
               t_ep_I_ATT_2_ep_id,
               t_ep_I_ATT_3_ep_id,
               t_ep_I_ATT_4_ep_id,
               t_ep_I_ATT_5_ep_id,
               t_ep_I_ATT_6_ep_id,
               t_ep_I_ATT_7_ep_id,
               t_ep_I_ATT_8_ep_id,
               t_ep_I_ATT_9_ep_id,
               t_ep_e1_item_cat_1_ep_id,
               t_ep_e1_item_cat_2_ep_id,
               t_ep_e1_item_cat_3_ep_id,
               t_ep_e1_item_cat_4_ep_id,
               t_ep_e1_item_cat_5_ep_id,
               t_ep_e1_item_cat_6_ep_id,
               t_ep_e1_item_cat_7_ep_id,
               t_ep_ebs_demand_class_ep_id,
               t_ep_ebs_prod_cat_ep_id,
               t_ep_ebs_prod_family_ep_id,
               t_ep_item_ep_id,
               t_ep_p1_ep_id,
               t_ep_p2_ep_id,
               t_ep_p2a_ep_id,
               t_ep_p2a1_ep_id,
               t_ep_p2a2_ep_id,
               t_ep_p2b_ep_id,
               t_ep_p3_ep_id,
               t_ep_p4_ep_id
          FROM
                GLOB_MDP_ADD_ITEMS a
         ) g
   ON    (m.item_id = g.item_id)
   WHEN MATCHED THEN UPDATE SET m.last_update_date          = SYSTIMESTAMP, 
             m.t_ep_I_ATT_1_ep_id             = g.t_ep_I_ATT_1_ep_id,
             m.t_ep_I_ATT_10_ep_id            = g.t_ep_I_ATT_10_ep_id,
             m.t_ep_I_ATT_2_ep_id             = g.t_ep_I_ATT_2_ep_id,
             m.t_ep_I_ATT_3_ep_id             = g.t_ep_I_ATT_3_ep_id,
             m.t_ep_I_ATT_4_ep_id             = g.t_ep_I_ATT_4_ep_id,
             m.t_ep_I_ATT_5_ep_id             = g.t_ep_I_ATT_5_ep_id,
             m.t_ep_I_ATT_6_ep_id             = g.t_ep_I_ATT_6_ep_id,
             m.t_ep_I_ATT_7_ep_id             = g.t_ep_I_ATT_7_ep_id,
             m.t_ep_I_ATT_8_ep_id             = g.t_ep_I_ATT_8_ep_id,
             m.t_ep_I_ATT_9_ep_id             = g.t_ep_I_ATT_9_ep_id,
             m.t_ep_e1_item_cat_1_ep_id       = g.t_ep_e1_item_cat_1_ep_id,
             m.t_ep_e1_item_cat_2_ep_id       = g.t_ep_e1_item_cat_2_ep_id,
             m.t_ep_e1_item_cat_3_ep_id       = g.t_ep_e1_item_cat_3_ep_id,
             m.t_ep_e1_item_cat_4_ep_id       = g.t_ep_e1_item_cat_4_ep_id,
             m.t_ep_e1_item_cat_5_ep_id       = g.t_ep_e1_item_cat_5_ep_id,
             m.t_ep_e1_item_cat_6_ep_id       = g.t_ep_e1_item_cat_6_ep_id,
             m.t_ep_e1_item_cat_7_ep_id       = g.t_ep_e1_item_cat_7_ep_id,
             m.t_ep_ebs_demand_class_ep_id    = g.t_ep_ebs_demand_class_ep_id,
             m.t_ep_ebs_prod_cat_ep_id        = g.t_ep_ebs_prod_cat_ep_id,
             m.t_ep_ebs_prod_family_ep_id     = g.t_ep_ebs_prod_family_ep_id,
             m.t_ep_item_ep_id                = g.t_ep_item_ep_id,
             m.t_ep_p1_ep_id                  = g.t_ep_p1_ep_id,
             m.t_ep_p2_ep_id                  = g.t_ep_p2_ep_id,
             m.t_ep_p2a_ep_id                 = g.t_ep_p2a_ep_id,
             m.t_ep_p2a1_ep_id                = g.t_ep_p2a1_ep_id,
             m.t_ep_p2a2_ep_id                = g.t_ep_p2a2_ep_id,
             m.t_ep_p2b_ep_id                 = g.t_ep_p2b_ep_id,
             m.t_ep_p3_ep_id                  = g.t_ep_p3_ep_id,
             m.t_ep_p4_ep_id                  = g.t_ep_p4_ep_id,
             m.t_ep_col_cat_lud               = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_1_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_2_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_3_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_4_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_5_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_6_lud         = SYSTIMESTAMP,
             m.t_ep_e1_item_cat_7_lud         = SYSTIMESTAMP,
             m.t_ep_ebs_demand_class_lud      = SYSTIMESTAMP,
             m.t_ep_ebs_prod_cat_lud          = SYSTIMESTAMP,
             m.t_ep_ebs_prod_family_lud       = SYSTIMESTAMP,
             m.t_ep_i_att_10_lud              = SYSTIMESTAMP,
             m.t_ep_i_att_1_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_2_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_3_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_4_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_5_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_6_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_7_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_8_lud               = SYSTIMESTAMP,
             m.t_ep_i_att_9_lud               = SYSTIMESTAMP,
             m.t_ep_item_lud                  = SYSTIMESTAMP,
             m.t_ep_manufacturer_lud          = SYSTIMESTAMP,
             m.t_ep_p1_lud                    = SYSTIMESTAMP,
             m.t_ep_p2_lud                    = SYSTIMESTAMP,
             m.t_ep_p2a1_lud                  = SYSTIMESTAMP,
             m.t_ep_p2a2_lud                  = SYSTIMESTAMP,
             m.t_ep_p2a_lud                   = SYSTIMESTAMP,
             m.t_ep_p2b_lud                   = SYSTIMESTAMP,
             m.t_ep_p3_lud                    = SYSTIMESTAMP,
             m.t_ep_p4_lud                    = SYSTIMESTAMP,
             m.t_ep_ww_cat_lud                = SYSTIMESTAMP;
 
   ---------------------------------------------------------------------------------------------
   -- Total of the rows MERGE (updated) in MDP_MATRIX
   ---------------------------------------------------------------------------------------------
   vs_mdp_update_count := sql%rowcount;
   COMMIT;
   vs_log_msg := 'Rows merged (updated) in MDP_MATRIX : '|| vs_mdp_update_count;                                     LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg, NULL , NULL);


 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                 vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;         DBEX( vs_msg, vs_proc, 'S' );

 
   -------------------------------------------------------------------------------------
   -- Clear down the Global temp table because the Apps Server may not close the session
   -------------------------------------------------------------------------------------
   vs_log_msg := 'TRUNCATE TABLE GLOB_MDP_ADD_ITEMS';
   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);

   dynamic_ddl('TRUNCATE TABLE GLOB_MDP_ADD_ITEMS');


   --------------------------------------------------------------------------
   -- Use Final COMMIT
   --------------------------------------------------------------------------
   COMMIT;

 
   ----------------------------------------------------------------------------------
   -- DB_SECTION_LOG - ANALYZE ALL THE LEVELS TABLES
   ----------------------------------------------------------------------------------
   vs_log_msg := 'Analyze all the levels tables';                                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source,'M', vs_log_msg ,  NULL , NULL , NULL);
   vs_msg     := vs_log_msg;
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_SECTION_LOG
   --------------------------------------------------------------------------
   vd_start_time := DBMS_UTILITY.get_time;

 
   --------------------------------------------------------------------------
   -- ANALYZE ALL THE LEVELS TABLES
   --------------------------------------------------------------------------
   vs_log_msg := 'ANALYZE ALL THE LEVELS TABLES';                                                                    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   
   --IF get_is_table_stats_exists   ('items') = 0 OR
   --   get_is_table_stats_rows_low ('items') = 1 THEN
      analyze_table('items',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_1') = 1 THEN
      analyze_table('t_ep_I_ATT_1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_10') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_10') = 1 THEN
      analyze_table('t_ep_I_ATT_10',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_2') = 1 THEN
      analyze_table('t_ep_I_ATT_2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_3') = 1 THEN
      analyze_table('t_ep_I_ATT_3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_4') = 1 THEN
      analyze_table('t_ep_I_ATT_4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_5') = 1 THEN
      analyze_table('t_ep_I_ATT_5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_6') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_6') = 1 THEN
      analyze_table('t_ep_I_ATT_6',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_7') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_7') = 1 THEN
      analyze_table('t_ep_I_ATT_7',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_8') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_8') = 1 THEN
      analyze_table('t_ep_I_ATT_8',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_I_ATT_9') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_I_ATT_9') = 1 THEN
      analyze_table('t_ep_I_ATT_9',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_1') = 1 THEN
      analyze_table('t_ep_e1_item_cat_1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_2') = 1 THEN
      analyze_table('t_ep_e1_item_cat_2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_3') = 1 THEN
      analyze_table('t_ep_e1_item_cat_3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_4') = 1 THEN
      analyze_table('t_ep_e1_item_cat_4',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_5') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_5') = 1 THEN
      analyze_table('t_ep_e1_item_cat_5',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_6') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_6') = 1 THEN
      analyze_table('t_ep_e1_item_cat_6',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_e1_item_cat_7') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_e1_item_cat_7') = 1 THEN
      analyze_table('t_ep_e1_item_cat_7',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_demand_class') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_demand_class') = 1 THEN
      analyze_table('t_ep_ebs_demand_class',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_prod_cat') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_prod_cat') = 1 THEN
      analyze_table('t_ep_ebs_prod_cat',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_ebs_prod_family') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_ebs_prod_family') = 1 THEN
      analyze_table('t_ep_ebs_prod_family',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_item') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_item') = 1 THEN
      analyze_table('t_ep_item',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p1') = 1 THEN
      analyze_table('t_ep_p1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p2') = 1 THEN
      analyze_table('t_ep_p2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p2a') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p2a') = 1 THEN
      analyze_table('t_ep_p2a',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p2a1') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p2a1') = 1 THEN
      analyze_table('t_ep_p2a1',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p2a2') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p2a2') = 1 THEN
      analyze_table('t_ep_p2a2',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p2b') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p2b') = 1 THEN
      analyze_table('t_ep_p2b',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p3') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p3') = 1 THEN
      analyze_table('t_ep_p3',0);
   --END IF;
   --IF get_is_table_stats_exists   ('t_ep_p4') = 0 OR
   --   get_is_table_stats_rows_low ('t_ep_p4') = 1 THEN
      analyze_table('t_ep_p4',0);
   --END IF;

 
    --------------------------------------------------------------------------
    -- Record the start and end times to be recorded in DB_SECTION_LOG
    --------------------------------------------------------------------------
   vs_section    := vs_msg;
   vd_end_time   := DBMS_UTILITY.get_time;                      vi_sec_log_id := vi_sec_log_id + 1;                    vs_sec_log_id := LPAD(TO_CHAR( vi_sec_log_id ),6,'0') ||' ';
   vt_duration   := ( vd_end_time - vd_start_time ) / 100 ;     vs_duration   := LPAD(TO_CHAR(vt_duration),8);
   vs_msg        := vs_sec_log_id ||'Section Completed in '|| vs_duration ||' seconds - '|| vs_section;            DBEX( vs_msg, vs_proc, 'S' );

 
 
   vs_log_msg := 'End of procedure';                                                                                 LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
 
   set_module ('E');
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_end_time  := DBMS_UTILITY.get_time;
   vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
   IF vs_status = 'SUCCESS' THEN
 
      vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - items MERGE [ '|| TO_CHAR(NVL(vs_dim_merge_count,0)) ||'] mdp_matrix MERGE ['|| TO_CHAR(NVL( vs_mdp_update_count,
0)) ||'] items Level UPDATE ['|| TO_CHAR(NVL(vs_dim_level_update_count,0)) ||'] T_SRC_ITEM_TMPL ['|| TO_CHAR(NVL(vi_stagging_count,0)) ||']';         DBEX( vs_msg, vs_proc, 'T' );
 
   ELSE
 
      vs_msg := 'Procedure Failed    in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - items MERGE [ '|| TO_CHAR(NVL(vs_dim_merge_count,0)) ||'] mdp_matrix MERGE ['|| TO_CHAR(NVL( vs_mdp_update_count,
0)) ||'] items Level UPDATE ['|| TO_CHAR(NVL(vs_dim_level_update_count,0)) ||'] T_SRC_ITEM_TMPL ['|| TO_CHAR(NVL(vi_stagging_count,0)) ||']';         DBEX( vs_msg, vs_proc, 'T' );

   END IF;
 END;

/
