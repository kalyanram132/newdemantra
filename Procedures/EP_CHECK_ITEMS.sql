--------------------------------------------------------
--  DDL for Procedure EP_CHECK_ITEMS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."EP_CHECK_ITEMS" -- $Revision: 1.200.14.3.2.2 $

      (dis_append IN VARCHAR2 DEFAULT NULL)
IS
   vs_sql                     VARCHAR2(32000);
   vs_msg                     VARCHAR2(2000);
   vs_proc                    VARCHAR2(30) := 'EP_CHECK_ITEMS';
 
   vi_error_code              NUMBER;
   vs_error_text              VARCHAR2(4000);
 
   dvi_exists                 INTEGER;
   dvs_sqlstr                 VARCHAR2(32000);
   dvs_sqltmp                 VARCHAR2(32000);
   dvi_err_code               INTEGER;
   dvs_err_msg                VARCHAR2(2000);
   dvs_field_name             VARCHAR2(30);
   dvs_proc_name              VARCHAR2(30) :='EP_CHECK_ITEMS';
   dvd_sysdate                DATE         := CAST(SYSTIMESTAMP AS DATE);
   dv_max_id                  INTEGER;
 
   vi_data_err_count          INTEGER := 0;
   vi_data_err_chk_01         INTEGER := 0;
   vi_data_err_chk_02         INTEGER := 0;
   vi_data_err_chk_03         INTEGER := 0;
   vi_data_err_chk_12         INTEGER := 0;
   vi_data_errors_tot         INTEGER := 0;
 
 
   vi_logging_level           INTEGER;
   vs_log_table               VARCHAR2(30);
   vs_proc_source             VARCHAR2(60);
   vi_log_it_commit           INTEGER;
   vs_log_msg                 VARCHAR2(2000);
   vs_temp_proc_name          VARCHAR2(30);
 
   vd_start_time_proc         NUMBER;
   vd_start_time              NUMBER;
   vd_end_time                NUMBER;
 
   vt_procedure               NUMBER := 0;
 
BEGIN
 
   pre_logon;
 
   set_module ( 'S' );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_start_time_proc := DBMS_UTILITY.get_time;
 
   --------------------------------------------------------------------------
   -- Record the Parameters passed to DB_CALL_LOG
   --------------------------------------------------------------------------
   vs_msg := 'dis_append => '  || NVL( dis_append  ,'NULL');
 
   DBEX(vs_msg, vs_proc, 'C');
 
   --------------------------------------------------------------------------
   -- Initialize LOG_IT
   --------------------------------------------------------------------------
   vs_proc_source   := 'EP_CHECK_ITEMS';
   vi_logging_level := 0;
 
   get_param_log_it(vs_proc_source, vs_log_table, vi_logging_level, vi_log_it_commit);

   vs_log_msg := RPAD('Logging Level',31) ||': '|| vi_logging_level;                                               LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'SP', vs_log_msg , 'T' , 'C', NULL);


 
   --------------------------------------------------------------------------
   -- Analyzing source staging tables
   --------------------------------------------------------------------------
   vs_log_msg := 'Analyzing source staging tables';                                                                  LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   FOR rec IN (SELECT DISTINCT ept.table_name, ut.num_rows
               FROM   e_plan_tree ept, user_tables ut
               WHERE  model_version         = 15
               AND    UPPER(ept.table_name) = UPPER(ut.table_name) )
   LOOP
 
 
      vs_log_msg := 'ANALYZE_TABLE : '|| rec.table_name;                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);

      analyze_table( rec.table_name, rec.num_rows );

   END LOOP;

 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
 
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL                           INSERT to _ERR table
   --------------------------------------------------------------------------
   vs_log_msg := 'Check 01 : Code IS NULL       Insert to _ERR table';                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
   vs_sql := 'INSERT INTO t_src_item_tmpl_err
              SELECT T_SRC_ITEM_TMPL.*,1,'''|| dvd_sysdate ||''',''Code IS NULL - ''
                     || DECODE (t_ep_p1                       , NULL,''t_ep_p1; '','''')
                     || DECODE (t_ep_p2a2                     , NULL,''t_ep_p2a2; '','''')
                     || DECODE (t_ep_p2b                      , NULL,''t_ep_p2b; '','''')
                     || DECODE (t_ep_i_att_4                  , NULL,''t_ep_i_att_4; '','''')
                     || DECODE (e1_item_category_7            , NULL,''e1_item_category_7; '','''')
                     || DECODE (ebs_product_family_code       , NULL,''ebs_product_family_code; '','''')
                     || DECODE (dm_item_code                  , NULL,''dm_item_code; '','''')
                     || DECODE (t_ep_i_att_5                  , NULL,''t_ep_i_att_5; '','''')
                     || DECODE (t_ep_p3                       , NULL,''t_ep_p3; '','''')
                     || DECODE (t_ep_i_att_6                  , NULL,''t_ep_i_att_6; '','''')
                     || DECODE (t_ep_p2                       , NULL,''t_ep_p2; '','''')
                     || DECODE (t_ep_p4                       , NULL,''t_ep_p4; '','''')
                     || DECODE (e1_item_category_5            , NULL,''e1_item_category_5; '','''')
                     || DECODE (ebs_product_category_code     , NULL,''ebs_product_category_code; '','''')
                     || DECODE (t_ep_p2a1                     , NULL,''t_ep_p2a1; '','''')
                     || DECODE (e1_item_category_6            , NULL,''e1_item_category_6; '','''')
                     || DECODE (t_ep_p2a                      , NULL,''t_ep_p2a; '','''')
                     || DECODE (t_ep_i_att_2                  , NULL,''t_ep_i_att_2; '','''')
                     || DECODE (t_ep_i_att_7                  , NULL,''t_ep_i_att_7; '','''')
                     || DECODE (t_ep_i_att_9                  , NULL,''t_ep_i_att_9; '','''')
                     || DECODE (t_ep_i_att_10                 , NULL,''t_ep_i_att_10; '','''')
                     || DECODE (e1_item_category_1            , NULL,''e1_item_category_1; '','''')
                     || DECODE (e1_item_category_4            , NULL,''e1_item_category_4; '','''')
                     || DECODE (ebs_demand_class_code         , NULL,''ebs_demand_class_code; '','''')
                     || DECODE (t_ep_i_att_3                  , NULL,''t_ep_i_att_3; '','''')
                     || DECODE (t_ep_i_att_8                  , NULL,''t_ep_i_att_8; '','''')
                     || DECODE (e1_item_category_2            , NULL,''e1_item_category_2; '','''')
                     || DECODE (e1_item_category_3            , NULL,''e1_item_category_3; '','''')
              FROM   T_SRC_ITEM_TMPL
              WHERE  1 = 2
              OR     T_SRC_ITEM_TMPL.t_ep_p1 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2b IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_4 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_7 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_product_family_code IS NULL
              OR     T_SRC_ITEM_TMPL.dm_item_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_5 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p3 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_6 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p4 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_5 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_product_category_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a1 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_6 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_7 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_9 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_10 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_1 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_4 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_demand_class_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_3 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_8 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_2 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_3 IS NULL';
 
   vi_data_err_chk_01 := 0;
 
   BEGIN
      EXECUTE IMMEDIATE vs_sql;
 
      vi_data_err_chk_01 := SQL%ROWCOUNT;
      vi_data_errors_tot := vi_data_errors_tot + vi_data_err_chk_01;
 
      EXCEPTION
         WHEN OTHERS THEN
 
         vi_error_code := SQLCODE;
         vs_error_text := SUBSTR(SQLERRM,1,4000);
 
         vs_log_msg    := vs_error_text;                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
         vs_log_msg    := vs_sql;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
         vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';            DBEX ( vs_msg , vs_proc_source, 'E');
         vs_msg        := vs_log_msg;                                                                                  DBEX ( vs_msg , vs_proc_source, 'E');
   END;
 
 
   vs_log_msg  := 'Check  01 : Code IS NULL errors found : '|| vi_data_err_chk_01;                                   LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
   COMMIT;
 
   --------------------------------------------------------------------------
   -- Check 01 : Code IS NULL                          DELETE from SRC_ table
   --------------------------------------------------------------------------
   IF vi_data_err_chk_01 > 0 THEN
 
      vs_log_msg := 'Check 01 : Code IS NULL       DELETE from SRC_ table';                                          LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      vs_sql := 'DELETE T_SRC_ITEM_TMPL
                 WHERE  1 = 2
              OR     T_SRC_ITEM_TMPL.t_ep_p1 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2b IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_4 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_7 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_product_family_code IS NULL
              OR     T_SRC_ITEM_TMPL.dm_item_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_5 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p3 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_6 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p4 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_5 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_product_category_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a1 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_6 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_p2a IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_2 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_7 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_9 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_10 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_1 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_4 IS NULL
              OR     T_SRC_ITEM_TMPL.ebs_demand_class_code IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_3 IS NULL
              OR     T_SRC_ITEM_TMPL.t_ep_i_att_8 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_2 IS NULL
              OR     T_SRC_ITEM_TMPL.e1_item_category_3 IS NULL';
 
      BEGIN
 
         EXECUTE IMMEDIATE vs_sql;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SUBSTR(SQLERRM,1,4000);
 
            vs_log_msg    := vs_error_text;                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_log_msg    := vs_sql;                                                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';         DBEX ( vs_msg , vs_proc_source, 'E');
            vs_msg        := vs_log_msg;                                                                               DBEX ( vs_msg , vs_proc_source, 'E');

      END;

   END IF;

   COMMIT;

 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Check 12 : Duplicate attribute value
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   vi_data_err_count  := 0;
   vi_data_err_chk_12 := 0;
 
   FOR rec1 IN ( SELECT DISTINCT table_name
                 FROM   E_PLAN_TREE
                 WHERE  model_version      = 15
                 AND    LOWER(e_plan_type) = 'attribute')
   LOOP
      FOR rec2 IN (SELECT e1.table_name, e1.field_name attfield, e1.e_plan_name attname, e2.field_name lfield, e2.e_plan_name lname
                   FROM   E_PLAN_TREE e1, E_PLAN_TREE e2
                   WHERE  e1.model_version = 15
                   AND    e2.model_version = 15
                   AND    e1.table_name    = rec1.table_name
                   AND    LOWER( e1.e_plan_type ) = 'attribute'
                   AND    e1.e_plan_child_node    = e2.e_plan_node_id
                   AND    LOWER( e2.e_plan_type ) = 'key')
      LOOP
 
         dvs_err_msg := 'Duplicate attribute value - '|| rec2.attfield;
 
         --------------------------------------------------------------------------
         -- Check 12 : Duplicate attribute value               INSERT to _ERR table
         --------------------------------------------------------------------------
         vs_log_msg  := 'Check  12 : Duplicate attribute value : '|| rec2.attfield ||'   INSERT to _ERR table';    LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         vs_sql := 'INSERT INTO '|| rec1.table_name ||'_err
                    SELECT '|| rec1.table_name ||'.*,12 error_code_record,'''|| dvd_sysdate ||''','''|| dvs_err_msg ||''' error_message_record
                    FROM   '|| rec1.table_name ||'
                    WHERE UPPER('|| rec2.lfield ||') IN ( SELECT UPPER(t2.'|| rec2.lfield ||')
                                                            FROM   '|| rec1.table_name ||' t2
                                                            GROUP BY t2.'|| rec2.lfield ||'
                                                            HAVING COUNT(DISTINCT t2.'|| rec2.attfield ||' ) > 1)';
 
         vs_log_msg := vs_sql;                                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         BEGIN
            EXECUTE IMMEDIATE vs_sql;
 
            vi_data_err_count  := SQL%ROWCOUNT;
            vi_data_err_chk_12 := vi_data_err_chk_12 + vi_data_err_count;
            vi_data_errors_tot := vi_data_errors_tot + vi_data_err_count;
 
         EXCEPTION
            WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SUBSTR(SQLERRM,1,4000);
 
            vs_log_msg    := vs_error_text;                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_log_msg    := vs_sql;                                                                                   LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';         DBEX ( vs_msg , vs_proc_source, 'E');
            vs_msg        := vs_log_msg;                                                                               DBEX ( vs_msg , vs_proc_source, 'E');
         END;
 
 
         vs_log_msg  := 'Check 12 : Duplicate attribute value -  Errors found : '|| vi_data_err_count;               LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
         IF vi_data_err_count > 0 THEN
 
            --------------------------------------------------------------------------
            -- Check 12 : Duplicate attribute value             DELETE from SRC_ table
            --------------------------------------------------------------------------
            vs_log_msg  := 'Check  12 : Duplicate attribute value : '|| rec2.attfield ||' DELETE from SRC_ table'; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
            vs_sql := 'DELETE '|| rec1.table_name ||'
                        WHERE UPPER('|| rec2.lfield ||') IN ( SELECT UPPER('|| rec2.lfield ||')
                                                                FROM   '|| rec1.table_name ||'
                                                                GROUP BY '|| rec2.lfield ||'
                                                                HAVING COUNT(DISTINCT '|| rec2.attfield ||' ) >1)';
 
            vs_log_msg := vs_sql;                                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
            BEGIN
 
               EXECUTE IMMEDIATE vs_sql;
 
            EXCEPTION
               WHEN OTHERS THEN
 
               vi_error_code := SQLCODE;
               vs_error_text := SUBSTR(SQLERRM,1,4000);
 
               vs_log_msg    := vs_error_text;                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_log_msg    := vs_sql;                                                                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';      DBEX ( vs_msg , vs_proc_source, 'E');
               vs_msg        := vs_log_msg;                                                                            DBEX ( vs_msg , vs_proc_source, 'E');
 
            END;
 
         END IF;
 
         COMMIT;

      END LOOP;
   END LOOP;

   vs_log_msg  := 'Check 12 : Duplicate attribute value -  Errors found : '|| vi_data_err_chk_12;                    LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);

 
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   -- Check 02 : Duplicate description value
   -- Check 03 : Duplicate relation value
   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   vi_data_err_count  := 0;
   vi_data_err_chk_02 := 0;
   vi_data_err_chk_03 := 0;
 
   FOR rec IN (SELECT field_name, ems.table_name, e_plan_node_id
               FROM   EP_MODEL_SYNTAX ems,
                      E_PLAN_TREE     ept
               WHERE  LOWER(LTRIM(RTRIM( ems.table_name ))) = LOWER(LTRIM(RTRIM( ept.e_plan_table_name )))
               AND    model_version = 15
               AND    dim_type      = 1)
   LOOP
 
      vi_data_err_count := 0;
 
      SELECT DISTINCT field_name
      INTO   dvs_field_name
      FROM   E_PLAN_TREE
      WHERE  model_version      = 15
      AND    LOWER(e_plan_type) = LOWER('DESC')
      AND    e_plan_child_node  = rec.e_plan_node_id;
 
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
 
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value             INSERT to _ERR table
      --------------------------------------------------------------------------
      vs_log_msg := 'Check 02 : Duplicate description value : '|| dvs_field_name ||'   INSERT to _ERR table';      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      dvs_err_msg:='Duplicate description value - '||dvs_field_name;
 
      vs_sql := 'INSERT INTO t_src_item_tmpl_err
                 SELECT T_SRC_ITEM_TMPL.*,2 error_code_record,'''|| dvd_sysdate ||''','''|| dvs_err_msg ||''' error_message_record
                 FROM   T_SRC_ITEM_TMPL
                 WHERE UPPER('||rec.field_name||') IN ( SELECT  UPPER('||rec.field_name||')
                                                          FROM T_SRC_ITEM_TMPL
                                                          WHERE '|| dvs_field_name ||' IS NOT NULL
                                                          GROUP BY  '|| rec.field_name ||'
                                                          HAVING  COUNT ( DISTINCT '|| dvs_field_name ||') > 1 )';
 
      BEGIN
         EXECUTE IMMEDIATE vs_sql;
 
         vi_data_err_count  := SQL%ROWCOUNT;
         vi_data_err_chk_02 := vi_data_err_chk_02 + vi_data_err_count;
         vi_data_errors_tot := vi_data_errors_tot + vi_data_err_count;
 
      EXCEPTION
         WHEN OTHERS THEN
 
            vi_error_code := SQLCODE;
            vs_error_text := SUBSTR(SQLERRM,1,4000);
 
            vs_log_msg    := vs_error_text;                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
            vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';         DBEX ( vs_msg , vs_proc_source, 'E');
            vs_msg        := vs_log_msg;                                                                               DBEX ( vs_msg , vs_proc_source, 'E');
 
      END;
 
      vs_log_msg  := 'Check 02 : Duplicate description value -  Errors found : '|| vi_data_err_count;                LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
      --------------------------------------------------------------------------
      -- Check 02 : Duplicate description value           DELETE from SRC_ table
      --------------------------------------------------------------------------
      IF vi_data_err_count > 0 THEN
 
         vs_log_msg := 'Check  2 : Duplicate description value : '|| dvs_field_name ||'   DELETE from SRC_ table'; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         vs_sql := 'DELETE T_SRC_ITEM_TMPL
                     WHERE UPPER('|| rec.field_name ||') IN ( SELECT UPPER('|| rec.field_name ||')
                                                                FROM  T_SRC_ITEM_TMPL
                                                                WHERE '|| dvs_field_name ||' IS NOT NULL
                                                                GROUP BY  '|| rec.field_name ||'
                                                                HAVING  COUNT ( DISTINCT '|| dvs_field_name||') > 1 )';
         BEGIN
 
            EXECUTE IMMEDIATE vs_sql;
 
         EXCEPTION
            WHEN OTHERS THEN
 
               vi_error_code := SQLCODE;
               vs_error_text := SUBSTR(SQLERRM,1,4000);
 
               vs_log_msg    := vs_error_text;                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';      DBEX ( vs_msg , vs_proc_source, 'E');
               vs_msg        := vs_log_msg;                                                                            DBEX ( vs_msg , vs_proc_source, 'E');
 
         END;
 
      END IF;
 
      COMMIT;
 
 
 
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      -- Check 03 : Duplicate relation value
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
      vi_data_err_count := 0;
      dvs_err_msg       := 'Duplicate relation value - '||rec.field_name;
 
      FOR rec2 IN ( SELECT field_name
                    FROM   E_PLAN_TREE
                    WHERE  model_version = 15
                    AND    LOWER( e_plan_type     ) = LOWER('code')
                    AND    LOWER( e_plan_base_dim ) = 'items'
                    AND    e_plan_child_node IN ( SELECT E_PLAN_NODE_ID
                                                  FROM   E_PLAN_TREE
                                                  WHERE  model_version       = 15
                                                  AND    UPPER( field_name ) = UPPER( rec.field_name )))
      LOOP
         dvs_sqltmp := dvs_sqltmp ||' COUNT(DISTINCT '||rec2.field_name||' ) > 1 OR ';
      END LOOP;
 
      IF dvs_sqltmp IS NOT NULL THEN
 
         dvs_sqltmp := RTRIM( dvs_sqltmp, 'OR ');
 
         --------------------------------------------------------------------------
         -- Check 03 : Duplicate relation value                INSERT to _ERR table
         --------------------------------------------------------------------------
         vs_log_msg := 'Check 03 : Duplicate relation value : '|| rec.field_name ||'   INSERT to _ERR table';      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
         vs_sql := 'INSERT INTO t_src_item_tmpl_err
             SELECT T_SRC_ITEM_TMPL.*,3 error_code_record,'''|| dvd_sysdate ||''','''|| dvs_err_msg ||''' error_message_record
             FROM   T_SRC_ITEM_TMPL
             WHERE  UPPER('|| rec.field_name ||') <> ''N/A''
             AND    UPPER('|| rec.field_name ||') IN ( SELECT UPPER('|| rec.field_name ||')
                                                       FROM   T_SRC_ITEM_TMPL
                                                       GROUP BY '|| rec.field_name ||'
                                                       HAVING   '|| dvs_sqltmp ||')';
         BEGIN
            EXECUTE IMMEDIATE vs_sql;
 
            vi_data_err_count  := SQL%ROWCOUNT;
            vi_data_err_chk_03 := vi_data_err_chk_03 + vi_data_err_count;
            vi_data_errors_tot := vi_data_errors_tot + vi_data_err_count;
 
         EXCEPTION
            WHEN OTHERS THEN
 
               vi_error_code := SQLCODE;
               vs_error_text := SUBSTR(SQLERRM,1,4000);
 
               vs_log_msg    := vs_error_text;                                                                         LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_log_msg    := vs_sql;                                                                                LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
               vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';      DBEX ( vs_msg , vs_proc_source, 'E');
               vs_msg        := vs_log_msg;                                                                            DBEX ( vs_msg , vs_proc_source, 'E');
 
         END;
 
         vs_log_msg  := 'Check 03 : Duplicate relation value -  Errors found : '|| vi_data_err_count;                LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
 
         --------------------------------------------------------------------------
         -- Check  3 : Duplicate relation value              DELETE from SRC_ table
         --------------------------------------------------------------------------
         IF vi_data_err_count > 0 THEN
 
            vs_log_msg := 'Check  3 : Duplicate relation value : '|| rec.field_name ||'   DELETE from SRC_ table'; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
            vs_sql := 'DELETE T_SRC_ITEM_TMPL
                        WHERE  UPPER('|| rec.field_name ||') <> ''N/A''
                        AND    UPPER('|| rec.field_name ||')  IN ( SELECT UPPER('||rec.field_name||')
                                                                     FROM   T_SRC_ITEM_TMPL
                                                                     GROUP BY '|| rec.field_name ||'
                                                                     HAVING   '|| dvs_sqltmp ||')';
 
            BEGIN
 
               EXECUTE IMMEDIATE vs_sql;
 
            EXCEPTION
               WHEN OTHERS THEN
 
                  vi_error_code := SQLCODE;
                  vs_error_text := SUBSTR(SQLERRM,1,4000);
 
                  vs_log_msg    := vs_error_text;                                                                      LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
                  vs_log_msg    := vs_sql;                                                                             LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
                  vs_msg        := 'The SQL error has been ignored to allow '|| vs_proc_source ||' to complete';   DBEX ( vs_msg , vs_proc_source, 'E');
                  vs_msg        := vs_log_msg;                                                                         DBEX ( vs_msg , vs_proc_source, 'E');
 
            END;
 
         END IF;
 
         COMMIT;
 
      END IF;
 
      dvs_sqltmp:='';
 
   END LOOP;
 
   vs_log_msg  := 'Check 02 : Duplicate description value -  Errors found : '|| vi_data_err_chk_02;                  LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);
   vs_log_msg  := 'Check 03 : Duplicate relation value    -  Errors found : '|| vi_data_err_chk_03;                  LOG_IT (vs_log_table, vi_logging_level ,3 , vs_proc_source, 'D', vs_log_msg , NULL , NULL);

 
 
 
   --------------------------------------------------------------------------
   -- Rebuild the items sequence.
   --------------------------------------------------------------------------
 
   --------------------------------------------------------------------------
   -- DROP SEQUENCE
   --------------------------------------------------------------------------
   IF get_is_sequence_exists('items_seq') = 1 THEN
 
      vs_sql     := 'DROP SEQUENCE items_seq';
      vs_log_msg := vs_sql;                                                                                            LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg , NULL , NULL);
 
      dynamic_ddl( vs_sql );
 
   END IF;
 
   dv_max_id := dynamic_number('SELECT MAX(NVL(item_id,0))  FROM items');
   dv_max_id := dv_max_id + 1;
   dv_max_id := NVL(dv_max_id,1);
 
   --------------------------------------------------------------------------
   -- CREATE SEQUENCE
   --------------------------------------------------------------------------
   vs_sql     := 'CREATE SEQUENCE items_seq START WITH '|| dv_max_id;
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl( vs_sql );
 
   --------------------------------------------------------------------------
   -- ALTER PROCEDURE    COMPILE
   --------------------------------------------------------------------------
   vs_sql     := 'ALTER PROCEDURE EP_LOAD_ITEMS COMPILE';
   vs_log_msg := vs_sql;                                                                                               LOG_IT (vs_log_table, vi_logging_level ,2 , vs_proc_source, 'M', vs_log_msg, NULL , NULL);
 
   dynamic_ddl( vs_sql );
 
 
 
   vs_log_msg := 'End of procedure';                                                                                 LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
 
   --------------------------------------------------------------------------
   -- Record the start and end times to be recorded in DB_TIMING_LOG
   --------------------------------------------------------------------------
   vd_end_time  := DBMS_UTILITY.get_time;
   vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
   vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds - ';
      vs_msg := vs_msg ||'Data validation errors :';
      vs_msg := vs_msg ||'Total  ['|| vi_data_errors_tot ||'] ';
      vs_msg := vs_msg ||'Chk 01 ['|| vi_data_err_chk_01 ||'] ';
      vs_msg := vs_msg ||'Chk 02 ['|| vi_data_err_chk_02 ||'] ';
      vs_msg := vs_msg ||'Chk 03 ['|| vi_data_err_chk_03 ||'] ';
      vs_msg := vs_msg ||'Chk 12 ['|| vi_data_err_chk_12 ||'] ';                                                   DBEX( vs_msg, vs_proc, 'T' );

   set_module ('E');

 
EXCEPTION
   WHEN OTHERS THEN
 
      send_err_message (dvs_proc_name,'Failed TO complete data checking - '||dvs_sqlstr);
 
      vs_log_msg :=  'Completed with errors';                                                                        LOG_IT (vs_log_table, vi_logging_level ,1 , vs_proc_source,'EP', vs_log_msg, NULL , NULL );
      --------------------------------------------------------------------------
      -- Record the start and end times to be recorded in DB_TIMING_LOG
      --------------------------------------------------------------------------
      vd_end_time  := DBMS_UTILITY.get_time;
      vt_procedure := vt_procedure + (( vd_end_time - vd_start_time_proc ) / 100 );
 
      vs_msg := 'Procedure Completed in '|| LPAD(TO_CHAR(vt_procedure),8) ||' seconds with errors - ';
      vs_msg := vs_msg ||'Data validation errors :';
      vs_msg := vs_msg ||'Total  ['|| vi_data_errors_tot ||'] ';
      vs_msg := vs_msg ||'Chk 01 ['|| vi_data_err_chk_01 ||'] ';
      vs_msg := vs_msg ||'Chk 02 ['|| vi_data_err_chk_02 ||'] ';
      vs_msg := vs_msg ||'Chk 03 ['|| vi_data_err_chk_03 ||'] ';
      vs_msg := vs_msg ||'Chk 12 ['|| vi_data_err_chk_12 ||'] ';                                                   DBEX( vs_msg, vs_proc, 'T' );


END  EP_CHECK_ITEMS;

/
