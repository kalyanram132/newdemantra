--------------------------------------------------------
--  DDL for Procedure PRC_CREATE_NEW_APO_COMBS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."PRC_CREATE_NEW_APO_COMBS" 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;   
    v_count NUMBER;

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_CREATE_NEW_APO_COMBS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE);



--   dynamic_ddl ('truncate table BIIO_APO_FCST');   
--   dynamic_ddl ('truncate table BIIO_APO_FCST_ERR');
--   
   COMMIT;
   
   check_and_drop('RR_BIIO_APO_FCST');
dynamic_ddl('create table RR_BIIO_APO_FCST AS
   SELECT SKU_CODE||''-30110101'' sku_code ,CL3_CODE,SALES_OFFICE,SUM(APO_BASELINE_FCST) RR_APO_FCST_OFFSET
   FROM T_SRC_SAPL_APO_FCST_TMPL /*where nvl(apo_baseline_fcst,0) <> 0 */ group by SKU_CODE ,CL3_CODE,SALES_OFFICE');
   
    check_and_drop('RR_APO_FCST_CUST');
dynamic_ddl('create table RR_APO_FCST_CUST AS
   SELECT CL3_CODE,SALES_OFFICE,SUM(APO_BASELINE_FCST) RR_APO_FCST_OFFSET
   FROM T_SRC_SAPL_APO_FCST_TMPL /*where nvl(apo_baseline_fcst,0) <> 0 */ group by CL3_CODE,SALES_OFFICE;');
   
   
--  dynamic_ddl('INSERT INTO BIIO_APO_FCST(SDATE,LEVEL1,LEVEL2,LEVEL3,RR_APO_FCST_OFFSET)
--   SELECT t1.WEEK_START_DATE,t1.SKU_CODE||''-30110101'' ,t1.CL3_CODE,t1.SALES_OFFICE,SUM(t1.APO_BASELINE_FCST)
--   FROM T_SRC_SAPL_APO_FCST_TMPL t1,RR_BIIO_APO_FCST t2
--   where t1.SKU_CODE||''-30110101'' = t2.sku_code
--   and t1.cl3_code = t2.cl3_code
--   and t1.sales_office = t2.sales_office 
--   and t2.rr_apo_fcst_offset <> 0 group by t1.WEEK_START_DATE,t1.SKU_CODE ,t1.CL3_CODE,t1.SALES_OFFICE');
   
   COMMIT;
   
  
   
 --   dynamic_ddl ('truncate table mdp_load_assist');
   /*Need to add min rowud logic to pick just one customer for all the customers except Coles/Woolworths/Metcash*/
   
          dynamic_ddl
          ( 'INSERT /*+ APPEND NOLOGGING */ INTO mdp_load_assist(item_id, location_id, is_fictive, boolean_qty)
            (SELECT /*+ FULL(imp) PARALLEL(imp, 64) */ i.item_id, l.location_id, 0, 0
            FROM RR_BIIO_APO_FCST imp, items i, location l,t_ep_item itm,t_ep_ebs_customer cl3,t_ep_l_att_6 l6
            WHERE 1 = 1
            AND imp.sku_code = itm.item
            and imp.cl3_Code = CL3.EBS_CUSTOMER
            and imp.sales_office =  l6.l_att_6 
            and itm.t_ep_item_ep_id = i.t_ep_item_ep_id
            and cl3.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
            and l6.t_Ep_l_att_6_ep_id = l.t_ep_l_att_6_ep_id
            and imp.RR_APO_FCST_OFFSET = 0
            AND NOT EXISTS(SELECT 1 FROM mdp_matrix m WHERE m.item_id = i.item_id AND m.location_id = l.location_id)
            )'
           );
           
          COMMIT;

          SELECT COUNT (1)
          INTO v_count
          FROM mdp_load_assist;
          
          --dbms_output.put_line('Count is ' || v_count);
           v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name,   'Count is '|| v_count || SYSDATE);

          IF v_count > 0
          THEN
            pkg_common_utils.prc_mdp_add ('mdp_load_assist');
            --dynamic_ddl ('truncate table mdp_load_assist');
          END IF;


   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name,   v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name,  'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

/
