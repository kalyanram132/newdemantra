--------------------------------------------------------
--  DDL for Procedure PRC_CLOSE_PROMOTION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "DEMANTRA"."PRC_CLOSE_PROMOTION" (
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL
)
IS
/******************************************************************************
   NAME:       PRC_CLOSE_PROMOTION
   PURPOSE:    Close the Promotion when user runs Close Promotion Method.


   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Inital

******************************************************************************/
   sql_str          VARCHAR2 (5000);
   v_prog_name      VARCHAR2 (100)  := 'PRC_CLOSE_PROMOTION';
   v_step           VARCHAR2 (100);
   case_deal_oi     NUMBER;
   new_close_date   DATE;
BEGIN
--   v_step := 'start ';
--   dbex (v_step || SYSDATE, v_prog_name);
  -- new_close_date := NEXT_DAY (TO_CHAR (SYSDATE, 'mm/dd/yyyy'), 'sunday');
   pre_logon;

   UPDATE promotion
      SET promotion_stat_id = '8',
          last_update_date = SYSDATE,     
          promo_close_user = user_id,
          promo_close_date = SYSDATE
    WHERE promotion_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE promotion_data
      SET promotion_stat_id = '8',
          last_update_date = SYSDATE
    WHERE promotion_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE promotion_matrix
      SET promotion_data_lud = SYSDATE,
          promotion_stat_id = '8', 
          last_update_date = SYSDATE 
    WHERE promotion_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   -- Only delete rows for promotion that have not been posted to GL --
   FOR rec IN
      (SELECT DISTINCT m.item_id, m.location_id,
                       ((CASE
                            WHEN allocation_wk6 > 0
                               THEN 42
                            WHEN allocation_wk5 > 0
                               THEN 35
                            WHEN allocation_wk4 > 0
                               THEN 28
                            WHEN allocation_wk3 > 0
                               THEN 21
                            WHEN allocation_wk2 > 0
                               THEN 14
                            WHEN allocation_wk1 > 0
                               THEN 7
                            ELSE 0
                         END
                        )
                       ) allocation_date
                  FROM promotion_matrix m
                 WHERE (item_id, location_id, promotion_id) IN (
                                          SELECT DISTINCT item_id,
                                                          location_id,
                                                          promotion_id
                                                     FROM promotion_data
                                                    WHERE promotion_id =
                                                                     member_id))
   LOOP
      DELETE      /*+ nologging */FROM promotion_data pd
            WHERE promotion_id = member_id
              AND pd.item_id = rec.item_id
              AND pd.location_id = rec.location_id
              AND sales_date > SYSDATE + rec.allocation_date
              AND (promotion_id, sales_date) NOT IN (
                     SELECT DISTINCT promotion_id, sales_date
                                FROM accrual_data
                               WHERE promotion_id = member_id
                                 AND (   gl_post_cd <> 0
                                      OR gl_post_coop <> 0
                                      OR am_gl_post_fix <> 0
                                      OR am_approved_claim_cd <> 0
                                      OR am_approved_claim_coop <> 0
                                     ));
   END LOOP;

   MERGE INTO promotion_dates pd
      USING (SELECT   promotion_id, MAX (sales_date) new_close_date
                 FROM promotion_data
                WHERE promotion_id = member_id
             GROUP BY promotion_id) pd1
      ON (pd.promotion_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.until_date = NEXT_DAY (TO_CHAR (pd1.new_close_date, 'mm/dd/yyyy'), 'sunday')
            WHERE pd.until_date > TO_CHAR (SYSDATE, 'mm/dd/yyyy')
         ;
   COMMIT;
   
   
   MERGE INTO promotion_matrix pm
      USING (SELECT   promotion_id, location_id, item_id,
                      MAX (sales_date) new_close_date
                 FROM promotion_data
                WHERE promotion_id = member_id
             GROUP BY promotion_id, location_id, item_id) pd1
      ON (    pm.promotion_id = pd1.promotion_id
          AND pm.item_id = pd1.item_id
          AND pm.location_id = pd1.location_id)
      WHEN MATCHED THEN
         UPDATE
            SET pm.until_date = NEXT_DAY (TO_CHAR (pd1.new_close_date, 'mm/dd/yyyy'), 'sunday')
            WHERE pm.until_date > TO_CHAR (SYSDATE, 'mm/dd/yyyy')
         ;
         
         COMMIT;
         
         
          FOR rec IN
      (SELECT DISTINCT m.item_id, m.location_id,
                       ((CASE
                            WHEN allocation_wk6 > 0
                               THEN 42
                            WHEN allocation_wk5 > 0
                               THEN 35
                            WHEN allocation_wk4 > 0
                               THEN 28
                            WHEN allocation_wk3 > 0
                               THEN 21
                            WHEN allocation_wk2 > 0
                               THEN 14
                            WHEN allocation_wk1 > 0
                               THEN 7
                            ELSE 0
                         END
                        )
                       ) allocation_date
                  FROM promotion_matrix m
                 WHERE (item_id, location_id, promotion_id) IN (
                            SELECT DISTINCT item_id, location_id,
                                            promotion_id
                                       FROM promotion_data
                                      WHERE promotion_id = member_id))
   LOOP
      DELETE      /*+ nologging */FROM accrual_data pd
            WHERE pd.accrual_id = member_id
              AND pd.item_id = rec.item_id
              AND pd.location_id = rec.location_id
              AND sales_date > SYSDATE + rec.allocation_date
              AND (   gl_post_cd <> 0
                   OR gl_post_coop <> 0
                   OR am_gl_post_fix <> 0
                   OR am_approved_claim_cd <> 0
                   OR am_approved_claim_coop <> 0
                   OR am_gl_close_cd <> 0
                   OR am_gl_close_coop <> 0
                   OR am_gl_post_fix <> 0
                  );
   END LOOP;
         
--   v_step := 'end ';
--   dbex (v_step || SYSDATE, v_prog_name);
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         dbex (   'Fatal Error in Step: '
               || v_step
               || '. SQLCODE = '
               || SQLCODE
               || ' '
               || SQLERRM,
               v_prog_name
              );
         RAISE;
      END;
END;
 
 
 

/
