--------------------------------------------------------
--  DDL for Table T_EP_P2B
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."T_EP_P2B" 
   (	"T_EP_P2B_EP_ID" NUMBER(10,0), 
	"IS_FICTIVE" NUMBER(1,0), 
	"LAST_UPDATE_DATE" DATE DEFAULT SYSTIMESTAMP, 
	"P2B" VARCHAR2(100 BYTE), 
	"P2B_DESC" VARCHAR2(2000 BYTE), 
	"FICTIVE_CHILD" NUMBER(10,0), 
	"METHOD_STATUS" NUMBER(2,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_DP" 
   CACHE ;
