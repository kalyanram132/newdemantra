--------------------------------------------------------
--  DDL for Table INPUTS
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."INPUTS" 
   (	"DATET" DATE, 
	"T" NUMBER(10,0), 
	"WINTER" NUMBER(1,0), 
	"SUMMER" NUMBER(1,0), 
	"D1" NUMBER(1,0), 
	"D2" NUMBER(1,0), 
	"D3" NUMBER(1,0), 
	"D4" NUMBER(1,0), 
	"D5" NUMBER(1,0), 
	"D6" NUMBER(1,0), 
	"D7" NUMBER(1,0), 
	"D8" NUMBER(1,0), 
	"D9" NUMBER(1,0), 
	"D10" NUMBER(1,0), 
	"D11" NUMBER(1,0), 
	"D12" NUMBER(1,0), 
	"NULLS" NUMBER(3,0), 
	"TM" NUMBER(20,0), 
	"BUSINESS_DAY_FILTER" NUMBER(1,0) DEFAULT 1, 
	"CONSTANT" NUMBER(1,0) DEFAULT 1, 
	"PERIOD_445" NUMBER, 
	"INDEPENDENCE" NUMBER(1,0), 
	"LABOR" NUMBER(1,0), 
	"MEMORIAL" NUMBER(1,0), 
	"NEWYEAR" NUMBER(1,0), 
	"THANKSGIVING" NUMBER(1,0), 
	"CHRISTMAS" NUMBER(1,0), 
	"NUM_OF_DAYS" NUMBER(3,0), 
	"SALES_DATE" DATE, 
	"INVT" NUMBER(10,0), 
	"EASTER_MON" NUMBER(1,0), 
	"EASTER_SUN" NUMBER(1,0), 
	"FATHERS" NUMBER(1,0), 
	"MOTHERS" NUMBER(1,0), 
	"WEEK1" NUMBER(3,0), 
	"WEEK2" NUMBER(3,0), 
	"WEEK3" NUMBER(3,0), 
	"WEEK4" NUMBER(3,0), 
	"WEEK5" NUMBER(3,0), 
	"WEEK6" NUMBER(3,0), 
	"SR_MONTH" NUMBER(3,0), 
	"T_EP_FISCAL_YR_ID" NUMBER(10,0), 
	"T_EP_FISCAL_YR_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"FISACAL_MONTH_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"T_EP_FISCAL_PERIOD_ID" NUMBER(10,0), 
	"T_EP_FISCAL_PERIOD_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"T_EP_FISCAL_WK_ID" NUMBER(10,0), 
	"T_EP_FISCAL_WK_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"QUARTER_PERIOD" NUMBER(10,0), 
	"FISCAL_QUARTER" NUMBER, 
	"FISCAL_YEAR" NUMBER, 
	"RUN_BUSINESS_PLAN" NUMBER, 
	"QUARTER" NUMBER, 
	"YEJ" NUMBER, 
	"T_EP_FISCAL_MONTH_ID" NUMBER(10,0), 
	"T_EP_FISCAL_MONTH_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"OS_MONTH" NUMBER, 
	"OS_QUARTER" NUMBER, 
	"OS_YEAR" NUMBER, 
	"OS_MONTH_ID" NUMBER(10,0), 
	"OS_MONTH_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"OS_QUARTER_ID" NUMBER(10,0), 
	"OS_QUARTER_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"OS_YEAR_ID" NUMBER(10,0), 
	"OS_YEAR_LUD" DATE DEFAULT SYSTIMESTAMP, 
	"TIME_BUCKET_NUM" NUMBER(10,0), 
	"PERIOD_ALLOC" NUMBER(10,5) DEFAULT 1, 
	"BUS_NUM_OF_DAYS" NUMBER(10,5), 
	"CALENDAR_MONTH" NUMBER, 
	"CALENDAR_QUARTER" NUMBER, 
	"CALENDAR_YEAR" NUMBER, 
	"CALENDAR_HALF_YEAR" NUMBER, 
	"COLES_MONTH" NUMBER, 
	"WW_MONTH" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_DP" ;
