--------------------------------------------------------
--  DDL for Table T_SRC_SAPL_SCAN
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."T_SRC_SAPL_SCAN" 
   (	"SCAN_DATE" DATE, 
	"SALES_OFFICE" VARCHAR2(100 BYTE), 
	"BANNER" VARCHAR2(100 BYTE), 
	"CUSTOMER_LEVEL3" VARCHAR2(100 BYTE), 
	"MATERIAL" VARCHAR2(100 BYTE), 
	"DEMAND_BASE" NUMBER(20,10), 
	"ACTUALS_INCR" NUMBER(20,10), 
	"PROMO_PRICE" NUMBER(20,10), 
	"RRP" NUMBER(20,10)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 81920 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_DP" ;
