--------------------------------------------------------
--  DDL for Table ACCRUAL
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."ACCRUAL" 
   (	"ACCRUAL_ID" NUMBER(10,0), 
	"ACCRUAL_CODE" VARCHAR2(120 CHAR), 
	"ACCRUAL_DESC" VARCHAR2(2000 CHAR), 
	"LAST_UPDATE_DATE" DATE DEFAULT SYSTIMESTAMP, 
	"IS_FICTIVE" NUMBER(1,0), 
	"APPLICATION_ID" VARCHAR2(63 CHAR) DEFAULT RAWTOHEX(SYS_GUID()), 
	"METHOD_STATUS" NUMBER(2,0), 
	"ACCRUAL_TYPE_ID" NUMBER(10,0) DEFAULT 0, 
	"ACCRUAL_STAT_ID" NUMBER(10,0) DEFAULT 0, 
	"ACCRUAL_PROGRAM_ID" NUMBER(10,0) DEFAULT 0, 
	"AM_NON_EXPORT_CLAIM" NUMBER(20,10)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 81920 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_ACCRUAL_DATA" ;
