--------------------------------------------------------
--  DDL for Table T_EP_ITEM
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."T_EP_ITEM" 
   (	"T_EP_ITEM_EP_ID" NUMBER(10,0), 
	"IS_FICTIVE" NUMBER(1,0), 
	"LAST_UPDATE_DATE" DATE DEFAULT SYSTIMESTAMP, 
	"ITEM" VARCHAR2(240 BYTE), 
	"DM_ITEM_DESC" VARCHAR2(2000 BYTE), 
	"FICTIVE_CHILD" NUMBER(10,0), 
	"T_EP_E1_ITEM_CAT_7_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_6_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_5_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_4_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_3_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_2_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_E1_ITEM_CAT_1_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"E1_PLANNING_UOM" VARCHAR2(100 BYTE), 
	"E1_SHIPPING_UOM" VARCHAR2(100 BYTE) DEFAULT 1, 
	"E1_WEIGHT_UOM" VARCHAR2(100 BYTE), 
	"E1_VOLUME_UOM" VARCHAR2(100 BYTE), 
	"E1_PRIMARY_UOM" VARCHAR2(100 BYTE), 
	"E1_SHIP_UOM_MULT" NUMBER(20,10) DEFAULT 1, 
	"E1_WHGT_UOM_MULT" NUMBER(20,10) DEFAULT 1, 
	"E1_PRIM_PLAN_FACTOR" NUMBER(20,10) DEFAULT 1, 
	"E1_VOL_UOM_MULT" NUMBER(20,10) DEFAULT 1, 
	"EBS_ITEM_DEST_KEY" NUMBER(10,0), 
	"T_EP_EBS_PROD_FAMILY_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_EBS_PROD_CAT_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"EBSUOM100" NUMBER DEFAULT 1, 
	"EBSUOM101" NUMBER DEFAULT 1, 
	"EBSUOM102" NUMBER DEFAULT 1, 
	"EBSUOM103" NUMBER DEFAULT 1, 
	"EBSUOM104" NUMBER DEFAULT 1, 
	"EBSUOM105" NUMBER DEFAULT 1, 
	"EBSUOM106" NUMBER DEFAULT 1, 
	"EBSUOM107" NUMBER DEFAULT 1, 
	"EBSUOM108" NUMBER DEFAULT 1, 
	"EBSUOM109" NUMBER DEFAULT 1, 
	"EBSUOM110" NUMBER DEFAULT 1, 
	"EBSUOM111" NUMBER DEFAULT 1, 
	"EBSUOM112" NUMBER DEFAULT 1, 
	"EBSUOM113" NUMBER DEFAULT 1, 
	"EBSUOM114" NUMBER DEFAULT 1, 
	"EBSUOM115" NUMBER DEFAULT 1, 
	"EBSUOM116" NUMBER DEFAULT 1, 
	"EBSUOM117" NUMBER DEFAULT 1, 
	"EBSUOM118" NUMBER DEFAULT 1, 
	"EBSUOM119" NUMBER DEFAULT 1, 
	"EBSUOM120" NUMBER DEFAULT 1, 
	"EBSUOM121" NUMBER DEFAULT 1, 
	"EBSUOM122" NUMBER DEFAULT 1, 
	"EBSUOM123" NUMBER DEFAULT 1, 
	"EBSUOM124" NUMBER DEFAULT 1, 
	"EBSUOM125" NUMBER DEFAULT 1, 
	"EBSUOM126" NUMBER DEFAULT 1, 
	"EBSUOM127" NUMBER DEFAULT 1, 
	"EBSUOM128" NUMBER DEFAULT 1, 
	"EBSUOM129" NUMBER DEFAULT 1, 
	"EBSUOM130" NUMBER DEFAULT 1, 
	"EBSUOM131" NUMBER DEFAULT 1, 
	"EBSUOM132" NUMBER DEFAULT 1, 
	"EBSUOM133" NUMBER DEFAULT 1, 
	"EBSUOM134" NUMBER DEFAULT 1, 
	"EBSUOM135" NUMBER DEFAULT 1, 
	"EBSUOM136" NUMBER DEFAULT 1, 
	"EBSUOM137" NUMBER DEFAULT 1, 
	"EBSUOM138" NUMBER DEFAULT 1, 
	"EBSUOM139" NUMBER DEFAULT 1, 
	"EBSUOM140" NUMBER DEFAULT 1, 
	"EBSUOM141" NUMBER DEFAULT 1, 
	"EBSUOM142" NUMBER DEFAULT 1, 
	"EBSUOM143" NUMBER DEFAULT 1, 
	"EBSUOM144" NUMBER DEFAULT 1, 
	"EBSUOM145" NUMBER DEFAULT 1, 
	"EBSUOM146" NUMBER DEFAULT 1, 
	"EBSUOM147" NUMBER DEFAULT 1, 
	"EBSUOM148" NUMBER DEFAULT 1, 
	"EBSUOM149" NUMBER DEFAULT 1, 
	"EBSUOM150" NUMBER DEFAULT 1, 
	"EBSUOM151" NUMBER DEFAULT 1, 
	"EBSUOM152" NUMBER DEFAULT 1, 
	"EBSUOM153" NUMBER DEFAULT 1, 
	"EBSUOM154" NUMBER DEFAULT 1, 
	"EBSUOM155" NUMBER DEFAULT 1, 
	"EBSUOM156" NUMBER DEFAULT 1, 
	"EBSUOM157" NUMBER DEFAULT 1, 
	"EBSUOM158" NUMBER DEFAULT 1, 
	"EBSUOM159" NUMBER DEFAULT 1, 
	"EBSUOM160" NUMBER DEFAULT 1, 
	"EBSUOM161" NUMBER DEFAULT 1, 
	"EBSUOM162" NUMBER DEFAULT 1, 
	"EBSUOM163" NUMBER DEFAULT 1, 
	"EBSUOM164" NUMBER DEFAULT 1, 
	"EBSUOM165" NUMBER DEFAULT 1, 
	"EBSUOM166" NUMBER DEFAULT 1, 
	"EBSUOM167" NUMBER DEFAULT 1, 
	"EBSUOM168" NUMBER DEFAULT 1, 
	"EBSUOM169" NUMBER DEFAULT 1, 
	"EBSUOM170" NUMBER DEFAULT 1, 
	"EBSUOM171" NUMBER DEFAULT 1, 
	"EBSUOM172" NUMBER DEFAULT 1, 
	"EBSUOM173" NUMBER DEFAULT 1, 
	"EBSUOM174" NUMBER DEFAULT 1, 
	"EBSUOM175" NUMBER DEFAULT 1, 
	"EBSUOM176" NUMBER DEFAULT 1, 
	"EBSUOM177" NUMBER DEFAULT 1, 
	"EBSUOM178" NUMBER DEFAULT 1, 
	"EBSUOM179" NUMBER DEFAULT 1, 
	"EBSUOM180" NUMBER DEFAULT 1, 
	"EBSUOM181" NUMBER DEFAULT 1, 
	"EBSUOM182" NUMBER DEFAULT 1, 
	"EBSUOM183" NUMBER DEFAULT 1, 
	"EBSUOM184" NUMBER DEFAULT 1, 
	"EBSUOM185" NUMBER DEFAULT 1, 
	"EBSUOM186" NUMBER DEFAULT 1, 
	"EBSUOM187" NUMBER DEFAULT 1, 
	"EBSUOM188" NUMBER DEFAULT 1, 
	"EBSUOM189" NUMBER DEFAULT 1, 
	"EBSUOM190" NUMBER DEFAULT 1, 
	"EBSUOM191" NUMBER DEFAULT 1, 
	"EBSUOM192" NUMBER DEFAULT 1, 
	"EBSUOM193" NUMBER DEFAULT 1, 
	"EBSUOM194" NUMBER DEFAULT 1, 
	"EBSUOM195" NUMBER DEFAULT 1, 
	"EBSUOM196" NUMBER DEFAULT 1, 
	"EBSUOM197" NUMBER DEFAULT 1, 
	"EBSUOM198" NUMBER DEFAULT 1, 
	"EBSUOM199" NUMBER DEFAULT 1, 
	"T_EP_P2_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_P2A_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_P2B_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_1_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_2_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_3_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_4_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_5_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_6_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_7_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_8_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_9_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_I_ATT_10_EP_ID" NUMBER(10,0) DEFAULT 0, 
	"EQ_UNIT" NUMBER(20,10) DEFAULT 1, 
	"CASE_UNITS" NUMBER(20,10) DEFAULT 1, 
	"CART" NUMBER(20,10) DEFAULT 1, 
	"MEAN_DEMAND" NUMBER(20,10), 
	"STANDARD_DEV" NUMBER(20,10), 
	"PRODUCT_STATUS" VARCHAR2(30 BYTE), 
	"METHOD_STATUS" NUMBER(20,10), 
	"TARGET_CODE" VARCHAR2(200 BYTE), 
	"TARGET_DESCRIPTION" VARCHAR2(200 BYTE), 
	"END_LAUNCH_PERIOD" DATE, 
	"LAUNCH_DATE" DATE, 
	"COPY_SOURCE_DATA" NUMBER(20,10) DEFAULT 1, 
	"SRC_T_EP_ITEM_EP_ID" NUMBER(20,10), 
	"CANNIZN_PERC" NUMBER(20,10) DEFAULT 0, 
	"REMOVE_COMBINATIONS" NUMBER(20,10) DEFAULT 2, 
	"RR_PRODUCT_CODE" NUMBER(20,10), 
	"RR_CUSTOMER_NO" NUMBER(20,10), 
	"RR_CHANNEL" NUMBER(20,10), 
	"RR_CUSTOMER" NUMBER(20,10), 
	"RR_REGION" NUMBER, 
	"PROPORT_FLAG_1" NUMBER(10,0) DEFAULT 0, 
	"PROPORT_FLAG_102" NUMBER(10,0) DEFAULT 0, 
	"T_EP_WW_CAT_ID" NUMBER(10,0) DEFAULT 0, 
	"T_EP_COL_CAT_ID" NUMBER(10,0) DEFAULT 0
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 2097152 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_DP" 
   CACHE ;
