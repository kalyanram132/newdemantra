--------------------------------------------------------
--  DDL for Table ACCRUAL_LEVELS
--------------------------------------------------------

  CREATE TABLE "DEMANTRA"."ACCRUAL_LEVELS" 
   (	"ACCRUAL_ID" NUMBER(10,0), 
	"LEVEL_ID" NUMBER(38,0), 
	"FILTER_ID" NUMBER(10,0), 
	"LEVEL_ORDER" NUMBER(3,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "TS_ACCRUAL_DATA" ;
