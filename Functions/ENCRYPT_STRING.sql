--------------------------------------------------------
--  DDL for Function ENCRYPT_STRING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."ENCRYPT_STRING" -- $Revision: 1.3 $

   (vs_text IN VARCHAR2)
   RETURN VARCHAR2
IS
   vs_TEXTCODE     VARCHAR2(40);
   vi_CODELENGTH   INT;
   vs_returnText   VARCHAR2(500);
   vi_chr          INT;
   vi_key          INT;
   vi_length       INT;

BEGIN

/*************************************************************************************************************************/
/* This is a translation from the Java method com/demantra/applicationServer/util/CryptographicServer.java: encodeString */
/* The algorthim and even variable names have been preserved as best as possible.                                        */
/*************************************************************************************************************************/

   /* Check if got Valid Text. */
   IF vs_text IS NULL THEN
      RETURN NULL;
   END IF;

   vs_returnText := encryption.get_hash( vs_text, 'DEMANTRA');

   RETURN vs_returnText;

END encrypt_string;

/
