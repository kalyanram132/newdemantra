--------------------------------------------------------
--  DDL for Function GET_IS_PARAM_EXISTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."GET_IS_PARAM_EXISTS" -- $Revision: 1.24 $

               ( p_table_name     VARCHAR2 DEFAULT NULL,
                 param_name       VARCHAR2 DEFAULT NULL)
RETURN NUMBER
IS
  vs_sql             VARCHAR2(1000);
  is_exists          NUMBER;

  table_not_found    EXCEPTION;
  PRAGMA             EXCEPTION_INIT(table_not_found, -942);

BEGIN

  vs_sql := 'SELECT COUNT(1)
             FROM   '||P_TABLE_NAME||'
             WHERE  LOWER(PNAME) = LOWER('''||PARAM_NAME||''')';

  /****************************************************************************************************/
  /* Handle the situation where the table name passed does not exist                                  */
  /* DYNAMIC_NUMBER cannot be used because when it fails it tries to insert rows to DB_EXCEPTION_LOG  */
  /* and you cannot do DML in a function                                                              */
  /****************************************************************************************************/
  --is_exists := dynamic_number(SqlStr);
  BEGIN

     EXECUTE IMMEDIATE vs_sql
     INTO is_exists;

  EXCEPTION
     WHEN NO_DATA_FOUND THEN
        RETURN NULL;

     WHEN TABLE_NOT_FOUND THEN
        /*******************************************/
        /* ORA-00942: table or view does not exist */
        /*******************************************/
        RETURN NULL;

     WHEN OTHERS THEN
        RETURN NULL;
  END;

  RETURN is_exists;

END get_is_param_exists;

/
