--------------------------------------------------------
--  DDL for Function GET_ENG_START_DATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."GET_ENG_START_DATE" -- $Revision: 1.3 $
   
                 (ii_profile_id   NUMBER DEFAULT NULL)
   RETURN DATE
IS
   vi_return                 NUMBER;
   vs_init_params_table      VARCHAR2(30);

   vd_default_date           DATE;
   vd_calc_start_date        DATE;
   vd_start_date             DATE;
   vd_last_date              DATE;
   vi_history_length         INTEGER;
   vi_too_few                INTEGER;
   vi_time_unit              INTEGER;

BEGIN

   --pre_logon;

   /*********************************************************************************************************************/
   /* The basis is "start_date" from INIT_PARAMS                                                                        */
   /*                                                                                                                   */
   /* If HistoryLength parameter is greater than TooFew parameter then start_date is calculated back                    */
   /* from the currently calculated last_date to last_date minus HistoryLength (which is in time buckets)               */
   /*                                                                                                                   */
   /* Eventually if the calculated start_date precedes 1/1/1900 it is set to 1/1/1900 to include all available history. */
   /*********************************************************************************************************************/

   vd_default_date := TO_DATE('01011900','MMDDYYYY');

   IF ii_profile_id IS NOT NULL THEN

      /**************************/
      /* Init_params table name */
      /**************************/

      IF get_is_engine_profile_exists(ii_profile_id) = 1 THEN

         /*************************************************************/
         /* Set the engine profile init_params_n table name           */
         /*************************************************************/

         vs_init_params_table := get_eng_profile_init_params(ii_profile_id);

         IF vs_init_params_table IS NOT NULL THEN

            IF get_is_table_exists(vs_init_params_table) = 1 THEN

               vd_last_date := get_eng_last_date(ii_profile_id);


               get_param(vs_init_params_table, 'start_date'   , vd_start_date);
               get_param(vs_init_params_table, 'HistoryLength', vi_history_length);
               get_param(vs_init_params_table, 'TooFew'       , vi_too_few);
               get_param(vs_init_params_table, 'timeunit'     , vi_time_unit);

               vd_calc_start_date := vd_start_date;

               IF vd_last_date IS NOT NULL       AND
                  vi_history_length > vi_too_few THEN


                  /***************************************************/
                  /* Time units (1 = Monthly, 2 = Daily, 3 = Weekly) */
                  /***************************************************/

                  IF vi_time_unit = 3 THEN
                     /***********/
                     /* Weekly  */
                     /***********/
                     vd_calc_start_date := vd_last_date - (7 * vi_history_length);

                  ELSIF vi_time_unit = 2  THEN
                     /***********/
                     /* Daily   */
                     /***********/
                     vd_calc_start_date := vd_last_date - vi_history_length;

                  ELSIF vi_time_unit = 1 THEN
                     /***********/
                     /* Monthly */
                     /***********/
                     vd_calc_start_date := ADD_MONTHS(vd_last_date, (-1 * vi_history_length));

                  END IF;

               END IF;

               IF vd_calc_start_date < vd_default_date THEN
                  vd_calc_start_date := vd_default_date;
               END IF;

            END IF;

         END IF;

      END IF;
   END IF;

   RETURN vd_calc_start_date;

END get_eng_start_date;

/
