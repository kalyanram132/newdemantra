--------------------------------------------------------
--  DDL for Function GET_MAX_DATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."GET_MAX_DATE" ( ii_minutes_offset  INTEGER DEFAULT 0 )
   RETURN VARCHAR2

IS
   vi_exists     INTEGER;
   vd_tmp_date   DATE;
   vs_ret_date   VARCHAR2(20);
BEGIN

   vs_ret_date := '11-20-2017 00:00:00';

   IF ii_minutes_offset <> 0 THEN

      vd_tmp_date := TO_DATE( vs_ret_date,  'mm-dd-yyyy hh24:mi:ss') + ( ii_minutes_offset / 1440 ) ;
      vs_ret_date := TO_CHAR( vd_tmp_date );

   END IF;


   RETURN vs_ret_date;
END;

/
