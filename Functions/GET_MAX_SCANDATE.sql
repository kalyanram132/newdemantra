--------------------------------------------------------
--  DDL for Function GET_MAX_SCANDATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."GET_MAX_SCANDATE" 
   RETURN VARCHAR2
IS
   ret_date    VARCHAR2(20);
BEGIN

   ret_date := '10-30-2017 00:00:00';

    RETURN ret_date;
END;

/
