--------------------------------------------------------
--  DDL for Function GET_ENG_LAST_DATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "DEMANTRA"."GET_ENG_LAST_DATE" -- $Revision: 1.3 $
   
                 (ii_profile_id   NUMBER DEFAULT NULL)
   RETURN DATE
IS
   vi_return                 NUMBER;
   vs_proc                   VARCHAR2(30) := 'GET_ENG_LAST_DATE';

   vs_init_params_table      VARCHAR2(30);

   vd_default_date           DATE;
   vd_calc_last_date         DATE;

   vd_last_date              DATE;
   vd_last_date_backup       DATE;
   vd_max_sales_date         DATE;

BEGIN

   --pre_logon;

   /*************************************************************************************/
   /* The logic is :                                                                    */
   /*                                                                                   */
   /* 1. current_last_date == last_date                                                 */
   /*                                                                                   */
   /* 2. if current_last_date == 1/1/1900 then current_last_date = last_date_backup     */
   /*                                                                                   */
   /* 3. if current_last_date == 1/1/1900 then current_last_date = max_sales_date       */
   /*                                                                                   */
   /* 4. if current_last_date == 1/1/1900 log error and return with 1/1/1900            */
   /*    The engine will treat 1/1/1900 as an error and will exit too.                  */
   /*                                                                                   */
   /*************************************************************************************/

   vd_default_date := TO_DATE('01011900','MMDDYYYY');

   IF ii_profile_id IS NOT NULL THEN

      /**************************/
      /* Init_params table name */
      /**************************/

      IF get_is_engine_profile_exists(ii_profile_id) = 1 THEN

         /*************************************************************/
         /* Set the engine profile init_params_n table name           */
         /*************************************************************/

         vs_init_params_table := get_eng_profile_init_params(ii_profile_id);

         IF vs_init_params_table IS NOT NULL THEN

            IF get_is_table_exists(vs_init_params_table) = 1 THEN

               get_param(vs_init_params_table, 'last_date'       , vd_last_date);
               get_param(vs_init_params_table, 'last_date_backup', vd_last_date_backup);
               get_param('SYS_PARAMS'        , 'max_sales_date'  , vd_max_sales_date);

               /**********************************/
               /* current_last_date == last_date */
               /**********************************/
               vd_calc_last_date := vd_last_date;

               /*******************************************************************************/
               /* If current_last_date == 1/1/1900 then current_last_date = last_date_backup  */
               /*******************************************************************************/
               IF vd_calc_last_date = vd_default_date THEN
                  vd_calc_last_date := vd_last_date_backup;
               END IF;

               /*******************************************************************************/
               /* 3. if current_last_date == 1/1/1900 then current_last_date = max_sales_date */
               /*******************************************************************************/
               IF vd_calc_last_date = vd_default_date THEN
                  vd_calc_last_date := vd_max_sales_date;
               END IF;

               /*******************************************************************************/
               /* 4. If current_last_date == 1/1/1900 log error and return with 1/1/1900      */
               /* (engine will treats 1/1/1900 as error and exits too)                        */
               /*******************************************************************************/

               IF vd_calc_last_date = vd_default_date THEN
                  DBEX('There is a problem with the Engine configuration : The engine last date is being calculated as 01/01/1900', vs_proc,'E');
               END IF;

            END IF;

         END IF;

      END IF;
   END IF;

   RETURN vd_calc_last_date;

END get_eng_last_date;

/
