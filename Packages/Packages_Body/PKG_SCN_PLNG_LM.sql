--------------------------------------------------------
--  DDL for Package Body PKG_SCN_PLNG_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_SCN_PLNG_LM" 
AS
/******************************************************************************
   NAME:       PKG_SCN_PLNG_LM
   PURPOSE:    All procedures for level methods related to Scenario Planning
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        20/03/2013  Lakshmi Annapragada / Redrock Consulting - Initial Version
   
   ******************************************************************************/

PROCEDURE prc_duplicate_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
  NAME:       PRC_DUPLICATE_SCENARIO
  PURPOSE:    Procedure to duplicate a scenario
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
   1.0        20/03/2013  Lakshmi Annapragada / Redrock Consulting - Initial Version
  ******************************************************************************/
  v_prog_name   VARCHAR2 (100);
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
   max_sales_date          DATE;
   max_date                VARCHAR2 (100);
   sql_str                 VARCHAR2 (10000);
   new_promotion_id        NUMBER (20);
   new_filter_id           NUMBER (20);
   date_range_change       NUMBER (20);
   new_promotion_stat_id   NUMBER (20);
   start_date              DATE;
   fore_col               VARCHAR2(100);
   scenario_name           VARCHAR2(100);
   new_scenario_id         NUMBER (10);
   scenario_val            NUMBER (10);
   guidelines_val          NUMBER (10);
   p2b_val                 NUMBER (10);
   v_year                  NUMBER(10);
   v_member_id             NUMBER;
   shift_wks               NUMBER DEFAULT 0;
   cnt_scn                 NUMBER;
   cnt_pg                  NUMBER;
      
   v_pg_id              NUMBER;
   l6_val               NUMBER;
   v_method_stat        NUMBER := 0;
   
BEGIN
  pre_logon;
  v_status      := 'start ';
  v_prog_name   := 'PRC_DUPLICATE_SCENARIO';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id,user_id);

   SELECT get_fore_col (0, 1)
     INTO fore_col
     FROM DUAL;

   --Create new scenario first--
     -- Go into loop to create all promotions for Promo Grp / Program we're duplicating --
   /*  SELECT DISTINCT p.scenario_id, p.promotion_guidelines_id,
                                  m.t_ep_p2b_ep_id
                   into scenario_val,guidelines_val,p2b_val
                             FROM promotion p, promotion_data pd, mdp_matrix m
                            WHERE p.promotion_id = member_id
                              AND pd.item_id = m.item_id
                              AND pd.location_id = m.location_id
                              AND p.promotion_id = pd.promotion_id;*/
   v_member_id := member_id;

   SELECT promotion_guidelines_id
   INTO v_pg_id
   FROM promotion WHERE promotion_id = v_member_id;
   
   
   IF v_pg_id = 0 THEN    
      GOTO PRC_END;
   END IF;
      
   
   SELECT  p.scenario_id, p.promotion_guidelines_id, m.t_ep_p2b_ep_id, m.t_ep_ebs_customer_ep_id, to_number(to_char(min(pd.sales_date),'YYYY'))
              INTO scenario_val, guidelines_val, p2b_val, l6_val, v_year
              FROM promotion p,
                   PROMOTION_DATA PD,
                   mdp_matrix m /*,
                   promotion_levels pl,
                   promotion_members pm*/
             WHERE p.promotion_id = v_member_id
               AND pd.item_id = m.item_id
               AND pd.location_id = m.location_id
               AND P.PROMOTION_ID = PD.PROMOTION_ID
               AND pd.is_self = 1
              /* AND pl.promotion_id = p.promotion_id
               AND pl.filter_id = pm.filter_id
               AND pl.level_id = 12
               AND pm.member_id = m.t_ep_p2b_ep_id*/
          GROUP BY p.scenario_id, p.promotion_guidelines_id, m.t_ep_p2b_ep_id, m.t_ep_ebs_customer_ep_id; 

   COMMIT;
   
   /*There should be only 1 duplicate scenario - check that there are no existing what if scenarios*/
   SELECT COUNT(DISTINCT s.rr_check_promo), COUNT(DISTINCT p.promotion_guidelines_id)
              INTO cnt_scn, cnt_pg
              FROM promotion p,
                   promotion_data pd,
                   promotion_dates d,
                   mdp_matrix m,
                   scenario s
             WHERE pd.item_id = m.item_id
               AND pd.location_id = m.location_id
               AND p.promotion_id = pd.promotion_id
               AND p.promotion_id = d.promotion_id
               and p.scenario_id = s.scenario_id
               AND pd.is_self = 1
               --AND p.scenario_id NOT IN (22, 262, 162)
               --AND p.promotion_guidelines_id = guidelines_val
               AND p.promotion_guidelines_id <> 0
               AND to_number(to_char(d.from_date,'YYYY')) = v_year
               AND m.t_ep_p2b_ep_id = p2b_val
               AND m.t_ep_ebs_customer_ep_id = l6_val; 
   
   /* Duplicate only if the source scenario is Current Yea Or Budget Plan.*/
   IF (scenario_val IN (22, 162, 262) AND cnt_scn = 1 AND cnt_pg = 1)
   THEN
   
   SELECT new_scenario, shift_by
     INTO scenario_name, shift_wks
     FROM promotion
    WHERE promotion_id = member_id;

   IF shift_wks > 0
   THEN
      shift_wks := shift_wks * 7;
   ELSE
      shift_wks := 0;
   END IF;

   SELECT scenario_seq.nextval
     INTO new_scenario_id
     FROM DUAL;

   INSERT INTO scenario
               (scenario_id, scenario_code, scenario_desc, last_update_date
               )
        VALUES (new_scenario_id, new_scenario_id, 'WIF: ' || scenario_name, SYSDATE
               );

   COMMIT; 
   
   
   INSERT INTO rr_scn_refresh_t(level_id, member_id, processed, batch_id, op_mode)
   VALUES (233, new_scenario_id, 0, rr_scn_refresh_seq.nextval, 1);
   
   COMMIT; 
   
   
   FOR rec IN (SELECT DISTINCT pd.promotion_id, p.promotion_stat_id
                          FROM promotion_data pd, mdp_matrix m, promotion p
                         WHERE pd.promotion_guidelines_id = guidelines_val
                           --rec1.promotion_guidelines_id
                           AND pd.promotion_id = p.promotion_id
                           and p.coop_deal_type <> 9  --dont include Incentive.
                           AND m.t_ep_p2b_ep_id = p2b_val
                           -- rec1.t_ep_p2b_ep_id
                           AND pd.scenario_id = scenario_val
                           AND to_number(to_char(pd.sales_date,'YYYY')) = v_year
                           --rec1.scenario_id
                           AND pd.item_id = m.item_id
                           AND pd.location_id = m.location_id)
   LOOP
      SELECT promotion_levels_seq.NEXTVAL
        INTO new_promotion_id
        FROM DUAL;
      
      
      IF(rec.promotion_stat_id < 2)
       THEN new_promotion_stat_id := 2;
       ELSE new_promotion_stat_id := rec.promotion_stat_id;
     END IF; 
     
     
-- INSERT DUPLICATE ROWS INTO PROMOTION--
      INSERT INTO dup_p
         SELECT *
           FROM promotion
          WHERE promotion_id = rec.promotion_id;

      UPDATE dup_p
         SET promotion_id = new_promotion_id,
             last_update_date = SYSDATE,
              t_ep_promo_conflict_id = 0,
                conflict_01 = NULL,
                conflict_02 = NULL,
                conflict_03 = NULL,
                conflict_04 = NULL,
                conflict_05 = NULL,
                conflict_06 = NULL,
                conflict_07 = NULL,
                conflict_08 = NULL,
                conflict_09 = NULL,
                conflict_10 = NULL,
                promo_create_date = SYSDATE,
                close_promotion_date = NULL,
                promo_commit_date = NULL,               
             promotion_code = new_promotion_id,             
             scenario_id = new_scenario_id,
             cust_promo_no = NULL,
             ship_date = ship_date + shift_wks,
             end_ship = end_ship + shift_wks,
             gl_exp_close = 0,
             promotion_stat_id = new_promotion_stat_id,
             scn_source_promo_id = rec.promotion_id;

      INSERT INTO promotion
         SELECT *
           FROM dup_p;

      COMMIT;     
      
      
      INSERT INTO rr_scn_refresh_t(level_id, member_id, processed, batch_id, op_mode)
      VALUES (232, new_promotion_id, 0, rr_scn_refresh_seq.nextval, 1);
      
    
-- INSERT DUPLICATE ROWS INTO PROMOTION_DATES--
      INSERT INTO dup_pdt
         SELECT *
           FROM promotion_dates
          WHERE promotion_id = rec.promotion_id;

      UPDATE dup_pdt
         SET promotion_id = new_promotion_id,
             from_date = from_date + shift_wks,
             until_date = until_date + shift_wks;

      INSERT INTO promotion_dates
         SELECT *
           FROM dup_pdt;

      COMMIT;    
      
      UPDATE promotion 
      SET promotion_desc = NVL(
                (SELECT    TO_CHAR (from_date, 'yyyy-mm-dd')
                        || ' : '
                        || new_promotion_id
                        || ' : NEW'
                   FROM promotion_dates pt
                  WHERE pt.promotion_id = new_promotion_id),new_promotion_id)
      WHERE promotion_id = new_promotion_id ;
      
      COMMIT;

-- INSERT DUPLICATE ROWS INTO PROMOTION_DATA--
      INSERT INTO dup_pd
         SELECT *
           FROM promotion_data
          WHERE promotion_id = rec.promotion_id;

 sql_str :=
         (   ' 
   UPDATE dup_pd
      SET promotion_id = '
          || new_promotion_id
          || ',
          last_update_date = SYSDATE,
          accrual_fcst = NULL,
          accrual_fcst_cd = NULL,
          accrual_fcst_coop = NULL,
          gl_post_cd = NULL,
          gl_post_coop = NULL,
          gl_post_fix = NULL,
          evt_vol_act = NULL,
          incr_evt_vol_act = NULL,
          volume_base_ttl = NULL,
          approved_claim_state = NULL,
          approved_coop_d = NULL,
          gl_vol = null,
          accrual_vol_fcst = null,
          --incr_evt_vol_or = NULL,          
          t_ep_promo_conflict_id = 0,
          promotion_stat_id = '||new_promotion_stat_id||',
          gl_exp_fix = 0,
          rr_supply_vol_base_pd = NULL,
          rr_supply_vol_incr_pd = NULL,
          rr_supply_eng_incr_pd = NULL,
          rr_supply_vol_pd = NULL,
          rr_accrual_vol_base_pd = NULL,
          rr_accrual_vol_incr_pd = NULL,
          rr_accrual_eng_incr_pd = NULL,
          rr_accrual_vol_pd = NULL,
          rr_accrual_cd_pd = NULL,
          rr_accrual_coop_pd = NULL,
          rr_accrual_hf_cd_pd = NULL,
          rr_accrual_hf_coop_pd = NULL,               
          rr_pl_cd_pd = NULL,
          rr_pl_coop_pd = NULL,
          rr_accrual_base_commit = NULL,
          rr_accrual_eng_incr_commit = NULL,
          rr_accrual_incr_commit = NULL,
          rr_accrual_vol_commit = NULL,
          rr_accrual_cd_commit = NULL,
          rr_accrual_coop_commit = NULL,
          rr_accrual_hf_cd_commit = NULL,
          rr_accrual_hf_coop_commit = NULL,   
          rr_ms_vol = NULL,
          accruals_type_id = 0,
          '
          || fore_col
          || '_uplift = null'
         );
      dynamic_ddl (sql_str);

    IF rec.promotion_stat_id >=2
    THEN
      --INSERT CORRECT BASELINE FOR PROMOTION--
      sql_str :=
         (   'merge into dup_pd pd
   using
   (select distinct sd.item_id,sd.location_id,sd.sales_date, 
         nvl(sd.manual_stat, nvl(sd.sim_val_182,nvl('
          || fore_col
          || ' *1, sd.npi_forecast))) as vol     
               FROM sales_data sd, dup_pd p
              WHERE sd.item_id = p.item_id
                AND sd.location_id = p.location_id
                AND sd.sales_date =  p.sales_date) s
                on
                (pd.item_id = s.item_id
                AND pd.location_id = s.location_id
                AND pd.sales_date = s.sales_date)
                when matched then update
                set PD.VOLUME_BASE_TTL = s.vol'
         );
      dynamic_ddl (sql_str);
    END IF;
      -- Update global temp table with new promotion_id --
      UPDATE dup_pd
         SET promotion_id = new_promotion_id,
             sales_date = sales_date + shift_wks,
             last_update_date = SYSDATE,
             scenario_id = new_scenario_id;

--INSERT NEW ROWS --
      INSERT INTO promotion_data
         SELECT *
           FROM dup_pd;

      COMMIT;


-- INSERT rows into Promotion levels Glob table --
      INSERT INTO dup_pl
         SELECT *
           FROM promotion_levels
          WHERE promotion_id = rec.promotion_id;

      UPDATE dup_pl
         SET promotion_id = new_promotion_id;      
      
      FOR rec2 IN (SELECT DISTINCT p.promotion_id, p.level_id, p.level_order,
                                   p.filter_id
                              FROM dup_pl p, promotion_members p1
                             WHERE p.filter_id = p1.filter_id)
      LOOP
         SELECT promotion_levels_seq.NEXTVAL
           INTO new_filter_id
           FROM DUAL;

         INSERT INTO promotion_levels
              VALUES (rec2.promotion_id, rec2.level_id, new_filter_id,
                      rec2.level_order);

         FOR rec3 IN (SELECT DISTINCT rec2.promotion_id, rec2.level_id,
                                      rec2.level_order, p1.member_id
                                 FROM promotion_members p1
                                WHERE rec2.filter_id = p1.filter_id)
         LOOP
            INSERT INTO promotion_members
                 VALUES (new_filter_id, rec3.member_id);            
         END LOOP;
      END LOOP;

      COMMIT;

-- INSERT DUPLICATE ROWS INTO PROMOTION_MATRIX--
      INSERT INTO dup_pm
         SELECT *
           FROM promotion_matrix
          WHERE promotion_id = rec.promotion_id;

      UPDATE dup_pm
         SET promotion_id = new_promotion_id,
             last_update_date = SYSDATE,
             promotion_stat_lud = SYSDATE,
             promotion_lud = SYSDATE,
             scenario_lud = SYSDATE,
             scenario_id = new_scenario_id,
             from_date = from_date + shift_wks,
             until_date = until_date + shift_wks,
             scan_forecast_offset = null,
             allocation_wk0 = null,
             allocation_wk1 = null,
             allocation_wk2 = null,
             allocation_wk3 = null,
             allocation_wk4 = null,
             allocation_wk5 = null,
             allocation_wk6 = null,
              rr_alloc_wk0 = null,
                rr_alloc_wk1 = null,
                rr_alloc_wk2 = null,
                rr_alloc_wk3 = null,
                rr_alloc_wk4 = null,
                rr_alloc_wk5 = null,
                rr_alloc_wk6 = null,
                rr_sync_wk0 = null,
                rr_sync_wk1 = null,
                rr_sync_wk2 = null,
                rr_sync_wk3 = null,
                rr_sync_wk4 = null,
                rr_sync_wk5 = null,
                rr_sync_wk6 = null,
             promotion_stat_id = new_promotion_stat_id;

      INSERT INTO promotion_matrix
         SELECT *
           FROM dup_pm;

      COMMIT;
      
      pkg_ptp_lm.prc_set_promo_type(USER_ID, LEVEL_ID, new_promotion_id);
      
   END LOOP;
    
  
  END IF;   
  
<<PRC_END>>

  IF (scenario_val IN (22, 162, 262) AND cnt_scn = 1 AND cnt_pg = 1)
  THEN 
     v_method_stat := 3;
  ELSIF (scenario_val NOT IN (22, 162, 262))
  THEN
    v_method_stat := 7;
  ELSIF  (cnt_scn <> 1)
  THEN
    v_method_stat := 5;
  ELSIF  (cnt_pg <> 1)
  THEN
    v_method_stat := 6;
  ELSE
    v_method_stat := 4;
  END IF;
  
  UPDATE promotion
  SET new_scenario = NULL,
          shift_by = NULL,
          last_update_date = SYSDATE,
          method_status = v_method_stat 
  WHERE promotion_id = member_id;

  COMMIT;
  
  prc_freeze_scenario(user_id,level_id,member_id);
  
  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    UPDATE promotion
    SET new_scenario = NULL, shift_by = NULL
    WHERE promotion_id = member_id;
  
    COMMIT;  
    RAISE;
  END;
END prc_duplicate_scenario;

PROCEDURE prc_freeze_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
  NAME:       PRC_FREEZE_SCENARIO
  PURPOSE:    Procedure to freeze a scenario
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
   1.0        25/03/2013  Lakshmi Annapragada / Redrock Consulting - Initial Version
  ******************************************************************************/
  v_prog_name   VARCHAR2 (100);
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_pg_id       NUMBER;
  v_scenario_id NUMBER;

BEGIN
  pre_logon;
  v_status      := 'start ';
  v_prog_name   := 'PRC_FREEZE_SCENARIO';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    
    SELECT promotion_guidelines_id, scenario_id INTO v_pg_id, v_scenario_id 
    FROM promotion 
    WHERE promotion_id = member_id;

    UPDATE promotion SET  promotion_type_id_bk =
                               DECODE (promotion_type_id,
                                 24, NVL(promotion_type_id_bk, 0),
                                 NVL(promotion_type_id, 0)
                                  ),
                          promotion_type_id = 24, 
                          last_update_date = SYSDATE
                   WHERE scenario_id = v_scenario_id
                   AND promotion_guidelines_id = v_pg_id;     
                     
     UPDATE promotion_data SET  promotion_type_id_bk =
                               DECODE (NVL(promotion_type_id, 0),
                                 24, NVL(promotion_type_id_bk, 0),
                                 NVL(promotion_type_id, 0)
                                  ),                          
                          activity_type_id_bk = 
                                DECODE (NVL(activity_type_id, 0),
                                 24, NVL(activity_type_id_bk, 0),
                                 NVL(activity_type_id, 0)
                                  ),
                          promotion_type_id = 24, 
                          activity_type_id = 24,
                          last_update_date = SYSDATE
              WHERE promotion_id IN (SELECT promotion_id 
                                       FROM promotion
                                      WHERE scenario_id = v_scenario_id
                                      AND promotion_guidelines_id = v_pg_id);                                   
     
      Merge /*+ PARALLEL(p,2) */ Into Promotion_Matrix P
            USING (SELECT DISTINCT promotion_id, item_id, location_id, promotion_type_id
                              FROM promotion_data pd
                               WHERE scenario_id = v_scenario_id
                                AND promotion_guidelines_id = v_pg_id
                               ) pd1
            ON (p.promotion_id = pd1.promotion_id
            AND p.item_id = pd1.item_id
            AND p.location_id = pd1.location_id)
            WHEN MATCHED THEN
               UPDATE
                  SET p.promotion_lud = SYSDATE, p.promotion_data_lud = sysdate,
                      p.promotion_type_id = pd1.promotion_type_id;
   
   COMMIT;
     
  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_freeze_scenario;   

PROCEDURE prc_unfreeze_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
  NAME:       PRC_UNFREEZE_SCENARIO
  PURPOSE:    Procedure to unfreeze a scenario
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
   1.0        25/03/2013  Lakshmi Annapragada / Redrock Consulting - Initial Version
  ******************************************************************************/
  v_prog_name   VARCHAR2 (100);
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
   max_sales_date   DATE;
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (10000);
   v_pg_id       NUMBER;
   v_scenario_id NUMBER;
   
BEGIN
  pre_logon;
  v_status      := 'start ';
  v_prog_name   := 'PRC_UNFREEZE_SCENARIO';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   
    SELECT max(promotion_guidelines_id), max(scenario_id) INTO v_pg_id, v_scenario_id 
    FROM promotion 
    WHERE promotion_id = member_id;
   
   UPDATE promotion SET promotion_type_id = 
                               DECODE (NVL(promotion_type_id_bk, 0),
                                 0, NVL(promotion_type_id, 0),
                                 NVL(promotion_type_id_bk, 0)
                                  ),
                      last_update_date = SYSDATE
                  WHERE scenario_id = v_scenario_id
                  AND promotion_guidelines_id = v_pg_id  ;
                  
     UPDATE promotion_data SET  promotion_type_id =
                               DECODE (NVL(promotion_type_id_bk, 0),
                                 0, NVL(promotion_type_id, 0),
                                 NVL(promotion_type_id_bk, 0)
                                  ),                           
                          activity_type_id =
                               DECODE (NVL(activity_type_id_bk, 0),
                                 0, NVL(activity_type_id, 0),
                                 NVL(activity_type_id_bk, 0)
                                  ),
                          last_update_date = SYSDATE
              WHERE promotion_id IN (SELECT promotion_id 
                                       FROM promotion
                                      WHERE scenario_id = v_scenario_id
                                      AND promotion_guidelines_id = v_pg_id);                                   
    
     Merge /*+ PARALLEL(p,2) */ Into Promotion_Matrix P
            USING (SELECT DISTINCT promotion_id, item_id, location_id, max(promotion_type_id) promotion_type_id
                              FROM promotion_data pd
                               WHERE scenario_id = v_scenario_id
                                AND promotion_guidelines_id = v_pg_id
                                group by promotion_id, item_id, location_id
                               ) pd1
            ON (p.promotion_id = pd1.promotion_id
            AND p.item_id = pd1.item_id
            AND p.location_id = pd1.location_id)
            WHEN MATCHED THEN
               UPDATE
                  SET p.promotion_lud = SYSDATE, p.promotion_data_lud = sysdate,
                      p.promotion_type_id = pd1.promotion_type_id;
   COMMIT;
     
  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_unfreeze_scenario;   

PROCEDURE prc_sync_scn_deals(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_SYNC_SCN_DEALS
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_SYNC_SCN_DEALS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_scn_id      NUMBER;
   
  -- v_ms           NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_SYNC_DEALS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
  /* SELECT pt.rr_margin_support
   INTO v_ms
   FROM promotion_type pt,
   promotion p
   WHERE p.promotion_type_id = pt.promotion_type_id
   AND p.promotion_id = member_id
   AND scenario_id <> 265;
   
   v_ms := 0;          */
   
   pkg_scn_plng.prc_get_scn_promos(member_id);   
   pkg_scn_plng.prc_set_mdp_allocation( member_id);
   pkg_scn_plng.prc_fix_promo_type( member_id);
   
 --  IF v_ms = 0 THEN 
   
      pkg_scn_plng.prc_calc_accrual( member_id);
      pkg_scn_plng.prc_promo_accrual_sync( member_id, 3, 0);
      pkg_scn_plng.prc_margin_support( member_id);
      pkg_scn_plng.prc_promo_accrual_sync( member_id, 3, 1);
      
--   END IF;
   
  /* IF v_ms = 1 THEN 
      
      pkg_sync_scn_pnl.prc_calc_accrual(user_id, member_id);
      pkg_sync_scn_pnl.prc_promo_accrual_sync(user_id, member_id, 3, 0);
      pkg_sync_scn_pnl.prc_margin_support(user_id, member_id);
      pkg_sync_scn_pnl.prc_promo_accrual_sync(user_id, member_id, 3, 1);
   
   END IF;
                  */
    pkg_scn_plng.prc_set_wif_scn_flag(member_id,1);
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
  
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END prc_sync_scn_deals;
  
  PROCEDURE prc_promote_wif_scn(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_PROMOTE_WIF_SCN
   PURPOSE:    Procedure to promote the what if scenario to Live scenario

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        08/04/2013  Lakshmi Annapragada / Red Rock Consulting

******************************************************************************/

   sql_str             VARCHAR2 (5000);

   v_prog_name         VARCHAR2 (100) := 'PRC_PROMOTE_WIF_SCN';
   v_status            VARCHAR2 (100);
   v_proc_log_id       NUMBER;
   v_live_scn_id       NUMBER;
   v_wif_scn_id        NUMBER;
   v_live_guideline_id NUMBER;
   sce_promo           NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_PROMOTE_WIF_SCN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   v_wif_scn_id := member_id;
   
   SELECT DISTINCT scenario_id, promotion_guidelines_id INTO v_live_scn_id, v_live_guideline_id
   FROM promotion 
   WHERE promotion_id IN (SELECT scn_source_promo_id  
   FROM promotion  
   WHERE scenario_id = v_wif_scn_id 
   AND promotion_id <> 0
   AND NVL(scn_source_promo_id, 0) <> 0
   AND rownum < 2);
   
   check_and_drop('rr_tmp_promote_' || member_id || '_promos_t');
   
   /* Add all promotions from Live scenario with status > = Accrual*/
   dynamic_ddl('CREATE TABLE rr_tmp_promote_' || member_id || '_promos_t AS    
                      SELECT  promotion_id
                        FROM promotion p
                       WHERE scenario_id = '||v_live_scn_id||'                         
                         AND promotion_id IN (SELECT scn_source_promo_id FROM promotion 
                                                WHERE scenario_id = '|| v_wif_scn_id||'
                                                AND NVL(scn_source_promo_id, 0) <> 0
                                                AND promotion_guidelines_id = ' || v_live_guideline_id || ')
                         AND promotion_stat_id >= 5'); --Accrual
   
   /* All the promotions in what if scenario with status < = Committed */ 
   dynamic_ddl('INSERT INTO rr_tmp_promote_' || member_id || '_promos_t
   SELECT promotion_id 
     FROM promotion 
    WHERE scenario_id = '||v_wif_scn_id||'
      AND promotion_id <> 0
      AND promotion_stat_id <= 4'); -- Committed 
   
   /* All the promotions that are created new in what if scenario */ 
   dynamic_ddl('INSERT INTO rr_tmp_promote_' || member_id || '_promos_t
   SELECT promotion_id 
     FROM promotion 
    WHERE scenario_id = '||v_wif_scn_id||'
      --AND scn_source_promo_id IS NULL
      AND NVL(scn_source_promo_id, 0) = 0
      AND promotion_id NOT IN (SELECT promotion_id FROM rr_tmp_promote_' || member_id || '_promos_t)');  
    
    COMMIT;
    
    pkg_scn_plng.prc_set_wif_scn_flag(v_wif_scn_id,0);
    
    dynamic_ddl('DELETE FROM promotion 
    WHERE scenario_id in ('||v_wif_scn_id ||') 
    AND promotion_id <> 0
    AND promotion_id NOT IN (SELECT promotion_id 
                             FROM rr_tmp_promote_' || member_id || '_promos_t)'); 
                             
    COMMIT;
    
    dynamic_ddl('INSERT INTO rr_scn_refresh_t(level_id, member_id, processed, batch_id, op_mode)
      SELECT 232, promotion_id, 0, rr_scn_refresh_seq.nextval, 3
      FROM promotion 
      WHERE scenario_id in ('|| v_live_scn_id||') 
      AND promotion_id <> 0
      AND promotion_guidelines_id = ' || v_live_guideline_id || '
      AND promotion_id NOT IN (SELECT promotion_id 
                             FROM rr_tmp_promote_' || member_id || '_promos_t)'); 
    
    COMMIT;
    
    dynamic_ddl('DELETE FROM promotion 
    WHERE scenario_id in ('|| v_live_scn_id||') 
    AND promotion_guidelines_id = ' || v_live_guideline_id || '
    AND promotion_id <> 0
    AND promotion_id NOT IN (SELECT promotion_id 
                             FROM rr_tmp_promote_' || member_id || '_promos_t)'); 
                             
    COMMIT;
    
    check_and_drop('rr_promote_scn_tmp_t');
           
    dynamic_ddl('CREATE TABLE rr_promote_scn_tmp_t AS
           SELECT promotion_id FROM promotion
           WHERE scenario_id = '||v_wif_scn_id||'
           AND NVL(scn_source_promo_id, 0) <> 0
           AND promotion_id <> 0
           AND promotion_id IN (SELECT promotion_id 
                             FROM rr_tmp_promote_' || member_id || '_promos_t)');                     
    
    COMMIT;
    
    UPDATE promotion
      SET scenario_id = v_live_scn_id, scn_source_promo_id = NULL
      WHERE promotion_id IN (SELECT promotion_id FROM rr_promote_scn_tmp_t);
    
    dynamic_ddl('INSERT INTO rr_scn_refresh_t(level_id, member_id, processed, batch_id, op_mode)
      SELECT 232, promotion_id, 0, rr_scn_refresh_seq.nextval, 2
      FROM promotion 
      WHERE promotion_id IN (SELECT promotion_id FROM rr_promote_scn_tmp_t)');
    
    COMMIT;
    
    UPDATE promotion_matrix
      SET scenario_id = v_live_scn_id      
      WHERE promotion_id IN (SELECT promotion_id FROM rr_promote_scn_tmp_t);
    
      
    UPDATE promotion_data
      SET scenario_id = v_live_scn_id
      WHERE promotion_id IN (SELECT promotion_id FROM rr_promote_scn_tmp_t);  
                                                    
     DELETE FROM promotion WHERE scenario_id = v_wif_scn_id;
     
     COMMIT;  
     
     DELETE FROM scenario WHERE scenario_id = v_wif_scn_id;
     
     COMMIT; 
     
     dynamic_ddl('INSERT INTO rr_scn_refresh_t(level_id, member_id, processed, batch_id, op_mode)
      VALUES (233, ' || v_wif_scn_id|| ', 0, rr_scn_refresh_seq.nextval, 3)');
     
     COMMIT;
     
     UPDATE promotion_matrix
                SET last_update_date = SYSDATE, 
                   promotion_data_lud = SYSDATE,
                   promotion_lud = SYSDATE,
                   scenario_lud = SYSDATE
                WHERE scenario_id IN (v_live_scn_id,v_wif_scn_id);    
                
    UPDATE scenario 
      SET last_update_date = sysdate 
      WHERE scenario_id IN (v_live_scn_id,v_wif_scn_id);
    
    UPDATE promotion_data 
        SET last_update_date = SYSDATE
        WHERE scenario_id IN (v_live_scn_id,v_wif_scn_id);
        
    UPDATE promotion 
        SET last_update_date = SYSDATE
        WHERE scenario_id IN (v_live_scn_id,v_wif_scn_id);
                
      COMMIT;          
      
    select promotion_id into sce_promo
    from promotion
    where scenario_id in (v_live_scn_id)
    and promotion_guidelines_id = v_live_guideline_id
    and rownum = 1;
   
   pkg_scn_plng_lm.prc_unfreeze_scenario(user_id,level_id,sce_promo);    
   pkg_scn_plng.prc_set_wif_scn_flag(sce_promo,0);
  
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END prc_promote_wif_scn;
 
  PROCEDURE prc_delete_wif_scn_lm(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_DELETE_WIF_SCN_LM
   PURPOSE:    Procedure to delete the what if scenario

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        08/04/2013  Lakshmi Annapragada / Red Rock Consulting

******************************************************************************/

   sql_str          VARCHAR2 (5000);

   v_prog_name    VARCHAR2 (100) := 'PRC_DELETE_WIF_SCN_LM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_live_scn_id      NUMBER;
   v_wif_scn_id   NUMBER;
   sce_promo      NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_DELETE_WIF_SCN_LM';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
    
    select scn_source_promo_id into sce_promo
    from promotion
    where scenario_id in (member_id)
    and SCN_SOURCE_PROMO_ID is not null
    and rownum = 1;
    
   pkg_scn_plng_lm.prc_unfreeze_scenario(user_id,level_id,sce_promo);  
    
   pkg_scn_plng.prc_set_wif_scn_flag(member_id,0);
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END prc_delete_wif_scn_lm;    

  PROCEDURE prc_reload_sales_base(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_RELOAD_SALES_BASE
   PURPOSE:    Procedure to delete the what if scenario

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        08/04/2013  Lakshmi Annapragada / Red Rock Consulting

******************************************************************************/

    start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);
   
   
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_RELOAD_SALES_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
     
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_RELOAD_SALES_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
    
   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;
   
   check_and_drop('tbl_wif_scn_' || member_id || '_t');

   sql_str := 'CREATE TABLE tbl_wif_scn_' || member_id || '_t
               AS SELECT promotion_id FROM promotion
               WHERE scenario_id = ' || member_id ;
                  
   dynamic_ddl(sql_str);
   
   sql_str := ('MERGE INTO promotion_data pd
      USING (SELECT 
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
             CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') 
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.dol_deduct_nine,NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0)))
                        ELSE 0 
                        END 
                  ELSE pd.volume_base_ttl
             END volume_base_ttl,
             CASE WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END evt_vol_act,
             CASE WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END incr_evt_vol_act,
             i.e1_ship_uom_mult units
             FROM sales_data sd, 
             promotion_data pd, 
             promotion_type pt, 
             promotion_stat ps, 
             promotion_matrix pm,
             mdp_matrix mdp,
             t_ep_item i,
             tbl_wif_scn_' || member_id || '_t pg
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND pg.promotion_id = pm.promotion_id
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.item_id = sd.item_id
             AND mdp.location_id = sd.location_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id             
             AND pm.scenario_id <> 265
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             --AND pg.t_ep_p2b_ep_id = mdp.t_ep_p2b_ep_id
             AND (NVL(CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') 
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.dol_deduct_nine,NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0)))
                        ELSE 0
                        END 
                  ELSE pd.volume_base_ttl
             END, 0) <> NVL(pd.volume_base_ttl, 0)
             OR NVL(CASE WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END, -99899899899) <> NVL(pd.evt_vol_act, -99899899899)
             OR NVL(CASE WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END, -99899899899) <> NVL(pd.incr_evt_vol_act, -99899899899)
             OR NVL(i.e1_ship_uom_mult, 0) <> NVL(pd.units, 0)   
             )
             ) pd1
      ON (pd1.promotion_id = pd.promotion_id
          AND pd1.item_id = pd.item_id
          AND pd1.location_id = pd.location_id
          AND pd1.sales_date = pd.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd.volume_base_ttl = pd1.volume_base_ttl,
                pd.evt_vol_act = pd1.evt_vol_act,
                pd.incr_evt_vol_act = pd1.incr_evt_vol_act,              
                pd.units = pd1.units,
                pd.last_update_date = SYSDATE
                ');
         
   dynamic_ddl(sql_str);
         
   COMMIT;
   
   UPDATE promotion_matrix
   SET last_update_date = SYSDATE,
   promotion_data_lud = SYSDATE
   WHERE scenario_id = member_id
   AND promotion_stat_id <> 8
   AND scenario_id <> 265;
   
   COMMIT;
   
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END prc_reload_sales_base; 
       
END;

/
