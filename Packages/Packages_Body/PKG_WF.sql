--------------------------------------------------------
--  DDL for Package Body PKG_WF
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_WF" 
AS
/******************************************************************************
   NAME:         PKG_WF  BODY
   PURPOSE:      All procedures commanly used for WF module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

   ******************************************************************************/
 PROCEDURE prc_wf_EPload
  is
/******************************************************************************
   NAME:       PRC_WF_MASTER_DATA_LOAD
   PURPOSE:    Procedure to Load Items/Customers

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        01/01/2016  Lakshmi Annapragada / UXC Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_EPLOAD';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_EPLOAD';
   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
--   prc_wf_lockusers;
  prc_wf_master_data_load;
  PRC_WF_SALES_LOAD;
  prc_wf_mdp_add;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_epload;

  PROCEDURE prc_wf_master_data_load
  IS
/******************************************************************************
   NAME:       PRC_WF_MASTER_DATA_LOAD
   PURPOSE:    Procedure to Load Items/Customers

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        01/01/2016  Lakshmi Annapragada / UXC Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_MASTER_DATA_LOAD';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_MASTER_DATA_LOAD';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;

   PKG_DATA_LOAD.PRC_TRUNCATE_SRC;
--   rr_prc_fix_promo_grp;
   pkg_data_load.prc_update_src;
   pkg_data_load.prc_replace_apostrophe;
   pkg_data_load.prc_ep_check_items;
   pkg_data_load.prc_ep_load_items;
   pkg_data_load.prc_ep_check_location;
   pkg_data_load.prc_ep_load_locations;

    PKG_DATA_LOAD.PRC_LOAD_GST_RATE;

   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_master_data_load;

  PROCEDURE prc_wf_sales_load
  IS
/******************************************************************************
   NAME:       PRC_WF_MASTER_DATA_LOAD
   PURPOSE:    Procedure to Load Items/Customers

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        01/01/2016  Lakshmi Annapragada / UXC Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_SALES_LOAD';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_SALES_LOAD';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;

   PKG_DATA_LOAD.PRC_TRUNCATE_Sales;
   pkg_data_load.prc_update_sales;
   pkg_data_load.prc_replace_apostrophe;
   pkg_data_load.prc_ep_load_sales;
--   pkg_data_load.prc_mdp_add;
--   pkg_data_load.prc_ep_load_mdp_level;
--   pkg_data_load.prc_update_from_till_date;
--   pkg_dm.prc_set_do_not_forecast;
--
--   pkg_ptp.prc_out_of_strategy;
--   PKG_PTP.PRC_CHECK_PROMO;
--   pkg_ptp.prc_update_promo_program;
--   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_sales_load;

    PROCEDURE prc_wf_load_scan
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_LOAD_SCAN';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   V_FY_ID            NUMBER;
   v_cnt NUMBER := 0;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

   select count(1) into v_cnt from t_src_sapl_scan ;
pkg_data_load.prc_load_scan;

   pkg_ptp.prc_offset_scan(sysdate-20*7,sysdate-7);
   IF(V_CNT>0)
   then
      PKG_PTP.PRC_OFFSET_SCAN(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 15*7,NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7);
   END IF;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END prc_wf_load_scan;

  PROCEDURE prc_wf_mdp_add
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_MDP_ADD';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    pkg_data_load.prc_mdp_add;
   pkg_data_load.prc_ep_load_mdp_level;
   pkg_data_load.prc_update_from_till_date;
   PKG_DM.PRC_SET_DO_NOT_FORECAST;

   PKG_PTP.PRC_UPDATE_PROMO_PROGRAM;
      pkg_dm.prc_update_dem_type;
   PKG_COMMON_UTILS.PRC_SET_MAX_SALES_DATE;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END PRC_WF_MDP_ADD;

    PROCEDURE prc_wf_load_claims
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_LOAD_CLAIMS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

pkg_data_load.prc_load_claims;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END prc_wf_load_claims;

   PROCEDURE prc_wf_pre_load_fcst
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PRE_LOAD_FCST';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

--PKG_PTP.PRC_POP_EXFACT_BASE(SYSDATE-7,NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (53 * 7));
----pkg_data_load.prc_cl_truncate_apo_fcst;
  pkg_apo.PRC_SYNC_FULL_INCREMENTALS(NEXT_DAY(TRUNC(sysdate, 'DD'), 'MONDAY') - (9 * 7), NEXT_DAY(TRUNC(sysdate, 'DD'), 'MONDAY') + (104 * 7));
prc_del_err_notifications;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         end;
  end PRC_WF_PRE_LOAD_FCST;

   PROCEDURE prc_wf_load_fcst_daily
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_LOAD_FCST_DAILY';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

--PKG_PTP.PRC_POP_EXFACT_BASE(SYSDATE-7,SYSDATE+105*7);
--pkg_data_load.prc_cl_truncate_apo_fcst;
--prc_wf_lockusers;
pkg_apo.prc_upd_apo_stg_daily;
pkg_apo.prc_insert_new_apo_combs;
pkg_data_load.prc_load_apo_fcst;
--PKG_PTP.PRC_POP_APO_FCST(sysdate-7,sysdate+110*7);
PRC_FORMAT_APO_ERRORS;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END prc_wf_load_fcst_daily;

     PROCEDURE prc_wf_load_fcst_weekly
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_LOAD_FCST_WEEKLY';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

PKG_APO.PRC_POP_EXFACT_BASE(SYSDATE-7,SYSDATE+105*7);
--pkg_data_load.prc_cl_truncate_apo_fcst;
--prc_wf_lockusers;
pkg_apo.prc_upd_apo_stg_weekly;
pkg_apo.prc_insert_new_apo_combs;
pkg_data_load.prc_load_apo_fcst;
--PKG_PTP.PRC_POP_APO_FCST(sysdate-7,sysdate+110*7);
PRC_FORMAT_APO_ERRORS;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END PRC_WF_LOAD_FCST_weekly;

  PROCEDURE prc_wf_processclaim
  IS
/******************************************************************************
   NAME:       PRC_WF_PART2
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PROCESSCLAIM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_PROCESSCLAIM';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   pkg_common_utils.prc_set_max_sales_date;
   pkg_claims.prc_link_claim;
   pkg_claims.prc_check_duplicate_claim;
   pkg_claims.prc_approve_claim;
   pkg_claims.prc_split_claim;
--lannapra   pkg_export.prc_export_claims;
   pkg_claims.prc_update_days_out;
   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_processclaim;

  PROCEDURE prc_wf_lockusers
  IS
/******************************************************************************
   NAME:       PRC_WF_PART3
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_LOCKUSERS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_LOCKUSERS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_data_load.prc_disable_logging;
   pkg_common_utils.prc_lock_users;
   pkg_common_utils.prc_disconnect_sessions;
   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_lockusers;


  PROCEDURE prc_wf_unlockusers
  IS
/******************************************************************************
   NAME:       PRC_WF_PART5
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_UNLOCKUSERS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_UNLOCKUSERS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_unlock_users;
   pkg_data_load.prc_enable_logging;
   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_unlockusers;

    PROCEDURE prc_wf_refreshpromo
  IS
/******************************************************************************
   NAME:       PRC_WF_PART7
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_REFRESHPROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_weeks        NUMBER := 52;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_REFRESHPROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

         pkg_scn_plng.prc_full_freeze;

      pkg_common_utils.prc_run_wf_schema('RefreshPopulationFull', 'Y');

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END PRC_WF_REFRESHPROMO;


  PROCEDURE prc_wf_promoaccrual
  IS
/******************************************************************************
   NAME:       PRC_WF_PART7
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PROMOACCRUAL';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   --v_weeks        NUMBER := 52;
  
  -- v_weeks        NUMBER := 70;
   
     -- v_weeks        NUMBER := 96; --Added by Rajesh Reddi to synch Accruals for year 2016
       v_weeks        NUMBER := 104; --Added by Rajesh Reddi to synch Accruals to consider for last 2 years


  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_PROMOACCRUAL';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;
   --pkg_scn_plng.prc_del_old_wif_scn;
   PKG_APO.PRC_POP_APO_FCST(sysdate-7,sysdate+104*7);
   PKG_PTP.PRC_UPDATE_PROMO;
--      pkg_scn_plng.prc_full_freeze;
--      pkg_common_utils.prc_run_wf_schema('RefreshPopulationFull', 'Y');
   pkg_ptp.prc_correct_pd(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7));
   pkg_ptp.prc_promo_commit_ss;
   PKG_ACCRUAL.PRC_SET_MDP_ALLOCATION(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (V_WEEKS * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (76 * 7));
   pkg_accrual.prc_clear_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (76 * 7));
   pkg_accrual.prc_calc_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7));
   pkg_accrual.prc_promo_accrual_sync(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7), 3, 0);--IBM  SD uplift for 104 week
   pkg_accrual.prc_margin_support(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (76 * 7));
   pkg_accrual.prc_promo_accrual_sync(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7), 3, 1);
   pkg_export.prc_export_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), greatest(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, trunc(sysdate-1,'MM')));
    pkg_ptp.prc_check_promo;
   PKG_PTP.PRC_OUT_OF_STRATEGY;
   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_promoaccrual;

  PROCEDURE prc_wf_promoaccrual_q1
  IS
/******************************************************************************
   NAME:       PRC_WF_PART7
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PROMOACCRUAL_Q1';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_weeks        NUMBER := 52;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_PROMOACCRUAL_Q1';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;
   --pkg_scn_plng.prc_del_old_wif_scn;
   PKG_APO.PRC_POP_APO_FCST(sysdate-7,sysdate+4*7);
      PKG_PTP.PRC_UPDATE_PROMO;
--      pkg_scn_plng.prc_full_freeze;
--      pkg_common_utils.prc_run_wf_schema('RefreshPopulationFull', 'Y');
   pkg_ptp.prc_correct_pd(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (4 * 7));
   pkg_ptp.prc_promo_commit_ss;
   PKG_ACCRUAL.PRC_SET_MDP_ALLOCATION(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (V_WEEKS * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_accrual.prc_clear_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_accrual.prc_calc_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_accrual.prc_promo_accrual_sync(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7), 3, 0);
   pkg_accrual.prc_margin_support(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_accrual.prc_promo_accrual_sync(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7), 3, 1);
   pkg_export.prc_export_accrual(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (v_weeks * 7), greatest(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, trunc(sysdate-1,'MM')));
    pkg_ptp.prc_check_promo;
   PKG_PTP.PRC_OUT_OF_STRATEGY;
   pkg_common_utils.prc_set_max_sales_date;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_promoaccrual_q1;

  PROCEDURE prc_wf_promoaccrualos
  IS
/******************************************************************************
   NAME:       PRC_WF_PART8
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PROMOACCRUALOS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_PROMOACCRUALOS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;
   pkg_accrual.prc_accrual_over_spend(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (104 * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_export.prc_export_accrual_overspend(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (104 * 7), TRUNC(SYSDATE, 'DD') + 1);
   pkg_accrual.prc_calc_accrual_close(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (104 * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_export.prc_export_accrual_close(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (104 * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (52 * 7));
   pkg_common_utils.prc_set_max_sales_date;

--One-off
--PKG_BUDGET.PRC_PNL_SS1(TO_DATE('01/01/2016','DD/MM/YYYY'),TO_DATE('31/12/2016','DD/MM/YYYY'));
--PKG_BUDGET.PRC_PNL_SS2(TO_DATE('01/01/2016','DD/MM/YYYY'),TO_DATE('31/12/2016','DD/MM/YYYY'));


   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_promoaccrualos;

  PROCEDURE prc_wf_export_fcst
  IS
/******************************************************************************
   NAME:       PRC_WF_PART9
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_EXPORT_FCST';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_EXPORT_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_common_utils.prc_set_max_sales_date;
   /*Need to add the procedure to calculate incremental fcst here*/
--   RR_PRC_SYNC_FULL_INCREMENTALS(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (15 * 7), NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (76 * 7));
--   pkg_ptp.prc_offset_scan(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 15*7,NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7);
--   pkg_ptp.prc_offset_dm_fcst(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (110 * 7));
--   pkg_export.prc_export_fcst;
pkg_export.prc_export_fcst_daily;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_wf_export_fcst;

 PROCEDURE prc_wf_export_fcst_weekly
  IS
/******************************************************************************
   NAME:       PRC_WF_PART9
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_EXPORT_FCST_WEEKLY';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   V_STATUS := 'Start ';
   v_prog_name := 'PRC_WF_EXPORT_FCST_WEEKLY';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


pkg_export.prc_export_fcst;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         end;
  END prc_wf_export_fcst_weekly;
  PROCEDURE prc_maintenance
  IS
/******************************************************************************
   NAME:       PRC_WF_PART9
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_MAINTENANCE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_MAINTENANCE_WEEK';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   pkg_common_utils.prc_rebuild_tables;    -- By SR --

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_wf_accrual_cancel
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_ACCRUAL_CANCEL
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);

   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_WF_ACCRUAL_CANCEL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;


    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');


    EXECUTE IMMEDIATE ' INSERT INTO t_exp_gl(batch_id, material, customer_no, promotion_id,sales_org,dist_channel,division, sales_date, case_deal_amount, coop_amount,
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    SELECT ' || v_batch_id || ',  I.I_ATT_1 , S.E1_CUST_CAT_4, P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,
    pd.sales_date, pd.rr_accrual_cd_post_gl, pd.rr_accrual_coop_post_gl, pt.promotion_type_desc,
    cdt.deal_type_code, coopt.deal_type_code, TO_DATE(''' || TO_CHAR(v_batch_date, 'DD/MM/YYYY HH24:MI:SS') || ''', ''DD/MM/YYYY HH24:MI:SS'') , ''CANCEL'',rec_cust.e1_cust_cat_4
    FROM T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    t_ep_p3 dc,
    t_ep_e1_cust_cat_4 rec_cust,
    promotion p,
    accrual_data pd,
    promotion_type pt,
    mdp_matrix mdp,
    activity ac,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
     and p.activity_id = ac.activity_id
    and p.recipient = rec_cust.t_ep_e1_cust_cat_4_ep_id
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND p.scenario_id IN (22, 262, 162)
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.rr_export_cancel = 0 and p.promotion_stat_id = 9
   AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
    AND ABS(NVL(pd.rr_accrual_cd_post_gl, 0) ) +
        ABS(NVL(pd.rr_accrual_coop_post_gl, 0) ) <> 0
  --  AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
    ';

   --COMMIT;

    sql_str := 'MERGE INTO promotion p
    USING(SELECT promotion_id
          FROM promotion where rr_Export_cancel = 0 and promotion_stat_id = 9) p1
    ON(p.promotion_id = p1.promotion_id)
    WHEN MATCHED THEN
    UPDATE SET rr_export_cancel = 1,
    last_update_date = SYSDATE';

    dynamic_ddl (sql_str);

   insert into T_EXP_GL_MASTER
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER
                 FROM t_exp_gl WHERE batch_id = v_batch_id);


   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

  PROCEDURE prc_maintenance_month
  IS
/******************************************************************************
   NAME:       PRC_WF_PART9
   PURPOSE:    Procedure to maintainance on monthly basis

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        17/10/2014  SR/ Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_MAINTENANCE_MONTH';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_MAINTENANCE_MONTH';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   drop_temps(0);

rr_analyze_schema(0);

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;



 PROCEDURE prc_wf_roll_shelf_price
  IS
/******************************************************************************
   NAME:       PRC_ROLL_SHELF_PRICE
   PURPOSE:    Procedure to check promotion out of strategy

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        24/09/2012  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_no_weeks       NUMBER := 52;

   v_prog_name    VARCHAR2 (100) := 'PRC_ROLL_SHELF_PRICE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN


   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_ROLL_SHELF_PRICE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS'),
                   'MM/DD/YYYY'
                  )
     INTO max_sales_date
     FROM DUAL;

   SELECT TO_CHAR (  TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                   - (v_no_weeks * 7),
                   'MM/DD/YYYY'
                  )
     INTO min_date
     FROM DUAL;

   max_date := max_sales_date;

   check_and_drop('tbl_shelf_price_roll');

   -- cmos 23-Jan-2017 (px ctas)
   dynamic_ddl('CREATE TABLE tbl_shelf_price_roll parallel 32 nologging AS
                SELECT /*+ FULL(sd) PARALLEL(sd, 32) */  sd.item_id, sd.location_id,
                CASE WHEN SUM(ABS(NVL(sd.sdata5, 0)) + ABS(NVL(sd.sdata6, 0))) <> 0
                    THEN MAX(CASE WHEN ABS(NVL(sd.sdata5, 0)) + ABS(NVL(sd.sdata6, 0)) <> 0 THEN sd.sales_date ELSE CAST(NULL AS DATE) END)
                     ELSE MAX(sd.sales_date)
                END sales_date
                FROM sales_data sd,
               mdp_matrix mdp
                WHERE sd.item_id = mdp.item_id
                AND sd.location_id = mdp.location_id
                AND mdp.dem_stream = 1
                AND NVL(sd.shelf_price_sd, 0) <> 0
                AND sd.sales_date BETWEEN TO_DATE(''' || min_date || ''', ''MM/DD/YYYY'')
                                      AND TO_DATE(''' || max_date || ''', ''MM/DD/YYYY'')
                GROUP BY sd.item_id, sd.location_id');
   dynamic_ddl('alter table tbl_shelf_price_roll noparallel');		-- cmos 23-Jan-2017

   check_and_drop('tbl_shelf_price');

   -- cmos 23-Jan-2017 (px ctas)
   dynamic_ddl('CREATE TABLE tbl_shelf_price parallel 32 nologging AS
                SELECT /*+ FULL(sd) PARALLEL(sd, 32) */  sd.item_id, sd.location_id, sd.sales_date, sd.shelf_price_sd
                FROM sales_data sd,
                tbl_shelf_price_roll sd1
                WHERE sd.item_id = sd1.item_id
                AND sd.location_id = sd1.location_id
                AND sd.sales_date = sd1.sales_date ');
   dynamic_ddl('alter table tbl_shelf_price noparallel');		-- cmos 23-Jan-2017

   dynamic_ddl('MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
                USING(SELECT /*+ FULL(sd) PARALLEL(sd, 32) */ sd.item_id, sd.location_id, sd.sales_date, t.shelf_price_sd
                      FROM tbl_shelf_price t,
                      sales_data sd
                      WHERE t.item_id = sd.item_id
                      AND t.location_id = sd.location_id
                      AND t.sales_date < sd.sales_date
                      AND NVL(t.shelf_price_sd, 0) <>  NVL(sd.shelf_price_sd, 0)) sd1
                ON(sd.item_id = sd1.item_id
                AND sd.location_id = sd1.location_id
                AND sd.sales_date = sd1.sales_date)
                WHEN MATCHED THEN
                UPDATE SET sd.shelf_price_sd = sd1.shelf_price_sd
                 ');
   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


  PROCEDURE prc_export_contracts
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_EXPORT_CONTRACTS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   pkg_export.prc_export_contracts(1,1);
--   PKG_EXPORT.PRC_EXPORT_OI;
   pkg_common_utils.prc_run_wf_schema('ExceptionWorkflowDaily', 'Y');


   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
END prc_export_contracts;

PROCEDURE prc_export_claims
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_EXPORT_CLAIMS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   pkg_export.prc_export_claims;

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
END prc_export_claims;

 PROCEDURE prc_wf_pre_engine
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_PRE_ENGINE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_WF_PRE_ENGINE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_scn_plng.prc_full_freeze;
   pkg_dm.prc_update_sim_cols;
  PKG_COMMON_UTILS.PRC_RUN_FULL_PROPORT_1;
   pkg_common_utils.prc_run_wf_schema('ExceptionWorkflowWeekly', 'Y');

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END prc_wf_pre_engine;


PROCEDURE prc_wf_post_engine
  IS
/******************************************************************************
   NAME:       PRC_EXPORT_BI
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_WF_POST_ENGINE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;

  BEGIN

   pre_logon;
   V_STATUS := 'Start ';
   v_prog_name := 'PRC_WF_POST_ENGINE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   pkg_apo.prc_offset_dm_fcst(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7));

   v_status := 'End ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
         END;
  END PRC_WF_POST_ENGINE;


END;

/
