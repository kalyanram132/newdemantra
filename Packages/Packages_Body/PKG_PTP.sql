--------------------------------------------------------
--  DDL for Package Body PKG_PTP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_PTP" 
AS
/******************************************************************************
   NAME:         PKG_PTP  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

   ******************************************************************************/

  PROCEDURE prc_check_promo
  IS
/******************************************************************************
   NAME:       PRC_CHECK_PROMO
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_no_weeks       NUMBER := 154;
   v_promo_week_range NUMBER := 10;

   v_lvl_id         NUMBER := 366;
   v_id_field       VARCHAR2(100);
   v_gtable_name     VARCHAR2(100);

   v_gt_id          VARCHAR2(100);
   v_unique_count   VARCHAR2(1000);
   v_unique_sel     VARCHAR2(1000);
   v_where_pct      VARCHAR2(2000);
   v_where_lvl      VARCHAR2(2000);
   v_unique_count_pct  VARCHAR2(1000);
   v_from_clause    VARCHAR2(2000);
   v_gtable         VARCHAR2(100);

   v_day         VARCHAR2(10);
   v_eng_profile NUMBER := 1;

   v_group_id1   VARCHAR2(2000);
   v_group_id2   VARCHAR2(2000);

   v_prog_name    VARCHAR2 (100) := 'PRC_CHECK_PROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CHECK_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   dynamic_ddl ('TRUNCATE TABLE t_ep_promotion_conflicts');

   SELECT gtable, id_field
   INTO v_gtable_name, v_id_field
   FROM group_tables
   WHERE group_table_id = v_lvl_id;

   UPDATE t_ep_ebs_customer
   SET t_ep_ebs_customer_ep_id_l = t_ep_ebs_customer_ep_id
   WHERE t_ep_ebs_customer_ep_id_l IS NULL;

   COMMIT;

   UPDATE /*+ parallel(pd,24) */ promotion_data pd
   SET pd.promo_conflict = 0,
       pd.t_ep_promo_conflict_id = 0
   WHERE NVL(pd.promo_conflict, 0) + NVL(pd.t_ep_promo_conflict_id, 0) <> 0
   AND scenario_id IN (22, 262, 162);

   COMMIT;

   UPDATE /*+ parallel(pm,24) */ promotion_matrix pm
   SET pm.t_ep_promo_conflict_id = 0
   WHERE pm.t_ep_promo_conflict_id = 1
   AND scenario_id IN (22, 262, 162);

   COMMIT;

   UPDATE /*+ parallel(p,24) nologging */promotion p
   SET conflict_01 = NULL,
       conflict_02 = NULL,
       conflict_03 = NULL,
       conflict_04 = NULL,
       conflict_05 = NULL,
       conflict_06 = NULL,
       conflict_07 = NULL,
       conflict_08 = NULL,
       conflict_09 = NULL,
       conflict_10 = NULL,
       RR_DISCOUNT_CHECK = 0,
       t_ep_promo_conflict_id = 0
   WHERE scenario_id IN (22, 262, 162)
    and ( t_ep_promo_conflict_id = 1
     or RR_DISCOUNT_CHECK = 1);

   COMMIT;

   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS'), 'MM/DD/YYYY')
   INTO max_sales_date
   FROM dual;

   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS') + (1 * 7), 'MM/DD/YYYY')
   INTO min_date
   FROM dual;

-- Set max check parameter for 8 weeks into future
   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS') + (v_no_weeks * 7), 'MM/DD/YYYY')
   INTO max_date
   FROM dual;

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   -----Update a flag to say a promotion is over 40% discount

       merge into promotion p using(
         SELECT /*+ parallel(pd, 12) */ distinct p.promotion_id
          FROM promotion p,
          promotion_data pd,
          promotion_type pt,
          promotion_stat ps,
          scenario s,
          promotion_dates pdt
          WHERE 1 = 1
          AND p.promotion_type_id = pt.promotion_type_id
          AND p.promotion_stat_id = ps.promotion_stat_id
          AND p.promotion_id = pd.promotion_id
          AND p.promotion_id = pdt.promotion_id
        --  AND NVL(pd.activity_type_id, 0) <> 0
          AND pd.is_self = 1
          AND p.scenario_id = s.scenario_id
          AND ps.rr_check_promo = 1
          AND s.rr_check_promo = 1
          AND pd.is_self = 1
          AND p.ignore_promo_conflict = 0
          AND nvl(pd.ed_price,0) <> 0
          AND (nvl(pd.ed_price,0) - nvl(pd.promo_price,0)) / nvl(pd.ed_price,0) between .4 and .99
          AND pdt.until_date >= min_date
          AND pdt.from_date <=  max_date
        ) n
        on (p.promotion_id = n.promotion_id)
        when matched then update set
        p.RR_DISCOUNT_CHECK = 1, last_update_date = sysdate;

   commit;

   check_and_drop('rr_promo_dates_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str :=
         'CREATE TABLE rr_promo_dates_t parallel 32 nologging AS
          SELECT /*+ parallel(pd, 12) */ pd.promotion_id, MIN(pd.sales_date) from_date, MAX(pd.sales_date) + 6 until_date
          FROM promotion p,
          promotion_data pd,
          promotion_type pt,
          promotion_stat ps,
          scenario s,
          promotion_dates pdt
          WHERE 1 = 1
          AND p.promotion_type_id = pt.promotion_type_id
          AND p.promotion_stat_id = ps.promotion_stat_id
          AND p.promotion_id = pd.promotion_id
          AND p.promotion_id = pdt.promotion_id
     --     AND NVL(pd.activity_type_id, 0) <> 0
          AND pd.is_self = 1
          AND p.scenario_id = s.scenario_id
          AND ps.rr_check_promo = 1
          AND s.rr_check_promo = 1
          AND p.ignore_promo_conflict = 0
          AND ( pt.rr_check_promo = 1
            OR p.RR_DISCOUNT_CHECK = 1)
          AND pdt.until_date >= TO_DATE(''' || min_date || ''', ''MM/DD/YYYY'')
          AND pdt.from_date <= TO_DATE(''' || max_date || ''', ''MM/DD/YYYY'')
          GROUP BY pd.promotion_id';

   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_promo_dates_t noparallel');		-- cmos 23-Jan-2017

   dynamic_ddl('TRUNCATE TABLE t_ep_promotion_conflicts');

   FOR rec IN (SELECT DISTINCT l10.group_id1, l2.group_id2
               FROM t_ep_l_att_10 l10, t_ep_l_att_2 l2, location l, conflict_groups_t cgt
               WHERE l10.group_id1 <> 0
               AND l2.group_id2 <> 0
               AND l10.group_id1 = cgt.group_id1
              AND l2.group_id2 = cgt.group_id2
               AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
               AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
   LOOP

      v_group_id1 := '(-1';
      FOR temp_1 IN (SELECT DISTINCT l10.t_ep_l_att_10_ep_id
                     FROM location l, t_ep_l_att_10 l10, t_ep_l_att_2 l2
                     WHERE l10.group_id1 = rec.group_id1
                     AND l2.group_id2 = rec.group_id2
                     AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
                     AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
      LOOP

          v_group_id1 := v_group_id1 || ', ' || temp_1.t_ep_l_att_10_ep_id;

      END LOOP;
      v_group_id1 := v_group_id1 || ')';

      v_group_id2 := '(-1';
      FOR temp_2 IN (SELECT DISTINCT l2.t_ep_l_att_2_ep_id
                     FROM location l, t_ep_l_att_10 l10, t_ep_l_att_2 l2
                     WHERE l10.group_id1 = rec.group_id1
                     AND l2.group_id2 = rec.group_id2
                     AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
                     AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
      LOOP

          v_group_id2 := v_group_id2 || ', ' || temp_2.t_ep_l_att_2_ep_id;

      END LOOP;
      v_group_id2 := v_group_id2 || ')';

      v_unique_count := '';
      v_unique_sel := '';
      v_from_clause := '';
      v_where_pct := '';
      v_where_lvl := '';
      v_unique_count_pct := '';


      FOR rec1 IN (SELECT group_table_id FROM conflict_groups_t WHERE group_id1 = rec.group_id1 AND group_id2 = rec.group_id2)
      LOOP

          SELECT gtable, id_field INTO v_gtable, v_gt_id FROM group_tables WHERE group_table_id = rec1.group_table_id;

          v_unique_count := v_unique_count || ', ' || v_gtable || '.' || v_gt_id || '_l ' || v_gt_id || '_l';
          v_unique_sel := v_unique_sel || ' ' || v_gtable || '.' || v_gt_id ;
          v_from_clause := v_from_clause || ',' || v_gtable || ' ' || v_gtable;
          v_where_pct := v_where_pct || ' AND pct.' || v_gt_id || '_l = ' || v_gtable || '.' || v_gt_id ;
          v_where_lvl := v_where_lvl || ' AND ' || v_gtable || '.' || v_gt_id || ' = m.' || v_gt_id ;
          v_unique_count_pct := v_unique_count_pct || ', pct.' || v_gt_id || '_l ' || v_gt_id || '_l';

      END LOOP;

      v_unique_count := LTRIM(v_unique_count, ',');



      check_and_drop('promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2);

-- Only checking for Promotions of Type Catalogue that are conflicting--
      -- cmos 23-Jan-2017 (px ctas)
      sql_str :=
         'CREATE TABLE promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2 || ' parallel 32 nologging AS
          (SELECT /*+ full(pm) parallel(pm, 12) */ DISTINCT pm.promotion_id, pm.item_id, m.t_ep_l_att_10_ep_id, m.t_ep_l_att_2_ep_id, i.datet sales_date, ' || v_unique_count || ',
           CASE WHEN COUNT(DISTINCT ' || v_unique_sel || ') OVER (PARTITION BY pm.item_id, m.t_ep_l_att_10_ep_id, m.t_ep_l_att_2_ep_id, m.t_ep_L_ATT_6_ep_id, id.datet) > 1
           THEN 2 ELSE 1 END count_1, m.t_ep_L_ATT_6_ep_id t_ep_e1_cust_state_ep_id,m.T_EP_EBS_CUSTOMER_EP_ID
           FROM promotion p,
           promotion_matrix pm,
           mdp_matrix m,
           t_ep_ebs_customer cl4,
           rr_promo_dates_t pdt,
           promotion_type pt,
           promotion_stat ps,
           scenario s,
           inputs i,
           inputs_daily id,
           ' || v_gtable_name || ' lvl
           ' || v_from_clause || '
           WHERE pm.item_id = m.item_id
           AND pm.location_id = m.location_id
           AND pm.promotion_id = p.promotion_id
           AND p.promotion_type_id = pt.promotion_type_id
           AND p.promotion_id = pm.promotion_id
           AND p.scenario_id = s.scenario_id
           AND m.t_ep_ebs_customer_ep_id  = cl4.t_ep_ebs_customer_ep_id
           AND (pt.rr_check_promo = 1
             or p.RR_DISCOUNT_CHECK = 1)
           AND ps.rr_check_promo = 1
           AND s.rr_check_promo = 1
           AND cl4.rr_check_promo = 1
           AND p.ignore_promo_conflict = 0
           AND pdt.promotion_id = p.promotion_id
           AND m.t_ep_l_att_10_ep_id IN  ' || v_group_id1 || '
           AND m.t_ep_l_att_2_ep_id IN ' || v_group_id2 || '
           AND  m.' || v_id_field || ' =  lvl.' || v_id_field || '
           AND pdt.until_date - pdt.from_date < ( ' || v_promo_week_range || ' * 7) + 1
           ' || v_where_lvl || '
           AND lvl.rr_promo_overlap <> 0
           AND p.promotion_stat_id = ps.promotion_stat_id
           AND pm.is_self = 1
           AND i.datet BETWEEN pdt.from_date AND pdt.until_date
           AND id.datet BETWEEN pdt.from_date + DECODE(MOD((pdt.until_date - pdt.from_date) + 1, 7), 0, lvl.rr_promo_offset_start, 0) AND pdt.until_date + DECODE(MOD((pdt.until_date - pdt.from_date) + 1, 7), 0, lvl.rr_promo_offset_start, 0)
           AND i.datet BETWEEN TO_DATE(''' || min_date || ''', ''MM/DD/YYYY'') AND TO_DATE(''' || max_date || ''', ''MM/DD/YYYY'')
           )';
    rr_pkg_proc_log.prc_proc_sql_log(1,sql_str);

      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);
      dynamic_ddl('alter table promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2 || '  noparallel');		-- cmos 23-Jan-2017
      COMMIT;

      dynamic_ddl(' TRUNCATE TABLE cg_members_t');

      check_and_drop('cg_members_t_' || rec.group_id1 || '_' || rec.group_id2);
      dynamic_ddl('CREATE TABLE cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || ' AS SELECT * FROM cg_members_t WHERE 1 = 2');

      sql_str :=
         'INSERT /*+ APPEND NOLOGGING */ INTO cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || '
          (SELECT /*+ full(pm) parallel(pm, 12) */ DISTINCT pm.promotion_id, pm.item_id, pm.location_id, pct.sales_date, pdt.from_date, pdt.until_date, lvl.rr_promo_overlap, lvl.rr_promo_offset_start
           ' || v_unique_count_pct || ', pct.t_ep_e1_cust_state_ep_id,pct.T_EP_EBS_CUSTOMER_EP_ID
           FROM promotion p,
           promotion_matrix pm,
           rr_promo_dates_t pdt,
           inputs i,
           mdp_matrix m,
           t_ep_ebs_customer cl4,
           promotion_type pt,
           promotion_stat ps,
           scenario s,
           ' || v_gtable_name || ' lvl,
           promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2 || ' pct
           ' || v_from_clause || '
           WHERE pm.promotion_id = p.promotion_id
           AND pct.promotion_id = p.promotion_id
           AND pm.item_id = m.item_id
           AND pm.location_id = m.location_id
           AND p.ignore_promo_conflict = 0
           AND p.promotion_type_id = pt.promotion_type_id
           AND (pt.rr_check_promo = 1
             or p.RR_DISCOUNT_CHECK = 1)
           AND p.promotion_stat_id = ps.promotion_stat_id
           AND ps.rr_check_promo = 1
           AND p.scenario_id = s.scenario_id
           AND s.rr_check_promo = 1
           AND m.t_ep_ebs_customer_ep_id  = cl4.t_ep_ebs_customer_ep_id
           AND cl4.rr_check_promo = 1
           AND pdt.promotion_id = p.promotion_id
           AND pdt.until_date - pdt.from_date < (' || v_promo_week_range || ' * 7) + 1
           AND m.t_ep_l_att_10_ep_id IN  ' || v_group_id1 || '
           AND m.t_ep_l_att_2_ep_id IN ' || v_group_id2 || '
           AND  m.' || v_id_field || ' =  lvl.' || v_id_field || '
           AND lvl.rr_promo_overlap <> 0
           AND m.t_ep_l_att_10_ep_id = pct.t_ep_l_att_10_ep_id
           AND m.t_ep_l_att_2_ep_id = pct.t_ep_l_att_2_ep_id
           AND pm.is_self = 1
           ' || v_where_pct || '
           AND m.item_id = pct.item_id
           AND i.datet = pct.sales_date
           AND ((pct.count_1 > 1))
           )';

    rr_pkg_proc_log.prc_proc_sql_log(1,sql_str);
      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);
      COMMIT;

      dynamic_ddl('INSERT /*+ APPEND NOLOGGING */  INTO  cg_members_t (SELECT * FROM cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || ')');

      COMMIT;

      sql_str :=
         'INSERT /*+ APPEND NOLOGGING */ INTO t_ep_promotion_conflicts(promotion_id, conflicting_promo_id)
          (SELECT /*+ full(m1) parallel(m1, 12) full(m2) parallel(m2, 12) */ DISTINCT m1.promotion_id, m2.promotion_id
          FROM cg_members_t m1,
          cg_members_t m2
          WHERE m1.promotion_id <> m2.promotion_id
          AND m1.item_id = m2.item_id
          AND (m1.level1 = m2.level1 AND m1.t_ep_e1_cust_state_ep_id = m2.t_ep_e1_cust_state_ep_id and m1.T_EP_EBS_CUSTOMER_EP_ID <> m2.T_EP_EBS_CUSTOMER_EP_ID)
          -- AND m1.location_id = m2.location_id
          AND (m1.sales_date + m1.rr_promo_offset_start BETWEEN m2.sales_date + m2.rr_promo_offset_start AND m2.sales_date + (m2.rr_promo_offset_start + 6)
                              OR
                              m1.sales_date + (m1.rr_promo_offset_start + 6) BETWEEN m2.sales_date + m2.rr_promo_offset_start AND m2.sales_date + (m2.rr_promo_offset_start + 6)
                 )
          )
         ';

      dynamic_ddl (sql_str);
      COMMIT;

      MERGE /*+ FULL(p) PARALLEL(p, 32) */ INTO promotion p
      USING(SELECT /*+ full(m1) parallel(m1, 12) */ DISTINCT promotion_id FROM cg_members_t m1
                        WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) p1
      ON(p.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, last_update_date = sysdate;

      COMMIT;

      MERGE /*+ FULL(pm) PARALLEL(pm, 32) */ INTO promotion_matrix pm
      USING(SELECT /*+ full(m1) parallel(m1, 12) */ DISTINCT promotion_id, item_id, location_id FROM cg_members_t
                        WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) pm1
      ON(pm.promotion_id = pm1.promotion_id
      AND pm.item_id = pm1.item_id
      AND pm.location_id = pm1.location_id)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, last_update_date = sysdate;

      COMMIT;

      MERGE /*+ FULL(pd) PARALLEL(pd, 32) */ INTO promotion_data pd
      USING(SELECT /*+ full(m1) parallel(m1, 12) */ DISTINCT promotion_id, item_id, location_id, sales_date FROM cg_members_t
                        WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) pd1
      ON(pd.promotion_id = pd1.promotion_id
      AND pd.item_id = pd1.item_id
      AND pd.location_id = pd1.location_id
      AND pd.sales_date = pd1.sales_date)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, promo_conflict = 1, last_update_date = sysdate;

      COMMIT;

----------------------
--Setup Promo Conflict
----------------------

   --RBURTON 3/5/2011 Added Activity Type & Promo Price to Conflict Details --
   MERGE  /*+ full(s1) parallel(s1,16) */ INTO t_ep_promotion_conflicts s1
      USING (SELECT   /*+ full(s) parallel(s,16) full(pd) parallel(pd,16)  */
                      s.conflicting_promo_id, MIN(pt.promotion_type_desc) promotion_type_desc,
                      MIN(ec.ebs_customer_desc) account_name,
                      ROUND (AVG (pd.promo_price), 2) promo_price
                 FROM promotion_type pt,
                      t_ep_promotion_conflicts s,
                      promotion_data pd,
                      promotion p,
                      location l,
                      t_ep_ebs_customer ec
                WHERE s.conflicting_promo_id = pd.promotion_id
                  AND p.promotion_id = pd.promotion_id
                  AND p.promotion_id = s.conflicting_promo_id
                  AND p.promotion_type_id = pt.promotion_type_id
                  AND pd.location_id = l.location_id
                  AND l.t_ep_ebs_customer_ep_id = ec.t_ep_ebs_customer_ep_id
             GROUP BY s.conflicting_promo_id) p1
      ON (p1.conflicting_promo_id = s1.conflicting_promo_id)
      WHEN MATCHED THEN
         UPDATE
            SET s1.activity_type = p1.promotion_type_desc,
                s1.promo_price = p1.promo_price,
                s1.account_name = p1.account_name
         ;
   COMMIT;

   END LOOP;



     --Update Promotion with conflicting data --
      FOR rec IN (SELECT          /*+parallel(c,12) */
                         DISTINCT promotion_id, conflicting_promo_id,
                                  activity_type,
                                  account_name,
                                  LTRIM (TO_CHAR (promo_price, '99.99')
                                        ) promo_price,
                                  RANK() OVER (PARTITION BY promotion_id ORDER BY conflicting_promo_id) rank
                             FROM t_ep_promotion_conflicts c)
      LOOP
         UPDATE promotion p
            SET conflict_01 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 1;

         UPDATE promotion p
            SET conflict_02 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 2;

         UPDATE promotion p
            SET conflict_03 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 3;

         UPDATE promotion p
            SET conflict_04 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 4;

         UPDATE promotion p
            SET conflict_05 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 5;

         UPDATE promotion p
            SET conflict_06 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 6;

         UPDATE promotion p
            SET conflict_07 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 7;

         UPDATE promotion p
            SET conflict_08 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 8;

         UPDATE promotion p
            SET conflict_09 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 9;

         UPDATE promotion p
            SET conflict_10 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
                   , last_update_date = sysdate
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 10;

      END LOOP;

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_update_promo
  IS
/******************************************************************************
   NAME:       PRC_UPDATE_PROMO
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   v_prog_name    VARCHAR2 (100) := 'PRC_UPDATE_PROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


--   MERGE /*+ FULL(pd) PARALLEL(pd, 16) */  INTO promotion_data pd
--   USING(SELECT /*+ FULL(pd) PARALLEL(pd, 16) */ pd.promotion_id, MIN(pd.activity_type_id) activity_type_id
--         FROM promotion_data pd,
--              promotion p
--         WHERE pd.activity_type_id = 13
--           AND p.scenario_id IN (22, 262, 162)
--         GROUP BY pd.promotion_id) pd1
--   ON(pd.promotion_id = pd1.promotion_id)
--   WHEN MATCHED THEN
--   UPDATE SET pd.activity_type_id = 13
--   WHERE pd.activity_type_id <> 13;

--   COMMIT;

--   MERGE /*+ FULL(pd) PARALLEL(pd, 16) */  INTO promotion_data pd
--   USING(SELECT /*+ FULL(pd) PARALLEL(pd, 16) */  pd.promotion_id, MIN(pd.activity_type_id) promotion_type_id
--         FROM promotion_data pd,
--              promotion p
--         WHERE pd.activity_type_id <> 0
--         AND pd.promotion_id = p.promotion_id
--         AND p.scenario_id IN (22, 262, 162)
--         AND p.promotion_stat_id NOT IN (0, 8)
--         GROUP BY pd.promotion_id) pd1
--   ON(pd.promotion_id = pd1.promotion_id)
--   WHEN MATCHED THEN
--   UPDATE SET pd.promotion_type_id = pd1.promotion_type_id
--   WHERE pd.promotion_type_id <> pd1.promotion_type_id;

--   COMMIT;

--   MERGE /*+ FULL(pm) PARALLEL(pm, 16) */ INTO promotion_matrix pm
--   USING(SELECT /*+ FULL(pd) PARALLEL(pd, 16) */  pd.promotion_id, MIN(pd.activity_type_id) promotion_type_id
--         FROM promotion_data pd,
--              promotion p
--         WHERE pd.activity_type_id <> 0
--         AND pd.promotion_id = p.promotion_id
--         AND p.scenario_id IN (22, 262, 162)
--         AND p.promotion_stat_id NOT IN (0, 8)
--         GROUP BY pd.promotion_id) pm1
--   ON(pm.promotion_id = pm1.promotion_id)
--   WHEN MATCHED THEN
--   UPDATE SET pm.promotion_type_id = pm1.promotion_type_id
--   WHERE pm.promotion_type_id <> pm1.promotion_type_id;

--   COMMIT;

--   MERGE /*+ FULL(p) PARALLEL(p, 16) */  INTO promotion p
--   USING(SELECT /*+ FULL(pd) PARALLEL(pd, 16) */  pd.promotion_id, MIN(pd.activity_type_id) promotion_type_id
--         FROM promotion_data pd,
--              promotion p
--         WHERE pd.activity_type_id <> 0
--         AND p.scenario_id IN (22, 262, 162)
--         AND pd.promotion_id = p.promotion_id
--         AND p.promotion_stat_id NOT IN (0, 8)
--         GROUP BY pd.promotion_id) p1
--   ON(p.promotion_id = p1.promotion_id)
--   WHEN MATCHED THEN
--   UPDATE SET p.promotion_type_id = p1.promotion_type_id
--   WHERE p.promotion_type_id <> p1.promotion_type_id;

--   COMMIT;

--   MERGE INTO promotion p
--   USING(SELECT p.promotion_id
--         FROM promotion p
--         WHERE p.promotion_type_id <> 13
--         AND NVL(p.approval, 0) = 0
--         AND p.scenario_id IN (22, 262, 162)
--         AND p.promotion_stat_id BETWEEN 4 AND 7
--         ) p1
--   ON(p.promotion_id = p1.promotion_id)
--   WHEN MATCHED THEN
--   UPDATE SET p.approval = 1;

--   COMMIT;

   ---Update Promotion Description if Start Date has changed and Remove NEW---

    MERGE INTO promotion p1
      USING (SELECT          /*+ parallel(p,24) */
                    DISTINCT p.promotion_id,
                                TO_CHAR (from_date,
                                         'yyyy-mm-dd'
                                        )
                             || ' : '
                             || p.promotion_id AS promo_desc
                        FROM promotion_dates pt, promotion p
                       WHERE p.promotion_id = pt.promotion_id
                         AND    TO_CHAR (from_date, 'yyyy-mm-dd')
                             || ' : '
                             || p.promotion_id IS NOT NULL
                         AND p.scenario_id IN (22, 262, 162)
                         AND TO_CHAR (p.promotion_desc) LIKE '%NEW') p2
      ON (p2.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET p1.promotion_code = p2.promotion_id,
                p1.promotion_desc = p2.promo_desc, last_update_date = SYSDATE
         ;
   COMMIT;

      ------Set Commited Date
   UPDATE promotion p
      SET promo_commit_date = SYSDATE
    WHERE promo_commit_date IS NULL
    AND promotion_stat_id IN (4, 5, 6, 7)
    AND scenario_id IN (22, 262, 162);

  COMMIT;

  ---------Ensure Sales not in future rows-----------------
   UPDATE promotion_data
      SET evt_vol_act = NULL,
          incr_evt_vol_act = NULL
    WHERE (evt_vol_act IS NOT NULL OR incr_evt_vol_act IS NOT NULL)
      AND sales_date > SYSDATE
      AND scenario_id IN (22, 262, 162);

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_out_of_strategy
  IS
/******************************************************************************
   NAME:       PRC_OUT_OF_STRATEGY
   PURPOSE:    Procedure to check promotion out of strategy

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        24/09/2012  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_no_weeks       NUMBER := 90;

   v_prog_name    VARCHAR2 (100) := 'PRC_OUT_OF_STRATEGY';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN


   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OUT_OF_STRATEGY';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS'),
                   'MM/DD/YYYY'
                  )
     INTO max_sales_date
     FROM DUAL;

   SELECT TO_CHAR (  TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                 --  - (v_no_weeks * 7)
                   ,'MM/DD/YYYY'
                  )
     INTO min_date
     FROM DUAL;

-- Set max check parameter for 8 weeks into future
   SELECT TO_CHAR (  TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                   + (v_no_weeks * 7),
                   'MM/DD/YYYY'
                  )
     INTO max_date
     FROM DUAL;

   dynamic_ddl ('TRUNCATE TABLE rr_out_of_strat_t');
   dynamic_ddl ('TRUNCATE TABLE rr_out_of_strat_combs_t');
   dynamic_ddl ('TRUNCATE TABLE rr_out_of_strat_pd_t');

   UPDATE promotion
      SET t_ep_out_of_strategy_id = 0
    WHERE t_ep_out_of_strategy_id = 1
      AND scenario_id IN (22, 262, 162)
      AND promotion_id IN (
             SELECT promotion_id
               FROM promotion_dates
              WHERE (until_date BETWEEN min_date AND max_date)
                 OR (from_date BETWEEN min_date AND max_date));

   UPDATE promotion_matrix
      SET t_ep_out_of_strategy_id = 0
    WHERE t_ep_out_of_strategy_id = 1
      AND scenario_id IN (22, 262, 162)
      AND promotion_id IN (
             SELECT promotion_id
               FROM promotion_dates
              WHERE (until_date BETWEEN min_date AND max_date)
                 OR (from_date BETWEEN min_date AND max_date));

   UPDATE promotion_data
      SET t_ep_out_of_strategy_id = 0
    WHERE t_ep_out_of_strategy_id = 1
      AND scenario_id IN (22, 262, 162)
      AND promotion_id IN (
             SELECT promotion_id
               FROM promotion_dates
              WHERE (until_date BETWEEN min_date AND max_date)
                 OR (from_date BETWEEN min_date AND max_date));

   --  Guardrail Flagging -- Identify the item ,customer, date where guardrails are violated
   INSERT /*+ APPEND NOLOGGING */ INTO rr_out_of_strat_combs_t
      SELECT  pd.item_id, pd.location_id, pd.sales_date
                 FROM promotion_data pd, promotion_dates pdt, promotion_type pt, mdp_matrix mdp, promotion p
                WHERE 1 = 1
                  AND pd.promotion_id = p.promotion_id
                  AND pdt.promotion_id = p.promotion_id
                  AND p.scenario_id IN (22, 262, 162)
                  AND pd.item_id = mdp.item_id
                  AND pd.location_id = mdp.location_id
                  AND (   (pdt.until_date BETWEEN min_date AND max_date)
                       OR (pdt.from_date BETWEEN min_date AND max_date)
                      )
                  AND pd.promotion_type_id = pt.promotion_type_id
                  AND pt.rr_guard_rail_activity IN (1, 2)
                  GROUP BY pd.item_id, pd.location_id, pd.sales_date
                  HAVING
                      (CASE WHEN max(pt.rr_guard_rail_activity) = 1 AND max(NVL(mdp.rr_stan_pp, 0)) <> 0 AND max(NVL(pd.promo_price, 0)) < max(NVL(mdp.rr_stan_pp, 0)) THEN 1
                            WHEN max(pt.rr_guard_rail_activity) = 2 AND max(NVL(mdp.rr_advert_pp, 0)) <> 0 AND max(NVL(pd.promo_price, 0)) < max(NVL(mdp.rr_advert_pp, 0)) THEN 1
                            ELSE 0
                       END = 1
                       OR
                       CASE WHEN max(pt.rr_guard_rail_activity) = 1 AND max(NVL(mdp.rr_gr_rrp, 0)) <> 0 AND max(NVL(pd.ed_price, 0)) < max(NVL(mdp.rr_gr_rrp, 0)) THEN 1
                            WHEN max(pt.rr_guard_rail_activity) = 2 AND max(NVL(mdp.rr_gr_rrp, 0)) <> 0 AND max(NVL(pd.ed_price, 0)) < max(NVL(mdp.rr_gr_rrp, 0)) THEN 1
                            ELSE 0
                       END = 1
                       OR
                       CASE WHEN max(pt.rr_guard_rail_activity) = 1
                            AND max(NVL (mdp.rr_stan_max, 0)) <> 0
                            AND sum(NVL(pd.case_buydown, 0)) > max(NVL (mdp.rr_stan_max, 0) * nvl(pd.units,1))
                            THEN 1
                            WHEN max(pt.rr_guard_rail_activity) = 2
                            AND max(NVL (mdp.rr_advert_max, 0)) <> 0
                            AND sum(NVL(pd.case_buydown, 0)) > max(NVL (mdp.rr_advert_max, 0) * nvl(pd.units,1))
                            THEN 1
                            ELSE 0
                       END = 1
                       );


   COMMIT;

  --  Guardrail Flagging -- Identify all promotions for item, customer, date combinations where guardrails are violated
  INSERT /*+ APPEND NOLOGGING */ INTO rr_out_of_strat_t
      SELECT DISTINCT pd.promotion_id
                 FROM promotion_data pd, promotion_dates pdt, rr_out_of_strat_combs_t osc, promotion p
                WHERE 1 = 1
                  AND pd.promotion_id = p.promotion_id
                  AND pdt.promotion_id = p.promotion_id
                  AND p.scenario_id IN (22, 262, 162)
                 AND osc.item_id = pd.item_id
                  AND osc.location_id = pd.location_id
                  AND osc.sales_date = pd.sales_date
                  AND (   (pdt.until_date BETWEEN min_date AND max_date)
                       OR (pdt.from_date BETWEEN min_date AND max_date)
                      )
                  AND pd.promotion_id > 0;

   COMMIT;

   INSERT /*+ APPEND NOLOGGING */ INTO rr_out_of_strat_pd_t
      SELECT DISTINCT pd.promotion_id, pd.item_id, pd.location_id, pd.sales_date
                 FROM promotion_data pd, promotion_dates pdt, rr_out_of_strat_combs_t osc, promotion p
                WHERE 1 = 1
                  AND pd.promotion_id = p.promotion_id
                  AND pdt.promotion_id = p.promotion_id
                  AND p.scenario_id IN (22, 262, 162)
                  AND osc.item_id = pd.item_id
                  AND osc.location_id = pd.location_id
                  AND osc.sales_date = pd.sales_date
                  AND (   (pdt.until_date BETWEEN min_date AND max_date)
                       OR (pdt.from_date BETWEEN min_date AND max_date)
                      )
                  AND pd.promotion_id > 0;

   COMMIT;



   MERGE INTO promotion p
      USING (SELECT DISTINCT t.promotion_id
               FROM rr_out_of_strat_t t) p1
      ON (p.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1
         ;

   MERGE INTO promotion_matrix pm
      USING (SELECT DISTINCT t.promotion_id
               FROM rr_out_of_strat_t t) pm1
      ON (pm.promotion_id = pm1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1
         ;

   COMMIT;

   MERGE /*+ INDEX(pd, PROMOTION_DATA_11_PK) */ INTO promotion_data pd
      USING (SELECT /*+ FULL(pd) PARALLEL(pd, 32) */   pd.promotion_id, pd.item_id, pd.location_id, pd.sales_date
               FROM rr_out_of_strat_pd_t pd
            ) pd1
      ON (pd.promotion_id = pd1.promotion_id
          AND pd.item_id = pd1.item_id
          AND pd.location_id = pd1.location_id
          AND pd.sales_date = pd1.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1
         ;

         commit;


   ------Check for Promotions for GCSC %
      dynamic_ddl ('TRUNCATE TABLE RR_OUT_OF_STRAT_GCSC');

   ---Aggregate EVT GCSC and  EVT NSV
    insert into RR_OUT_OF_STRAT_GCSC
               SELECT branch_data.promotion_id, branch_data.location_id,branch_data.item_id,branch_data.sales_date, mdp.rr_gr_evt_gcsc,
               sum(
                  (((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (NVL(branch_data.cust_list_price_p, 0)  )) )
                  - ((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (NVL(branch_data.case_buydown, 0) + (NVL(branch_data.units, 0) * NVL(branch_data.rr_handling_d, 0)) ))+ NVL(branch_data.event_cost, 0) + NVL(branch_data.rr_handling_coop, 0) )
                  - ((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (nvl(branch_data.ttl_base_bus_dev_spnd_a, 0)
+
((nvl(branch_data.frieght_deduction_p , 0)  +nvl(branch_data.bbds_p , 0) + nvl(branch_data.rebate_p , 0) + nvl(branch_data.bnps_p , 0))
   * (nvl(branch_data.cust_list_price_p, 0)
   - greatest(nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.tot_efficiencies_p, 0), nvl(branch_data.price_adjust_a, 0))
   - nvl(branch_data.base_non_per_spnd_a, 0)
   - (nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.market_price_p, 0))
   - (nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.case_buydownoi, 0))
   - nvl(branch_data.case_buydown, 0)
))
+
nvl(branch_data.ttl_promo_trde_spnd_a, 0)
+
nvl(branch_data.total_trad_a, 0)
+ nvl(branch_data.franch_reimb_p,0))) ))
                   - ((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (NVL(branch_data.cogs_p, 0)  )) )
                  ) over (partition by p.promotion_id,branch_data.sales_date,t_ep_p2b_EP_ID)  evt_gcsc,
                  sum(--evt nsv
                  (((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (NVL(branch_data.cust_list_price_p, 0)  )) )
                  - ((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (NVL(branch_data.case_buydown, 0) + (NVL(branch_data.units, 0) * NVL(branch_data.rr_handling_d, 0)) ))+ NVL(branch_data.event_cost, 0) + NVL(branch_data.rr_handling_coop, 0) )
                  - ((((CASE WHEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) <> 0 THEN NVL(branch_data.evt_vol_act, 0) + NVL(branch_data.incr_evt_vol_act, 0) ELSE NVL(branch_data.volume_base_ttl, 0) +  NVL(branch_data.incr_evt_vol_or, NVL(branch_data.rr_accrual_eng_incr_commit, NVL(branch_data.fore_5_uplift, NVL( branch_data.fore_0_uplift,0)))) END * NVL(branch_data.rr_redemp_p, 1)) + NVL(branch_data.rr_ms_vol, 0)) * (nvl(branch_data.ttl_base_bus_dev_spnd_a, 0)
+
((nvl(branch_data.frieght_deduction_p , 0)  +nvl(branch_data.bbds_p , 0) + nvl(branch_data.rebate_p , 0) + nvl(branch_data.bnps_p , 0))
   * (nvl(branch_data.cust_list_price_p, 0)
   - greatest(nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.tot_efficiencies_p, 0), nvl(branch_data.price_adjust_a, 0))
   - nvl(branch_data.base_non_per_spnd_a, 0)
   - (nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.market_price_p, 0))
   - (nvl(branch_data.cust_list_price_p, 0) * nvl(branch_data.case_buydownoi, 0))
   - nvl(branch_data.case_buydown, 0)
))
+
nvl(branch_data.ttl_promo_trde_spnd_a, 0)
+
nvl(branch_data.total_trad_a, 0)
+ nvl(branch_data.franch_reimb_p,0))) ))
                  )over (partition by p.promotion_id,branch_data.sales_date,t_ep_p2b_EP_ID)  evt_nsv
                 FROM promotion_data branch_data, promotion_dates pdt,promotion_type pt, mdp_matrix mdp, promotion p
                WHERE 1 = 1
                  AND branch_data.promotion_id = p.promotion_id
                  AND pdt.promotion_id = p.promotion_id
                  AND p.scenario_id IN (22, 262, 162)
                  and p.t_ep_out_of_strategy_id = 0 ----Only Check for promos not flagged.
                  AND branch_data.item_id = mdp.item_id
                  AND branch_data.location_id = mdp.location_id
                  AND branch_data.promotion_type_id = pt.promotion_type_id
                  AND pt.rr_guard_rail_activity IN (1, 2)
                  AND (   (pdt.until_date BETWEEN min_date AND max_date)
                       OR (pdt.from_date BETWEEN min_date AND max_date)
                      )

                       ;


   dynamic_ddl ('TRUNCATE TABLE rr_out_of_strat_t');

   ---Set where outside of Guardrails
               insert into rr_out_of_strat_t
               SELECT promotion_id
                 FROM RR_OUT_OF_STRAT_GCSC
                WHERE 1 = 1
                  and (rr_gr_evt_gcsc) <> 0
                  and (rr_gr_evt_gcsc) > (case when evt_gcsc = 0 or evt_nsv = 0 then 0 else   evt_gcsc / evt_nsv end) ---Evt GCSC %
                     ;

   COMMIT;

    MERGE INTO promotion p
      USING (SELECT DISTINCT t.promotion_id
               FROM rr_out_of_strat_t t) p1
      ON (p.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1, last_update_date = sysdate
         ;

   MERGE INTO promotion_matrix pm
      USING (SELECT DISTINCT t.promotion_id
               FROM rr_out_of_strat_t t) pm1
      ON (pm.promotion_id = pm1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1, last_update_date = sysdate
         ;

   COMMIT;

   MERGE /*+ INDEX(pd, PROMOTION_DATA_11_PK) */ INTO promotion_data pd
      USING (SELECT DISTINCT t.promotion_id
               FROM rr_out_of_strat_t t
            ) pd1
      ON (pd.promotion_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET t_ep_out_of_strategy_id = 1, last_update_date = sysdate
         ;

   commit;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
           RAISE;
         END;
  END;

  PROCEDURE prc_roll_shelf_price
  IS
/******************************************************************************
   NAME:       PRC_ROLL_SHELF_PRICE
   PURPOSE:    Procedure to check promotion out of strategy

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        24/09/2012  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_no_weeks       NUMBER := 52;

   v_prog_name    VARCHAR2 (100) := 'PRC_ROLL_SHELF_PRICE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

  BEGIN


   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_ROLL_SHELF_PRICE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS'),
                   'MM/DD/YYYY'
                  )
     INTO max_sales_date
     FROM DUAL;

   SELECT TO_CHAR (  TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                   - (v_no_weeks * 7),
                   'MM/DD/YYYY'
                  )
     INTO min_date
     FROM DUAL;

   max_date := max_sales_date;

   check_and_drop('tbl_shelf_price_roll');

   -- cmos 23-Jan-2017 (px ctas)
   dynamic_ddl('CREATE TABLE tbl_shelf_price_roll parallel 32 nologging AS
                SELECT /*+ FULL(sd) PARALLEL(sd, 32) */  sd.item_id, sd.location_id,
                CASE WHEN SUM(ABS(NVL(sd.sdata5, 0)) + ABS(NVL(sd.sdata6, 0))) <> 0
                    THEN MAX(CASE WHEN ABS(NVL(sd.sdata5, 0)) + ABS(NVL(sd.sdata6, 0)) <> 0 THEN sd.sales_date ELSE CAST(NULL AS DATE) END)
                     ELSE MAX(sd.sales_date)
                END sales_date
                FROM sales_data sd,
               mdp_matrix mdp
                WHERE sd.item_id = mdp.item_id
                AND sd.location_id = mdp.location_id
                AND mdp.dem_stream = 1
                AND NVL(sd.shelf_price_sd, 0) <> 0
                AND sd.sales_date BETWEEN TO_DATE(''' || min_date || ''', ''MM/DD/YYYY'')
                                      AND TO_DATE(''' || max_date || ''', ''MM/DD/YYYY'')
                GROUP BY sd.item_id, sd.location_id');
   dynamic_ddl('alter table tbl_shelf_price_roll noparallel');		-- cmos 23-Jan-2017

   check_and_drop('tbl_shelf_price');

   -- cmos 23-Jan-2017 (px ctas)
   dynamic_ddl('CREATE TABLE tbl_shelf_price parallel 32 nologging AS
                SELECT /*+ FULL(sd) PARALLEL(sd, 32) */  sd.item_id, sd.location_id, sd.sales_date, sd.shelf_price_sd
                FROM sales_data sd,
                tbl_shelf_price_roll sd1
                WHERE sd.item_id = sd1.item_id
                AND sd.location_id = sd1.location_id
                AND sd.sales_date = sd1.sales_date ');
   dynamic_ddl('alter table tbl_shelf_price noparallel');		-- cmos 23-Jan-2017

   dynamic_ddl('MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
                USING(SELECT /*+ FULL(sd) PARALLEL(sd, 32) */ sd.item_id, sd.location_id, sd.sales_date, t.shelf_price_sd
                      FROM tbl_shelf_price t,
                      sales_data sd
                      WHERE t.item_id = sd.item_id
                      AND t.location_id = sd.location_id
                      AND t.sales_date < sd.sales_date
                      AND NVL(t.shelf_price_sd, 0) <>  NVL(sd.shelf_price_sd, 0)) sd1
                ON(sd.item_id = sd1.item_id
                AND sd.location_id = sd1.location_id
                AND sd.sales_date = sd1.sales_date)
                WHEN MATCHED THEN
                UPDATE SET sd.shelf_price_sd = sd1.shelf_price_sd
                 ');
   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_correct_pd_vol_base(p_start_date DATE,
                                    p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_CORRECT_PD
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_CORRECT_PD_VOL_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CORRECT_PD_VOL_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE tbl_promo_correct_pd parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 32) */
               p.promotion_id
               FROM promotion p,
               promotion_dates pdt
               WHERE pdt.promotion_id = p.promotion_id
               AND p.scenario_id <> 265
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

   dynamic_ddl(sql_str);
   dynamic_ddl('alter table tbl_promo_correct_pd noparallel');		-- cmos 23-Jan-2017

   sql_str := ('MERGE  INTO promotion_data pd
      USING (SELECT /*+ parallel(pd,24) parallel(p, 8)  */
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
             CASE WHEN pd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD'), v_day) - 7, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END
                  ELSE pd.volume_base_ttl
             END volume_base_ttl,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE 0
             END evt_vol_act,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN 0
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE 0
             END incr_evt_vol_act,
             i.e1_ship_uom_mult units
             FROM sales_data sd,
             promotion_data pd,
             promotion_type pt,
             promotion_stat ps,
             promotion_matrix pm,
             tbl_promo_correct_pd p,
             t_ep_l_att_6 latt6,
             mdp_matrix mdp,
             t_ep_item i
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND i.t_ep_item_ep_id = mdp.t_ep_item_ep_id
             AND mdp.item_id = sd.item_id
             AND mdp.location_id = sd.location_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id
             AND pm.promotion_id = p.promotion_id
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             AND pd.is_self = 1
             AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             AND (NVL(CASE WHEN pd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD'), v_day) - 7, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END
                  ELSE pd.volume_base_ttl
             END, 0) <> NVL(pd.volume_base_ttl, 0)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                 ELSE 0
             END, 0) <> NVL(pd.evt_vol_act, 0)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN 0
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE 0
             END, 0) <> NVL(pd.incr_evt_vol_act, 0)
             OR NVL(i.e1_ship_uom_mult, 0) <> NVL(pd.units, 0)
             )
             ) pd1
      ON (pd1.promotion_id = pd.promotion_id
          AND pd1.item_id = pd.item_id
          AND pd1.location_id = pd.location_id
          AND pd1.sales_date = pd.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd.volume_base_ttl = pd1.volume_base_ttl,
                pd.evt_vol_act = pd1.evt_vol_act,
                pd.incr_evt_vol_act = pd1.incr_evt_vol_act,
                pd.units = pd1.units
                ');

   dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

   PROCEDURE prc_correct_pd(p_start_date DATE,
                            p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_CORRECT_PD
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_CORRECT_PD';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CORRECT_PD';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;

   check_and_drop('tbl_promo_correct_pd');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE tbl_promo_correct_pd parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 32) */
               p.promotion_id
               FROM promotion p,
               promotion_dates pdt
               WHERE pdt.promotion_id = p.promotion_id
               AND p.scenario_id <> 265
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

   dynamic_ddl(sql_str);
   dynamic_ddl('alter table tbl_promo_correct_pd noparallel');		-- cmos 23-Jan-2017

   check_and_drop('tbl_promo_correct_vals');

   --             sd.ebspricelist128 list_price_p,
--             sd.ebspricelist123 cust_list_price_p,
--             sd.ebspricelist100 tot_efficiencies_p,
--             sd.ebspricelist124 market_price_p,
--             sd.ebspricelist101 frieght_deduction_p,
--             sd.ebspricelist125 bnps_p,
--             sd.bdf_base_rate_corp bbds_p,
--             sd.bdf_fixed_funds_corp rebate_p,
--             sd.ebspricelist126 cogs_p,
--             sd.ebspricelist102 distribution_p,
--             sd.ebspricelist127 spoils_p,
--             sd.ebspricelist129 def_efficiencies_rate_p,
--             sd.rev_diff bbds_deferred_p,
--             sd.ebs_sh_ship_qty_rd list_sales_a,
--             sd.ebs_sh_req_qty_rd tot_efficiencies_a,
--             sd.sdata12 price_adjust_a,
--             sd.sdata10 gross_sales_a,
--             sd.sdata11 base_non_per_spnd_a,
--             sd.ebs_bh_req_qty_rd ttl_base_bus_dev_spnd_a,
--             sd.ebs_sh_ship_qty_sd ttl_promo_trde_spnd_a,
--             sd.sdata13 total_trad_a,
--             sd.ebs_bh_book_qty_rd net_sales_a,
--             sd.ebs_bh_req_qty_bd cogs_a,
--             sd.ebs_bh_book_qty_bd tot_distribution_a,
--             sd.sdata14 spoils_a,
--             sd.sdata4 non_work_co_mark_a,
--             sd.sdata9 cust_contribution_a,
--             i.e1_ship_uom_mult units,
--             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price,
--             sd.ff_2 rr_roi_effeciencies,
--             sd.ff_4 rr_roi_rebate

--             OR NVL(sd.ebspricelist128, 0) <> NVL(pd.list_price_p, 0)
--             OR NVL(sd.ebspricelist123, 0) <> NVL(pd.cust_list_price_p, 0)
--             OR NVL(sd.ebspricelist100, 0) <> NVL(pd.tot_efficiencies_p, 0)
--             OR NVL(sd.ebspricelist124, 0) <> NVL(pd.market_price_p, 0)
--             OR NVL(sd.ebspricelist101, 0) <> NVL(pd.frieght_deduction_p, 0)
--             OR NVL(sd.ebspricelist125, 0) <> NVL(pd.bnps_p, 0)
--             OR NVL(sd.bdf_base_rate_corp, 0) <> NVL(pd.bbds_p, 0)
--             OR NVL(sd.bdf_fixed_funds_corp, 0) <> NVL(pd.rebate_p, 0)
--             OR NVL(sd.ebspricelist126, 0) <> NVL(pd.cogs_p, 0)
--             OR NVL(sd.ebspricelist102, 0) <> NVL(pd.distribution_p, 0)
--             OR NVL(sd.ebspricelist127, 0) <> NVL(pd.spoils_p, 0)
--             OR NVL(sd.ebspricelist129, 0) <> NVL(pd.def_efficiencies_rate_p, 0)
--             OR NVL(sd.rev_diff, 0) <> NVL(pd.bbds_deferred_p, 0)
--             OR NVL(sd.ebs_sh_ship_qty_rd, 0) <> NVL(pd.list_sales_a, 0)
--             OR NVL(sd.ebs_sh_req_qty_rd, 0) <> NVL(pd.tot_efficiencies_a, 0)
--             OR NVL(sd.sdata12, 0) <> NVL(pd.price_adjust_a, 0)
--             OR NVL(sd.sdata10, 0) <> NVL(pd.gross_sales_a, 0)
--             OR NVL(sd.sdata11, 0) <> NVL(pd.base_non_per_spnd_a, 0)
--             OR NVL(sd.ebs_bh_req_qty_rd, 0) <> NVL(pd.ttl_base_bus_dev_spnd_a, 0)
--             OR NVL(sd.ebs_sh_ship_qty_sd, 0) <> NVL(pd.ttl_promo_trde_spnd_a, 0)
--             OR NVL(sd.sdata13, 0) <> NVL(pd.total_trad_a, 0)
--             OR NVL(sd.ebs_bh_book_qty_rd, 0) <> NVL(pd.net_sales_a, 0)
--             OR NVL(sd.ebs_bh_req_qty_bd, 0) <> NVL(pd.cogs_a, 0)
--             OR NVL(sd.ebs_bh_book_qty_bd, 0) <> NVL(pd.tot_distribution_a, 0)
--             OR NVL(sd.sdata14, 0) <> NVL(pd.spoils_a, 0)
--             OR NVL(sd.sdata4, 0) <> NVL(pd.non_work_co_mark_a, 0)
--             OR NVL(sd.sdata9, 0) <> NVL(pd.cust_contribution_a, 0)
--             OR NVL(i.e1_ship_uom_mult, 0) <> NVL(pd.units, 0)
--             OR NVL(NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)), 0) <> NVL(pd.ed_price, 0)
--             OR NVL(sd.ff_2, 0) <> NVL(pd.rr_roi_effeciencies, 0)
--             OR NVL(sd.ff_4, 0) <> NVL(pd.rr_roi_rebate, 0))

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE tbl_promo_correct_vals parallel 32 nologging AS
               SELECT /*+ PARALLEL(sd, 16) PARALLEL(pd, 8) */
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
              DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN 1 = 1 OR sd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                                                + CASE
                                                                      WHEN mdp.allocation_wk6 > 0 THEN 42
                                                                      WHEN mdp.allocation_wk5 > 0 THEN 35
                                                                      WHEN mdp.allocation_wk4 > 0 THEN 28
                                                                      WHEN mdp.allocation_wk3 > 0 THEN 21
                                                                      WHEN mdp.allocation_wk2 > 0 THEN 14
                                                                      WHEN mdp.allocation_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END
                  ELSE 0
             END) volume_base_ttl,
             DECODE( pt.rr_ms_exfact_pos , 0 , 0,
              1, CASE WHEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0)  <> 0 THEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0)
                 ELSE (NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || fore_column || ', sd.npi_forecast))) * (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)) END,
              2, CASE WHEN NVL(sd.actual_quantity, 0) <> 0 THEN NVL(sd.actual_quantity, 0)
                  ELSE DECODE(NVL(sd.demand_type, 0),1,(NVL(sd.exfact_base,0)* (nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)),
                  NVL(sd.unexfact_or,(NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || fore_column || ', sd.npi_forecast)))*
                  (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)))) END ) vol,
             DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END) evt_vol_act,
             DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END) incr_evt_vol_act,
          ---   CASE WHEN pd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE + 1, 'DD'), v_day) - 14, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
          ---                                                      + CASE
          ---                                                            WHEN pm.rr_alloc_wk6 > 0 THEN 42
          ---                                                            WHEN pm.rr_alloc_wk5 > 0 THEN 35
          ---                                                            WHEN pm.rr_alloc_wk4 > 0 THEN 28
          ---                                                            WHEN pm.rr_alloc_wk3 > 0 THEN 21
          ---                                                            WHEN pm.rr_alloc_wk2 > 0 THEN 14
          ---                                                            WHEN pm.rr_alloc_wk1 > 0 THEN 7
          ---                                                            ELSE 0
          ---                                                         END
          ---        THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1
          ---              THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
          ---              ELSE 0
          ---              END
          ---        ELSE pd.volume_base_ttl
          ---   END volume_base_ttl,
          ---   CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
          ---        THEN sd.actual_quantity
          ---        WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
          ---        THEN sd.sdata5
          ---        ELSE NULL
          ---   END evt_vol_act,
          ---   CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
          ---        THEN NULL
          ---        WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
          ---        THEN sd.sdata6
          ---       ELSE NULL
          ---   END incr_evt_vol_act,
             sd.ebspricelist109 list_price_p,
             sd.ebspricelist110 cust_list_price_p, --war_price_p,
             sd.ebspricelist111 tot_efficiencies_p, --trad_allowance_p,
             sd.ebspricelist112 price_adjust_a, --trad_allowance_amt,
             sd.ebspricelist113 base_non_per_spnd_a, --temp_price_adj_amt,
             sd.ebspricelist114 market_price_p, --tier_disc_p,
             sd.ebspricelist117 ttl_base_bus_dev_spnd_a, --trad_terms_fix_amt,
             sd.ebspricelist118 frieght_deduction_p, --trad_terms_fix_perc,
             sd.ebspricelist120 bnps_p, --reb_scale_only_p,
             sd.ebspricelist122 ttl_promo_trde_spnd_a, --trad_terms_var_amt,
             sd.ff_input1 bbds_p , --trad_terms_var_p,
             sd.cust_input3 total_trad_a,  --trade_promo_amt,
             sd.mkt_acc3 rebate_p, --trade_promo_p,
             sd.mkt_acc1 franch_reimb_p, --franch_reimb_p,
             sd.ebspricelist126 cogs_p, -- cogs_p,
             i.e1_ship_uom_mult units, --units
             i.e1_whgt_uom_mult litres, --litres
             DECODE( pt.rr_margin_support , 1 , 0, sd.cust_input1)  ms_cd_not_ms_promo,  --- ms cd for non ms promo
             DECODE( pt.rr_margin_support , 1 , 0, sd.reg_raw_order)  ms_vol_not_ms_promo --- ms volume for non ms promo
             FROM tbl_promo_correct_pd p,
             promotion_matrix pm,
             promotion_data pd,
             sales_data sd,
             promotion_type pt,
             promotion_stat ps,
             mdp_matrix mdp,
             t_ep_l_att_6 latt6,
             t_ep_item i
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.item_id = pd.item_id
             AND mdp.location_id = pd.location_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id
             --AND pm.scenario_id <> 265
             AND pd.promotion_id = p.promotion_id
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             AND pd.is_self = 1
             AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             AND (NVL(CASE WHEN pd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE + 1, 'DD'), v_day) - 14, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END
                  ELSE pd.volume_base_ttl
             END, 0) <> NVL(pd.volume_base_ttl, 0)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END, -99899899899) <> NVL(pd.evt_vol_act, -99899899899)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END, -99899899899) <> NVL(pd.incr_evt_vol_act, -99899899899)
             OR NVL(sd.ebspricelist109, 0) <> NVL(pd.list_price_p, 0)
             OR NVL(sd.ebspricelist110, 0) <> NVL(pd.cust_list_price_p,0) --war_price_p,
             OR NVL(sd.ebspricelist111, 0) <> NVL(pd.tot_efficiencies_p, 0) --trad_allowance_p,
             OR NVL(sd.ebspricelist112, 0) <> NVL(pd.price_adjust_a, 0) --trad_allowance_amt,
             OR NVL(sd.ebspricelist113, 0) <> NVL(pd.base_non_per_spnd_a, 0) --temp_price_adj_amt,
             OR NVL(sd.ebspricelist114, 0) <> NVL(pd.market_price_p, 0) --tier_disc_p,
             OR NVL(sd.ebspricelist117, 0) <> NVL(pd.ttl_base_bus_dev_spnd_a, 0) --trad_terms_fix_amt,
             OR NVL(sd.ebspricelist118, 0) <> NVL(pd.frieght_deduction_p, 0) --trad_terms_fix_perc,
             OR NVL(sd.ebspricelist120, 0) <> NVL(pd.bnps_p, 0) --reb_scale_only_p,
             OR NVL(sd.ebspricelist122, 0) <> NVL(pd.ttl_promo_trde_spnd_a, 0) --trad_terms_var_amt,
             OR NVL(sd.ff_input1, 0) <> NVL(pd.bbds_p , 0) --trad_terms_var_p,
             OR NVL(sd.cust_input3, 0) <> NVL(pd.total_trad_a, 0)  --trade_promo_amt,
             OR NVL(sd.mkt_acc3, 0) <> NVL(pd.rebate_p, 0) --trade_promo_p,
             OR NVL(sd.mkt_acc1, 0) <> NVL(pd.franch_reimb_p,0) --franch_reimb
             OR NVL(sd.ebspricelist126, 0) <> NVL(pd.cogs_p,0) --cogs_p
             OR NVL(i.e1_whgt_uom_mult,0) <> NVL(pd.units,0) --units
             OR NVL(i.e1_whgt_uom_mult,0) <> NVL(pd.litres,0)
             OR NVL(sd.cust_input1, 0) <> NVL(pd.ms_cd_not_ms_promo,0)
             OR NVL(sd.reg_raw_order, 0) <> NVL(pd.ms_vol_not_ms_promo,0) )';

   dbms_output.put_line(sql_str);
   dynamic_ddl(sql_str);
   dynamic_ddl('alter table tbl_promo_correct_vals noparallel');		-- cmos 23-Jan-2017

   sql_str := ('MERGE /*+ INDEX(pd, promotion_data_11_pk) */ INTO promotion_data pd
      USING (SELECT /*+ FULL(p) PARALLEL(p, 32) */ * FROM tbl_promo_correct_vals p
             ) pd1
      ON (pd1.promotion_id = pd.promotion_id
          AND pd1.item_id = pd.item_id
          AND pd1.location_id = pd.location_id
          AND pd1.sales_date = pd.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd.volume_base_ttl = pd1.volume_base_ttl,
                pd.evt_vol_act = pd1.evt_vol_act,
                pd.incr_evt_vol_act = pd1.incr_evt_vol_act,
                pd.list_price_p = pd1.list_price_p,
                pd.cust_list_price_p = pd1.cust_list_price_p,
                pd.tot_efficiencies_p = pd1.tot_efficiencies_p,
                pd.market_price_p = pd1.market_price_p,
                pd.frieght_deduction_p = pd1.frieght_deduction_p,
                pd.bnps_p = pd1.bnps_p,
                pd.bbds_p = pd1.bbds_p,
                pd.rebate_p = pd1.rebate_p,
                pd.cogs_p = pd1.cogs_p,
                pd.franch_reimb_p = pd1.franch_reimb_p,
--                pd.distribution_p = pd1.distribution_p,
--                pd.spoils_p = pd1.spoils_p,
--                pd.def_efficiencies_rate_p = pd1.def_efficiencies_rate_p,
--                pd.bbds_deferred_p = pd1.bbds_deferred_p,
--                pd.list_sales_a = pd1.list_sales_a,
--                pd.tot_efficiencies_a = pd1.tot_efficiencies_a,
                pd.price_adjust_a = pd1.price_adjust_a,
--                pd.gross_sales_a = pd1.gross_sales_a,
                pd.base_non_per_spnd_a = pd1.base_non_per_spnd_a,
                pd.ttl_base_bus_dev_spnd_a = pd1.ttl_base_bus_dev_spnd_a,
                pd.ttl_promo_trde_spnd_a = pd1.ttl_promo_trde_spnd_a,
                pd.units = pd1.units,
                pd.litres = pd1.litres,
                pd.total_trad_a = pd1.total_trad_a,
                pd.ms_cd_not_ms_promo = pd1.ms_cd_not_ms_promo,
                 pd.ms_vol_not_ms_promo = pd1.ms_vol_not_ms_promo
--                pd.net_sales_a = pd1.net_sales_a,
--                pd.cogs_a = pd1.cogs_a,
--                pd.tot_distribution_a = pd1.tot_distribution_a,
--                pd.spoils_a = pd1.spoils_a,
--                pd.non_work_co_mark_a = pd1.non_work_co_mark_a,
--                pd.cust_contribution_a = pd1.cust_contribution_a,

--                pd.ed_price = decode(nvl(pd1.ed_price,0),0,pd.ed_price, pd1.ed_price),
--                pd.rr_roi_effeciencies = pd1.rr_roi_effeciencies,
--                pd.rr_roi_rebate = pd1.rr_roi_rebate
                ');

   dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_correct_pd_pl_actuals(p_start_date DATE,
                                      p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_CORRECT_PD_PL_ACTUALS
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
  start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_CORRECT_PD_PL_ACTUALS';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CORRECT_PD_PL_ACTUALS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;

   sql_str := ('MERGE /*+ parallel(pd,24) */ INTO promotion_data pd
      USING (SELECT /*+ parallel(pd,24) parallel(sd,24) */
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
             sd.ebspricelist128 list_price_p,
             sd.ebspricelist123 cust_list_price_p,
             sd.ebspricelist100 tot_efficiencies_p,
             sd.ebspricelist124 market_price_p,
             sd.ebspricelist101 frieght_deduction_p,
             sd.ebspricelist125 bnps_p,
             sd.bdf_base_rate_corp bbds_p,
             sd.bdf_fixed_funds_corp rebate_p,
             sd.ebspricelist126 cogs_p,
             sd.ebspricelist102 distribution_p,
             sd.ebspricelist127 spoils_p,
             sd.ebspricelist129 def_efficiencies_rate_p,
             sd.ebs_sh_ship_qty_rd list_sales_a,
             sd.ebs_sh_req_qty_rd tot_efficiencies_a,
             sd.sdata12 price_adjust_a,
             sd.sdata10 gross_sales_a,
             sd.sdata11 base_non_per_spnd_a,
             sd.ebs_bh_req_qty_rd ttl_base_bus_dev_spnd_a,
             sd.ebs_sh_ship_qty_sd ttl_promo_trde_spnd_a,
             sd.sdata13 total_trad_a,
             sd.ebs_bh_book_qty_rd net_sales_a,
             sd.ebs_bh_req_qty_bd cogs_a,
             sd.ebs_bh_book_qty_bd tot_distribution_a,
             sd.sdata14 spoils_a,
             sd.sdata4 non_work_co_mark_a,
             sd.sdata9 cust_contribution_a
             FROM sales_data sd,
             promotion_data pd,
             promotion_type pt,
             promotion_stat ps,
             promotion_matrix pm,
             t_ep_l_att_6 latt6,
             mdp_matrix mdp,
             t_ep_item i
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.item_id = sd.item_id
             AND mdp.location_id = sd.location_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id
             AND pm.scenario_id <> 265
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             AND pd.is_self = 1
             AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             AND (NVL(sd.ebspricelist128, 0) <> NVL(pd.list_price_p, 0)
             OR NVL(sd.ebspricelist123, 0) <> NVL(pd.cust_list_price_p, 0)
             OR NVL(sd.ebspricelist100, 0) <> NVL(pd.tot_efficiencies_p, 0)
             OR NVL(sd.ebspricelist124, 0) <> NVL(pd.market_price_p, 0)
             OR NVL(sd.ebspricelist101, 0) <> NVL(pd.frieght_deduction_p, 0)
             OR NVL(sd.ebspricelist125, 0) <> NVL(pd.bnps_p, 0)
             OR NVL(sd.bdf_base_rate_corp, 0) <> NVL(pd.bbds_p, 0)
             OR NVL(sd.bdf_fixed_funds_corp, 0) <> NVL(pd.rebate_p, 0)
             OR NVL(sd.ebspricelist126, 0) <> NVL(pd.cogs_p, 0)
             OR NVL(sd.ebspricelist102, 0) <> NVL(pd.distribution_p, 0)
             OR NVL(sd.ebspricelist127, 0) <> NVL(pd.spoils_p, 0)
             OR NVL(sd.ebspricelist129, 0) <> NVL(pd.def_efficiencies_rate_p, 0)
             OR NVL(sd.ebs_sh_ship_qty_rd, 0) <> NVL(pd.list_sales_a, 0)
             OR NVL(sd.ebs_sh_req_qty_rd, 0) <> NVL(pd.tot_efficiencies_a, 0)
             OR NVL(sd.sdata12, 0) <> NVL(pd.price_adjust_a, 0)
             OR NVL(sd.sdata10, 0) <> NVL(pd.gross_sales_a, 0)
             OR NVL(sd.sdata11, 0) <> NVL(pd.base_non_per_spnd_a, 0)
             OR NVL(sd.ebs_bh_req_qty_rd, 0) <> NVL(pd.ttl_base_bus_dev_spnd_a, 0)
             OR NVL(sd.ebs_sh_ship_qty_sd, 0) <> NVL(pd.ttl_promo_trde_spnd_a, 0)
             OR NVL(sd.sdata13, 0) <> NVL(pd.total_trad_a, 0)
             OR NVL(sd.ebs_bh_book_qty_rd, 0) <> NVL(pd.net_sales_a, 0)
             OR NVL(sd.ebs_bh_req_qty_bd, 0) <> NVL(pd.cogs_a, 0)
             OR NVL(sd.ebs_bh_book_qty_bd, 0) <> NVL(pd.tot_distribution_a, 0)
             OR NVL(sd.sdata14, 0) <> NVL(pd.spoils_a, 0)
             OR NVL(sd.sdata4, 0) <> NVL(pd.non_work_co_mark_a, 0)
             OR NVL(sd.sdata9, 0) <> NVL(pd.cust_contribution_a, 0))
             ) pd1
      ON (pd1.promotion_id = pd.promotion_id
          AND pd1.item_id = pd.item_id
          AND pd1.location_id = pd.location_id
          AND pd1.sales_date = pd.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd.list_price_p = pd1.list_price_p,
                pd.cust_list_price_p = pd1.cust_list_price_p,
                pd.tot_efficiencies_p = pd1.tot_efficiencies_p,
                pd.market_price_p = pd1.market_price_p,
                pd.frieght_deduction_p = pd1.frieght_deduction_p,
                pd.bnps_p = pd1.bnps_p,
                pd.bbds_p = pd1.bbds_p,
                pd.rebate_p = pd1.rebate_p,
                pd.cogs_p = pd1.cogs_p,
                pd.distribution_p = pd1.distribution_p,
                pd.spoils_p = pd1.spoils_p,
                pd.def_efficiencies_rate_p = pd1.def_efficiencies_rate_p,
                pd.list_sales_a = pd1.list_sales_a,
                pd.tot_efficiencies_a = pd1.tot_efficiencies_a,
                pd.price_adjust_a = pd1.price_adjust_a,
                pd.gross_sales_a = pd1.gross_sales_a,
                pd.base_non_per_spnd_a = pd1.base_non_per_spnd_a,
                pd.ttl_base_bus_dev_spnd_a = pd1.ttl_base_bus_dev_spnd_a,
                pd.ttl_promo_trde_spnd_a = pd1.ttl_promo_trde_spnd_a,
                pd.total_trad_a = pd1.total_trad_a,
                pd.net_sales_a = pd1.net_sales_a,
                pd.cogs_a = pd1.cogs_a,
                pd.tot_distribution_a = pd1.tot_distribution_a,
                pd.spoils_a = pd1.spoils_a,
                pd.non_work_co_mark_a = pd1.non_work_co_mark_a,
                pd.cust_contribution_a = pd1.cust_contribution_a
                ');

   dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  procedure PRC_POP_EXFACT_BASE(P_START_DATE date,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
  sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_POP_EXFACT_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_POP_EXFACT_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_POP_EXFACT_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



 check_and_drop('rr_exfact_pop_temp');

 sql_str := 'create table rr_exfact_pop_temp as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             sd.sales_date,
             sd.sales_date  plan_date
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
          --   AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                AND NVL(sd.exfact_base, 0)  <> 0)sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id
             AND sd.location_id = sd1.location_id
             AND sd.plan_date = sd1.sales_date
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                              ';



--   dynamic_ddl(sql_str);

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_exfact_pop_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
          )
      WHEN MATCHED THEN
         UPDATE
         SET  exfact_base = 0       ';

--   dynamic_ddl(sql_str);
    UPDATE /*+ FULL(sd) PARALLEL(sd, 24) */ SALES_DATA SD
  SET SD.EXFACT_BASE = 0
  WHERE nvl(sd.exfact_base, 0) <> 0
  and sd.sales_date between  start_date  and end_date;

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_pop_apo_fcst(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_apo_fcst
   PURPOSE:    Temp procedure to un offset the apo forecast values received

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        26/11/2015  Lakshmi Annapragada / UXC

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   FORE_COLUMN   varchar2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_POP_APO_FCST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   dynamic_ddl('merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, mdp_matrix m
      where  SD.SALES_DATE BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - 6*7
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + 6*7
        AND M.DEM_STREAM = 1
        and m.item_id = sd.item_id
        and m.location_id = sd.location_id
        AND nvl(SD.MANUAL_STAT,0) <> 0) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   when matched then update set
   S.manual_stat = 0,
   s.last_update_date = sysdate');

   commit;

   v_status := 'Start2';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_apo_fcst_pre_temp');

-- cmos 23-Jan-2017 (px ctas)
sql_str := 'CREATE TABLE rr_apo_fcst_pre_temp parallel 32 nologging as SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             (next_day(sd.sales_Date,''MONDAY'')-7 + NVL(mdp.scan_forecast_offset, 0) * 7)  plan_date,
             sum(NVL(sd.exfact_base,0)) manual_stat
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND NVL(sd.exfact_base,0) <> 0
            -- AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY((sd.sales_date + (NVL(mdp.scan_forecast_offset, 0) * 7)), ''MONDAY'') - 7
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                                group by  sd.item_id,
             sd.location_id,( next_day(sd.sales_Date,''MONDAY'')-7+ NVL(mdp.scan_forecast_offset, 0) * 7)
                                ';
  dynamic_ddl(sql_str);
  dynamic_ddl('alter table rr_apo_fcst_pre_temp noparallel');		-- cmos 23-Jan-2017

   v_status := 'Start2.1';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_apo_fcst_pop_temp');

sql_str := 'create table rr_apo_fcst_pop_temp as   SELECT /*+ full(sd) parallel(sd) index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date,  sd.manual_stat
              FROM rr_apo_fcst_pre_temp sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.manual_stat, 0) - NVL(sd1.manual_stat, 0)) <> 0';




    --dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'Start2.2';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            T.ITEM_ID,T.LOCATION_ID,I.DATET plan_date,T.MANUAL_STAT*(I.NUM_OF_DAYS/7) MANUAL_STAT_SPLIT
            FROM RR_APO_FCST_PRE_TEMP T,INPUTS I
            WHERE T.PLAN_DATE = NEXT_DAY(I.DATET,''MONDAY'')-7
            ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET sd.manual_stat = sd1.MANUAL_STAT_SPLIT
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, manual_stat)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.MANUAL_STAT_SPLIT)';


   dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'Start2.3';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             sd.sales_Date  ,
             NVL(sd.exfact_base,0) manual_stat
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 0
             and NVL(sd.exfact_base,0) <> nvl(sd.manual_stat, 0)
            -- AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY((sd.sales_date + (NVL(mdp.scan_forecast_offset, 0) * 7)), ''MONDAY'') - 7
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')

            ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.sales_date = sd.sales_date
          )
      WHEN MATCHED THEN
         UPDATE
         SET sd.manual_stat = sd1.MANUAL_STAT
     ';


  dynamic_ddl(sql_str);

    update /*+ FULL(pd) PARALLEL(sd, 64) */ SALES_DATA SD
  SET sd.manual_stat = sd.exfact_base
  where NVL(SD.EXFACT_BASE,0) <> NVL(SD.MANUAL_STAT, 0)
  and sd.sales_date between  p_start_date  and p_end_date;

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  end;

PROCEDURE prc_offset_scan(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, MDP_MATRIX M
      where SD.LOCATION_ID = M.LOCATION_ID
        and SD.ITEM_ID = M.ITEM_ID
        and sd.sales_date between start_date and  end_date
       and M.DEM_STREAM = 1
           and sd.ITEM_ID = 1115 and sd.LOCATION_ID in (54379,75518)
        and SD.act_part_ord is not null) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   when matched then update set
   S.act_part_ord = null,
   s.last_update_date = sysdate;

   commit;

   v_status := 'Start2';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_offset_scan_temp');

 -- cmos 23-Jan-2017 (px ctas)
 sql_str := 'create table rr_offset_scan_temp parallel 32 nologging as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date, sd.act_part_ord
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7)   plan_date,
             sum(NVL(sd.sdata5, 0)) act_part_ord
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               group by sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7) )sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.act_part_ord, 0) - NVL(sd1.act_part_ord, 0))  <> 0';



   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_offset_scan_temp noparallel');		-- cmos 23-Jan-2017

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_offset_scan_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET act_part_ord = sd1.act_part_ord
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, act_part_ord)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.act_part_ord)';

   dynamic_ddl(sql_str);

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;
  PROCEDURE prc_offset_dm_fcst(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_OFFSET_DM_FCST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OFFSET_DM_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_OFFSET_DM_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, MDP_MATRIX M
      where SD.LOCATION_ID = M.LOCATION_ID
        and SD.ITEM_ID = M.ITEM_ID
        AND SD.SALES_DATE BETWEEN START_DATE AND  END_DATE
    --   and M.DEM_STREAM = 0
        and (nvl(sd.engine_base,0) <> 0 or nvl(sd.sales_dif,0) <> 0) ) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   WHEN MATCHED THEN UPDATE SET
   S.engine_base = 0,s.sales_dif = 0,
   s.last_update_date = sysdate;

   commit;

   v_status := 'Start2';
   --v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_offset_fcst_temp');

 -- cmos 23-Jan-2017 (px ctas)
 sql_str := 'create table rr_offset_fcst_temp parallel 32 nologging as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date, sd.engine_base, sd.sales_dif
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7)  plan_date,
             sum(sd.'||fore_column||') engine_base,
             sum(sd.final_pos_fcst) sales_dif
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               group by sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7) )sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.engine_base, 0) - NVL(sd1.engine_base, 0))  +  ABS(NVL(sd.sales_dif, 0) - NVL(sd1.sales_dif, 0)) <> 0';



   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_offset_fcst_temp noparallel');		-- cmos 23-Jan-2017

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_offset_fcst_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET engine_base = sd1.engine_base,
         sales_dif = sd1.sales_dif
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, engine_base,sales_dif)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.engine_base,sd1.sales_dif)';

   dynamic_ddl(sql_str);

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  end;


  PROCEDURE prc_check_scan
  AS
/******************************************************************************
   NAME:       PRC_CHECK_SCAN
   PURPOSE:

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_prog_name   VARCHAR2 (100)   := 'PRC_CHECK_SCAN';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

   v_min_incr    NUMBER          := 0.2;
   v_min_scan    NUMBER          := 1000;
   mindate       DATE;
   maxdate       DATE;
   fore_column   VARCHAR2 (30);
   sql_str       VARCHAR2 (4000);

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CHECK_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   maxdate := NEXT_DAY(to_char(sysdate, 'mm/dd/yyyy'), 'monday') - 21;
   mindate := maxdate - 180;

   --For Testing Procedures --
      --mindate := '01/12/2009';
      --maxdate := '01/19/2009';
   SELECT fore_column_name
     INTO fore_column
     FROM forecast_history
    WHERE time_sig = (SELECT MAX (time_sig)
                        FROM forecast_history
                       WHERE status = 1);

   -- CHECK BIIO SOURCE SCAN DATA AGAINST LOADED SCAN IN SALES ---

   v_status := 'Start insert and analyze tbl_source_scan_err ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   dynamic_ddl ('truncate table tbl_source_scan_err');
   dynamic_ddl
      ('insert into /*+ APPEND NOLOGGING */ tbl_source_scan_err (product_code, customer_no,sales_date,sd_base,src_base,sd_incr,src_incr)
           select /*+ parallel(sd,16) parallel(m,16) parallel(s,16) */ i.dm_item_desc, e.dm_site_desc,sd.sales_date,sum(sd.sdata5),sum(s.demand_base),sum(sd.sdata6), sum(s.actuals_incr)
           from biio_scan_monthly s, sales_data sd, mdp_matrix m, t_ep_item i, t_ep_site e
             where sd.sales_Date = s.sDate
             and sd.item_id = m.item_id
             and sd.location_id = m.location_id
             and s.level1 = i.DM_ITEM_DESC
             and s.level2 = e.site
             and m.t_ep_item_ep_id = i.t_ep_item_ep_id
             and m.t_ep_site_ep_id = e.t_ep_site_ep_id
             and safe_division ( (nvl(s.demand_base,0) + nvl(s.actuals_incr,0)) - (nvl(sd.sdata5,0) + nvl(sd.sdata6,0) ) , nvl(sd.sdata5,0) + nvl(sd.sdata6,0) , 0) > 0.15
             group by i.dm_item_desc, e.dm_site_desc,sd.sales_date
             order by 1 asc, 2 asc, 3 asc, 4 asc'
      );
   dynamic_ddl ('analyze table tbl_source_scan_err compute statistics');

   v_status := 'End insert and analyze tbl_source_scan_err ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   check_and_drop ('TBL_SCAN_BASELINES');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := ('
            CREATE TABLE /*+ nologging */ tbl_scan_baselines tablespace TS_SOURCE parallel 32 nologging
            AS
            SELECT /*+ parallel(sd,8) parallel(m,8) */ p2b_desc as Promotion_Group, ebs_account_desc as Account_State,AVG(sdata5) avg_scan,AVG('|| fore_column ||') avg_fcst,
            avg(sdata6) avg_incr, AVG(NVL(sdata5,0)+NVL(sdata6,0)) avg_ttl_scan, MIN(NVL(sdata5,0)+NVL(sdata6,0)) min_ttl_scan, MAX(NVL(sdata5,0)+NVL(sdata6,0)) max_ttl_scan
            FROM sales_data pd, t_ep_p2b p, t_ep_ebs_account c, mdp_matrix m
            WHERE pd.item_id = m.item_id
            AND pd.location_id = m.location_id
            AND m.t_ep_p2b_ep_id = p.t_ep_p2b_ep_id
            AND m.t_ep_ebs_account_ep_id = c.t_ep_ebs_account_ep_id
            AND pd.sales_date BETWEEN '''|| mindate || ''' AND ''' || maxdate || '''
            AND pd.sdata5 > 0
            GROUP BY p2b_desc, ebs_account_desc ');

   dynamic_ddl(sql_str);
   dynamic_ddl('alter table tbl_scan_baselines noparallel');		-- cmos 23-Jan-2017
   commit;

   dynamic_ddl('CREATE UNIQUE INDEX TBL_SCAN_BASELINES_X ON TBL_SCAN_BASELINES
                (PROMOTION_GROUP, ACCOUNT_STATE)
                NOLOGGING
                TABLESPACE TS_SOURCE_X');

   check_and_drop ('TBL_MISSING_PROMOS');
   -- cmos 23-Jan-2017 (px ctas)
   sql_str :=
      (   'CREATE TABLE tbl_missing_promos parallel 32 nologging AS
            SELECT /*+ parallel(sd,16) parallel(m,16)  */ r.l_att_10_desc as country, e.ebs_account_desc as Account_State, p.p2b_desc Promo_Group, sd.sales_date, SUM(sdata5) base,SUM(sdata6) incr,
            ABS(1 - safe_division(SUM(sdata5),(SUM(NVL(sdata5,'||fore_column||' )+sdata6)),0)) AS diff,
            avg(safe_division(sd.shelf_price_sd-sd.C_COST, sd.shelf_price_sd,0)) avg_perc_discount
            FROM sales_data sd, t_ep_ebs_account e, t_ep_p2b p, mdp_matrix m, t_ep_item i, t_ep_l_att_10 r
            WHERE sd.item_id = m.item_id
            AND sd.location_id = m.location_id
            AND m.t_ep_ebs_account_ep_id = e.t_ep_ebs_account_ep_id
            AND m.t_ep_p2b_ep_id = p.t_ep_p2b_ep_id
            AND m.t_ep_item_ep_id = i.t_ep_item_ep_id
            AND m.t_ep_l_Att_10_ep_id = r.t_ep_l_att_10_ep_id
            AND sd.sdata6 > 0
            AND sd.C_COST > 0 -- RB Added to only create promotions where a definate discount has occured
            AND sd.shelf_price_sd > sd.C_COST --RB Added to further constrain validity of promotional activity
            AND m.do_fore = 1
            AND m.prediction_status = 1
            and m.t_ep_L_ATT_8_ep_id = 1
            and m.t_ep_I_ATT_5_ep_id = 4
            AND ABS(1 - safe_division(sdata5,(NVL(sdata5,'||fore_column||')+sdata6),0)) > '
       || v_min_incr
       || '
            AND (m.t_ep_p2b_ep_id,m.t_ep_ebs_account_ep_id,sd.sales_date) NOT IN (SELECT DISTINCT
            md.t_ep_p2b_ep_id, md.t_ep_ebs_account_ep_id, pd.sales_date
            FROM promotion_data pd, mdp_matrix md
            WHERE pd.activity_id in (12,4,5,9,6,15,17)
              and md.location_id = pd.location_id
              and md.item_id = pd.item_id)
            AND sd.sales_date BETWEEN '''|| mindate || ''' AND ''' || maxdate || '''
            GROUP BY r.l_att_10_desc, e.ebs_account_desc, p.p2b_desc,sd.sales_date'
      );
   dynamic_ddl (sql_str);
   dynamic_ddl('alter table tbl_missing_promos noparallel');		-- cmos 23-Jan-2017
   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_fix_promo_type
  AS
/******************************************************************************
   NAME:       PRC_FIX_PROMO_TYPE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_prog_name   VARCHAR2 (100)   := 'PRC_FIX_PROMO_TYPE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

   mindate       DATE;
   maxdate       DATE;

   sql_str       VARCHAR2 (32000);

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_FIX_PROMO_TYPE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   mindate := NEXT_DAY(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MONDAY') - 21;
   maxdate := mindate + 364;

   check_and_drop('rr_fix_promo_type_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_fix_promo_type_t parallel 32 nologging AS
               SELECT /*+ FULL(pd) PARALLEL(pd, 32) */  p.promotion_id, MAX(lock_cd_deal_type) lock_type,
               CASE WHEN MAX(pt.lock_cd_deal_type) = 0
                    THEN MIN(pd.activity_type_id)
                    ELSE MAX(CASE WHEN pt.lock_cd_deal_type = 1 THEN pd.activity_type_id ELSE 0 END)
               END promotion_type_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_data pd,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_id = pdt.promotion_id
               AND p.promotion_id = pd.promotion_id
               AND p.scenario_id <> 265
               AND pd.activity_type_id = pt.promotion_type_id
               AND pd.activity_type_id <> 0
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(maxdate, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(mindate, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               GROUP BY p.promotion_id';
   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_fix_promo_type_t noparallel');		-- cmos 23-Jan-2017

   sql_str := 'MERGE /*+ FULL(p) PARALLEL(p, 16) */   INTO promotion p
               USING(SELECT /*+ FULL(fix) PARALLEL(fix, 16) */  fix.promotion_id, fix.promotion_type_id, fix.lock_type,
                     pt.cd_deal_type_id cd_deal_type, pt.coop_deal_type_id coop_deal_type, pt.oi_deal_type_id oi_deal_type
                     FROM rr_fix_promo_type_t fix,
                     promotion_type pt
                     WHERE pt.promotion_type_id = fix.promotion_type_id
                     ) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET p.promotion_type_id = p1.promotion_type_id,
               p.cd_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.cd_deal_type ELSE p.cd_deal_type END,
               p.coop_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.coop_deal_type ELSE p.coop_deal_type END,
               p.oi_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.oi_deal_type ELSE p.oi_deal_type END
               WHERE p.promotion_type_id <> p1.promotion_type_id
               OR NVL(p.cd_deal_type, -99999) <> NVL(CASE WHEN p1.lock_type = 1 THEN p1.cd_deal_type ELSE p.cd_deal_type END, -99999)
               OR NVL(p.coop_deal_type, -99999) <> NVL(CASE WHEN p1.lock_type = 1 THEN p1.coop_deal_type ELSE p.coop_deal_type END, -99999)
               OR NVL(p.oi_deal_type, -99999) <> NVL(CASE WHEN p1.lock_type = 1 THEN p1.oi_deal_type ELSE p.oi_deal_type END, -99999)';

   dynamic_ddl(sql_str);
   COMMIT;

   sql_str := 'MERGE /*+ FULL(pm) PARALLEL(pm, 16) */  INTO promotion_matrix pm
               USING(SELECT /*+ FULL(fix) PARALLEL(fix, 16) */   promotion_id, promotion_type_id, lock_type
                     FROM rr_fix_promo_type_t fix) pm1
               ON(pm.promotion_id = pm1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET pm.promotion_type_id = pm1.promotion_type_id
               WHERE pm.promotion_type_id <> pm1.promotion_type_id';

   dynamic_ddl(sql_str);
   COMMIT;

   sql_str := 'MERGE /*+ FULL(pd) PARALLEL(pd, 16) */  INTO promotion_data pd
               USING(SELECT /*+ FULL(fix) PARALLEL(fix, 16) */  promotion_id, promotion_type_id, lock_type
                     FROM rr_fix_promo_type_t fix) pd1
               ON(pd.promotion_id = pd1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET pd.promotion_type_id = pd1.promotion_type_id,
               pd.activity_type_id = NVL(CASE WHEN pd1.lock_type = 1 THEN pd1.promotion_type_id ELSE pd.activity_type_id END, pd1.promotion_type_id)
               WHERE pd.promotion_type_id <> pd1.promotion_type_id
               OR pd.activity_type_id <> NVL(CASE WHEN pd1.lock_type = 1 THEN pd1.promotion_type_id ELSE pd.activity_type_id END, pd1.promotion_type_id) ';

   dynamic_ddl(sql_str);
   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

PROCEDURE prc_refresh_promo_cache
AS
   v_schema_id          INTEGER;
   vi_server            VARCHAR2 (100);
   vi_user              VARCHAR2 (100);
   vi_password          VARCHAR2 (100);
   vs_appserverurl      VARCHAR2 (200);
   vs_devappserverurl   VARCHAR2 (200);
   level_id             NUMBER (10);
   member_id            NUMBER (10);
   v_schema             VARCHAR2 (2000);
   v_server             VARCHAR2 (2000);
   v_user               VARCHAR2 (2000);
   v_password           VARCHAR2 (2000);
   servlet              VARCHAR2 (2000);
   buff                 VARCHAR2 (2000);

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;

BEGIN

   pre_logon;

    v_status := 'start ';
    v_prog_name := 'PRC_REFRESH_PROMO_CACHE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   get_param ('sys_params', 'AppServerURL', vs_appserverurl);
   get_param ('sys_params', 'DevAppServerURL', vs_devappserverurl);

   FOR rec IN (SELECT promotion_id
                          FROM promotion WHERE scenario_id <> 265)
   LOOP

      --dbms_output.put_line(v_server);
      level_id := 232;
      member_id := rec.promotion_id;
      vi_server := NVL (v_server, NVL (vs_appserverurl, vs_devappserverurl));
      vi_user := NVL (v_user, 'ptp');
      vi_password := NVL (v_password, 'ptp');
      servlet := 'portal/CustomRefreshMember.jsp?';
      servlet :=
              servlet || 'LevelId=' || level_id || '&memberId=' || member_id;
    --  dbex (vi_server || '/' || servlet, 'JS_REFRESH');


      UPDATE promotion_matrix SET promotion_lud = SYSDATE
      WHERE promotion_id = member_id;

      -- dbms_output.put_line(vi_server||'/'||servlet);
      SELECT UTL_HTTP.request (vi_server || '/' || servlet)
        INTO buff
        FROM DUAL;
      END LOOP;

      COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

END;

  PROCEDURE prc_refresh_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL)
  AS

  sql_str         VARCHAR2 (32000);
  v_id_field      VARCHAR2 (100);
  v_search_table  VARCHAR2(20);
  v_gtable  VARCHAR2(300);
  cnt             NUMBER;

  v_prog_name    VARCHAR2 (100) := 'PRC_REFRESH_PROMO';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;
  v_step         VARCHAR2(50);

  v_prod_cnt     NUMBER;
  v_pg_cnt       NUMBER;
  v_custno_cnt   NUMBER;
  v_kad_cnt      NUMBER;
  v_ka_cnt       NUMBER;
  v_comb_cnt     NUMBER;

  v_accruals_type_id        NUMBER;
  v_promotion_type_id       NUMBER;
  v_t_ep_out_of_strategy_id NUMBER;
  v_t_ep_promo_conflict_id  NUMBER;
  v_promotion_stat_id       NUMBER;
  v_promotion_guidelines_id NUMBER;
  v_scenario_id             NUMBER;

  p_user_id   NUMBER;
  p_member_id NUMBER;
  p_level_id  NUMBER;
  v_error     NUMBER;

  v_p_lvl_cnt NUMBER := 0;
  v_c_lvl_cnt NUMBER := 0;

  v_from_date   DATE;
  v_until_date  DATE;

  v_count       NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_REFRESH_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

   COMMIT;

   SELECT COUNT(1)
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id;

   IF v_count <> 0 THEN

   v_step := '1 ';
   dbex (v_step || SYSDATE, v_prog_name);

   p_user_id := user_id;
   p_member_id := member_id;
  p_level_id := level_id;

   SELECT from_date, until_date
   INTO v_from_date, v_until_date
   FROM promotion_dates
   WHERE promotion_id = p_member_id;

   SELECT COUNT(1)
   INTO v_p_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (12, 424);

   SELECT COUNT(1)
   INTO v_c_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (426, 490, 491);


   SELECT accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
          promotion_guidelines_id, scenario_id
      INTO v_accruals_type_id, v_promotion_type_id, v_t_ep_out_of_strategy_id, v_t_ep_promo_conflict_id, v_promotion_stat_id,
        v_promotion_guidelines_id, v_scenario_id
   FROM promotion
   WHERE promotion_id = p_member_id;

   check_and_drop('tmpcombs_' || p_user_id || '_' || p_member_id);

   dynamic_ddl('CREATE TABLE tmpcombs_' || p_user_id || '_' || p_member_id || '(promotion_id NUMBER, item_id NUMBER, location_id NUMBER,
                accruals_type_id NUMBER, promotion_type_id NUMBER, t_ep_out_of_strategy_id NUMBER,
                t_ep_promo_conflict_id NUMBER, promotion_stat_id NUMBER, promotion_guidelines_id NUMBER, scenario_id NUMBER)');

   sql_str :=
            'MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO tmpcombs_' || p_user_id || '_' || p_member_id || ' pm
                                      USING(SELECT '
         || p_member_id
         || ' promotion_id, m.item_id, m.location_id, '
         || v_accruals_type_id || ' accruals_type_id, '
         || v_promotion_type_id || ' promotion_type_id, '
         || v_t_ep_out_of_strategy_id || ' t_ep_out_of_strategy_id, '
         || v_t_ep_promo_conflict_id || ' t_ep_promo_conflict_id, '
         || v_promotion_stat_id || ' promotion_stat_id, '
         || v_promotion_guidelines_id  || ' promotion_guidelines_id, '
         || v_scenario_id || ' scenario_id
                            FROM mdp_matrix m
                            WHERE 1 = 1
                                                    ';

  FOR rec1 IN (SELECT DISTINCT level_id FROM
                   (SELECT DISTINCT usp.level_id
                              FROM user_security_population usp
                             WHERE usp.user_id = p_user_id
                             AND usp.permission_id IN (3, 4)
                   )
                   WHERE level_id IN (SELECT group_table_id
                                      FROM group_tables
                                      WHERE 1 = 1
                                      AND lower(search_table) IN ('items', 'location'))
                   )
   LOOP
         SELECT gtable, search_table, id_field
           INTO v_gtable, v_search_table, v_id_field
           FROM group_tables
          WHERE group_table_id = rec1.level_id
          AND lower(search_table) IN ('items', 'location');

         sql_str := sql_str || 'AND ' || CASE LOWER(v_search_table)
                                         WHEN 'items' THEN 'm'
                                         WHEN 'location' THEN 'm'
                                         END || '.' || v_id_field || ' IN (-1 ';

         cnt := 0;

         FOR rec2 IN (SELECT DISTINCT usp.member_id
                                 FROM user_security_population usp
                                WHERE usp.user_id = p_user_id
                                AND usp.permission_id IN (3, 4)
                                AND usp.level_id = rec1.level_id)
         LOOP
            sql_str := sql_str || ',' || rec2.member_id;
            cnt := cnt + 1;
         END LOOP;

    sql_str :=
            sql_str
            || ')
                  ';
      END LOOP;

      FOR rec1 IN (SELECT DISTINCT level_id FROM
                   (
                   SELECT DISTINCT pl.level_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id)
                   WHERE level_id IN (SELECT group_table_id
                                      FROM group_tables
                                      WHERE 1 = 1
                                      AND lower(search_table) IN ('items', 'location'))
                   )
   LOOP
         SELECT gtable, search_table, id_field
           INTO v_gtable, v_search_table, v_id_field
           FROM group_tables
          WHERE group_table_id = rec1.level_id
          AND lower(search_table) IN ('items', 'location');

         sql_str := sql_str || 'AND ' || CASE LOWER(v_search_table)
                                         WHEN 'items' THEN 'm'
                                         WHEN 'location' THEN 'm'
                                         END || '.' || v_id_field || ' IN (-1 ';

         cnt := 0;

         FOR rec2 IN (SELECT DISTINCT pl.level_id, pl.filter_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id AND pl.level_id = rec1.level_id)
        LOOP

         FOR rec3 IN (SELECT DISTINCT pm.member_id
                                 FROM promotion_members pm
                                WHERE pm.filter_id = rec2.filter_id)
         LOOP
            sql_str := sql_str || ',' || rec3.member_id;
            cnt := cnt + 1;
         END LOOP;

         END LOOP;

         sql_str :=
            sql_str
            || ')
                  ';
      END LOOP;


      sql_str :=
            sql_str
         || '
         AND NOT EXISTS (SELECT 1 FROM promotion_matrix pm2 WHERE pm2.promotion_id = ' || p_member_id ||
                        ' AND pm2.item_id = m.item_id AND pm2.location_id = m.location_id
                          ))pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
                             promotion_guidelines_id, scenario_id)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id, pm1.t_ep_promo_conflict_id,
                             pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)';

      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);

      --EXECUTE IMMEDIATE sql_str;
      COMMIT;

   cnt := 0;

   EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM tmpcombs_' || p_user_id || '_' || p_member_id
   INTO cnt;

   dynamic_ddl('INSERT INTO promotion_matrix(promotion_id, item_id, location_id,
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id, from_date, until_date)
                SELECT x.promotion_id, x.item_id, x.location_id,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id,
                TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),
                TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id');

   COMMIT;

   dynamic_ddl('INSERT INTO promotion_data(promotion_id, item_id, location_id, sales_date,
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, activity_type_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id)
                SELECT x.promotion_id, x.item_id, x.location_id, i.datet sales_date,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id, x.promotion_type_id ,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m,
                inputs i
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                AND i.datet BETWEEN NEXT_DAY(TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7
                AND NEXT_DAY(TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7');

   COMMIT;


   END IF;

<<end_proc>>
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;


   PROCEDURE prc_refresh_past_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL)
  AS

  sql_str         VARCHAR2 (32000);
  v_id_field      VARCHAR2 (100);
  v_search_table  VARCHAR2(20);
  v_gtable  VARCHAR2(300);
  cnt             NUMBER;

  v_prog_name    VARCHAR2 (100) := 'PRC_REFRESH_PAST_PROMO';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;
  v_step         VARCHAR2(50);

  v_prod_cnt     NUMBER := 120;
  v_pg_cnt       NUMBER := 120;
  v_custno_cnt   NUMBER := 30;
  v_kad_cnt      NUMBER := 30;
  v_ka_cnt       NUMBER := 30;
  v_comb_cnt     NUMBER := 10000;

  v_accruals_type_id        NUMBER;
  v_promotion_type_id       NUMBER;
  v_t_ep_out_of_strategy_id NUMBER;
  v_t_ep_promo_conflict_id  NUMBER;
  v_promotion_stat_id       NUMBER;
  v_promotion_guidelines_id NUMBER;
  v_scenario_id             NUMBER;

  p_user_id   NUMBER;
  p_member_id NUMBER;
  p_level_id  NUMBER;
  v_error     NUMBER;

  v_p_lvl_cnt NUMBER := 0;
  v_c_lvl_cnt NUMBER := 0;

  v_from_date   DATE;
  v_until_date  DATE;

  v_count       NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_REFRESH_PAST_PROMO';
   --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status ||'  '||member_id ||'  '|| SYSDATE, user_id, level_id, member_id);

   COMMIT;

   SELECT COUNT(1)
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);

   IF v_count <> 0 THEN

   v_step := '1 ';
   dbex (v_step || SYSDATE, v_prog_name);

   p_user_id := user_id;
   p_member_id := member_id;
   p_level_id := level_id;

   SELECT from_date, until_date
   INTO v_from_date, v_until_date
   FROM promotion_dates
   WHERE promotion_id = p_member_id;

   SELECT COUNT(1)
   INTO v_p_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (12, 424, 362)
   AND promotion_id = p_member_id;

   SELECT COUNT(1)
   INTO v_c_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (426, 490, 491, 478, 366)
   AND promotion_id = p_member_id;

   IF v_p_lvl_cnt = 0 OR v_c_lvl_cnt = 0 THEN

    UPDATE promotion
    SET oi_combs_status = 1
    WHERE promotion_id = p_member_id
      AND scenario_id <> 265;

    COMMIT;

    GOTO end_proc;

   END IF;



   SELECT accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
          promotion_guidelines_id, scenario_id
      INTO v_accruals_type_id, v_promotion_type_id, v_t_ep_out_of_strategy_id, v_t_ep_promo_conflict_id, v_promotion_stat_id,
        v_promotion_guidelines_id, v_scenario_id
   FROM promotion
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;

   check_and_drop('tmpcombs_' || p_user_id || '_' || p_member_id);

   dynamic_ddl('CREATE TABLE tmpcombs_' || p_user_id || '_' || p_member_id || '(promotion_id NUMBER, item_id NUMBER, location_id NUMBER,
                accruals_type_id NUMBER, promotion_type_id NUMBER, t_ep_out_of_strategy_id NUMBER,
                t_ep_promo_conflict_id NUMBER, promotion_stat_id NUMBER, promotion_guidelines_id NUMBER, scenario_id NUMBER)');
   --v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, '1' || SYSDATE);
   sql_str :=
            'MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO tmpcombs_' || p_user_id || '_' || p_member_id || ' pm
                                      USING(SELECT '
         || p_member_id
         || ' promotion_id, m.item_id, m.location_id, '
         || v_accruals_type_id || ' accruals_type_id, '
         || v_promotion_type_id || ' promotion_type_id, '
         || v_t_ep_out_of_strategy_id || ' t_ep_out_of_strategy_id, '
         || v_t_ep_promo_conflict_id || ' t_ep_promo_conflict_id, '
         || v_promotion_stat_id || ' promotion_stat_id, '
         || v_promotion_guidelines_id  || ' promotion_guidelines_id, '
         || v_scenario_id || ' scenario_id
                            FROM mdp_matrix m
                            WHERE 1 = 1
                                                    ';

  FOR rec1 IN (SELECT DISTINCT level_id FROM
                   (SELECT DISTINCT usp.level_id
                              FROM user_security_population usp
                             WHERE usp.user_id = p_user_id
                             AND usp.permission_id IN (3, 4)
                   UNION ALL
                   SELECT DISTINCT pl.level_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id)
                   WHERE level_id IN (SELECT group_table_id
                                      FROM group_tables
                                      WHERE 1 = 1
                                      AND lower(search_table) IN ('items', 'location'))
                   )
   LOOP
         SELECT gtable, search_table, id_field
           INTO v_gtable, v_search_table, v_id_field
           FROM group_tables
          WHERE group_table_id = rec1.level_id
          AND lower(search_table) IN ('items', 'location');

         sql_str := sql_str || 'AND m.' || v_id_field || ' IN (-1 ';

         cnt := 0;

         FOR rec2 IN (SELECT DISTINCT usp.member_id
                                 FROM user_security_population usp
                                WHERE usp.user_id = p_user_id
                                AND usp.permission_id IN (3, 4)
                                AND usp.level_id = rec1.level_id)
         LOOP
            sql_str := sql_str || ',' || rec2.member_id;
            cnt := cnt + 1;
         END LOOP;

         FOR rec2 IN (SELECT DISTINCT pl.level_id, pl.filter_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id AND pl.level_id = rec1.level_id)
        LOOP

         FOR rec3 IN (SELECT DISTINCT pm.member_id
                                 FROM promotion_members pm
                                WHERE pm.filter_id = rec2.filter_id)
         LOOP
            sql_str := sql_str || ',' || rec3.member_id;
            cnt := cnt + 1;
         END LOOP;

         END LOOP;

            sql_str :=
            sql_str
            || ')
                  ';
      END LOOP;

      IF v_error = 1 THEN

        UPDATE promotion
        SET oi_combs_status = 2
        WHERE promotion_id = p_member_id
          AND scenario_id <> 265;

        COMMIT;
        GOTO end_proc;

      END IF;

      sql_str :=
            sql_str
         || '
         AND NOT EXISTS (SELECT 1 FROM promotion_matrix pm2 WHERE pm2.promotion_id = ' || p_member_id ||
                        ' AND pm2.item_id = m.item_id AND pm2.location_id = m.location_id
                          AND pm2.scenario_id <> 265))pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
                             promotion_guidelines_id, scenario_id)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id, pm1.t_ep_promo_conflict_id,
                             pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)';

      --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, sql_str || SYSDATE, user_id, level_id, member_id);
      dynamic_ddl (sql_str);

      --EXECUTE IMMEDIATE sql_str;
      COMMIT;

   cnt := 0;

   EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM tmpcombs_' || p_user_id || '_' || p_member_id
   INTO cnt;

   IF cnt > v_comb_cnt THEN

        UPDATE promotion
        SET oi_combs_status = 3
        WHERE promotion_id = p_member_id
        AND scenario_id <> 265;

        COMMIT;

        GOTO end_proc;

   END IF;


dynamic_ddl(' MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO promotion_matrix pm
                                      USING(SELECT x. promotion_id, x.item_id, x.location_id,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id, --v_from_date,v_until_date
                TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')v_from_date,
                TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') v_until_date
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                                          AND (m.item_id, m.location_id) in (SELECT item_id, location_id FROM t_claim_combs))pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id, from_date, until_date)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id,
                pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id,
                pm1.t_ep_promo_conflict_id, pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id,
                v_from_date,v_until_date)'
                );

   COMMIT;


dynamic_ddl('MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO promotion_data pm
                                      USING(SELECT x.promotion_id, x.item_id, x.location_id, i.datet sales_date,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m,
                inputs i
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                AND (m.item_id, m.location_id) in (SELECT item_id, location_id FROM t_claim_combs)
                AND i.datet BETWEEN NEXT_DAY(TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7
                AND NEXT_DAY(TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7)pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, sales_date,
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id)
                      VALUES( pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.sales_date,
                pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id,
                pm1.t_ep_promo_conflict_id, pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)');
                commit;

   UPDATE promotion
   SET oi_combs_status = 10
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;

   COMMIT;

   END IF;

<<end_proc>>
   v_status := 'end ';
   --v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
            UPDATE promotion
            SET oi_combs_status = 9
            WHERE promotion_id = p_member_id
            AND scenario_id <> 265;

            COMMIT;

         END;
    END;

 PROCEDURE prc_past_promo
  IS
/******************************************************************************
   NAME:       prc_past_promo
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Mamta Jangra / Red Rock Consulting

******************************************************************************/

   sql_str          VARCHAR2 (5000);

   v_prog_name    VARCHAR2 (100) := 'PRC_PAST_PROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_past_promo_weeks NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_PAST_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   select to_number(pval)
   INTO v_past_promo_weeks
   FROM sys_params
   WHERE pname ='past_promo_weeks';

      For rec in (select  distinct p.promotion_id from promotion_data pd, promotion p,mdp_matrix m, promotion_dates pds
                                             where pd.item_id= m.item_id and pd.location_id =m.location_id and p.promotion_id= pd.promotion_id and p.promotion_id= pds.promotion_id
                                             and p.promotion_stat_id in (4,5,6,7)
                                             and m.t_ep_L_ATT_10_EP_ID = 12 --and (m.item_id , m.location_id) in ( select item_id , location_id from t_claim_combs)
                                             and trunc( pds.from_date) between trunc(NEXT_DAY(sysdate-7*v_past_promo_weeks, 'MONDAY'))-7  and sysdate)
   Loop
   pkg_ptp.prc_refresh_past_promo(null,null,rec.promotion_id);
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex ('PRC_REFRESH_PAST_PROMO', v_package_name, v_status||'  '||rec.promotion_id||'  ' || SYSDATE);

   End loop;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


PROCEDURE prc_past_promo_oneoff
  IS
/******************************************************************************
   NAME:       prc_past_promo
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Mamta Jangra / Red Rock Consulting

******************************************************************************/

   sql_str          VARCHAR2 (5000);

   v_prog_name    VARCHAR2 (100) := 'PRC_PAST_PROMO_ONEOFF';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_past_promo_weeks NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_PAST_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   select to_number(pval)
   INTO v_past_promo_weeks
   FROM sys_params
   WHERE pname ='past_promo_weeks';

      For rec in (select  distinct p.promotion_id from promotion_data pd, promotion p,mdp_matrix m, promotion_dates pds
                                             where pd.item_id= m.item_id and pd.location_id =m.location_id and p.promotion_id= pd.promotion_id and p.promotion_id= pds.promotion_id
                                             and p.promotion_stat_id in (4,5,6,7)
                                             and m.t_ep_L_ATT_10_EP_ID = 12 --and (m.item_id , m.location_id) in ( select item_id , location_id from t_claim_combs)
                                             and trunc( pds.from_date) between trunc(NEXT_DAY(sysdate-7*v_past_promo_weeks, 'MONDAY'))-7  and sysdate)
   Loop
   pkg_ptp.prc_refresh_past_promo_oneoff(null,null,rec.promotion_id);
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex ('PRC_REFRESH_PAST_PROMO', v_package_name, v_status||'  '||rec.promotion_id||'  ' || SYSDATE);

   End loop;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;
PROCEDURE prc_refresh_past_promo_oneoff (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL)
  AS

  sql_str         VARCHAR2 (32000);
  v_id_field      VARCHAR2 (100);
  v_search_table  VARCHAR2(20);
  v_gtable  VARCHAR2(300);
  cnt             NUMBER;

  v_prog_name    VARCHAR2 (100) := 'PRC_REFRESH_PAST_PROMO_ONEOFF';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;
  v_step         VARCHAR2(50);

  v_prod_cnt     NUMBER := 120;
  v_pg_cnt       NUMBER := 120;
  v_custno_cnt   NUMBER := 30;
  v_kad_cnt      NUMBER := 30;
  v_ka_cnt       NUMBER := 30;
  v_comb_cnt     NUMBER := 10000;

  v_accruals_type_id        NUMBER;
  v_promotion_type_id       NUMBER;
  v_t_ep_out_of_strategy_id NUMBER;
  v_t_ep_promo_conflict_id  NUMBER;
  v_promotion_stat_id       NUMBER;
  v_promotion_guidelines_id NUMBER;
  v_scenario_id             NUMBER;

  p_user_id   NUMBER;
  p_member_id NUMBER;
  p_level_id  NUMBER;
  v_error     NUMBER;

  v_p_lvl_cnt NUMBER := 0;
  v_c_lvl_cnt NUMBER := 0;

  v_from_date   DATE;
  v_until_date  DATE;

  v_count       NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_REFRESH_PAST_PROMO';
   --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status ||'  '||member_id ||'  '|| SYSDATE, user_id, level_id, member_id);

   COMMIT;

   SELECT COUNT(1)
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);

   IF v_count <> 0 THEN

   v_step := '1 ';
   dbex (v_step || SYSDATE, v_prog_name);

   p_user_id := user_id;
   p_member_id := member_id;
   p_level_id := level_id;

   SELECT from_date, until_date
   INTO v_from_date, v_until_date
   FROM promotion_dates
   WHERE promotion_id = p_member_id;

   SELECT COUNT(1)
   INTO v_p_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (12, 424, 362)
   AND promotion_id = p_member_id;

   SELECT COUNT(1)
   INTO v_c_lvl_cnt
   FROM promotion_levels
   WHERE level_id IN (426, 490, 491, 478, 366)
   AND promotion_id = p_member_id;

   IF v_p_lvl_cnt = 0 OR v_c_lvl_cnt = 0 THEN

    UPDATE promotion
    SET oi_combs_status = 1
    WHERE promotion_id = p_member_id
      AND scenario_id <> 265;

    COMMIT;

    GOTO end_proc;

   END IF;



   SELECT accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
          promotion_guidelines_id, scenario_id
      INTO v_accruals_type_id, v_promotion_type_id, v_t_ep_out_of_strategy_id, v_t_ep_promo_conflict_id, v_promotion_stat_id,
        v_promotion_guidelines_id, v_scenario_id
   FROM promotion
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;

   check_and_drop('tmpcombs_' || p_user_id || '_' || p_member_id);

   dynamic_ddl('CREATE TABLE tmpcombs_' || p_user_id || '_' || p_member_id || '(promotion_id NUMBER, item_id NUMBER, location_id NUMBER,
                accruals_type_id NUMBER, promotion_type_id NUMBER, t_ep_out_of_strategy_id NUMBER,
                t_ep_promo_conflict_id NUMBER, promotion_stat_id NUMBER, promotion_guidelines_id NUMBER, scenario_id NUMBER)');
   --v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, '1' || SYSDATE);
   sql_str :=
            'MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO tmpcombs_' || p_user_id || '_' || p_member_id || ' pm
                                      USING(SELECT '
         || p_member_id
         || ' promotion_id, m.item_id, m.location_id, '
         || v_accruals_type_id || ' accruals_type_id, '
         || v_promotion_type_id || ' promotion_type_id, '
         || v_t_ep_out_of_strategy_id || ' t_ep_out_of_strategy_id, '
         || v_t_ep_promo_conflict_id || ' t_ep_promo_conflict_id, '
         || v_promotion_stat_id || ' promotion_stat_id, '
         || v_promotion_guidelines_id  || ' promotion_guidelines_id, '
         || v_scenario_id || ' scenario_id
                            FROM mdp_matrix m
                            WHERE 1 = 1
                                                    ';

  FOR rec1 IN (SELECT DISTINCT level_id FROM
                   (SELECT DISTINCT usp.level_id
                              FROM user_security_population usp
                             WHERE usp.user_id = p_user_id
                             AND usp.permission_id IN (3, 4)
                   UNION ALL
                   SELECT DISTINCT pl.level_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id)
                   WHERE level_id IN (SELECT group_table_id
                                      FROM group_tables
                                      WHERE 1 = 1
                                      AND lower(search_table) IN ('items', 'location'))
                   )
   LOOP
         SELECT gtable, search_table, id_field
           INTO v_gtable, v_search_table, v_id_field
           FROM group_tables
          WHERE group_table_id = rec1.level_id
          AND lower(search_table) IN ('items', 'location');

         sql_str := sql_str || 'AND m.' || v_id_field || ' IN (-1 ';

         cnt := 0;

         FOR rec2 IN (SELECT DISTINCT usp.member_id
                                 FROM user_security_population usp
                                WHERE usp.user_id = p_user_id
                                AND usp.permission_id IN (3, 4)
                                AND usp.level_id = rec1.level_id)
         LOOP
            sql_str := sql_str || ',' || rec2.member_id;
            cnt := cnt + 1;
         END LOOP;

         FOR rec2 IN (SELECT DISTINCT pl.level_id, pl.filter_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id AND pl.level_id = rec1.level_id)
        LOOP

         FOR rec3 IN (SELECT DISTINCT pm.member_id
                                 FROM promotion_members pm
                                WHERE pm.filter_id = rec2.filter_id)
         LOOP
            sql_str := sql_str || ',' || rec3.member_id;
            cnt := cnt + 1;
         END LOOP;

         END LOOP;

            sql_str :=
            sql_str
            || ')
                  ';
      END LOOP;

      IF v_error = 1 THEN

        UPDATE promotion
        SET oi_combs_status = 2
        WHERE promotion_id = p_member_id
          AND scenario_id <> 265;

        COMMIT;
        GOTO end_proc;

      END IF;

      sql_str :=
            sql_str
         || '
         AND NOT EXISTS (SELECT 1 FROM promotion_matrix pm2 WHERE pm2.promotion_id = ' || p_member_id ||
                        ' AND pm2.item_id = m.item_id AND pm2.location_id = m.location_id
                          AND pm2.scenario_id <> 265))pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
                             promotion_guidelines_id, scenario_id)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id, pm1.t_ep_promo_conflict_id,
                             pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)';

      v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, sql_str || SYSDATE, user_id, level_id, member_id);
      dynamic_ddl (sql_str);

      --EXECUTE IMMEDIATE sql_str;
      COMMIT;

   cnt := 0;

   EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM tmpcombs_' || p_user_id || '_' || p_member_id
   INTO cnt;

   IF cnt > v_comb_cnt THEN

        UPDATE promotion
        SET oi_combs_status = 3
        WHERE promotion_id = p_member_id
        AND scenario_id <> 265;

        COMMIT;

        GOTO end_proc;

   END IF;


dynamic_ddl(' MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO promotion_matrix pm
                                      USING(SELECT x. promotion_id, x.item_id, x.location_id,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id, --v_from_date,v_until_date
                TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')v_from_date,
                TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') v_until_date
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                                          --AND (m.item_id, m.location_id) in (SELECT item_id, location_id FROM t_claim_combs)
                                                            )pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id, from_date, until_date)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id,
                pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id,
                pm1.t_ep_promo_conflict_id, pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id,
                v_from_date,v_until_date)'
                );

   COMMIT;


dynamic_ddl('MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO promotion_data pm
                                      USING(SELECT x.promotion_id, x.item_id, x.location_id, i.datet sales_date,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id,
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m,
                inputs i
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                --AND (m.item_id, m.location_id) in (SELECT item_id, location_id FROM t_claim_combs)
                AND i.datet BETWEEN NEXT_DAY(TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7
                AND NEXT_DAY(TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7)pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, sales_date,
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id,
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id)
                      VALUES( pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.sales_date,
                pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id,
                pm1.t_ep_promo_conflict_id, pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)');
                commit;

   UPDATE promotion
   SET oi_combs_status = 10
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;

   COMMIT;

   END IF;

<<end_proc>>
v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            --RAISE;
            UPDATE promotion
            SET oi_combs_status = 9
            WHERE promotion_id = p_member_id
            AND scenario_id <> 265;

            COMMIT;

         END;
    END;

PROCEDURE prc_promo_commit_ss
  IS
/******************************************************************************
   NAME:       prc_past_promo
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Mamta Jangra / Red Rock Consulting

******************************************************************************/

   sql_str          VARCHAR2 (5000);

   v_prog_name    VARCHAR2 (100) := 'PRC_PROMO_COMMIT_SS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_future_promo_weeks NUMBER;
   fore_column      VARCHAR2(100);
   v_m_date         VARCHAR2(20);
   v_eng_profile    NUMBER := 1;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_PROMO_COMMIT_SS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   select to_number(pval)
   INTO v_future_promo_weeks
   FROM sys_params
   WHERE pname ='weeks_commit_snapshot';

   SELECT get_max_date
    into v_m_date
    FROM DUAL;

       --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;

   check_and_drop('rr_promos_calc_ss_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_calc_ss_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps
               WHERE 1 = 1
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND ps.promotion_stat_id = 4
               AND nvl(p.promo_ss_flag, 0) = 0
               AND p.scenario_id IN (22, 262, 162)
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date >= TRUNC(sysdate,''YY'')
        ----       AND pdt.until_date <= sysdate + (' || v_future_promo_weeks  ||' * 7)  --- modified to one full calendar month from sysdate
               AND pdt.until_date < TRUNC (add_months(sysdate,2), ''MM'') ';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_calc_ss_t noparallel');		-- cmos 23-Jan-2017
    COMMIT;

     check_and_drop('rr_promos_calc_val_ss_t');

    -- cmos 23-Jan-2017 (px ctas)
    sql_str := 'CREATE TABLE rr_promos_calc_val_ss_t
                TABLESPACE ts_temp_tables parallel 32 nologging AS
                SELECT /*+ FULL(pd) PARALLEL(pd, 32) */ pd.promotion_id,
               pd.item_id,
               pd.location_id,
               pd.sales_date,
               CASE
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= pd.sales_date
                         OR  NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0)
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl,0)
               END base_vol_commit,
               CASE
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >=  pd.sales_date
                        OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 )
                    THEN NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.incr_evt_vol_or, NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END incr_vol_commit,
               NVL(pd.rr_redemp_p, 1) redemp_perc_commit ,
               NVL(pd.event_cost,0) coop_commit ,
               NVL(pd.case_buydown,0) case_deal_commit,
               pd.case_buydownoi cd_oi_commit ,
               pd.rr_oi_p cd_oi_p_commit ,
               NVL(rr_ms_vol,0) ms_vol_commit,
               NVL(pd.ms_cd_not_ms_promo,0) ms_cd_np_commit,
               nvl(ms_vol_not_ms_promo, 0) ms_vol_np_commit
               FROM promotion p, promotion_data pd,  rr_promos_calc_ss_t rpt
               WHERE 1 = 1
               AND p.promotion_id = rpt.promotion_id
               AND p.promotion_id = pd.promotion_id
               AND pd.is_self = 1'; -- lannapra Added 15 May 2015


    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_calc_val_ss_t noparallel');		-- cmos 23-Jan-2017
    COMMIT;

        sql_str := 'MERGE /*+ FULL(pd) PARALLEL(pd, 16) */ INTO promotion_data pd
               USING(SELECT /*+ FULL(t) PARALLEL(t, 32) */ * FROM rr_promos_calc_val_ss_t t) pd1
               ON(pd.promotion_id = pd1.promotion_id
               AND pd.item_id = pd1.item_id
               AND pd.location_id = pd1.location_id
               AND pd.sales_date = pd1.sales_date)
               WHEN MATCHED THEN
               UPDATE SET pd.base_vol_commit = pd1.base_vol_commit,
                          pd.incr_vol_commit = pd1.incr_vol_commit,
                          pd.redemp_perc_commit = pd1.redemp_perc_commit,
                          pd.case_deal_commit = pd1.case_deal_commit,
                          pd.coop_commit = pd1.coop_commit,
                          pd.ms_vol_commit = pd1.ms_vol_commit,
                          pd.cd_oi_commit = pd1.cd_oi_commit,
                          pd.cd_oi_p_commit = pd1.cd_oi_p_commit,
                          pd.ms_cd_np_commit = pd1.ms_cd_np_commit,
                          pd.ms_vol_np_commit = pd1.ms_vol_np_commit';

   dynamic_ddl(sql_str);
   COMMIT;

        sql_str := 'MERGE /*+ FULL(pd) PARALLEL(p, 16) */ INTO promotion p
               USING(SELECT /*+ FULL(t) PARALLEL(t, 32) */ distinct promotion_id FROM rr_promos_calc_val_ss_t t) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET p.promo_ss_flag = 1,
                          p.promo_ss_date = sysdate';

   dynamic_ddl(sql_str);
   COMMIT;


   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


PROCEDURE prc_update_promo_program
  IS
/******************************************************************************
   NAME:       prc_update_promo_program
   PURPOSE:    Procedure to update promotional programs from promoted_group and cl3 combinations

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        15/10/2015  Lakshmi Annapragada / Red Rock Consulting

******************************************************************************/

   v_sql_str          VARCHAR2 (32000);

   v_prog_name    VARCHAR2 (100) := 'PRC_UPDATE_PROMO_PROGRAM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_future_promo_weeks NUMBER;
   v_cal_year      NUMBER;
   v_m_date         VARCHAR2(20);
   v_eng_profile    NUMBER := 1;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_PROMO_PROGRAM';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    select distinct calendar_year
    into v_cal_year
    from inputs where trunc(datet,'YYYY') = trunc(sysdate,'YYYY');



    FOR REC in (select min(datet) min_date,to_char(max(Datet),'YYYY') yr
    FROM INPUTS  WHERE CALENDAR_YEAR BETWEEN V_CAL_YEAR -1 AND V_CAL_YEAR + 1 group by calendar_year)
  ---  and calendar_year <> v_cal_year group by calendar_year)

   LOOP
      MERGE INTO PROMOTION_GUIDELINES PG1
      USING (SELECT DISTINCT P2B.P2B||'_'||LA6.EBS_CUSTOMER||'_'||rec.yr PG_CODE,P2B.P2B_DESC||'_'||LA6.EBS_CUSTOMER_DESC||'_'||rec.yr PG_DESC,P2B.T_EP_P2B_EP_ID LVL1, LA6.T_EP_EBS_CUSTOMER_EP_ID LVL2,
      TO_DATE('01/01/'||REC.YR,'DD/MM/YYYY') RANGE_START,TO_DATE('31/12/'||REC.YR,'DD/MM/YYYY') RANGE_END
                    --INTO v_level1, v_level2
                    FROM
                    mdp_matrix mdp,
                    t_ep_p2b p2b,
                    t_ep_ebs_customer la6
                    WHERE la6.t_ep_ebs_customer_ep_id = mdp.t_ep_ebs_customer_ep_id
                    AND P2B.T_EP_P2B_EP_ID = MDP.T_EP_P2B_EP_ID
                    AND LA6.T_EP_EBS_CUSTOMER_EP_ID <> 0
                    and  P2B.T_EP_P2B_EP_ID <> 0
                    --and p2b.p2b_desc like 'Co-Packed Alcohol-N/A'
                    )pg2
                  on(pg1.level1 = pg2.lvl1
              and pg1.level2 = pg2.lvl2
              AND to_char(PG1.RANGE_START, 'dd/mm/yyyy') = to_char(PG2.RANGE_START, 'dd/mm/yyyy')
              AND to_char(PG1.RANGE_END, 'dd/mm/yyyy') = to_char(PG2.RANGE_END, 'dd/mm/yyyy')
              and pg1.promotion_guidelines_id <> 0)
          WHEN MATCHED THEN UPDATE SET PG1.PROMOTION_GUIDELINES_CODE = PG2.PG_CODE,PG1.PROMOTION_GUIDELINES_DESC = PG2.PG_DESC
          WHEN NOT MATCHED THEN INSERT (PROMOTION_GUIDELINES_ID,PROMOTION_GUIDELINES_CODE,PROMOTION_GUIDELINES_DESC,LEVEL1,LEVEL2,RANGE_START,RANGE_END)
          values(promotion_guidelines_seq.nextval,pg2.pg_code,pg2.pg_desc,pg2.lvl1,pg2.lvl2,pg2.range_start,pg2.range_End);

COMMIT;
END LOOP;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_update_promo_freq
  IS
/******************************************************************************
   NAME:       prc_update_promo_program
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Mamta Jangra / Red Rock Consulting

******************************************************************************/

   v_sql_str          VARCHAR2 (32000);

   v_prog_name    VARCHAR2 (100) := 'PRC_UPDATE_PROMO_FREQ';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_future_promo_weeks NUMBER;
   v_cal_year      NUMBER;
   v_m_date         VARCHAR2(20);
   v_eng_profile    NUMBER := 1;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_PROMO_FREQ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

----Null out all values first

    update mdp_matrix
set rr_prom_freq_ly = 0,
    rr_prom_freq_cy = 0,
    rr_prom_freq_fy = 0,
    rr_prom_dep_frq_ly = 0,
    rr_prom_dep_frq_cy = 0,
    rr_prom_dep_frq_fy = 0
    where nvl(rr_prom_freq_ly,0) +
    nvl(rr_prom_freq_cy,0) +
    nvl(rr_prom_freq_fy,0) +
    nvl(rr_prom_dep_frq_ly,0) +
    nvl(rr_prom_dep_frq_cy,0) +
    nvl(rr_prom_dep_frq_fy,0) <> 0;

    commit;

----Set Frequency and Depth Frequency Numbers for Past Year

merge into mdp_matrix m using (
select m.location_id, m.item_id, count(distinct sales_date) freq,
       count(distinct case when pd.promo_price <= m.rr_advert_pp then 1 else null end) depth_freq
from promotion_data pd,
      promotion p,
      promotion_type t,
      mdp_matrix m,
      t_ep_L_ATT_2 c1
where 1=1
  and pd.promotion_id = p.promotion_id
  and p.promotion_type_id = t.promotion_type_id
  and pd.location_id = m.location_id
  and pd.item_id = m.item_id
  and m.t_ep_l_att_2_ep_id = c1.T_EP_L_ATT_2_EP_ID
  and pd.sales_date = next_day(pd.sales_date,'MONDAY')-7
  and upper(c1.l_att_2_desc) like '%GROCERY%'
  and m.rr_advert_pp is not null
  and pd.sales_date between add_months(TRUNC(sysdate,'YEAR'),-12) and add_months(TRUNC(sysdate,'YEAR'),0)-1
  and p.RR_INCLD_FREQ_COUNT = 1
  GROUP BY m.location_id, m.item_id) n
  on (m.location_id = n.location_id
  and m.item_id = n.item_id)
  when matched then update
  set m.rr_prom_freq_ly = n.freq,
      m.rr_prom_dep_frq_ly = depth_freq,
      m.last_update_date = sysdate;

 commit;

   ----Set Frequency and Depth Frequency Numbers for Current Year

  merge into mdp_matrix m using (
select m.location_id, m.item_id, count(distinct sales_date) freq,
       count( case when pd.promo_price <= m.rr_advert_pp then 1 else null end ) depth_freq
from promotion_data pd,
      promotion p,
      promotion_type t,
      mdp_matrix m,
      t_ep_L_ATT_2 c1
where 1=1
  and pd.promotion_id = p.promotion_id
  and p.promotion_type_id = t.promotion_type_id
  and pd.location_id = m.location_id
  and pd.item_id = m.item_id
  and m.t_ep_l_att_2_ep_id = c1.T_EP_L_ATT_2_EP_ID
  and pd.sales_date = next_day(pd.sales_date,'MONDAY')-7
  and upper(c1.l_att_2_desc) like '%GROCERY%'
  and m.rr_advert_pp is not null
  and pd.sales_date between add_months(TRUNC(sysdate,'YEAR'),0) and add_months(TRUNC(sysdate,'YEAR'),12)-1
  and p.RR_INCLD_FREQ_COUNT = 1
  GROUP BY m.location_id, m.item_id) n
  on (m.location_id = n.location_id
  and m.item_id = n.item_id)
  when matched then update
  set m.rr_prom_freq_cy = n.freq,
      m.rr_prom_dep_frq_cy = depth_freq,
      m.last_update_date = sysdate;

  commit;

  ----Set Frequency and Depth Frequency Numbers for Future Year

  merge into mdp_matrix m using (
select m.location_id, m.item_id, count(distinct sales_date) freq,
       count(distinct  case when pd.promo_price <= m.rr_advert_pp then 1 else null end) depth_freq
from promotion_data pd,
      promotion p,
      promotion_type t,
      mdp_matrix m,
      t_ep_L_ATT_2 c1
where 1=1
  and pd.promotion_id = p.promotion_id
  and p.promotion_type_id = t.promotion_type_id
  and pd.location_id = m.location_id
  and pd.item_id = m.item_id
  and m.t_ep_l_att_2_ep_id = c1.T_EP_L_ATT_2_EP_ID
  and pd.sales_date = next_day(pd.sales_date,'MONDAY')-7
  and upper(c1.l_att_2_desc) like '%GROCERY%'
  and m.rr_advert_pp is not null
  and pd.sales_date between add_months(TRUNC(sysdate,'YEAR'),12) and add_months(TRUNC(sysdate,'YEAR'),24)-1
  and p.RR_INCLD_FREQ_COUNT = 1
  GROUP BY m.location_id, m.item_id) n
  on (m.location_id = n.location_id
  and m.item_id = n.item_id)
  when matched then update
  set m.rr_prom_freq_fy = n.freq,
      m.rr_prom_dep_frq_fy = depth_freq,
      m.last_update_date = sysdate;

      commit;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

END;

/
