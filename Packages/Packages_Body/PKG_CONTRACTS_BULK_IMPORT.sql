--------------------------------------------------------
--  DDL for Package Body PKG_CONTRACTS_BULK_IMPORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_CONTRACTS_BULK_IMPORT" 
AS
/******************************************************************************
    NAME:       PKG_CONTRACTS_BULK_IMPORT
    PURPOSE:    All procedures commanly used for contracts bulk import
    REVISIONS:
    Ver        Date        Author
    ---------  ----------  ---------------------------------------------------
    1.0        01/02/2016  Sanjiiv Chavaan / Redrock Consulting - Initial version
******************************************************************************/
v_package_name           VARCHAR2(100) := 'PKG_CONTRACTS_BULK_IMPORT';
v_status                 VARCHAR2(4096);
v_proc_log_id            NUMBER;
--
PROCEDURE PRC_ALL_PRE_PROCESSING
AS
    --
    v_prog_name          VARCHAR2(100) := 'PRC_ALL_PRE_PROCESSING';    
    vn_count             NUMBER;
    vn_user_id           NUMBER;
    vn_batch_id          NUMBER;
    vc_matl_str          VARCHAR2(240);
    vc_custlvl_str       VARCHAR2(240);
    vc_custlvl4_str      VARCHAR2(240);
    vc_custlvl2_str      VARCHAR2(240);
    vc_custlvl3_str      VARCHAR2(240);
    vc_recp_str          VARCHAR2(240);
    --
BEGIN
   --
   v_status := 'Start - Pre Processing';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   --
   SELECT COUNT(1) INTO vn_count FROM T_IMP_CONTRACT;
   --
   IF vn_count > 0 THEN
        --
        SELECT CONTRACT_BATCH_ID.nextval INTO vn_batch_id FROM DUAL;
        SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
        -- Update the batch_id/Reccord_id
        UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
        SET batch_id         = vn_batch_id,
        created_by           = vn_user_id,
        record_id            = CONTRACT_RECORD_ID.nextval,
        creation_date        = sysdate,
        last_updated_by      = vn_user_id,
        investment_end_date  = CASE WHEN TO_CHAR(TO_DATE(investment_end_date,'DD/MM/YYYY'),'YYYY') = '9999' THEN TO_CHAR(TO_DATE(investment_end_date,'DD/MM/YYYY'),'DD/MM/')||'2025' ELSE investment_end_date END,
        last_update_date     = sysdate,
        INVESTMENT_ID_GRP    = CASE WHEN INVESTMENT_ID_GRP IS NULL THEN 1 ELSE INVESTMENT_ID_GRP END,
        material             = CASE WHEN MATERIAL IS NOT NULL AND MATERIAL <> '0' THEN MATERIAL||'-30110101' ELSE MATERIAL END                        -- as per call with Andrew
        ;
        ---
        COMMIT;
        ---
        FOR C1 IN (SELECT /*+ full(ct) parallel(ct,16) */ * FROM T_IMP_CONTRACT) LOOP
            IF C1.material IS NOT NULL AND C1.MATERIAL <> '0' THEN
                vc_matl_str := c1.material;
                IF length(C1.material) < 18 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..18 - length(substr(C1.material,1,instr(C1.material,'-')- 1))  loop
                        vc_matl_str := '0'||vc_matl_str;
                        --dbms_output.put_line('str-'||str);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    material = vc_matl_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --     
            END IF;
            --- Adding leading 0's to the Customer Columns as per mail from Andrew Plus
            IF C1.CUSTOMER_LEVEL IS NOT NULL  THEN
                vc_custlvl_str := C1.CUSTOMER_LEVEL; 
                IF length(vc_custlvl_str) < 10 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..10 - length(vc_custlvl_str)  loop
                        vc_custlvl_str := '0'||vc_custlvl_str;
                        --dbms_output.put_line('vc_string-'||vc_string);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    CUSTOMER_LEVEL = vc_custlvl_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --  
            END IF;
            ---
            IF C1.CUSTOMER_LEVEL2 IS NOT NULL  THEN
                vc_custlvl2_str := C1.CUSTOMER_LEVEL2; 
                IF length(vc_custlvl2_str) < 10 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..10 - length(vc_custlvl2_str)  loop
                        vc_custlvl2_str := '0'||vc_custlvl2_str;
                        --dbms_output.put_line('vc_string-'||vc_string);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    CUSTOMER_LEVEL2 = vc_custlvl2_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --  
            END IF;
            ---
            IF C1.CUSTOMER_LEVEL3 IS NOT NULL  THEN
                vc_custlvl3_str := C1.CUSTOMER_LEVEL3; 
                IF length(vc_custlvl3_str) < 10 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..10 - length(vc_custlvl3_str)  loop
                        vc_custlvl3_str := '0'||vc_custlvl3_str;
                        --dbms_output.put_line('vc_string-'||vc_string);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    CUSTOMER_LEVEL3 = vc_custlvl3_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --  
            END IF;
            ---
            IF C1.CUSTOMER_LEVEL4 IS NOT NULL  THEN
                vc_custlvl4_str := C1.CUSTOMER_LEVEL4; 
                IF length(vc_custlvl4_str) < 10 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..10 - length(vc_custlvl4_str)  loop
                        vc_custlvl4_str := '0'||vc_custlvl4_str;
                        --dbms_output.put_line('vc_string-'||vc_string);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    CUSTOMER_LEVEL4 = vc_custlvl4_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --  
            END IF;
            ---
            IF C1.RECIPIENT IS NOT NULL  THEN
                vc_recp_str := C1.RECIPIENT; 
                IF length(vc_recp_str) < 10 THEN
                    -- Populate the leading 0 for the SKU
                    FOR i in 1..10 - length(vc_recp_str)  loop
                        vc_recp_str := '0'||vc_recp_str;
                        --dbms_output.put_line('vc_string-'||vc_string);
                    END LOOP;
                END IF;
                --
                update /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT ct
                set    RECIPIENT = vc_recp_str
                where  batch_id  = c1.batch_id
                and    record_id = c1.record_id; 
                --  
            END IF;
            ---
        END LOOP;
        --
        COMMIT;    
        --
        DYNAMIC_DDL('TRUNCATE TABLE T_IMP_CONTRACT_TMP');
        --
    END IF;
    /*
    --
    UPDATE t_imp_contract
    SET   investment_end_date   = to_char(to_date(investment_end_date,'DD/MON/RR'),'DD/MM/YYYY'),
            investment_start_date = to_char(to_date(investment_start_date,'DD/MON/RR'),'DD/MM/YYYY'),
            material              = decode(material,'0',null,material);
    --
    COMMIT;
    */
    --
    v_status := 'End - Post Processing';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
EXCEPTION
   WHEN OTHERS THEN
      BEGIN
         v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),dbms_utility.format_error_stack,dbms_utility.format_error_backtrace);
         RAISE;
      END;
END PRC_ALL_PRE_PROCESSING;
--
    
PROCEDURE PRC_VALIDATE_CONTRACTS    
AS    
    --
    v_prog_name              VARCHAR2(100) := 'PRC_VALIDATE_CONTRACTS';
    vc_err_message           VARCHAR2(32767);
    vn_count                 NUMBER;
    vc_key_comb_vl           VARCHAR2(4096);
    --
    seq                      ACCESS_SEQ%ROWTYPE;
    vc_display_field         VARCHAR2(240);
    vc_gt_name               VARCHAR2(240);
    vc_gt_label              VARCHAR2(240); 
    vc_id_field              VARCHAR2(2000);    
    vc_id_field1             VARCHAR2(240);    
    vc_id_field2             VARCHAR2(240);    
    vc_id_field3             VARCHAR2(240);    
    vc_id_field4             VARCHAR2(240);    
    vc_id_field5             VARCHAR2(240);    
    vc_id_field6             VARCHAR2(240);    
    vc_id_field7             VARCHAR2(240);    
    vc_id_field8             VARCHAR2(240);    
    vc_id_field9             VARCHAR2(240);
    vc_id_field10            VARCHAR2(240);        
    sql_str                  VARCHAR2(32767);
    vc_stg_col_name          VARCHAR2(25);
    vn_user_id               NUMBER;
    --
BEGIN
   --pre_logon;
   --
   v_status := 'Start - Validations';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   --
   select user_id into vn_user_id from user_id where user_name = 'EDI';
   -- Part 1 Validate the records in the staging table
   FOR C1 IN (SELECT /*+ full(ct) parallel(ct,16) */ * FROM T_IMP_CONTRACT ct)
   --
   LOOP
        vc_err_message                         := NULL;
        vc_key_comb_vl                         := NULL;
        vc_display_field                    := NULL;
        vc_gt_name                            := NULL;
        vc_gt_label                            := NULL;
        vc_id_field                            := NULL;
        vc_id_field1                        := NULL;
        vc_id_field2                        := NULL;
        vc_id_field3                        := NULL;
        vc_id_field4                        := NULL;
        vc_id_field5                        := NULL;
        vc_id_field6                        := NULL;
        vc_id_field7                        := NULL;
        vc_id_field8                        := NULL;
        vc_id_field9                        := NULL;
        vc_id_field10                        := NULL;
        vc_stg_col_name                        := NULL;
        seq                                    := NULL;
      -- Val1 
        IF C1.contract_id IS NULL THEN
            vc_err_message := 'Contract ID cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE    
            SELECT count(1) INTO vn_count FROM contract_group WHERE contract_group_code = c1.contract_id;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Contract ID does not exist in Demantra = '||C1.contract_id||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
        END IF;
      -- Val2
        IF C1.investment_id_grp IS NULL THEN
            vc_err_message := 'SAP Investment ID cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        END IF;
      -- Val3 
        IF C1.investment_type IS NULL THEN
            vc_err_message := 'Investment Type cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE    
            SELECT count(1) INTO vn_count FROM comp_type WHERE comp_type_code = c1.investment_type;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Invalid Investment Type = '||C1.investment_type||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;    
        END IF;
        -- Val4    
        IF C1.key_combination IS NULL THEN
            vc_err_message := 'Key Combinations cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE
            --
            SELECT count(1) INTO vn_count FROM access_seq WHERE access_seq_code = c1.key_combination;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Key combination code does not exist in Demantra = '||C1.key_combination||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            ELSE
                /*
                -- Further Validations on the given Key combinations    
                vc_key_comb_vl := '('||''''||C1.SALES_ORG||''''||
                                 ','||''''||C1.DIVISION||''''||
                                 ','||''''||C1.CUSTOMER_LEVEL2||''''||
                                 ','||''''||C1.CUSTOMER_LEVEL3||''''||
                                 ','||''''||C1.CUSTOMER_LEVEL4||''''||
                                 ','||''''||C1.CUSTOMER_LEVEL||''''||
                                 ','||''''||C1.SALES_OFFICE||''''||
                                 ','||''''||C1.TIER||''''||
                                 ','||''''||C1.BRAND2||''''||
                                 ','||''''||C1.BRAND3||''''||
                                 ','||''''||C1.PACK1||''''||
                                 ','||''''||C1.PACK2||''''||
                                 ','||''''||C1.PACK3||''''||
                                 ','||''''||C1.TAX_CLASS_MAT||''''||
                                 ','||''''||C1.DIST_CHANNEL||''''||
                                 ','||''''||C1.CUST_PRICING_PROCEDURE||''''||
                                 ','||''''||C1.MATERIAL||''''||
                                 ')';    
                */                         
                --
                --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, vc_key_comb_vl);
                --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, vc_id_field);
                --
                SELECT * INTO seq FROM access_seq WHERE access_seq_code =  c1.key_combination;
                --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'After - '||seq.level1||'-'||seq.level2||'-'||seq.level3||'-'||seq.level4||'-'||seq.level5||'-'||seq.level6||'-'||seq.level7||'-'||seq.level8||'-'||seq.level9||'-'||seq.level10);
                --
                IF seq.level1 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field1 FROM group_tables WHERE group_table_id = seq.level1;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field||''''||vc_id_field1||''''||',';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level1-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;    
                END IF;
                --
                IF seq.level2 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field2 FROM group_tables WHERE group_table_id = seq.level2;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field2||''''||',';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level2-'||vc_id_field);    
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level3 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field3 FROM group_tables WHERE group_table_id = seq.level3;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field3||''''||',';    
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level3-'||vc_id_field);     
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level4 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field4 FROM group_tables WHERE group_table_id = seq.level4;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field4||''''||',';            
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level4-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level5 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field5 FROM group_tables WHERE group_table_id = seq.level5;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field5||''''||',';            
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level5-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level6 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field6 FROM group_tables WHERE group_table_id = seq.level6;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field6||''''||',';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level6-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level7 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field7 FROM group_tables WHERE group_table_id = seq.level7;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field7||''''||',';            
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level7-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level8 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field8 FROM group_tables WHERE group_table_id = seq.level8;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field8||''''||',';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level8-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;    
                    END IF;
                END IF;
                --
                IF seq.level9 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field9 FROM group_tables WHERE group_table_id = seq.level9;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field9||''''||',';            
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level9-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                END IF;
                --
                IF seq.level10 IS NOT NULL THEN
                    --
                    SELECT id_display_field,gtable,table_label,lower(id_field) INTO vc_display_field,vc_gt_name,vc_gt_label,vc_id_field10 FROM group_tables WHERE group_table_id = seq.level10;
                    SELECT stg_tab_name INTO vc_stg_col_name from T_IMP_CONTRACT_MAPPING where group_tab_name = vc_gt_name;
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'vc_stg_col_name-'||vc_stg_col_name);
                    EXECUTE IMMEDIATE 'SELECT /* + index (T_IMP_CONTRACT t_imp_contract_idx) */ '||vc_stg_col_name||' FROM T_IMP_CONTRACT WHERE batch_id = '||c1.batch_id||' and record_id = '||c1.record_id INTO vc_key_comb_vl;
                    vc_key_comb_vl := '('||''''||vc_key_comb_vl||''''||')';
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name,'vc_key_comb_vl-'||vc_key_comb_vl);
                    EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM '||vc_gt_name||' WHERE '||vc_display_field||' IN '||vc_key_comb_vl INTO vn_count;
                    --
                    vc_id_field := vc_id_field|| ''''||vc_id_field10||''''||',';            
                    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'level10-'||vc_id_field);
                    IF vn_count = 0 then
                        vc_err_message := 'The given '||vc_gt_label||' key combination value cannot be blank OR the given value doesnt exists in Demantra'||'|';
                        --
                        UPDATE T_IMP_CONTRACT
                        SET error_message = error_message||vc_err_message
                        WHERE batch_id = c1.batch_id
                        AND   record_id = c1.record_id;
                    END IF;
                  --
                END IF;
                -- update the ep_id list for the given key combination    
                vc_id_field := '('||vc_id_field||''''||''''||')';
                UPDATE T_IMP_CONTRACT
                SET ID_FIELD_VALUE = vc_id_field
                WHERE  batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --    
            END IF;
        --                
        END IF;
        -- Val22
        IF C1.INVESTMENT_START_DATE IS NULL THEN
            vc_err_message := 'Investment Start Date cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE    
            select count(1) INTO vn_count from CONTRACT_GROUP where to_date(C1.INVESTMENT_START_DATE,'DD/MM/YYYY') between period_start and period_end and contract_group_id = c1.contract_id;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment Start date outside the contract period = '||C1.INVESTMENT_START_DATE||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
        END IF;
        -- Val23
        IF C1.INVESTMENT_END_DATE IS NULL THEN
            vc_err_message := 'Investment End Date cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE    
            select count(1) INTO vn_count from CONTRACT_GROUP where to_date(C1.INVESTMENT_END_DATE,'DD/MM/YYYY') between period_start and period_end and contract_group_id = c1.contract_id;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment End date outside the contract period = '||C1.INVESTMENT_END_DATE||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
        END IF;
        -- Val24
        IF C1.CASE_DEAL_DOLLAR_LV1 IS NOT NULL THEN
            SELECT count(1) INTO vn_count FROM comp_type WHERE comp_type_code = c1.investment_type and deal_type = 2;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment Type is not a Case Deal Dollar Type. Values found in CD Dollar - '||C1.investment_type||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
            IF C1.CASE_DEAL_DOLLAR_LV2 IS NOT NULL THEN
                 --
                 IF C1.CASE_DEAL_DOLLAR_LV1 IS NULL OR C1.MIN_VOL_LV1 IS NULL OR C1.MIN_VOL_LV2 IS NULL  THEN
                    --
                    vc_err_message := 'MIN_VOL_LV1/MIN_VOL_LV2/CASE_DEAL_DOLLAR_LV1 not entered for Scaled Case Deal Dollar Type-'||C1.investment_type||'|';
                    --
                    --
                    UPDATE T_IMP_CONTRACT
                    SET error_message = error_message||vc_err_message
                    WHERE batch_id = c1.batch_id
                    AND   record_id = c1.record_id;
                    --
                 END IF;
                 --
            END IF;
            -- 
            IF C1.CASE_DEAL_DOLLAR_LV3 IS NOT NULL THEN
                --
                IF C1.CASE_DEAL_DOLLAR_LV1 IS NULL OR C1.CASE_DEAL_DOLLAR_LV2 IS NULL OR C1.MIN_VOL_LV1 IS NULL OR C1.MIN_VOL_LV2 IS NULL OR C1.MIN_VOL_LV3 IS NULL THEN
                    --
                    vc_err_message := 'MIN_VOL_LV1/MIN_VOL_LV2/MIN_VOL_LV3/CASE_DEAL_DOLLAR_LV1/CASE_DEAL_DOLLAR_LV2 not entered for Scaled Case Deal Dollar Type-'||C1.investment_type||'|';
                    --
                    UPDATE T_IMP_CONTRACT
                    SET error_message = error_message||vc_err_message
                    WHERE batch_id = c1.batch_id
                    AND   record_id = c1.record_id;
                    --
                END IF;
                --
            END IF;
        END IF;
        -- Val25
        IF C1.CASE_DEAL_PERC_LV1 IS NOT NULL THEN
            SELECT count(1) INTO vn_count FROM comp_type WHERE comp_type_code = c1.investment_type and deal_type = 1;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment Type is not a CD Percentage Type. Values found in CD Percentage = '||C1.investment_type||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
            IF C1.CASE_DEAL_PERC_LV2 IS NOT NULL THEN
                --
                IF C1.CASE_DEAL_PERC_LV1 IS NULL OR C1.MIN_VOL_LV1 IS NULL OR C1.MIN_VOL_LV2 IS NULL  THEN
                    --
                    vc_err_message := 'MIN_VOL_LV1/MIN_VOL_LV2/CASE_DEAL_PERC_LV1 not entered for Scaled Case Deal Percentage Type = '||C1.investment_type||'|';
                    --
                    UPDATE T_IMP_CONTRACT
                    SET error_message = error_message||vc_err_message
                    WHERE batch_id = c1.batch_id
                    AND   record_id = c1.record_id;
                    --
                END IF;
                --
            END IF;
            -- 
            IF C1.CASE_DEAL_PERC_LV3 IS NOT NULL THEN
                --
                IF C1.CASE_DEAL_PERC_LV1 IS NULL OR C1.CASE_DEAL_PERC_LV2 IS NULL OR C1.MIN_VOL_LV1 IS NULL OR C1.MIN_VOL_LV2 IS NULL OR C1.MIN_VOL_LV3 IS NULL THEN
                    --
                    vc_err_message := 'MIN_VOL_LV1/MIN_VOL_LV2/MIN_VOL_LV3/CASE_DEAL_PERC_LV1/CASE_DEAL_PERC_LV2 not entered for Scaled Case Deal Percentage Type = '||C1.investment_type||'|';
                    --
                    UPDATE T_IMP_CONTRACT
                    SET error_message = error_message||vc_err_message
                    WHERE batch_id = c1.batch_id
                    AND   record_id = c1.record_id;
                    --
                END IF;
                --
            END IF;
        END IF;
        -- Val26
        IF C1.LUMPSUM IS NOT NULL THEN
            SELECT count(1) INTO vn_count FROM comp_type WHERE comp_type_code = c1.investment_type and deal_type = 3;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment Type is not a Lumpsum Type. Values found in Lumpsum = -'||C1.investment_type||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
        END IF;
        -- Val27
        IF C1.RECIPIENT is not null THEN
            --
            SELECT count(1) INTO vn_count FROM t_ep_e1_cust_cat_4 WHERE E1_CUST_CAT_4 = c1.RECIPIENT;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'RECIPIENT  does not exist in Demantra = '||C1.RECIPIENT||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --
        END IF;
        -- Val28
        IF C1.tax_consideration IS NULL THEN
            vc_err_message := 'Tax consideration cannot be blank|';
            --
            UPDATE T_IMP_CONTRACT
            SET error_message = error_message||vc_err_message
            WHERE batch_id = c1.batch_id
            AND   record_id = c1.record_id;
            --
        ELSE    
            SELECT count(1) INTO vn_count FROM tbl_tax_consideration WHERE TAX_CONSID_ID = c1.tax_consideration;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Tax Consideration does not exist in Demantra = '||C1.tax_consideration||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;    
        END IF;
        --- Val29 -- Check for the Investment Type/Contract_group - Receipent Condition
        IF C1.RECIPIENT IS NULL THEN
            --
            SELECT COUNT(1) INTO vn_count 
            FROM  CONTRACT_GROUP c, COMP_TYPE t 
            WHERE c.comp_type_id = t.comp_type_id 
            AND   comp_type_code         = C1.investment_type        -- 'ZT52'
            AND   c.contract_group_id     = C1.contract_id             -- 204
            AND   t.recipient_req         = 1;
            --
            IF vn_count = 1 THEN
                vc_err_message := 'Recipient Required for the Investment_Type Selected |';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
            END IF;
            --        
        END IF;
        -- Val30 
        IF C1.investment_type IS NOT NULL AND C1.key_combination IS NOT NULL THEN
            --
            SELECT COUNT(1) INTO vn_count 
            FROM  comp_type             a,
                    comp_type_seq     b
            WHERE a.comp_type_id       = b.comp_type_id
            AND  a.comp_type_code     = C1.investment_type
            AND   b.access_seq_id       = C1.key_combination;
            --
            IF vn_count = 0 THEN
                vc_err_message := 'Investment Type/Key Combination does not exist in Demantra = '||C1.investment_type||'/'||C1.key_combination||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
            END IF;
            --        
        END IF;
        -- Val31 
        IF C1.investment_start_date IS NOT NULL AND C1.investment_end_date IS NOT NULL THEN
            --
            IF TO_DATE(C1.investment_start_date,'DD/MM/YYYY') > TO_DATE(C1.investment_end_date,'DD/MM/YYYY') THEN 
                --                
                vc_err_message := 'Investment Start Date -'||C1.investment_start_date||' must be less than Investment End Date -'||C1.investment_end_date||'|';
                --
                UPDATE T_IMP_CONTRACT
                SET error_message = error_message||vc_err_message
                WHERE batch_id = c1.batch_id
                AND   record_id = c1.record_id;
                --
            END IF;
            --        
        END IF;
        --
        COMMIT;
        --
   END LOOP; -- End of Validation Loop
    --
    UPDATE T_IMP_CONTRACT
    SET process_flag = 3,
    last_update_date = sysdate,
    last_updated_by = vn_user_id
    WHERE error_message is not null;
    --
    COMMIT;
    --
    v_status := 'End - Validations';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
    --v_status := 'Start Insert';
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
    -- Insert all the Valid records in the Tmp Table - T_IMP_CONTRACT_TMP
    INSERT INTO T_IMP_CONTRACT_TMP(batch_id,record_id,contract_id,investment_id_grp,investment_type,key_combination,sales_org,dist_channel,division,customer_level2,
                                            customer_level3,customer_level4,customer_level,sales_office,tier,cust_pricing_procedure,brand2,brand3,pack1,pack2,pack3,tax_class_mat,material,
                                            investment_start_date,investment_end_date,case_deal_dollar_lv1,case_deal_dollar_lv2,case_deal_dollar_lv3,case_deal_perc_lv1,
                                            case_deal_perc_lv2,case_deal_perc_lv3,min_vol_lv1,min_vol_lv2,min_vol_lv3,lumpsum,rebate_details,payment_details,
                                            recipient,tax_consideration,ID_FIELD_VALUE,error_message,process_flag,created_by,creation_date,last_updated_by,last_update_date)
    SELECT /*+ full(ct) parallel(ct,16) */ * FROM T_IMP_CONTRACT ct
    WHERE error_message IS NULL;
    --
    COMMIT;
     
    --v_status := 'End Insert';
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    -- Update all the EP Id's for the records to be processed.  -- old code
    --UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT_TMP ct
    /*
    SET  t_ep_p4_ep_id = CASE WHEN SALES_ORG IS NOT NULL THEN (select t_ep_p4_ep_id FROM t_ep_p4 WHERE P4 = SALES_ORG) ELSE null END,
          t_ep_p3_ep_id = CASE WHEN DIST_CHANNEL IS NOT NULL THEN (select t_ep_p3_ep_id FROM t_ep_p3 WHERE P3 = DIST_CHANNEL) ELSE null END,
          t_ep_p2_ep_id = CASE WHEN DIVISION IS NOT NULL THEN (select t_ep_p2_ep_id FROM t_ep_p2 WHERE P2 = DIVISION) ELSE null END,
          t_ep_e1_cust_city_ep_id = CASE WHEN CUSTOMER_LEVEL2 IS NOT NULL THEN (select t_ep_e1_cust_city_ep_id FROM t_ep_e1_cust_city WHERE E1_CUST_CITY = CUSTOMER_LEVEL2) ELSE null END,
          t_ep_ebs_customer_ep_id = CASE WHEN CUSTOMER_LEVEL3 IS NOT NULL THEN (select t_ep_ebs_customer_ep_id FROM t_ep_ebs_customer WHERE EBS_CUSTOMER = CUSTOMER_LEVEL3) ELSE null END,
          t_ep_ebs_account_ep_id = CASE WHEN CUSTOMER_LEVEL4 IS NOT NULL THEN (select t_ep_ebs_account_ep_id FROM t_ep_ebs_account WHERE EBS_ACCOUNT = CUSTOMER_LEVEL4) ELSE null END,
          t_ep_e1_cust_cat_4_ep_id = CASE WHEN CUSTOMER_LEVEL IS NOT NULL THEN (select t_ep_e1_cust_cat_4_ep_id FROM t_ep_e1_cust_cat_4 WHERE E1_CUST_CAT_4 = CUSTOMER_LEVEL) ELSE null END,
          t_ep_l_att_6_ep_id = CASE WHEN SALES_OFFICE IS NOT NULL THEN (select t_ep_l_att_6_ep_id FROM t_ep_l_att_6 WHERE L_ATT_6 = SALES_OFFICE) ELSE null END,
          t_ep_e1_cust_cat_2_EP_ID = CASE WHEN TIER IS NOT NULL THEN (select t_ep_e1_cust_cat_2_EP_ID FROM t_ep_e1_cust_cat_2 WHERE E1_CUST_CAT_2 = TIER) ELSE null END,
          t_ep_e1_cust_ctry_ep_id = CASE WHEN CUST_PRICING_PROCEDURE IS NOT NULL THEN (select t_ep_e1_cust_ctry_ep_id FROM t_ep_e1_cust_ctry WHERE E1_CUST_CTRY = CUST_PRICING_PROCEDURE) ELSE null END,
          t_ep_e1_item_cat_4_ep_id = CASE WHEN BRAND2 IS NOT NULL THEN (select t_ep_e1_item_cat_4_ep_id FROM t_ep_e1_item_cat_4 WHERE E1_ITEM_CAT_4 = BRAND2) ELSE null END,
          t_ep_e1_item_cat_6_ep_id = CASE WHEN BRAND3 IS NOT NULL THEN (select t_ep_e1_item_cat_6_ep_id FROM t_ep_e1_item_cat_6 WHERE E1_ITEM_CAT_6 = BRAND3) ELSE null END,
          t_ep_ebs_prod_cat_ep_id = CASE WHEN PACK1 IS NOT NULL THEN (select t_ep_ebs_prod_cat_ep_id FROM t_ep_ebs_prod_cat WHERE EBS_PROD_CAT = PACK1) ELSE null END,
          t_ep_I_att_10_ep_id = CASE WHEN PACK2 IS NOT NULL THEN (select t_ep_I_att_10_ep_id FROM t_ep_I_att_10 WHERE I_ATT_10 = PACK2) ELSE null END,
          t_ep_e1_item_cat_7_ep_id = CASE WHEN PACK3 IS NOT NULL THEN (select t_ep_e1_item_cat_7_ep_id FROM t_ep_e1_item_cat_7 WHERE E1_ITEM_CAT_7 = PACK3) ELSE null END,
          t_ep_I_att_6_ep_id = CASE WHEN TAX_CLASS_MAT IS NOT NULL THEN (select t_ep_I_att_6_ep_id FROM t_ep_I_att_6 WHERE I_ATT_6 = TAX_CLASS_MAT) ELSE null END,
          t_ep_item_ep_id = CASE WHEN MATERIAL IS NOT NULL THEN (select t_ep_item_ep_id FROM t_ep_item WHERE item = MATERIAL) ELSE null END;
              
    */        
    --v_status := 'Start Update';
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT_TMP ct
    SET  t_ep_p4_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_p4_ep_id%') THEN (select t_ep_p4_ep_id FROM t_ep_p4 WHERE P4 = SALES_ORG) ELSE null END,
          t_ep_p3_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_p3_ep_id%') THEN (select t_ep_p3_ep_id FROM t_ep_p3 WHERE P3 = DIST_CHANNEL) ELSE null END,
          t_ep_p2_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_p2_ep_id%') THEN (select t_ep_p2_ep_id FROM t_ep_p2 WHERE P2 = DIVISION) ELSE null END,
          t_ep_e1_cust_city_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_cust_city_ep_id%') THEN (select t_ep_e1_cust_city_ep_id FROM t_ep_e1_cust_city WHERE E1_CUST_CITY = CUSTOMER_LEVEL2) ELSE null END,
          t_ep_ebs_customer_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_ebs_customer_ep_id%') THEN (select t_ep_ebs_customer_ep_id FROM t_ep_ebs_customer WHERE EBS_CUSTOMER = CUSTOMER_LEVEL3) ELSE null END,
          t_ep_ebs_account_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_ebs_account_ep_id%') THEN (select t_ep_ebs_account_ep_id FROM t_ep_ebs_account WHERE EBS_ACCOUNT = CUSTOMER_LEVEL4) ELSE null END,
          t_ep_e1_cust_cat_4_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_cust_cat_4_ep_id%') THEN (select t_ep_e1_cust_cat_4_ep_id FROM t_ep_e1_cust_cat_4 WHERE E1_CUST_CAT_4 = CUSTOMER_LEVEL) ELSE null END,
          t_ep_l_att_6_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_l_att_6_ep_id%') THEN (select t_ep_l_att_6_ep_id FROM t_ep_l_att_6 WHERE L_ATT_6 = SALES_OFFICE) ELSE null END,
          t_ep_e1_cust_cat_2_EP_ID = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_cust_cat_2_ep_id%') THEN (select t_ep_e1_cust_cat_2_EP_ID FROM t_ep_e1_cust_cat_2 WHERE E1_CUST_CAT_2 = TIER) ELSE null END,
          t_ep_e1_cust_ctry_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_cust_ctry_ep_id%') THEN (select t_ep_e1_cust_ctry_ep_id FROM t_ep_e1_cust_ctry WHERE E1_CUST_CTRY = CUST_PRICING_PROCEDURE) ELSE null END,
          t_ep_e1_item_cat_4_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_item_cat_4_ep_id%') THEN (select t_ep_e1_item_cat_4_ep_id FROM t_ep_e1_item_cat_4 WHERE E1_ITEM_CAT_4 = BRAND2) ELSE null END,
          t_ep_e1_item_cat_6_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_item_cat_6_ep_id%') THEN (select t_ep_e1_item_cat_6_ep_id FROM t_ep_e1_item_cat_6 WHERE E1_ITEM_CAT_6 = BRAND3) ELSE null END,
          t_ep_ebs_prod_cat_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_ebs_prod_cat_ep_id%') THEN (select t_ep_ebs_prod_cat_ep_id FROM t_ep_ebs_prod_cat WHERE EBS_PROD_CAT = PACK1) ELSE null END,
          t_ep_I_att_10_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_i_att_10_ep_id%') THEN (select t_ep_I_att_10_ep_id FROM t_ep_I_att_10 WHERE I_ATT_10 = PACK2) ELSE null END,
          t_ep_e1_item_cat_7_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_e1_item_cat_7_ep_id%') THEN (select t_ep_e1_item_cat_7_ep_id FROM t_ep_e1_item_cat_7 WHERE E1_ITEM_CAT_7 = PACK3) ELSE null END,
          t_ep_I_att_6_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_i_att_6_ep_id%') THEN (select t_ep_I_att_6_ep_id FROM t_ep_I_att_6 WHERE I_ATT_6 = TAX_CLASS_MAT) ELSE null END,
          t_ep_item_ep_id = CASE WHEN 1 = (select 1 from T_IMP_CONTRACT_TMP ct1 where ct1.batch_id = ct.batch_id and ct1.record_id = ct.record_id and ct1.id_field_value like '%t_ep_item_ep_id%') THEN (select t_ep_item_ep_id FROM t_ep_item WHERE item = MATERIAL) ELSE null END,
          T_EP_E1_CUST_RECP_EP_ID = CASE WHEN RECIPIENT IS NOT NULL THEN (select t_ep_e1_cust_cat_4_ep_id FROM t_ep_e1_cust_cat_4 WHERE E1_CUST_CAT_4 = RECIPIENT) ELSE null END;    

    -- Change to populate the t_ep_e1_cust_cat_4_ep_id if recipient value is there -- Mail from Andrew
    --UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT_TMP ct
    --SET t_ep_e1_cust_cat_4_ep_id = CASE WHEN RECIPIENT IS NOT NULL THEN (select t_ep_e1_cust_cat_4_ep_id FROM t_ep_e1_cust_cat_4 WHERE E1_CUST_CAT_4 = RECIPIENT) ELSE null END;
    ---
    COMMIT;
    --
    --v_status := 'End Update';
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
   WHEN OTHERS THEN
      BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),dbms_utility.format_error_stack,dbms_utility.format_error_backtrace);
            RAISE;
      END;
END PRC_VALIDATE_CONTRACTS;
--
PROCEDURE PRC_IMPORT_CONTRACTS
AS
    --
    v_prog_name                   VARCHAR2(100) := 'PRC_IMPORT_CONTRACTS';    
    v_new_cont_id                 NUMBER;
    v_new_sap_id                  NUMBER;
    v_oi_flag                     NUMBER := 0;
    v_method_status               NUMBER := 0;
    vc_payment_details            VARCHAR2(200);
    vc_recipient                  VARCHAR2(200);
    vc_tax_consideration          NUMBER;
    vc_rebate_details             VARCHAR2(200);
    --
    rec                           contract%ROWTYPE;
    seq                           access_seq%ROWTYPE;
    cgp                           contract_group%ROWTYPE;
    v_amortize_val                NUMBER;
    v_sequence_ep_id_desc         VARCHAR2(32767);
    v_ep_id_desc                  VARCHAR2(400);
    vn_user_id                    NUMBER;
    v_where_sql                   VARCHAR2(32767);
    vn_comp_type_id               NUMBER;
    vn_comp_type_seq_id           NUMBER;
    v_deal_type                   NUMBER;    
    v_amortize                    NUMBER;
    --
BEGIN
    --
    v_status := 'Start - Bulk Import';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
    select user_id into vn_user_id from user_id where user_name = 'EDI';
    --
    -- Part2 Create the Investment and Investment lines 
    -- Adding rebate details also in the grouping criteria
    FOR C1 IN (SELECT /*+ full(ct) parallel(ct,16) */ DISTINCT contract_id,investment_id_grp,investment_type,key_combination,to_date(investment_start_date,'DD/MM/YYYY') START_DATE,
                to_date(investment_end_date,'DD/MM/YYYY') END_DATE,recipient,REBATE_DETAILS
                FROM T_IMP_CONTRACT_TMP ct
                WHERE error_message IS NULL
                AND process_flag is null
                --AND ROWNUM  = 1
                --and contract_id = 265
                --and investment_id_grp = 1001
                --and investment_type = 'ZBG%'
                --and key_combination = 20
                --and investment_start_date = '27/09/2004'
                --and investment_end_date = '31/12/2025'
                GROUP BY contract_id,investment_id_grp,investment_type,key_combination,to_date(investment_start_date,'DD/MM/YYYY'),to_date(investment_end_date,'DD/MM/YYYY'),recipient,REBATE_DETAILS)
    LOOP
    --
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 0' || SYSDATE);
    v_status := 'Contract_ID-'||c1.contract_id||'-'||'investment_id_grp-'||c1.investment_id_grp||'-'||'investment_type-'||c1.investment_type||'-'||'key_combination-'||c1.key_combination||'-'||'START_DATE-'||c1.START_DATE||'-'||
    'END_DATE-'||c1.END_DATE||'-'||'recipient-'||c1.recipient||'-'||c1.REBATE_DETAILS;
    --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
      BEGIN
            --
            rec := null;
            seq := null;
            cgp := null;
            v_amortize := null;
            -- Create the Investment for the contract_group
            SELECT contract_seq.nextval INTO v_new_cont_id FROM dual;

            -- SELECT oi_flag INTO v_oi_flag FROM comp_type WHERE comp_type_id IN (SELECT comp_type_id FROM contract_group WHERE contract_group_id = C1.contract_id); -- Discussed with Luke
            -- SELECT COMP_TYPE_ID,COMP_TYPE_SEQ_ID INTO vn_comp_type_id,vn_comp_type_seq_id FROM comp_type_seq WHERE access_seq = C1.investment_type and access_seq_id = C1.key_combination;

            SELECT a.COMP_TYPE_ID,a.COMP_TYPE_SEQ_ID,b.oi_flag
            INTO   vn_comp_type_id,vn_comp_type_seq_id,v_oi_flag
            FROM   comp_type_seq a,
                     comp_type b 
            WHERE a.comp_type_id    = b.comp_type_id 
            AND   b.comp_type_code  = C1.investment_type 
            AND   a.access_seq_id   = C1.key_combination;
            
            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 1' || SYSDATE);

            SELECT MAX(payment_details),MAX(T_EP_E1_CUST_RECP_EP_ID),MAX(TAX_CONSIDERATION),MAX(REBATE_DETAILS) 
            INTO   vc_payment_details,vc_recipient,vc_tax_consideration,vc_rebate_details
            FROM   T_IMP_CONTRACT_TMP
            WHERE  contract_id          = C1.contract_id
            AND    investment_id_grp = C1.investment_id_grp 
            AND    investment_type      = C1.investment_type
            AND    key_combination      = C1.key_combination
            AND    to_date(investment_start_date,'DD/MM/YYYY') = C1.START_DATE 
            AND    to_date(investment_end_date,'DD/MM/YYYY')   = C1.END_DATE
            AND    nvl(recipient,'XYZ') = NVL(C1.recipient,'XYZ')
            AND    nvl(REBATE_DETAILS,'XYZ') = NVL(C1.REBATE_DETAILS,'XYZ');
            --
            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 2' || SYSDATE);
            --
            INSERT INTO CONTRACT(
            contract_id                  ,
            contract_code                ,
            contract_desc                ,
            is_fictive                   ,
            new_contract_group_id        ,
            comp_type_seq_id             ,
            comp_type_id                 ,
            rebate_value                 ,
            payment_frequency            ,
            recipient_cl3                ,
            recipient_cl4                ,
            recipient                    ,
            recipient_sales_office       ,
            payment_comments             ,
            tax_consideration            ,
            investment_details           ,
            created_user
            )
            SELECT v_new_cont_id         ,
            v_new_cont_id                , 
            TO_CHAR(C1.start_date,'yyyy-mm-dd')||' : '||v_new_cont_id,
            0                            ,
            C1.contract_id               ,
            --contract_group_id          ,
            vn_comp_type_seq_id          ,                                    -- 
            vn_comp_type_id              ,                                    -- 
            null                         ,                                    -- As discussed
            5                            ,                                    -- As discussed            
            NULL                         ,                                    -- As discussed
            NULL                         ,                                    -- As discussed    
            decode(v_oi_flag,1,null,vc_recipient),
            NULL                         ,                                    -- As discussed
            vc_payment_details           ,
            vc_tax_consideration         ,
            vc_rebate_details            ,
            vn_user_id                                                        -- As discussed - EDI
            FROM dual;
            --WHERE contract_group_id = C1.contract_id;
            ---
            --COMMIT;
            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 3' || SYSDATE);

            UPDATE contract_group
            SET rebate_value = null,  last_update_date = sysdate
            WHERE contract_group_id = C1.contract_id;

            --COMMIT;

            --Get all Contract Row Data
            SELECT * INTO rec FROM contract WHERE contract_id = v_new_cont_id;

            SELECT * INTO seq FROM access_seq WHERE access_seq_code = c1.key_combination;

            SELECT * INTO cgp FROM contract_group WHERE contract_group_id = rec.new_contract_group_id;

            SELECT pval INTO v_amortize_val FROM sys_params WHERE pname = 'cont_lump_amortize_amt'; 
            --
            /*Delete Fictive contract Lines - Created When Contract is created on its own*/
            DELETE FROM contract_line WHERE contract_group_id = rec.new_contract_group_id AND IS_FICTIVE = 1;
            --COMMIT;

            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 4' || SYSDATE);
            SELECT contract_payment_seq.nextval INTO v_new_sap_id FROM dual;

            --- Create SAP Investment ID'S
            INSERT INTO CONTRACT_PAYMENT(contract_payment_id,contract_payment_code,is_fictive,contract_payment_desc,created_user)                   -- sap_investment_id
            VALUES (v_new_sap_id,v_new_sap_id,0,to_char(C1.start_date,'yyyy-mm-dd')||' : '||v_new_sap_id,vn_user_id);            

            --COMMIT;
            
            -- Update SAP Investment ID in the TMP table
            UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT_TMP ct
            SET SAP_INVESTMENT_ID                  = v_new_sap_id
            WHERE contract_id                 = C1.contract_id
            AND investment_id_grp             = C1.investment_id_grp 
            AND investment_type               = C1.investment_type
            AND key_combination               = C1.key_combination
            AND to_date(investment_start_date,'DD/MM/YYYY') = C1.START_DATE 
            AND to_date(investment_end_date,'DD/MM/YYYY')   = C1.END_DATE
            AND NVL(recipient,'XYZ')           = NVL(C1.recipient,'XYZ')
            AND nvl(REBATE_DETAILS,'XYZ')     = NVL(C1.REBATE_DETAILS,'XYZ');

            --COMMIT;
            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 5' || SYSDATE);
            INSERT INTO contract_line(
            contract_line_id,
            contract_line_code, 
            contract_line_desc,
            contract_group_id,
            contract_id,
            contract_payment_id,
            contract_stat_id,
            is_fictive,
            access_seq_id,
            comp_type_id,
            comp_type_seq_id,
            created_user,
            start_date,
            end_date,
            t_cont_lvl1_ep_id,
            t_cont_lvl2_ep_id,
            t_cont_lvl3_ep_id,
            t_cont_lvl4_ep_id,
            t_cont_lvl5_ep_id,
            t_cont_lvl_ep_id,
            prop_factor,                                    
            t_ep_p4_ep_id,                                    -- sales_org                --
            t_ep_p3_ep_id,                                    -- dist_channel                --
            t_ep_p2_ep_id,                                    -- division
            t_ep_e1_cust_city_ep_id,                          -- customer_level2
            t_ep_ebs_customer_ep_id,                          -- customer_level3
            t_ep_ebs_account_ep_id,                           -- customer_level4            
            t_ep_e1_cust_cat_4_ep_id,                         -- customer_level
            t_ep_l_att_6_ep_id,                               -- sales_office
            t_ep_e1_cust_cat_2_ep_id,                         -- tier
            t_ep_e1_cust_ctry_ep_id,                          -- cust_pricing_procedure    
            t_ep_e1_item_cat_4_ep_id,                         -- brand2
            t_ep_e1_item_cat_6_ep_id,                         -- brand3
            t_ep_ebs_prod_cat_ep_id,                          -- pack1
            t_ep_I_att_10_ep_id,                              -- pack2
            t_ep_e1_item_cat_7_ep_id,                         -- pack3
            t_ep_i_att_6_ep_id,                               -- tax_class_mat
            t_ep_item_ep_id,                                  -- material
            lumpsum,
            case_deal_amt,
            case_deal_amt_l2,
            case_deal_amt_l3,
            case_deal_perc,
            case_deal_perc_l2,
            case_deal_perc_l3,
            tier_max_val1,
            tier_max_val2,
            tier_max_val3,
            rr_cl_amortize
            )
            SELECT /*+ full(t) parallel(t,16) */ 
            contract_line_seq.nextval,
            contract_line_seq.nextval,
            to_char(to_date(t.investment_start_date,'DD/MM/YYYY'),'yyyy-mm-dd') ||' : ' ||contract_line_seq.nextval,
            rec.new_contract_group_id,                        -- contract_group
            v_new_cont_id,                                    -- contract
            t.sap_investment_id,                              -- sap_investment_id
            4,                                                -- approved
            0,
            seq.access_seq_id,
            rec.comp_type_id,
            rec.comp_type_seq_id,
            vn_user_id,
            to_date(t.investment_start_date,'DD/MM/YYYY'),
            to_date(t.investment_end_date,'DD/MM/YYYY'),
            cgp.t_cont_lvl1_ep_id,
            cgp.t_cont_lvl2_ep_id,
            cgp.t_cont_lvl3_ep_id,
            cgp.t_cont_lvl4_ep_id,
            cgp.t_cont_lvl5_ep_id,
            cgp.t_cont_lvl_ep_id,
            null,                                            -- prop_factor                -- Ask what value ??
            t.t_ep_p4_ep_id,                                 -- SALES_ORG                -- 
            t.t_ep_p3_ep_id,                                 -- DIST_CHANNEL
            t.t_ep_p2_ep_id,                                 -- DIVISION
            t.t_ep_e1_cust_city_ep_id,                       -- CUSTOMER_LEVEL2
            t.t_ep_ebs_customer_ep_id,                       -- CUSTOMER_LEVEL3
            t.t_ep_ebs_account_ep_id,                        -- CUSTOMER_LEVEL4            
            t.t_ep_e1_cust_cat_4_ep_id,                      -- CUSTOMER_LEVEL
            t.t_ep_l_att_6_ep_id,                            -- SALES_OFFICE
            t.t_ep_e1_cust_cat_2_ep_id,                      -- TIER
            t_ep_e1_cust_ctry_ep_id,                         -- CUST_PRICING_PROCEDURE
            t.t_ep_e1_item_cat_4_ep_id,                      -- BRAND2
            t.t_ep_e1_item_cat_6_ep_id,                      -- BRAND3
            t.t_ep_ebs_prod_cat_ep_id,                       -- PACK1
            t.t_ep_I_att_10_ep_id,                           -- PACK2
            t.t_ep_e1_item_cat_7_ep_id,                      -- PACK3
            t.t_ep_I_att_6_ep_id,                            -- TAX_CLASS_MAT
            t.t_ep_item_ep_id,
            t.lumpsum,
            t.case_deal_dollar_lv1,
            t.case_deal_dollar_lv2,
            t.case_deal_dollar_lv3,
            t.case_deal_perc_lv1,
            t.case_deal_perc_lv2,
            t.case_deal_perc_lv3,
            --case when t.case_deal_perc_lv1 is not null then t.case_deal_perc_lv1/1000 else t.case_deal_perc_lv1 end,    --t.case_deal_perc_lv1,
            --case when t.case_deal_perc_lv2 is not null then t.case_deal_perc_lv2/1000 else t.case_deal_perc_lv2 end,    --t.case_deal_perc_lv2,
            --case when t.case_deal_perc_lv3 is not null then t.case_deal_perc_lv3/1000 else t.case_deal_perc_lv3 end,    --t.case_deal_perc_lv3,
            t.min_vol_lv1,
            t.min_vol_lv2,
            t.min_vol_lv3,
            case when (rec.comp_type_id = 7 AND v_amortize_val <= NVL(t.lumpsum,0)) THEN 1 ELSE 0 END                -- rr_cl_amortize
            FROM T_IMP_CONTRACT_TMP t
            WHERE contract_id               = C1.contract_id
            AND investment_id_grp           = C1.investment_id_grp 
            AND investment_type             = C1.investment_type
            AND key_combination             = C1.key_combination
            AND to_date(investment_start_date,'DD/MM/YYYY') = C1.START_DATE 
            AND to_date(investment_end_date,'DD/MM/YYYY')   = C1.END_DATE
            AND nvl(recipient,'XYZ') = NVL(C1.recipient,'XYZ')
            AND nvl(REBATE_DETAILS,'XYZ') = NVL(C1.REBATE_DETAILS,'XYZ');
            
            IF 1 = 1 THEN    -- Only commit when all is done.. 
                --
                UPDATE /*+ full(ct) parallel(ct,16) */ T_IMP_CONTRACT_TMP ct
                SET PROCESS_FLAG = 1
                WHERE contract_id               = C1.contract_id
                AND investment_id_grp           = C1.investment_id_grp 
                AND investment_type             = C1.investment_type
                AND key_combination             = C1.key_combination
                AND to_date(investment_start_date,'DD/MM/YYYY') = C1.START_DATE 
                AND to_date(investment_end_date,'DD/MM/YYYY')   = C1.END_DATE
                AND NVL(recipient,'XYZ')        = NVL(C1.recipient,'XYZ')
                AND nvl(REBATE_DETAILS,'XYZ')  = NVL(C1.REBATE_DETAILS,'XYZ');
                --
                COMMIT;
                --
            END IF;
            -- 
            --v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, 'Reached - 6' || SYSDATE);
            --
            /*
            SELECT deal_type INTO v_deal_type FROM comp_type WHERE comp_type_id = rec.comp_type_id;
            ----Fixed Fund 
            IF v_deal_type = 3 THEN
                 ----Set Amortization rules if Sponsorship and is over threshold
                 IF v_amortize_val <= nvl(rec.rebate_value,0) AND rec.comp_type_id = 7 THEN
                      v_amortize := 1;
                 END IF;
                 --
                 UPDATE contract_line
                 SET rr_cl_amortize = v_amortize,last_update_date = sysdate 
                 WHERE contract_id = v_new_cont_id;
                 --
                 COMMIT;
                 --
            END IF;        
            --
            */
      EXCEPTION
         WHEN OTHERS THEN
            BEGIN
               v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error for Record: ' || v_status,TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),dbms_utility.format_error_stack,dbms_utility.format_error_backtrace);
               ROLLBACK;
            END;
      END;        
   END LOOP; -- outer loop
    -- Merge the Process Records with T_IMP_CONTRACT
    MERGE INTO /*+ full(a) parallel(a,16) */ T_IMP_CONTRACT a 
    USING (SELECT BATCH_ID,RECORD_ID,PROCESS_FLAG FROM T_IMP_CONTRACT_TMP
            WHERE PROCESS_FLAG = 1) b
    ON (a.batch_id = b.batch_id and
      a.record_id = b.record_id)
    WHEN MATCHED THEN
    UPDATE SET a.process_flag = b.process_flag,last_update_date = sysdate,last_updated_by = vn_user_id;    

    COMMIT;
    --
    v_status := 'End - Bulk Import';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),dbms_utility.format_error_stack,dbms_utility.format_error_backtrace);
            RAISE;
        END;
END PRC_IMPORT_CONTRACTS;
--

PROCEDURE PRC_ALL_POST_PROCESSING
AS
    --
    v_prog_name   VARCHAR2 (100) := 'PRC_ALL_POST_PROCESSING';
    --
BEGIN
    --
    v_status := 'Start - Post Processing';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    -- Part3 Create the CSV for all error records From T_IMP_CONTRACT

    -- Part4 Archive the batch records in the archive table
    INSERT INTO T_IMP_CONTRACT_ARCHIVE (batch_id,record_id,contract_id,investment_id_grp,investment_type,key_combination,sales_org,dist_channel,division,customer_level2,
                                            customer_level3,customer_level4,customer_level,sales_office,tier,cust_pricing_procedure,brand2,brand3,pack1,pack2,pack3,tax_class_mat,material,
                                            investment_start_date,investment_end_date,case_deal_dollar_lv1,case_deal_dollar_lv2,case_deal_dollar_lv3,case_deal_perc_lv1,
                                            case_deal_perc_lv2,case_deal_perc_lv3,min_vol_lv1,min_vol_lv2,min_vol_lv3,lumpsum,rebate_details,payment_details,
                                            recipient,tax_consideration,ID_FIELD_VALUE,error_message,process_flag,created_by,creation_date,last_updated_by,last_update_date)
    SELECT /*+ full(ct) parallel(ct,16) */  
    batch_id,record_id,contract_id,investment_id_grp,investment_type,key_combination,sales_org,dist_channel,division,customer_level2,
                                            customer_level3,customer_level4,customer_level,sales_office,tier,cust_pricing_procedure,brand2,brand3,pack1,pack2,pack3,tax_class_mat,material,
                                            investment_start_date,investment_end_date,case_deal_dollar_lv1,case_deal_dollar_lv2,case_deal_dollar_lv3,case_deal_perc_lv1,
                                            case_deal_perc_lv2,case_deal_perc_lv3,min_vol_lv1,min_vol_lv2,min_vol_lv3,lumpsum,rebate_details,payment_details,
                                            recipient,tax_consideration,ID_FIELD_VALUE,error_message,process_flag,created_by,creation_date,last_updated_by,last_update_date
    FROM T_IMP_CONTRACT ct;

    DYNAMIC_DDL('TRUNCATE TABLE T_IMP_CONTRACT');

    COMMIT;
    --
    v_status := 'End - Post Processing';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    --
EXCEPTION
   WHEN OTHERS THEN
      BEGIN
         v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),dbms_utility.format_error_stack,dbms_utility.format_error_backtrace);
         RAISE;
      END;
END PRC_ALL_POST_PROCESSING;
--
    
END PKG_CONTRACTS_BULK_IMPORT;

/
