--------------------------------------------------------
--  DDL for Package Body PKG_ACCRUAL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_ACCRUAL" 
AS
/******************************************************************************
   NAME:         PKG_ACCRUAL  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

   ******************************************************************************/

  PROCEDURE prc_clear_accrual(p_start_date  DATE,
                              p_end_date    DATE,
                              p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_CLEAR_ACCRUAL
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
--   v_weeks          NUMBER := 52;
  -- v_weeks          NUMBER := 70;
   --v_weeks          NUMBER := 96;--Changes added to consider the Accrual synhup for year 2016
   v_weeks          NUMBER := 104;--Changes added to consider the Accrual synhup for Last 2 years



   v_eng_profile    NUMBER := 1;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_CLEAR_ACCRUAL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss')
    INTO max_sales_date
    FROM DUAL;


   check_and_drop('rr_promos_clear_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_clear_t
               TABLESPACE ts_temp_tables parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.scenario_id IN (22, 262, 162)
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND NVL(ps.rr_null_accrual_pd, 0) = 1 AND NVL(pt.rr_null_accrual_pd, 0) = 1
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               ';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_clear_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str := 'MERGE INTO promotion_data pd
               USING(SELECT /*+ FULL(pd) PARALLEL(pd, 24) */ pd.promotion_id, pd.item_id, pd.location_id, pd.sales_date,
               0 rr_accrual_vol_base_pd,
               0 rr_accrual_vol_incr_pd,
               0 rr_accrual_eng_incr_pd,
               0 rr_accrual_vol_pd,
               0 rr_supply_vol_base_pd,
               0 rr_supply_vol_incr_pd,
               0 rr_supply_eng_incr_pd,
               0 rr_supply_vol_pd,
               0 rr_accrual_cd_pd,
               0 rr_accrual_coop_pd,
               0 rr_accrual_hf_cd_pd,
               0 rr_accrual_hf_coop_pd,
               0 rr_accrual_base_commit,
               0 rr_accrual_incr_commit,
               0 rr_accrual_eng_incr_commit,
               0 rr_accrual_vol_commit,
               0 rr_accrual_cd_commit,
               0 rr_accrual_coop_commit,
               0 rr_pl_cd_pd,
               0 rr_pl_coop_pd,
               0 rr_pl_hf_cd_pd,
               0 rr_pl_hf_coop_pd,
               0 rr_accrual_hf_cd_commit,
               0 rr_accrual_hf_coop_commit
               FROM rr_promos_clear_t rpt,
               promotion_data pd
               WHERE 1 = 1
               AND rpt.promotion_id = pd.promotion_id) pd1
               ON(pd.promotion_id = pd1.promotion_id
               AND pd.item_id = pd1.item_id
               AND pd.location_id = pd1.location_id
               AND pd.sales_date = pd1.sales_date)
               WHEN MATCHED THEN
               UPDATE SET pd.rr_accrual_vol_base_pd = pd1.rr_accrual_vol_base_pd,
               pd.rr_accrual_vol_incr_pd = pd1.rr_accrual_vol_incr_pd,
               pd.rr_accrual_eng_incr_pd = pd1.rr_accrual_eng_incr_pd,
               pd.rr_accrual_vol_pd = pd1.rr_accrual_vol_pd,
               pd.rr_supply_vol_base_pd = pd1.rr_supply_vol_base_pd,
               pd.rr_supply_vol_incr_pd = pd1.rr_supply_vol_incr_pd,
               pd.rr_supply_eng_incr_pd = pd1.rr_supply_eng_incr_pd,
               pd.rr_supply_vol_pd = pd1.rr_supply_vol_pd,
               pd.rr_accrual_cd_pd = pd1.rr_accrual_cd_pd,
               pd.rr_accrual_coop_pd = pd1.rr_accrual_coop_pd,
               pd.rr_accrual_hf_cd_pd = pd1.rr_accrual_hf_cd_pd,
               pd.rr_accrual_hf_coop_pd = pd1.rr_accrual_hf_coop_pd,
               pd.rr_accrual_base_commit = pd1.rr_accrual_base_commit,
               pd.rr_accrual_incr_commit = pd1.rr_accrual_incr_commit,
               pd.rr_accrual_eng_incr_commit = pd1.rr_accrual_eng_incr_commit,
               pd.rr_accrual_vol_commit = pd1.rr_accrual_vol_commit,
               pd.rr_accrual_cd_commit = pd1.rr_accrual_cd_commit,
               pd.rr_accrual_coop_commit = pd1.rr_accrual_coop_commit,
               pd.rr_pl_cd_pd = pd1.rr_pl_cd_pd,
               pd.rr_pl_coop_pd = pd1.rr_pl_coop_pd,
               pd.rr_pl_hf_cd_pd = pd1.rr_pl_hf_cd_pd,
               pd.rr_pl_hf_coop_pd = pd1.rr_pl_hf_coop_pd,
               pd.rr_accrual_hf_cd_commit = pd1.rr_accrual_hf_cd_commit,
               pd.rr_accrual_hf_coop_commit = pd1.rr_accrual_hf_coop_commit,
               pd.last_update_date = SYSDATE';

    dynamic_ddl(sql_str);
    COMMIT;

    sql_str := 'MERGE INTO accrual_data ad
               USING(SELECT /*+ FULL(ad) PARALLEL(ad, 24) */ ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
               0 rr_accrual_vol_base_ad,
               0 rr_accrual_vol_incr_ad,
               0 rr_accrual_eng_incr_ad,
               0 rr_accrual_vol_ad,
               0 rr_supply_vol_base_ad,
               0 rr_supply_vol_incr_ad,
               0 rr_supply_eng_incr_ad,
               0 rr_supply_vol_ad,
               0 rr_accrual_cd_ad,
               0 rr_accrual_coop_ad,
               0 rr_accrual_hf_cd_ad,
               0 rr_accrual_hf_coop_ad,
               0 rr_accrual_os_cd_ad,
               0 rr_accrual_os_coop_ad,
               0 rr_accrual_os_hf_cd_ad,
               0 rr_accrual_os_hf_coop_ad,
               0 rr_pl_cd_ad,
               0 rr_pl_coop_ad,
               0 rr_pl_hf_cd_ad,
               0 rr_pl_hf_coop_ad,
               0 rr_accrual_cd_ad_gl,
               0 rr_accrual_coop_ad_gl,
               0 rr_accrual_os_cd_ad_gl,
               0 rr_accrual_os_coop_ad_gl,
               0 rr_accrual_hf_cd_ad_gl,
               0 rr_accrual_hf_coop_ad_gl,
               0 rr_accrual_os_hf_cd_ad_gl,
               0 rr_accrual_os_hf_coop_ad_gl,
               -rr_accrual_cd_ad_gl rr_accrual_cd_post_gl,
               -rr_accrual_coop_ad_gl rr_accrual_coop_post_gl,
               -rr_accrual_os_cd_ad_gl rr_accrual_os_cd_post_gl,
               -rr_accrual_os_coop_ad_gl rr_accrual_os_coop_post_gl,
               -rr_accrual_hf_cd_ad_gl rr_accrual_hf_cd_post_gl,
               -rr_accrual_hf_coop_ad_gl rr_accrual_hf_coop_post_gl,
               -rr_accrual_os_hf_cd_ad_gl rr_accrual_os_hf_cd_post_gl,
               -rr_accrual_os_hf_coop_ad_gl rr_accrual_os_hf_coop_post_gl
               FROM rr_promos_clear_t rpt,
               accrual_data ad
               WHERE 1 = 1
               AND rpt.promotion_id = ad.accrual_id) ad1
               ON(ad.accrual_id = ad1.accrual_id
               AND ad.item_id = ad1.item_id
               AND ad.location_id = ad1.location_id
               AND ad.sales_date = ad1.sales_date)
               WHEN MATCHED THEN
               UPDATE SET ad.rr_accrual_vol_ad = ad1.rr_accrual_vol_ad,
               ad.rr_accrual_cd_ad = ad1.rr_accrual_cd_ad,
               ad.rr_accrual_coop_ad = ad1.rr_accrual_coop_ad,
               ad.rr_accrual_os_cd_ad = ad1.rr_accrual_os_cd_ad,
               ad.rr_accrual_os_coop_ad = ad1.rr_accrual_os_coop_ad,
               ad.rr_accrual_hf_cd_ad = ad1.rr_accrual_hf_cd_ad,
               ad.rr_accrual_hf_coop_ad = ad1.rr_accrual_hf_coop_ad,
               ad.rr_accrual_os_hf_cd_ad = ad1.rr_accrual_os_hf_cd_ad,
               ad.rr_accrual_os_hf_coop_ad = ad1.rr_accrual_os_hf_coop_ad,
               ad.rr_accrual_cd_ad_gl = ad1.rr_accrual_cd_ad_gl,
               ad.rr_accrual_coop_ad_gl = ad1.rr_accrual_coop_ad_gl,
               ad.rr_accrual_os_cd_ad_gl = ad1.rr_accrual_os_cd_ad_gl,
               ad.rr_accrual_os_coop_ad_gl = ad1.rr_accrual_os_coop_ad_gl,
               ad.rr_accrual_hf_cd_ad_gl = ad1.rr_accrual_hf_cd_ad_gl,
               ad.rr_accrual_hf_coop_ad_gl = ad1.rr_accrual_hf_coop_ad_gl,
               ad.rr_accrual_os_hf_cd_ad_gl = ad1.rr_accrual_os_hf_cd_ad_gl,
               ad.rr_accrual_os_hf_coop_ad_gl = ad1.rr_accrual_os_hf_coop_ad_gl,
               ad.rr_accrual_cd_post_gl = ad1.rr_accrual_cd_post_gl,
               ad.rr_accrual_coop_post_gl = ad1.rr_accrual_coop_post_gl,
               ad.rr_accrual_os_cd_post_gl = ad1.rr_accrual_os_cd_post_gl,
               ad.rr_accrual_os_coop_post_gl = ad1.rr_accrual_os_coop_post_gl,
               ad.rr_accrual_hf_cd_post_gl = ad1.rr_accrual_hf_cd_post_gl,
               ad.rr_accrual_hf_coop_post_gl = ad1.rr_accrual_hf_coop_post_gl,
               ad.rr_accrual_os_hf_cd_post_gl = ad1.rr_accrual_os_hf_cd_post_gl,
               ad.rr_accrual_os_hf_coop_post_gl = ad1.rr_accrual_os_hf_coop_post_gl,
               ad.last_update_date = SYSDATE';

    dynamic_ddl(sql_str);
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

  PROCEDURE prc_set_mdp_allocation(p_start_date  DATE,
                                   p_end_date    DATE)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_SET_MDP_ALLOCATION
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;

   sql_str          VARCHAR2 (20000);

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_SET_MDP_ALLOCATION';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    MERGE  /*+ full(m) parallel(m,24) */ INTO mdp_matrix m
      USING (SELECT item_id, location_id
               FROM mdp_matrix
              WHERE dem_stream = 0
                AND (allocation_wk0 IS NULL OR allocation_wk0 <> 1)) m1
      ON (m.item_id = m1.item_id AND m.location_id = m1.location_id)
      WHEN MATCHED THEN
         UPDATE
            SET m.allocation_wk0 = 1, m.allocation_wk1 = 0,
                m.allocation_wk2 = 0, m.allocation_wk3 = 0,
                m.allocation_wk4 = 0, m.allocation_wk5 = 0,
                m.allocation_wk6 = 0
         ;
    COMMIT;
    update promotion_matrix
set rr_promo_offset = 0, rr_sync_wk0 = 1, rr_sync_wk1 = 0, rr_sync_wk2 = 0, rr_sync_wk3 = 0, rr_sync_wk4 = 0, rr_sync_wk5 = 0,
rr_alloc_wk0 = 1, rr_alloc_wk1 = 0, rr_alloc_wk2 = 0, rr_alloc_wk3 = 0, rr_alloc_wk4 = 0, rr_alloc_wk5 = 0
where promotion_id in (select promotion_id from promotion where original_promo_reference like '%XOPLFTX%')
and (nvl(rr_promo_offset, 1) <> 0
or nvl(rr_sync_wk0, 0) <> 1
or nvl(rr_sync_wk1, 1) <> 0
or nvl(rr_sync_wk2, 1) <> 0
or nvl(rr_sync_wk3, 1) <> 0
or nvl(rr_sync_wk4, 1) <> 0
or nvl(rr_sync_wk5, 1) <> 0
or nvl(rr_alloc_wk0, 0) <> 1
or nvl(rr_alloc_wk1, 1) <> 0
or nvl(rr_alloc_wk2, 1) <> 0
or nvl(rr_alloc_wk3, 1) <> 0
or nvl(rr_alloc_wk4, 1) <> 0
or nvl(rr_alloc_wk5, 1) <> 0
);


/*    UPDATE promotion_stat
    SET rr_sync_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END,
    rr_sync_vol_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END;

    UPDATE promotion_type
    SET rr_sync_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END,
    rr_sync_vol_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END; */

    COMMIT;

   check_and_drop('rr_promos_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_t
               TABLESPACE ts_temp_tables parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND p.promotion_id = pdt.promotion_id
               AND NVL(p.original_promo_reference, ''ABCSDFEDA'') NOT LIKE ''%XOPLFTX%''
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') ';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str := 'MERGE INTO promotion_matrix pm
               USING(SELECT /*+ FULL(pm) PARALLEL(pm, 16) FULL(m) PARALLEL(m, 16) */ pm.promotion_id, pm.item_id, pm.location_id,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk0 IS NULL
                         THEN NVL(m.allocation_wk0, 0)
                         ELSE pm.rr_alloc_wk0
                         END
                    ELSE 1 END rr_alloc_wk0,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk1 IS NULL
                         THEN NVL(m.allocation_wk1, 0)
                         ELSE pm.rr_alloc_wk1
                         END
                    ELSE 0 END rr_alloc_wk1,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk2 IS NULL
                         THEN NVL(m.allocation_wk2, 0)
                         ELSE pm.rr_alloc_wk2
                         END
                    ELSE 0 END rr_alloc_wk2,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk3 IS NULL
                         THEN NVL(m.allocation_wk3, 0)
                         ELSE pm.rr_alloc_wk3
                         END
                    ELSE 0 END rr_alloc_wk3,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk4 IS NULL
                         THEN NVL(m.allocation_wk4, 0)
                         ELSE pm.rr_alloc_wk4
                         END
                    ELSE 0 END rr_alloc_wk4,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk5 IS NULL
                         THEN NVL(m.allocation_wk5, 0)
                         ELSE pm.rr_alloc_wk5
                         END
                    ELSE 0 END rr_alloc_wk5,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_ms_exfact_pos IN (0, 1) THEN
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk6 IS NULL
                         THEN NVL(m.allocation_wk6, 0)
                         ELSE pm.rr_alloc_wk6
                         END
                    ELSE 0 END rr_alloc_wk6,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 1
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk0 IS NULL
                    THEN NVL(m.allocation_wk0, 0)
                    ELSE pm.rr_sync_wk0
                    END rr_sync_wk0,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk1 IS NULL
                    THEN NVL(m.allocation_wk1, 0)
                    ELSE pm.rr_sync_wk1
                    END rr_sync_wk1,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk2 IS NULL
                    THEN NVL(m.allocation_wk2, 0)
                    ELSE pm.rr_sync_wk2
                    END rr_sync_wk2,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk3 IS NULL
                    THEN NVL(m.allocation_wk3, 0)
                    ELSE pm.rr_sync_wk3
                    END rr_sync_wk3,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk4 IS NULL
                    THEN NVL(m.allocation_wk4, 0)
                    ELSE pm.rr_sync_wk4
                    END rr_sync_wk4,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk5 IS NULL
                    THEN NVL(m.allocation_wk5, 0)
                    ELSE pm.rr_sync_wk5
                    END rr_sync_wk5,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk6 IS NULL
                    THEN NVL(m.allocation_wk6, 0)
                    ELSE pm.rr_sync_wk6
                    END rr_sync_wk6,
               CASE WHEN pt.rr_ms_exfact_pos = 2
                    THEN 0
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk6 IS NULL) AND NVL(m.allocation_wk6, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 6 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 6 END
                    WHEN pm.rr_sync_wk6 > 0
                    THEN 6
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk5 IS NULL) AND NVL(m.allocation_wk5, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 5 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 5 END
                    WHEN pm.rr_sync_wk5 > 0
                    THEN 5
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk4 IS NULL) AND NVL(m.allocation_wk4, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 4 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 4 END
                    WHEN pm.rr_sync_wk4 > 0
                    THEN 4
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk3 IS NULL) AND NVL(m.allocation_wk3, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 3 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 3 END
                    WHEN pm.rr_sync_wk3 > 0
                    THEN 3
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk2 IS NULL) AND NVL(m.allocation_wk2, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 2 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 2 END
                    WHEN pm.rr_sync_wk2 > 0
                    THEN 2
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk1 IS NULL) AND NVL(m.allocation_wk1, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 1 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 1 END
                    WHEN pm.rr_sync_wk1 > 0
                    THEN 1
                    ELSE 0
               END rr_promo_offset,
               NVL(latt6.rr_accrual_adjust, 0) rr_accrual_adjust
               FROM rr_promos_t rpt,
               promotion p,
               promotion_matrix pm,
               mdp_matrix m,
               promotion_stat ps,
               promotion_type pt,
               t_ep_ebs_customer latt6
               WHERE 1 = 1
               AND rpt.promotion_id = p.promotion_id
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               --AND (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0)
               /*AND (CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk0, 0) ELSE 1 END <> NVL(pm.rr_alloc_wk0, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk1, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk1, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk2, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk2, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk3, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk3, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk4, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk4, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk5, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk5, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk6, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk6, -99999)
                    OR NVL(m.allocation_wk0, 0) <> NVL(pm.rr_sync_wk0, -99999)
                    OR NVL(m.allocation_wk1, 0) <> NVL(pm.rr_sync_wk1, -99999)
                    OR NVL(m.allocation_wk2, 0) <> NVL(pm.rr_sync_wk2, -99999)
                    OR NVL(m.allocation_wk3, 0) <> NVL(pm.rr_sync_wk3, -99999)
                    OR NVL(m.allocation_wk4, 0) <> NVL(pm.rr_sync_wk4, -99999)
                    OR NVL(m.allocation_wk5, 0) <> NVL(pm.rr_sync_wk5, -99999)
                    OR NVL(m.allocation_wk6, 0) <> NVL(pm.rr_sync_wk6, -99999)
                    --OR NVL(latt6.rr_accrual_adjust, 0) <> NVL(pm.rr_accrual_adjust, -1)
                    )*/
               AND p.promotion_id = pm.promotion_id
               AND pm.item_id = m.item_id
               AND pm.location_id = m.location_id
               AND latt6.t_ep_ebs_customer_ep_id = m.t_ep_ebs_customer_ep_id) pm1
               ON(pm.promotion_id = pm1.promotion_id
               AND pm.item_id = pm1.item_id
               AND pm.location_id = pm1.location_id)
               WHEN MATCHED THEN
               UPDATE SET pm.rr_alloc_wk0 = pm1.rr_alloc_wk0,
               pm.rr_alloc_wk1 = pm1.rr_alloc_wk1,
               pm.rr_alloc_wk2 = pm1.rr_alloc_wk2,
               pm.rr_alloc_wk3 = pm1.rr_alloc_wk3,
               pm.rr_alloc_wk4 = pm1.rr_alloc_wk4,
               pm.rr_alloc_wk5 = pm1.rr_alloc_wk5,
               pm.rr_alloc_wk6 = pm1.rr_alloc_wk6,
               pm.rr_sync_wk0 = pm1.rr_sync_wk0,
               pm.rr_sync_wk1 = pm1.rr_sync_wk1,
               pm.rr_sync_wk2 = pm1.rr_sync_wk2,
               pm.rr_sync_wk3 = pm1.rr_sync_wk3,
               pm.rr_sync_wk4 = pm1.rr_sync_wk4,
               pm.rr_sync_wk5 = pm1.rr_sync_wk5,
               pm.rr_sync_wk6 = pm1.rr_sync_wk6,
               --pm.rr_accrual_adjust = pm1.rr_accrual_adjust,
               pm.rr_promo_offset = pm1.rr_promo_offset
               WHERE NVL(pm.rr_alloc_wk0, 0) <> NVL(pm1.rr_alloc_wk0, -9999)
               OR NVL(pm.rr_alloc_wk1, 0) <> NVL(pm1.rr_alloc_wk1, -9999)
               OR NVL(pm.rr_alloc_wk2, 0) <> NVL(pm1.rr_alloc_wk2, -9999)
               OR NVL(pm.rr_alloc_wk3, 0) <> NVL(pm1.rr_alloc_wk3, -9999)
               OR NVL(pm.rr_alloc_wk4, 0) <> NVL(pm1.rr_alloc_wk4, -9999)
               OR NVL(pm.rr_alloc_wk5, 0) <> NVL(pm1.rr_alloc_wk5, -9999)
               OR NVL(pm.rr_alloc_wk6, 0) <> NVL(pm1.rr_alloc_wk6, -9999)
               OR NVL(pm.rr_sync_wk0, 0) <> NVL(pm1.rr_sync_wk0, -9999)
               OR NVL(pm.rr_sync_wk1, 0) <> NVL(pm1.rr_sync_wk1, -9999)
               OR NVL(pm.rr_sync_wk2, 0) <> NVL(pm1.rr_sync_wk2, -9999)
               OR NVL(pm.rr_sync_wk3, 0) <> NVL(pm1.rr_sync_wk3, -9999)
               OR NVL(pm.rr_sync_wk4, 0) <> NVL(pm1.rr_sync_wk4, -9999)
               OR NVL(pm.rr_sync_wk5, 0) <> NVL(pm1.rr_sync_wk5, -9999)
               OR NVL(pm.rr_sync_wk6, 0) <> NVL(pm1.rr_sync_wk6, -9999)
               OR NVL(pm.rr_promo_offset, 0) <> NVL(pm1.rr_promo_offset, -9999)
               ';

    dynamic_ddl(sql_str);
    COMMIT;

    v_status := 'End ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

  PROCEDURE prc_calc_accrual(p_start_date  DATE,
                             p_end_date    DATE,
                             p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_CALC_ACCRUAL
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (32767);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_m_date         VARCHAR2(20);
  -- v_weeks          NUMBER := 52;
  -- v_weeks          NUMBER := 70;
  -- v_weeks          NUMBER := 96;--Changes added to consider the Accrual synhup for year 2016
v_weeks          NUMBER := 104;--Changes added to consider the Accrual synhup for Last 2 years

   v_eng_profile    NUMBER := 1;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_CALC_ACCRUAL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') + 7
    INTO max_sales_date
    FROM DUAL;

    SELECT get_max_date
    INTO v_m_date
    FROM DUAL;

   check_and_drop('rr_promos_calc_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_calc_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_margin_support = 0
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_calc_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    check_and_drop('rr_promos_calc_val_t');

    -- cmos 23-Jan-2017 (px ctas)
    sql_str := 'CREATE TABLE rr_promos_calc_val_t
                TABLESPACE ts_temp_tables parallel 32 nologging AS
                SELECT /*+ FULL(pd) PARALLEL(pd, 32) */ pd.promotion_id,
               pd.item_id,
               pd.location_id,
               pd.sales_date,
               CASE
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR  NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl,0)
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_base_pd,
               CASE
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                        OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.incr_evt_vol_or, NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_incr_pd,
               CASE WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    ELSE NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_eng_incr_pd,
               CASE
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or, NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_pd,
               CASE
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0)
               END rr_supply_vol_base_pd,
               CASE
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.incr_evt_vol_act, 0)
                    ELSE CASE WHEN m.dem_stream = 0 THEN 0 ELSE NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))) END
               END rr_supply_vol_incr_pd,
               CASE WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    ELSE NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))
               END rr_supply_eng_incr_pd,
               CASE
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END rr_supply_vol_pd,
               CASE
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1)* (NVL(pd.case_buydown,0)) rr_pl_cd_pd,
               CASE
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) * NVL(pd.units, 0) * (NVL(pd.rr_handling_d, 0)) rr_pl_hf_cd_pd,
               CASE
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0 ELSE NVL(pd.event_cost,0)
               END rr_pl_coop_pd,
               CASE
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0 ELSE NVL(pd.rr_handling_coop, 0)
               END rr_pl_hf_coop_pd,
               CASE
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                          OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1)* (NVL(pd.case_buydown,0)) rr_accrual_cd_pd,
               CASE
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                          OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) * NVL(pd.units, 0) * (NVL(pd.rr_handling_d, 0)) rr_accrual_hf_cd_pd,
               CASE
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0 ELSE NVL(pd.event_cost,0)
               END rr_accrual_coop_pd,
               CASE
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0
                    ELSE NVL(pd.rr_handling_coop, 0)
               END rr_accrual_hf_coop_pd,
               CASE WHEN p.approval = 1 AND ((pd.accruals_type_id IN (0) AND (pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7)) AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 8)
                         OR
                         (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 8))
                    THEN 1
                    WHEN p.approval = 1 AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 8 AND pd.accruals_type_id IN (1)  THEN 2
                    ELSE pd.accruals_type_id
               END accruals_type_id,
               CASE
                    WHEN p.approval = 1 AND (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                                  OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 )  AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7 AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN 6
                    WHEN p.approval = 1 AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7)
                    THEN 5
                    WHEN pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7
                    THEN 4
                    ELSE pd.promotion_stat_id
               END promotion_stat_id,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE
                            WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                            THEN 0
                            WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0  AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                            THEN NVL(pd.evt_vol_act, 0)
                            ELSE NVL(pd.volume_base_ttl,0)
                          END * NVL(pd.rr_redemp_p,1)
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_base_commit, 0)
                              ELSE pd.rr_accrual_base_commit
                         END
               END  rr_accrual_base_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE
                            WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                            THEN 0
                            WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                                  OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                            THEN NVL(pd.incr_evt_vol_act, 0)
                            ELSE NVL(pd.incr_evt_vol_or, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))
                          END * NVL(pd.rr_redemp_p,1)
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_incr_commit, 0)
                              ELSE pd.rr_accrual_incr_commit
                         END
               END  rr_accrual_incr_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                            THEN 0
                            ELSE NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))
                         END
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_eng_incr_commit, 0)
                              ELSE pd.rr_accrual_eng_incr_commit
                         END
                END rr_accrual_eng_incr_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE
                            WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                            THEN 0
                            WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                                  OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                            THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                            ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))
                          END * NVL(pd.rr_redemp_p,1)
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_vol_commit, 0)
                              ELSE pd.rr_accrual_vol_commit
                         END
               END  rr_accrual_vol_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE
                            WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                            THEN 0
                            WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                                  OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                            THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                            ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))
                          END * NVL(pd.rr_redemp_p,1)  * (NVL(pd.case_buydown,0))
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_cd_commit, 0)
                              ELSE pd.rr_accrual_cd_commit
                         END
               END rr_accrual_cd_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                              THEN 0
                              ELSE NVL(pd.event_cost,0)
                         END
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_coop_commit, 0)
                              ELSE pd.rr_accrual_coop_commit
                         END
               END rr_accrual_coop_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE
                            WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                            THEN 0
                            WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                                  OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                            THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                            ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))
                          END * NVL(pd.rr_redemp_p,1) * NVL(pd.units, 0) * (NVL(pd.rr_handling_d,0))
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_hf_cd_commit, 0)
                              ELSE pd.rr_accrual_hf_cd_commit
                         END
               END rr_accrual_hf_cd_commit,
               CASE WHEN p.approval = 1 AND ((pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.accruals_type_id = 0)
                         OR (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN CASE WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                              THEN 0
                              ELSE NVL(pd.rr_handling_coop, 0)
                         END
                    ELSE CASE WHEN p.approval = 1 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (pm.rr_promo_offset * 7) AND pd.promotion_stat_id >= 4
                              THEN NVL(pd.rr_accrual_hf_coop_commit, 0)
                              ELSE pd.rr_accrual_hf_coop_commit
                         END
               END rr_accrual_hf_coop_commit
               FROM promotion p, promotion_data pd, promotion_matrix pm, mdp_matrix m, rr_promos_calc_t rpt, promotion_stat ps, promotion_type pt,
               t_ep_ebs_customer latt6
               WHERE 1 = 1
               AND pd.promotion_type_id = pt.promotion_type_id
               AND pd.promotion_stat_id = ps.promotion_stat_id
               AND p.promotion_id = rpt.promotion_id
               AND p.promotion_id = pm.promotion_id
               AND pd.promotion_id = pm.promotion_id
               AND pd.item_id = pm.item_id
               AND pd.location_id = pm.location_id
               AND pd.is_self = 1 -- lannapra Added 15 May 2015
               AND pm.item_id = m.item_id
               AND pm.location_id = m.location_id
               AND latt6.t_ep_ebs_customer_ep_id = m.t_ep_ebs_customer_ep_id
               AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'')';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_calc_val_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str := 'MERGE /*+ FULL(pd) PARALLEL(pd, 16) */ INTO promotion_data pd
               USING(SELECT /*+ FULL(t) PARALLEL(t, 32) */ * FROM rr_promos_calc_val_t t) pd1
               ON(pd.promotion_id = pd1.promotion_id
               AND pd.item_id = pd1.item_id
               AND pd.location_id = pd1.location_id
               AND pd.sales_date = pd1.sales_date)
               WHEN MATCHED THEN
               UPDATE SET pd.rr_supply_vol_base_pd = NVL(pd1.rr_supply_vol_base_pd, 0),
               pd.rr_supply_vol_incr_pd = NVL(pd1.rr_supply_vol_incr_pd, 0),
               pd.rr_supply_eng_incr_pd = NVL(pd1.rr_supply_eng_incr_pd, 0),
               pd.rr_supply_vol_pd = NVL(pd1.rr_supply_vol_pd, 0),
               pd.rr_accrual_vol_base_pd = NVL(pd1.rr_accrual_vol_base_pd, 0),
               pd.rr_accrual_vol_incr_pd = NVL(pd1.rr_accrual_vol_incr_pd, 0),
               pd.rr_accrual_eng_incr_pd = NVL(pd1.rr_accrual_eng_incr_pd, 0),
               pd.rr_accrual_vol_pd = NVL(pd1.rr_accrual_vol_pd, 0),
               pd.rr_accrual_cd_pd = NVL(pd1.rr_accrual_cd_pd, 0),
               pd.rr_accrual_coop_pd = NVL(pd1.rr_accrual_coop_pd, 0),
               pd.rr_accrual_hf_cd_pd = NVL(pd1.rr_accrual_hf_cd_pd, 0),
               pd.rr_accrual_hf_coop_pd = NVL(pd1.rr_accrual_hf_coop_pd, 0),
               pd.rr_pl_cd_pd = NVL(pd1.rr_pl_cd_pd, 0),
               pd.rr_pl_coop_pd = NVL(pd1.rr_pl_coop_pd, 0),
               pd.rr_pl_hf_cd_pd = NVL(pd1.rr_pl_hf_cd_pd, 0),
               pd.rr_pl_hf_coop_pd = NVL(pd1.rr_pl_hf_coop_pd, 0),
               pd.rr_accrual_base_commit =  pd1.rr_accrual_base_commit,
               pd.rr_accrual_eng_incr_commit = pd1.rr_accrual_eng_incr_commit,
               pd.rr_accrual_incr_commit =  pd1.rr_accrual_incr_commit,
               pd.rr_accrual_vol_commit =  pd1.rr_accrual_vol_commit,
               pd.rr_accrual_cd_commit =  pd1.rr_accrual_cd_commit,
               pd.rr_accrual_coop_commit = pd1.rr_accrual_coop_commit,
               pd.rr_accrual_hf_cd_commit = pd1.rr_accrual_hf_cd_commit,
               pd.rr_accrual_hf_coop_commit = pd1.rr_accrual_hf_coop_commit,
               pd.promotion_stat_id = pd1.promotion_stat_id,
               pd.accruals_type_id = pd1.accruals_type_id,
               pd.last_update_date = SYSDATE
               WHERE 1 = 2
               OR NVL(pd.rr_supply_vol_base_pd, 0) <> NVL(pd1.rr_supply_vol_base_pd, 0)
               OR NVL(pd.rr_supply_vol_incr_pd, 0) <> NVL(pd1.rr_supply_vol_incr_pd, 0)
               OR NVL(pd.rr_supply_eng_incr_pd, 0) <> NVL(pd1.rr_supply_eng_incr_pd, 0)
               OR NVL(pd.rr_supply_vol_pd, 0) <> NVL(pd1.rr_supply_vol_pd, 0)
               OR NVL(pd.rr_accrual_vol_base_pd, 0) <> NVL(pd1.rr_accrual_vol_base_pd, 0)
               OR NVL(pd.rr_accrual_vol_incr_pd, 0) <> NVL(pd1.rr_accrual_vol_incr_pd, 0)
               OR NVL(pd.rr_accrual_eng_incr_pd, 0) <> NVL(pd1.rr_accrual_eng_incr_pd, 0)
               OR NVL(pd.rr_accrual_vol_pd, 0) <> NVL(pd1.rr_accrual_vol_pd, 0)
               OR NVL(pd.rr_accrual_cd_pd, 0) <> NVL(pd1.rr_accrual_cd_pd, 0)
               OR NVL(pd.rr_accrual_coop_pd, 0) <> NVL(pd1.rr_accrual_coop_pd, 0)
               OR NVL(pd.rr_pl_cd_pd, 0) <> NVL(pd1.rr_pl_cd_pd, 0)
               OR NVL(pd.rr_pl_coop_pd, 0) <> NVL(pd1.rr_pl_coop_pd, 0)
               OR NVL(pd.rr_accrual_hf_cd_pd, 0) <> NVL(pd1.rr_accrual_hf_cd_pd, 0)
               OR NVL(pd.rr_accrual_hf_coop_pd, 0) <> NVL(pd1.rr_accrual_hf_coop_pd, 0)
               OR NVL(pd.rr_pl_hf_cd_pd, 0) <> NVL(pd1.rr_pl_hf_cd_pd, 0)
               OR NVL(pd.rr_pl_hf_coop_pd, 0) <> NVL(pd1.rr_pl_hf_coop_pd, 0)
               OR NVL(pd.rr_accrual_base_commit, 0) <> NVL(pd1.rr_accrual_base_commit, 0)
               OR NVL(pd.rr_accrual_eng_incr_commit, 0) <> NVL(pd1.rr_accrual_eng_incr_commit, 0)
               OR NVL(pd.rr_accrual_incr_commit, 0) <> NVL(pd1.rr_accrual_incr_commit, 0)
               OR NVL(pd.rr_accrual_vol_commit, 0) <> NVL(pd1.rr_accrual_vol_commit, 0)
               OR NVL(pd.rr_accrual_cd_commit, 0) <> NVL(pd1.rr_accrual_cd_commit, 0)
               OR NVL(pd.rr_accrual_coop_commit, 0) <> NVL(pd1.rr_accrual_coop_commit, 0)
               OR NVL(pd.rr_accrual_hf_cd_commit, 0) <> NVL(pd1.rr_accrual_hf_cd_commit, 0)
               OR NVL(pd.rr_accrual_hf_coop_commit, 0) <> NVL(pd1.rr_accrual_hf_coop_commit, 0)
               OR NVL(pd.promotion_stat_id, 0) <> NVL(pd1.promotion_stat_id, 0)
               OR NVL(pd.accruals_type_id, 0) <> NVL(pd1.accruals_type_id, 0)';

    dbms_output.put_line(sql_str);
    dynamic_ddl(sql_str);
    COMMIT;
        ---

    sql_str := 'MERGE INTO promotion_matrix pm
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 32) */ pd.promotion_id,
               --pd.item_id, pd.location_id,
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_calc_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id
               --, pd.item_id, pd.location_id
               ) pm1
               ON(pm.promotion_id = pm1.promotion_id
               --AND pm.item_id = pm1.item_id
               --AND pm.location_id = pm1.location_id
               )
               WHEN MATCHED THEN
               UPDATE SET pm.promotion_stat_id = pm1.promotion_stat_id, pm.accruals_type_id = pm1.accruals_type_id,
               pm.promotion_data_lud = SYSDATE
               ';

    dynamic_ddl(sql_str);
    COMMIT;
    ---

    sql_str := 'MERGE INTO promotion p
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 32) */ pd.promotion_id,
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_calc_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET p.promotion_stat_id = p1.promotion_stat_id,
               p.accruals_type_id = p1.accruals_type_id
               ';

    dynamic_ddl(sql_str);
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

PROCEDURE prc_promo_accrual_sync(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_sync_flag   NUMBER DEFAULT 3,
                                     p_sync_ms     NUMBER DEFAULT 0,
                                     p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PROMO_ACCRUAL_SYNC
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
  -- v_weeks          NUMBER := 52;
    -- v_weeks          NUMBER := 70;
  -- v_weeks          NUMBER := 96;--Changes added to consider the Accrual synhup for year 2016
   v_weeks          NUMBER := 104;--Changes added to consider the Accrual synhup for Last 2 years

   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;
   p_gen_sync_rule  VARCHAR2(4000);
   p_sales_sync_rule  VARCHAR2(4000);
   p_alloc_sync_rule  VARCHAR2(4000);
    -- Added Sanjiiv CR7 Permanent MS Fix
   TYPE   cls_ms_line_type IS RECORD (promotion_id        promotion.promotion_id%TYPE,
                                       item_id             sales_data.item_id%TYPE,
                                       location_id         sales_data.location_id%TYPE,
                                       plan_date           sales_data.sales_date%TYPE);
    TYPE   cls_ms_line_tbl_type IS TABLE OF cls_ms_line_type;
    cls_ms_line_tbl            cls_ms_line_tbl_type; 
    -- 2 
    TYPE   cls_ms_line_type1 IS RECORD (item_id             sales_data.item_id%TYPE,
                                        location_id         sales_data.location_id%TYPE,
                                        sales_date          sales_data.sales_date%TYPE,
                                        cust_input2            sales_data.cust_input2%TYPE,
                                        tiv                    sales_data.cust_input2%TYPE);
    TYPE   cls_ms_line_tbl_type1 IS TABLE OF cls_ms_line_type1;
    cls_ms_line_tbl1            cls_ms_line_tbl_type1;  
    ---    
    -- 3 
    TYPE   cls_ms_line_type2 IS RECORD (item_id             sales_data.item_id%TYPE,
                                        location_id         sales_data.location_id%TYPE,
                                        sales_date          sales_data.sales_date%TYPE);
    TYPE   cls_ms_line_tbl_type2 IS TABLE OF cls_ms_line_type2;
    cls_ms_line_tbl2            cls_ms_line_tbl_type2;
    --
    fetch_size NUMBER := 500000;
    l_cursor  SYS_REFCURSOR;
    ---
    dml_errors              EXCEPTION;
    PRAGMA EXCEPTION_INIT(dml_errors, -24381);
    vn_errors               NUMBER;
    vn_count                NUMBER;
    --- End Sanjiiv CR7
   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_PROMO_ACCRUAL_SYNC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss')
    INTO max_sales_date
    FROM DUAL;

    p_alloc_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.alloc_wk6 WHEN -35 THEN rspt.alloc_wk5  WHEN -28 THEN rspt.alloc_wk4 WHEN -21 THEN rspt.alloc_wk3 WHEN -14 THEN rspt.alloc_wk2 WHEN -7 THEN rspt.alloc_wk1 WHEN 0 THEN rspt.alloc_wk0 END ';
    p_sales_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.sync_wk6 WHEN -35 THEN rspt.sync_wk5 WHEN -28 THEN rspt.sync_wk4 WHEN -21 THEN rspt.sync_wk3 WHEN -14 THEN rspt.sync_wk2 WHEN -7 THEN rspt.sync_wk1 WHEN 0 THEN rspt.sync_wk0 END ';

   check_and_drop('rr_promos_sync_' || p_sync_ms || '_t');

       v_status := 'Start1 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_sync_' || p_sync_ms || '_t
               TABLESPACE ts_temp_tables parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id,
               CASE WHEN TO_CHAR(pdt.from_date, ''DAY'') = ''' || v_day || ''' AND MOD((TRUNC(pdt.until_date, ''DD'') - TRUNC(pdt.from_date, ''DD'')) + 1, 7) = 0 THEN 1
                    ELSE 1
               END rr_start_day,
               CASE WHEN ps.rr_sync_sd = 1 AND pt.rr_sync_sd = 1 THEN 1 ELSE 0 END rr_sync_sd,
               CASE WHEN ps.rr_sync_ad = 1 AND pt.rr_sync_ad = 1 THEN 1 ELSE 0 END rr_sync_ad,
               CASE WHEN ps.rr_sync_vol_sd = 1 AND pt.rr_sync_vol_sd = 1 THEN 1 ELSE 0 END rr_sync_vol_sd,
               CASE WHEN ps.rr_sync_vol_ad = 1 AND pt.rr_sync_vol_ad = 1 THEN 1 ELSE 0 END rr_sync_vol_ad,
               p.promotion_type_id,
               p.promotion_stat_id,
               p.cd_deal_type,
               p.coop_deal_type,
               p.oi_deal_type
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_margin_support = ' || p_sync_ms || '
               AND (1 = 2 ' ||
               CASE WHEN p_sync_flag IN (1, 3) THEN '
               OR (ps.rr_sync_sd = 1 AND pt.rr_sync_sd = 1)
               ' END ||
               CASE WHEN p_sync_flag IN (2, 3) THEN '
               OR (ps.rr_sync_ad = 1 AND pt.rr_sync_ad = 1)
               ' END || '
               )
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

    dynamic_ddl (sql_str);
    dynamic_ddl('alter table rr_promos_sync_' || p_sync_ms || '_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;


    check_and_drop('rr_sync_promo_' || p_sync_ms || '_t');

          v_status := 'Start2 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    -- cmos 23-Jan-2017 (px ctas)
    sql_str :=('CREATE TABLE rr_sync_promo_' || p_sync_ms || '_t(promotion_id, item_id, location_id, sales_date, rr_sync_sd, rr_sync_ad, promotion_type_id, promotion_stat_id,
                                            promo_price, case_buydown, case_oi, rr_oi_p, rr_redemp_p, base_plan, incr_plan, engine_incr, accrual_vol,
                                            base_plan_commit, incr_plan_commit, engine_incr_commit, accrual_vol_commit,
                                            sup_base_plan, sup_incr_plan, sup_engine_incr, supply_vol, pl_cd, pl_coop,  pl_hf_cd, pl_hf_coop,
                                            accrual_cd, accrual_coop, accrual_hf_cd, accrual_hf_coop,
                                            accrual_cd_commit, accrual_coop_commit, accrual_hf_cd_commit, accrual_hf_coop_commit,
                                            alloc_wk0, alloc_wk1, alloc_wk2, alloc_wk3, alloc_wk4, alloc_wk5,alloc_wk6,
                                            sync_wk0, sync_wk1, sync_wk2, sync_wk3, sync_wk4, sync_wk5, sync_wk6, rr_promo_offset, rr_ms_vol)
                                            TABLESPACE ts_temp_tables parallel 32 nologging
         AS SELECT /*+ full(pd) parallel(pd,64) */
              pd.promotion_id,
              pd.item_id,
              pd.location_id,
              pd.sales_date,
              rpt.rr_sync_sd,
              rpt.rr_sync_ad,
              pd.promotion_type_id,
              pd.promotion_stat_id,
              decode(rtrim(to_char(pd.sales_date,''DAY'')),''MONDAY'',NVL(pd.promo_price,0)*pd.is_self,null)  promo_price,
              decode(rtrim(to_char(pd.sales_date,''DAY'')),''MONDAY'',NVL(pd.case_buydown,0)*pd.is_self,null) case_buydown,
              pd.case_buydownoi*pd.is_self case_oi,
              pd.rr_oi_p*pd.is_self rr_oi_p,
              NVL(pd.rr_redemp_p,1) rr_redemp_p,
              pd.rr_accrual_vol_base_pd base_plan,
              pd.rr_accrual_vol_incr_pd incr_plan,
              pd.rr_accrual_eng_incr_pd engine_incr,
              pd.rr_accrual_vol_pd accrual_vol,
              pd.rr_accrual_base_commit base_plan_commit,
              pd.rr_accrual_incr_commit incr_plan_commit,
              pd.rr_accrual_eng_incr_commit engine_incr_commit,
              pd.rr_accrual_vol_commit accrual_vol_commit,
              pd.rr_supply_vol_base_pd sup_base_plan,
              pd.rr_supply_vol_incr_pd sup_incr_plan,
              pd.rr_supply_eng_incr_pd sup_engine_incr,
              pd.rr_supply_vol_pd supply_vol,
              pd.rr_pl_cd_pd pl_cd,
              pd.rr_pl_coop_pd pl_coop,
              pd.rr_pl_hf_cd_pd pl_hf_cd,
              pd.rr_pl_hf_coop_pd pl_hf_coop,
              pd.rr_accrual_cd_pd accrual_cd,
              pd.rr_accrual_coop_pd accrual_coop,
              pd.rr_accrual_hf_cd_pd accrual_hf_cd,
              pd.rr_accrual_hf_coop_pd accrual_hf_coop,
              pd.rr_accrual_cd_commit accrual_cd_commit,
              pd.rr_accrual_coop_commit accrual_coop_commit,
              pd.rr_accrual_hf_cd_commit accrual_hf_cd_commit,
              pd.rr_accrual_hf_coop_commit accrual_hf_coop_commit,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_alloc_wk0 END alloc_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk1 END alloc_wk1,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk2 END alloc_wk2,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk3 END alloc_wk3,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk4 END alloc_wk4,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk5 END alloc_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk6 END alloc_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_sync_wk0 END sync_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk1 END sync_wk1,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk2 END sync_wk2,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk3 END sync_wk3,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk4 END sync_wk4,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk5 END sync_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk6 END sync_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_promo_offset END rr_promo_offset,
              NVL(pd.rr_ms_vol, 0) rr_ms_vol
            FROM  promotion_data pd,
                  promotion p,
                  promotion_matrix pm,
                  rr_promos_sync_' || p_sync_ms || '_t rpt
            WHERE 1 = 1
            AND     pm.promotion_id         = p.promotion_id
            AND     pd.promotion_id         = pm.promotion_id
            AND   pd.item_id                 = pm.item_id
            AND   pd.location_id         = pm.location_id
            AND   pd.is_self = 1 --Added lannapra 15 May 2015
            AND   pd.sales_date             >= TO_DATE('''|| TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   pd.sales_date             <= TO_DATE('''|| TO_CHAR(v_max, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   p.promotion_id    = rpt.promotion_id
            '
                );
       ---
    dynamic_ddl (sql_str);
    dynamic_ddl('alter table rr_sync_promo_' || p_sync_ms || '_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

 --   check_and_drop('rr_sync_promo_offset_' || p_sync_ms || '_t');

 --   sql_str :=('CREATE TABLE rr_sync_promo_offset_' || p_sync_ms || '_t(promotion_id,
--                 item_id, location_id, sales_date, plan_date, rr_sync_sd, rr_sync_ad, promotion_type_id, promotion_stat_id, promo_price,
  --               case_buydown,    case_oi, rr_oi_p,
 --                base_plan, incr_plan, engine_incr,    accrual_vol,
 --                base_plan_commit, incr_plan_commit, engine_incr_commit,    accrual_vol_commit,
  --               sup_base_plan, sup_incr_plan, sup_engine_incr,    supply_vol,
 --                accrual_cd, accrual_coop, accrual_hf_cd, accrual_hf_coop,
 --                accrual_cd_commit, accrual_coop_commit, accrual_hf_cd_commit, accrual_hf_coop_commit,
 --                base_plan_a, incr_plan_a, engine_incr_a, accrual_vol_a,
  --               base_plan_commit_a, incr_plan_commit_a, engine_incr_commit_a, accrual_vol_commit_a,
  --               accrual_cd_a, accrual_coop_a, accrual_hf_cd_a, accrual_hf_coop_a,
  --               accrual_cd_commit_a, accrual_coop_commit_a, accrual_hf_cd_commit_a, accrual_hf_coop_commit_a,
 --                base_plan_s, incr_plan_s, engine_incr_s, accrual_vol_s, accrual_cd_s, accrual_coop_s, accrual_hf_cd_s, accrual_hf_coop_s, rr_ms_vol)
  --               TABLESPACE ts_temp_tables
  --        AS SELECT /*+ full(rspt) parallel(rspt,32) */ rspt.promotion_id,
 --            rspt.item_id, rspt.location_id, rspt.sales_date, i.datet  plan_date, rspt.rr_sync_sd, rspt.rr_sync_ad, rspt.promotion_type_id,
  --           rspt.promotion_stat_id, rspt.promo_price, rspt.case_buydown,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.case_oi,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.rr_oi_p,
  --           CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.base_plan base_plan,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.incr_plan incr_plan,
  --           CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.engine_incr engine_incr,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_vol accrual_vol,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.base_plan_commit base_plan_commit,
  --           CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.incr_plan_commit incr_plan_commit,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.engine_incr_commit engine_incr_commit,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_vol_commit accrual_vol_commit,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_base_plan sup_base_plan,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_incr_plan sup_incr_plan,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_engine_incr sup_engine_incr,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.supply_vol supply_vol,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_cd accrual_cd,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_coop accrual_coop,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_cd accrual_hf_cd,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_coop accrual_hf_coop,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_cd_commit accrual_cd_commit,
  --           CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_coop_commit accrual_coop_commit,
  --           CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_cd_commit accrual_hf_cd_commit,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_coop_commit accrual_hf_coop_commit,
 --            ' || p_alloc_sync_rule || ' * rspt.base_plan base_plan_a,
 --            ' || p_alloc_sync_rule || ' * rspt.incr_plan incr_plan_a,
 --            ' || p_alloc_sync_rule || ' * rspt.engine_incr engine_incr_a,
 --            ' || p_alloc_sync_rule || ' * rspt.accrual_vol accrual_vol_a,
  --           ' || p_alloc_sync_rule || ' * rspt.base_plan_commit base_plan_commit_a,
 --            ' || p_alloc_sync_rule || ' * rspt.incr_plan_commit incr_plan_commit_a,
 --            ' || p_alloc_sync_rule || ' * rspt.engine_incr_commit engine_incr_commit_a,
 --            ' || p_alloc_sync_rule || ' * rspt.accrual_vol_commit accrual_vol_commit_a,
 --            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd END accrual_cd_a,
 --            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop END accrual_coop_a,
 --            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_cd END accrual_hf_cd_a,
 --            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_coop END accrual_hf_coop_a,
--             ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd_commit END accrual_cd_commit_a,
--             ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop_commit END accrual_coop_commit_a,
 --            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd_commit END accrual_hf_cd_commit_a,
--             ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop_commit END accrual_hf_coop_commit_a,
 --            ' || p_sales_sync_rule || ' * rspt.sup_base_plan base_plan_s,
 --            ' || p_sales_sync_rule || ' * rspt.sup_incr_plan incr_plan_s,
--             ' || p_sales_sync_rule || ' * rspt.sup_engine_incr engine_incr_s,
 --            ' || p_sales_sync_rule || ' * rspt.supply_vol accrual_vol_s,
 --            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_cd END accrual_cd_s,
 --            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_coop END accrual_coop_s,
 --            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_cd END accrual_hf_cd_s,
 --            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_coop END accrual_hf_coop_s,
 --            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.rr_ms_vol rr_ms_vol
 --         FROM rr_sync_promo_' || p_sync_ms || '_t rspt,
 --         inputs i
 --         WHERE i.datet BETWEEN rspt.sales_date - (rspt.rr_promo_offset * 7) AND rspt.sales_date');

--     dynamic_ddl (sql_str);
--     COMMIT;



    -- New code for perf -- 11/12/2014 --

          v_status := 'Start3 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    check_and_drop('rr_sync_promo_offset_' || p_sync_ms || '_idx');

    dynamic_ddl('TRUNCATE TABLE rr_sync_promo_offset_' || p_sync_ms || '_t REUSE STORAGE');

    -- p_alloc_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.alloc_wk6 WHEN -35 THEN rspt.alloc_wk5  WHEN -28 THEN rspt.alloc_wk4 WHEN -21 THEN rspt.alloc_wk3 WHEN -14 THEN rspt.alloc_wk2 WHEN -7 THEN rspt.alloc_wk1 WHEN 0 THEN rspt.alloc_wk0 END ';
    -- p_sales_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.sync_wk6 WHEN -35 THEN rspt.sync_wk5 WHEN -28 THEN rspt.sync_wk4 WHEN -21 THEN rspt.sync_wk3 WHEN -14 THEN rspt.sync_wk2 WHEN -7 THEN rspt.sync_wk1 WHEN 0 THEN rspt.sync_wk0 END ';

    p_gen_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN 1 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN 0 THEN 1 ELSE 0 END * (i.num_of_days/7) END ';
    p_alloc_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN rspt.alloc_wk0 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN -42 THEN rspt.alloc_wk6 WHEN -35 THEN rspt.alloc_wk5 WHEN -28 THEN rspt.alloc_wk4 WHEN -21 THEN rspt.alloc_wk3 WHEN -14 THEN rspt.alloc_wk2 WHEN -7 THEN rspt.alloc_wk1 WHEN 0 THEN rspt.alloc_wk0 END * (i.num_of_days/7) END ';
    p_sales_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN rspt.sync_wk0 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN -42 THEN rspt.sync_wk6 WHEN -35 THEN rspt.sync_wk5 WHEN -28 THEN rspt.sync_wk4 WHEN -21 THEN rspt.sync_wk3 WHEN -14 THEN rspt.sync_wk2 WHEN -7 THEN rspt.sync_wk1 WHEN 0 THEN rspt.sync_wk0 END * (i.num_of_days/7) END ';


    FOR i IN 0 ..6
    LOOP

    sql_str :=('INSERT /*+ APPEND NOLOGGING */ INTO rr_sync_promo_offset_' || p_sync_ms || '_t(promotion_id,
item_id, location_id, sales_date, plan_date, rr_sync_sd, rr_sync_ad, promotion_type_id, promotion_stat_id, promo_price,
case_buydown,    case_oi, rr_oi_p, rr_redemp_p,
base_plan, incr_plan, engine_incr,    accrual_vol,  '
--base_plan_commit, incr_plan_commit, engine_incr_commit,    accrual_vol_commit,
|| 'sup_base_plan, sup_incr_plan, sup_engine_incr,    supply_vol,
accrual_cd, accrual_coop, accrual_hf_cd, accrual_hf_coop, '
--accrual_cd_commit, accrual_coop_commit, accrual_hf_cd_commit, accrual_hf_coop_commit,
|| 'base_plan_a, incr_plan_a, engine_incr_a, accrual_vol_a, 
base_plan_commit_a, incr_plan_commit_a,
--engine_incr_commit_a, 
accrual_vol_commit_a,'--IBM/IN1066768
|| 'accrual_cd_a, accrual_coop_a, accrual_hf_cd_a, accrual_hf_coop_a, 
accrual_cd_commit_a, accrual_coop_commit_a,'--IBM/IN1066768
--accrual_hf_cd_commit_a, accrual_hf_coop_commit_a,
|| 'base_plan_s, incr_plan_s, engine_incr_s, accrual_vol_s, accrual_cd_s, accrual_coop_s, accrual_hf_cd_s, accrual_hf_coop_s, rr_ms_vol)
SELECT /* full(rspt) parallel(rspt,32) */ rspt.promotion_id,
rspt.item_id, rspt.location_id, rspt.sales_date, i.datet  plan_date, rspt.rr_sync_sd, rspt.rr_sync_ad, rspt.promotion_type_id,
rspt.promotion_stat_id,' || CASE WHEN i =0 THEN ' rspt.promo_price ' ELSE 'NULL' END || ', ' || CASE WHEN i =0 THEN ' rspt.case_buydown ' ELSE 'NULL' END || ',
' || CASE WHEN i =0 THEN ' rspt.case_oi ' ELSE 'NULL' END || ',
' || CASE WHEN i =0 THEN ' rspt.rr_oi_p ' ELSE 'NULL' END || ',
' || CASE WHEN i =0 THEN ' rspt.rr_redemp_p ' ELSE 'NULL' END || ',
--CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.case_oi,
--CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.rr_oi_p,
'
            || p_gen_sync_rule || ' * rspt.base_plan base_plan,'
            || p_gen_sync_rule || ' * rspt.incr_plan incr_plan,'
            || p_gen_sync_rule || ' * rspt.engine_incr engine_incr,'
            || p_gen_sync_rule || ' * rspt.accrual_vol accrual_vol,'
            --|| p_gen_sync_rule || ' * rspt.base_plan_commit base_plan_commit,'
            --|| p_gen_sync_rule || ' * rspt.incr_plan_commit incr_plan_commit,'
            --|| p_gen_sync_rule || ' * rspt.engine_incr_commit engine_incr_commit,'
            --|| p_gen_sync_rule || ' * rspt.accrual_vol_commit accrual_vol_commit,'
            || p_gen_sync_rule || ' * rspt.sup_base_plan sup_base_plan,'
            || p_gen_sync_rule || ' * rspt.sup_incr_plan sup_incr_plan,'
            || p_gen_sync_rule || ' * rspt.sup_engine_incr sup_engine_incr,'
            || p_gen_sync_rule || ' * rspt.supply_vol supply_vol,'
            || p_gen_sync_rule || ' * rspt.accrual_cd accrual_cd,'
            || p_gen_sync_rule || ' * rspt.accrual_coop accrual_coop,'
            || p_gen_sync_rule || ' * rspt.accrual_hf_cd accrual_hf_cd,'
            || p_gen_sync_rule || ' * rspt.accrual_hf_coop accrual_hf_coop,'
            --|| p_gen_sync_rule || ' * rspt.accrual_cd_commit accrual_cd_commit,'
            --|| p_gen_sync_rule || ' * rspt.accrual_coop_commit accrual_coop_commit,'
            --|| p_gen_sync_rule || ' * rspt.accrual_hf_cd_commit accrual_hf_cd_commit,'
            --|| p_gen_sync_rule || ' * rspt.accrual_hf_coop_commit accrual_hf_coop_commit,'
            || p_alloc_sync_rule || ' * rspt.base_plan base_plan_a,'
            || p_alloc_sync_rule || ' * rspt.incr_plan incr_plan_a,'
            || p_alloc_sync_rule || ' * rspt.engine_incr engine_incr_a,'
            || p_alloc_sync_rule || ' * rspt.accrual_vol accrual_vol_a,'
            || p_alloc_sync_rule || ' * rspt.base_plan_commit base_plan_commit_a,'--IBM
            || p_alloc_sync_rule || ' * rspt.incr_plan_commit incr_plan_commit_a,'--IBM
            --|| p_alloc_sync_rule || ' * rspt.engine_incr_commit engine_incr_commit_a,'
            || p_alloc_sync_rule || ' * rspt.accrual_vol_commit accrual_vol_commit_a,'--IBM/IN1066768
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd END accrual_cd_a,'
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop END accrual_coop_a,'
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_cd END accrual_hf_cd_a,'
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_coop END accrual_hf_coop_a,'
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd_commit END accrual_cd_commit_a,'--IBM/IN1066768
            || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop_commit END accrual_coop_commit_a,'--IBM/IN1066768
            --|| p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd_commit END accrual_hf_cd_commit_a,'
            --|| p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop_commit END accrual_hf_coop_commit_a,'
            || p_sales_sync_rule || ' * rspt.sup_base_plan base_plan_s,'
            || p_sales_sync_rule || ' * rspt.sup_incr_plan incr_plan_s,'
            || p_sales_sync_rule || ' * rspt.sup_engine_incr engine_incr_s,'
            || p_sales_sync_rule || ' * rspt.supply_vol accrual_vol_s,'
            || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_cd END accrual_cd_s,'
            || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_coop END accrual_coop_s,'
            || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_cd END accrual_hf_cd_s,'
            || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_coop END accrual_hf_coop_s,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.rr_ms_vol rr_ms_vol
         FROM rr_sync_promo_' || p_sync_ms || '_t rspt,
         inputs i
         WHERE 1= 1
         AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY(rspt.sales_date , ''MONDAY'') - 7 - (' || i || '*7)
         AND ' || i || ' <= rspt.rr_promo_offset');

    rr_pkg_proc_log.prc_proc_sql_log(v_proc_log_id, sql_str);
    dynamic_ddl (sql_str);
    COMMIT;
     END LOOP;

    sql_str := 'CREATE INDEX rr_sync_promo_offset_' || p_sync_ms || '_idx ON rr_sync_promo_offset_' || p_sync_ms || '_t(promotion_id, item_id, location_id, plan_date)';
    dynamic_ddl (sql_str);
    COMMIT;

    v_status := 'Start4 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    IF p_sync_flag IN (1, 3) THEN

         IF p_sync_ms = 0 THEN

              check_and_drop('rr_sync_promo_offset_g_' || p_sync_ms || '_t');

          -- cmos 23-Jan-2017 (px ctas)
              sql_str :=('CREATE TABLE rr_sync_promo_offset_g_' || p_sync_ms || '_t
                          TABLESPACE ts_temp_tables parallel 32 nologging
                          AS SELECT /*+ full(sd) parallel(sd,64) */ sd.item_id,
                                         sd.location_id,
                                         sd.plan_date,
                                         SUM(sd.base_plan_s) base_plan,
                                         SUM(sd.incr_plan_s) incr_vol,
                                         SUM(sd.engine_incr_s) engine_vol,
                                         SUM(sd.sup_incr_plan) incr_plan_non_offset,
                                         SUM(sd.sup_engine_incr) engine_incr_non_offset,
                                         MIN(CASE WHEN sd.promo_price = 0 THEN null ELSE sd.promo_price END) promo_price,
                                         SUM (NVL(CASE WHEN p.activity_id in ( 12,4,5,9,6,1,15,3,2,17 ) THEN sd.case_buydown ELSE NULL END, 0)) case_buydown,    -- ebspricelist103
                                         MAX (NVL(CASE WHEN p.activity_id in ( 12,4,5,9,6,1,15,3,2,17 ) THEN sd.rr_redemp_p ELSE NULL END, 0))  rr_redemp_p,      -- si_acc1
                                      --   MAX (NVL(CASE WHEN p.activity_id in ( 12,4,5,9,6,1,15,3,2,17 ) THEN sd.case_oi ELSE NULL END, 0))      pl_6_case_oi,    -- ebspricelist104
                                       MAX (CASE WHEN p.activity_id in ( 2 ) THEN CASE WHEN sd.case_oi = 0 THEN NULL ELSE sd.case_oi END ELSE NULL END)      pl_6_case_oi,    -- ebspricelist104
                                         SUM (NVL(CASE WHEN p.activity_id in ( 12,4,5,9,6,1,15,3,2,17 ) THEN sd.accrual_cd_s ELSE NULL END, 0))  pl_6_accrual_cd,    -- ebspricelist106
                                         SUM (NVL(CASE WHEN p.activity_id in ( 15 ) THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_6_accrual_coop,    --ebspricelist107
                                         SUM (NVL(CASE WHEN p.activity_id in ( 10 ) THEN sd.case_buydown ELSE NULL END, 0))  pl_13_accrual_cd,   -- mkt_input1
                                         SUM (NVL(CASE WHEN p.activity_id in ( 10 ) THEN sd.accrual_cd_s ELSE NULL END, 0))  pl_8_accrual_cd,    -- mkt_input_perc
                                       --  MAX (NVL(CASE WHEN p.activity_id in ( 10 ) THEN sd.case_oi ELSE NULL END, 0))      pl_13_case_oi,      --- mkt_input2
                                       MAX (NVL(CASE WHEN p.activity_id in ( 2 ) THEN sd.rr_oi_p ELSE NULL END, 0))      pl_13_case_oi,      --- mkt_input2
                                         SUM (NVL(CASE WHEN p.activity_id in ( 10 ) THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_8_accrual_coop, --- mkt_input3
                                         SUM (NVL(CASE WHEN p.activity_id in (12,4,5,9,6,1,15,3,2,17) THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_13_accrual_coop,  --- si_acc2
                                         --- Added Sanjiiv for Lumpsum Nonoffset value
                                         SUM (NVL(sd.accrual_coop, 0)) nonoff_accrual_coop                                                -- Sanjiiv     for nonoffsetLP
                                  FROM rr_sync_promo_offset_' || p_sync_ms || '_t sd,
                                       mdp_matrix          mdp,
                                       promotion p,
                                       tbl_cd_deal_type cdt,
                                       tbl_coop_deal_type coopt,
                                       tbl_oi_deal_type oit
                                  WHERE sd.item_id                   = mdp.item_id
                                  AND   sd.location_id               = mdp.location_id
                                  AND   sd.promotion_id       = p.promotion_id
                                  AND   NVL(p.cd_deal_type, 0)        = cdt.cd_deal_type_id
                                  AND   NVL(p.coop_deal_type, 0)      = coopt.coop_deal_type_id
                                  AND   NVL(p.oi_deal_type, 0)        = oit.oi_deal_type_id
                                  AND   sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                  AND   sd.rr_sync_sd         = 1
                                  GROUP BY sd.item_id,sd.location_id,sd.plan_date
                   ');

              dynamic_ddl (sql_str);
          dynamic_ddl('alter table rr_sync_promo_offset_g_' || p_sync_ms || '_t noparallel');        -- cmos 23-Jan-2017
              COMMIT;

              sql_str := 'CREATE UNIQUE INDEX rr_sync_promo_offset_g_' || p_sync_ms || '_idx ON rr_sync_promo_offset_g_' || p_sync_ms || '_t(item_id, location_id, plan_date)';
              dynamic_ddl (sql_str);
              COMMIT;

                    v_status := 'Start5 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


    -- Commenting out for temp issue -- INC0046165  -- SR

 --             sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
 --                       USING(SELECT /*+ FULL(sd) PARALLEL(sd, 64) INDEX(sd1, rr_sync_promo_offset_g_' || p_sync_ms || '_idx) */
 /*                       NVL(sd.item_id, sd1.item_id) item_id, NVL(sd.location_id, sd1.location_id) location_id,
                        NVL(sd.sales_date, sd1.plan_date) plan_date, sd1.incr_plan_non_offset,  sd1.incr_vol, sd1.engine_vol, sd1.pl_6_case_oi,
                        sd1.pl_6_oi_p, sd1.pl_6_accrual_cd, sd1.pl_6_accrual_coop, sd1.pl_13_case_oi, sd1.pl_13_oi_p, sd1.pl_13_accrual_cd,
                        sd1.pl_13_accrual_coop, sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,
                        sd1.pl_17_accrual_hf_cd, sd1.pl_13_accrual_hf_coop, sd1.pl_8_accrual_cd, sd1.pl_8_accrual_coop
                        FROM rr_sync_promo_offset_g_' || p_sync_ms || '_t  sd1 FULL OUTER JOIN
                        sales_data sd ON (sd.sales_date = sd1.plan_date
                        AND sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                        AND sd1.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''))
                        WHERE 1 = 1
                        AND (ABS(NVL(sd.sd_uplift, 0) - NVL(sd1.incr_plan_non_offset, 0)) +
                        ABS(NVL(sd.final_pos_fcst, 0) - NVL(sd1.incr_vol, 0)) +
                        ABS(NVL(sd.offset_engine_incr , 0) - NVL(sd1.engine_vol, 0)) +
                        ABS(NVL(sd.mkt_input1, 0) - NVL(sd1.pl_6_case_oi, 0)) +
                        ABS(NVL(sd.si_acc2, 0) - NVL(sd1.pl_6_oi_p, 0)) +
                        ABS(NVL(sd.mkt_input2, 0) - NVL(sd1.pl_6_accrual_cd, 0)) +
                        ABS(NVL(sd.mkt_input3, 0) - NVL(sd1.pl_6_accrual_coop, 0)) +
                        ABS(NVL(sd.mkt_acc2, 0) - NVL(sd1.pl_13_case_oi, 0)) +
                        ABS(NVL(sd.mkt_acc1, 0) - NVL(sd1.pl_13_oi_p, 0)) +
                        ABS(NVL(sd.mkt_acc3, 0) - NVL(sd1.pl_13_accrual_cd, 0)) +
                        ABS(NVL(sd.si_acc1, 0) - NVL(sd1.pl_13_accrual_coop, 0)) +
                        ABS(NVL(sd.ebspricelist104, 0) - NVL(sd1.pl_17_case_oi, 0)) +
                        ABS(NVL(sd.ebspricelist103, 0) - NVL(sd1.pl_17_oi_p, 0)) +
                        ABS(NVL(sd.ebspricelist106, 0) - NVL(sd1.pl_17_accrual_cd, 0)) +
                        ABS(NVL(sd.ebspricelist107, 0) - NVL(sd1.pl_17_accrual_coop, 0)) +
                        ABS(NVL(sd.cust_input1, 0) - NVL(sd1.pl_17_accrual_hf_cd, 0)) +
                        ABS(NVL(sd.cust_input2, 0) - NVL(sd1.pl_13_accrual_hf_coop, 0)) +
                        ABS(NVL(sd.tiv, 0) - NVL(sd1.pl_8_accrual_cd, 0)) +
                        ABS(NVL(sd.tiv_per_actual, 0) - NVL(sd1.pl_8_accrual_coop, 0))) <> 0) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET  sd_uplift = sd1.incr_plan_non_offset,
                        final_pos_fcst = sd1.incr_vol,
                        offset_engine_incr = sd1.engine_vol,
                        mkt_input1 = sd1.pl_6_case_oi,
                        si_acc2 = sd1.pl_6_oi_p,
                        mkt_input2 = sd1.pl_6_accrual_cd,
                        mkt_input3 = sd1.pl_6_accrual_coop,
                        mkt_acc2 = sd1.pl_13_case_oi,
                        mkt_acc1 = sd1.pl_13_oi_p,
                        mkt_acc3 = sd1.pl_13_accrual_cd,
                        si_acc1 = sd1.pl_13_accrual_coop,
                        ebspricelist104 = sd1.pl_17_case_oi,
                        ebspricelist103 = sd1.pl_17_oi_p,
                        ebspricelist106 = sd1.pl_17_accrual_cd,
                        ebspricelist107 = sd1.pl_17_accrual_coop,
                        cust_input1 = sd1.pl_17_accrual_hf_cd,
                        cust_input2 = sd1.pl_13_accrual_hf_coop,
                        tiv = sd1.pl_8_accrual_cd,
                        tiv_per_actual = sd1.pl_8_accrual_coop,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date, sd_uplift, final_pos_fcst, offset_engine_incr, mkt_input1, si_acc2, mkt_input2, mkt_input3,
                               mkt_acc2, mkt_acc1, mkt_acc3, si_acc1, ebspricelist104, ebspricelist103, ebspricelist106, ebspricelist107,
                               cust_input1, cust_input2, tiv, tiv_per_actual, last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.incr_plan_non_offset,  sd1.incr_vol, sd1.engine_vol, sd1.pl_6_case_oi, sd1.pl_6_oi_p,
                               sd1.pl_6_accrual_cd, sd1.pl_6_accrual_coop, sd1.pl_13_case_oi, sd1.pl_13_oi_p, sd1.pl_13_accrual_cd, sd1.pl_13_accrual_coop,
                               sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,
                               sd1.pl_17_accrual_hf_cd, sd1.pl_13_accrual_hf_coop, sd1.pl_8_accrual_cd, sd1.pl_8_accrual_coop, SYSDATE )';
 */



 -- New code for temp issue -- INC0046165 -- SR

 check_and_drop('RR_PRO_ACC_SYNC_TEMP');

 -- cmos 23-Jan-2017 (px ctas)
 -- cmos 28-Feb-2017 (px index in select on sd)
 sql_str := 'CREATE TABLE RR_PRO_ACC_SYNC_TEMP parallel 32 nologging AS SELECT /*+ index(sd,sales_data_pk) parallel(32) INDEX(sd1, rr_sync_promo_offset_g_' || p_sync_ms || '_idx) */
                        NVL(sd.item_id, sd1.item_id) item_id, NVL(sd.location_id, sd1.location_id) location_id,
                        NVL(sd.sales_date, sd1.plan_date) plan_date,
                        sd1.base_plan,  sd1.incr_vol, sd1.engine_vol, sd1.incr_plan_non_offset,  sd1.engine_incr_non_offset, sd1.promo_price,
                        sd1.case_buydown, sd1.rr_redemp_p, sd1.pl_6_case_oi, sd1.pl_6_accrual_cd,  sd1.pl_6_accrual_coop,
                        sd1.pl_13_accrual_cd,  sd1.pl_8_accrual_cd,  sd1.pl_13_case_oi, sd1.pl_8_accrual_coop, sd1.pl_13_accrual_coop,sd1.nonoff_accrual_coop                -- Added Sanjiiv for nonoffsetLP
                       --- sd1.pl_6_oi_p, sd1.pl_13_oi_p, sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,sd1.pl_17_accrual_hf_cd, sd1.pl_13_accrual_hf_coop,
                        FROM rr_sync_promo_offset_g_' || p_sync_ms || '_t  sd1 FULL OUTER JOIN
                        sales_data sd ON (sd.sales_date = sd1.plan_date
                        AND sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                        AND sd1.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''))
                        WHERE 1 = 1
                        AND
                        ABS(NVL(sd.final_pos_fcst, 0) - NVL(sd1.incr_vol, 0)) +
                        ABS(NVL(sd.offset_engine_incr , 0) - NVL(sd1.engine_vol, 0)) +
                        ABS(NVL(sd.sd_uplift, 0) - NVL(sd1.incr_plan_non_offset, 0)) +
                        ABS(NVL(sd.fin_ord_plan, 0) - NVL(sd1.promo_price, 0)) +
                        ABS(NVL(sd.ebspricelist103, 0) - NVL(sd1.case_buydown, 0)) +
                        ABS(NVL(sd.si_acc1, 0) -  NVL(sd1.rr_redemp_p, 0)) +
                        ABS(NVL(sd.ebspricelist104, 0) - NVL(sd1.pl_6_case_oi, 0)) +
                        ABS(NVL(sd.ebspricelist106, 0) - NVL(sd1.pl_6_accrual_cd, 0 )) +
                        ABS(NVL(sd.ebspricelist107, 0) - NVL(sd1.pl_6_accrual_coop, 0 )) +
                        ABS(NVL(sd.mkt_input1, 0) - NVL(sd1.pl_13_accrual_cd, 0)) +
                        ABS(NVL(sd.mkt_input_perc, 0) -  NVL(sd1.pl_8_accrual_cd, 0)) +
                        ABS(NVL(sd.mkt_input2, 0) -  NVL(sd1.pl_13_case_oi, 0)) +
                        ABS(NVL(sd.mkt_input3, 0) - NVL(sd1.pl_8_accrual_coop, 0)) +
                        ABS(NVL(sd.si_acc2, 0) - NVL(sd1.pl_13_accrual_coop, 0)) +
                        ABS(NVL(sd.rr_accrual_coop_noff, 0) - NVL(sd1.nonoff_accrual_coop, 0)) <> 0                -- Added Sanjiiv for nonoffset lumpsum
                        ';




            dynamic_ddl (sql_str);
        dynamic_ddl('alter table RR_PRO_ACC_SYNC_TEMP noparallel');        -- cmos 23-Jan-2017
            COMMIT;

            -- cmos 01-Mar-2016 (use px merge with px fts on sd and dop on select)
            -- rdas 02-Mar-2017 change merge parallel to 16 ( instead of 32 )
            sql_str := 'MERGE /*+ enable_parallel_dml full(sd) parallel(sd,16) */ INTO sales_data sd
                        USING(SELECT /*+ parallel(32) */ * FROM RR_PRO_ACC_SYNC_TEMP) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET
                        final_pos_fcst = sd1.incr_vol,
                        offset_engine_incr = sd1.engine_vol,
                        sd_uplift = sd1.incr_plan_non_offset,
                        fin_ord_plan = sd1.promo_price,
                        ebspricelist103 = sd1.case_buydown,
                        si_acc1 = sd1.rr_redemp_p,
                        ebspricelist104 = sd1.pl_6_case_oi,
                        ebspricelist106 = sd1.pl_6_accrual_cd,
                        ebspricelist107 = sd1.pl_6_accrual_coop,
                        mkt_input1 = sd1.pl_13_accrual_cd,
                        mkt_input_perc = sd1.pl_8_accrual_cd,
                        mkt_input2 = sd1.pl_13_case_oi,
                        mkt_input3 = sd1.pl_8_accrual_coop,
                        si_acc2 = sd1.pl_13_accrual_coop,
                        rr_accrual_coop_noff = sd1.nonoff_accrual_coop,            --- Added Sanjiiv
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date,  final_pos_fcst, offset_engine_incr, sd_uplift, fin_ord_plan, ebspricelist103, si_acc1, ebspricelist104 ,
                        ebspricelist106 , ebspricelist107 , mkt_input1 , mkt_input_perc , mkt_input2, mkt_input3 ,si_acc2 , rr_accrual_coop_noff,last_update_date)                -- Added sanjiiv + in values
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.incr_vol, sd1.engine_vol,sd1.incr_plan_non_offset, sd1.promo_price,sd1.case_buydown, sd1.rr_redemp_p, sd1.pl_6_case_oi,
                        sd1.pl_6_accrual_cd, sd1.pl_6_accrual_coop, sd1.pl_13_accrual_cd,  sd1.pl_8_accrual_cd, sd1.pl_13_case_oi, sd1.pl_8_accrual_coop, sd1.pl_13_accrual_coop,sd1.nonoff_accrual_coop, SYSDATE)';



            dynamic_ddl (sql_str);
            COMMIT;

            sql_str := 'DROP TABLE RR_PRO_ACC_SYNC_TEMP';
            --dynamic_ddl (sql_str);
            commit;

       -- New code completed --  INC0046165   -- SR

      ELSE

            v_status := 'Start6 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

           sql_str := 'MERGE /*+ INDEX(sd, SALES_DATA_PK) */ INTO sales_data sd
                        USING(SELECT /*+ full(sd) parallel(sd,64) */ sd.item_id,
                               sd.location_id,
                               sd.plan_date,
                               MAX(NVL(sd.promo_price, 0)) promo_price,
                               MAX(NVL(sd.case_buydown, 0)) case_buydown,
                               SUM(sd.base_plan_s) base_plan,
                               SUM(sd.incr_plan_s) incr_vol,
                               SUM(sd.engine_incr_s) engine_vol,
                               SUM(sd.sup_incr_plan) incr_plan_non_offset,
                               SUM(sd.sup_engine_incr) engine_incr_non_offset,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.case_oi ELSE NULL END, 0)) pl_6_case_oi,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_6_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 6 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_6_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 6 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_6_accrual_coop,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.case_oi ELSE NULL END, 0)) pl_13_case_oi,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_13_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 13 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_13_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 13 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_13_accrual_coop,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.case_oi ELSE NULL END, 0)) pl_17_case_oi,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_17_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 17 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_17_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 17 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_17_accrual_coop,
                               SUM (NVL(sd.accrual_hf_cd_s, 0)) pl_17_accrual_hf_cd,
                               SUM (NVL(sd.accrual_hf_coop_s, 0)) pl_13_accrual_hf_coop
                        FROM rr_sync_promo_offset_' || p_sync_ms || '_t sd,
                             mdp_matrix          mdp,
                             promotion p,
                             tbl_cd_deal_type cdt,
                             tbl_coop_deal_type coopt,
                             tbl_oi_deal_type oit
                        WHERE 1 = 1
                        AND   sd.item_id                   = mdp.item_id
                        AND   sd.location_id               = mdp.location_id
                        AND   sd.promotion_id       = p.promotion_id
                        AND   p.cd_deal_type        = cdt.cd_deal_type_id
                        AND   p.coop_deal_type      = coopt.coop_deal_type_id
                        AND   p.oi_deal_type        = oit.oi_deal_type_id
                        AND   sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                        AND   sd.rr_sync_sd         = 1
                        GROUP BY sd.item_id,sd.location_id,sd.plan_date) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET
                        mkt_acc2 = GREATEST(NVL(sd.mkt_acc2, 0), NVL(sd1.pl_13_case_oi, 0)),
                        mkt_acc1 = GREATEST(NVL(sd.mkt_acc1, 0), NVL(sd1.pl_13_oi_p, 0)),
                        mkt_acc3 = NVL(sd.mkt_acc3, 0) + sd1.pl_13_accrual_cd,
                        si_acc1 = NVL(sd.si_acc1, 0) + sd1.pl_13_accrual_coop,
                        ebspricelist104 = GREATEST(sd.ebspricelist104, sd1.pl_17_case_oi),
                        ebspricelist103 = GREATEST(NVL(sd.ebspricelist103, 0), NVL(sd1.pl_17_oi_p, 0)),
                        ebspricelist106 = NVL(sd.ebspricelist106, 0) + sd1.pl_17_accrual_cd,
                        ebspricelist107 = NVL(sd.ebspricelist107, 0) + sd1.pl_17_accrual_coop,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date,
                               mkt_acc2, mkt_acc1, mkt_acc3, si_acc1,
                               ebspricelist104, ebspricelist103, ebspricelist106, ebspricelist107,
                               last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date,
                               sd1.pl_13_case_oi, sd1.pl_13_oi_p, sd1.pl_13_accrual_cd, sd1.pl_13_accrual_coop,
                               sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,
                               SYSDATE )';

        ---    dynamic_ddl (sql_str);


         sql_str := 'MERGE /*+ INDEX(sd, SALES_DATA_PK) */ INTO sales_data sd
                        USING(SELECT /*+ full(sd) parallel(sd,64) */ sd.item_id,
                               sd.location_id,
                               sd.plan_date,
                              SUM (NVL(CASE WHEN p.activity_id in ( 7,8 ) THEN sd.case_buydown ELSE NULL END, 0)) case_buydown,    -- cust_input1
                              SUM (NVL(CASE WHEN p.activity_id in ( 7,8 ) THEN sd.accrual_cd_s ELSE NULL END, 0))  pl_6_accrual_cd,    -- cust_input2
                              SUM (NVL(CASE WHEN p.activity_id in (7,8) THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_13_accrual_coop,  --- tiv
                              SUM (NVL(sd.rr_ms_vol,0))  rr_ms_vol
                        FROM rr_sync_promo_offset_' || p_sync_ms || '_t sd,
                             mdp_matrix          mdp,
                             promotion p,
                             tbl_cd_deal_type cdt,
                             tbl_coop_deal_type coopt,
                             tbl_oi_deal_type oit
                        WHERE 1 = 1
                        AND   sd.item_id                   = mdp.item_id
                        AND   sd.location_id               = mdp.location_id
                        AND   sd.promotion_id       = p.promotion_id
                        AND   p.cd_deal_type        = cdt.cd_deal_type_id
                        AND   p.coop_deal_type      = coopt.coop_deal_type_id
                        AND   p.oi_deal_type        = oit.oi_deal_type_id
                        AND   sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                        AND   sd.rr_sync_sd         = 1
                        GROUP BY sd.item_id,sd.location_id,sd.plan_date) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET
                        cust_input1 = case_buydown,
                        cust_input2 = pl_6_accrual_cd,
                        tiv = pl_13_accrual_coop,
                        reg_raw_order = rr_ms_vol,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date,
                               cust_input1, cust_input2, tiv, reg_raw_order,last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date,
                               sd1.case_buydown, sd1.pl_6_accrual_cd, sd1.pl_13_accrual_coop, sd1.rr_ms_vol,SYSDATE)';


        dynamic_ddl (sql_str);



            COMMIT;
            
        ------------ Added Sanjiiv CR7 MS Permanent Fix
            v_status := 'Start 1 -';
            v_prog_name := 'RR_MS_PERMANENT_FIX';
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
            -- create a temp table - RR_PROMO_MS_TEMP and filter the eligible contract lines only  
            check_and_drop('RR_PROMO_MS_TEMP');
            dynamic_ddl('CREATE TABLE RR_PROMO_MS_TEMP (
                        promotion_id              number,
                        item_id                   number,
                        location_id              number,
                        plan_date                date)
                        tablespace ts_temp_tables NOLOGGING');
            ---
            check_and_drop('RR_PROMO_SD_MS_TEMP');
            dynamic_ddl('CREATE TABLE RR_PROMO_SD_MS_TEMP(
                        item_id                   number,
                        location_id              number,
                        sales_date               date,
                        cust_input2                 number,
                        tiv                         number)
                        tablespace ts_temp_tables NOLOGGING');
            check_and_drop('RR_BULK_DML_ERR_MSSG');
            dynamic_ddl('CREATE TABLE RR_BULK_DML_ERR_MSSG(
                        dml_number                   varchar2(100),
                        error_at                   varchar2(4000),
                        error_msg                varchar2(4000))
                        tablespace ts_temp_tables NOLOGGING');
            -- 1     
            v_status := 'Start 1.1 -';
            v_prog_name := 'RR_MS_PERMANENT_FIX';
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));    
            -- Check if the Table exists before processing 
            select count(1)
            into vn_count    
            from user_tables 
            where table_name = 'RR_SYNC_PROMO_OFFSET_1_T';
            
            IF vn_count = 1 THEN        -- Process only if the AllPromoSync Table exists - RR_SYNC_PROMO_OFFSET_1_T
                ---
                OPEN l_cursor FOR 
                'select tmp.promotion_id,
                      tmp. item_id,
                      tmp.location_id,
                      tmp.plan_date  plan_date
                from  rr_sync_promo_offset_'|| p_sync_ms||'_t tmp
                where (NVL(accrual_cd_s,0) <> 0 OR NVL(accrual_coop_s,0) <> 0)';
                LOOP
                    -- bulk fetch(read) operation
                    FETCH l_cursor BULK COLLECT INTO cls_ms_line_tbl LIMIT fetch_size;
                    -- bulk Update operation
                    BEGIN
                        FORALL i IN cls_ms_line_tbl.first .. cls_ms_line_tbl.last SAVE EXCEPTIONS
                            -- Bulk Insert into the TMP Table - RR_PROMO_MS_TEMP
                            EXECUTE IMMEDIATE 'INSERT INTO RR_PROMO_MS_TEMP VALUES (:1,:2,:3,:4)' using cls_ms_line_tbl(i).promotion_id,cls_ms_line_tbl(i).item_id,cls_ms_line_tbl(i).location_id,cls_ms_line_tbl(i).plan_date;
                            --            
                    EXCEPTION
                        WHEN DML_ERRORS THEN                           -- Now we figure out what failed and why.
                           vn_errors := SQL%BULK_EXCEPTIONS.COUNT;
                           --dbms_output.put_line('NO of Update statements that failed in OE_ORDER_LINES_ALL: ' ||vn_errors);
                           FOR i IN 1..vn_errors LOOP
                              sql_str := 'INSERT INTO RR_BULK_DML_ERR_MSSG VALUES (:1,:2,:3)'; 
                              EXECUTE IMMEDIATE sql_str USING 'DML - Insert1','Error #' || i || ' occurred during iteration # ' || SQL%BULK_EXCEPTIONS(i).ERROR_INDEX,'Error message is ' ||SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE);
                           END LOOP;
                        WHEN OTHERS THEN
                           null;  
                    END;
                    COMMIT;
                    EXIT WHEN l_cursor%NOTFOUND;
                END LOOP;
                CLOSE l_cursor;
                
                v_status := 'Start 1.2 - ';
                v_prog_name := 'RR_MS_PERMANENT_FIX';
                --v_package_name := 'RR_MS_PERMANENT_FIX';
                v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
                
                --/*
                -- 2 Bulk Collect SD DATA into the Temp Table 
                OPEN l_cursor FOR 
                SELECT /*+ APPEND PARALLEL index_ffs(tmp SALES_DATA_3IDX) parallel_index(tmp SALES_DATA_3IDX) */
                       tmp. item_id,
                       tmp.location_id,
                       tmp.sales_date,
                       nvl(tmp.cust_input2,0) cust_input2,
                       nvl(tmp.tiv,0)  tiv
                FROM   sales_data tmp 
                WHERE  sales_date between NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - (52 * 7) AND NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (76 * 7);
                LOOP
                    -- bulk fetch(read) operation
                    FETCH l_cursor BULK COLLECT INTO cls_ms_line_tbl1 LIMIT fetch_size;
                    -- bulk Update operation
                    BEGIN
                        FORALL i IN cls_ms_line_tbl1.first .. cls_ms_line_tbl1.last SAVE EXCEPTIONS
                            -- Bulk Insert into the TMP Table - RR_PROMO_SD_MS_TEMP
                            EXECUTE IMMEDIATE 'INSERT INTO RR_PROMO_SD_MS_TEMP VALUES (:1,:2,:3,:4,:5)' USING cls_ms_line_tbl1(i).item_id,cls_ms_line_tbl1(i).location_id,cls_ms_line_tbl1(i).sales_date,
                            cls_ms_line_tbl1(i).cust_input2,cls_ms_line_tbl1(i).tiv;
                            -- 
                    EXCEPTION
                        WHEN DML_ERRORS THEN                           -- Now we figure out what failed and why.
                           vn_errors := SQL%BULK_EXCEPTIONS.COUNT;
                           --dbms_output.put_line('NO of Update statements that failed in OE_ORDER_LINES_ALL: ' ||vn_errors);
                           FOR i IN 1..vn_errors LOOP
                              sql_str := 'INSERT INTO RR_BULK_DML_ERR_MSSG VALUES (:1,:2,:3)'; 
                              EXECUTE IMMEDIATE sql_str USING 'DML - Insert2','Error #' || i || ' occurred during iteration # ' || SQL%BULK_EXCEPTIONS(i).ERROR_INDEX,'Error message is ' ||SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE);
                           END LOOP;
                        WHEN OTHERS THEN
                           null;  
                    END;    
                    COMMIT;
                    EXIT WHEN l_cursor%NOTFOUND;
                END LOOP;
                CLOSE l_cursor;
                --- 
                v_status := 'Start 1.3 - ';
                v_prog_name := 'RR_MS_PERMANENT_FIX';
                v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
                --/*
                -- 3 Bulk Collect SD DATA into the Temp Table 
                -- 
                OPEN l_cursor FOR 
                'select tmp.item_id,
                        tmp.location_id,
                        tmp.sales_date
                from    RR_PROMO_SD_MS_TEMP TMP
                WHERE   (NVL(cust_input2,0) <> 0 OR NVL(tiv,0) <> 0)
                MINUS
                select  distinct tmp.item_id,
                        tmp.location_id,
                        tmp.plan_date
                from    RR_PROMO_MS_TEMP tmp';
                LOOP
                    -- bulk fetch(read) operation
                    FETCH l_cursor BULK COLLECT INTO cls_ms_line_tbl2 LIMIT fetch_size;
                    -- bulk Update operation
                    BEGIN
                        FORALL i IN cls_ms_line_tbl2.first .. cls_ms_line_tbl2.last SAVE EXCEPTIONS
                        --
                        UPDATE sales_data 
                        SET   cust_input2 = 0,
                              tiv           = 0,
                              last_update_date = SYSDATE
                        WHERE  item_id         = cls_ms_line_tbl2(i).item_id
                        AND    location_id     = cls_ms_line_tbl2(i).location_id 
                        AND    sales_date      = cls_ms_line_tbl2(i).sales_date;    
                        --    
                    EXCEPTION
                        WHEN DML_ERRORS THEN                           -- Now we figure out what failed and why.
                           vn_errors := SQL%BULK_EXCEPTIONS.COUNT;
                           --dbms_output.put_line('NO of Update statements that failed in OE_ORDER_LINES_ALL: ' ||vn_errors);
                           FOR i IN 1..vn_errors LOOP
                              sql_str := 'INSERT INTO RR_BULK_DML_ERR_MSSG VALUES (:1,:2,:3)'; 
                              EXECUTE IMMEDIATE sql_str USING 'DML - UPDATE1','Error #' || i || ' occurred during iteration # ' || SQL%BULK_EXCEPTIONS(i).ERROR_INDEX,'Error message is ' ||SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE);
                           END LOOP;
                        WHEN OTHERS THEN
                           null;  
                    END;
                    --    
                    COMMIT;
                    EXIT WHEN l_cursor%NOTFOUND;
                END LOOP;
                CLOSE l_cursor;
                --*/
            END IF;
            v_status := 'End - ';
            v_prog_name := 'RR_MS_PERMANENT_FIX';
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
            --    End Sanjiiv CR7 MS Permanent Fix     
            
      END IF;

    END IF;

          v_status := 'Start7 ';
          v_prog_name := 'PRC_PROMO_ACCRUAL_SYNC';            --- Added Sanjiiv for PRC_PROMO_ACCRUAL_SYNC logging
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    IF p_sync_flag IN (2, 3) THEN

      sql_str := 'MERGE INTO accrual a
                  USING (SELECT  DISTINCT p.promotion_id, p.promotion_code, p.promotion_desc, p.promotion_stat_id, p.promotion_type_id
                         FROM rr_promos_sync_' || p_sync_ms || '_t rpt,
                         promotion p
                         WHERE p.promotion_id = rpt.promotion_id
                         AND rpt.rr_sync_ad = 1) a1
                  ON(a.accrual_id = a1.promotion_id)
                  WHEN MATCHED THEN
                  UPDATE SET accrual_code = a1.promotion_code,
                             accrual_desc = a1.promotion_desc,
                             accrual_stat_id = a1.promotion_stat_id,
                             accrual_type_id = a1.promotion_type_id
                  WHEN NOT MATCHED THEN
                  INSERT (accrual_id, accrual_code, accrual_desc, accrual_stat_id, accrual_type_id)
                  VALUES (a1.promotion_id, a1.promotion_code, a1.promotion_desc, a1.promotion_stat_id, a1.promotion_type_id)';

      dynamic_ddl (sql_str);
      COMMIT;

            v_status := 'Start7 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      sql_str := 'MERGE INTO accrual_matrix am
                  USING (SELECT /*+ FULL(pm) PARALLEL(pm, 64) */ pm.item_id, pm.location_id, pm.promotion_id,
                  pm.promotion_stat_id, pm.promotion_type_id, pm.from_date - 42 from_date, pm.until_date + 42 until_date
                  FROM promotion_matrix pm,
                  rr_promos_sync_' || p_sync_ms || '_t rpt
                  WHERE rpt.promotion_id = pm.promotion_id
                  AND rpt.rr_sync_ad = 1
                  ) am1
                  ON(am.accrual_id = am1.promotion_id
                  AND am.item_id = am1.item_id
                  AND am.location_id = am1.location_id)
                  WHEN MATCHED THEN
                  UPDATE SET accrual_stat_id = am1.promotion_stat_id, accrual_type_id = am1.promotion_type_id,
                  --from_date = am1.from_date, until_date = am1.until_date,
                  accrual_data_lud = SYSDATE
                  WHERE NVL(am.accrual_stat_id, 0) <> NVL(am1.promotion_stat_id, 0)
                  OR NVL(am.accrual_type_id, 0) <> NVL(am1.promotion_type_id, 0)
                  -- OR NVL(am.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) >  NVL(am1.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))
                  -- OR NVL(am.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) < NVL(am1.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))
                  WHEN NOT MATCHED THEN
                  INSERT (accrual_id, item_id, location_id, accrual_stat_id, accrual_type_id /*, from_date, until_date */)
                  VALUES (am1.promotion_id, am1.item_id, am1.location_id, am1.promotion_stat_id, am1.promotion_type_id /*, am1.from_date, am1.until_date */)';

      dynamic_ddl (sql_str);
      COMMIT;

            v_status := 'Start8 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      sql_str := 'MERGE INTO /*+ INDEX(ad, rr_sync_promo_offset_' || p_sync_ms || '_idx) */ rr_sync_promo_offset_' || p_sync_ms || '_t ad
                  USING (SELECT   /*+ full(ad) parallel (ad, 64) */
                      ad.item_id, ad.location_id, ad.accrual_id, ad.sales_date plan_date,
                      0 rr_accrual_vol_base_ad,
                      0 rr_accrual_vol_incr_ad,
                      0 rr_accrual_vol_eng_incr_ad,
                      0 rr_accrual_vol_ad,
                      0 rr_accrual_cd_ad,
                      0 rr_accrual_coop_ad,
                      0 rr_accrual_hf_cd_ad,
                      0 rr_accrual_hf_coop_ad,
                      0 rr_accrual_vol_base_un_ad,
                      0 rr_accrual_vol_incr_un_ad,
                      0 rr_accrual_vol_eng_incr_un_ad,
                      0 rr_accrual_vol_un_ad,
                      0 rr_accrual_cd_un_ad,
                      0 rr_accrual_coop_un_ad,
                      0 rr_accrual_hf_cd_un_ad,
                      0 rr_accrual_hf_coop_un_ad,
                      rr_accrual_vol_base_commit rr_accrual_vol_base_commit,
                      rr_accrual_vol_incr_commit rr_accrual_vol_incr_commit,
                      rr_accrual_vol_eng_incr_commit rr_accrual_vol_eng_incr_commit,
                      rr_accrual_vol_commit rr_accrual_vol_commit,
                      rr_accrual_cd_commit rr_accrual_cd_commit,
                      rr_accrual_coop_commit rr_accrual_coop_commit,
                      rr_accrual_hf_cd_commit rr_accrual_hf_cd_commit,
                      rr_accrual_hf_coop_commit rr_accrual_hf_coop_commit,
                      rr_accrual_vol_base_un_commit rr_accrual_vol_base_un_commit,
                      rr_accrual_vol_incr_un_commit rr_accrual_vol_incr_un_commit,
                      rr_accrual_vol_eng_un_commit rr_accrual_vol_eng_un_commit,
                      rr_accrual_vol_un_commit rr_accrual_vol_un_commit,
                      rr_accrual_cd_un_commit rr_accrual_cd_un_commit,
                      rr_accrual_coop_un_commit rr_accrual_coop_un_commit,
                      rr_accrual_hf_cd_un_commit rr_accrual_hf_cd_un_commit,
                      rr_accrual_hf_coop_un_commit rr_accrual_hf_coop_un_commit,
                      ad.accrual_stat_id promotion_stat_id,
                      ad.accrual_type_id promotion_type_id,
                      1 rr_sync_ad,
                      0 rr_sync_sd,
                      0 rr_ms_vol
                  FROM accrual_data ad,
                  promotion_stat ps,
                  promotion_type pt
                  WHERE 1 = 1
                  AND ad.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                  AND ad.promotion_stat_id = ps.promotion_stat_id
                  AND ad.promotion_type_id = pt.promotion_type_id
                  AND pt.rr_margin_support = ' || p_sync_ms || '
                  AND NVL(pt.rr_sync_ad, 0) + NVL(pt.rr_sync_vol_ad, 0) > 0
                  AND NVL(ps.rr_sync_ad, 0) + NVL(ps.rr_sync_vol_ad, 0) > 0
                  AND ABS(NVL(ad.rr_accrual_vol_base_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_ad, 0))
                  + ABS(NVL(ad.rr_accrual_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_ad, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_commit, 0))
                  + ABS(NVL(ad.rr_accrual_cd_commit, 0)) + ABS(NVL(ad.rr_accrual_coop_commit, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_commit, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_un_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_un_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_cd_un_commit, 0)) + ABS(NVL(ad.rr_accrual_coop_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_un_commit, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_un_commit, 0))
                  + ABS(NVL(ad.rr_ms_vol, 0))
                  <> 0
                  ) ad1
                  ON (    ad1.item_id = ad.item_id
                  AND ad1.location_id = ad.location_id
                  AND ad1.accrual_id = ad.promotion_id
                  AND ad1.plan_date = ad.plan_date)
                  WHEN NOT MATCHED THEN
                  INSERT(promotion_id, item_id, location_id, sales_date, plan_date,
                         base_plan, incr_plan, engine_incr,
                         accrual_vol, accrual_cd, accrual_coop,
                         accrual_hf_cd, accrual_hf_coop,
                         base_plan_a, incr_plan_a, engine_incr_a,
                         accrual_vol_a, accrual_cd_a, accrual_coop_a,
                         accrual_hf_cd_a, accrual_hf_coop_a,
                         base_plan_commit, incr_plan_commit, engine_incr_commit,
                         accrual_vol_commit, accrual_cd_commit, accrual_coop_commit,
                         accrual_hf_cd_commit, accrual_hf_coop_commit,
                         base_plan_commit_a, incr_plan_commit_a, engine_incr_commit_a,
                         accrual_vol_commit_a, accrual_cd_commit_a, accrual_coop_commit_a,
                         accrual_hf_cd_commit_a, accrual_hf_coop_commit_a,
                         promotion_stat_id, promotion_type_id, rr_sync_ad, rr_sync_sd, rr_ms_vol)
                  VALUES(ad1.accrual_id, ad1.item_id, ad1.location_id, ad1.plan_date, ad1.plan_date,
                         ad1.rr_accrual_vol_base_un_ad, ad1.rr_accrual_vol_incr_un_ad, ad1.rr_accrual_vol_eng_incr_un_ad,
                         ad1.rr_accrual_vol_un_ad, ad1.rr_accrual_cd_un_ad, ad1.rr_accrual_coop_un_ad,
                         ad1.rr_accrual_hf_cd_un_ad, ad1.rr_accrual_hf_coop_un_ad,
                         ad1.rr_accrual_vol_base_ad, ad1.rr_accrual_vol_incr_ad, ad1.rr_accrual_vol_eng_incr_ad,
                         ad1.rr_accrual_vol_ad, ad1.rr_accrual_cd_ad, ad1.rr_accrual_coop_ad,
                         ad1.rr_accrual_hf_cd_ad, ad1.rr_accrual_hf_coop_ad,
                         ad1.rr_accrual_vol_base_un_commit, ad1.rr_accrual_vol_incr_un_commit, ad1.rr_accrual_vol_eng_un_commit,
                         ad1.rr_accrual_vol_un_commit, ad1.rr_accrual_cd_un_commit, ad1.rr_accrual_coop_un_commit,
                         ad1.rr_accrual_hf_cd_un_commit, ad1.rr_accrual_hf_coop_un_commit,
                         ad1.rr_accrual_vol_base_commit, ad1.rr_accrual_vol_incr_commit, ad1.rr_accrual_vol_eng_incr_commit,
                         ad1.rr_accrual_vol_commit, ad1.rr_accrual_cd_commit, ad1.rr_accrual_coop_commit,
                         ad1.rr_accrual_hf_cd_commit, ad1.rr_accrual_hf_coop_commit,
                         ad1.promotion_stat_id, ad1.promotion_type_id, ad1.rr_sync_ad, ad1.rr_sync_ad, ad1.rr_ms_vol)';

      --dynamic_ddl (sql_str);

      check_and_drop('rr_promo_ad_rev_' || p_sync_ms || '_t');

      -- cmos 23-Jan-2017 (px ctas)
      dynamic_ddl('CREATE TABLE rr_promo_ad_rev_' || p_sync_ms || '_t
                  TABLESPACE ts_temp_tables parallel 32 nologging AS
                  SELECT * FROM (
                  SELECT /*+ ordered  full(ad) parallel(ad) */  /* parallel(ad) */
                      ad.item_id, ad.location_id, ad.accrual_id, ad.sales_date plan_date,
                      0 rr_accrual_vol_base_ad,
                      0 rr_accrual_vol_incr_ad,
                      0 rr_accrual_vol_eng_incr_ad,
                      0 rr_accrual_vol_ad,
                      0 rr_accrual_cd_ad,
                      0 rr_accrual_coop_ad,
                      0 rr_accrual_hf_cd_ad,
                      0 rr_accrual_hf_coop_ad,
                      0 rr_accrual_vol_base_un_ad,
                      0 rr_accrual_vol_incr_un_ad,
                      0 rr_accrual_vol_eng_incr_un_ad,
                      0 rr_accrual_vol_un_ad,
                      0 rr_accrual_cd_un_ad,
                      0 rr_accrual_coop_un_ad,
                      0 rr_accrual_hf_cd_un_ad,
                      0 rr_accrual_hf_coop_un_ad,
                      rr_accrual_vol_base_commit rr_accrual_vol_base_commit,
                      rr_accrual_vol_incr_commit rr_accrual_vol_incr_commit,
                      rr_accrual_vol_eng_incr_commit rr_accrual_vol_eng_incr_commit,
                      rr_accrual_vol_commit rr_accrual_vol_commit,
                      rr_accrual_cd_commit rr_accrual_cd_commit,
                      rr_accrual_coop_commit rr_accrual_coop_commit,
                      rr_accrual_hf_cd_commit rr_accrual_hf_cd_commit,
                      rr_accrual_hf_coop_commit rr_accrual_hf_coop_commit,
                      rr_accrual_vol_base_un_commit rr_accrual_vol_base_un_commit,
                      rr_accrual_vol_incr_un_commit rr_accrual_vol_incr_un_commit,
                      rr_accrual_vol_eng_un_commit rr_accrual_vol_eng_un_commit,
                      rr_accrual_vol_un_commit rr_accrual_vol_un_commit,
                      rr_accrual_cd_un_commit rr_accrual_cd_un_commit,
                      rr_accrual_coop_un_commit rr_accrual_coop_un_commit,
                      rr_accrual_hf_cd_un_commit rr_accrual_hf_cd_un_commit,
                      rr_accrual_hf_coop_un_commit rr_accrual_hf_coop_un_commit,
                      ad.accrual_stat_id promotion_stat_id,
                      ad.accrual_type_id promotion_type_id,
                      1 rr_sync_ad,
                      0 rr_sync_sd,
                      0 rr_ms_vol
                  FROM accrual_data ad,
                  (SELECT * FROM promotion_stat WHERE 1 = 1
                  AND NVL(rr_sync_ad, 0) + NVL(rr_sync_vol_ad, 0) > 0) ps,
                  (SELECT * FROM promotion_type WHERE 1 = 1
                   AND rr_margin_support = ' || p_sync_ms || '
                   AND NVL(rr_sync_ad, 0) + NVL(rr_sync_vol_ad, 0) > 0) pt
                  WHERE 1 = 1
                  AND ad.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                  --AND ad.sales_date BETWEEN and
                  AND ad.promotion_stat_id = ps.promotion_stat_id
                  AND ad.promotion_type_id = pt.promotion_type_id
                  AND ABS(NVL(ad.rr_accrual_vol_base_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_ad, 0))
                  + ABS(NVL(ad.rr_accrual_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_ad, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_un_ad, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_commit, 0))
                  + ABS(NVL(ad.rr_accrual_cd_commit, 0)) + ABS(NVL(ad.rr_accrual_coop_commit, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_commit, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_base_un_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_un_commit, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_vol_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_cd_un_commit, 0)) + ABS(NVL(ad.rr_accrual_coop_un_commit, 0))
                  + ABS(NVL(ad.rr_accrual_hf_cd_un_commit, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_un_commit, 0))
                  + ABS(NVL(ad.rr_ms_vol, 0))
                  <> 0) ad1
                  WHERE 1 = 1
                  AND NOT EXISTS (SELECT /*+ INDEX(ad2, rr_sync_promo_offset_' || p_sync_ms || '_idx) */ 1
                                 FROM rr_sync_promo_offset_' || p_sync_ms || '_t ad2
                                 WHERE ad2.item_id = ad1.item_id
                                 AND ad2.location_id = ad1.location_id
                                 AND ad2.promotion_id = ad1.accrual_id
                                 AND ad2.plan_date = ad1.plan_date
                                 AND ROWNUM=1)');
      dynamic_ddl('alter table rr_promo_ad_rev_' || p_sync_ms || '_t noparallel');        -- cmos 23-Jan-2017


      dynamic_ddl('INSERT /*+ APPEND NOLOGGING */ INTO rr_sync_promo_offset_' || p_sync_ms || '_t (promotion_id, item_id, location_id, sales_date, plan_date,
                         base_plan, incr_plan, engine_incr,
                         accrual_vol, accrual_cd, accrual_coop,
                         accrual_hf_cd, accrual_hf_coop,
                         base_plan_a, incr_plan_a, engine_incr_a,
                         accrual_vol_a, accrual_cd_a, accrual_coop_a,
                         accrual_hf_cd_a, accrual_hf_coop_a,
                         base_plan_commit, incr_plan_commit, engine_incr_commit,
                         accrual_vol_commit, accrual_cd_commit, accrual_coop_commit,
                         accrual_hf_cd_commit, accrual_hf_coop_commit,
                         base_plan_commit_a, incr_plan_commit_a, engine_incr_commit_a,
                         accrual_vol_commit_a, accrual_cd_commit_a, accrual_coop_commit_a,
                         accrual_hf_cd_commit_a, accrual_hf_coop_commit_a,
                         promotion_stat_id, promotion_type_id, rr_sync_ad, rr_sync_sd, rr_ms_vol)
      SELECT ad1.accrual_id, ad1.item_id, ad1.location_id, ad1.plan_date, ad1.plan_date,
                         ad1.rr_accrual_vol_base_un_ad, ad1.rr_accrual_vol_incr_un_ad, ad1.rr_accrual_vol_eng_incr_un_ad,
                         ad1.rr_accrual_vol_un_ad, ad1.rr_accrual_cd_un_ad, ad1.rr_accrual_coop_un_ad,
                         ad1.rr_accrual_hf_cd_un_ad, ad1.rr_accrual_hf_coop_un_ad,
                         ad1.rr_accrual_vol_base_ad, ad1.rr_accrual_vol_incr_ad, ad1.rr_accrual_vol_eng_incr_ad,
                         ad1.rr_accrual_vol_ad, ad1.rr_accrual_cd_ad, ad1.rr_accrual_coop_ad,
                         ad1.rr_accrual_hf_cd_ad, ad1.rr_accrual_hf_coop_ad,
                         ad1.rr_accrual_vol_base_un_commit, ad1.rr_accrual_vol_incr_un_commit, ad1.rr_accrual_vol_eng_un_commit,
                         ad1.rr_accrual_vol_un_commit, ad1.rr_accrual_cd_un_commit, ad1.rr_accrual_coop_un_commit,
                         ad1.rr_accrual_hf_cd_un_commit, ad1.rr_accrual_hf_coop_un_commit,
                         ad1.rr_accrual_vol_base_commit, ad1.rr_accrual_vol_incr_commit, ad1.rr_accrual_vol_eng_incr_commit,
                         ad1.rr_accrual_vol_commit, ad1.rr_accrual_cd_commit, ad1.rr_accrual_coop_commit,
                         ad1.rr_accrual_hf_cd_commit, ad1.rr_accrual_hf_coop_commit,
                         ad1.promotion_stat_id, ad1.promotion_type_id, ad1.rr_sync_ad, ad1.rr_sync_ad, ad1.rr_ms_vol
                         FROM rr_promo_ad_rev_' || p_sync_ms || '_t ad1');

      COMMIT;

      v_status := 'Start9 ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      sql_str := 'MERGE /*+ INDEX(sd, accrual_data_1141_pk) */  INTO accrual_data ad
                  USING (SELECT   /*+ full(ad) parallel (ad, 64) */
                      ad.item_id, ad.location_id, ad.promotion_id accrual_id, ad.plan_date,
                      SUM(base_plan) rr_accrual_vol_base_un_ad, SUM(incr_plan) rr_accrual_vol_incr_un_ad, SUM(engine_incr) rr_accrual_vol_eng_incr_un_ad,
                      SUM(accrual_vol) rr_accrual_vol_un_ad, SUM(accrual_cd) rr_accrual_cd_un_ad, SUM(accrual_coop) rr_accrual_coop_un_ad,
                      SUM(accrual_hf_cd) rr_accrual_hf_cd_un_ad, SUM(accrual_hf_coop) rr_accrual_hf_coop_un_ad,
                      SUM(base_plan_a) rr_accrual_vol_base_ad, SUM(incr_plan_a) rr_accrual_vol_incr_ad, SUM(engine_incr_a) rr_accrual_vol_eng_incr_ad,
                      SUM(accrual_vol_a) rr_accrual_vol_ad, SUM(accrual_cd_a) rr_accrual_cd_ad, SUM(accrual_coop_a) rr_accrual_coop_ad,
                      SUM(accrual_hf_cd_a) rr_accrual_hf_cd_ad, SUM(accrual_hf_coop_a) rr_accrual_hf_coop_ad,
                      SUM(base_plan_commit) rr_accrual_vol_base_un_commit, SUM(incr_plan_commit) rr_accrual_vol_incr_un_commit,
                      SUM(engine_incr_commit) rr_accrual_vol_eng_un_commit,
                      SUM(accrual_vol_commit) rr_accrual_vol_un_commit, SUM(accrual_cd_commit) rr_accrual_cd_un_commit,
                      SUM(accrual_coop_commit) rr_accrual_coop_un_commit,
                      SUM(accrual_hf_cd_commit) rr_accrual_hf_cd_un_commit, SUM(accrual_hf_coop_commit) rr_accrual_hf_coop_un_commit,
                      SUM(base_plan_commit_a) rr_accrual_vol_base_commit, SUM(incr_plan_commit_a) rr_accrual_vol_incr_commit,
                      SUM(engine_incr_commit_a) rr_accrual_vol_eng_incr_commit,
                      SUM(accrual_vol_commit_a) rr_accrual_vol_commit, SUM(accrual_cd_commit_a) rr_accrual_cd_commit, SUM(accrual_coop_commit_a) rr_accrual_coop_commit,
                      SUM(accrual_hf_cd_commit_a) rr_accrual_hf_cd_commit, SUM(accrual_hf_coop_commit_a) rr_accrual_hf_coop_commit,
                      MAX(promotion_stat_id) promotion_stat_id, MAX(promotion_type_id) promotion_type_id, SUM(rr_ms_vol) rr_ms_vol
                  FROM rr_sync_promo_offset_' || p_sync_ms || '_t ad
                  WHERE 1 = 1
                  AND ad.rr_sync_ad = 1
                  AND ad.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                  GROUP BY ad.item_id, ad.location_id, ad.promotion_id, ad.plan_date
                  ) ad1
                  ON (  ad1.item_id = ad.item_id
                  AND ad1.location_id = ad.location_id
                  AND ad1.accrual_id = ad.accrual_id
                  AND ad1.plan_date = ad.sales_date)
                  WHEN MATCHED THEN
                  UPDATE SET ad.rr_accrual_vol_base_un_commit = ad1.rr_accrual_vol_base_un_commit,
                  ad.rr_accrual_vol_incr_un_commit = ad1.rr_accrual_vol_incr_un_commit, ad.rr_accrual_vol_eng_un_commit = ad1.rr_accrual_vol_eng_un_commit,
                  ad.rr_accrual_vol_un_commit = ad1.rr_accrual_vol_un_commit, ad.rr_accrual_cd_un_commit = ad1.rr_accrual_cd_un_commit,
                  ad.rr_accrual_coop_un_commit = ad1.rr_accrual_coop_un_commit,
                  ad.rr_accrual_hf_cd_un_commit = ad1.rr_accrual_hf_cd_un_commit, ad.rr_accrual_hf_coop_un_commit = ad1.rr_accrual_hf_coop_un_commit,
                  ad.rr_accrual_vol_base_commit = ad1.rr_accrual_vol_base_commit,
                  ad.rr_accrual_vol_incr_commit = ad1.rr_accrual_vol_incr_commit, ad.rr_accrual_vol_eng_incr_commit = ad1.rr_accrual_vol_eng_incr_commit,
                  ad.rr_accrual_vol_commit = ad1.rr_accrual_vol_commit, ad.rr_accrual_cd_commit = ad1.rr_accrual_cd_commit,
                  ad.rr_accrual_coop_commit = ad1.rr_accrual_coop_commit,
                  ad.rr_accrual_hf_cd_commit = ad1.rr_accrual_hf_cd_commit, ad.rr_accrual_hf_coop_commit = ad1.rr_accrual_hf_coop_commit,
                  ad.rr_accrual_vol_base_un_ad = ad1.rr_accrual_vol_base_un_ad,
                  ad.rr_accrual_vol_incr_un_ad = ad1.rr_accrual_vol_incr_un_ad, ad.rr_accrual_vol_eng_incr_un_ad = ad1.rr_accrual_vol_eng_incr_un_ad,
                  ad.rr_accrual_vol_un_ad = ad1.rr_accrual_vol_un_ad, ad.rr_accrual_cd_un_ad = ad1.rr_accrual_cd_un_ad,
                  ad.rr_accrual_coop_un_ad = ad1.rr_accrual_coop_un_ad,
                  ad.rr_accrual_hf_cd_un_ad = ad1.rr_accrual_hf_cd_un_ad, ad.rr_accrual_hf_coop_un_ad = ad1.rr_accrual_hf_coop_un_ad,
                  ad.rr_accrual_vol_base_ad = ad1.rr_accrual_vol_base_ad,
                  ad.rr_accrual_vol_incr_ad = ad1.rr_accrual_vol_incr_ad, ad.rr_accrual_vol_eng_incr_ad = ad1.rr_accrual_vol_eng_incr_ad,
                  ad.rr_accrual_vol_ad = ad1.rr_accrual_vol_ad, ad.rr_accrual_cd_ad = ad1.rr_accrual_cd_ad, ad.rr_accrual_coop_ad = ad1.rr_accrual_coop_ad,
                  ad.rr_accrual_hf_cd_ad = ad1.rr_accrual_hf_cd_ad, ad.rr_accrual_hf_coop_ad = ad1.rr_accrual_hf_coop_ad,
                  ad.accrual_stat_id = ad1.promotion_stat_id, ad.accrual_type_id = ad1.promotion_type_id,
                  ad.promotion_stat_id = ad1.promotion_stat_id, ad.promotion_type_id = ad1.promotion_type_id,
                  ad.rr_ms_vol = ad1.rr_ms_vol,
                  ad.last_update_date = SYSDATE
                  WHERE 1 = 2
                  OR NVL(ad.rr_accrual_vol_base_un_ad, 0) <> NVL(ad1.rr_accrual_vol_base_un_ad, 0)
                  OR NVL(ad.rr_accrual_vol_incr_un_ad, 0) <> NVL(ad1.rr_accrual_vol_incr_un_ad, 0)
                  OR NVL(ad.rr_accrual_vol_eng_incr_un_ad, 0) <> NVL(ad1.rr_accrual_vol_eng_incr_un_ad, 0)
                  OR NVL(ad.rr_accrual_vol_un_ad, 0) <> NVL(ad1.rr_accrual_vol_un_ad, 0)
                  OR NVL(ad.rr_accrual_cd_un_ad, 0) <> NVL(ad1.rr_accrual_cd_un_ad, 0)
                  OR NVL(ad.rr_accrual_coop_un_ad, 0) <> NVL(ad1.rr_accrual_coop_un_ad, 0)
                  OR NVL(ad.rr_accrual_hf_cd_un_ad, 0) <> NVL(ad1.rr_accrual_hf_cd_un_ad, 0)
                  OR NVL(ad.rr_accrual_hf_coop_un_ad, 0) <> NVL(ad1.rr_accrual_hf_coop_un_ad, 0)
                  OR NVL(ad.rr_accrual_vol_base_ad, 0) <> NVL(ad1.rr_accrual_vol_base_ad, 0)
                  OR NVL(ad.rr_accrual_vol_incr_ad, 0) <> NVL(ad1.rr_accrual_vol_incr_ad, 0)
                  OR NVL(ad.rr_accrual_vol_eng_incr_ad, 0) <> NVL(ad1.rr_accrual_vol_eng_incr_ad, 0)
                  OR NVL(ad.rr_accrual_vol_ad, 0) <> NVL(ad1.rr_accrual_vol_ad, 0)
                  OR NVL(ad.rr_accrual_cd_ad, 0) <> NVL(ad1.rr_accrual_cd_ad, 0)
                  OR NVL(ad.rr_accrual_coop_ad, 0) <> NVL(ad1.rr_accrual_coop_ad, 0)
                  OR NVL(ad.rr_accrual_hf_cd_ad, 0) <> NVL(ad1.rr_accrual_hf_cd_ad, 0)
                  OR NVL(ad.rr_accrual_hf_coop_ad, 0) <> NVL(ad1.rr_accrual_hf_coop_ad, 0)
                  OR NVL(ad.rr_accrual_vol_base_un_commit, 0) <> NVL(ad1.rr_accrual_vol_base_un_commit, 0)
                  OR NVL(ad.rr_accrual_vol_incr_un_commit, 0) <> NVL(ad1.rr_accrual_vol_incr_un_commit, 0)
                  OR NVL(ad.rr_accrual_vol_eng_un_commit, 0) <> NVL(ad1.rr_accrual_vol_eng_un_commit, 0)
                  OR NVL(ad.rr_accrual_vol_un_commit, 0) <> NVL(ad1.rr_accrual_vol_un_commit, 0)
                  OR NVL(ad.rr_accrual_cd_un_commit, 0) <> NVL(ad1.rr_accrual_cd_un_commit, 0)
                  OR NVL(ad.rr_accrual_coop_un_commit, 0) <> NVL(ad1.rr_accrual_coop_un_commit, 0)
                  OR NVL(ad.rr_accrual_hf_cd_un_commit, 0) <> NVL(ad1.rr_accrual_hf_cd_un_commit, 0)
                  OR NVL(ad.rr_accrual_hf_coop_un_commit, 0) <> NVL(ad1.rr_accrual_hf_coop_un_commit, 0)
                  OR NVL(ad.rr_accrual_vol_base_commit, 0) <> NVL(ad1.rr_accrual_vol_base_commit, 0)
                  OR NVL(ad.rr_accrual_vol_incr_commit, 0) <> NVL(ad1.rr_accrual_vol_incr_commit, 0)
                  OR NVL(ad.rr_accrual_vol_eng_incr_commit, 0) <> NVL(ad1.rr_accrual_vol_eng_incr_commit, 0)
                  OR NVL(ad.rr_accrual_vol_commit, 0) <> NVL(ad1.rr_accrual_vol_commit, 0)
                  OR NVL(ad.rr_accrual_cd_commit, 0) <> NVL(ad1.rr_accrual_cd_commit, 0)
                  OR NVL(ad.rr_accrual_coop_commit, 0) <> NVL(ad1.rr_accrual_coop_commit, 0)
                  OR NVL(ad.rr_accrual_hf_cd_commit, 0) <> NVL(ad1.rr_accrual_hf_cd_commit, 0)
                  OR NVL(ad.rr_accrual_hf_coop_commit, 0) <> NVL(ad1.rr_accrual_hf_coop_commit, 0)
                  OR NVL(ad.promotion_stat_id, 0) <> NVL(ad1.promotion_stat_id, 0)
                  OR NVL(ad.promotion_type_id, 0) <> NVL(ad1.promotion_type_id, 0)
                  OR NVL(ad.accrual_stat_id, 0) <> NVL(ad1.promotion_stat_id, 0)
                  OR NVL(ad.accrual_type_id, 0) <> NVL(ad1.promotion_type_id, 0)
                  OR NVL(ad.rr_ms_vol, 0) <> NVL(ad1.rr_ms_vol, 0)
                  WHEN NOT MATCHED THEN
                  INSERT (accrual_id, item_id, location_id, sales_date, rr_accrual_vol_base_un_ad,
                          rr_accrual_vol_incr_un_ad, rr_accrual_vol_eng_incr_un_ad,
                          rr_accrual_vol_un_ad, rr_accrual_cd_un_ad, rr_accrual_coop_un_ad,
                          rr_accrual_hf_cd_un_ad, rr_accrual_hf_coop_un_ad,
                          rr_accrual_vol_base_ad, rr_accrual_vol_incr_ad, rr_accrual_vol_eng_incr_ad,
                          rr_accrual_vol_ad, rr_accrual_cd_ad, rr_accrual_coop_ad,
                          rr_accrual_hf_cd_ad, rr_accrual_hf_coop_ad,
                          rr_accrual_vol_base_un_commit, rr_accrual_vol_incr_un_commit, rr_accrual_vol_eng_un_commit,
                          rr_accrual_vol_un_commit, rr_accrual_cd_un_commit, rr_accrual_coop_un_commit,
                          rr_accrual_hf_cd_un_commit, rr_accrual_hf_coop_un_commit,
                          rr_accrual_vol_base_commit, rr_accrual_vol_incr_commit, rr_accrual_vol_eng_incr_commit,
                          rr_accrual_vol_commit, rr_accrual_cd_commit, rr_accrual_coop_commit,
                          rr_accrual_hf_cd_commit, rr_accrual_hf_coop_commit,
                          accrual_stat_id, accrual_type_id, promotion_stat_id, promotion_type_id, rr_ms_vol)
                  VALUES (ad1.accrual_id, ad1.item_id, ad1.location_id, ad1.plan_date,
                          ad1.rr_accrual_vol_base_un_ad, ad1.rr_accrual_vol_incr_un_ad, ad1.rr_accrual_vol_eng_incr_un_ad,
                          ad1.rr_accrual_vol_un_ad, ad1.rr_accrual_cd_un_ad, ad1.rr_accrual_coop_un_ad,
                          ad1.rr_accrual_hf_cd_un_ad, ad1.rr_accrual_hf_coop_un_ad,
                          ad1.rr_accrual_vol_base_ad, ad1.rr_accrual_vol_incr_ad, ad1.rr_accrual_vol_eng_incr_ad,
                          ad1.rr_accrual_vol_ad, ad1.rr_accrual_cd_ad, ad1.rr_accrual_coop_ad,
                          ad1.rr_accrual_hf_cd_ad, ad1.rr_accrual_hf_coop_ad,
                          ad1.rr_accrual_vol_base_un_commit, ad1.rr_accrual_vol_incr_un_commit, ad1.rr_accrual_vol_eng_un_commit,
                          ad1.rr_accrual_vol_un_commit, ad1.rr_accrual_cd_un_commit, ad1.rr_accrual_coop_un_commit,
                          ad1.rr_accrual_hf_cd_un_commit, ad1.rr_accrual_hf_coop_un_commit,
                          ad1.rr_accrual_vol_base_commit, ad1.rr_accrual_vol_incr_commit, ad1.rr_accrual_vol_eng_incr_commit,
                          ad1.rr_accrual_vol_commit, ad1.rr_accrual_cd_commit, ad1.rr_accrual_coop_commit,
                          ad1.rr_accrual_hf_cd_commit, ad1.rr_accrual_hf_coop_commit,
                          ad1.promotion_stat_id, ad1.promotion_type_id, ad1.promotion_stat_id, ad1.promotion_type_id, ad1.rr_ms_vol)';

      dynamic_ddl (sql_str);
      COMMIT;

            v_status := 'Start10 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      sql_str := 'MERGE INTO accrual_matrix am
                  USING (SELECT /*+ full(ad) parallel (ad, 32) */ ad.item_id, ad.location_id, ad.accrual_id,
                  max(ad.sales_date) until_date, min(ad.sales_date) from_date
                  FROM accrual_data ad,
                  rr_promos_sync_' || p_sync_ms || '_t rpt
                  WHERE rpt.promotion_id = ad.accrual_id
                  GROUP BY ad.item_id, ad.location_id, ad.accrual_id
                  ) am1
                  ON(am.accrual_id = am1.accrual_id
                  AND am.item_id = am1.item_id
                  AND am.location_id = am1.location_id)
                  WHEN MATCHED THEN
                  UPDATE SET from_date = am1.from_date, until_date = am1.until_date, accrual_data_lud = SYSDATE
                  WHERE 1 = 2
                  OR NVL(am.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) >  NVL(am1.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))
                  OR NVL(am.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) < NVL(am1.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))';

      dynamic_ddl (sql_str);
      COMMIT;

    END IF;


    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

    PROCEDURE prc_accrual_over_spend(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_ACCRUAL_OVER_SPEND
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 52;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_ACCRUAL_OVER_SPEND';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    check_and_drop('rr_promos_os_t');

    -- cmos 23-Jan-2017 (px ctas)
    sql_str := 'CREATE TABLE rr_promos_os_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ a.accrual_id,
               CASE WHEN SUM(NVL(ad.rr_accrual_cd_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_cd_ad, 0)) THEN 1 ELSE 0 END rr_case_deal_os,
               CASE WHEN SUM(NVL(ad.rr_accrual_coop_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_coop_ad, 0)) THEN 1 ELSE 0 END rr_coop_os,
               CASE WHEN SUM(NVL(ad.rr_accrual_hf_cd_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_hf_cd_ad, 0)) THEN 1 ELSE 0 END rr_hf_case_deal_os,
               CASE WHEN SUM(NVL(ad.rr_accrual_hf_coop_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_hf_coop_ad, 0)) THEN 1 ELSE 0 END rr_hf_coop_os,
               SUM(NVL(ad.rr_approved_claim_cd_ad, 0)) - SUM(NVL(ad.rr_accrual_cd_ad_gl, 0)) rr_case_deal_os_amt,
               SUM(NVL(ad.rr_approved_claim_coop_ad, 0)) - SUM(NVL(ad.rr_accrual_coop_ad_gl, 0)) rr_coop_os_amt,
               SUM(NVL(ad.rr_approved_claim_hf_cd_ad, 0)) - SUM(NVL(ad.rr_accrual_hf_cd_ad_gl, 0)) rr_hf_case_deal_os_amt,
               SUM(NVL(ad.rr_approved_claim_hf_coop_ad, 0)) - SUM(NVL(ad.rr_accrual_hf_coop_ad_gl, 0)) rr_hf_coop_os_amt,
               SUM(NVL(ad.rr_accrual_cd_ad_gl, 0)) rr_case_deal_ttl_amt,
               SUM(NVL(ad.rr_accrual_coop_ad_gl, 0)) rr_coop_ttl_amt,
               SUM(NVL(ad.rr_accrual_hf_cd_ad_gl, 0)) rr_hf_case_deal_ttl_amt,
               SUM(NVL(ad.rr_accrual_hf_coop_ad_gl, 0)) rr_hf_coop_ttl_amt,
               COUNT(1) rr_case_deal_cnt,
               COUNT(1) rr_coop_cnt,
               COUNT(1) rr_hf_case_deal_cnt,
               COUNT(1) rr_hf_coop_cnt
               FROM promotion p,
               accrual a,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt,
               accrual_data ad
               WHERE 1 = 1
               AND p.promotion_id = a.accrual_id
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_sync_ad = 1 AND ps.rr_sync_ad = 1
               AND p.promotion_id = pdt.promotion_id
               AND a.accrual_id = ad.accrual_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               GROUP BY a.accrual_id
               HAVING SUM(NVL(ad.rr_accrual_cd_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_cd_ad, 0))
               OR SUM(NVL(ad.rr_accrual_coop_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_coop_ad, 0))
               OR SUM(NVL(ad.rr_accrual_hf_cd_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_hf_cd_ad, 0))
               OR SUM(NVL(ad.rr_accrual_hf_coop_ad_gl, 0)) < SUM(NVL(ad.rr_approved_claim_hf_coop_ad, 0))';

    dynamic_ddl (sql_str);
    dynamic_ddl('alter table rr_promos_os_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str := 'MERGE INTO accrual_data ad
                USING(SELECT ad.item_id, ad.location_id, ad.sales_date, ad.accrual_id,
                CASE WHEN rpt.rr_case_deal_os = 1 THEN rpt.rr_case_deal_os_amt * CASE WHEN rpt.rr_case_deal_ttl_amt = 0 THEN 1/rpt.rr_case_deal_cnt ELSE SAFE_DIVISION(ad.rr_accrual_cd_ad_gl, rpt.rr_case_deal_ttl_amt, 0) END ELSE 0 END rr_accrual_os_cd_ad,
                CASE WHEN rpt.rr_coop_os = 1 THEN rpt.rr_coop_os_amt * CASE WHEN rpt.rr_coop_ttl_amt = 0 THEN 1/rpt.rr_coop_cnt ELSE SAFE_DIVISION(ad.rr_accrual_coop_ad_gl, rpt.rr_coop_ttl_amt, 0) END ELSE 0 END rr_accrual_os_coop_ad,
                CASE WHEN rpt.rr_hf_case_deal_os = 1 THEN rpt.rr_hf_case_deal_os_amt * CASE WHEN rpt.rr_hf_case_deal_ttl_amt = 0 THEN 1/rpt.rr_hf_case_deal_cnt ELSE SAFE_DIVISION(ad.rr_accrual_hf_cd_ad_gl, rpt.rr_hf_case_deal_ttl_amt, 0) END ELSE 0 END rr_accrual_os_hf_cd_ad,
                CASE WHEN rpt.rr_hf_coop_os = 1 THEN rpt.rr_hf_coop_os_amt * CASE WHEN rpt.rr_hf_coop_ttl_amt = 0 THEN 1/rpt.rr_hf_coop_cnt ELSE SAFE_DIVISION(ad.rr_accrual_hf_coop_ad_gl, rpt.rr_hf_coop_ttl_amt, 0) END ELSE 0 END rr_accrual_os_hf_coop_ad
                FROM accrual_data ad,
                rr_promos_os_t rpt
                WHERE ad.accrual_id = rpt.accrual_id)ad1
                ON(ad.accrual_id = ad1.accrual_id
                AND ad.item_id = ad1.item_id
                AND ad.location_id = ad1.location_id
                AND ad.sales_date = ad1.sales_date)
                WHEN MATCHED THEN
                UPDATE SET rr_accrual_os_cd_ad = ad1.rr_accrual_os_cd_ad,
                rr_accrual_os_coop_ad = ad1.rr_accrual_os_coop_ad,
                rr_accrual_os_hf_cd_ad = ad1.rr_accrual_os_hf_cd_ad,
                rr_accrual_os_hf_coop_ad = ad1.rr_accrual_os_hf_coop_ad,
                last_update_date = SYSDATE';

    dynamic_ddl (sql_str);
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

    PROCEDURE prc_margin_support(p_start_date  DATE,
                                 p_end_date    DATE,
                                 p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_MARGIN_SUPPORT
   PURPOSE:    Calculates latest margin support

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 52;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_MARGIN_SUPPORT';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

        --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') + 7
    INTO max_sales_date
    FROM DUAL;

    check_and_drop('rr_promos_ms_calc_t');

    -- cmos 23-Jan-2017 (px ctas)
    sql_str := 'CREATE TABLE rr_promos_ms_calc_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 64) */ p.promotion_id
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_margin_support = 1
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_ms_calc_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str:= 'MERGE INTO promotion_data pd
               USING(SELECT pd.promotion_id, pd.item_id, pd.location_id, pd.sales_date,
        --       CASE WHEN NVL(sd.actual_quantity, 0) <> 0 THEN NVL(sd.actual_quantity, 0)
        --       ELSE DECODE(NVL(sd.demand_type, 0),1,(NVL(sd.exfact_base,0)* (nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)),
        --       NVL(sd.unexfact_or,(NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || get_fore_col(0, v_eng_profile) || ', sd.npi_forecast)))*
        --      (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)))) END
        --      vol,
              DECODE( pt.rr_ms_exfact_pos , 0 , 0,
              1, CASE WHEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0)  <> 0 THEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0)
                 ELSE (NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || get_fore_col(0, v_eng_profile) || ', sd.npi_forecast))) * (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)) END,
              --2, CASE WHEN NVL(sd.actual_quantity, 0) <> 0 THEN NVL(sd.actual_quantity, 0)
              -- Changed Sanjiiv as per issue raised by Asahi Mail Subject - WW 30 pack cans margin support on 21092016
              2, CASE WHEN pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')-14  THEN NVL(sd.actual_quantity, 0)
                  ELSE DECODE(NVL(sd.demand_type, 0),1,(NVL(sd.exfact_base,0)* (nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)),
                  NVL(sd.unexfact_or,(NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || get_fore_col(0, v_eng_profile) || ', sd.npi_forecast)))*
                  (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)))) END ) vol,
              CASE WHEN p.approval = 1 AND ((pd.accruals_type_id IN (0) AND (pd.sales_date = TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')))
                         OR
                         (pd.sales_date < TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND p.accruals_type_id = 0 AND pd.promotion_stat_id >= 4))
                    THEN 1
                    WHEN p.approval = 1 AND pd.promotion_stat_id >= 4 AND pd.accruals_type_id IN (1)  THEN 2
                    ELSE pd.accruals_type_id
               END accruals_type_id,
               CASE
                    WHEN p.approval = 1 AND NVL(sd.actual_quantity, 0) <> 0 AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7
                    THEN 6
                    WHEN p.approval = 1 AND pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7 AND pd.sales_date <= TO_DATE(''' || TO_CHAR(max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                    THEN 5
                    WHEN pd.promotion_stat_id >= 4 AND pd.promotion_stat_id < 7
                    THEN 4
                    ELSE pd.promotion_stat_id
               END promotion_stat_id
                     FROM promotion p,
                     promotion_data pd,
                     sales_data sd,
                     rr_promos_ms_calc_t ms,
                     promotion_type pt
                     WHERE p.promotion_id = ms.promotion_id
                     AND p.promotion_id = pd.promotion_id
                     AND p.promotion_type_id = pt.promotion_type_id
                     AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                       AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                     AND pd.item_id = sd.item_id
                     AND pd.location_id = sd.location_id
                     AND pd.is_Self = 1 -- lannapra Added 15 May 2015
                     AND pd.sales_date = sd.sales_date) pd1
                ON(pd.promotion_id = pd1.promotion_id
                AND pd.item_id = pd1.item_id
                AND pd.location_id = pd1.location_id
                AND pd.sales_date = pd1.sales_date )
                WHEN MATCHED THEN
                UPDATE SET rr_accrual_cd_pd = pd1.vol * pd.case_buydown,
                rr_accrual_coop_pd = pd.event_cost,
                --rr_accrual_vol_pd = pd1.vol,
                rr_pl_cd_pd = pd1.vol * pd.case_buydown,
                rr_pl_coop_pd = pd.event_cost,
                rr_ms_vol = pd1.vol,
                accruals_type_id = pd1.accruals_type_id,
                promotion_stat_id = pd1.promotion_stat_id,
                last_update_date = SYSDATE';

    dynamic_ddl(sql_str);
    COMMIT;

    sql_str := 'MERGE INTO promotion_matrix pm
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 32) */ pd.promotion_id,
               --pd.item_id, pd.location_id,
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_ms_calc_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id
               --, pd.item_id, pd.location_id
               ) pm1
               ON(pm.promotion_id = pm1.promotion_id
               --AND pm.item_id = pm1.item_id
               --AND pm.location_id = pm1.location_id
               )
               WHEN MATCHED THEN
               UPDATE SET pm.promotion_stat_id = pm1.promotion_stat_id, pm.accruals_type_id = pm1.accruals_type_id,
               pm.promotion_data_lud = SYSDATE
               ';

    dynamic_ddl(sql_str);
    COMMIT;
    ---

    sql_str := 'MERGE INTO promotion p
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 32) */ pd.promotion_id,
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_ms_calc_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN
               UPDATE SET p.promotion_stat_id = p1.promotion_stat_id,
               p.accruals_type_id = p1.accruals_type_id
               ';

    dynamic_ddl(sql_str);
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

    PROCEDURE prc_calc_accrual_close(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_CALC_ACCRUAL_CLOSE
   PURPOSE:    Calculates latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 52;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_CALC_ACCRUAL_CLOSE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    check_and_drop('rr_promos_close_t');

    -- cmos 23-Jan-2017 (px ctas)
    sql_str := 'CREATE TABLE rr_promos_close_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_stat ps,
               promotion_type pt,
               promotion_dates pdt
               WHERE 1 = 1
               AND pt.promotion_type_id = p.promotion_type_id
               AND ps.promotion_stat_id = p.promotion_stat_id
               AND p.promotion_id = pdt.promotion_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_accrual_close = 1
               AND ps.rr_accrual_close = 1
               AND p.rr_export_close = 0
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') ';

    dynamic_ddl (sql_str);
    dynamic_ddl('alter table rr_promos_close_t noparallel');        -- cmos 23-Jan-2017
    COMMIT;

    sql_str := 'MERGE INTO accrual_data ad
                USING(SELECT ad.item_id, ad.location_id, ad.sales_date, ad.accrual_id,
                NVL(ad.rr_approved_claim_cd_ad, 0) - (NVL(ad.rr_accrual_cd_ad_gl, 0) + NVL(ad.rr_accrual_os_cd_ad_gl, 0)) rr_accrual_close_cd_ad,
                NVL(ad.rr_approved_claim_coop_ad, 0) - (NVL(ad.rr_accrual_coop_ad_gl, 0) + NVL(ad.rr_accrual_os_coop_ad_gl, 0)) rr_accrual_close_coop_ad,
                NVL(ad.rr_approved_claim_hf_cd_ad, 0) - (NVL(ad.rr_accrual_hf_cd_ad_gl, 0) + NVL(ad.rr_accrual_os_hf_cd_ad_gl, 0)) rr_accrual_close_hf_cd_ad,
                NVL(ad.rr_approved_claim_hf_coop_ad, 0) - (NVL(ad.rr_accrual_hf_coop_ad_gl, 0) + NVL(ad.rr_accrual_os_hf_coop_ad_gl, 0)) rr_accrual_close_hf_coop_ad
                FROM accrual_data ad,
                rr_promos_close_t rpt
                WHERE ad.accrual_id = rpt.promotion_id) ad1
                ON(ad.accrual_id = ad1.accrual_id
                AND ad.item_id = ad1.item_id
                AND ad.location_id = ad1.location_id
                AND ad.sales_date = ad1.sales_date)
                WHEN MATCHED THEN
                UPDATE SET rr_accrual_close_cd_ad = ad1.rr_accrual_close_cd_ad,
                rr_accrual_close_coop_ad = ad1.rr_accrual_close_coop_ad,
                rr_accrual_close_hf_cd_ad = ad1.rr_accrual_close_hf_cd_ad,
                rr_accrual_close_hf_coop_ad = ad1.rr_accrual_close_hf_coop_ad,
                last_update_date = SYSDATE,
                accrual_stat_id = 8,
                promotion_stat_id = 8';

    dynamic_ddl (sql_str);
    COMMIT;

    sql_str := 'MERGE INTO accrual_matrix am
                USING(SELECT promotion_id
                FROM rr_promos_close_t) am1
                ON(am.accrual_id = am1.promotion_id)
                WHEN MATCHED THEN
                UPDATE SET am.accrual_stat_id = 8';

    dynamic_ddl (sql_str);
    COMMIT;

    sql_str := 'MERGE INTO accrual a
                USING(SELECT promotion_id
                FROM rr_promos_close_t) a1
                ON(a.accrual_id = a1.promotion_id)
                WHEN MATCHED THEN
                UPDATE SET a.accrual_stat_id = 8';

    dynamic_ddl (sql_str);
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

END;

/
