--------------------------------------------------------
--  DDL for Package Body PKG_CLAIMS_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_CLAIMS_LM" 
AS

/******************************************************************************
   NAME:       PKG_PTP
   PURPOSE:    All procedures commanly used for claims module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   PROCEDURE prc_reverse_claim(user_id     NUMBER DEFAULT NULL,
                               level_id    NUMBER DEFAULT NULL,
                               member_id   NUMBER DEFAULT NULL)
   AS
   
   sql_str        VARCHAR2 (10000);
   v_prog_name    VARCHAR2 (100)  := 'PRC_REVERSE_CLAIM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   new_claim_id   NUMBER;
   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_REVERSE_CLAIM';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
        
       -- INSERT DUPLICATE ROWS INTO CLAIM--
       INSERT INTO dup_claim
          SELECT *
            FROM settlement
           WHERE settlement_id = member_id
           AND NVL(rr_default_claim, 0) = 0;
    
       UPDATE dup_claim
          SET settlement_id = settlement_seq.NEXTVAL,
              settlement_code = settlement_seq.NEXTVAL,
              date_posted = SYSDATE,
              last_update_date = SYSDATE,
              gst = -gst,
              claim_case_deal_d = -claim_case_deal_d,
              co_op_$ = -co_op_$,
              claim_case_deal_hf_d = -claim_case_deal_hf_d,
              claim_coop_hf = -claim_coop_hf,
              gl_exp_claim = 0,
              gl_exp_claim_detail = 0,
              settlement_status_id=8;  --- added FN 6/10/15 linked status
    
       INSERT INTO settlement
          SELECT *
            FROM dup_claim;
    
       COMMIT;
       
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;
   
   PROCEDURE prc_reverse_group_claim(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL)
   AS
   
   sql_str        VARCHAR2 (10000);
   v_prog_name    VARCHAR2 (100)  := 'PRC_REVERSE_CLAIM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   new_claim_id   NUMBER;
   v_claim        VARCHAR2 (1000);
   v_cust_div     NUMBER;
   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_REVERSE_CLAIM';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
        
         --If Nec Create Glob Temp Tables:--
       /*
    
       CREATE GLOBAL TEMPORARY TABLE DUP_CLAIM
       ON COMMIT DELETE ROWS
       NOCACHE
       as select * from settlement
       where 1=2
    
        */
        
       --Get Claim Description and Customer Division
       select settlement_desc, t_Ep_e1_cust_cat_3_Ep_id
       into v_claim, v_cust_div 
       from settlement
       where settlement_id = member_id
       AND NVL(rr_default_claim, 0) = 0; 
       
       
       -- INSERT DUPLICATE ROWS INTO CLAIM--
       INSERT INTO dup_claim
          SELECT *
            FROM settlement
           WHERE settlement_desc = v_claim
             --AND t_ep_e1_cust_cat_3_ep_id = v_cust_div
             ;
    
        UPDATE dup_claim
          SET settlement_id = settlement_seq.NEXTVAL,
              settlement_code = settlement_seq.NEXTVAL,
              date_posted = SYSDATE,
              last_update_date = SYSDATE,
              gst = -gst,
              claim_case_deal_d = -claim_case_deal_d,
              co_op_$ = -co_op_$,
              claim_case_deal_hf_d = -claim_case_deal_hf_d,
              claim_coop_hf = -claim_coop_hf,
              gl_exp_claim = 0,
              gl_exp_claim_detail = 0,
              settlement_status_id=8;  --- added FN 6/10/15 linked status
              
       INSERT INTO settlement
          SELECT *
            FROM dup_claim;
    
       COMMIT;
       
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;

   PROCEDURE prc_reset_approved_claim(user_id     NUMBER DEFAULT NULL,
                                      level_id    NUMBER DEFAULT NULL,
                                      member_id   NUMBER DEFAULT NULL)
   AS
   
   sql_str        VARCHAR2 (10000);
   v_prog_name    VARCHAR2 (100)  := 'PRC_RESET_APPROVED_CLAIM';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   new_claim_id   NUMBER;
   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_RESET_APPROVED_CLAIM';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
        
       UPDATE settlement 
        SET settlement_status_id = 8,
          gl_exp_claim = 0, 
          last_update_date = SYSDATE
        WHERE settlement_id = member_id
        AND gl_exp_claim <> 1
        AND gl_exp_claim_detail <> 1
        AND settlement_status_id = 4
        AND NVL(rr_default_claim, 0) = 0;
    
       COMMIT;
       
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;


   PROCEDURE prc_reset_group_claims(user_id     NUMBER DEFAULT NULL,
                                    level_id    NUMBER DEFAULT NULL,
                                    member_id   NUMBER DEFAULT NULL)
   AS
   
   sql_str        VARCHAR2 (10000);
   v_prog_name    VARCHAR2 (100)  := 'PRC_RESET_GROUP_CLAIMS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   
   v_claim_desc   VARCHAR2(2000); 
   v_claim_status NUMBER;
   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_RESET_GROUP_CLAIMS';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
        
       SELECT settlement_desc, settlement_status_id
       INTO v_claim_desc, v_claim_status
       FROM settlement
       WHERE settlement_id = member_id
       AND NVL(rr_default_claim, 0) = 0;
        
       IF v_claim_desc IS NOT NULL
       THEN 
            
            UPDATE settlement 
            SET settlement_status_id = 8, 
                gl_exp_claim = 0,
                last_update_date = SYSDATE
            WHERE settlement_desc = v_claim_desc 
            AND settlement_status_id = v_claim_status          -- Reset only lines that have same status
            AND settlement_status_id in (4,7)
            AND gl_exp_claim <> 1
            AND gl_exp_claim_detail <> 1;

            COMMIT;
       
       END IF;
       
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;
   
   PROCEDURE prc_createas_settlement(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL)
   AS
      
   v_prog_name    VARCHAR2 (100)  := 'PRC_CREATEAS_SETTLEMENT';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   
   counter        NUMBER;
   rec            settlement%ROWTYPE;
   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_CREATEAS_SETTLEMENT';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
       

       SELECT s.number_of_rows
              INTO counter
         FROM settlement s
        WHERE s.settlement_id = member_id;
    
       IF counter IS NULL OR counter <= 0
       THEN
          counter := 1;
       END IF;
        
       --counter := number_of_rows;
       SELECT * INTO rec
       FROM settlement s
       WHERE s.settlement_id = member_id;
       
       WHILE counter <> 0
       LOOP

                INSERT INTO settlement
                            (settlement_id, settlement_code,
                             settlement_desc, date_posted, start_date, end_date,
                             settlement_type_id, settlement_status_id,
                             key_account, key_account_state, corporate_customer, customer_name,
                             brand, promotion_group, promoted_product, settlement_owner_id,
                             t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_10_ep_id, t_ep_e1_cust_cat_4_EP_ID, t_ep_ebs_customer_EP_ID, t_ep_ebs_account_EP_ID,		-- Sanjiiv Banner Group Change -- removed column t_ep_ebs_zone_EP_ID
                             --case_deal, case_hf_deal, unit_deal, unit_hf_deal,
                             inv_date, bill_to,
                             gl_code_id, settlement_amount, last_update_date,t_ep_l_att_6_ep_id,sub_banner   --- added sales_office
                            )
                     VALUES (settlement_seq.NEXTVAL, settlement_seq.NEXTVAL,
                             rec.settlement_desc, SYSDATE, rec.start_date,
                             rec.end_date, rec.settlement_type_id,
                             1, rec.key_account, rec.key_account_state, rec.corporate_customer, rec.customer_name,
                             rec.brand, rec.promotion_group, rec.promoted_product, rec.settlement_owner_id,
                             rec.t_ep_e1_cust_cat_2_ep_id, rec.t_ep_l_att_10_ep_id, rec.t_ep_e1_cust_cat_4_EP_ID, rec.t_ep_ebs_customer_EP_ID, rec.t_ep_ebs_account_EP_ID, -- Sanjiiv BG Change -- removed rec.t_ep_ebs_zone_EP_ID,
                             --rec.case_deal, rec.case_hf_deal, rec.unit_deal, rec.unit_hf_deal,
                             rec.inv_date, rec.bill_to,
                             rec.gl_code_id, 0, SYSDATE, rec.t_ep_l_att_6_ep_id, rec.sub_banner
                            );
    
                COMMIT;
                counter := counter - 1;
                  
       END LOOP;
              
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;   

   PROCEDURE prc_delete_claims(user_id     NUMBER DEFAULT NULL,
                               level_id    NUMBER DEFAULT NULL,
                               member_id   NUMBER DEFAULT NULL)
   AS
      
   v_prog_name    VARCHAR2 (100)  := 'PRC_DELETE_CLAIMS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_DELETE_CLAIMS';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
       
       DELETE FROM settlement
       WHERE settlement_id = member_id
       AND settlement_status_id not in (4,7)
       AND nvl(settlement_number,0) = 0            --LP 18/05/2011            --LP 18/05/2011
       AND gl_exp_claim <> 1
       AND gl_exp_claim_detail <> 1
       AND NVL(rr_default_claim, 0) = 0;
       --AND settlement_desc not like 'Default %'; --MSH 16-Nov-2010 Commented out for SD ticket78843.
      
       COMMIT;       
              
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;   

   PROCEDURE prc_delete_group_claims(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL)
   AS
      
   v_prog_name         VARCHAR2 (100)  := 'PRC_DELETE_GROUP_CLAIMS';
   v_status            VARCHAR2 (100);
   v_proc_log_id       NUMBER;
   
   v_claim_desc        settlement.settlement_desc%TYPE;
   v_claim_status      settlement.settlement_status_id%TYPE;
   v_claim_sales_div   settlement.t_ep_e1_cust_cat_3_ep_id%TYPE;

   
   BEGIN
   
       pre_logon;
       v_status := 'Start ';
       v_prog_name := 'PRC_DELETE_GROUP_CLAIMS';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
       
       SELECT settlement_desc, settlement_status_id, t_ep_e1_cust_cat_3_ep_id
       INTO v_claim_desc, v_claim_status, v_claim_sales_div
       FROM settlement
       WHERE settlement_id = member_id
       AND NVL(rr_default_claim, 0) = 0;
       --AND settlement_desc not like 'Default %'; -- MSH Commented out for SD 78843
    
       IF v_claim_desc IS NOT NULL THEN 
       
         DELETE FROM settlement
               WHERE settlement_desc = v_claim_desc 
                 AND settlement_status_id = v_claim_status          -- Delete only lines that have same status
                 AND t_ep_e1_cust_cat_3_ep_id = v_claim_sales_div   -- Delete only lines that have same Sales Division
                 AND settlement_status_id NOT IN (4,7)
                 AND nvl(settlement_number,0) = 0            --LP 18/05/2011
                 AND gl_exp_claim <> 1
                 AND gl_exp_claim_detail <> 1;
      
         COMMIT;
       
       END IF;       
              
       v_status := 'end ';
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
   END;   
   
    PROCEDURE prc_link_claim (user_id     NUMBER,
                             level_id    NUMBER,
                             member_id   NUMBER)
   AS
   
    datevar       NUMBER (10);
    datevarind    NUMBER (10);
    
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;  
    
    mindate       DATE;
    maxdate       DATE;
    v_debug       NUMBER := 0;
    v_debug_count NUMBER  := 0;
    v_day         VARCHAR2(10);
    
    v_batch_dt    VARCHAR2(20);
    claim_no      VARCHAR2(2000);
   
   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LINK_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
    
   SELECT settlement_desc
     INTO claim_no
     FROM settlement
    WHERE settlement_id = member_id
    AND NVL(rr_default_claim, 0) = 0;
    
        
    -------------Start Check Duplicat Claim---------------
    
    
  --  dynamic_ddl('TRUNCATE TABLE tbl_duplicate_claims');
      check_and_drop('tbl_dup_claim_' || user_id);
    COMMIT;

   dynamic_ddl(' create table tbl_dup_claim_' || user_id || ' as 
    (
        SELECT MIN(settlement_id) settlement_id ,
        NVL(s.settlement_desc, ''0'') settlement_desc,
        NVL(s.key_account_state, 0) key_account_state,
        NVL(s.key_account, 0) key_account,
        NVL(s.corporate_customer, 0) corporate_customer,
        NVL(s.customer_name, 0) customer_name,
        NVL(s.brand, 0) brand,
        NVL(s.promotion_group, 0) promotion_group,
        NVL(s.promoted_product, 0) promoted_product,
        0 promotion_id,
        NVL(s.case_deal, 0) case_deal,
        NVL(s.unit_deal, 0) unit_deal,
        NVL(s.case_hf_deal, 0) case_hf_deal,
        NVL(s.unit_hf_deal, 0) unit_hf_deal,
        ROUND(NVL(s.claim_case_deal_d, 0), 2) claim_case_deal_d, 
        ROUND(NVL(s.claim_case_deal_hf_d, 0), 2) claim_case_deal_hf_d,
        ROUND(NVL(s.claim_coop_hf, 0), 2) claim_coop_hf,
        ROUND(NVL(s.co_op_$, 0), 2) co_op_$,
        nvl(s.start_date, to_date(''01-01-1900'', ''DD-MM-YYYY'')) start_date, 
        nvl(s.end_date, to_date(''01-01-2100'', ''DD-MM-YYYY'')) end_date,
        NVL(s.sub_banner, 0) sub_banner
        FROM settlement s
        WHERE s.rr_default_claim =0
        AND s.date_posted BETWEEN (SYSDATE - 365) AND SYSDATE
        AND s.settlement_desc = ''' ||claim_no ||'''
        AND s.settlement_status_id <> 4
        GROUP BY NVL(s.settlement_desc, ''0''),
        NVL(s.key_account_state, 0),
        NVL(s.key_account, 0),
        NVL(s.corporate_customer, 0),
        nvl(s.customer_name, 0),
        NVL(s.sub_banner, 0) ,
        NVL(s.brand, 0),
        NVL(s.promotion_group, 0),
        NVL(s.promoted_product, 0),
        NVL(s.case_deal, 0),
        NVL(s.unit_deal, 0),
        NVL(s.case_hf_deal, 0),
        NVL(s.unit_hf_deal, 0),
        ROUND(NVL(s.claim_case_deal_d, 0), 2), 
        ROUND(NVL(s.claim_case_deal_hf_d, 0), 2),
        ROUND(NVL(s.claim_coop_hf, 0), 2),
        ROUND(NVL(s.co_op_$, 0), 2),
        NVL(s.start_date, to_date(''01-01-1900'', ''DD-MM-YYYY'')), 
        NVL(s.end_date, to_date(''01-01-2100'', ''DD-MM-YYYY''))
        HAVING COUNT(1) > 1
    )');

    COMMIT;

dynamic_ddl('
    MERGE INTO settlement s
    USING ( 
        SELECT s.settlement_id, s1.settlement_id duplicate_to
        FROM settlement s, tbl_dup_claim_' || user_id || ' s1 
        WHERE 1=1
        AND s.settlement_status_id NOT IN (4, 7) 
        AND s.settlement_id <> s1.settlement_id
        AND s.rr_default_claim = 0
        AND s.settlement_desc = ''' ||claim_no ||'''
        AND s.date_posted BETWEEN (SYSDATE - 365) AND SYSDATE
        AND NVL(s.settlement_desc, ''0'') = s1.settlement_desc
        AND NVL(s.key_account_state, 0) = s1.key_account_state
        AND NVL(s.key_account, 0) = s1.key_account
        AND NVL(s.corporate_customer, 0) = s1.corporate_customer
        and nvl(s.customer_name, 0) = s1.customer_name
        and nvl(s.sub_banner, 0 ) = s1.sub_banner
        AND NVL(s.brand, 0) = s1.brand
        AND NVL(s.promotion_group, 0) = s1.promotion_group
        AND NVL(s.promoted_product, 0) = s1.promoted_product
        AND NVL(s.case_deal, 0) = s1.case_deal
        AND NVL(s.unit_deal, 0) = s1.unit_deal
        AND NVL(s.case_hf_deal, 0) = s1.case_hf_deal
        AND NVL(s.unit_hf_deal, 0) = s1.unit_hf_deal
        AND  ROUND(NVL(s.claim_case_deal_d, 0), 2) = s1.claim_case_deal_d
        AND ROUND(NVL(s.claim_case_deal_hf_d, 0), 2) = s1.claim_case_deal_hf_d
        AND ROUND(NVL(s.claim_coop_hf, 0), 2) = s1.claim_coop_hf
        AND ROUND(NVL(s.co_op_$, 0), 2) = s1.co_op_$
        AND NVL(s.start_date, to_date(''01-01-1900'', ''DD-MM-YYYY'')) = s1.start_date 
        AND NVL(s.end_date, to_date(''01-01-2100'', ''DD-MM-YYYY'')) = s1.end_date
        ) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN
        UPDATE SET s.settlement_status_id = 5, s.duplicate_to = s1.duplicate_to');    
 
    COMMIT;        
    
    
    -------------END Check Duplicat Claim-----------------
    
    get_param ('sys_params', 'DSMOIShipDateDifference', datevar);
    get_param ('sys_params', 'DSMOIShipDateDifferenceInd', datevarind);
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';    
    
    mindate := NEXT_DAY(ADD_MONTHS(SYSDATE, -12), v_day) -7;
    
        -----Reset Claims from Unlinked to New, ready for Re-linking----
    UPDATE settlement
       SET settlement_status_id = 1,
           promotion_id = 0,
           last_update_date = sysdate
      WHERE settlement_status_id in  (9,10,12)  --LP 29/11
       AND settlement_desc = claim_no;
     
    COMMIT; 
    
    check_and_drop('rr_claim_matrix_' || user_id );
    
    dynamic_ddl('create table rr_claim_matrix_' || user_id || ' as
          SELECT  
                       s.settlement_id, 
                       m.item_id,
                       m.location_id,
                       n.rr_claim_max_cd
          FROM settlement s,
          mdp_matrix m,
          t_ep_ebs_customer n
          WHERE 1=0');
    
    FOR rec IN (SELECT DISTINCT 
                DECODE(NVL (s.key_account_state, 0), 0, 0, 1) kas_flag, 
                DECODE(NVL (s.key_account, 0), 0, 0, 1) ka_flag, 
                DECODE(NVL (s.corporate_customer, 0), 0, 0, 1) cc_flag, 
                decode(nvl (s.customer_name, 0), 0, 0, 1) cn_flag, 
                DECODE(NVL (s.sub_banner, 0), 0, 0, 1) sb_flag,   --- added sales_office 12/11/15
                DECODE(NVL (s.brand, 0), 0, 0, 1) b_flag, 
                DECODE(NVL (s.promotion_group, 0), 0, 0, 1) pg_flag, 
                DECODE(NVL (s.promoted_product, 0), 0, 0, 1) pp_flag
                FROM settlement s
                WHERE NVL(s.rr_default_claim, 0) = 0
                AND s.settlement_desc = claim_no
                AND   NVL (s.key_account_state, 0)
                + NVL (s.key_account, 0)
                + NVL (s.corporate_customer, 0)
                + nvl (s.customer_name, 0) 
                + NVL (s.sub_banner, 0)                                       ---- added sales office , fn 12/11/2015
                + NVL (s.brand, 0) 
                + NVL (s.promotion_group, 0) 
                + NVL (s.promoted_product, 0) > 0
                AND s.settlement_status_id IN (1, 10)) 
    LOOP
    
          dynamic_ddl('INSERT INTO rr_claim_matrix_' || user_id || '
          SELECT /*+ full(s) parallel(s, 32) index(m, mdp_claim_idx) */
                       settlement_id, 
                       m.item_id,
                       m.location_id,
                       n.rr_claim_max_cd
          FROM settlement s,
          mdp_matrix m,
          ----t_ep_L_ATT_6 n
          t_ep_ebs_customer n
          WHERE 
          m.t_ep_ebs_customer_ep_id = n.t_ep_ebs_customer_ep_id ' ||
          CASE WHEN rec.kas_flag = 0 THEN NULL ELSE ' AND m.t_ep_ebs_account_ep_id = s.key_account_state ' END ||
          CASE WHEN rec.ka_flag = 0 THEN NULL ELSE ' AND m.t_ep_ebs_customer_ep_id = s.key_account' END ||
          CASE WHEN rec.cc_flag = 0 THEN NULL ELSE ' AND m.t_ep_ebs_tp_zone_EP_ID = s.corporate_customer' END ||			-- Sanjiiv Banner Group Change
          CASE WHEN rec.cn_flag = 0 THEN NULL ELSE ' AND m.t_ep_e1_cust_cat_4_ep_id = s.customer_name' END ||
          CASE WHEN rec.b_flag = 0 THEN NULL ELSE ' AND m.t_ep_e1_item_cat_4_ep_id = s.brand' END ||
          CASE WHEN rec.pg_flag = 0 THEN NULL ELSE ' AND m.t_ep_p2b_ep_id = s.promotion_group' END ||
          CASE WHEN rec.pp_flag = 0 THEN NULL ELSE ' AND m.t_ep_i_att_1_ep_id = s.promoted_product' END ||
          ' AND NVL(s.rr_default_claim, 0) = 0
          AND s.settlement_desc = ''' || claim_no || '''
          AND 0 ' ||
              CASE WHEN rec.kas_flag = 1 THEN NULL ELSE ' +  NVL (s.key_account_state, 0) ' END || 
              CASE WHEN rec.ka_flag = 1 THEN NULL ELSE ' + NVL (s.key_account, 0) ' END || 
              CASE WHEN rec.cc_flag = 1 THEN NULL ELSE ' + NVL (s.corporate_customer, 0) ' END || 
              CASE WHEN rec.cn_flag = 1 THEN NULL ELSE ' + NVL (s.customer_name, 0) ' END || 
              CASE WHEN rec.b_flag = 1 THEN NULL ELSE ' + NVL (s.brand, 0) ' END ||  
              CASE WHEN rec.pg_flag = 1 THEN NULL ELSE ' + NVL (s.promotion_group, 0) ' END ||  
              CASE WHEN rec.pp_flag = 1 THEN NULL ELSE ' + NVL (s.promoted_product, 0) ' END ||  
                            '  = 0
          ' || CASE WHEN rec.kas_flag = 0 THEN NULL ELSE ' and nvl (s.key_account_state, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.ka_flag = 0 THEN NULL ELSE ' AND NVL (s.key_account, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.cc_flag = 0 THEN NULL ELSE ' AND NVL (s.corporate_customer, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.cn_flag = 0 THEN NULL ELSE ' AND NVL (s.customer_name, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.b_flag = 0 THEN NULL ELSE ' AND NVL (s.brand, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.pg_flag = 0 THEN NULL ELSE ' AND NVL (s.promotion_group, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.pp_flag = 0 THEN NULL ELSE ' AND NVL (s.promoted_product, 0)  <> 0 ' END || ' 
          AND s.settlement_status_id IN (1, 10)');
          
          COMMIT;
    
    END LOOP;
    
    check_and_drop('rr_prop_match_' || user_id );
    
    -- Invoiced customer fix --
 MERGE INTO settlement s
    USING(SELECT /*+ parallel */
                 s.settlement_id, 
                 MAX(m.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id,
                 MAX(m.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
                 MAX(m.t_ep_L_ATT_2_ep_id) t_ep_L_ATT_2_ep_id, 
                 MAX(m.t_ep_L_ATT_6_ep_id) t_ep_L_ATT_6_ep_id
          FROM settlement s,
          location m
          WHERE 1 = 1
          AND s.settlement_desc = claim_no
          AND s.key_account = m.t_ep_ebs_customer_ep_id
          AND NVL(s.rr_default_claim, 0) = 0
          AND   NVL (s.key_account_state, 0)
                + NVL (s.key_account, 0)
                + NVL (s.corporate_customer, 0)
                + NVL (s.customer_name, 0) 
                + NVL (s.brand, 0) 
                + NVL (s.promotion_group, 0) 
                + NVL (s.promoted_product, 0) > 0
          AND s.settlement_status_id IN (1, 10)
          GROUP BY s.settlement_id) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id, 
    s.t_ep_L_ATT_2_ep_id = s1.t_ep_L_ATT_2_ep_id, 
    s.t_ep_L_ATT_6_ep_id = s1.t_ep_L_ATT_6_ep_id,
    s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id;    
    COMMIT; 
    
    
  MERGE INTO settlement s
     USING (SELECT                            /*+ parallel(sm, 32) full(sm) */
                  s.settlement_id,
                   MAX (l.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id,
                   MAX (l.t_ep_e1_cust_cat_2_ep_id) t_ep_e1_cust_cat_2_ep_id,
                  max (l.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
                  MAX (l.t_ep_l_att_6_ep_id) t_ep_l_att_6_ep_id   ---- added sales office , fn 12/11/2015
              FROM settlement s, location l        --rr_settlement_matrix sm
             WHERE 1 = 1
                   AND s.settlement_desc = claim_no
                   AND NVL (s.key_account_state ,0) = CASE NVL (s.key_account_state , 0)
                          WHEN 0 THEN  NVL (s.key_account_state, 0)
                          ELSE  l.t_ep_ebs_account_ep_id 
                       END
                   AND NVL (s.key_account, 0)=CASE NVL (s.key_account, 0)
                          WHEN 0 THEN NVL (s.key_account, 0)
                          ELSE  l.t_ep_ebs_customer_ep_id 
                       END
                   AND NVL (s.corporate_customer, 0) = CASE NVL (s.corporate_customer, 0)
                          WHEN 0 THEN NVL (s.corporate_customer, 0)
                          ELSE  l.t_ep_l_att_6_ep_id
                       END
		   AND NVL (s.customer_name, 0) = CASE NVL (s.customer_name, 0)
                          WHEN 0 THEN NVL (s.customer_name, 0)
                          ELSE  l.t_ep_e1_cust_cat_4_ep_id
                       END
                   AND TRUNC (s.LAST_UPDATE_DATE) BETWEEN TRUNC (SYSDATE - 1)
                                                    AND TRUNC (SYSDATE)
                   AND (   NVL (s.key_account_state , 0) <> 0
                        OR NVL (s.key_account, 0) <> 0
                        OR NVL (s.corporate_customer, 0) <> 0
			OR NVL (s.customer_name, 0) <> 0) group by s.settlement_id) s1
        ON (s.settlement_id = s1.settlement_id)
WHEN MATCHED
THEN
   UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id,
    s.t_ep_e1_cust_cat_2_ep_id = s1.t_ep_e1_cust_cat_2_ep_id,
  --   s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id,
    s.t_ep_l_att_6_ep_id = s1.t_ep_l_att_6_ep_id;   ---- ---- added sales office , fn 12/11/2015
        
    COMMIT;
    
      -- Calculate Store Quantity and Cartons based on Claim Amount and Case / Unit Deal for Promotion Groups--
      -- May need to verify this logic - possibly an item in a promo grp may not all have the same units per case --
      -- Calculate Store Quantity and Cartons based on Claim Amount and Case / Unit Deal for Promoted Products--
      
    dynamic_ddl('MERGE INTO settlement s
    USING(SELECT /*+ parallel(s,12) parallel(m,12) */
                 s.settlement_id, 
                 MAX(i.e1_vol_uom_mult) units_per_case,
                 MAX(CASE WHEN s.case_deal > 0 THEN SAFE_DIVISION(s.claim_case_deal_d, s.case_deal, 0) 
                      WHEN s.case_hf_deal >0 THEN SAFE_DIVISION(s.claim_case_deal_hf_d, s.case_hf_deal, 0)
                      ELSE NULL
                 END) cartons, 
                 MAX(CASE WHEN s.unit_deal > 0 THEN SAFE_DIVISION(s.claim_case_deal_d, s.unit_deal, 0) 
                      WHEN s.unit_hf_deal >0 THEN SAFE_DIVISION(s.claim_case_deal_hf_d, s.unit_hf_deal, 0)
                      ELSE NULL
                 END) store_quantity
          FROM settlement s,
          mdp_matrix m,
          t_ep_item i,
          rr_claim_matrix_' || user_id ||' sm
          WHERE 1 = 1
          AND s.settlement_desc = '''||claim_no ||'''
          AND m.item_id = sm.item_id
          AND m.location_id = sm.location_id
          AND s.settlement_id = sm.settlement_id
          AND m.t_ep_item_ep_id = i.t_ep_item_ep_id
          AND NVL(s.rr_default_claim, 0) = 0
          AND NVL (s.promotion_group, 0) 
              + NVL (s.promoted_product, 0) > 0
          AND s.settlement_status_id IN (1, 10)
          GROUP BY s.settlement_id) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.cartons = s1.cartons,
    s.store_quantity = s1.store_quantity,
    s.units_per_case = s1.units_per_case');
    
    COMMIT;
    
    v_batch_dt := 'PromotionId'; 
    
    dynamic_ddl('CREATE TABLE rr_prop_match_' || user_id ||' as
    SELECT rr_proposed_match_seq.nextval seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
    SELECT s.settlement_id, p.promotion_id, 
           0 date_diff,
           1 deal_type,
           0 deal_type_diff,
           0 deal_diff, 
           0 start_dt_diff, 0 row_rank, ''' || v_batch_dt || ''' batch_date,
           0 fund_avail,           
           1 deal_match_diff
    FROM settlement s, 
    promotion p, 
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss    
    WHERE 1 = 1 
    AND s.settlement_desc =  ''' ||claim_no ||'''
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0
    AND p.scenario_id IN (22, 262, 162)
    AND s.promotion_id = p.promotion_id
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.promotion_stat_id = ps.promotion_stat_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1    
    AND NVL (s.promotion_id, 0) > 0
    GROUP BY s.settlement_id, p.promotion_id)');
    
    COMMIT;
    
    v_batch_dt := 'CustPromo'; 
    
    dynamic_ddl('INSERT INTO rr_prop_match_' || user_id ||'(seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
                                  deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff) 
    SELECT rr_proposed_match_seq.nextval, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
    SELECT s.settlement_id, p.promotion_id, 
           CASE WHEN MAX(ABS((next_day(pdt.from_date,''' || v_day || ''')-7)-(next_day(s.start_date,''' || v_day || ''')-7))) + 
                  MAX(ABS((pdt.until_date)-(pdt.from_date-(next_day(pdt.from_date,''' || v_day || ''')-7))-(s.end_date-(s.start_date-(next_day(s.start_date,''' || v_day || ''')-7))))) <= 0 THEN 0
               WHEN MAX((NEXT_DAY(TRUNC(pdt.from_date, ''DD''),''' || v_day || ''')-7)) <= MAX((NEXT_DAY(TRUNC(s.start_date, ''DD''),''' || v_day || ''')-7))  
                  AND MAX((TRUNC(pdt.until_date, ''DD''))-(TRUNC(pdt.from_date, ''DD'')-(NEXT_DAY(TRUNC(pdt.from_date, ''DD''),''' || v_day || ''')-7))) 
                      >= MAX(TRUNC(s.end_date, ''DD'')- (TRUNC(s.start_date, ''DD'')-(NEXT_DAY(TRUNC(s.start_date, ''DD''),''' || v_day || ''')-7)))  THEN 1
               ELSE 3 END date_diff,
           2 deal_type,
           0 deal_type_diff,
           0 deal_diff, 
           0 start_dt_diff, 0 row_rank, ''' ||v_batch_dt || ''' batch_date,
           0 fund_avail,
           1 deal_match_diff
    FROM settlement s, 
    promotion p, 
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss,
    promotion_dates pdt
    WHERE 1 = 1 
    AND s.settlement_desc =  ''' ||claim_no ||'''
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0
    AND p.scenario_id IN (22, 262, 162)
    AND s.customer_promo_no = p.cust_promo_no
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.promotion_stat_id = ps.promotion_stat_id
    AND p.promotion_id = pdt.promotion_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1    
    AND NVL (s.customer_promo_no, ''!!!@#$!!!'') <> ''!!!@#$!!!''
    AND NVL (s.promotion_id, 0) = 0
    GROUP BY s.settlement_id, p.promotion_id)');
    
    COMMIT;
    
    v_batch_dt := 'Deals'; 
  
    dynamic_ddl('INSERT INTO rr_prop_match_' || user_id ||'(seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
                                  deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff) 
    SELECT rr_proposed_match_seq.nextval, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
    SELECT s.settlement_id, pd.promotion_id, 
     CASE WHEN MAX(ABS((next_day(TRUNC(pdt.from_date,''DD''),''' || v_day || ''')-7)-(next_day(TRUNC(s.start_date,''DD''),''' || v_day || ''')-7))) + 
                  MAX(ABS((TRUNC(pdt.until_date,''DD''))-(TRUNC(pdt.from_date,''DD'')-(next_day(TRUNC(pdt.from_date,''DD''),''' || v_day || ''')-7))-(TRUNC(s.end_date,''DD'')-(TRUNC(s.start_date,''DD'')-(next_day(TRUNC(s.start_date,''DD''),''' || v_day || ''')-7))))) <= 0 THEN 0        
               WHEN MAX((NEXT_DAY(TRUNC(pdt.from_date, ''DD''),''' || v_day || ''')-7)) <= MAX((NEXT_DAY(TRUNC(s.start_date, ''DD''),''' || v_day || ''')-7))  
                  AND MAX((TRUNC(pdt.until_date, ''DD''))-(TRUNC(pdt.from_date, ''DD'')-(NEXT_DAY(TRUNC(pdt.from_date, ''DD''),''' || v_day || ''')-7))) 
                      >= MAX(TRUNC(s.end_date, ''DD'')- (TRUNC(s.start_date, ''DD'')-(NEXT_DAY(TRUNC(s.start_date, ''DD''),''' || v_day || ''')-7)))  THEN 1
               ELSE 3 END date_diff,
           MIN(CASE WHEN s.case_deal <> 0 THEN 3 
                WHEN s.unit_deal <> 0 THEN 4
                WHEN s.co_op_$ <> 0 THEN 5
                WHEN s.case_hf_deal <> 0 THEN 6
                WHEN s.unit_hf_deal <> 0 THEN 7     
                WHEN s.claim_coop_hf <> 0 THEN 8
                ELSE 20
           END) deal_type,
            MIN(CASE 
                WHEN NVL(s.claim_case_deal_hf_d,0) <> 0 or NVL(s.claim_coop_hf,0) <> 0
                THEN 0
                WHEN NVL(pd.case_buydown,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) = NVL(p.cd_deal_type, 0) AND NVL(s.claim_case_deal_d, 0) <> 0
                THEN 0
                WHEN NVL(pd.event_cost, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) = NVL(p.coop_deal_type, 0) AND NVL(s.co_op_$, 0) <> 0
                THEN 0
                WHEN NVL(s.claim_case_deal_d,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) = 0 OR NVL(s.co_op_$, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) = 0
                THEN 0
                WHEN NVL(pd.case_buydown,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> NVL(p.cd_deal_type, 0) 
                THEN 2
                WHEN NVL(pd.event_cost, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> NVL(p.coop_deal_type, 0) 
                THEN 2
                WHEN NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> NVL(p.cd_deal_type, 0) 
                THEN 3
                WHEN NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> NVL(p.coop_deal_type, 0) 
                THEN 3                
                ELSE 3
           END) deal_type_diff,
           CASE WHEN MAX(sm.rr_claim_max_cd) = 1 AND MAX(s.case_deal) <> 0 THEN CASE WHEN MAX(NVL(s.case_deal, 0)) <= MAX(NVL(pd.case_buydown, 0)) THEN 0  WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS(MAX(NVL(s.case_deal, 0)) - MAX(NVL(pd.case_buydown, 0))) ELSE 9999999999 END
                WHEN MAX(sm.rr_claim_max_cd) = 1 AND MAX(s.unit_deal) <> 0 THEN CASE WHEN (MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) <= MAX(NVL(pd.case_buydown, 0)) THEN 0  WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS((MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) - (MAX(NVL(pd.case_buydown, 0)))) ELSE 9999999999 END
                WHEN MAX(s.case_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS(MAX(NVL(s.case_deal, 0)) - MAX(NVL(pd.case_buydown, 0))) ELSE 9999999999 END
                WHEN MAX(s.unit_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS((MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) - (MAX(NVL(pd.case_buydown, 0)))) ELSE 9999999999 END
                WHEN MAX(s.co_op_$) <> 0 THEN CASE WHEN SUM(NVL(pd.event_cost, 0)) <> 0 THEN ABS(MAX(NVL(s.co_op_$, 0)) - SUM(NVL(pd.event_cost, 0)))   ELSE 9999999999 END
                WHEN MAX(s.case_hf_deal) <> 0 THEN CASE WHEN (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))) <> 0 THEN ABS(MAX(NVL(s.case_hf_deal, 0)) - (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))))  ELSE 9999999999 END
                WHEN MAX(s.unit_hf_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.rr_handling_d, 0)) <> 0 THEN ABS(MAX(NVL(s.unit_hf_deal, 0)) - MAX(NVL(pd.rr_handling_d, 0)))  ELSE 9999999999 END
                WHEN MAX(s.claim_coop_hf) <> 0 THEN CASE WHEN SUM(NVL(pd.rr_handling_coop, 0)) <> 0 THEN ABS(MAX(NVL(s.claim_coop_hf, 0)) - SUM(NVL(pd.rr_handling_coop, 0))) ELSE 9999999999 END
                ELSE 9999999999
           END  deal_diff, 
           MAX(ABS(pdt.from_date-s.start_date)) start_dt_diff, 0 row_rank, '''||v_batch_dt||''' batch_date,
           CASE WHEN MAX(s.case_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_cd_pd, 0))
                WHEN MAX(s.unit_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_cd_pd, 0))
                WHEN MAX(s.co_op_$) <> 0 THEN SUM(NVL(pd.rr_accrual_coop_pd, 0))
                WHEN MAX(s.case_hf_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_cd_pd, 0))
                WHEN MAX(s.unit_hf_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_cd_pd, 0)) 
                WHEN MAX(s.claim_coop_hf) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_coop_pd, 0)) 
                ELSE 0
           END fund_avail,
           CASE WHEN MAX(s.case_deal) <> 0 OR MAX(s.claim_case_deal_d) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.unit_deal) <> 0 OR MAX(s.claim_case_deal_d) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.co_op_$) <> 0 THEN CASE WHEN SUM(NVL(pd.event_cost, 0)) <> 0 THEN 1  ELSE 2 END
                WHEN MAX(s.case_hf_deal) <> 0 OR MAX(s.claim_case_deal_hf_d) <> 0 THEN CASE WHEN (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.unit_hf_deal) <> 0 OR MAX(s.claim_case_deal_hf_d) <> 0 THEN CASE WHEN MAX(NVL(pd.rr_handling_d, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.claim_coop_hf) <> 0 THEN CASE WHEN SUM(NVL(pd.rr_handling_coop, 0)) <> 0 THEN 1 ELSE 2 END
                ELSE 2
           END  deal_match_diff 
    FROM settlement s, 
    promotion_data pd, 
    mdp_matrix m, 
    promotion p, 
    promotion_dates pdt,
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss,
    rr_claim_matrix_' || user_id || ' sm    
    WHERE 1 = 1 
    AND s.settlement_desc =  ''' ||claim_no ||'''
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0    
    AND p.scenario_id IN (22, 262, 162)
    AND pd.promotion_id = p.promotion_id
    AND pdt.promotion_id = p.promotion_id
    AND pd.item_id = m.item_id    
    AND pd.location_id = m.location_id
    AND pd.promotion_type_id = pt.promotion_type_id
    AND pd.promotion_stat_id = ps.promotion_stat_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1
    AND m.item_id = sm.item_id
    AND m.location_id = sm.location_id
    AND s.settlement_id = sm.settlement_id
    /* AND ABS(NVL(s.case_deal, 0))
        + ABS(NVL(s.unit_deal, 0)) 
        + ABS(NVL(s.co_op_$, 0)) 
        + ABS(NVL(s.case_hf_deal, 0)) 
        + ABS(NVL(s.unit_hf_deal, 0)) 
        + ABS(NVL(s.claim_coop_hf, 0)) <> 0 */
    AND NVL (s.customer_promo_no, ''!!!@#$!!!'') = ''!!!@#$!!!''
    AND NVL (s.promotion_id, 0) = 0
    AND NVL (s.key_account_state, 0)
        + NVL (s.key_account, 0)
        + NVL (s.corporate_customer, 0)
        + NVL (s.customer_name, 0) 
        + NVL (s.brand, 0) 
        + NVL (s.promotion_group, 0) 
        + nvl (s.promoted_product, 0) 
        + NVL (s.sub_banner, 0) > 0      ---- added sales office , fn 12/11/2015
    AND pd.sales_date BETWEEN NEXT_DAY(s.start_date,''' || v_day || ''') - 7 AND NEXT_DAY(s.end_date,''' || v_day || ''') - 1 --Align to Start and End of week
    GROUP BY s.settlement_id, pd.promotion_id)');
   
    COMMIT;
   
    dynamic_ddl('MERGE INTO rr_prop_match_' || user_id ||' pm
    USING(SELECT seq_id, settlement_id, promotion_id, 
    RANK() OVER ( PARTITION BY settlement_id ORDER BY deal_match_diff, deal_type_diff, deal_diff, date_diff, start_dt_diff, fund_avail desc, seq_id) row_rank
    FROM rr_prop_match_' || user_id ||'
    WHERE 1 = 1
    --AND batch_date = v_batch_dt
    ) pm1
    ON(pm.seq_id = pm1.seq_id)
    WHEN MATCHED THEN 
    UPDATE SET pm.row_rank = pm1.row_rank');
    
    COMMIT;
    
    dynamic_ddl('MERGE INTO settlement s
    USING (SELECT settlement_id, promotion_id, deal_match_diff, date_diff, deal_type, deal_type_diff, deal_diff, start_dt_diff
    FROM rr_prop_match_' || user_id ||'
    WHERE row_rank = 1 
    --AND batch_date = v_batch_dt
    ) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.promotion_id = CASE WHEN deal_type_diff <> 0 THEN 0 ELSE s1.promotion_id END,  --LP 29/11/13
               s.settlement_status_id = CASE WHEN deal_type_diff <> 0 THEN 10  --Unlinked   LP 29/11/13
                                             WHEN deal_type IN (20) THEN 
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                     THEN 12  --Linked PM
                                                       ELSE 12 END
                                             WHEN deal_type IN (1, 2) THEN
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff between 0 and 0.01 THEN 8   --Exact Match
                                                       WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff > 0.01             THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                                                      THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             WHEN deal_type IN (3, 4, 6, 7) THEN
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff between 0 and 0.01 THEN 8   --Exact Match
                                                       WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff > 0.01             THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                                                      THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             WHEN deal_type IN (5, 8) THEN 
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 THEN 8   --Exact Match
                                                       WHEN date_diff > 1                                                     THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             ELSE 12
                                        END,
               s.last_update_date = SYSDATE,
               s.link_date        = SYSDATE,
               s.date_diff        = s1.date_diff,
               s.deal_match_diff  = s1.deal_match_diff, 
               s.deal_type_diff   = s1.deal_type_diff ');

    COMMIT;
    
    dynamic_ddl('MERGE INTO settlement s
    USING (SELECT settlement_id 
    FROM settlement s,
    settlement_status ss
    WHERE s.settlement_status_id = ss.settlement_status_id  
    AND ss.rr_link_claim = 1
    AND s.rr_default_claim = 0
    AND s.settlement_desc = ''' ||claim_no ||'''
    AND NOT EXISTS (SELECT 1 FROM rr_prop_match_' || user_id ||' pm WHERE pm.settlement_id = s.settlement_id)) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.settlement_status_id = 10,
               s.last_update_date = SYSDATE');  
                
    COMMIT;

  -----------------------------------------------------------------------------
  ---------------------Update Promotion Status---------------------------------
   
    dynamic_ddl('MERGE INTO promotion_data pd
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_prop_match_' || user_id ||' pm, settlement s
    WHERE pm.row_rank = 1 
    AND pm.batch_date = '''||v_batch_dt||'''
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) pd1
    ON(pd.promotion_id = pd1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET pd.promotion_stat_id = 7,
               last_update_date = SYSDATE WHERE pd.promotion_stat_id <> 7'); 
               
    COMMIT;
  
    dynamic_ddl('MERGE INTO promotion_matrix pm
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_prop_match_' || user_id ||' pm, settlement s
    WHERE pm.row_rank = 1     
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) pm1
    ON(pm.promotion_id = pm1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET pm.promotion_stat_id = 7,
               pm.promotion_data_lud = SYSDATE,
               last_update_date = SYSDATE WHERE pm.promotion_stat_id <> 7');
               
    COMMIT;
  
    dynamic_ddl('MERGE INTO promotion p
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_prop_match_' || user_id ||' pm, settlement s
    WHERE pm.row_rank = 1   
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) p1
    ON(p.promotion_id = p1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET p.promotion_stat_id = 7,
               last_update_date = SYSDATE WHERE p.promotion_stat_id <> 7');   
    
    COMMIT;

      check_and_drop('tbl_dup_claim_' || user_id);
      check_and_drop('rr_prop_match_' || user_id );
      check_and_drop('rr_claim_matrix_' || user_id );
      
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;
   
   PROCEDURE prc_validate_claim(user_id   NUMBER,
                                level_id  NUMBER,
                                member_id NUMBER)
   AS
   
    datevar       NUMBER (10);
    datevarind    NUMBER (10);
    
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;  
    
    mindate       DATE;
    maxdate       DATE;
    v_debug       NUMBER := 0;
    v_debug_count NUMBER  := 0;
    v_day         VARCHAR2(10);
    
    v_batch_dt    VARCHAR2(20);
    
    approved_claim_amount   NUMBER         := 0;
    ttl_claim_amount        NUMBER;
    minclaim_allowance      NUMBER;
    maxclaim_allowance      NUMBER;
    small_claim             NUMBER;
    num_of_lines            NUMBER;
    
    v_no_lines_cd           NUMBER;
    v_no_lines_coop         NUMBER;
    v_no_lines_hf_cd        NUMBER;
    v_no_lines_hf_coop      NUMBER;
    
    v_unit_scan             NUMBER;
    v_promo_id              NUMBER;
    v_promo_stat_id         NUMBER;
    v_accrual_cd            NUMBER;
    v_accrual_coop          NUMBER;
    v_accrual_hf_cd         NUMBER;
    v_accrual_hf_coop       NUMBER;
    
    v_ttl_cd                NUMBER;
    v_ttl_coop              NUMBER;
    v_ttl_hf_cd             NUMBER;
    v_ttl_hf_coop           NUMBER;
    
    claim_no                VARCHAR2(2000);
   
   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_APPROVE_AND_VALIDATE_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
    
    
    SELECT settlement_desc
     INTO claim_no
     FROM settlement
    WHERE settlement_id = member_id
    AND NVL(rr_default_claim, 0) = 0;
    
       -----Reset Claims from Unapproved to Linked, ready for Re-approval----
    UPDATE settlement
       SET settlement_status_id = 8,
           last_update_date = sysdate
     WHERE settlement_status_id = 3 and settlement_desc =claim_no ;
    
    ------------Reset GL Exp Claim back to No if it is not Yes----------------------
    UPDATE settlement s
      SET s.gl_exp_claim = 0,                                      --Set to No
          s.last_update_date = SYSDATE
    WHERE s.gl_exp_claim NOT IN (0, 1)                             --No or Yes
      AND s.settlement_desc = claim_no
      AND NVL(s.rr_default_claim, 0) = 0;

   COMMIT;
   
      ---Set non export claims to Denied if claim is Closed
   
   UPDATE settlement s
      SET settlement_status_id = 6,
          last_update_date = SYSDATE
    WHERE s.promotion_id IN (SELECT promotion_id
                            FROM promotion
                           WHERE promotion_stat_id = 8
                           AND scenario_id IN (22, 262, 162))
      AND s.gl_exp_claim <> 1
      AND s.gl_exp_claim_detail <> 1
      AND nvl(s.promotion_id,0) <> 0
      AND s.rr_default_claim = 0
      AND s.settlement_desc = claim_no
      AND NVL(s.rr_default_claim, 0) = 0;
   
   ---Only set claims to Lined if manually linked
   UPDATE settlement s
      SET settlement_status_id = 8,
          s.last_update_date = SYSDATE
    WHERE settlement_status_id IN (1, 10)
      AND NVL (promotion_id, 0) <> 0
      AND s.settlement_desc = claim_no
      AND NVL(s.rr_default_claim, 0) = 0
      AND s.gl_exp_claim = 0;

   COMMIT;
    
    get_param ('sys_params', 'DSMTtlClaimAllowance', ttl_claim_amount);
    get_param ('sys_params', 'DSMMinClaimAllowance', minclaim_allowance);
    get_param ('sys_params', 'DSMMaxClaimAllowance', maxclaim_allowance);
    get_param ('sys_params', 'DSMSmallClaim', small_claim);      
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';
    
    ----FIRST SECTION - THROW ALL TTL CLAIMS > TRUE CLAIM INTO DISPUTE --
    MERGE INTO settlement s
    USING(SELECT s.promotion_id,
                 SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                 SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                 SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                 SUM( NVL(s.co_op_$, 0) ) ttl_coop
          FROM settlement s,
          settlement_status ss
          WHERE s.settlement_status_id = ss.settlement_status_id
          AND ss.settlement_status_id IN (4, 8)
          AND NVL(s.rr_default_claim, 0) = 0
          AND s.promotion_id IN (SELECT DISTINCT s1.promotion_id FROM settlement s1 WHERE s1.settlement_desc = claim_no)
          GROUP BY s.promotion_id
          HAVING SUM (NVL(s.claim_case_deal_hf_d, 0) + NVL(s.claim_coop_hf, 0) + NVL(s.claim_case_deal_d, 0) + NVL(s.co_op_$, 0) ) > ttl_claim_amount)s1
    ON(s.promotion_id = s1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET settlement_status_id = 6,
               last_update_date = SYSDATE
    WHERE s.settlement_status_id IN (8);
    
    COMMIT;
    
       -- Select All Claims for both Approved and Linked Status to be checked against True Scan Amount--
   -- Claims will be Unapproved if Claim already entered and Claim Amount Over Scanned Amount --
    FOR rec IN (SELECT    s.promotion_id,
                          NVL (s.customer_promo_no, 0) customer_promo_no,               -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                          DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) cpn, -- LP 02/02/2011 - Change from customer_promo_no to promotion_id                          
                          /* NVL (s.key_account_state, 0) key_account_state,
                          NVL (s.key_account, 0) key_account,
                          NVL (s.corporate_customer, 0) corporate_customer,
                          NVL (s.customer_name, 0) customer_name,
                          NVL (s.brand, 0) brand,
                          NVL (s.promotion_group, 0) promotion_group,
                          NVL (s.promoted_product, 0) promoted_product, */
                          SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                          SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                          SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                          SUM( NVL(s.co_op_$, 0) ) ttl_coop
                     FROM settlement s
                    WHERE s.settlement_status_id IN (8) --(8,4)
                      --AND s.gl_exp_claim = 0                                      -- LP 02/02/2011 - Only Check Claims that have not exported
                      AND s.rr_default_claim = 0
                      AND s.settlement_desc = claim_no
                      AND s.promotion_id > 0
                      AND   NVL (s.key_account_state, 0)
                          + NVL (s.key_account, 0)
                          + NVL (s.corporate_customer, 0)
                          + NVL (s.customer_name, 0)
                          + NVL (s.brand, 0)
                          + NVL (s.promotion_group, 0)
                          + nvl (s.promoted_product, 0)
                          + NVL (s.sub_banner, 0)
                          + DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) > 0
                 GROUP BY s.promotion_id,
                          s.customer_promo_no
                          /* s.key_account_state,
                          s.key_account,
                          s.corporate_customer,
                          s.customer_name,
                          s.brand,
                          s.promotion_group,
                          s.promoted_product */
                          )
    LOOP
        -- Get Count of total number of claim lines for buffer ttl claim vs ttl scan --
          
        IF -- rec.cpn > 0
           1 = 1
        THEN
           -- Get TRUE Allowed Claim Amount from PD Table where Evt Status is not closed--
               -- Will Auto Deny if Promotion Closed, or Unapprove Claim if over scanned amount --
           
           SELECT SUM (CASE WHEN NVL(s.claim_case_deal_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.co_op_$, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_case_deal_hf_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_coop_hf, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM ( NVL(s.claim_case_deal_d, 0) ),
                  SUM ( NVL(s.co_op_$, 0) ),
                  SUM ( NVL(s.claim_case_deal_hf_d, 0) ),
                  SUM ( NVL(s.claim_coop_hf, 0) )
              INTO v_no_lines_cd, v_no_lines_coop, v_no_lines_hf_cd, v_no_lines_hf_coop,
                   v_ttl_cd, v_ttl_coop, v_ttl_hf_cd, v_ttl_hf_coop
              FROM settlement s
              WHERE s.promotion_id = rec.promotion_id
                AND s.settlement_status_id IN (4, 8)
                AND NVL(s.rr_default_claim, 0) = 0;
           
           BEGIN
           
              SELECT /*+ parallel (pd,16) parallel (p,16) */   p.promotion_id, p.promotion_stat_id,
                    SUM (  NVL (pd.rr_accrual_cd_ad_gl, 0) ) rr_accrual_cd_pd,
                    SUM (  NVL (pd.rr_accrual_coop_ad_gl, 0) ) rr_accrual_coop_pd,
                    SUM (  NVL (pd.rr_accrual_hf_cd_ad_gl, 0) ) rr_accrual_hf_cd_pd,
                    SUM (  NVL (pd.rr_accrual_hf_coop_ad_gl, 0) ) rr_accrual_hf_coop_pd                                         
                    -- rec.customer_promo_no,                                 -- LP 02/02/2011 - Not Required
                     --rec.ttl_settlement
                INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
                FROM accrual_data pd, promotion p
               WHERE rec.promotion_id = p.promotion_id
                 AND p.promotion_id = pd.accrual_id
                 AND p.scenario_id IN (22, 262, 162)
              GROUP BY p.promotion_id,
                     p.promotion_stat_id;
                     --rec.customer_promo_no
                     --rec.ttl_settlement
           EXCEPTION
           WHEN NO_DATA_FOUND THEN 
                    
              SELECT 
                    rec.promotion_id , 
                    NVL((SELECT p.promotion_stat_id 
                         FROM promotion p 
                         WHERE  p.promotion_id = rec.promotion_id
                         AND p.scenario_id IN (22, 262, 162)), 0),
                    0 rr_accrual_cd_pd,
                    0 rr_accrual_coop_pd,
                    0 rr_accrual_hf_cd_pd,
                    0 rr_accrual_hf_coop_pd                     
              INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
              FROM DUAL;
              
           WHEN OTHERS THEN
              RAISE;
           END;         
              --Auto Deny Claims where Promotion is Closed--
              IF v_promo_stat_id = 8
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 6,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND s.settlement_desc = claim_no
                    AND NVL(s.rr_default_claim, 0) = 0;
              END IF;
  
              --Change Status to Unapproved for LINKED Claims that are over TRUE claim At CustState Level--
              IF (
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop <= small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (minclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (minclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (minclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (minclaim_allowance * v_no_lines_hf_coop)))
                 OR 
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop > small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (maxclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (maxclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (maxclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (maxclaim_allowance * v_no_lines_hf_coop)))
                 )
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 3,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND s.settlement_desc = claim_no
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;
              
              ELSE
                 UPDATE settlement s
                    SET settlement_status_id = 4,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND s.settlement_desc = claim_no
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;  
              END IF;           
        END IF;
    END LOOP;
    
    FOR rec IN (SELECT    s.promotion_id,
                          NVL (s.customer_promo_no, 0) customer_promo_no,               -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                          DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) cpn, -- LP 02/02/2011 - Change from customer_promo_no to promotion_id    
                          /* NVL (s.key_account_state, 0) key_account_state,
                          NVL (s.key_account, 0) key_account,
                          NVL (s.corporate_customer, 0) corporate_customer,
                          NVL (s.customer_name, 0) customer_name,
                          NVL (s.brand, 0) brand,
                          NVL (s.promotion_group, 0) promotion_group,
                          NVL (s.promoted_product, 0) promoted_product, */
                          SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                          SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                          SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                          SUM( NVL(s.co_op_$, 0) ) ttl_coop
                     FROM settlement s
                    WHERE s.settlement_status_id IN (8) --(8,4)
                      --AND s.gl_exp_claim = 0                                      -- LP 02/02/2011 - Only Check Claims that have not exported
                      AND s.rr_default_claim = 0
                      AND s.promotion_id > 0
                      AND   NVL (s.key_account_state, 0)
                          + NVL (s.key_account, 0)
                          + NVL (s.corporate_customer, 0)
                          + NVL (s.customer_name, 0)
                          + NVL (s.brand, 0)
                          + NVL (s.promotion_group, 0)
                          + nvl (s.promoted_product, 0)
                          + NVL (s.sub_banner, 0)
                          + DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) > 0
                 GROUP BY s.promotion_id,
                          s.customer_promo_no
                          /* s.key_account_state,
                          s.key_account,
                          s.corporate_customer,
                          s.customer_name,
                          s.brand,
                          s.promotion_group,
                          s.promoted_product */
                          )
    LOOP
    
        IF -- rec.cpn > 0
           1 = 1
        THEN
           -- Get TRUE Allowed Claim Amount from PD Table where Evt Status is not closed--
               -- Will Auto Deny if Promotion Closed, or Unapprove Claim if over scanned amount --
           
           SELECT SUM (CASE WHEN NVL(s.claim_case_deal_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.co_op_$, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_case_deal_hf_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_coop_hf, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM ( NVL(s.claim_case_deal_d, 0) ),
                  SUM ( NVL(s.co_op_$, 0) ),
                  SUM ( NVL(s.claim_case_deal_hf_d, 0) ),
                  SUM ( NVL(s.claim_coop_hf, 0) )
              INTO v_no_lines_cd, v_no_lines_coop, v_no_lines_hf_cd, v_no_lines_hf_coop,
                   v_ttl_cd, v_ttl_coop, v_ttl_hf_cd, v_ttl_hf_coop
              FROM settlement s
              WHERE s.promotion_id = rec.promotion_id
                AND s.settlement_status_id IN (4, 8)
                AND NVL(s.rr_default_claim, 0) = 0;
           
           BEGIN
           
           SELECT /*+ parallel (pd,16) parallel (p,16) */   p.promotion_id, p.promotion_stat_id,
                    SUM (  NVL (pd.rr_accrual_cd_ad_gl, 0) ) rr_accrual_cd_pd,
                    SUM (  NVL (pd.rr_accrual_coop_ad_gl, 0) ) rr_accrual_coop_pd,
                    SUM (  NVL (pd.rr_accrual_hf_cd_ad_gl, 0) ) rr_accrual_hf_cd_pd,
                    SUM (  NVL (pd.rr_accrual_hf_coop_ad_gl, 0) ) rr_accrual_hf_coop_pd                                         
                    -- rec.customer_promo_no,                                 -- LP 02/02/2011 - Not Required
                     --rec.ttl_settlement
                INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
                FROM accrual_data pd, promotion p
               WHERE rec.promotion_id = p.promotion_id
                 AND p.promotion_id = pd.accrual_id
                 AND p.scenario_id IN (22, 262, 162)
            GROUP BY p.promotion_id,
                     p.promotion_stat_id;
                     --rec.customer_promo_no
                     --rec.ttl_settlement
           EXCEPTION
           WHEN NO_DATA_FOUND THEN 
                    
              SELECT 
                    rec.promotion_id , 
                    NVL((SELECT p.promotion_stat_id 
                         FROM promotion p 
                         WHERE  p.promotion_id = rec.promotion_id
                         AND p.scenario_id IN (22, 262, 162)), 0),
                    0 rr_accrual_cd_pd,
                    0 rr_accrual_coop_pd,
                    0 rr_accrual_hf_cd_pd,
                    0 rr_accrual_hf_coop_pd                     
              INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
              FROM DUAL;
              
           WHEN OTHERS THEN
              RAISE;
           END;          
              --Auto Deny Claims where Promotion is Closed--
              IF v_promo_stat_id = 8
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 6,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0
                    AND s.settlement_desc = claim_no;
                    
              END IF;
  
              --Change Status to Unapproved for LINKED Claims that are over TRUE claim At CustState Level--
              IF (
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop <= small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (minclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (minclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (minclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (minclaim_allowance * v_no_lines_hf_coop)))
                 OR 
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop > small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (maxclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (maxclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (maxclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (maxclaim_allowance * v_no_lines_hf_coop)))
                 )
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 3,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND s.settlement_desc = claim_no
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;
              ELSE
                 UPDATE settlement s
                    Set Settlement_Status_Id = 4,
                        Last_Update_Date = Sysdate,
                        auto_approve_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND s.settlement_desc = claim_no
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;                 
              END IF;           
        END IF;
    
    END LOOP;
    
    -------LP Claim Validation

   -----------Set GL Exp Claim for all claims that are not approved----------------
   UPDATE settlement s
      SET s.gl_exp_claim = 2,                                  -- Not Approved
          s.last_update_date = SYSDATE
    WHERE s.gl_exp_claim = 0
      AND s.settlement_status_id NOT IN (4, 7)
      AND s.gl_exp_claim_detail <> 1
      AND s.settlement_desc = claim_no
      AND NVL(s.rr_default_claim, 0) = 0;                                   --LP

   COMMIT;

   --- Create Extract of CURRENT Approved Claims like T_EXP_CLAIMS_GL - Retail ----
   INSERT /*+ nologging */ INTO js_link_settlements_gt
      SELECT s.settlement_id
        FROM settlement s, mdp_matrix m, promotion_data pd, promotion_dates d
       WHERE 1 = 1
         AND s.settlement_status_id IN (4, 7)         
         AND s.gl_exp_claim_detail = 0
         AND s.gl_exp_claim = 0
         AND pd.promotion_stat_id in (1,4,5,6,7) -- LP - 8/6
         AND NVL(s.bill_to, 0) <> 0
         AND NVL(s.promotion_id, 0) <> 0
         AND pd.is_self = 1
         AND pd.scenario_id IN (22, 262, 162)
         AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
         AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
         AND m.T_ep_ebs_tp_zone_ep_id = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_ebs_tp_zone_ep_id, s.corporate_customer)				-- Sanjiiv Banner Group Change
         and m.t_ep_e1_cust_cat_4_ep_id = decode (nvl (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
         AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.sub_banner, 0), 0, m.t_ep_l_att_6_ep_id, s.sub_banner)
         AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
         AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
         AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
         AND pd.promotion_id = s.promotion_id
         AND pd.item_id = m.item_id
         AND pd.location_id = m.location_id
         AND pd.promotion_id = d.promotion_id
         AND (NEXT_DAY (d.from_date, v_day) - 7) <=
                         (NEXT_DAY (s.start_date, v_day) - 7
                         )                                      --Offset dates
         AND (NEXT_DAY (d.until_date, v_day) - 1) >=
                                (s.end_date + (((next_day(start_date, v_day)-7) - s.start_date)))
           AND pd.sales_date BETWEEN (NEXT_DAY (s.start_date, v_day) - 7
                         ) AND (s.end_date + (((next_day(start_date, v_day)-7) - s.start_date))) 
         AND s.settlement_desc = claim_no
         AND NVL(s.rr_default_claim, 0) = 0;                                --LP

   --------Set GL Exp Claim to Valid if in JS_SETTLEMENT_VALIDATION-------------------
   UPDATE settlement s
      SET s.gl_exp_claim = CASE WHEN s.end_date > sysdate THEN 5 --Future Valid
                                ELSE 4       --Valid
                                END ,                                        --Valid
          s.last_update_date = SYSDATE
    WHERE s.settlement_id IN (SELECT DISTINCT t.settlement_id
                                         FROM js_link_settlements_gt t)
      AND s.settlement_desc = claim_no
      AND s.gl_exp_claim_detail <> 1
      AND NVL(s.rr_default_claim, 0) = 0;                                   --LP

--------Set Remaining Claims in No Status for Retail to Invalid----------------
   UPDATE settlement s
      SET s.gl_exp_claim = 3,                                         --Invalid
          s.last_update_date = SYSDATE
    WHERE s.gl_exp_claim = 0                                             --NO
      AND s.settlement_desc = claim_no
      AND s.gl_exp_claim_detail <> 1
      AND NVL(s.rr_default_claim, 0) = 0;                                   --LP

   COMMIT;
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;
      
END;

/
