--------------------------------------------------------
--  DDL for Package Body PKG_CLAIMS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_CLAIMS" 
AS

/******************************************************************************
   NAME:       PKG_PTP
   PURPOSE:    All procedures commanly used for claims module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   PROCEDURE prc_link_claim 
   AS
   
    datevar       NUMBER (10);
    datevarind    NUMBER (10);
    
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;  
    
    mindate       DATE;
    maxdate       DATE;
    v_debug       NUMBER := 0;
    v_debug_count NUMBER  := 0;
    v_day         VARCHAR2(10);
    
    v_batch_dt    VARCHAR2(20);
   
   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LINK_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'DSMOIShipDateDifference', datevar);
    get_param ('sys_params', 'DSMOIShipDateDifferenceInd', datevarind);
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';    
    
    mindate := NEXT_DAY(ADD_MONTHS(SYSDATE, -12), v_day) -7;
    
    dynamic_ddl('TRUNCATE TABLE rr_settlement_matrix');
    
    -----Reset Claims from Unlinked/Linked CD/Linked PM to New, ready for Re-linking----
    UPDATE settlement
       SET settlement_status_id = 1,
           promotion_id = 0,
           last_update_date = sysdate
     WHERE settlement_status_id in  (9,10,12);  --LP 29/11
     
    COMMIT;  
    
    FOR rec IN (SELECT DISTINCT 
                DECODE(NVL (s.key_account_state, 0), 0, 0, 1) kas_flag, 
                DECODE(NVL (s.key_account, 0), 0, 0, 1) ka_flag, 
                DECODE(NVL (s.corporate_customer, 0), 0, 0, 1) cc_flag, 
                decode(nvl (s.customer_name, 0), 0, 0, 1) cn_flag, 
                DECODE(NVL (s.sub_banner, 0), 0, 0, 1) sb_flag,             ---- added sales office , fn 12/11/2015
                DECODE(NVL (s.brand, 0), 0, 0, 1) b_flag, 
                DECODE(NVL (s.promotion_group, 0), 0, 0, 1) pg_flag, 
                DECODE(NVL (s.promoted_product, 0), 0, 0, 1) pp_flag
                FROM settlement s
                WHERE NVL(s.rr_default_claim, 0) = 0
                AND   NVL (s.key_account_state, 0)
                + NVL (s.key_account, 0)
                + NVL (s.corporate_customer, 0)
                + nvl (s.customer_name, 0) 
                + NVL (s.sub_banner, 0)                                       ---- added sales office , fn 12/11/2015
                + NVL (s.brand, 0) 
                + NVL (s.promotion_group, 0) 
                + NVL (s.promoted_product, 0) > 0
                AND s.settlement_status_id IN (1, 10)) 
    LOOP
    
          dynamic_ddl('INSERT INTO rr_settlement_matrix
          SELECT /*+ full(s) parallel(s, 32) index(m, mdp_claim_idx) */
                       settlement_id, 
                       m.item_id,
                       m.location_id, 
                       n.rr_claim_max_cd
          FROM settlement s,
      ---      t_ep_L_ATT_6 n,    --- changed to CL3 per Luke , fn 12/11/15
               t_ep_ebs_customer n,
               mdp_matrix m
          WHERE
          m.t_ep_ebs_customer_ep_id = n.t_ep_ebs_customer_ep_id ' ||        --- changed to CL3 per Luke , fn 12/11/15
          case when rec.kas_flag = 0 then null else ' AND m.t_ep_ebs_account_ep_id = s.key_account_state ' end ||
          CASE WHEN rec.ka_flag = 0 THEN NULL ELSE ' AND m.t_ep_ebs_customer_ep_id = s.key_account' END ||
          CASE WHEN rec.cc_flag = 0 THEN NULL ELSE ' AND m.t_ep_ebs_tp_zone_EP_ID = s.corporate_customer' END ||					--- Sanjiiv Banner Group Change
          case when rec.cn_flag = 0 then null else ' AND m.t_ep_e1_cust_cat_4_ep_id = s.customer_name' end ||
          CASE WHEN rec.sb_flag = 0 THEN NULL ELSE ' AND m.t_ep_l_att_6_ep_id = s.sub_banner' END ||    ---- added sales office , fn 12/11/2015
          CASE WHEN rec.b_flag = 0 THEN NULL ELSE ' AND m.t_ep_e1_item_cat_4_ep_id = s.brand' END ||
          CASE WHEN rec.pg_flag = 0 THEN NULL ELSE ' AND m.t_ep_p2b_ep_id = s.promotion_group' END ||
          CASE WHEN rec.pp_flag = 0 THEN NULL ELSE ' AND m.t_ep_i_att_1_ep_id = s.promoted_product' END ||
          ' AND NVL(s.rr_default_claim, 0) = 0
          AND 0 ' ||
              case when rec.kas_flag = 1 then null else ' +  NVL (s.key_account_state, 0) ' end || 
              CASE WHEN rec.ka_flag = 1 THEN NULL ELSE ' + NVL (s.key_account, 0) ' END || 
              CASE WHEN rec.cc_flag = 1 THEN NULL ELSE ' + NVL (s.corporate_customer, 0) ' END || 
              case when rec.cn_flag = 1 then null else ' + NVL (s.customer_name, 0) ' end || 
              CASE WHEN rec.sb_flag = 1 THEN NULL ELSE ' + NVL (s.sub_banner, 0) ' END ||               ---- added sales office , fn 12/11/2015
              CASE WHEN rec.b_flag = 1 THEN NULL ELSE ' + NVL (s.brand, 0) ' END ||  
              CASE WHEN rec.pg_flag = 1 THEN NULL ELSE ' + NVL (s.promotion_group, 0) ' END ||  
              CASE WHEN rec.pp_flag = 1 THEN NULL ELSE ' + NVL (s.promoted_product, 0) ' END ||  
                            '  = 0
          ' || CASE WHEN rec.kas_flag = 0 THEN NULL ELSE ' AND NVL (s.key_account_state, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.ka_flag = 0 THEN NULL ELSE ' AND NVL (s.key_account, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.cc_flag = 0 THEN NULL ELSE ' AND NVL (s.corporate_customer, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.cn_flag = 0 THEN NULL ELSE ' AND NVL (s.customer_name, 0)  <> 0 ' END || '   
          ' || CASE WHEN rec.sb_flag = 0 THEN NULL ELSE ' AND NVL (s.sub_banner, 0)  <> 0 ' END || '     
          ' || CASE WHEN rec.b_flag = 0 THEN NULL ELSE ' AND NVL (s.brand, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.pg_flag = 0 THEN NULL ELSE ' AND NVL (s.promotion_group, 0)  <> 0 ' END || ' 
          ' || CASE WHEN rec.pp_flag = 0 THEN NULL ELSE ' AND NVL (s.promoted_product, 0)  <> 0 ' END || '                             
          AND s.settlement_status_id IN (1, 10)');
          
          COMMIT;
    
    END LOOP;      
    
    /*
    INSERT INTO /*+ APPEND NOLOGGING  rr_settlement_matrix
    SELECT /*+ full(s) parallel(s, 32) index(m, mdp_claim_idx) 
                 settlement_id, 
                 m.item_id,
                 m.location_id
    FROM settlement s,
    mdp_matrix m
    WHERE 1 = 1
    AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
    AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
    AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_l_att_6_ep_id, s.corporate_customer)
    AND m.t_ep_e1_cust_cat_4_ep_id = DECODE (NVL (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
    AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
    AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
    AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
    AND NVL(s.rr_default_claim, 0) = 0
    AND   NVL (s.key_account_state, 0)
                + NVL (s.key_account, 0)
                + NVL (s.corporate_customer, 0)
                + NVL (s.customer_name, 0) 
                + NVL (s.brand, 0) 
                + NVL (s.promotion_group, 0) 
                + NVL (s.promoted_product, 0) > 0
    AND s.settlement_status_id IN (1, 10);
    
    COMMIT;
    
    */    
    
    dynamic_ddl('TRUNCATE TABLE rr_proposed_match');
    
    -- invoiced customer -- 
    
    MERGE INTO settlement s
    USING(SELECT /*+ parallel */
                 s.settlement_id, 
                 MAX(m.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id,
                 MAX(m.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
                 MAX(m.t_ep_L_ATT_2_ep_id) t_ep_L_ATT_2_ep_id, 
                 MAX(m.t_ep_L_ATT_6_ep_id) t_ep_L_ATT_6_ep_id
          FROM settlement s,
          location m
          WHERE 1 = 1
          AND s.key_account = m.t_ep_ebs_customer_ep_id
          AND NVL(s.rr_default_claim, 0) = 0
          AND   NVL (s.key_account_state, 0)
                + NVL (s.key_account, 0)
                + NVL (s.corporate_customer, 0)
                + NVL (s.customer_name, 0) 
                + NVL (s.brand, 0) 
                + NVL (s.promotion_group, 0) 
                + NVL (s.promoted_product, 0) > 0
          AND s.settlement_status_id IN (1, 10)
          GROUP BY s.settlement_id) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id, 
    s.t_ep_L_ATT_2_ep_id = s1.t_ep_L_ATT_2_ep_id, 
    s.t_ep_L_ATT_6_ep_id = s1.t_ep_L_ATT_6_ep_id,
    s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id;

    COMMIT; 
    
     MERGE INTO settlement s
     USING (SELECT                            /*+ parallel(sm, 32) full(sm) */
                  s.settlement_id,
                   MAX (l.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id,
                   max (l.t_ep_e1_cust_cat_2_ep_id) t_ep_e1_cust_cat_2_ep_id,
                   max (l.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
                   MAX (l.t_ep_l_att_6_ep_id) t_ep_l_att_6_ep_id   ---- added sales office , fn 12/11/2015
              FROM settlement s, location l        --rr_settlement_matrix sm
             WHERE 1 = 1
                   AND NVL (s.key_account_state ,0) = CASE NVL (s.key_account_state , 0)
                          WHEN 0 THEN  NVL (s.key_account_state, 0)
                          ELSE  l.t_ep_ebs_account_ep_id 
                       end
                   AND NVL (s.key_account, 0)= CASE NVL (s.key_account, 0)
                          WHEN 0 THEN NVL (s.key_account, 0)
                          ELSE  l.t_ep_ebs_customer_ep_id 
                       END
                   AND NVL (s.corporate_customer, 0) = CASE NVL (s.corporate_customer, 0)
                          WHEN 0 THEN NVL (s.corporate_customer, 0)
                          ELSE  l.t_ep_ebs_tp_zone_EP_ID										-- Sanjiiv Banner Group Change
                       end
		               AND NVL (s.customer_name, 0) = CASE NVL (s.customer_name, 0)
                          WHEN 0 THEN NVL (s.customer_name, 0)
                          ELSE  l.t_ep_e1_cust_cat_4_ep_id
                       END
                   AND TRUNC (s.LAST_UPDATE_DATE) BETWEEN TRUNC (SYSDATE - 1)
                                                    AND TRUNC (SYSDATE)
                   AND (   NVL (s.key_account_state , 0) <> 0
                        OR NVL (s.key_account, 0) <> 0
                        OR NVL (s.corporate_customer, 0) <> 0
			OR NVL (s.customer_name, 0) <> 0) group by s.settlement_id) s1
        ON (s.settlement_id = s1.settlement_id)
WHEN MATCHED
THEN
   UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id,
    s.t_ep_e1_cust_cat_2_ep_id = s1.t_ep_e1_cust_cat_2_ep_id,
 --   s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id,
    s.t_ep_l_att_6_ep_id = s1.t_ep_l_att_6_ep_id;   ---- ---- added sales office , fn 12/11/2015
     
    COMMIT;
        
      -- Calculate Store Quantity and Cartons based on Claim Amount and Case / Unit Deal for Promotion Groups--
      -- May need to verify this logic - possibly an item in a promo grp may not all have the same units per case --
      -- Calculate Store Quantity and Cartons based on Claim Amount and Case / Unit Deal for Promoted Products--
      
    MERGE INTO settlement s
    USING(SELECT /*+ full(s) parallel(s,32) full(m) prallel(m, 32) */
                 s.settlement_id, 
                 MAX(i.e1_vol_uom_mult) units_per_case,
                 MAX(CASE WHEN s.case_deal > 0 THEN SAFE_DIVISION(s.claim_case_deal_d, s.case_deal, 0) 
                      WHEN s.case_hf_deal >0 THEN SAFE_DIVISION(s.claim_case_deal_hf_d, s.case_hf_deal, 0)
                      ELSE NULL
                 END) cartons, 
                 MAX(CASE WHEN s.unit_deal > 0 THEN SAFE_DIVISION(s.claim_case_deal_d, s.unit_deal, 0) 
                      WHEN s.unit_hf_deal >0 THEN SAFE_DIVISION(s.claim_case_deal_hf_d, s.unit_hf_deal, 0)
                      ELSE NULL
                 END) store_quantity
          FROM settlement s,
          mdp_matrix m,
          t_ep_item i,
          rr_settlement_matrix sm
          WHERE 1 = 1
          AND m.item_id = sm.item_id
          AND m.location_id = sm.location_id
          AND s.settlement_id = sm.settlement_id
          AND m.t_ep_item_ep_id = i.t_ep_item_ep_id
          AND NVL(s.rr_default_claim, 0) = 0
          AND NVL (s.promotion_group, 0) 
              + NVL (s.promoted_product, 0) > 0
          AND s.settlement_status_id IN (1, 10)
          GROUP BY s.settlement_id) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.cartons = s1.cartons,
    s.store_quantity = s1.store_quantity,
    s.units_per_case = s1.units_per_case;
    
    COMMIT;
    
    v_batch_dt := 'PromotionId'; 
    
    INSERT INTO rr_proposed_match(seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
                                  deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff) 
    SELECT rr_proposed_match_seq.nextval, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
    SELECT /*+ full(s) parallel(s,32) */ s.settlement_id, p.promotion_id, 
           0 date_diff,
           1 deal_type,
           0 deal_type_diff,
           0 deal_diff, 
           0 start_dt_diff, 0 row_rank, v_batch_dt batch_date,
           0 fund_avail,           
           1 deal_match_diff
    FROM settlement s, 
    promotion p, 
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss    
    WHERE 1 = 1 
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0
    AND p.scenario_id IN (22, 262, 162)
    AND s.promotion_id = p.promotion_id
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.promotion_stat_id = ps.promotion_stat_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1    
    AND NVL (s.promotion_id, 0) > 0
    GROUP BY s.settlement_id, p.promotion_id);
    
    COMMIT;
    
    v_batch_dt := 'CustPromo'; 
    
    INSERT INTO rr_proposed_match(seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
                                  deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff) 
    SELECT rr_proposed_match_seq.nextval, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
    SELECT /*+ full(s) parallel(s,32) */ s.settlement_id, p.promotion_id, 
           CASE WHEN MAX(ABS((next_day(pdt.from_date,v_day)-7)-(next_day(s.start_date,v_day)-7))) + 
                  MAX(ABS((pdt.until_date)-(pdt.from_date-(next_day(pdt.from_date,v_day)-7))-(s.end_date-(s.start_date-(next_day(s.start_date,v_day)-7))))) <= 0 THEN 0
               WHEN MAX((NEXT_DAY(TRUNC(pdt.from_date, 'DD'),v_day)-7)) <= MAX((NEXT_DAY(TRUNC(s.start_date, 'DD'),v_day)-7))  
                  AND MAX((TRUNC(pdt.until_date, 'DD'))-(TRUNC(pdt.from_date, 'DD')-(NEXT_DAY(TRUNC(pdt.from_date, 'DD'),v_day)-7))) 
                                                    >= MAX(TRUNC(s.end_date, 'DD')- (TRUNC(s.start_date, 'DD')-(NEXT_DAY(TRUNC(s.start_date, 'DD'),v_day)-7)))  THEN 1
               ELSE 3 END date_diff,
           2 deal_type,
           0 deal_type_diff,
           0 deal_diff, 
           0 start_dt_diff, 0 row_rank, v_batch_dt batch_date,
           0 fund_avail,
           1 deal_match_diff
    FROM settlement s, 
    promotion p, 
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss,
    promotion_dates pdt
    WHERE 1 = 1 
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0
    AND p.scenario_id IN (22, 262, 162)
    AND s.customer_promo_no = p.cust_promo_no
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.promotion_stat_id = ps.promotion_stat_id
    AND p.promotion_id = pdt.promotion_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1    
    AND NVL (s.customer_promo_no, '!!!@#$!!!') <> '!!!@#$!!!'
    AND NVL (s.promotion_id, 0) = 0
    GROUP BY s.settlement_id, p.promotion_id);
    
    COMMIT;
    
    v_batch_dt := 'Deals'; 
  
    INSERT INTO rr_proposed_match(seq_id, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
                                  deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff) 
    SELECT rr_proposed_match_seq.nextval, settlement_id, promotion_id, date_diff, deal_type, deal_type_diff, 
           deal_diff, start_dt_diff, row_rank, batch_date, fund_avail, deal_match_diff
    FROM (
   
    SELECT /*+ full(s) parallel(s, 16) full(m) parallel(m, 16) */ s.settlement_id, pd.promotion_id, 
            CASE WHEN MAX(ABS((next_day(TRUNC(pdt.from_date,'DD'),v_day)-7)-(next_day(TRUNC(s.start_date,'DD'),v_day)-7))) + 
                  MAX(ABS((TRUNC(pdt.until_date,'DD'))-(TRUNC(pdt.from_date,'DD')-(next_day(TRUNC(pdt.from_date,'DD'),v_day)-7))-(TRUNC(s.end_date,'DD')-(TRUNC(s.start_date,'DD')-(next_day(TRUNC(s.start_date,'DD'),v_day)-7))))) <= 0 THEN 0            
             WHEN MAX((NEXT_DAY(TRUNC(pdt.from_date, 'DD'),v_day)-7)) <= MAX((NEXT_DAY(TRUNC(s.start_date, 'DD'),v_day)-7))  
                  AND MAX((TRUNC(pdt.until_date, 'DD'))-(TRUNC(pdt.from_date, 'DD')-(NEXT_DAY(TRUNC(pdt.from_date, 'DD'),v_day)-7))) 
                      >= MAX(TRUNC(s.end_date, 'DD')- (TRUNC(s.start_date, 'DD')-(NEXT_DAY(TRUNC(s.start_date, 'DD'),v_day)-7)))  THEN 1
               ELSE 3 END date_diff,
           MIN(CASE WHEN s.case_deal <> 0 THEN 3 
                WHEN s.unit_deal <> 0 THEN 4
                WHEN s.co_op_$ <> 0 THEN 5
                WHEN s.case_hf_deal <> 0 THEN 6
                WHEN s.unit_hf_deal <> 0 THEN 7     
                WHEN s.claim_coop_hf <> 0 THEN 8
                ELSE 20
           END) deal_type,
           MIN(CASE 
                WHEN NVL(s.claim_case_deal_hf_d,0) <> 0 or NVL(s.claim_coop_hf,0) <> 0
                THEN 0
                WHEN NVL(pd.case_buydown,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) = NVL(p.cd_deal_type, 0) AND NVL(s.claim_case_deal_d, 0) <> 0
                THEN 0
                WHEN NVL(pd.event_cost, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) = NVL(p.coop_deal_type, 0) AND NVL(s.co_op_$, 0) <> 0
                THEN 0
                WHEN NVL(s.claim_case_deal_d,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) = 0 OR NVL(s.co_op_$, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) = 0
                THEN 0
                WHEN NVL(pd.case_buydown,0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> NVL(p.cd_deal_type, 0) 
                THEN 2
                WHEN NVL(pd.event_cost, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> NVL(p.coop_deal_type, 0) 
                THEN 2
                WHEN NVL(s.deal_type_cd_claim, 0) <> 0 AND NVL(s.deal_type_cd_claim, 0) <> NVL(p.cd_deal_type, 0) 
                THEN 3
                WHEN NVL(s.deal_type_coop_claim, 0) <> 0 AND NVL(s.deal_type_coop_claim, 0) <> NVL(p.coop_deal_type, 0) 
                THEN 3                
                ELSE 3
           END) deal_type_diff,
           CASE WHEN MAX(sm.rr_claim_max_cd) = 1 AND MAX(s.case_deal) <> 0 THEN CASE WHEN MAX(NVL(s.case_deal, 0)) <= MAX(NVL(pd.case_buydown, 0)) THEN 0  WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS(MAX(NVL(s.case_deal, 0)) - MAX(NVL(pd.case_buydown, 0))) ELSE 9999999999 END
                WHEN MAX(sm.rr_claim_max_cd) = 1 AND MAX(s.unit_deal) <> 0 THEN CASE WHEN (MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) <= MAX(NVL(pd.case_buydown, 0)) THEN 0  WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS((MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) - (MAX(NVL(pd.case_buydown, 0)))) ELSE 9999999999 END
                WHEN MAX(s.case_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS(MAX(NVL(s.case_deal, 0)) - MAX(NVL(pd.case_buydown, 0))) ELSE 9999999999 END
                WHEN MAX(s.unit_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN ABS((MAX(NVL(s.unit_deal, 0)) * MAX(NVL(pd.units, 0))) - (MAX(NVL(pd.case_buydown, 0)))) ELSE 9999999999 END
                WHEN MAX(s.co_op_$) <> 0 THEN CASE WHEN SUM(NVL(pd.event_cost, 0)) <> 0 THEN ABS(MAX(NVL(s.co_op_$, 0)) - SUM(NVL(pd.event_cost, 0)))   ELSE 9999999999 END
                WHEN MAX(s.case_hf_deal) <> 0 THEN CASE WHEN (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))) <> 0 THEN ABS(MAX(NVL(s.case_hf_deal, 0)) - (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))))  ELSE 9999999999 END
                WHEN MAX(s.unit_hf_deal) <> 0 THEN CASE WHEN MAX(NVL(pd.rr_handling_d, 0)) <> 0 THEN ABS(MAX(NVL(s.unit_hf_deal, 0)) - MAX(NVL(pd.rr_handling_d, 0)))  ELSE 9999999999 END
                WHEN MAX(s.claim_coop_hf) <> 0 THEN CASE WHEN SUM(NVL(pd.rr_handling_coop, 0)) <> 0 THEN ABS(MAX(NVL(s.claim_coop_hf, 0)) - SUM(NVL(pd.rr_handling_coop, 0))) ELSE 9999999999 END
                ELSE 9999999999
           END  deal_diff, 
           MAX(ABS(pdt.from_date-s.start_date)) start_dt_diff, 0 row_rank, v_batch_dt batch_date,
           CASE WHEN MAX(s.case_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_cd_pd, 0))
                WHEN MAX(s.unit_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_cd_pd, 0))
                WHEN MAX(s.co_op_$) <> 0 THEN SUM(NVL(pd.rr_accrual_coop_pd, 0))
                WHEN MAX(s.case_hf_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_cd_pd, 0))
                WHEN MAX(s.unit_hf_deal) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_cd_pd, 0)) 
                WHEN MAX(s.claim_coop_hf) <> 0 THEN SUM(NVL(pd.rr_accrual_hf_coop_pd, 0)) 
                ELSE 0
           END fund_avail,
           CASE WHEN MAX(s.case_deal) <> 0 OR MAX(s.claim_case_deal_d) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.unit_deal) <> 0 OR MAX(s.claim_case_deal_d) <> 0 THEN CASE WHEN MAX(NVL(pd.case_buydown, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.co_op_$) <> 0 THEN CASE WHEN SUM(NVL(pd.event_cost, 0)) <> 0 THEN 1  ELSE 2 END
                WHEN MAX(s.case_hf_deal) <> 0 OR MAX(s.claim_case_deal_hf_d) <> 0 THEN CASE WHEN (MAX(NVL(pd.rr_handling_d, 0)) * MAX(NVL(pd.units, 0))) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.unit_hf_deal) <> 0 OR MAX(s.claim_case_deal_hf_d) <> 0 THEN CASE WHEN MAX(NVL(pd.rr_handling_d, 0)) <> 0 THEN 1 ELSE 2 END
                WHEN MAX(s.claim_coop_hf) <> 0 THEN CASE WHEN SUM(NVL(pd.rr_handling_coop, 0)) <> 0 THEN 1 ELSE 2 END
                ELSE 2
           END  deal_match_diff 
    FROM settlement s, 
    promotion_data pd, 
    mdp_matrix m, 
    promotion p, 
    promotion_dates pdt,
    promotion_stat ps,
    promotion_type pt,
    settlement_status ss,
    rr_settlement_matrix sm
    WHERE 1 = 1 
    AND s.settlement_status_id = ss.settlement_status_id
    AND ss.rr_link_claim = 1   
    AND s.rr_default_claim = 0    
    AND p.scenario_id IN (22, 262, 162)
    AND pd.promotion_id = p.promotion_id
    AND pdt.promotion_id = p.promotion_id
    AND pd.item_id = m.item_id 
    AND pd.is_self = 1 -- lannapra Added 15May 2015   
    AND pd.location_id = m.location_id
    AND pd.promotion_type_id = pt.promotion_type_id
    AND pd.promotion_stat_id = ps.promotion_stat_id
    AND pt.rr_link_claim = 1
    AND ps.rr_link_claim = 1
    --AND s.t_ep_l_att_10_ep_id = m.t_ep_l_att_10_ep_id
--    AND s.t_ep_e1_cust_cat_2_ep_id = m.t_ep_e1_cust_cat_2_ep_id
    AND m.item_id = sm.item_id
    AND m.location_id = sm.location_id
    AND s.settlement_id = sm.settlement_id
    /* AND ABS(NVL(s.case_deal, 0))
        + ABS(NVL(s.unit_deal, 0)) 
        + ABS(NVL(s.co_op_$, 0)) 
        + ABS(NVL(s.case_hf_deal, 0)) 
        + ABS(NVL(s.unit_hf_deal, 0)) 
        + ABS(NVL(s.claim_coop_hf, 0)) <> 0 */
    AND NVL (s.customer_promo_no, '!!!@#$!!!') = '!!!@#$!!!'
    AND NVL (s.promotion_id, 0) = 0
    AND NVL (s.key_account_state, 0)
        + NVL (s.key_account, 0)
        + NVL (s.corporate_customer, 0)
        + NVL (s.customer_name, 0) 
        + NVL (s.brand, 0) 
        + NVL (s.promotion_group, 0) 
        + nvl (s.promoted_product, 0)
        + NVL (s.sub_banner, 0)> 0      ---- added sales office , fn 12/11/2015
    AND pd.sales_date BETWEEN NEXT_DAY(s.start_date,v_day) - 7 AND NEXT_DAY(s.end_date,v_day) - 1 --Align to Start and End of week
    GROUP BY s.settlement_id, pd.promotion_id);
   
    COMMIT;
   
    MERGE INTO rr_proposed_match pm
    USING(SELECT seq_id, settlement_id, promotion_id, 
    RANK() OVER ( PARTITION BY settlement_id ORDER BY deal_match_diff, deal_type_diff, deal_diff, date_diff, start_dt_diff, fund_avail desc, seq_id) row_rank
    FROM rr_proposed_match
    WHERE 1 = 1
    --AND batch_date = v_batch_dt
    ) pm1
    ON(pm.seq_id = pm1.seq_id)
    WHEN MATCHED THEN 
    UPDATE SET pm.row_rank = pm1.row_rank;
    
    COMMIT;
    
    MERGE INTO settlement s
    USING (SELECT settlement_id, promotion_id, deal_match_diff, date_diff, deal_type, deal_type_diff, deal_diff, start_dt_diff
    FROM rr_proposed_match 
    WHERE row_rank = 1 
    --AND batch_date = v_batch_dt
    ) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.promotion_id = CASE WHEN deal_type_diff <> 0 THEN 0 ELSE s1.promotion_id END,
               s.settlement_status_id = CASE WHEN deal_type_diff <> 0 THEN 10  --Unlinked   LP 29/11/13
                                             WHEN deal_type IN (20) THEN --LP 29/11/13
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                     THEN 12  --Linked PM
                                                       ELSE 12 END
                                             WHEN deal_type IN (1, 2) THEN
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff between 0 and 0.01 THEN 8   --Exact Match
                                                       WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff > 0.01             THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                                                      THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             WHEN deal_type IN (3, 4, 6, 7) THEN
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff between 0 and 0.01 THEN 8   --Exact Match
                                                       WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 AND deal_diff > 0.01             THEN 9   --Linked CD
                                                       WHEN date_diff > 1                                                                                      THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             WHEN deal_type IN (5, 8) THEN 
                                                  CASE WHEN deal_match_diff = 1 AND date_diff in (0,1) AND deal_type_diff = 0 THEN 8   --Exact Match
                                                       WHEN date_diff > 1                                                     THEN 12  --Linked PM
                                                       ELSE 12 
                                                  END
                                             ELSE 12
                                        END,
               s.last_update_date = SYSDATE,
               s.link_date        = SYSDATE,
               s.date_diff        = s1.date_diff,
               s.deal_match_diff  = s1.deal_match_diff , 
               s.deal_type_diff   = s1.deal_type_diff  ;

    COMMIT;
    
    MERGE INTO settlement s
    USING (SELECT settlement_id 
    FROM settlement s,
    settlement_status ss
    WHERE s.settlement_status_id = ss.settlement_status_id  
    AND ss.rr_link_claim = 1
    AND s.rr_default_claim = 0
    AND NOT EXISTS (SELECT 1 FROM rr_proposed_match pm WHERE pm.settlement_id = s.settlement_id)) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN 
    UPDATE SET s.settlement_status_id = 10,
               s.last_update_date = SYSDATE;  
                
    COMMIT;

  -----------------------------------------------------------------------------
  ---------------------Update Promotion Status---------------------------------
   
    MERGE INTO promotion_data pd
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_proposed_match pm, settlement s
    WHERE pm.row_rank = 1 
    AND pm.batch_date = v_batch_dt
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) pd1
    ON(pd.promotion_id = pd1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET pd.promotion_stat_id = 7,
               last_update_date = SYSDATE WHERE pd.promotion_stat_id <> 7;
               
    COMMIT;
    
    MERGE INTO promotion_matrix pm
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_proposed_match pm, settlement s
    WHERE pm.row_rank = 1     
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) pm1
    ON(pm.promotion_id = pm1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET pm.promotion_stat_id = 7,
               pm.promotion_data_lud = SYSDATE,
               last_update_date = SYSDATE WHERE pm.promotion_stat_id <> 7;
               
    COMMIT;
  
    MERGE INTO promotion p
    USING(SELECT DISTINCT s.promotion_id
    FROM rr_proposed_match pm, settlement s
    WHERE pm.row_rank = 1   
    AND pm.settlement_id = s.settlement_id
    AND s.promotion_id > 0) p1
    ON(p.promotion_id = p1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET p.promotion_stat_id = 7,
               last_update_date = SYSDATE WHERE p.promotion_stat_id <> 7;    
    
    COMMIT;

----SLOB Workaround -----

MERGE INTO settlement s USING(
    SELECT s.settlement_id 
      FROM promotion p, settlement s  
     WHERE s.promotion_id = p.promotion_id 
       AND p.scenario_id in (236,22,162)
       AND p.promotion_type_id = 13 --SLOB
       AND p.approval <> 1
       AND s.gl_exp_claim <> 1
) s1
ON (s.settlement_id = s1.settlement_id)
WHEN MATCHED THEN UPDATE
SET s.settlement_status_id = 6, --Denied
    s.last_update_date = SYSDATE;

COMMIT;

    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;
   
   PROCEDURE prc_approve_claim 
   AS
   
    datevar       NUMBER (10);
    datevarind    NUMBER (10);
    
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;  
    
    mindate       DATE;
    maxdate       DATE;
    v_debug       NUMBER := 0;
    v_debug_count NUMBER  := 0;
    v_day         VARCHAR2(10);
    
    v_batch_dt    VARCHAR2(20);
    
    approved_claim_amount   NUMBER         := 0;
    ttl_claim_amount        NUMBER;
    minclaim_allowance      NUMBER;
    maxclaim_allowance      NUMBER;
    small_claim             NUMBER;
    num_of_lines            NUMBER;
    
    v_no_lines_cd           NUMBER;
    v_no_lines_coop         NUMBER;
    v_no_lines_hf_cd        NUMBER;
    v_no_lines_hf_coop      NUMBER;
    
    v_unit_scan             NUMBER;
    v_promo_id              NUMBER;
    v_promo_stat_id         NUMBER;
    v_accrual_cd            NUMBER;
    v_accrual_coop          NUMBER;
    v_accrual_hf_cd         NUMBER;
    v_accrual_hf_coop       NUMBER;
    
    v_ttl_cd                NUMBER;
    v_ttl_coop              NUMBER;
    v_ttl_hf_cd             NUMBER;
    v_ttl_hf_coop           NUMBER;
   
   
   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_APPROVE_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'DSMTtlClaimAllowance', ttl_claim_amount);
    get_param ('sys_params', 'DSMMinClaimAllowance', minclaim_allowance);
    get_param ('sys_params', 'DSMMaxClaimAllowance', maxclaim_allowance);
    get_param ('sys_params', 'DSMSmallClaim', small_claim);

    -----Reset Claims from Unapproved to Linked, ready for Re-approval----
    UPDATE settlement
       SET settlement_status_id = 8,
           last_update_date = sysdate
     WHERE settlement_status_id = 3;
     
    COMMIT;  


----SLOB Workaround -----

MERGE INTO settlement s USING(
    SELECT s.settlement_id 
      FROM promotion p, settlement s  
     WHERE s.promotion_id = p.promotion_id 
       AND p.scenario_id in (236,22,162)
       AND p.promotion_type_id = 13 --SLOB
       AND p.approval <> 1
       AND s.gl_exp_claim <> 1
) s1
ON (s.settlement_id = s1.settlement_id)
WHEN MATCHED THEN UPDATE
SET s.settlement_status_id = 6, --Denied
    s.last_update_date = SYSDATE;

COMMIT;

    
    ----FIRST SECTION - THROW ALL TTL CLAIMS > TRUE CLAIM INTO DISPUTE --
    MERGE INTO settlement s
    USING(SELECT s.promotion_id,
                 SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                 SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                 SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                 SUM( NVL(s.co_op_$, 0) ) ttl_coop
          FROM settlement s,
          settlement_status ss
          WHERE s.settlement_status_id = ss.settlement_status_id
          AND ss.settlement_status_id IN (4, 8)
          AND NVL(s.rr_default_claim, 0) = 0
          GROUP BY s.promotion_id
          HAVING SUM (NVL(s.claim_case_deal_hf_d, 0) + NVL(s.claim_coop_hf, 0) + NVL(s.claim_case_deal_d, 0) + NVL(s.co_op_$, 0) ) > ttl_claim_amount)s1
    ON(s.promotion_id = s1.promotion_id)
    WHEN MATCHED THEN 
    UPDATE SET settlement_status_id = 6,
               last_update_date = SYSDATE
    WHERE s.settlement_status_id IN (8);
    
    COMMIT;
    
       -- Select All Claims for both Approved and Linked Status to be checked against True Scan Amount--
   -- Claims will be Unapproved if Claim already entered and Claim Amount Over Scanned Amount --
    FOR rec IN (SELECT    s.promotion_id,
                          NVL (s.customer_promo_no, 0) customer_promo_no,               -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                          DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) cpn, -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                          /* NVL (s.key_account_state, 0) key_account_state,
                          NVL (s.key_account, 0) key_account,
                          NVL (s.corporate_customer, 0) corporate_customer,
                          NVL (s.customer_name, 0) customer_name,
                          NVL (s.brand, 0) brand,
                          NVL (s.promotion_group, 0) promotion_group,
                          NVL (s.promoted_product, 0) promoted_product,  */                        
                          SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                          SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                          SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                          SUM( NVL(s.co_op_$, 0) ) ttl_coop
                     FROM settlement s
                    WHERE s.settlement_status_id IN (8) --(8,4)
                      --AND s.gl_exp_claim = 0                                      -- LP 02/02/2011 - Only Check Claims that have not exported
                      AND s.rr_default_claim = 0
                      AND s.promotion_id > 0
                      AND   NVL (s.key_account_state, 0)
                          + NVL (s.key_account, 0)
                          + NVL (s.corporate_customer, 0)
                          + NVL (s.customer_name, 0)
                          + NVL (s.brand, 0)
                          + NVL (s.promotion_group, 0)
                          + nvl (s.promoted_product, 0)
                          + NVL (s.sub_banner, 0)
                          + DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) > 0
                 GROUP BY s.promotion_id,
                          s.customer_promo_no
                          /* s.key_account_state,
                          s.key_account,
                          s.corporate_customer,
                          s.customer_name,
                          s.brand,
                          s.promotion_group,
                          s.promoted_product,
                          s.customer_promo_no */)
    LOOP
        -- Get Count of total number of claim lines for buffer ttl claim vs ttl scan --
          
        IF  -- rec.cpn > 0
              1 = 1
        THEN
           -- Get TRUE Allowed Claim Amount from PD Table where Evt Status is not closed--
               -- Will Auto Deny if Promotion Closed, or Unapprove Claim if over scanned amount --
           
           SELECT SUM (CASE WHEN NVL(s.claim_case_deal_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.co_op_$, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_case_deal_hf_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_coop_hf, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM ( NVL(s.claim_case_deal_d, 0) ),
                  SUM ( NVL(s.co_op_$, 0) ),
                  SUM ( NVL(s.claim_case_deal_hf_d, 0) ),
                  SUM ( NVL(s.claim_coop_hf, 0) )
              INTO v_no_lines_cd, v_no_lines_coop, v_no_lines_hf_cd, v_no_lines_hf_coop,
                   v_ttl_cd, v_ttl_coop, v_ttl_hf_cd, v_ttl_hf_coop
              FROM settlement s
              WHERE s.promotion_id = rec.promotion_id
                AND s.settlement_status_id IN (4, 8)
                AND NVL(s.rr_default_claim, 0) = 0;
           
           BEGIN
           
              SELECT /*+ parallel (pd,16) parallel (p,16) */   p.promotion_id, p.promotion_stat_id,
                    SUM (  NVL (pd.rr_accrual_cd_ad_gl, 0) ) rr_accrual_cd_pd,
                    SUM (  NVL (pd.rr_accrual_coop_ad_gl, 0) ) rr_accrual_coop_pd,
                    SUM (  NVL (pd.rr_accrual_hf_cd_ad_gl, 0) ) rr_accrual_hf_cd_pd,
                    SUM (  NVL (pd.rr_accrual_hf_coop_ad_gl, 0) ) rr_accrual_hf_coop_pd                                         
                    -- rec.customer_promo_no,                                 -- LP 02/02/2011 - Not Required
                     --rec.ttl_settlement
                INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
                FROM accrual_data pd, promotion p
               WHERE rec.promotion_id = p.promotion_id
                 AND p.promotion_id = pd.accrual_id
                 AND p.scenario_id IN (22, 262, 162)
              GROUP BY p.promotion_id,
                     p.promotion_stat_id;
                     --rec.customer_promo_no
                     --rec.ttl_settlement
           EXCEPTION
           WHEN NO_DATA_FOUND THEN 
                    
              SELECT 
                    p.promotion_id, p.promotion_stat_id,
                    0 rr_accrual_cd_pd,
                    0 rr_accrual_coop_pd,
                    0 rr_accrual_hf_cd_pd,
                    0 rr_accrual_hf_coop_pd                     
              INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
              FROM promotion p
              WHERE p.promotion_id = rec.promotion_id
              AND p.scenario_id IN (22, 262, 162);
              
           WHEN OTHERS THEN
              RAISE;
           END;         
              --Auto Deny Claims where Promotion is Closed--
              IF v_promo_stat_id = 8
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 6,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0;
              END IF;
  
              --Change Status to Unapproved for LINKED Claims that are over TRUE claim At CustState Level--
              IF (
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop <= small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (minclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (minclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (minclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (minclaim_allowance * v_no_lines_hf_coop)))
                 OR 
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop > small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (maxclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (maxclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (maxclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (maxclaim_allowance * v_no_lines_hf_coop)))
                 )
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 3,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;
              END IF;          
        END IF;
    END LOOP;
    
    FOR rec IN (SELECT    s.promotion_id,
                          NVL (s.customer_promo_no, 0) customer_promo_no,               -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                          DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) cpn, -- LP 02/02/2011 - Change from customer_promo_no to promotion_id                          
                          /* NVL (s.key_account_state, 0) key_account_state,
                          NVL (s.key_account, 0) key_account,
                          NVL (s.corporate_customer, 0) corporate_customer,
                          NVL (s.customer_name, 0) customer_name,
                          NVL (s.brand, 0) brand,
                          NVL (s.promotion_group, 0) promotion_group,
                          NVL (s.promoted_product, 0) promoted_product, */
                          SUM( NVL(s.claim_case_deal_hf_d, 0) ) ttl_hf_cd,
                          SUM( NVL(s.claim_coop_hf, 0) ) ttl_hf_coop,  
                          SUM( NVL(s.claim_case_deal_d, 0) ) ttl_cd,
                          SUM( NVL(s.co_op_$, 0) ) ttl_coop
                     FROM settlement s
                    WHERE s.settlement_status_id IN (8) --(8,4)
                      --AND s.gl_exp_claim = 0                                      -- LP 02/02/2011 - Only Check Claims that have not exported
                      AND s.rr_default_claim = 0
                      AND s.promotion_id > 0
                      AND   NVL (s.key_account_state, 0)
                          + NVL (s.key_account, 0)
                          + NVL (s.corporate_customer, 0)
                          + NVL (s.customer_name, 0)
                          + NVL (s.brand, 0)
                          + NVL (s.promotion_group, 0)
                          + nvl (s.promoted_product, 0)
                          + NVL (s.sub_banner, 0)
                          + DECODE (NVL (s.customer_promo_no, '0'), '0', 0, 0, 0, 1) > 0
                 GROUP BY s.promotion_id,
                          s.customer_promo_no
                          /* s.key_account_state,
                          s.key_account,
                          s.corporate_customer,
                          s.customer_name,
                          s.brand,
                          s.promotion_group,
                          s.promoted_product */
                          )
    LOOP
    
        IF -- rec.cpn > 0
           1 = 1
        THEN
           -- Get TRUE Allowed Claim Amount from PD Table where Evt Status is not closed--
               -- Will Auto Deny if Promotion Closed, or Unapprove Claim if over scanned amount --
           
           SELECT SUM (CASE WHEN NVL(s.claim_case_deal_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.co_op_$, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_case_deal_hf_d, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM (CASE WHEN NVL(s.claim_coop_hf, 0) <> 0 THEN 1 ELSE 0 END),
                  SUM ( NVL(s.claim_case_deal_d, 0) ),
                  SUM ( NVL(s.co_op_$, 0) ),
                  SUM ( NVL(s.claim_case_deal_hf_d, 0) ),
                  SUM ( NVL(s.claim_coop_hf, 0) )
              INTO v_no_lines_cd, v_no_lines_coop, v_no_lines_hf_cd, v_no_lines_hf_coop,
                   v_ttl_cd, v_ttl_coop, v_ttl_hf_cd, v_ttl_hf_coop
              FROM settlement s
              WHERE s.promotion_id = rec.promotion_id
                AND s.settlement_status_id IN (4, 8)
                AND NVL(s.rr_default_claim, 0) = 0;
           
           BEGIN
           
           SELECT /*+ parallel (pd,16) parallel (p,16) */   p.promotion_id, p.promotion_stat_id,
                    SUM (  NVL (pd.rr_accrual_cd_ad_gl, 0) ) rr_accrual_cd_pd,
                    SUM (  NVL (pd.rr_accrual_coop_ad_gl, 0) ) rr_accrual_coop_pd,
                    SUM (  NVL (pd.rr_accrual_hf_cd_ad_gl, 0) ) rr_accrual_hf_cd_pd,
                    SUM (  NVL (pd.rr_accrual_hf_coop_ad_gl, 0) ) rr_accrual_hf_coop_pd                                         
                    -- rec.customer_promo_no,                                 -- LP 02/02/2011 - Not Required
                     --rec.ttl_settlement
                INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
                FROM accrual_data pd, promotion p
               WHERE rec.promotion_id = p.promotion_id
                 AND p.promotion_id = pd.accrual_id
                 AND p.scenario_id IN (22, 262, 162)
            GROUP BY p.promotion_id,
                     p.promotion_stat_id;
                     --rec.customer_promo_no
                     --rec.ttl_settlement
           EXCEPTION
           WHEN NO_DATA_FOUND THEN 
                    
              SELECT 
                    p.promotion_id, p.promotion_stat_id,
                    0 rr_accrual_cd_pd,
                    0 rr_accrual_coop_pd,
                    0 rr_accrual_hf_cd_pd,
                    0 rr_accrual_hf_coop_pd                     
              INTO v_promo_id, v_promo_stat_id, v_accrual_cd, v_accrual_coop, v_accrual_hf_cd, v_accrual_hf_coop
              FROM promotion p
              WHERE p.promotion_id = rec.promotion_id
              AND p.scenario_id IN (22, 262, 162);
              
           WHEN OTHERS THEN
              RAISE;
           END;          
              --Auto Deny Claims where Promotion is Closed--
              IF v_promo_stat_id = 8
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 6,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0;
              END IF;
  
              --Change Status to Unapproved for LINKED Claims that are over TRUE claim At CustState Level--
              IF (
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop <= small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (minclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (minclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (minclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (minclaim_allowance * v_no_lines_hf_coop)))
                 OR 
                 ((v_ttl_cd + v_ttl_coop + v_ttl_hf_cd + v_ttl_hf_coop > small_claim)
                  AND 
                 (v_ttl_cd > v_accrual_cd + (maxclaim_allowance * v_no_lines_cd)
                 OR v_ttl_coop > v_accrual_coop + (maxclaim_allowance * v_no_lines_coop)
                 OR v_ttl_hf_cd > v_accrual_hf_cd + (maxclaim_allowance * v_no_lines_hf_cd)
                 OR v_ttl_hf_coop > v_accrual_hf_coop + (maxclaim_allowance * v_no_lines_hf_coop)))
                 )
              THEN
                 UPDATE settlement s
                    SET settlement_status_id = 3,
                        last_update_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;
              ELSE
                 UPDATE settlement s
                    SET settlement_status_id = 4,
                        last_update_date = SYSDATE,
                        auto_approve_date = SYSDATE
                  WHERE s.promotion_id = v_promo_id
                  --  AND s.customer_promo_no = rec1.customer_promo_no            -- LP 02/02/2011 - Change from customer_promo_no to promotion_id
                    AND s.settlement_status_id = 8
                    AND NVL(s.rr_default_claim, 0) = 0;
  
                 COMMIT;                 
              END IF;          
        END IF;
    
    END LOOP;
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;

   PROCEDURE prc_split_claim 
   AS
   
    datevar            NUMBER (10);
    datevarind         NUMBER (10);
    
    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;  

    v_debug            NUMBER := 0;
    v_debug_count      NUMBER  := 0;
    v_day              VARCHAR2(10);
    
    v_batch_dt         VARCHAR2(20);
    max_scan_date      DATE;
    max_sales_date     DATE;
    maxdate            VARCHAR2 (100);
    mindate            VARCHAR2 (100);
    sql_str            VARCHAR2 (10000);

    v_batch_id         INTEGER;
    v_no_records_exp   INTEGER;
    v_batch_date     DATE := SYSDATE;
   
   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_SPLIT_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';  
    
    SELECT pval
    INTO max_sales_date
    FROM sys_params
    WHERE pname = 'max_sales_date';  
    
    maxdate := NEXT_DAY (TRUNC(SYSDATE, 'DD'), v_day) - 21;
    mindate := NEXT_DAY(ADD_MONTHS (maxdate, -12), v_day);
    
    dynamic_ddl ('TRUNCATE TABLE t_exp_claims_gl_temp');
    dynamic_ddl ('TRUNCATE TABLE t_exp_claims_gl');
    dynamic_ddl ('TRUNCATE TABLE js_export_claim_ids');
    dynamic_ddl ('TRUNCATE TABLE js_non_export_claims');
    check_and_drop('rr_split_settlement_matrix');    --- Added by SR -- Temp issue --
  
   --For Testing Procedures --
     --mindate := '01/01/2007';
     --maxdate := '01/19/2009';

----SLOB Workaround -----

MERGE INTO settlement s USING(
    SELECT s.settlement_id 
      FROM promotion p, settlement s  
     WHERE s.promotion_id = p.promotion_id 
       AND p.scenario_id in (236,22,162)
       AND p.promotion_type_id = 13 --SLOB
       AND p.approval <> 1
       AND s.gl_exp_claim <> 1
) s1
ON (s.settlement_id = s1.settlement_id)
WHEN MATCHED THEN UPDATE
SET s.settlement_status_id = 6, --Denied
    s.last_update_date = SYSDATE;

COMMIT;


------------Reset GL Exp Claim back to No if it is not Yes----------------------
   UPDATE settlement s
      SET s.gl_exp_claim = 0                                       --Set to No
    WHERE s.gl_exp_claim NOT IN (0, 1)                             --No or Yes
      AND s.gl_exp_claim_detail <> 1
      AND NVL(s.rr_default_claim, 0) = 0;

   COMMIT;

-------------Set non export claims to Denied if claim is Closed-----------------
   UPDATE settlement s
      SET settlement_status_id = 6,
          last_update_date = SYSDATE
    WHERE s.promotion_id IN (SELECT /*+ parallel(p,12) */
                                    promotion_id
                               FROM promotion p
                              WHERE promotion_stat_id = 8
                              AND p.scenario_id IN (22, 262, 162))
      AND s.gl_exp_claim <> 1
      AND s.gl_exp_claim_detail <> 1
      AND NVL (s.promotion_id, 0) <> 0
      AND s.rr_default_claim = 0;

    COMMIT;


-----------Set GL Exp Claim for all claims that are not approved----------------
   UPDATE settlement s
      SET s.gl_exp_claim = 2                                   -- Not Approved
    WHERE s.gl_exp_claim = 0 
    AND s.settlement_status_id NOT IN (4, 7)
    AND NVL(s.rr_default_claim, 0) = 0;

   COMMIT;

   --- Create Extract of CURRENT Approved Claims for T_EXP_CLAIMS_GL ----
   
   -- Added by SR -- Temp Issue -- 
                              
    dynamic_ddl('CREATE TABLE rr_split_settlement_matrix AS 
 SELECT /*+  full(s) parallel(s,24) index(m, mdp_claim_idx) */
             pm.promotion_id, pm.item_id, pm.location_id, s.settlement_id
        FROM settlement s, mdp_matrix m, promotion_matrix pm
       WHERE 1 = 1
         AND s.settlement_status_id IN (4, 7)
         -- /*AND s.t_ep_e1_cust_cat_3_ep_id IN (2,3) */       --Only Sync Retail Claims
         AND s.gl_exp_claim_detail = 0
         AND s.gl_exp_claim = 0
         AND NVL(s.rr_default_claim, 0) = 0
         --  LP 28/01/11 - Only get Claims that have no detail exported
         --AND s.promotion_id IS NOT NULL             -- LP 18/05/2011
         AND NVL (s.promotion_id, 0) <> 0
         AND pm.is_self = 1
         AND pm.scenario_id IN (22, 262, 162)
         AND NVL (s.bill_to, 0) <> 0
         --and s.gl_update_pd is not null
         AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
         AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
         AND m.t_ep_ebs_tp_zone_EP_ID = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_ebs_tp_zone_EP_ID, s.corporate_customer)					-- Sanjiiv Banner Group Change
         AND m.t_ep_e1_cust_cat_4_ep_id = DECODE (NVL (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
         AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.sub_banner, 0), 0, m.t_ep_l_att_6_ep_id, s.sub_banner)
         AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
         AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
         AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
         AND pm.promotion_id = s.promotion_id
         AND pm.item_id = m.item_id
         AND pm.location_id = m.location_id
         AND (NVL(pm.rr_alloc_wk0, 0)+NVL(pm.rr_alloc_wk1, 0)+
         NVL(pm.rr_alloc_wk2, 0)+NVL(pm.rr_alloc_wk3, 0)+NVL(pm.rr_alloc_wk4, 0)+
         NVL(pm.rr_alloc_wk5, 0)+NVL(pm.rr_alloc_wk6, 0)) <> 0')
;



  -- Added by SR -- Temp Issue -- 

dynamic_ddl('INSERT      /*+ APPEND NOLOGGING */INTO t_exp_claims_gl_temp
      SELECT /*+  full(m) parallel(m, 32)  */
             pd.sales_date, pd.item_id, pd.location_id, s.settlement_id,
             s.gl_exp_claim, pd.promotion_id,             
                ROUND(DECODE(SUM (NVL(pd.rr_accrual_cd_pd, 0)) OVER (PARTITION BY s.settlement_id),
                 0, (NVL(s.claim_case_deal_d, 0))
                  / COUNT (1) OVER (PARTITION BY s.settlement_id),
                   (NVL(s.claim_case_deal_d, 0)) * ((NVL(pd.rr_accrual_cd_pd, 0))
                    / SUM (NVL(pd.rr_accrual_cd_pd, 0)) OVER (PARTITION BY s.settlement_id)
                   )
                ), 2) approved_casedeal_un,
             ROUND(DECODE
                (SUM (NVL(pd.rr_accrual_coop_pd, 0)) OVER (PARTITION BY s.settlement_id),
                 0, NVL (s.co_op_$, 0)
                  / COUNT (1) OVER (PARTITION BY s.settlement_id),
                   NVL (s.co_op_$, 0)
                 * (  (NVL(pd.rr_accrual_coop_pd, 0))
                    / SUM (NVL(pd.rr_accrual_coop_pd, 0)) OVER (PARTITION BY s.settlement_id)
                   )
                ), 2) approved_coop_un,
             ROUND(DECODE
                (SUM (NVL(pd.rr_accrual_hf_cd_pd, 0)) OVER (PARTITION BY s.settlement_id),
                 0, (NVL(s.claim_case_deal_hf_d, 0))
                  / COUNT (1) OVER (PARTITION BY s.settlement_id),
                   (NVL(s.claim_case_deal_hf_d, 0)) * ((NVL(pd.rr_accrual_hf_cd_pd, 0))
                    / SUM (NVL(pd.rr_accrual_hf_cd_pd, 0)) OVER (PARTITION BY s.settlement_id)
                   )
                ), 2) approved_hf_casedeal_un,
             ROUND(DECODE
                (SUM (NVL(pd.rr_accrual_hf_coop_pd, 0)) OVER (PARTITION BY s.settlement_id),
                 0, NVL (s.claim_coop_hf, 0)
                  / COUNT (1) OVER (PARTITION BY s.settlement_id),
                   NVL (s.claim_coop_hf, 0)
                 * (  (NVL(pd.rr_accrual_hf_coop_pd, 0))
                    / SUM (NVL(pd.rr_accrual_hf_coop_pd, 0)) OVER (PARTITION BY s.settlement_id)
                   )
                ), 2) approved_hf_coop_un,
             0 approved_casedeal,
             0 approved_coop,
             0 approved_hf_casedeal,
             0 approved_hf_coop,
             settlement_desc
        FROM settlement s, rr_split_settlement_matrix m, promotion_data pd, promotion_dates d
       WHERE 1 = 1
         AND s.settlement_status_id IN (4, 7)
         -- /*AND s.t_ep_e1_cust_cat_3_ep_id IN (2,3) */       --Only Sync Retail Claims
         AND s.gl_exp_claim_detail = 0
         AND s.gl_exp_claim = 0
         AND NVL(s.rr_default_claim, 0) = 0
         --  LP 28/01/11 - Only get Claims that have no detail exported
         --AND s.promotion_id IS NOT NULL             -- LP 18/05/2011
         AND NVL (s.promotion_id, 0) <> 0
         AND pd.is_self = 1
         AND pd.promotion_stat_id IN (4, 5, 6, 7)
         AND pd.scenario_id IN (22, 262, 162)
         AND NVL (s.bill_to, 0) <> 0
         --and s.gl_update_pd is not null
         AND pd.promotion_id = m.promotion_id
         AND s.settlement_id = m.settlement_id
         AND pd.item_id = m.item_id
         AND pd.location_id = m.location_id
         AND pd.promotion_id = d.promotion_id
         AND (NEXT_DAY (d.from_date,'''||v_day||''') - 7) <= (NEXT_DAY (s.start_date, '''||v_day||''') - 7) --Offset dates                                                               
         AND (NEXT_DAY (d.until_date, '''||v_day||''') - 1) >= (s.end_date + (((NEXT_DAY (start_date, '''||v_day||''') - 7) - s.start_date))) --Offset dates                                                               
         AND pd.sales_date BETWEEN (NEXT_DAY (s.start_date, '''||v_day||''') - 7)
                               AND (s.end_date + (((NEXT_DAY (start_date, '''||v_day||''') - 7 ) - s.start_date )))');
   
   COMMIT;
   
   MERGE INTO t_exp_claims_gl_temp c
   USING(SELECT i.datet sales_date, c.item_id, c.location_id, c.settlement_id,
                c.gl_exp_claim, c.promotion_id,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN 0 THEN 1 ELSE 0 END * c.approved_casedeal_un), 2) approved_casedeal_un,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN 0 THEN 1 ELSE 0 END * c.approved_coop_un), 2) approved_coop_un,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN 0 THEN 1 ELSE 0 END * c.approved_hf_casedeal_un), 2) approved_hf_casedeal_un,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN 0 THEN 1 ELSE 0 END * c.approved_hf_coop_un), 2) approved_hf_coop_un,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN -42 THEN pm.rr_alloc_wk6 
                                           WHEN -35 THEN pm.rr_alloc_wk5 
                                           WHEN -28 THEN pm.rr_alloc_wk4
                                           WHEN -21 THEN pm.rr_alloc_wk3
                                           WHEN -14 THEN pm.rr_alloc_wk2
                                           WHEN -7 THEN pm.rr_alloc_wk1 
                                           WHEN 0 THEN pm.rr_alloc_wk0
                END * NVL(c.approved_casedeal_un, 0)), 2) approved_casedeal,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN -42 THEN pm.rr_alloc_wk6 
                                           WHEN -35 THEN pm.rr_alloc_wk5 
                                           WHEN -28 THEN pm.rr_alloc_wk4
                                           WHEN -21 THEN pm.rr_alloc_wk3
                                           WHEN -14 THEN pm.rr_alloc_wk2
                                           WHEN -7 THEN pm.rr_alloc_wk1 
                                           WHEN 0 THEN pm.rr_alloc_wk0
                END * NVL(c.approved_coop_un, 0)), 2) approved_coop,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN -42 THEN pm.rr_alloc_wk6 
                                           WHEN -35 THEN pm.rr_alloc_wk5 
                                           WHEN -28 THEN pm.rr_alloc_wk4
                                           WHEN -21 THEN pm.rr_alloc_wk3
                                           WHEN -14 THEN pm.rr_alloc_wk2
                                           WHEN -7 THEN pm.rr_alloc_wk1 
                                           WHEN 0 THEN pm.rr_alloc_wk0
                END * NVL(c.approved_hf_casedeal_un, 0)), 2) approved_hf_casedeal,
                ROUND(SUM(CASE i.datet - c.sales_date WHEN -42 THEN pm.rr_alloc_wk6 
                                           WHEN -35 THEN pm.rr_alloc_wk5 
                                           WHEN -28 THEN pm.rr_alloc_wk4
                                           WHEN -21 THEN pm.rr_alloc_wk3
                                           WHEN -14 THEN pm.rr_alloc_wk2
                                           WHEN -7 THEN pm.rr_alloc_wk1 
                                           WHEN 0 THEN pm.rr_alloc_wk0
                END * NVL(c.approved_hf_coop_un, 0)), 2) approved_hf_coop,
                c.settlement_desc
         FROM t_exp_claims_gl_temp c,
              promotion_matrix pm,
              inputs i
         WHERE c.promotion_id = pm.promotion_id
         AND c.item_id = pm.item_id
         AND c.location_id = pm.location_id
         and pm.is_self = 1 -- lannapra Added 15 May 2015
         AND i.datet BETWEEN c.sales_date - (pm.rr_promo_offset * 7) AND c.sales_date
         GROUP BY i.datet, c.item_id, c.location_id, c.settlement_id,
                  c.gl_exp_claim, c.promotion_id, c.settlement_desc) c1
         ON(c.promotion_id = c1.promotion_id
         AND c.item_id = c1.item_id
         AND c.location_id = c1.location_id
         AND c.sales_date = c1.sales_date
         AND c.settlement_id = c1.settlement_id)
         WHEN MATCHED THEN 
         UPDATE SET c.approved_casedeal = c1.approved_casedeal,
                    c.approved_coop = c1.approved_coop,
                    c.approved_hf_casedeal = c1.approved_hf_casedeal,
                    c.approved_hf_coop = c1.approved_hf_coop
         WHEN NOT MATCHED THEN 
         INSERT (c.sales_date, c.item_id, c.location_id, c.settlement_id, c.gl_exp_claim, c.promotion_id, c.settlement_desc, 
                 c.approved_casedeal_un, c.approved_coop_un, c.approved_hf_casedeal_un, c.approved_hf_coop_un, 
                 c.approved_casedeal, c.approved_coop, c.approved_hf_casedeal, c.approved_hf_coop)
         VALUES(c1.sales_date, c1.item_id, c1.location_id, c1.settlement_id, c1.gl_exp_claim, c1.promotion_id, c1.settlement_desc, 
                c1.approved_casedeal_un, c1.approved_coop_un, c1.approved_hf_casedeal_un, c1.approved_hf_coop_un, 
                c1.approved_casedeal, c1.approved_coop, c1.approved_hf_casedeal, c1.approved_hf_coop);
    
   COMMIT;
   
--------Set GL Exp Claim to Valid if in t_exp_claims_gl_temp-------------------
   MERGE INTO settlement s
   USING
      (
          SELECT c.settlement_id, CASE WHEN c.end_date > sysdate THEN 5 --Future Valid
                                                                    ELSE 4       --Valid
                                                                    END 
                                                                    gl_exp_claim
            FROM settlement c
           WHERE c.settlement_id IN (SELECT          /*+ parallel(t,24) */
                                     DISTINCT t.settlement_id
                                         FROM t_exp_claims_gl_temp t)
             AND c.gl_exp_claim_detail <> 1             
      ) e
      on ( s.settlement_id = e.settlement_id)
      WHEN MATCHED THEN UPDATE
      SET s.gl_exp_claim = e.gl_exp_claim,
          s.last_update_date = sysdate;

   COMMIT;
   

--------Set Remaining Claims in No Status for Retail to Invalid----------------
   UPDATE settlement s
      SET s.gl_exp_claim = 3 ,                                       --Invalid
          s.last_update_date = sysdate
    WHERE s.gl_exp_claim = 0                                              --NO
      AND s.gl_exp_claim_detail <> 1
      AND NVL(s.rr_default_claim, 0) = 0
          /*AND s.t_ep_e1_cust_cat_3_ep_id IN (2,3) */;         --Only Retail Claims;

   COMMIT;

--------Remove Group Claims from Temp if a line is Invalid or Not Approved-----
    DELETE      /*+ nologging */FROM t_exp_claims_gl_temp g
         WHERE g.settlement_id IN (
                                    SELECT /*+ parallel(s1,24) */ s1.settlement_id 
                                     From Settlement S1
                                    WHERE (s1.t_ep_l_att_10_ep_id, s1.settlement_desc) in 
                                            (Select    /*+ parallel(s,24) */
                                            DISTINCT  s.t_ep_l_att_10_ep_id,s.settlement_desc
                                                FROM settlement s
                                               WHERE gl_exp_claim IN (2, 3, 5)));

   -----Calculate missing splits---------
   INSERT      /*+ nologging */INTO t_exp_claims_gl_temp
      (SELECT   /*+ parallel*/
                MIN (s1.sales_date) sales_date, MIN (s1.item_id) item_id,
                MIN (s1.location_id) location_id, s.settlement_id,
                MIN (s.gl_exp_claim) gl_exp_claim,
                MIN (s1.promotion_id) promotion_id,
                NVL (s.claim_case_deal_d, 0)
                - SUM (NVL (s2.approved_casedeal_un, 0)) approved_casedeal_un,
                NVL (s.co_op_$, 0)
                - SUM (NVL (s2.approved_coop_un, 0)) approved_coop_un,
                NVL (s.claim_case_deal_hf_d, 0)
                - SUM (NVL (s2.approved_hf_casedeal_un, 0)) approved_hf_casedeal_un,
                NVL (s.claim_coop_hf, 0)
                - SUM (NVL (s2.approved_hf_coop_un, 0)) approved_hf_coop_un,
                NVL (s.claim_case_deal_d, 0)
                - SUM (NVL (s2.approved_casedeal, 0)) approved_casedeal,
                NVL (s.co_op_$, 0)
                - SUM (NVL (s2.approved_coop, 0)) approved_coop,
                NVL (s.claim_case_deal_hf_d, 0)
                - SUM (NVL (s2.approved_hf_casedeal, 0)) approved_hf_casedeal,
                NVL (s.claim_coop_hf, 0)
                - SUM (NVL (s2.approved_hf_coop, 0)) approved_hf_coop,
                s.settlement_desc
           FROM settlement s,
                (SELECT   /*+ parallel*/
                          promotion_id, settlement_id, item_id, location_id,
                          sales_date sales_date
                     FROM t_exp_claims_gl_temp
                    WHERE rowid IN (SELECT MIN(rowid) FROM t_exp_claims_gl_temp GROUP BY settlement_id)) s1,
                t_exp_claims_gl_temp s2
          WHERE s.settlement_id = s1.settlement_id
            AND s.settlement_id = s2.settlement_id
       GROUP BY s.settlement_id,
                s.claim_case_deal_d,
                s.co_op_$,
                s.claim_case_deal_hf_d,
                s.claim_coop_hf,
                s.settlement_desc);

   COMMIT;

   -- INSERT Approved Claims at GL level to table --
   INSERT      /*+ nologging */INTO t_exp_claims_gl t
      (SELECT   /*+ parallel(g,24) parallel(l,24) parallel(it,24) parallel(s,24) parallel(i,24)  */
                i.dm_item_desc, s.dm_site_desc, g.promotion_id, g.sales_date,
                SUM (g.approved_casedeal), SUM (g.approved_coop),
                SUM (g.approved_hf_casedeal), SUM (g.approved_hf_coop),
                SUM (NVL (g.approved_casedeal, 0) + NVL (g.approved_coop, 0) + NVL (g.approved_hf_casedeal, 0) + NVL (g.approved_hf_coop, 0))
           FROM t_ep_item i,
                t_ep_site s,
                items it,
                location l,
                t_exp_claims_gl_temp g
          WHERE g.item_id = it.item_id
            AND it.t_ep_item_ep_id = i.t_ep_item_ep_id
            AND g.location_id = l.location_id
            AND l.t_ep_site_ep_id = s.t_ep_site_ep_id
       --           AND g.gl_exp_claim = 0       --LP 28/01/11 - Use Detail Flag instead
       GROUP BY i.dm_item_desc, s.dm_site_desc, g.promotion_id, g.sales_date);

   COMMIT;
   
--   SELECT exportsummarymasterid.NEXTVAL
--   INTO v_batch_id
--   FROM DUAL;
--    
--    
--   INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');
--   
--   insert into T_EXP_GL(BATCH_ID, MATERIAL, CUSTOMER_NO, PROMOTION_ID,SALES_ORG,DIST_CHANNEL,DIVISION, SALES_DATE, CASE_DEAL_AMOUNT, COOP_AMOUNT, 
--                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
--      (select   /*+ parallel(g,24) parallel(l,24) parallel(it,24) parallel(s,24) parallel(i,24)  */
--                v_batch_id, I.I_ATT_1 , S.E1_CUST_CAT_4,g.promotion_id,
--    SORG.P4,DC.P3,DIV.P2,  g.sales_date,
--                -SUM (G.APPROVED_CASEDEAL), -SUM (G.APPROVED_COOP),
--             --   -SUM (G.APPROVED_HF_CASEDEAL), -SUM (G.APPROVED_HF_COOP),
--                NULL, cd_dt.deal_type_code, coop_dt.deal_type_code, v_batch_date, 'CLAIM',rec_cust.e1_cust_cat_4
--           FROM T_EP_I_ATT_1 I,
--                T_EP_E1_CUST_CAT_4 S,
--                T_EP_P4 SORG,
--                T_EP_P2 DIV,
--                T_EP_P3 DC,
--                t_ep_e1_cust_cat_4 rec_cust,
--                items it,
--                location l,
--                t_exp_claims_gl_temp g,
--                PROMOTION P,
--                activity ac,
--                tbl_deal_type cd_dt,
--                tbl_deal_type coop_dt
--          where G.ITEM_ID = IT.ITEM_ID
--           and P.RECIPIENT = REC_CUST.T_EP_E1_CUST_CAT_4_EP_ID
--           and p.activity_id = ac.activity_id
--    and it.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
--    and l.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
--    and it.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
--    and it.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
--    and it.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
--          --  AND it.t_ep_item_ep_id = i.t_ep_item_ep_id
--            AND g.location_id = l.location_id
--            AND g.promotion_id = p.promotion_id
--            AND p.scenario_id IN (22, 262, 162)
--            AND NVL(ac.cd_deal_type, 0) = cd_dt.deal_type_id
--            AND NVL(ac.coop_deal_type, 0) = coop_dt.deal_type_id
--          --  AND l.t_ep_site_ep_id = s.t_ep_site_ep_id
--       --           AND g.gl_exp_claim = 0       --LP 28/01/11 - Use Detail Flag instead
--       GROUP BY  I.I_ATT_1 , S.E1_CUST_CAT_4, SORG.P4,DC.P3,DIV.P2, g.promotion_id, g.sales_date, cd_dt.deal_type_code, coop_dt.deal_type_code, rec_cust.e1_cust_cat_4
--       HAVING ABS(SUM (NVL(g.approved_casedeal, 0))) + ABS(SUM (NVL(g.approved_coop, 0))) +
--                ABS(SUM (NVL(g.approved_hf_casedeal, 0))) + ABS(SUM (NVL(g.approved_hf_coop, 0))) <> 0);
--
--   
--       INSERT INTO t_exp_gl_master                   
--                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
--TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER 
--                 FROM t_exp_gl WHERE batch_id = v_batch_id);
--
--   SELECT COUNT (*)
--     INTO v_no_records_exp
--     FROM t_exp_gl
--     WHERE batch_id = v_batch_id;
--
--   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;
--    
--    COMMIT;


   --- Post Approved Claim Information Back to Promotion Data ----
   MERGE /*+ parallel(pd,24) */ INTO promotion_data pd
      USING (SELECT   /*+ parallel(pd,t) */
                      sales_date, item_id, location_id, promotion_id,
                      SUM (approved_casedeal_un) approved_casedeal,
                      SUM (approved_coop_un) approved_coop,
                      SUM (approved_hf_casedeal_un) approved_hf_casedeal,
                      SUM (approved_hf_coop_un) approved_hf_coop
                 FROM t_exp_claims_gl_temp t
             GROUP BY sales_date, item_id, location_id, promotion_id) pd1
      ON (    pd.sales_date = pd1.sales_date
          AND pd.item_id = pd1.item_id
          AND pd.location_id = pd1.location_id
          AND pd.promotion_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.rr_approved_claim_cd_pd =
                     NVL (pd.rr_approved_claim_cd_pd, 0)
                   + NVL (pd1.approved_casedeal, 0),                
                pd.rr_approved_claim_coop_pd =
                       NVL (pd.rr_approved_claim_coop_pd, 0)
                       + NVL (pd1.approved_coop, 0),
                pd.rr_approved_claim_hf_cd_pd =
                     NVL (pd.rr_approved_claim_hf_cd_pd, 0)
                   + NVL (pd1.approved_hf_casedeal, 0),                
                pd.rr_approved_claim_hf_coop_pd =
                       NVL (pd.rr_approved_claim_hf_coop_pd, 0)
                       + NVL (pd1.approved_hf_coop, 0),
                last_update_date = SYSDATE
         ;
   COMMIT;


   --- Post Approved Claim Information Back to Accrual Data ----  LP 28/01/11
   MERGE /*+ parallel(pd,24) */ INTO accrual_data pd
      USING (SELECT   /*+ parallel(t,24) */
                      sales_date, item_id, location_id, promotion_id,
                      SUM (approved_casedeal_un) approved_casedeal_un,
                      SUM (approved_coop_un) approved_coop_un,
                      SUM (approved_hf_casedeal_un) approved_hf_casedeal_un,
                      SUM (approved_hf_coop_un) approved_hf_coop_un,
                      SUM (approved_casedeal) approved_casedeal,
                      SUM (approved_coop) approved_coop,
                      SUM (approved_hf_casedeal) approved_hf_casedeal,
                      SUM (approved_hf_coop) approved_hf_coop
                 FROM t_exp_claims_gl_temp t
             GROUP BY sales_date, item_id, location_id, promotion_id) pd1
      ON (    pd.sales_date = pd1.sales_date
          AND pd.item_id = pd1.item_id
          AND pd.location_id = pd1.location_id
          AND pd.accrual_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.rr_approved_claim_cd_ad =
                     NVL (pd.rr_approved_claim_cd_ad, 0)
                   + NVL (pd1.approved_casedeal, 0),                
                pd.rr_approved_claim_coop_ad =
                     NVL (pd.rr_approved_claim_coop_ad, 0)
                   + NVL (pd1.approved_coop, 0),
                pd.rr_approved_claim_hf_cd_ad =
                     NVL (pd.rr_approved_claim_hf_cd_ad, 0)
                   + NVL (pd1.approved_hf_casedeal, 0),                
                pd.rr_approved_claim_hf_coop_ad =
                     NVL (pd.rr_approved_claim_hf_coop_ad, 0)
                   + NVL (pd1.approved_hf_coop, 0),
                pd.rr_approved_claim_cd_ad_un =
                     NVL (pd.rr_approved_claim_cd_ad_un, 0)
                   + NVL (pd1.approved_casedeal_un, 0),                
                pd.rr_approved_claim_coop_ad_un =
                     NVL (pd.rr_approved_claim_coop_ad_un, 0)
                   + NVL (pd1.approved_coop_un, 0),
                pd.rr_approved_claim_hf_cd_ad_un =
                     NVL (pd.rr_approved_claim_hf_cd_ad_un, 0)
                   + NVL (pd1.approved_hf_casedeal_un, 0),                
                pd.rr_app_claim_hf_coop_ad_un =
                     NVL (pd.rr_app_claim_hf_coop_ad_un, 0)
                   + NVL (pd1.approved_hf_coop_un, 0),
                last_update_date = SYSDATE
      WHEN NOT MATCHED THEN
   INSERT (pd.item_id, pd.location_id, pd.sales_date, pd.accrual_id,
           pd.rr_approved_claim_cd_ad, pd.rr_approved_claim_coop_ad, pd.rr_approved_claim_hf_cd_ad, pd.rr_approved_claim_hf_coop_ad,
           pd.rr_approved_claim_cd_ad_un, pd.rr_approved_claim_coop_ad_un, pd.rr_approved_claim_hf_cd_ad_un, pd.rr_app_claim_hf_coop_ad_un,
           last_update_date)
         VALUES (pd1.item_id, pd1.location_id, pd1.sales_date, pd1.promotion_id,
                 pd1.approved_casedeal, pd1.approved_coop, pd1.approved_hf_casedeal, pd1.approved_hf_coop,
                 pd1.approved_casedeal_un, pd1.approved_coop_un, pd1.approved_hf_casedeal_un, pd1.approved_hf_coop_un,
                 SYSDATE);
   COMMIT;

   MERGE INTO settlement s
      USING (SELECT /*+ parallel(g,16) */
                    DISTINCT settlement_id
                        FROM t_exp_claims_gl_temp) s1
      ON (s.settlement_id = s1.settlement_id)
      WHEN MATCHED THEN
         UPDATE
            SET s.gl_exp_claim_detail = 1, last_update_date = SYSDATE
         ;
   COMMIT;


   -- Post Dispute Claim Details back to Promotion Data ---
/*
   MERGE INTO promotion_data pd
      USING (SELECT   sales_date, item_id, location_id, promotion_id,
                      MAX (settlement_status_id) claim_step,
                      SUM (dispute_dollar) dispute_dollar
                 FROM (SELECT /*+ parallel(pd,24) parallel(s,24) parallel(m,24) *
                              pd.sales_date, pd.item_id, pd.location_id,
                              settlement_status_id, pd.promotion_id,
                              DECODE
                                 (SUM (  NVL (pd.rr_accrual_cd_pd, 0)
                                      ) OVER (PARTITION BY s.settlement_id),
                                  0, NVL (settlement_amount, 0)
                                   / COUNT (1) OVER (PARTITION BY s.settlement_id),
                                    NVL (settlement_amount, 0)
                                  * (  (  NVL (pd.rr_accrual_cd_pd, 0)
                                       )
                                     / SUM (  NVL (pd.rr_accrual_cd_pd, 0)
                                           ) OVER (PARTITION BY s.settlement_id)
                                    )
                                 ) dispute_dollar
                         FROM settlement s, mdp_matrix m, promotion_data pd
                        WHERE 1 = 1
                          AND s.settlement_status_id IN (3, 6, 11)
                          AND s.gl_exp_claim_detail <> 1          --18/05/2011
                          --AND s.promotion_id IS NOT NULL             -- LP 18/05/2011
                          AND NVL (s.promotion_id, 0) <> 0
                          AND pd.is_self = 1
                          AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
                          AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
                          AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_l_att_6_ep_id, s.corporate_customer)
                          AND m.t_ep_e1_cust_cat_4_ep_id = DECODE (NVL (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
                          AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
                          AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
                          AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
                          AND pd.promotion_id = s.promotion_id
                          AND pd.scenario_id IN (22, 262, 162)
                          AND pd.item_id = m.item_id
                          AND pd.location_id = m.location_id
                          AND pd.sales_date BETWEEN s.start_date AND s.end_date                                                                            
                      )
             GROUP BY sales_date, item_id, location_id, promotion_id) pd1
      ON (    pd.sales_date = pd1.sales_date
          AND pd.item_id = pd1.item_id
          AND pd.location_id = pd1.location_id
          AND pd.promotion_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.dispute_dollar_detail = NVL (pd1.dispute_dollar, 0),
                pd.claim_step = NVL (pd1.claim_step, 0),
                last_update_date = SYSDATE
         ;
   COMMIT;
*/

----Clear out Unposted Amounts

--   UPDATE promotion pd
 --     SET pd.promo_non_export_claim = null,
  --        pd.last_update_date = sysdate
  --  WHERE pd.promo_non_export_claim is not null;
          
  -- COMMIT;

  UPDATE accrual ad   ---ACCRUAL TABLE
      SET ad.am_non_export_claim = null,
          ad.last_update_date = sysdate
    WHERE ad.am_non_export_claim is not null;

  COMMIT;
  
  UPDATE promotion ad   ---ACCRUAL TABLE
      SET ad.am_non_export_claim = null,
          ad.last_update_date = sysdate
    WHERE ad.am_non_export_claim is not null;

  COMMIT;

   INSERT INTO js_non_export_claims ---Modify so it generates at promotion level
      SELECT /*+ parallel(pd,24) parallel(s,24) */
              pd.promotion_id, sum ( NVL (s.claim_case_deal_d, 0) + nvl(s.co_op_$,0) + nvl(s.claim_case_deal_hf_d,0)+ nvl(s.claim_coop_hf,0)) claim_amount
        FROM settlement s, promotion pd
       WHERE 1 = 1
         AND s.gl_exp_claim <> 1
         AND s.gl_exp_claim_detail = 0
         AND NVL (s.promotion_id, 0) <> 0
         AND pd.promotion_id = s.promotion_id
         AND pd.promotion_stat_id IN (4, 5, 6, 7)
         AND pd.scenario_id IN (22, 262, 162)
         GROUP BY pd.promotion_id;

   COMMIT;
--   
--   MERGE                         /*+ parallel(pd,24)*/  INTO promotion_data pd
--      USING (SELECT   /*+ parallel(c,24) */
--                      c.sales_date, c.item_id, c.location_id, c.promotion_id,
--                      SUM (claim_amount) claim_amount
--                 FROM js_non_export_claims c, promotion p
--                WHERE c.promotion_id = p.promotion_id
--             GROUP BY c.sales_date, c.item_id, c.location_id, c.promotion_id) pd1
--      ON (    pd.promotion_id = pd1.promotion_id
--          AND pd.item_id = pd1.item_id
--          AND pd.location_id = pd1.location_id
--          AND pd.sales_date = pd1.sales_date)
--      WHEN MATCHED THEN
--         UPDATE
--            SET pd.promo_non_export_claim = claim_amount,
--                pd.last_update_date = sysdate
--         ;
--   COMMIT;
---- Syncronises Dispute Dollar from PD to AD--
   MERGE                         /*+ parallel(pd,12)*/  INTO accrual pd  --Uncomment this but modify to the accrual level
      USING (SELECT   /*+ parallel(c,24) */  c.promotion_id,
                      SUM (claim_amount) claim_amount
                 FROM js_non_export_claims c, accrual a
                WHERE c.promotion_id = a.accrual_id
             GROUP BY c.promotion_id
   ) sd1
     ON (sd1.promotion_id = pd.accrual_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.am_non_export_claim = claim_amount,
                last_update_date = SYSDATE
      WHEN NOT MATCHED THEN  ---LEAVE this commented
   INSERT (pd.accrual_id,
           pd.am_non_export_claim, last_update_date)
         VALUES (sd1.promotion_id,
                 sd1.claim_amount, sysdate);
   COMMIT;    
   
   MERGE                         /*+ parallel(pd,12)*/  INTO promotion pd  --Uncomment this but modify to the accrual level
      USING (SELECT   /*+ parallel(c,24) */  c.promotion_id,
                      SUM (claim_amount) claim_amount
                 FROM js_non_export_claims c, promotion a
                WHERE c.promotion_id = a.promotion_id
             GROUP BY c.promotion_id
   ) sd1
     ON (sd1.promotion_id = pd.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.am_non_export_claim = claim_amount,
                last_update_date = SYSDATE ;
   COMMIT;   


   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

--    EXCEPTION
--    WHEN OTHERS THEN
--        BEGIN
--        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
--                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
--                            dbms_utility.format_error_stack,
--                            dbms_utility.format_error_backtrace    
--                          );
--        RAISE;
--        END;    
   END;

   PROCEDURE prc_default_claims 
   AS
   
    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;        

   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_DEFAULT_CLAIMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
        
    pkg_common_utils.prc_update_accountmgr;

  
     --LP 1.6 - NEW Feature
     --Update the Default Claim flag for users in User Groups
     -----Claims Entry - Food Service
     -----Claims Entry - Retail
     UPDATE settlement_owner
        SET create_default_claims = 0
      WHERE create_default_claims <> 0;
  
     COMMIT;
     
 
      MERGE INTO settlement_owner s
        USING (SELECT DISTINCT user_id
                 FROM user_groups
                WHERE group_id IN (247,580)    -- Australia Claims groups 
                                    ) g
        ON (s.settlement_owner_code = g.user_id)
        WHEN MATCHED THEN
           UPDATE
              SET create_default_claims = 3  -- new value to differentiate claims
           ;
     COMMIT;
     
     MERGE INTO settlement_owner s
        USING (SELECT DISTINCT user_id
                 FROM user_groups
                WHERE group_id IN (248)    -- New Zealand Claims groups 
                                    ) g
        ON (s.settlement_owner_code = g.user_id)
        WHEN MATCHED THEN
           UPDATE
              SET create_default_claims = 4  -- new value to differentiate claims
           ;
     COMMIT;
     
     --Set FoodService Customers claim entry to 0. Do not create defalt claims
     /* UPDATE t_ep_ebs_customer
        SET claims_entry = 0
      WHERE t_ep_ebs_customer_ep_id IN ();
  
     COMMIT;*/
     
     --ReCreate Settlement Defaults Table in case any new series created on Settlement--
     check_and_drop ('settlement_defaults');
     dynamic_ddl
        ('CREATE TABLE settlement_defaults AS SELECT * FROM settlement WHERE settlement_id = 0'
        );
  
     -- AU Users --
     INSERT      /*+ NOLOGGING */INTO settlement_defaults sd
                 (settlement_id, settlement_code, settlement_desc,
                  settlement_number, settlement_owner_id, settlement_amount,
                  gl_code_id, settlement_status_id, settlement_type_id,
                  invoice_id, promotion_id, promoted_product, related_ws,
                  t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_10_ep_id, t_ep_l_att_2_ep_id, t_ep_ebs_customer_ep_id, claim_parent_id, rr_default_claim,T_EP_L_ATT_6_EP_ID)
        SELECT
               settlement_seq.NEXTVAL,
               settlement_seq.CURRVAL || '-' || 'Default ' || ebs_customer_desc || ' Claim',
               'Default ' || ebs_customer_desc || ' Claim', settlement_seq.CURRVAL,
               389 settlement_owner_id, 0 settlement_amount,  gl_code_id, 1 settlement_status_id, settlement_type_id, 
               1, 0, 0, 15359, t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_10_ep_id, t_ep_l_att_2_ep_id,  t_ep_ebs_customer_ep_id, 0, 1,0
       FROM (SELECT  /*+ full(l) parallel(l,8) */ DISTINCT ebsc.ebs_customer_desc, glc.gl_code_id,  
       l.t_ep_e1_cust_cat_2_ep_id, l.t_ep_l_att_10_ep_id ,
       0 t_ep_l_att_2_ep_id, l.t_ep_ebs_customer_ep_id, st.settlement_type_id
        FROM t_ep_e1_cust_cat_2 l2,
        t_ep_l_att_10 l10,
        location l,
        t_ep_ebs_customer ebsc,
        gl_code glc,
        settlement_type st
        WHERE ebsc.claims_entry = 1
        AND l2.t_ep_e1_cust_cat_2_ep_id = l.t_ep_e1_cust_cat_2_ep_id
        AND l10.t_ep_l_att_10_ep_id = l.t_ep_l_att_10_ep_id
        AND ebsc.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
        AND glc.gl_code_id = 0  --IN (0, 1)
        AND l.location_id <> 0
        AND l.t_ep_e1_cust_cat_2_ep_id <> 0
        AND l.t_ep_l_att_10_ep_id <> 0
        And St.Settlement_Type_Id = 0 );
  
     COMMIT;
  
     -- Clean Any Default Rows from Settlement Table First --
     DELETE      /*+ parallel(s,8) nologging */FROM settlement s
           WHERE rr_default_claim = 1
             --AND settlement_desc LIKE '%Default%'
             AND settlement_id > 0
             AND gl_exp_claim <> 1
             AND gl_exp_claim_detail = 0
             AND settlement_amount = 0
             AND s.settlement_number > 0                         -- LP 11/5/2011
             AND settlement_status_id NOT IN (4, 7);
  
     COMMIT;
  
  -- Create a new Default Claim Row for every PTP User listed in USER_ID FOR RETAIL ONLY--
     FOR rec IN (SELECT DISTINCT settlement_owner_id, settlement_owner_desc
                            From Settlement_Owner
                           WHERE create_default_claims = 3)
     LOOP
        FOR rec1 IN (SELECT /*+ parallel(s,8) */
                          distinct  settlement_id, settlement_code, settlement_desc,
                            settlement_number, settlement_owner_id, settlement_amount,
                            gl_code_id, settlement_status_id, settlement_type_id,
                            invoice_id, promotion_id, promoted_product, related_ws,
                            t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_2_ep_id, t_ep_l_att_10_ep_id, t_ep_ebs_customer_ep_id, claim_parent_id, rr_default_claim
                       FROM settlement_defaults s
                      WHERE settlement_id > 0)
        LOOP
           INSERT      /*+ nologging */INTO settlement
                       (settlement_id, settlement_code,
                        settlement_desc, settlement_number,
                        settlement_owner_id, settlement_amount,
                        gl_code_id, settlement_status_id,
                        settlement_type_id, invoice_id,
                        promotion_id, promoted_product,
                        related_ws, t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_2_ep_id,
                        t_ep_l_att_10_ep_id, t_ep_ebs_customer_ep_id, claim_parent_id, 
                        rr_default_claim, last_update_date,
                        t_ep_e1_cust_cat_4_EP_ID, t_ep_ebs_zone_EP_ID,T_EP_L_ATT_6_EP_ID        				--- Sanjiiv Banner Group Change     -- Removed the column t_ep_ebs_account_EP_ID        
                       )
                VALUES (settlement_seq.NEXTVAL, settlement_seq.NEXTVAL,
                        rec1.settlement_desc, rec1.settlement_number,
                        rec.settlement_owner_id, rec1.settlement_amount,
                        rec1.gl_code_id, rec1.settlement_status_id,
                        rec1.settlement_type_id, 0,
                        NVL(rec1.promotion_id,0), rec1.promoted_product,--Modified as per INC0011212
                        rec1.related_ws, rec1.t_ep_e1_cust_cat_2_ep_id, rec1.t_ep_l_att_2_ep_id,
                        rec1.t_ep_l_att_10_ep_id, rec1.t_ep_ebs_customer_ep_id, rec1.claim_parent_id, 
                        rec1.rr_default_claim,  
                        SYSDATE,
                        0,0,0
                       );
        END LOOP;
     END LOOP;
  
     COMMIT;  
     
   
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;

   PROCEDURE prc_update_days_out 
   AS
   
    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;        

   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_DAYS_OUT';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
        
   for rec in (select distinct s.settlement_id,
                               ABS (ROUND (s.inv_date - SYSDATE, 0))
                                                                      days_o
                          FROM settlement s
                         WHERE s.settlement_status_id IN (2, 3, 6, 9, 10, 11, 12))
   LOOP
      IF rec.days_o BETWEEN 1 AND 10
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 1,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      ELSIF rec.days_o BETWEEN 11 AND 20
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 2,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      ELSIF rec.days_o BETWEEN 21 AND 30
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 3,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      ELSIF rec.days_o BETWEEN 31 AND 60
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 4,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      ELSIF rec.days_o BETWEEN 61 AND 90
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 5,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      ELSIF rec.days_o > 90
      THEN
         UPDATE settlement s
            SET s.days_outstanding_id = 6,
                last_update_date = SYSDATE
          WHERE s.settlement_id = rec.settlement_id;

         COMMIT;
      END IF;
   END LOOP;
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;
   
   PROCEDURE prc_check_duplicate_claim 
   AS
   
    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;        

   BEGIN 
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_CHECK_DUPLICATE_CLAIM';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl('TRUNCATE TABLE tbl_duplicate_claims');

    COMMIT;

    INSERT /*+ APPEND NOLOGGING */ INTO tbl_duplicate_claims  
    (
        SELECT MIN(settlement_id) settlement_id ,
        NVL(s.settlement_desc, '0') settlement_desc,
        NVL(s.key_account_state, 0) key_account_state,
        NVL(s.key_account, 0) key_account,
        nvl(s.corporate_customer, 0) corporate_customer,
        nvl(s.customer_name, 0) customer_name,
        NVL(s.brand, 0) brand,
        NVL(s.promotion_group, 0) promotion_group,
        NVL(s.promoted_product, 0) promoted_product,
        0,--NVL(s.promotion_id, 0) customer_promo_no,                               -- LP 02/02/2011 - Changed from customer_promo_no to promotion_id
        NVL(s.case_deal, 0) case_deal,
        NVL(s.unit_deal, 0) unit_deal,
        NVL(s.case_hf_deal, 0) case_hf_deal,
        NVL(s.unit_hf_deal, 0) unit_hf_deal,
        ROUND(NVL(s.claim_case_deal_d, 0), 2) claim_case_deal_d, 
        ROUND(NVL(s.claim_case_deal_hf_d, 0), 2) claim_case_deal_hf_d,
        ROUND(NVL(s.claim_coop_hf, 0), 2) claim_coop_hf,
        ROUND(NVL(s.co_op_$, 0), 2) co_op_$,
        nvl(s.start_date, to_date('01-01-1900', 'DD-MM-YYYY')) start_date, 
        nvl(s.end_date, to_date('01-01-2100', 'DD-MM-YYYY')) end_date,
        NVL(s.sub_banner, 0) sub_banner 
        FROM settlement s
        WHERE s.rr_default_claim =0
       -- AND s.settlement_status_id in (1,8) 
        AND s.date_posted BETWEEN (SYSDATE - 365) AND SYSDATE
        GROUP BY NVL(s.settlement_desc, '0'),
        NVL(s.key_account_state, 0),
        NVL(s.key_account, 0),
        nvl(s.corporate_customer, 0),
        nvl(s.customer_name, 0),
        NVL(s.sub_banner, 0),
        NVL(s.brand, 0),
        NVL(s.promotion_group, 0),
        NVL(s.promoted_product, 0),
        --NVL(s.promotion_id, 0),                               -- LP 02/02/2011 - Changed from customer_promo_no to promotion_id
        NVL(s.case_deal, 0),
        NVL(s.unit_deal, 0),
        NVL(s.case_hf_deal, 0),
        NVL(s.unit_hf_deal, 0),
        ROUND(NVL(s.claim_case_deal_d, 0), 2), 
        ROUND(NVL(s.claim_case_deal_hf_d, 0), 2),
        ROUND(NVL(s.claim_coop_hf, 0), 2),
        ROUND(NVL(s.co_op_$, 0), 2),
        NVL(s.start_date, to_date('01-01-1900', 'DD-MM-YYYY')), 
        NVL(s.end_date, to_date('01-01-2100', 'DD-MM-YYYY'))
        HAVING COUNT(1) > 1
    );

    COMMIT;

    MERGE INTO settlement s
    USING ( 
        SELECT /*+ parallel(s,8)  parallel(s1,8) */ s.settlement_id, s1.settlement_id duplicate_to
        FROM settlement s, tbl_duplicate_claims s1 
        WHERE 1=1
        AND s.settlement_status_id NOT IN (4, 7) 
        AND s.settlement_id <> s1.settlement_id
        AND s.rr_default_claim = 0
        AND s.date_posted BETWEEN (SYSDATE - 365) AND SYSDATE
        AND NVL(s.settlement_desc, '0') = s1.settlement_desc
        AND NVL(s.key_account_state, 0) = s1.key_account_state
        AND NVL(s.key_account, 0) = s1.key_account
        AND NVL(s.corporate_customer, 0) = s1.corporate_customer
        and nvl(s.customer_name, 0) = s1.customer_name
        AND NVL(s.sub_banner, 0) = s1.sub_banner
        AND NVL(s.brand, 0) = s1.brand
        AND NVL(s.promotion_group, 0) = s1.promotion_group
        AND NVL(s.promoted_product, 0) = s1.promoted_product
   --     AND NVL(s.promotion_id, 0) = s1.customer_promo_no               --Promo ID can be different
        AND NVL(s.case_deal, 0) = s1.case_deal
        AND NVL(s.unit_deal, 0) = s1.unit_deal
        AND NVL(s.case_hf_deal, 0) = s1.case_hf_deal
        AND NVL(s.unit_hf_deal, 0) = s1.unit_hf_deal
        AND ROUND(NVL(s.claim_case_deal_d, 0), 2) = s1.claim_case_deal_d
        AND ROUND(NVL(s.claim_case_deal_hf_d, 0), 2) = s1.claim_case_deal_hf_d
        AND ROUND(NVL(s.claim_coop_hf, 0), 2) = s1.claim_coop_hf
        AND ROUND(NVL(s.co_op_$, 0), 2) = s1.co_op_$
        AND NVL(s.start_date, to_date('01-01-1900', 'DD-MM-YYYY')) = s1.start_date 
        AND NVL(s.end_date, to_date('01-01-2100', 'DD-MM-YYYY')) = s1.end_date
        ) s1
    ON(s.settlement_id = s1.settlement_id)
    WHEN MATCHED THEN
        UPDATE SET s.settlement_status_id = 5, s.duplicate_to = s1.duplicate_to;    
 
    COMMIT;        
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;    
   END;
   
    PROCEDURE prc_create_claim_combos
AS
   v_prog_name   VARCHAR2 (100);
   v_status           VARCHAR2 (100);
   v_proc_log_id   NUMBER;

   mindate            DATE;
   maxdate           DATE;
   v_from_date     DATE;
   v_until_date      DATE;
   sql_str              VARCHAR2 (30000);


   v_batch_dt        VARCHAR2 (20);
BEGIN
   pre_logon;
   v_status := 'start ';
   v_prog_name := 'prc_create_claim_combos';
   v_proc_log_id :=
      rr_pkg_proc_log.
      fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT TO_DATE (SUBSTR (pval, 1, 10), 'mm-dd-yyyy')
     INTO v_from_date
     FROM sys_params
    WHERE pname LIKE 'min_sales_date';

   SELECT TO_DATE (SUBSTR (pval, 1, 10), 'mm-dd-yyyy')
     INTO v_until_date
     FROM sys_params
    WHERE pname LIKE 'max_fore_sales_date';


   check_and_drop ('create_claim_combs');

   dynamic_ddl (
      'CREATE TABLE create_claim_combs as 
   (select  CUSTOMER_NAME customer_lvl, PROMOTED_PRODUCT product_lvl ,'
      || '''product'''
      || ' product_lvl_n from settlement
        where nvl(GL_EXP_CLAIM,0) <>1
          and nvl(CUSTOMER_NAME,0) <>0
          and nvl(promoted_product,0)<>0
          and t_ep_L_ATT_10_EP_ID = 12 
          UNION 
    select  CUSTOMER_NAME customer_lvl, promotion_group product_lvl ,'
      || '''promoted_group'''
      || 'product_lvl_n from settlement
        where nvl(GL_EXP_CLAIM,0) <>1
          and nvl(CUSTOMER_NAME,0) <>0
          and nvl(promotion_group,0)<>0
          and t_ep_L_ATT_10_EP_ID = 12 )
    ');
   check_and_drop ('t_claim_combs');

   dynamic_ddl (
      'CREATE TABLE t_claim_combs(item_id number, location_id number   )');



   FOR rec IN (SELECT DISTINCT customer_lvl
                 FROM create_claim_combs
                WHERE product_lvl_n = 'product')
   LOOP
      sql_str :=
         ' MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO t_claim_combs m
                                      USING( select i.item_id, l.location_id
                            FROM items i,location l                           
                            WHERE 1 = 1 and  t_ep_e1_cust_cat_4_ep_id = ';
      sql_str :=
         sql_str || rec.customer_lvl || ' and  t_ep_i_att_1_ep_id  in (-1 ';


      FOR rec1
         IN (SELECT product_lvl
               FROM create_claim_combs
              WHERE customer_lvl = rec.customer_lvl
                    AND product_lvl_n = 'product')
      LOOP
         sql_str := sql_str || ' ,' || rec1.product_lvl;
      END LOOP;

      sql_str :=
         sql_str
         || ' )AND NOT EXISTS (SELECT 1 FROM mdp_matrix m2 WHERE m2.item_id = i.item_id AND m2.location_id = l.location_id
                          ))m1
                      ON(m.item_id = m1.item_id
                         AND m.location_id = m1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT( item_id, location_id)
                      VALUES( m1.item_id, m1.location_id)';
     -- v_proc_log_id :=      rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, sql_str || SYSDATE);
      dynamic_ddl (sql_str);
   END LOOP;



   FOR rec IN (SELECT DISTINCT customer_lvl, product_lvl
                 FROM create_claim_combs
                WHERE product_lvl_n = 'promoted_group')
   LOOP
      sql_str :=
         ' MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO t_claim_combs m
                                      USING( select i.item_id, l.location_id
                            FROM items i,location l                           
                            WHERE 1 = 1 and  t_ep_e1_cust_cat_4_ep_id = '
         || rec.customer_lvl;
      sql_str := sql_str || ' and  t_ep_p2b_ep_id  in (-1 ';

      FOR rec1
         IN (SELECT product_lvl
               FROM create_claim_combs
              WHERE customer_lvl = rec.customer_lvl
                    AND product_lvl_n = 'promoted_group')
      LOOP
         sql_str := sql_str || ' ,' || rec1.product_lvl;
      END LOOP;

      sql_str :=
         sql_str
         || ' )AND NOT EXISTS (SELECT 1 FROM mdp_matrix m2 WHERE m2.item_id = i.item_id AND m2.location_id = l.location_id
                          ))m1
                      ON(m.item_id = m1.item_id
                         AND m.location_id = m1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT( item_id, location_id)
                      VALUES( m1.item_id, m1.location_id)';

     -- v_proc_log_id :=
      --   rr_pkg_proc_log.
       --  fcn_dbex (v_prog_name, v_package_name, sql_str || SYSDATE);
      dynamic_ddl (sql_str);
   END LOOP;



   dynamic_ddl ('Truncate TABLE mdp_load_assist');
   dynamic_ddl (
      'INSERT INTO mdp_load_assist(item_id, location_id)
                SELECT DISTINCT item_id, location_id FROM t_claim_combs');

   COMMIT;


   pkg_common_utils.
   prc_mdp_add ('mdp_load_assist', v_from_date, v_until_date);



   v_status := 'end ';
   v_proc_log_id :=
      rr_pkg_proc_log.
      fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         v_proc_log_id :=
            rr_pkg_proc_log.fcn_dbex (v_prog_name,
                                      v_package_name,
                                      'Fatal Error in Step: ' || v_status,
                                      TO_CHAR (SQLCODE),
                                      TO_CHAR (SQLCODE),
                                      DBMS_UTILITY.format_error_stack,
                                      DBMS_UTILITY.format_error_backtrace);
         RAISE;
      END;
END; 
 

END;

/
