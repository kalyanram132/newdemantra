--------------------------------------------------------
--  DDL for Package Body PKG_APO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_APO" 
AS
/******************************************************************************
   NAME:       PKG_APO
   PURPOSE:    All procedures commanly used for APO
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/02/2016  Lakshmi Annapragada/ UXC Redrock Consulting - Initial version

   ******************************************************************************/

PROCEDURE   PRC_SYNC_FULL_INCREMENTALS   (p_start_date  DATE,
                              p_end_date    DATE)
IS
/**************************************************************************
   NAME:       SYNC_FULL_INCREMENTALS
   PURPOSE:    Syncronises FULL Incremental Volume to be offset, deals to be offset,
               REMINDER: Exfactory Fcsted Combinations will have no offset/allocation

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  -------------------------------------------------
  1.0         01/JAN/2016   Lakshmi Annapragada

**************************************************************************/
   v_min            DATE;
   v_max            DATE;
   count_months     NUMBER;
   counter          NUMBER           := 1;
   max_sales_date   DATE;
   v_m_date         VARCHAR2(20);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2 (100);
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   v_prog_name      VARCHAR2 (100)  ;
   v_status           VARCHAR2 (100);
   v_proc_log_id  NUMBER;

     v_eng_profile    NUMBER := 1;
    p_gen_sync_rule  VARCHAR2(4000);
   p_sales_sync_rule  VARCHAR2(4000);
   p_alloc_sync_rule  VARCHAR2(4000);
   v_get_indies     NUMBER (10);
BEGIN

    v_status := 'Start ';
    v_prog_name := 'RR_PRC_SYNC_FULL_INCREMENTALS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name,  v_status || SYSDATE);


   pre_logon;

-- Select PE Forecast Column --
      SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

     SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';

   get_param ('init_params_0', 'lead', count_months);
--   get_param ('sys_params', 'max_sales_date', max_sales_date);
  SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') + 7
    INTO max_sales_date
    FROM DUAL;
     SELECT get_max_date
    INTO v_m_date
    FROM DUAL;
   count_months := (safe_division (count_months, 52, 1) * 12);

   -- cmos 23-Jan-2017 (px update)
   -- cmos 25-Feb-2017 (use px index instead of fts)
   -- cmos 28-Feb-2017 (fts is faster than index)
   -- cmos 01-Mar-2017 (remove pdml as table is not partitioned)
   UPDATE /*+ full(sd) parallel(sd,32) */ sales_data sd
      SET
          sd.final_pos_fcst = NULL,
          sd.last_update_date = SYSDATE
    WHERE sales_date >= v_min
      AND sales_date <= v_max
      AND (  sd.final_pos_fcst IS NOT NULL
           );

   COMMIT;

 pkg_accrual.prc_set_mdp_allocation(p_start_date ,p_end_date )      ;

 v_status := 'Calculation ';
  check_and_drop('rr_promos_incr_calc_t');

   -- cmos 23-Jan-2017 (px ctas)
   sql_str := 'CREATE TABLE rr_promos_incr_calc_t parallel 32 nologging AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id ,
               CASE WHEN TO_CHAR(pdt.from_date, ''DAY'') = ''' || v_day || ''' AND MOD((TRUNC(pdt.until_date, ''DD'') - TRUNC(pdt.from_date, ''DD'')) + 1, 7) = 0 THEN 1
                    ELSE 1
               END rr_start_day,
               CASE WHEN ps.rr_sync_sd = 1 AND pt.rr_sync_sd = 1 THEN 1 ELSE 0 END rr_sync_sd,
               CASE WHEN ps.rr_sync_ad = 1 AND pt.rr_sync_ad = 1 THEN 1 ELSE 0 END rr_sync_ad,
               CASE WHEN ps.rr_sync_vol_sd = 1 AND pt.rr_sync_vol_sd = 1 THEN 1 ELSE 0 END rr_sync_vol_sd,
               CASE WHEN ps.rr_sync_vol_ad = 1 AND pt.rr_sync_vol_ad = 1 THEN 1 ELSE 0 END rr_sync_vol_ad,
               p.promotion_type_id,
               p.promotion_stat_id,
               p.cd_deal_type,
               p.coop_deal_type,
               p.oi_deal_type
               FROM promotion p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id IN (22, 262, 162)
               AND pt.rr_margin_support = 0
               AND ( 1=2
                  OR (ps.rr_sync_sd = 1 AND pt.rr_sync_sd = 1)
                  OR (ps.rr_sync_ad = 1 AND pt.rr_sync_ad = 1))
--               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
--               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

    dynamic_ddl(sql_str);
    dynamic_ddl('alter table rr_promos_incr_calc_t noparallel');		-- cmos 23-Jan-2017
    COMMIT;


  sql_str := 'MERGE /*+ FULL(pd) PARALLEL(pd, 16) */ INTO promotion_data pd
               USING(
 SELECT /*+ FULL(pd) PARALLEL(pd, 32) */ pd.promotion_id,
               pd.item_id,
               pd.location_id,
               pd.sales_date,
               pd.promotion_stat_id,
               CASE
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0)
               END rr_supply_vol_base_pd,
               CASE
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN (TO_DATE(''' || v_m_date || ''' , ''MM-DD-YYYY HH24:MI:SS'') >= CASE WHEN m.dem_stream = 0 THEN pd.sales_date ELSE TO_DATE(''01/01/2100'', ''DD/MM/YYYY'') END
                         OR NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 ) AND NVL(latt6.rr_accrual_adjust, 0) = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.incr_evt_vol_act, 0)
                    ELSE CASE WHEN m.dem_stream = 0 THEN 0 ELSE NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0)))) END
               END rr_supply_vol_incr_pd
               FROM promotion p,
               promotion_data pd,
               promotion_matrix pm,
               mdp_matrix m,
               rr_promos_incr_calc_t rpt,
               promotion_stat ps, promotion_type pt,
               t_ep_ebs_customer latt6
               WHERE 1 = 1
               AND pd.promotion_type_id = pt.promotion_type_id
               AND pd.promotion_stat_id = ps.promotion_stat_id
               AND p.promotion_id = rpt.promotion_id
               AND p.promotion_id = pm.promotion_id
               AND pd.promotion_id = pm.promotion_id
               AND pd.item_id = pm.item_id
               AND pd.location_id = pm.location_id
               AND pd.is_self = 1 -- lannapra Added 15 May 2015
               AND pm.item_id = m.item_id
               AND pm.location_id = m.location_id
               AND latt6.t_ep_ebs_customer_ep_id = m.t_ep_ebs_customer_ep_id
               AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'')) pd1
               ON(pd.promotion_id = pd1.promotion_id
               AND pd.item_id = pd1.item_id
               AND pd.location_id = pd1.location_id
               AND pd.sales_date = pd1.sales_date)
               WHEN MATCHED THEN UPDATE SET
                 pd.rr_supply_vol_incr_pd = NVL(pd1.rr_supply_vol_incr_pd, 0),
                   pd.promotion_stat_id = pd1.promotion_stat_id,
               pd.last_update_date = SYSDATE
               WHERE 1 = 2
               OR NVL(pd.rr_supply_vol_incr_pd, 0) <> NVL(pd1.rr_supply_vol_incr_pd, 0)';

  --  RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,sql_str);
    dynamic_ddl(sql_str);
    COMMIT;

  v_status := 'rr_sync_promo_incr_t';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE);

    check_and_drop('rr_sync_promo_incr_t');
    -- cmos 23-Jan-2017 (px ctas)
    sql_str :=('CREATE TABLE rr_sync_promo_incr_t(promotion_id, item_id, location_id, sales_date, rr_sync_sd, sup_incr_plan,
                                            alloc_wk0, alloc_wk1, alloc_wk2, alloc_wk3, alloc_wk4, alloc_wk5,alloc_wk6,
                                            sync_wk0, sync_wk1, sync_wk2, sync_wk3, sync_wk4, sync_wk5, sync_wk6, rr_promo_offset)
                                            TABLESPACE ts_temp_tables parallel 32 nologging
         AS SELECT /*+ full(pd) parallel(pd,64) */
              pd.promotion_id,
              pd.item_id,
              pd.location_id,
              pd.sales_date,
              rpt.rr_sync_sd,
              pd.rr_supply_vol_incr_pd sup_incr_plan,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_alloc_wk0 END alloc_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk1 END alloc_wk1,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk2 END alloc_wk2,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk3 END alloc_wk3,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk4 END alloc_wk4,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk5 END alloc_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk6 END alloc_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_sync_wk0 END sync_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk1 END sync_wk1,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk2 END sync_wk2,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk3 END sync_wk3,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk4 END sync_wk4,
						  CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk5 END sync_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk6 END sync_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_promo_offset END rr_promo_offset
            FROM  promotion_data pd,
                  promotion p,
                  promotion_matrix pm,
                  rr_promos_incr_calc_t rpt
            WHERE 1 = 1
            AND 	pm.promotion_id 		= p.promotion_id
            AND 	pd.promotion_id 		= pm.promotion_id
            AND   pd.item_id 				= pm.item_id
            AND   pd.location_id 		= pm.location_id
            AND   pd.is_self = 1 --Added lannapra 15 May 2015
            AND   pd.sales_date 			>= TO_DATE('''|| TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   pd.sales_date 			<= TO_DATE('''|| TO_CHAR(v_max, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   p.promotion_id    = rpt.promotion_id
            '
				);
       ---
    dynamic_ddl (sql_str);
    dynamic_ddl('alter table rr_sync_promo_incr_t noparallel');		-- cmos 23-Jan-2017
    COMMIT;

        v_status := 'rr_sync_promo_offset_incr_t';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE);

    check_and_drop('rr_sync_promo_offset_incr_idx');

    dynamic_ddl('TRUNCATE TABLE rr_sync_promo_offset_incr_t REUSE STORAGE');

    p_gen_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN 1 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN 0 THEN 1 ELSE 0 END * (i.num_of_days/7) END ';
    p_alloc_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN rspt.alloc_wk0 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN -42 THEN rspt.alloc_wk6 WHEN -35 THEN rspt.alloc_wk5 WHEN -28 THEN rspt.alloc_wk4 WHEN -21 THEN rspt.alloc_wk3 WHEN -14 THEN rspt.alloc_wk2 WHEN -7 THEN rspt.alloc_wk1 WHEN 0 THEN rspt.alloc_wk0 END * (i.num_of_days/7) END ';
    p_sales_sync_rule := ' CASE WHEN i.datet = rspt.sales_date THEN rspt.sync_wk0 WHEN (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) = (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) THEN 0 ELSE CASE (NEXT_DAY(i.datet, ''' || v_day || ''' ) -7) - (NEXT_DAY(rspt.sales_date, ''' || v_day || ''' ) -7) WHEN -42 THEN rspt.sync_wk6 WHEN -35 THEN rspt.sync_wk5 WHEN -28 THEN rspt.sync_wk4 WHEN -21 THEN rspt.sync_wk3 WHEN -14 THEN rspt.sync_wk2 WHEN -7 THEN rspt.sync_wk1 WHEN 0 THEN rspt.sync_wk0 END * (i.num_of_days/7) END ';


    FOR i IN 0 ..6
    LOOP

    sql_str :=('INSERT /*+ APPEND NOLOGGING */ INTO rr_sync_promo_offset_incr_t(promotion_id,
item_id, location_id, sales_date, plan_date, rr_sync_sd, incr_plan_s)
SELECT /* full(rspt) parallel(rspt,32) */ rspt.promotion_id,
rspt.item_id, rspt.location_id, rspt.sales_date, i.datet  plan_date, rspt.rr_sync_sd, '
            || p_sales_sync_rule || ' * rspt.sup_incr_plan incr_plan_s
         FROM rr_sync_promo_incr_t rspt,
         inputs i
         WHERE 1= 1
         AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY(rspt.sales_date , ''MONDAY'') - 7 - (' || i || '*7)
         AND ' || i || ' <= rspt.rr_promo_offset');

   -- rr_pkg_proc_log.prc_proc_sql_log(v_proc_log_id, sql_str);
    dynamic_ddl (sql_str);
    COMMIT;
     END LOOP;

    sql_str := 'CREATE INDEX rr_sync_promo_offset_incr_idx ON rr_sync_promo_offset_incr_t(promotion_id, item_id, location_id, plan_date)';
    dynamic_ddl (sql_str);
    COMMIT;

   v_status := 'Sync to SD ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE);



              check_and_drop('rr_sync_promo_offset_g_incr_t');

	      -- cmos 23-Jan-2017 (px ctas)
              sql_str :=('CREATE TABLE rr_sync_promo_offset_g_incr_t
                          TABLESPACE ts_temp_tables parallel 32 nologging
                          AS SELECT /*+ full(sd) parallel(sd,64) */ sd.item_id,
                                         sd.location_id,
                                         sd.plan_date,
                                         SUM(sd.incr_plan_s) incr_vol
                                  FROM rr_sync_promo_offset_incr_t sd,
                                       mdp_matrix      	mdp,
                                       promotion p,
                                       tbl_cd_deal_type cdt,
                                       tbl_coop_deal_type coopt,
                                       tbl_oi_deal_type oit
                                  WHERE sd.item_id       			= mdp.item_id
                                  AND   sd.location_id   			= mdp.location_id
                                  AND   sd.promotion_id       = p.promotion_id
                                  AND   NVL(p.cd_deal_type, 0)        = cdt.cd_deal_type_id
                                  AND   NVL(p.coop_deal_type, 0)      = coopt.coop_deal_type_id
                                  AND   NVL(p.oi_deal_type, 0)        = oit.oi_deal_type_id
                                  AND   sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                  AND   sd.rr_sync_sd         = 1
                                  GROUP BY sd.item_id,sd.location_id,sd.plan_date
                   ');

              dynamic_ddl (sql_str);
	      dynamic_ddl('alter table rr_sync_promo_offset_g_incr_t noparallel');		-- cmos 23-Jan-2017
              COMMIT;

              sql_str := 'CREATE UNIQUE INDEX rr_sync_promo_os_g_incr_idx ON rr_sync_promo_offset_g_incr_t(item_id, location_id, plan_date)';
              dynamic_ddl (sql_str);
              COMMIT;

                    v_status := 'Start5 ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE);


 check_and_drop('RR_P_ACC_SYNC_TEMP');

 -- cmos 23-Jan-2017 (px ctas)
 -- cmos 25-Feb-2017 (use px index instead of fts)
 -- cmos 01-Mar-2017 (use correct index name in hint of temp table)
 sql_str := 'CREATE TABLE RR_P_ACC_SYNC_TEMP parallel 32 nologging AS SELECT /*+ index(sd,sales_data_pk) parallel(32) INDEX(sd1,rr_sync_promo_os_g_incr_idx) */
                        NVL(sd.item_id, sd1.item_id) item_id, NVL(sd.location_id, sd1.location_id) location_id,
                        NVL(sd.sales_date, sd1.plan_date) plan_date,
                          sd1.incr_vol
                        FROM rr_sync_promo_offset_g_incr_t  sd1 FULL OUTER JOIN
                        sales_data sd ON (sd.sales_date = sd1.plan_date
                        AND sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                        AND sd1.plan_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''))
                        WHERE 1 = 1
                        AND
                        ABS(NVL(sd.final_pos_fcst, 0) - NVL(sd1.incr_vol, 0))  <> 0';




            dynamic_ddl (sql_str);
	    dynamic_ddl('alter table RR_P_ACC_SYNC_TEMP noparallel');		-- cmos 23-Jan-2017
            COMMIT;

	    -- cmos 01-Mar-2016 (use px merge with px fts on sd and dop on select)
            sql_str := 'MERGE /*+ enable_parallel_dml full(sd) parallel(sd,32) */ INTO sales_data sd
                        USING(SELECT /*+ parallel(32) */ * FROM RR_P_ACC_SYNC_TEMP) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET
                        final_pos_fcst = sd1.incr_vol,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date,  final_pos_fcst, last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.incr_vol,  SYSDATE)';



            dynamic_ddl (sql_str);
            COMMIT;


   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

PROCEDURE prc_insert_new_apo_combs
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;
    v_count NUMBER;

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_INSERT_NEW_APO_COMBS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



   dynamic_ddl ('truncate table BIIO_APO_FCST');
   dynamic_ddl ('truncate table BIIO_APO_FCST_ERR');

   COMMIT;

   check_and_drop('RR_BIIO_APO_FCST');
dynamic_ddl('create table RR_BIIO_APO_FCST AS
   SELECT SKU_CODE||''-30110101'' sku_code ,CL3_CODE,SALES_OFFICE,SUM(APO_BASELINE_FCST) RR_APO_FCST_OFFSET
   FROM RR_APO_FCST_TMPL /*where nvl(apo_baseline_fcst,0) <> 0 */ group by SKU_CODE ,CL3_CODE,SALES_OFFICE');

  dynamic_ddl('INSERT INTO BIIO_APO_FCST(SDATE,LEVEL1,LEVEL2,LEVEL3,RR_APO_FCST_OFFSET)
   SELECT t1.WEEK_START_DATE,t1.SKU_CODE||''-30110101'' ,t1.CL3_CODE,t1.SALES_OFFICE,SUM(t1.APO_BASELINE_FCST)
   FROM RR_APO_FCST_TMPL t1,RR_BIIO_APO_FCST t2
   where t1.SKU_CODE||''-30110101'' = t2.sku_code
   and t1.cl3_code = t2.cl3_code
   and t1.sales_office = t2.sales_office'||
   case when ( v_weekly_flag = 1) then ' and t2.rr_apo_fcst_offset <> 0 ' end ||'
   group by t1.WEEK_START_DATE,t1.SKU_CODE ,t1.CL3_CODE,t1.SALES_OFFICE');

   COMMIT;

    check_and_drop('rr_apo_npd_combs_t');
-- cmos 23-Jan-2017 (px ctas)
dynamic_ddl ('CREATE TABLE rr_apo_npd_combs_t parallel 32 nologging AS
             SELECT
           /*+ FULL(imp) PARALLEL(imp, 64) */
           DISTINCT i.item_id,
           l.t_ep_ebs_customer_ep_id,
           l.t_ep_l_att_6_ep_id
         FROM RR_BIIO_APO_FCST imp,
           items i,
           location l,
           t_ep_item itm,
           t_ep_ebs_customer cl3,
           t_ep_l_att_6 l6
         WHERE 1                         = 1
         AND imp.sku_code                = itm.item
         AND imp.cl3_Code                = CL3.EBS_CUSTOMER
         AND imp.sales_office            = l6.l_att_6
         AND itm.t_ep_item_ep_id         = i.t_ep_item_ep_id
         AND cl3.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
         AND l6.t_Ep_l_att_6_ep_id       = l.t_ep_l_att_6_ep_id
         AND imp.RR_APO_FCST_OFFSET <> 0
         AND NOT EXISTS
           (SELECT 1
           FROM mdp_matrix m
           WHERE m.item_id   = i.item_id
           AND m.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
           AND m.t_Ep_l_att_6_ep_id = l.t_Ep_l_att_6_ep_id
           )');
dynamic_ddl('alter table rr_apo_npd_combs_t noparallel');		-- cmos 23-Jan-2017
COMMIT;

check_and_drop('rr_tmp_apo_new_loc_t');
dynamic_ddl ('
         CREATE TABLE rr_tmp_apo_new_loc_t AS
         SELECT DISTINCT t_ep_ebs_customer_ep_id,
           t_ep_l_att_6_ep_id,
           location_id
         FROM
           (SELECT t_ep_ebs_customer_ep_id,
             t_ep_l_att_6_ep_id,
             location_id,
             RANK() OVER ( PARTITION BY t_ep_ebs_customer_ep_id,t_ep_l_att_6_ep_id ORDER BY num_rows DESC) row_rank
           FROM
             (SELECT t.t_ep_ebs_customer_ep_id,
               t.t_ep_l_att_6_ep_id,
               sd.location_id,
               COUNT(1) num_rows
             FROM sales_data sd,
               mdp_matrix m,
               rr_apo_npd_combs_t t
             WHERE sd.item_id   = m.item_id
             AND sd.location_id = m.location_id
               --and sd.item_id = t.item_id
             AND m.t_ep_l_att_6_ep_id      = t.t_ep_l_att_6_ep_id
             AND m.t_ep_ebs_customer_ep_id = t.t_ep_ebs_customer_ep_id
             AND sd.sales_date BETWEEN sysdate-52*7 AND sysdate
             GROUP BY t.t_ep_ebs_customer_ep_id,
               t.t_ep_l_att_6_ep_id,
               sd.location_id
             ORDER BY COUNT(1) DESC
             )
           )
         WHERE row_rank = 1 ')
;

   COMMIT;
   dynamic_ddl ('truncate table mdp_load_assist');
          dynamic_ddl
          ( 'INSERT /*+ APPEND NOLOGGING */ INTO mdp_load_assist(item_id, location_id, is_fictive, boolean_qty)
            (SELECT t2.item_id,
                    t1.location_id,
                    0,0
             FROM rr_tmp_apo_new_loc_t t1,
               rr_apo_npd_combs_t t2
             WHERE t1.t_ep_ebs_customer_ep_id = t2.t_ep_ebs_customer_ep_id
               AND t1.t_ep_l_att_6_ep_id        = t2.t_ep_l_att_6_ep_id
            )'
           );

          COMMIT;

          SELECT COUNT (1)
          INTO v_count
          FROM mdp_load_assist;

          --dbms_output.put_line('Count is ' || v_count);
           v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  'Count is '|| v_count || SYSDATE);

          IF v_count > 0
          THEN
            PKG_COMMON_UTILS.PRC_MDP_ADD ('mdp_load_assist');
      --      dynamic_ddl ('truncate table mdp_load_assist');
          END IF;


   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

END PRC_INSERT_NEW_APO_COMBS;

PROCEDURE prc_upd_apo_stg_daily
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;
    V_COUNT number;
    V_DATE date;

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_UPD_APO_STG_DAILY';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

      select min(DATET) into V_DATE
    from INPUTS where TRUNC(NEXT_DAY(sysdate,'MONDAY'),'DD')-7 = DATET; 	-- cmos 28-Feb-2017 (got changed on 28-Feb-2017 by Asahi)


     select count(1) into V_count from RR_APO_FCST_TMPL;

   V_STATUS := 'Start1 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

    dynamic_ddl('TRUNCATE TABLE T_SRC_SAPL_APO_FCST_TMPL_BK');

   dynamic_ddl('insert into T_SRC_SAPL_APO_FCST_TMPL_BK select * from T_SRC_SAPL_APO_FCST_TMPL');
   commit;


   dynamic_ddl('TRUNCATE TABLE RR_APO_FCST_TMPL');

    select count(1) into V_count from RR_APO_FCST_TMPL;

    V_STATUS := 'Step 2 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

   /*ADDING COMBINATIONS WHERE THE FORECAST CHANGED*/
   insert into RR_APO_FCST_TMPL
    SELECT t1.* FROM T_SRC_SAPL_APO_FCST_TMPL_BK T1 , T_SRC_SAPL_APO_FCST_TMPL_BK2 T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 AND T1.WEEK_START_DATE = T2.WEEK_START_DATE
 and t1.apo_baseline_fcst <> t2.apo_baseline_fcst
  and t1.week_start_date between v_date and (v_date + 52*7) ;

 commit;

  select count(1) into V_count from RR_APO_FCST_TMPL;

    V_STATUS := 'Step 3 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

 /*ADDING COMBINATIONS THAT ARE NEW IN CURRENT LOAD*/
 insert into RR_APO_FCST_TMPL
  SELECT * FROM T_SRC_SAPL_APO_FCST_TMPL_BK T1 WHERE not EXISTS(SELECT 1 FROM T_SRC_SAPL_APO_FCST_TMPL_BK2 T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 and t1.week_start_date = t2.week_start_date
 )
  and t1.week_start_date between v_date and (v_date + 52*7) ;

   commit;

    select count(1) into V_count from RR_APO_FCST_TMPL;

    V_STATUS := 'Step 4 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);


   /*CLEARING FORECAST FOR THE COMBINATIONS THAT ARE MISSED IN CURRENT LOAD*/
   insert into RR_APO_FCST_TMPL(WEEK_START_DATE,SKU_CODE,CL3_CODE,SALES_OFFICE,APO_BASELINE_FCST)
  SELECT T1.WEEK_START_DATE,T1.SKU_CODE,T1.CL3_CODE,T1.SALES_OFFICE,0  FROM T_SRC_SAPL_APO_FCST_TMPL_BK2 T1 WHERE not EXISTS(SELECT 1 FROM T_SRC_SAPL_APO_FCST_TMPL_BK T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 and t1.week_start_date = t2.week_start_date
 )
 and t1.week_start_date between v_date and (v_date + 52*7)  ;
      COMMIT;

       select count(1) into V_count from RR_APO_FCST_TMPL;

    V_STATUS := 'Step 5 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

      /** Inserting the errors from Previous load - To account for any master data changes */

       insert into RR_APO_FCST_TMPL(WEEK_START_DATE,SKU_CODE,CL3_CODE,SALES_OFFICE,APO_BASELINE_FCST)
       select distinct t1.* from t_Src_sapl_apo_FcsT_tmpl t1 , biio_apo_fcst_err t2
where t1.sku_code = substr(t2.level1,1,18)
and t1.cl3_code = t2.level2
and t1.sales_office = t2.level3
and t2.sdate between v_date and (v_date + 52*7) ;  --Added 15/04/2016 lannapra
--  select sdate,substr(level1,1,18),level2,level3,rr_apo_fcst_offset
--  from biio_apo_fcst_err ;

  commit;

 select count(1) into V_count from RR_APO_FCST_TMPL;

    V_STATUS := 'Step 6 Num Rows = '||v_count;

   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

   DYNAMIC_DDL('delete from rr_apo_fcst_tmpl where week_start_Date > to_date('''||to_char(v_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'')+ 52*7 ');

   commit;

    select count(1) into V_count from RR_APO_FCST_TMPL;

   v_status := 'end num rows = '||v_count;
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

end PRC_UPD_APO_STG_DAILY;

PROCEDURE prc_upd_apo_stg_weekly
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;
    v_count NUMBER;

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_UPD_APO_STG_WEEKLY';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

 v_status := 'Start1';

   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   DYNAMIC_DDL('TRUNCATE TABLE RR_APO_FCST_TMPL');

   commit;
   V_WEEKLY_FLAG := 1;

 insert into RR_APO_FCST_TMPL
  SELECT * FROM T_SRC_SAPL_APO_FCST_TMPL;

  COMMIT;


    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

end PRC_UPD_APO_STG_WEEKLY;

procedure PRC_POP_EXFACT_BASE(P_START_DATE date,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
  sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_POP_EXFACT_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_POP_EXFACT_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_POP_EXFACT_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



 check_and_drop('rr_exfact_pop_temp');

 sql_str := 'create table rr_exfact_pop_temp as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             sd.sales_date,
             sd.sales_date  plan_date
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
          --   AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                                AND NVL(sd.exfact_base, 0)  <> 0)sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id
             AND sd.location_id = sd1.location_id
             AND sd.plan_date = sd1.sales_date
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                              ';



--   dynamic_ddl(sql_str);

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_exfact_pop_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
          )
      WHEN MATCHED THEN
         UPDATE
         SET  exfact_base = 0       ';

--   dynamic_ddl(sql_str);
    -- cmos 23-Jan-2017 (px update)
    -- cmos 25-Feb-2017 (use px index instead of fts)
    -- cmos 28-Feb-2017 (px update, fts is faster than index)
    -- cmos 01-Mar-2017 (remove pdml as table is not partitioned)
    -- cmos 07-Mar-2017 (re-add pdml hint)
    UPDATE /*+ enable_parallel_dml full(sd) parallel(sd,32) */ SALES_DATA SD
  SET SD.EXFACT_BASE = 0
  WHERE nvl(sd.exfact_base, 0) <> 0
  and sd.sales_date between  start_date  and end_date;

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_pop_apo_fcst(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_apo_fcst
   PURPOSE:    Temp procedure to un offset the apo forecast values received

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        26/11/2015  Lakshmi Annapragada / UXC

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   FORE_COLUMN   varchar2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_POP_APO_FCST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   /***** Start RFC CHG3297 - To update null values to zeroes : Lakshmi Annapragada 05 April 2016*************/
   -- cmos 23-Jan-2017 (px update)
   -- cmos 01-Mar-2017 (remove pdml as table is not partitioned)
   dynamic_ddl('    update  /*+ FULL(sd) PARALLEL(sd,32) */ sales_data sd set sd.manual_stat = 0 where sd.manual_stat is null and
         sd.SALES_DATE BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - 6*7
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + 6*7
                               and demand_type = 1');

   COMMIT;
   /******** End RFC CHG3297 - To update null values to zeroes *******/

   -- cmos 20-Feb-2017 (px merge)
   -- cmos 25-Feb-2017 (use px index in select part)
   dynamic_ddl('merge /*+ enable_parallel_dml full(s) parallel(s,32) */ into SALES_DATA S using (
     select  /*+ index(sd,sales_data_pk) parallel(32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, mdp_matrix m
      where  SD.SALES_DATE BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - 6*7
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + 6*7
        AND M.DEM_STREAM = 1
        and m.item_id = sd.item_id
        and m.location_id = sd.location_id
        AND nvl(SD.MANUAL_STAT,0) <> 0) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   when matched then update set
   S.manual_stat = 0,
   s.last_update_date = sysdate');

   commit;

   v_status := 'Start2';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_apo_fcst_pre_temp');

-- cmos 23-Jan-2017 (px ctas)
sql_str := 'CREATE TABLE rr_apo_fcst_pre_temp parallel 32 nologging as SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             (next_day(sd.sales_Date,''MONDAY'')-7 + NVL(mdp.scan_forecast_offset, 0) * 7)  plan_date,
             sum(NVL(sd.exfact_base,0)) manual_stat
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND NVL(sd.exfact_base,0) <> 0
            -- AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY((sd.sales_date + (NVL(mdp.scan_forecast_offset, 0) * 7)), ''MONDAY'') - 7
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                                group by  sd.item_id,
             sd.location_id,( next_day(sd.sales_Date,''MONDAY'')-7+ NVL(mdp.scan_forecast_offset, 0) * 7)
                                ';
  dynamic_ddl(sql_str);
  dynamic_ddl('alter table rr_apo_fcst_pre_temp noparallel');		-- cmos 23-Jan-2017

   v_status := 'Start2.1';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_apo_fcst_pop_temp');

sql_str := 'create table rr_apo_fcst_pop_temp as   SELECT /*+ full(sd) parallel(sd) index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date,  sd.manual_stat
              FROM rr_apo_fcst_pre_temp sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.manual_stat, 0) - NVL(sd1.manual_stat, 0)) <> 0';




    --dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'Start2.2';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t) */
            T.ITEM_ID,T.LOCATION_ID,I.DATET plan_date,T.MANUAL_STAT*(I.NUM_OF_DAYS/7) MANUAL_STAT_SPLIT
            FROM RR_APO_FCST_PRE_TEMP T,INPUTS I
            WHERE T.PLAN_DATE = NEXT_DAY(I.DATET,''MONDAY'')-7
            ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
--         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
--                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET sd.manual_stat = sd1.MANUAL_STAT_SPLIT
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, manual_stat)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.MANUAL_STAT_SPLIT)';


   dynamic_ddl(sql_str);

   COMMIT;

   v_status := 'Start2.3';
   v_prog_name := 'PRC_POP_APO_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   SQL_STR := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ full(sd) parallel(sd) */
             sd.item_id,
             sd.location_id,
             sd.sales_Date  ,
             NVL(sd.exfact_base,0) manual_stat
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 0
             and NVL(sd.exfact_base,0) <> nvl(sd.manual_stat, 0)
            -- AND NEXT_DAY(i.datet, ''MONDAY'') -7 = NEXT_DAY((sd.sales_date + (NVL(mdp.scan_forecast_offset, 0) * 7)), ''MONDAY'') - 7
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')

            ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.sales_date = sd.sales_date
          )
      WHEN MATCHED THEN
         UPDATE
         SET sd.manual_stat = sd1.MANUAL_STAT
     ';


--   dynamic_ddl(sql_str);

--    update /*+ FULL(pd) PARALLEL(sd, 64) */ SALES_DATA SD
--  SET sd.manual_stat = sd.exfact_base
--  WHERE NVL(SD.EXFACT_BASE,0) <> NVL(SD.MANUAL_STAT, 0)
--  and sd.demand_type = 0
--  and sd.sales_date between  p_start_date  and p_end_date;

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  end;

PROCEDURE prc_offset_scan(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, MDP_MATRIX M
      where SD.LOCATION_ID = M.LOCATION_ID
        and SD.ITEM_ID = M.ITEM_ID
        and sd.sales_date between start_date and  end_date
       and M.DEM_STREAM = 1
--           and sd.ITEM_ID = 1115 and sd.LOCATION_ID in (54379,75518)
        and SD.act_part_ord is not null) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   when matched then update set
   S.act_part_ord = null,
   s.last_update_date = sysdate;

   commit;

   v_status := 'Start2';
   v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_offset_scan_temp');

 -- cmos 23-Jan-2017 (px ctas)
 sql_str := 'create table rr_offset_scan_temp parallel 32 nologging as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date, sd.act_part_ord
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7)   plan_date,
             sum(NVL(sd.sdata5, 0)) act_part_ord
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               group by sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7) )sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.act_part_ord, 0) - NVL(sd1.act_part_ord, 0))  <> 0';



   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_offset_scan_temp noparallel');		-- cmos 23-Jan-2017

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_offset_scan_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET act_part_ord = sd1.act_part_ord
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, act_part_ord)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.act_part_ord)';

   dynamic_ddl(sql_str);

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;
  PROCEDURE prc_offset_dm_fcst(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_OFFSET_DM_FCST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OFFSET_DM_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_OFFSET_DM_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, MDP_MATRIX M
      where SD.LOCATION_ID = M.LOCATION_ID
        and SD.ITEM_ID = M.ITEM_ID
        AND SD.SALES_DATE BETWEEN START_DATE AND  END_DATE
    --   and M.DEM_STREAM = 0
        and (nvl(sd.engine_base,0) <> 0 or nvl(sd.sales_dif,0) <> 0) ) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   WHEN MATCHED THEN UPDATE SET
   S.engine_base = 0,s.sales_dif = 0,
   s.last_update_date = sysdate;

   commit;

   v_status := 'Start2';
   --v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_offset_fcst_temp');

 -- cmos 23-Jan-2017 (px ctas)
 sql_str := 'create table rr_offset_fcst_temp parallel 32 nologging as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date, sd.engine_base, sd.sales_dif
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7)  plan_date,
             sum(sd.'||fore_column||') engine_base,
             sum(sd.final_pos_fcst) sales_dif
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               group by sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7) )sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND  ABS(NVL(sd.engine_base, 0) - NVL(sd1.engine_base, 0))  +  ABS(NVL(sd.sales_dif, 0) - NVL(sd1.sales_dif, 0)) <> 0';



   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_offset_fcst_temp noparallel');		-- cmos 23-Jan-2017

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_offset_fcst_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET engine_base = sd1.engine_base,
         sales_dif = sd1.sales_dif
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, engine_base,sales_dif)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.engine_base,sd1.sales_dif)';

   dynamic_ddl(sql_str);

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  end;

    PROCEDURE prc_offset_incr_fcst(p_start_date DATE,
                                p_end_date   DATE)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_OFFSET_INCR_FCST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_OFFSET_INCR_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';
   v_prog_name := 'PRC_OFFSET_INCR_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   merge into SALES_DATA S using (
     select  /*+ parallel(sd, 32) */ SD.LOCATION_ID,SD.ITEM_ID,SD.SALES_DATE
       from SALES_DATA SD, MDP_MATRIX M
      where SD.LOCATION_ID = M.LOCATION_ID
        and SD.ITEM_ID = M.ITEM_ID
        and sd.sales_date between start_date and  end_date
        AND m.dem_stream = 1
        and  nvl(sd.sales_dif,0) <> 0 ) T
   on
     (S.LOCATION_ID = T.LOCATION_ID
      and S.ITEM_ID = T.ITEM_ID
      and S.SALES_DATE = T.SALES_DATE)
   WHEN MATCHED THEN UPDATE SET
   s.sales_dif = 0,
   s.last_update_date = sysdate;

   commit;

   v_status := 'Start2';
   --v_prog_name := 'PRC_OFFSET_SCAN';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


 check_and_drop('rr_offset_incr_fcst_temp');

 -- cmos 23-Jan-2017 (px ctas)
 sql_str := 'create table rr_offset_incr_fcst_temp parallel 32 nologging as   SELECT /*+ index(sd1, sales_data_pk) */  sd.item_id, sd.location_id, sd.plan_date,  sd.sales_dif
              FROM(SELECT /*+ parallel(sd) */
             sd.item_id,
             sd.location_id,
             (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7)  plan_date,
             sum(sd.final_pos_fcst) sales_dif
             FROM sales_data sd,
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id
             AND mdp.dem_stream = 1
             AND sd.sales_date  BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               group by sd.item_id,
             sd.location_id,
              (next_day(sd.sales_Date,''MONDAY'')-7 - NVL(mdp.scan_forecast_offset, 0) * 7) )sd,
             sales_data sd1
             WHERE 1 = 1
             AND sd.item_id = sd1.item_id (+)
             AND sd.location_id = sd1.location_id (+)
             AND sd.plan_date = sd1.sales_date (+)
             AND sd.plan_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
                               AND ABS(NVL(sd.sales_dif, 0) - NVL(sd1.sales_dif, 0)) <> 0';



   dynamic_ddl(sql_str);
   dynamic_ddl('alter table rr_offset_incr_fcst_temp noparallel');		-- cmos 23-Jan-2017

   COMMIT;

 sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ parallel(t,24) */
            * from rr_offset_incr_fcst_temp t ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
         and  sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7)
          )
      WHEN MATCHED THEN
         UPDATE
         SET sales_dif = sd1.sales_dif
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, sales_dif)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.sales_dif)';

   dynamic_ddl(sql_str);

   COMMIT;


   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         end;
  end;

  PROCEDURE prc_compare_apo_prev_load
  AS
/******************************************************************************
   NAME:       prc_compare_apo_prev_load
   PURPOSE:    Temp procedure to compare apo volume baseline from the previous load

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
    1.0        11/02/2016  Lakshmi Annapragada/ UXC Redrock Consulting - Initial version

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);


   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);

   v_eng_profile    NUMBER := 1;

   v_prog_name   VARCHAR2 (100)   := 'PRC_COMPARE_APO_PREV_LOAD';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;

  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_COMPARE_APO_PREV_LOAD';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;


   ----NULL out all "Exfactory" Combinations

   v_status := 'Start1';

   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   dynamic_ddl('TRUNCATE TABLE RR_APO_FCST_TMPL');

   /*ADDING COMBINATIONS WHERE THE FORECAST CHANGED*/
   INSERT INTO RR_APO_FCST_TMPL
    SELECT t1.* FROM T_SRC_SAPL_APO_FCST_TMPL T1 , T_SRC_SAPL_APO_FCST_TMPL_BK T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 AND T1.WEEK_START_DATE = T2.WEEK_START_DATE
 and t1.apo_baseline_fcst <> t2.apo_baseline_fcst;

 commit;

 /*ADDING COMBINATIONS THAT ARE NEW IN CURRENT LOAD*/
 INSERT INTO RR_APO_FCST_TMPL
  SELECT * FROM T_SRC_SAPL_APO_FCST_TMPL T1 WHERE not EXISTS(SELECT 1 FROM T_SRC_SAPL_APO_FCST_TMPL_BK T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 and t1.week_start_date = t2.week_start_date
 );

   commit;

   /*CLEARING FORECAST FOR THE COMBINATIONS THAT ARE MISSED IN CURRENT LOAD*/
   insert into rr_apo_fcst_tmpl(week_start_date,sku_code,cl3_code,sales_office,apo_baseline_fcst)
  SELECT T1.WEEK_START_DATE,T1.SKU_CODE,T1.CL3_CODE,T1.SALES_OFFICE,0  FROM T_SRC_SAPL_APO_FCST_TMPL_BK T1 WHERE not EXISTS(SELECT 1 FROM T_SRC_SAPL_APO_FCST_TMPL T2
 WHERE T1.SKU_CODE = T2.SKU_CODE
 AND T1.CL3_CODE = T2.CL3_CODE
 AND T1.SALES_OFFICE = T2.SALES_OFFICE
 and t1.week_start_date = t2.week_start_date
 );
      COMMIT;

      /** Inserting the errors from Previous load - To account for any master data changes */

       insert into rr_apo_fcst_tmpl(week_start_date,sku_code,cl3_code,sales_office,apo_baseline_fcst)
  select sdate,level1,level2,level3,rr_apo_fcst_offset
  from biio_apo_fcst_err ;

  commit;

   v_status := 'end';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         end;
  end;

END;

/
