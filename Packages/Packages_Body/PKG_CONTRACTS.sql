--------------------------------------------------------
--  DDL for Package Body PKG_CONTRACTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_CONTRACTS" 
AS
  /******************************************************************************
  NAME:       PKG_PTP
  PURPOSE:    All procedures commanly used for claims module
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
  1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version
  ******************************************************************************/

PROCEDURE prc_sync_contract_to_sd(
    p_start_date    DATE,
    p_end_date      DATE,
    p_full_load     NUMBER DEFAULT 0)
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;
  v_dummy_prod_id   NUMBER := 0;
  v_mdp_assist      NUMBER :=0;
  v_col_names       VARCHAR2(32000);
  v_insert_sql     VARCHAR2(32000);
  v_merge_sql      VARCHAR2(32000);
  v_select_sql     VARCHAR2(32000);
  v_from_sql       VARCHAR2(32000);
  v_col_where_sql  VARCHAR2(32000);
  v_cust_col_where_sql VARCHAR2(32000);
  v_where_sql      VARCHAR2(32000);
  v_extra_where_SQL varchar2(32000);
  v_group_by_SQL   varchar2(32000);
  sql_str        varchar2(32000);

  v_last_run_date  DATE;
  v_new_run_date   DATE := sysdate;

begin
  pre_logon;
  V_STATUS      := 'Start Mode=' || p_full_load || ' ';
  v_prog_name   := 'PRC_SYNC_CONTRACT_TO_SD';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

 -----------------------
 ----Mode Parameters----
 -----------------------
 -- 0 = Partial Load
 -- 1 = Full Load
 -----------------------

  --Truncate for a full load to ensure all combinations are sync correctly
  if p_full_load = 1 then
     DYNAMIC_DDL('TRUNCATE TABLE CONTRACT_DATA');
  end if;

 ---Get last Sync Date

    SELECT TO_DATE(pval, 'mm-dd-yyyy hh24:mi:ss')
    INTO v_last_run_date
    FROM sys_params
    WHERE pname = 'last_contract_sync';


  v_col_names := '';
  v_where_sql := ' WHERE 1=1';

---------------------------------------------------
----Sync CONTRACT_LINES to CONTRACT_DATA-----------
---------------------------------------------------

  ----Retrieve column names for Heriarachies used in Contract Management-----
   FOR rec1 IN (select distinct id_field
                  from group_Tables g, access_seq s
                 where g.GROUP_TABLE_ID = s.level1
                    or g.GROUP_TABLE_ID = s.level2
                    or g.GROUP_TABLE_ID = s.level3
                    or g.GROUP_TABLE_ID = s.level4
                    or g.GROUP_TABLE_ID = s.level5
                    or g.GROUP_TABLE_ID = s.level6
                    or g.GROUP_TABLE_ID = s.level7
                    or g.GROUP_TABLE_ID = s.level8
                    or g.GROUP_TABLE_ID = s.level9
                    or g.GROUP_TABLE_ID = s.level10
    )
    LOOP
        v_col_where_sql  :=   v_col_where_sql || ' AND m.' || rec1.id_field || ' = nvl(c.' || rec1.id_field || ', m.' || rec1.id_field ||  ')';
     end loop;

   ----Retrieve all Customer column names for Heriarachies used in Contract Management-----
   FOR rec1 IN (select distinct id_field
                  from group_Tables g, access_seq s
                 where lower(g.search_table) = 'location'
                    and (g.GROUP_TABLE_ID = s.level1
                    or g.GROUP_TABLE_ID = s.level2
                    or g.GROUP_TABLE_ID = s.level3
                    or g.GROUP_TABLE_ID = s.level4
                    or g.GROUP_TABLE_ID = s.level5
                    or g.GROUP_TABLE_ID = s.level6
                    or g.GROUP_TABLE_ID = s.level7
                    or g.GROUP_TABLE_ID = s.level8
                    or g.GROUP_TABLE_ID = s.level9
                    or g.GROUP_TABLE_ID = s.level10)

    )
    LOOP
        v_cust_col_where_sql := v_cust_col_where_sql || ' AND m.' || rec1.id_field || ' = nvl(c.' || rec1.id_field || ', m.' || rec1.id_field ||  ')';
     end loop;

   ---------------------------------------------------------------
   ----Create Lower Level Rows for Case Deal and Percentage------
   --------------------------------------------------------------
       --Create Temp Table
   -- cmos 23-Feb-2017 (px dop 32)
   -- cmos 28-Feb-2018 (pc dop 64)
   v_select_sql :=  'select /*+ parallel(64) */ m.location_id, m.item_id, i.datet sales_date, c.contract_group_id,' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id,' ||
                           'c.case_deal_amt, c.case_deal_perc, t.deal_type, t.comp_type_id ';
   v_from_sql :=  ' FROM mdp_matrix m, contract_line c, inputs i, comp_type_seq s, comp_type t ';
   v_where_sql  :=  ' WHERE i.datet between c.start_date and c.end_date '||
                                     'AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                                         'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                                     'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     'AND c.contract_stat_id in (2,3,4,6,8,10) '||
                                     'AND c.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     'AND c.start_date < TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                                     'AND t.comp_type_id = s.comp_type_id ' || v_col_where_sql;
   v_extra_where_SQL := ' AND t.deal_type in (1,2)';
   if p_full_load = 0 then
     v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
   end if;

   check_and_drop('v_tmp_cont_data');
   -- cmos 23-Feb-2017 (px ctas)
   -- cmos 28-Feb-2017 (px dop 64)
   DYNAMIC_DDL('create table v_tmp_cont_data parallel 64 nologging as ' ||v_select_sql||v_from_sql || v_where_sql || v_extra_where_SQL  );
   dynamic_ddl('alter table v_tmp_cont_data noparallel');		-- cmos 23-Feb-2017


    V_STATUS      := 'Step 2 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

	 -- cmos 28-Feb-2018 (px index creation)
         sql_str := 'CREATE INDEX v_tmp_cont_data_idx ON v_tmp_cont_data(item_id, location_id, sales_date) parallel 16 nologging';
         dynamic_ddl (sql_str);
	 dynamic_ddl ( 'alter index v_tmp_cont_data_idx noparallel' );		-- cmos 28-Feb-2017

     --Merge Temp Table into CONTRACT_DATA
   v_extra_where_SQL := '';
   -- cmos 23-Feb-2017 (px merge)
   -- Asahi 01-Mar-2017 (changed dop from 32 to 16)
   DYNAMIC_DDL( 'MERGE /*+ enable_parallel_dml full(cd) parallel(cd,16) */ into contract_data cd using (select /*+ full(tcd) parallel(tcd,16) */ * from v_tmp_cont_data tcd where 1=1 '||  v_extra_where_SQL ||') a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.case_deal_amt = a.case_deal_amt, '||
                  'cd.case_deal_perc = a.case_deal_perc, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.case_deal_amt, cd.case_deal_perc,cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.case_deal_amt, a.case_deal_perc, sysdate) ');


  commit;

      V_STATUS      := 'Step 3 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

------------------------------------------------------------------
 ----Create Lower Level Rows for Lumpsum Except Amortization------
 -----------------------------------------------------------------
    --Update Glob Prop in contract_line

     v_select_sql := 'SELECT /*+parallel */ c.contract_line_id, sum(case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end) glob_prop ';
     v_from_sql :=  ' FROM mdp_matrix m, contract_line c, comp_type_seq s, comp_type t ';
     v_where_SQL := 'WHERE c.contract_stat_id in (2,3,4,6,8,10) '||
                                     'AND GREATEST(TRUNC(c.start_date ,''MM''),NEXT_DAY(c.start_date ,''MONDAY'')-7) '||
                                          ' BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     '      AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                                     'AND t.comp_type_id = s.comp_type_id ' ||
                                     'AND t.deal_type in (3) ' || v_col_where_sql;
     v_group_by_sql := ' GROUP BY c.contract_line_id ';
     if p_full_load = 0 then
        v_extra_where_SQL := ' AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
     end if;

     check_and_drop('v_tmp_cont_data');
     -- cmos 23-Feb-2017 (px ctas)
     DYNAMIC_DDL('create table v_tmp_cont_data parallel 32 nologging as ' ||v_select_sql||v_from_sql || v_where_SQL || v_col_where_sql || v_extra_where_SQL || v_group_by_sql );
     dynamic_ddl('alter table v_tmp_cont_data noparallel');		-- cmos 23-Feb-2017

   --      sql_str := 'CREATE INDEX v_tmp_cont_data_idx ON v_tmp_cont_data(item_id, location_id, sales_date)';
   --      dynamic_ddl (sql_str);

    V_STATUS      := 'Step 4 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);


     DYNAMIC_DDL( 'MERGE INTO /*+parallel */  CONTRACT_LINE cl using (select /*+parallel */ * from v_tmp_cont_data)a '||
                     'on (cl.contract_line_id = a.contract_line_id) '||
                     'when matched then update '||
                     'set cl.prop_factor = a.glob_prop ');

     commit;

     V_STATUS      := 'Step 5 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);


   --Create Temp Table for updating Contract_data
     v_select_sql :=  'select /*+parallel */ m.location_id, m.item_id, GREATEST(TRUNC(c.start_date ,''MM''),NEXT_DAY(c.start_date ,''MONDAY'')-7) sales_date, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, (c.lumpsum * (case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end / c.prop_factor)) lumpsum';
     v_extra_where_SQL := ' AND nvl(c.RR_CL_AMORTIZE,0) = 0';
     if p_full_load = 0 then
       v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ';
     end if;

     check_and_drop('v_tmp_cont_data');
     -- cmos 23-Feb-2017 (px ctas)
     DYNAMIC_DDL('create table v_tmp_cont_data parallel 32 nologging as ' ||v_select_sql||v_from_sql || v_where_sql ||  v_extra_where_SQL );
     dynamic_ddl('alter table v_tmp_cont_data noparallel');		-- cmos 23-Feb-2017

     sql_str := 'CREATE INDEX v_tmp_cont_data_idx ON v_tmp_cont_data(item_id, location_id, sales_date)';
     dynamic_ddl (sql_str);

     V_STATUS      := 'Step 6 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

   --Merge into Contract_data

   DYNAMIC_DDL( 'MERGE INTO /*+parallel */  contract_data cd using (select /*+parallel */ * from v_tmp_cont_data) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');


  commit;

    V_STATUS      := 'Step 7 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

 ------------------------------------------------------------
 ----Create Lower Level Rows for Lumpsum Amortization--------
 ------------------------------------------------------------
    --Get number of periods to split by
     merge into  contract_line l using
     (select /*+parallel */ c.contract_line_id, count(distinct calendar_month) AMORTIZE_PERIODS
          from contract_line c, comp_type_seq s, comp_type t, inputs i
         WHERE c.contract_stat_id in (2,3,4,6,8,10)
               --AND i.datet between GREATEST(TRUNC(p_start_date, 'MM'), NEXT_DAY(p_start_date,'MONDAY')-7)  and p_end_date
               AND (c.last_update_date >= v_last_run_date or p_full_load = 1)
             --  AND c.start_date < p_end_date
               and c.end_date > p_start_date
               AND c.comp_type_seq_id = s.comp_type_seq_id
               AND t.comp_type_id = s.comp_type_id
               AND t.deal_type in (3)
               AND nvl(c.RR_CL_AMORTIZE,0) = 1
               AND i.datet between GREATEST(TRUNC(c.start_date, 'MM'), NEXT_DAY(c.start_date,'MONDAY')-7)
                               AND c.end_date
         GROUP BY c.contract_line_id
      )a
     on (l.contract_line_id = a.contract_line_id)
     when matched then update
     set l.AMORTIZE_PERIODS = a.AMORTIZE_PERIODS;

     commit;

        V_STATUS      := 'Step 8 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

    --Create Temp Table for updating Contract_data
     v_select_sql :=  'select /*+parallel */ m.location_id, m.item_id, min(i.datet) sales_date, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, '||
                           'max(c.lumpsum * case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end / (c.prop_factor * AMORTIZE_PERIODS)) lumpsum, ' ||
                           'i.calendar_month ';
     v_from_sql :=  ' FROM mdp_matrix m, contract_line c, inputs i, comp_type_seq s, comp_type t  ';
     v_where_SQL := ' WHERE i.datet between c.start_date and c.end_date '||
                     'AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                   'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                   'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                     'AND c.contract_stat_id in (2,3,4,6,8,10) '||
                     'AND c.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                     'AND t.comp_type_id = s.comp_type_id ' ||
                     'AND t.deal_type in (3) ' ||v_col_where_sql;
     v_extra_where_SQL := ' AND nvl(c.RR_CL_AMORTIZE,0) = 1 ';
     if p_full_load = 0 then
        v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
     end if;
     v_group_by_sql    := ' GROUP BY m.location_id, m.item_id, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, i.calendar_month';
     check_and_drop('v_tmp_cont_data');
     -- cmos 23-Feb-2017 (px ctas)
     DYNAMIC_DDL('create table v_tmp_cont_data parallel 32 nologging as ' ||v_select_sql||v_from_sql || v_where_sql ||  v_extra_where_SQL  || v_group_by_sql );
     dynamic_ddl('alter table v_tmp_cont_data noparallel');		-- cmos 23-Feb-2017

     sql_str := 'CREATE INDEX v_tmp_cont_data_idx ON v_tmp_cont_data(item_id, location_id, sales_date)';
     dynamic_ddl (sql_str);

    V_STATUS      := 'Step 9 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

    --Merge into Contract_data
   DYNAMIC_DDL( 'MERGE INTO /*+parallel */  contract_data cd using (select /*+parallel */ * from v_tmp_cont_data) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');


      commit;


------------------------------------------------------------
-----Lumpsum combinations that have no combinations  ------
------------------------------------------------------------

    V_STATUS      := 'Step 10 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

-----Get all Lumpsum combinations that have no combinations
     check_and_drop('v_tmp_cont_data_miss');
     -- cmos 23-Feb-2017 (px ctas)
     DYNAMIC_DDL('create table v_tmp_cont_data_miss parallel 32 nologging as
                  select l.*
                  from contract_line l, comp_type c
                 where l.comp_type_id = c.comp_type_id
                   and deal_type = 3
                   and l.contract_stat_id in (2,3,4,6,8,10)
                   AND l.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                   and nvl(l.lumpsum,0) <> 0
                   and l.contract_line_id not in (select contract_line_id from contract_data ) ' );
    dynamic_ddl('alter table v_tmp_cont_data_miss noparallel');		-- cmos 23-Feb-2017

    V_STATUS      := 'Step 11 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

 ----Get all Combinations that have a dummy product against it
    check_and_drop('v_tmp_cont_data_miss_comb');
     -- cmos 23-Feb-2017 (px ctas)
     DYNAMIC_DDL('create table v_tmp_cont_data_miss_comb parallel 32 nologging as ' ||
                 ' select /*+parallel */ c.contract_line_id, max(m.location_id) location_id, min(m.item_id) item_id, 0 new_comb ' ||
                 ' FROM mdp_matrix m, v_tmp_cont_data_miss c, t_ep_item i ' ||
                 ' WHERE m.t_ep_item_ep_id = i.t_ep_item_ep_id' ||
                 ' AND i.item like ''999999999999999999%''' ||
                 v_cust_col_where_sql ||
                 ' GROUP BY contract_line_id');
     dynamic_ddl('alter table v_tmp_cont_data_miss_comb noparallel');		-- cmos 07-Mar-2017 (fix table name)

   select min(item_id) into v_dummy_prod_id
     from items i, t_ep_item mt
    where i.t_ep_item_ep_id = mt.t_ep_item_ep_id
      AND mt.item like '999999999999999999%';

    V_STATUS      := 'Step 12 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

 ----Create all othe Customer combinations that do not have a Dummy Product

     DYNAMIC_DDL(' INSERT INTO v_tmp_cont_data_miss_comb ' ||
                 ' select /*+parallel */ c.contract_line_id, max(m.location_id) location_id, '|| v_dummy_prod_id || ' item_id, 1 new_comb ' ||
                 ' FROM location m, v_tmp_cont_data_miss c ' ||
                 ' WHERE c.contract_line_id not in (select contract_line_id from v_tmp_cont_data_miss_comb ) ' ||
                 v_cust_col_where_sql ||
                 ' GROUP BY contract_line_id');

    commit;

    V_STATUS      := 'Step 13 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);


        --Merge into Contract_data for Non Amortised Contracts
   DYNAMIC_DDL( 'MERGE INTO /*+parallel */  contract_data cd using ( ' ||
                 'select /*+parallel */ b.location_id,b.item_id,  GREATEST(TRUNC(a.start_date ,''MM''),NEXT_DAY(a.start_date ,''MONDAY'')-7) sales_date, '||
                 ' a.contract_group_id, a.contract_id, a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum ' ||
                 '   from v_tmp_cont_data_miss a,v_tmp_cont_data_miss_comb b ' ||
                  'where a.contract_line_id = b.contract_line_id ' ||
                  '  and nvl(a.RR_CL_AMORTIZE,0) = 0 ) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');


      commit;

    V_STATUS      := 'Step 14 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

              --Merge into Contract_data for All Amortised Contracts
   DYNAMIC_DDL( 'MERGE INTO /*+parallel */  contract_data cd using ( ' ||
                 'select /*+parallel */ b.location_id,b.item_id, min(i.datet) sales_date,a.contract_group_id, ' ||
                 ' a.contract_id, a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, max(a.lumpsum) / max(a.AMORTIZE_PERIODS) lumpsum,i.calendar_month ' ||
                 '   from v_tmp_cont_data_miss a,v_tmp_cont_data_miss_comb b, inputs i ' ||
                  'where a.contract_line_id = b.contract_line_id ' ||
                   ' and i.datet between a.start_date and a.end_date '||
                   ' AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                   'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                   'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                    ' AND a.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                  '  and nvl(a.RR_CL_AMORTIZE,0) = 1 ' ||
                  '  GROUP BY b.location_id,b.item_id,a.contract_group_id, a.contract_id, a.contract_payment_id, ' ||
                 '  a.contract_line_id, a.comp_type_seq_id, i.calendar_month) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');


        commit;


    V_STATUS      := 'Step 15 = SD ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

  ----------------------------------------------------------------------
  ------- Sync CONCTRACT_DATA to SD ------------------------------------
  ----------------------------------------------------------------------


    v_extra_where_SQL:='';


    ---If Partial Sync, get all locaton_id/item_ids that need to be sync)
    if p_full_load = 0  then

        DYNAMIC_DDL('truncate table v_tmp_cont_sync_ids');
        INSERT into v_tmp_cont_sync_ids
         (location_id,item_id)
         select /*+parallel */ distinct location_id, item_id
           from contract_data c
          WHERE c.last_update_date >= v_last_run_date
         ;
        v_extra_where_SQL := ' AND (cd.location_id,item_id) in (select c.location_id,c.item_id from v_tmp_cont_sync_ids c) ' ;

     elsif p_full_load = 1 then

        ---Clear all values in SD
	-- cmos 23-Feb-2017 (use px index instead of fts)
	-- cmos 28-Feb-2017 (use px fts on merge and px index on select)
        MERGE /*+ enable_parallel_dml full(sd) parallel(sd,16) */ into sales_data sd using(
            select /*+ index(sd2,sales_data_pk) parallel(16) */ location_id,item_id,sales_date,
                        null ebspricelist104,
                        null ebspricelist111,
                        null ebspricelist112,
                        null ebspricelist113,
                        null ebspricelist114,
                        null ebspricelist115,
                        null ebspricelist116,
                        null ebspricelist117,
                        null ebspricelist118,
                        null ebspricelist121,
                        null ebspricelist122,
                        null mkt_acc1,
                        null mkt_acc3,
                        null ff_input1,
                        null cust_input3,
                        null mkt_acc2
            from sales_data sd2
           WHERE sales_date between p_start_date and p_end_date
             AND (location_id,item_id,sales_date) not in (select distinct location_id,item_id,sales_date from contract_data where sales_date between p_start_date and p_end_date)
             and     ABS(NVL(ebspricelist104, 0))
                   + ABS(NVL(ebspricelist111, 0))
                   + ABS(NVL(ebspricelist112, 0))
                   + ABS(NVL(ebspricelist113, 0))
                   + ABS(NVL(ebspricelist114, 0))
                   + ABS(NVL(ebspricelist115, 0))
                   + ABS(NVL(ebspricelist116, 0))
                   + ABS(NVL(ebspricelist117, 0))
                   + ABS(NVL(ebspricelist118, 0))
                   + ABS(NVL(ebspricelist121, 0))
                   + ABS(NVL(ebspricelist122, 0))
                   + ABS(NVL(mkt_acc1, 0))
                   + ABS(NVL(mkt_acc3, 0))
                   + ABS(NVL(ff_input1, 0))
                   + ABS(NVL(cust_input3, 0))
                   + ABS(NVL(mkt_acc2, 0)) <> 0
        )sd1
        on ( sd.location_id = sd1.location_id
         and sd.item_id = sd1.item_id
         and sd.sales_date = sd1.sales_date)
         WHEN MATCHED THEN UPDATE
                    set sd.ebspricelist104 = sd1.ebspricelist104,
                        sd.ebspricelist111 = sd1.ebspricelist111,
                        sd.ebspricelist112 = sd1.ebspricelist112,
                        sd.ebspricelist113 = sd1.ebspricelist113,
                        sd.ebspricelist114 = sd1.ebspricelist114,
                        sd.ebspricelist115 = sd1.ebspricelist115,
                        sd.ebspricelist116 = sd1.ebspricelist116,
                        sd.ebspricelist117 = sd1.ebspricelist117,
                        sd.ebspricelist118 = sd1.ebspricelist118,
                        sd.ebspricelist121 = sd1.ebspricelist121,
                        sd.ebspricelist122 = sd1.ebspricelist122,
                        sd.mkt_acc1 = sd1.mkt_acc1,
                        sd.mkt_acc3 = sd1.mkt_acc3,
                        sd.ff_input1 = sd1.ff_input1,
                        sd.cust_input3 = sd1.cust_input3,
                        sd.mkt_acc2 = sd1.mkt_acc2,
                        last_update_date = SYSDATE ;

        commit;

     end if;


    V_STATUS      := 'Step 16 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

  -- Cteate temporary table of SD data
  check_and_drop('RR_CON_D_SYNC_TEMP');
  -- cmos 23-Feb-2017 (px ctas)
  -- cmos 28-Feb-2017 (px dop 64)
  -- cmos 02-Mar-2017 (supply dop=32 to select part and reduce dop on ctas to 32)
  sql_str := 'CREATE TABLE RR_CON_D_SYNC_TEMP parallel 32 nologging as ' ||
              'SELECT /*+ parallel(32) */ cd.item_id, cd.location_id, cd.sales_date,'||
              'max(CASE WHEN s.comp_type_id in (1) THEN cd.case_deal_perc ELSE NULL END) ebspricelist111,' ||
              'max(CASE WHEN s.comp_type_id in (2) THEN cd.case_deal_amt ELSE NULL END) ebspricelist112,' ||
              'max(CASE WHEN s.comp_type_id in (3) THEN cd.case_deal_amt ELSE NULL END) ebspricelist113,' ||
              'max(CASE WHEN s.comp_type_id in (4) THEN cd.case_deal_perc ELSE NULL END) ebspricelist114,' ||
              'sum(CASE WHEN s.comp_type_id in (5) THEN cd.lumpsum ELSE NULL END) ebspricelist116,' ||
              'sum(CASE WHEN s.comp_type_id in (6) THEN cd.case_deal_amt ELSE NULL END) ebspricelist117,' ||
              'sum(CASE WHEN s.comp_type_id in (7) THEN cd.lumpsum ELSE NULL END) ebspricelist115,' ||
              'sum(CASE WHEN s.comp_type_id in (8) THEN cd.lumpsum ELSE NULL END) ebspricelist121,' ||
              'sum(CASE WHEN s.comp_type_id in (9) THEN cd.case_deal_amt ELSE NULL END) mkt_acc1,' ||
              'sum(CASE WHEN s.comp_type_id in (10) THEN cd.case_deal_perc ELSE NULL END) ebspricelist118,' ||
              'sum(CASE WHEN s.comp_type_id in (11) THEN cd.case_deal_amt ELSE NULL END) ebspricelist122,' ||
              'sum(CASE WHEN s.comp_type_id in (12) THEN cd.case_deal_perc ELSE NULL END) ff_input1,' ||
              'sum(CASE WHEN s.comp_type_id in (13) THEN cd.case_deal_amt ELSE NULL END) cust_input3,' ||
              'sum(CASE WHEN s.comp_type_id in (14) THEN cd.case_deal_perc ELSE NULL END) mkt_acc3,' ||
              'min(CASE WHEN s.comp_type_id in (15) THEN cd.case_deal_amt ELSE NULL END) ebspricelist104,' ||
              'sum(CASE WHEN s.comp_type_id in (16) THEN cd.case_deal_perc ELSE NULL END) mkt_acc2 ' ||
              'FROM contract_data cd, comp_type_seq s ' ||
              'WHERE cd.sales_date BETWEEN TO_DATE('''|| TO_CHAR(p_start_date, 'DD/MM/YYYY') ||''',''DD/MM/YYYY'')' || ' AND TO_DATE('''|| TO_CHAR(p_end_date, 'DD/MM/YYYY') ||''',''DD/MM/YYYY'')' ||
              ' AND cd.comp_type_seq_id = s.comp_type_seq_id '||
                v_extra_where_SQL ||
              'GROUP BY cd.item_id, cd.location_id, cd.sales_date';

            dynamic_ddl (sql_str);
	    dynamic_ddl('alter table RR_CON_D_SYNC_TEMP noparallel');		-- cmos 23-Feb-2017
            COMMIT;

	 -- cmos 28-Feb-2017 (px index creation)
         sql_str := 'CREATE INDEX RR_CON_D_SYNC_TEMP_idx ON RR_CON_D_SYNC_TEMP(item_id, location_id, sales_date) parallel 16 nologging';
         dynamic_ddl (sql_str);
	 dynamic_ddl ( 'alter index RR_CON_D_SYNC_TEMP_idx noparallel' );	-- cmos 28-Feb-2017
    COMMIT;


    V_STATUS      := 'Step 17 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

     --Merge DATA into SD
     -- cmos 23-Feb-2017 (use px index instead of fts)
     -- cmos 28-Feb-2017 (use px fts on merge)
            sql_str := 'MERGE /*+ enable_parallel_dml full(sd) parallel(sd,32) */ into sales_data sd
                        USING(SELECT /*+ parallel(32) */ * FROM RR_CON_D_SYNC_TEMP) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.sales_date)
                        WHEN MATCHED THEN
                        UPDATE SET
                        sd.ebspricelist104 = sd1.ebspricelist104,
                        sd.ebspricelist111 = sd1.ebspricelist111,
                        sd.ebspricelist112 = sd1.ebspricelist112,
                        sd.ebspricelist113 = sd1.ebspricelist113,
                        sd.ebspricelist114 = sd1.ebspricelist114,
                        sd.ebspricelist115 = sd1.ebspricelist115,
                        sd.ebspricelist116 = sd1.ebspricelist116,
                        sd.ebspricelist117 = sd1.ebspricelist117,
                        sd.ebspricelist118 = sd1.ebspricelist118,
                        sd.ebspricelist121 = sd1.ebspricelist121,
                        sd.ebspricelist122 = sd1.ebspricelist122,
                        sd.mkt_acc1 = sd1.mkt_acc1,
                        sd.mkt_acc3 = sd1.mkt_acc3,
                        sd.ff_input1 = sd1.ff_input1,
                        sd.cust_input3 = sd1.cust_input3,
                        sd.mkt_acc2 = sd1.mkt_acc2,
                        sd.last_update_date = sysdate
                        WHERE
                         nvl(sd.ebspricelist104,0) <> nvl(sd1.ebspricelist104,0) or
                        nvl(sd.ebspricelist111,0) <> nvl(sd1.ebspricelist111,0) or
                        nvl(sd.ebspricelist112,0) <> nvl(sd1.ebspricelist112,0) or
                        nvl(sd.ebspricelist113,0) <> nvl(sd1.ebspricelist113,0) or
                        nvl(sd.ebspricelist114,0) <> nvl(sd1.ebspricelist114,0) or
                        nvl(sd.ebspricelist115,0) <> nvl(sd1.ebspricelist115,0) or
                        nvl(sd.ebspricelist116,0) <> nvl(sd1.ebspricelist116,0) or
                        nvl(sd.ebspricelist117,0) <> nvl(sd1.ebspricelist117,0) or
                        nvl(sd.ebspricelist118,0) <> nvl(sd1.ebspricelist118,0) or
                        nvl(sd.ebspricelist121,0) <> nvl(sd1.ebspricelist121,0) or
                        nvl(sd.ebspricelist122,0) <> nvl(sd1.ebspricelist122,0) or
                        nvl(sd.mkt_acc1,0) <> nvl(sd1.mkt_acc1,0) or
                        nvl(sd.mkt_acc3,0) <> nvl(sd1.mkt_acc3,0) or
                        nvl(sd.ff_input1,0) <> nvl(sd1.ff_input1,0) or
                        nvl(sd.cust_input3,0) <> nvl(sd1.cust_input3,0) or
                        nvl(sd.mkt_acc2,0) <> nvl(sd1.mkt_acc2,0)
                        WHEN NOT MATCHED THEN
                        INSERT(item_id, location_id, sales_date, ebspricelist104, ebspricelist111, ebspricelist112, ebspricelist113,
                        ebspricelist114, ebspricelist115, ebspricelist116, ebspricelist117, ebspricelist118,
                        ebspricelist121, ebspricelist122, mkt_acc1, mkt_acc3, ff_input1, cust_input3,mkt_acc2, last_update_date
                        )
                        VALUES(sd1.item_id, sd1.location_id, sd1.sales_date, sd1.ebspricelist104, sd1.ebspricelist111, sd1.ebspricelist112, sd1.ebspricelist112,
                        sd1.ebspricelist114, sd1.ebspricelist115, sd1.ebspricelist116, sd1.ebspricelist117, sd1.ebspricelist118,
                        sd1.ebspricelist121, sd1.ebspricelist122, sd1.mkt_acc1, sd1.mkt_acc3, sd1.ff_input1, sd1.cust_input3, sd1.mkt_acc2, sysdate)';

      dynamic_ddl (sql_str);
      COMMIT;

------------------------------------------
-- Add in New Combinations
------------------------------------------

          V_STATUS      := 'Step 18 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

   dynamic_ddl ('Truncate TABLE mdp_load_assist');
   dynamic_ddl (
      'INSERT INTO mdp_load_assist(item_id, location_id)
                SELECT DISTINCT item_id, location_id FROM v_tmp_cont_data_miss_comb where new_comb = 1');

   COMMIT;

   select count(*) into v_mdp_assist from mdp_load_assist;

   if v_mdp_assist <> 0 then

      pkg_common_utils.prc_mdp_add ('mdp_load_assist', p_start_date, p_end_date);

   end if;




   ---Update Sync Date
    update sys_params
    set pval = TO_CHAR(v_new_run_date, 'mm-dd-yyyy hh24:mi:ss')
    WHERE pname = 'last_contract_sync';

    commit;

  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_sync_contract_to_sd;

PROCEDURE prc_sync_contract_to_sd_daily
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_SYNC_CONTRACT_TO_SD_DAILY';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

  PKG_CONTRACTS.PRC_SYNC_CONTRACT_TO_SD( SYSDATE-7, SYSDATE + 730, 0);

  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
end prc_sync_contract_to_sd_DAILY;

PROCEDURE prc_sync_contract_to_sd_WEEKLY
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_SYNC_CONTRACT_TO_SD_WEEKLY';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);

  PKG_CONTRACTS.PRC_SYNC_CONTRACT_TO_SD( SYSDATE-7, SYSDATE + 730, 1);		-- cmos 02-Mar-2017 (got changed again by Asahi)

  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  END;
END PRC_SYNC_CONTRACT_TO_SD_WEEKLY;


END;

/
