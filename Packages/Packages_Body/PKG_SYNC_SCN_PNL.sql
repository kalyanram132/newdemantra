--------------------------------------------------------
--  DDL for Package Body PKG_SYNC_SCN_PNL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_SYNC_SCN_PNL" 
AS
/******************************************************************************
   NAME:         PKG_SYNC_SCN_PNL  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version
   
   ******************************************************************************/

  PROCEDURE prc_get_scn_promos(p_scn_id IN NUMBER)
  AS
/******************************************************************************
   NAME:       PRC_GET_SCN_PROMOS
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        26/03/2013  Lakshmi Annapragada

******************************************************************************/

   v_prog_name   VARCHAR2 (100)   := 'PRC_GET_SCN_PROMOS';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;           
   guidelines_val          NUMBER (10);
   p2b_val                 NUMBER (10);
   
  BEGIN
   pre_logon;
  v_status      := 'start ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);   
  
   /* SELECT DISTINCT p.scenario_id, p.promotion_guidelines_id,
                               m.t_ep_p2b_ep_id
              INTO scenario_val, guidelines_val, p2b_val
                          FROM promotion p, promotion_data pd, mdp_matrix m
                         WHERE p.promotion_id = p_promo_id
                           AND pd.item_id = m.item_id
                           AND pd.location_id = m.location_id
                           AND p.promotion_id = pd.promotion_id;   */
      
    check_and_drop('rr_tmp_scn' || p_scn_id || 'promos_t');  
    
  /*  dynamic_ddl('CREATE TABLE rr_tmp_scn' || p_scn_id || 'promos_t AS    
    SELECT DISTINCT pd.promotion_id
                              FROM promotion_data pd, mdp_matrix m
                             WHERE pd.promotion_guidelines_id =
                                                    guidelines_val
                               AND m.t_ep_p2b_ep_id = p2b_val
                               AND pd.scenario_id = p_scn_id
                               AND pd.item_Id = m.item_id
                               AND pd.location_id = m.location_id') ;   */
       
       dynamic_ddl('CREATE TABLE rr_tmp_scn' || p_scn_id || 'promos_t AS    
                      SELECT  p.promotion_id
                        FROM promotion p
                       WHERE p.scenario_id = '||p_scn_id||'');
                             
                               
    COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END prc_get_scn_promos;
    

  
  PROCEDURE prc_fix_promo_type (p_scn_id NUMBER)
  AS
/******************************************************************************
   NAME:       PRC_FIX_PROMO_TYPE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
1.0        26/03/2013  Lakshmi Annapragada

******************************************************************************/

   v_prog_name   VARCHAR2 (100)   := 'PRC_FIX_PROMO_TYPE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
   mindate       DATE;
   maxdate       DATE;

   sql_str       VARCHAR2 (32000);   
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_FIX_PROMO_TYPE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   mindate := NEXT_DAY(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MONDAY') - 21;
   maxdate := mindate + 364;

   
   
   sql_str := 'CREATE TABLE rr_fix_promo_type_' || p_scn_id || '_t AS
               SELECT p.promotion_id, MAX(lock_cd_deal_type) lock_type,
               CASE WHEN MAX(pt.lock_cd_deal_type) = 0 
                    THEN MIN(pd.activity_type_id) 
                    ELSE MAX(CASE WHEN pt.lock_cd_deal_type = 1 THEN pd.activity_type_id ELSE 0 END)
               END promotion_type_id
               FROM rr_tmp_scn' || p_scn_id || 'promos_t p,
               promotion_dates pdt,
               promotion_data pd,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_id = pdt.promotion_id
               AND p.promotion_id = pd.promotion_id               
               AND pd.activity_type_id = pt.promotion_type_id
               AND pd.activity_type_id <> 0
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(maxdate, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(mindate, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               GROUP BY p.promotion_id';
   dynamic_ddl(sql_str);
   
   sql_str := 'MERGE INTO promotion p
               USING(SELECT fix.promotion_id, fix.promotion_type_id, fix.lock_type,
                     pt.cd_deal_type_id cd_deal_type, pt.coop_deal_type_id coop_deal_type, pt.oi_deal_type_id oi_deal_type
                     FROM rr_fix_promo_type_' || p_scn_id || '_t fix,
                     promotion_type pt
                     WHERE pt.promotion_type_id = fix.promotion_type_id
                     ) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN 
               UPDATE SET p.promotion_type_id = p1.promotion_type_id,
               p.cd_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.cd_deal_type ELSE p.cd_deal_type END,
               p.coop_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.coop_deal_type ELSE p.coop_deal_type END,
               p.oi_deal_type = CASE WHEN p1.lock_type = 1 THEN p1.oi_deal_type ELSE p.oi_deal_type END';
               
   dynamic_ddl(sql_str);
   COMMIT;
   
   sql_str := 'MERGE INTO promotion_matrix pm
               USING(SELECT promotion_id, promotion_type_id, lock_type
                     FROM rr_fix_promo_type_' || p_scn_id || '_t) pm1
               ON(pm.promotion_id = pm1.promotion_id)
               WHEN MATCHED THEN 
               UPDATE SET pm.promotion_type_id = pm1.promotion_type_id ';
               
   dynamic_ddl(sql_str);
   COMMIT;
   
   sql_str := 'MERGE INTO promotion_data pd
               USING(SELECT promotion_id, promotion_type_id, lock_type
                     FROM rr_fix_promo_type_' || p_scn_id || '_t) pd1
               ON(pd.promotion_id = pd1.promotion_id)
               WHEN MATCHED THEN 
               UPDATE SET pd.promotion_type_id = pd1.promotion_type_id,
               pd.activity_type_id = CASE WHEN pd1.lock_type = 1 THEN pd1.promotion_type_id ELSE pd.activity_type_id END';
               
   dynamic_ddl(sql_str);
   COMMIT;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_set_mdp_allocation(p_scn_id      NUMBER)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_SET_MDP_ALLOCATION
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   
   sql_str          VARCHAR2 (20000);
   
   p_start_date     DATE;
   p_end_date       DATE;
   
   v_day            VARCHAR2(10);
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_SET_MDP_ALLOCATION';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    

    
/*    UPDATE promotion_stat 
    SET rr_sync_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END,
    rr_sync_vol_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END;
    
    UPDATE promotion_type 
    SET rr_sync_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END,
    rr_sync_vol_pd = CASE WHEN rr_sync_sd = 1 OR rr_sync_ad = 1 THEN 1 ELSE 0 END; */
    
    COMMIT;
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';
    
    p_start_date := NEXT_DAY(SYSDATE - (26 *7), v_day) - 7;
    p_end_date := NEXT_DAY(SYSDATE + (104 *7), v_day) - 7;
    
/*    check_and_drop('rr_promo_cb_' || p_promo_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promo_cb_' || p_promo_id || '_t AS
               SELECT pm.item_id, pm.location_id 
               FROM promotion_matrix pm
               WHERE pm.promotion_id = ' || p_promotion_id;
   
   dynamic_ddl(sql_str);     */
   
   check_and_drop('rr_promos_' || p_scn_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promos_' || p_scn_id || '_t AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM rr_tmp_scn' || p_scn_id || 'promos_t p,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.promotion_id = pdt.promotion_id
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0               
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';    
    
    dynamic_ddl(sql_str);   
    COMMIT;
    
    sql_str := 'MERGE INTO promotion_matrix pm 
               USING(SELECT /*+ FULL(p) PARALLEL(p, 24) */ pm.promotion_id, pm.item_id, pm.location_id, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk0 IS NULL
                         THEN NVL(m.allocation_wk0, 0)
                         ELSE pm.rr_alloc_wk0
                         END
                    ELSE 1 END rr_alloc_wk0, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk1 IS NULL
                         THEN NVL(m.allocation_wk1, 0)
                         ELSE pm.rr_alloc_wk1
                         END
                    ELSE 0 END rr_alloc_wk1,
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk2 IS NULL
                         THEN NVL(m.allocation_wk2, 0) 
                         ELSE pm.rr_alloc_wk2
                         END
                    ELSE 0 END rr_alloc_wk2, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk3 IS NULL
                         THEN NVL(m.allocation_wk3, 0) 
                         ELSE pm.rr_alloc_wk3
                         END
                    ELSE 0 END rr_alloc_wk3, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk4 IS NULL
                         THEN NVL(m.allocation_wk4, 0) 
                         ELSE pm.rr_alloc_wk4
                         END
                    ELSE 0 END rr_alloc_wk4, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk5 IS NULL
                         THEN NVL(m.allocation_wk5, 0) 
                         ELSE pm.rr_alloc_wk5
                         END
                    ELSE 0 END rr_alloc_wk5, 
               CASE WHEN m.rr_accrual_offset = 1 AND pt.rr_margin_support = 0 THEN 
                    CASE WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_alloc_wk6 IS NULL
                         THEN NVL(m.allocation_wk6, 0) 
                         ELSE pm.rr_alloc_wk6
                         END
                    ELSE 0 END rr_alloc_wk6,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 1
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk0 IS NULL
                    THEN NVL(m.allocation_wk0, 0)
                    ELSE pm.rr_sync_wk0
                    END rr_sync_wk0,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk1 IS NULL
                    THEN NVL(m.allocation_wk1, 0)
                    ELSE pm.rr_sync_wk1
                    END rr_sync_wk1,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk2 IS NULL
                    THEN NVL(m.allocation_wk2, 0)
                    ELSE pm.rr_sync_wk2
                    END rr_sync_wk2,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk3 IS NULL
                    THEN NVL(m.allocation_wk3, 0)
                    ELSE pm.rr_sync_wk3
                    END rr_sync_wk3,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk4 IS NULL
                    THEN NVL(m.allocation_wk4, 0) 
                    ELSE pm.rr_sync_wk4
                    END rr_sync_wk4,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk5 IS NULL
                    THEN NVL(m.allocation_wk5, 0) 
                    ELSE pm.rr_sync_wk5
                    END rr_sync_wk5,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk6 IS NULL
                    THEN NVL(m.allocation_wk6, 0)
                    ELSE pm.rr_sync_wk6
                    END rr_sync_wk6,
               CASE WHEN pt.rr_margin_support = 1
                    THEN 0
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk6 IS NULL) AND NVL(m.allocation_wk6, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 6 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 6 END
                    WHEN pm.rr_sync_wk6 > 0
                    THEN 6
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk5 IS NULL) AND NVL(m.allocation_wk5, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 5 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 5 END
                    WHEN pm.rr_sync_wk5 > 0
                    THEN 5
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk4 IS NULL) AND NVL(m.allocation_wk4, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 4 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 4 END
                    WHEN pm.rr_sync_wk4 > 0
                    THEN 4
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk3 IS NULL) AND NVL(m.allocation_wk3, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 3 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 3 END
                    WHEN pm.rr_sync_wk3 > 0
                    THEN 3
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk2 IS NULL) AND NVL(m.allocation_wk2, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 2 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 2 END
                    WHEN pm.rr_sync_wk2 > 0
                    THEN 2
                    WHEN ((ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0) OR pm.rr_sync_wk1 IS NULL) AND NVL(m.allocation_wk1, 0) > 0
                    THEN CASE WHEN NVL(m.scan_forecast_offset, 0) + 1 > 6 THEN 6 ELSE NVL(m.scan_forecast_offset, 0) + 1 END
                    WHEN pm.rr_sync_wk1 > 0
                    THEN 1
                    ELSE 0
               END rr_promo_offset,
               NVL(latt6.rr_accrual_adjust, 0) rr_accrual_adjust
               FROM rr_promos_' || p_scn_id || '_t rpt, 
               promotion p,
               promotion_matrix pm,
               mdp_matrix m,
               promotion_stat ps,
               promotion_type pt,
               t_ep_l_att_6 latt6
               WHERE 1 = 1  
               AND rpt.promotion_id = p.promotion_id               
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id               
               --AND (ps.rr_lock_alloc_perc = 0 AND pt.rr_lock_alloc_perc = 0)
               /*AND (CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk0, 0) ELSE 1 END <> NVL(pm.rr_alloc_wk0, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk1, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk1, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk2, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk2, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk3, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk3, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk4, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk4, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk5, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk5, -99999)
                    OR CASE WHEN m.rr_accrual_offset = 1 THEN NVL(m.allocation_wk6, 0) ELSE 0 END  <> NVL(pm.rr_alloc_wk6, -99999)
                    OR NVL(m.allocation_wk0, 0) <> NVL(pm.rr_sync_wk0, -99999)
                    OR NVL(m.allocation_wk1, 0) <> NVL(pm.rr_sync_wk1, -99999)
                    OR NVL(m.allocation_wk2, 0) <> NVL(pm.rr_sync_wk2, -99999)
                    OR NVL(m.allocation_wk3, 0) <> NVL(pm.rr_sync_wk3, -99999)
                    OR NVL(m.allocation_wk4, 0) <> NVL(pm.rr_sync_wk4, -99999)
                    OR NVL(m.allocation_wk5, 0) <> NVL(pm.rr_sync_wk5, -99999)
                    OR NVL(m.allocation_wk6, 0) <> NVL(pm.rr_sync_wk6, -99999)
                    --OR NVL(latt6.rr_accrual_adjust, 0) <> NVL(pm.rr_accrual_adjust, -1)
                    )*/                    
               AND p.promotion_id = pm.promotion_id
               AND pm.item_id = m.item_id
               AND pm.location_id = m.location_id
               AND latt6.t_ep_l_att_6_ep_id = m.t_ep_l_att_6_ep_id) pm1
               ON(pm.promotion_id = pm1.promotion_id
               AND pm.item_id = pm1.item_id
               AND pm.location_id = pm1.location_id)
               WHEN MATCHED THEN 
               UPDATE SET pm.rr_alloc_wk0 = pm1.rr_alloc_wk0,
               pm.rr_alloc_wk1 = pm1.rr_alloc_wk1,
               pm.rr_alloc_wk2 = pm1.rr_alloc_wk2,
               pm.rr_alloc_wk3 = pm1.rr_alloc_wk3,
               pm.rr_alloc_wk4 = pm1.rr_alloc_wk4,
               pm.rr_alloc_wk5 = pm1.rr_alloc_wk5,
               pm.rr_alloc_wk6 = pm1.rr_alloc_wk6,
               pm.rr_sync_wk0 = pm1.rr_sync_wk0,
               pm.rr_sync_wk1 = pm1.rr_sync_wk1,
               pm.rr_sync_wk2 = pm1.rr_sync_wk2,
               pm.rr_sync_wk3 = pm1.rr_sync_wk3,
               pm.rr_sync_wk4 = pm1.rr_sync_wk4,
               pm.rr_sync_wk5 = pm1.rr_sync_wk5,
               pm.rr_sync_wk6 = pm1.rr_sync_wk6,
               --pm.rr_accrual_adjust = pm1.rr_accrual_adjust,
               pm.rr_promo_offset = pm1.rr_promo_offset
               WHERE NVL(pm.rr_alloc_wk0, 0) <> NVL(pm1.rr_alloc_wk0, -9999)
               OR NVL(pm.rr_alloc_wk1, 0) <> NVL(pm1.rr_alloc_wk1, -9999)
               OR NVL(pm.rr_alloc_wk2, 0) <> NVL(pm1.rr_alloc_wk2, -9999)
               OR NVL(pm.rr_alloc_wk3, 0) <> NVL(pm1.rr_alloc_wk3, -9999)
               OR NVL(pm.rr_alloc_wk4, 0) <> NVL(pm1.rr_alloc_wk4, -9999)
               OR NVL(pm.rr_alloc_wk5, 0) <> NVL(pm1.rr_alloc_wk5, -9999)
               OR NVL(pm.rr_alloc_wk6, 0) <> NVL(pm1.rr_alloc_wk6, -9999)
               OR NVL(pm.rr_sync_wk0, 0) <> NVL(pm1.rr_sync_wk0, -9999)
               OR NVL(pm.rr_sync_wk1, 0) <> NVL(pm1.rr_sync_wk1, -9999)
               OR NVL(pm.rr_sync_wk2, 0) <> NVL(pm1.rr_sync_wk2, -9999)
               OR NVL(pm.rr_sync_wk3, 0) <> NVL(pm1.rr_sync_wk3, -9999)
               OR NVL(pm.rr_sync_wk4, 0) <> NVL(pm1.rr_sync_wk4, -9999)
               OR NVL(pm.rr_sync_wk5, 0) <> NVL(pm1.rr_sync_wk5, -9999)
               OR NVL(pm.rr_sync_wk6, 0) <> NVL(pm1.rr_sync_wk6, -9999)      
               OR NVL(pm.rr_promo_offset, 0) <> NVL(pm1.rr_promo_offset, -9999)
               ';
   
    dynamic_ddl(sql_str);   
    COMMIT;  
  
    v_status := 'End ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END;
  
  PROCEDURE prc_calc_accrual(p_scn_id     NUMBER)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_CALC_ACCRUAL
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32767);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   
   v_eng_profile    NUMBER := 1;
   v_day            VARCHAR2(20);
   
   p_start_date     DATE;
   p_end_date       DATE;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_CALC_ACCRUAL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;    
    ---
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';
    
    p_start_date := NEXT_DAY(SYSDATE - (26 *7), v_day) - 7;
    p_end_date := NEXT_DAY(SYSDATE + (104 *7), v_day) - 7;
    
    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);
    
    ---    
    
 /*  check_and_drop('rr_promo_comb_' || p_scn_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promo_comb_' || p_scn_id || '_t AS
               SELECT pm.item_id, pm.location_id 
               FROM promotion_matrix pm
               WHERE pm.promotion_id = ' || p_promotion_id;
   
   dynamic_ddl(sql_str);   */
    
    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);
    
    ---
    
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    INTO max_sales_date 
    FROM DUAL;    
    
   check_and_drop('rr_promos_calc_' || p_scn_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promos_calc_' || p_scn_id || '_t AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               rr_tmp_scn' || p_scn_id || 'promos_t t,  
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_id = t.promotion_id
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.promotion_id = pdt.promotion_id 
               AND p.scenario_id <> 265
               AND pt.rr_margin_support = 0
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';    
    
    dynamic_ddl(sql_str);   
    COMMIT;
    
    sql_str := 'MERGE INTO promotion_data pd
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 24) */ pd.promotion_id, 
               pd.item_id, 
               pd.location_id, 
               pd.sales_date, 
               CASE 
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl,0)
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_base_pd,
               CASE 
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.incr_evt_vol_act, 0) 
                    ELSE NVL(pd.incr_evt_vol_or, NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_incr_pd,
               CASE WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    ELSE NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_eng_incr_pd,               
               CASE   
                    WHEN ps.rr_sync_vol_ad = 0 OR pt.rr_sync_vol_ad = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) 
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or, NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) rr_accrual_vol_pd,
               CASE 
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0)
               END rr_supply_vol_base_pd,
               CASE 
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END rr_supply_vol_incr_pd,
               CASE WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    ELSE NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))
               END rr_supply_eng_incr_pd,               
               CASE 
                    WHEN ps.rr_sync_vol_sd = 0 OR pt.rr_sync_vol_sd = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END rr_supply_vol_pd,
               CASE 
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1)* (NVL(pd.case_buydown,0)) rr_pl_cd_pd,
               CASE 
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) * NVL(pd.units, 0) * (NVL(pd.rr_handling_d, 0)) rr_pl_hf_cd_pd,               
               CASE 
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0 ELSE NVL(pd.event_cost,0)
               END rr_pl_coop_pd,
               CASE 
                    WHEN ps.rr_sync_sd = 0 OR pt.rr_sync_sd = 0
                    THEN 0 ELSE NVL(pd.rr_handling_coop, 0)
               END rr_pl_hf_coop_pd,               
               CASE 
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1)* (NVL(pd.case_buydown,0)) rr_accrual_cd_pd,
               CASE 
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0
                    WHEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0) <> 0 AND NVL(latt6.rr_accrual_adjust, 0) = 0
                    THEN NVL(pd.evt_vol_act, 0) + NVL(pd.incr_evt_vol_act, 0)
                    ELSE NVL(pd.volume_base_ttl, 0) +  NVL(pd.incr_evt_vol_or,  NVL(pd.rr_accrual_eng_incr_commit, NVL(pd.fore_5_uplift, nvl(pd.'|| fore_column || '_uplift,0))))
               END * NVL(pd.rr_redemp_p, 1) * NVL(pd.units, 0) * (NVL(pd.rr_handling_d, 0)) rr_accrual_hf_cd_pd,               
               CASE 
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0 ELSE NVL(pd.event_cost,0)
               END rr_accrual_coop_pd,
               CASE 
                    WHEN ps.rr_sync_ad = 0 OR pt.rr_sync_ad = 0
                    THEN 0 
                    ELSE NVL(pd.rr_handling_coop, 0)
               END rr_accrual_hf_coop_pd
               FROM promotion p, promotion_data pd, promotion_matrix pm, mdp_matrix m, rr_promos_calc_' || p_scn_id || '_t rpt, 
               promotion_stat ps, promotion_type pt,
               t_ep_l_att_6 latt6
               WHERE 1 = 1
               AND pd.promotion_type_id = pt.promotion_type_id
               AND pd.promotion_stat_id = ps.promotion_stat_id
               AND p.promotion_id = rpt.promotion_id
               AND p.promotion_id = pm.promotion_id
               AND pd.promotion_id = pm.promotion_id
               AND pd.item_id = pm.item_id
               AND pd.location_id = pm.location_id
               AND pm.item_id = m.item_id
               AND pm.location_id = m.location_id
               AND latt6.t_ep_l_att_6_ep_id = m.t_ep_l_att_6_ep_id
               AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') ||''', ''DD/MM/YYYY'')) pd1
               ON(pd.promotion_id = pd1.promotion_id
               AND pd.item_id = pd1.item_id
               AND pd.location_id = pd1.location_id
               AND pd.sales_date = pd1.sales_date)
               WHEN MATCHED THEN 
               UPDATE SET pd.rr_supply_vol_base_pd = NVL(pd1.rr_supply_vol_base_pd, 0),
               pd.rr_supply_vol_incr_pd = NVL(pd1.rr_supply_vol_incr_pd, 0),
               pd.rr_supply_eng_incr_pd = NVL(pd1.rr_supply_eng_incr_pd, 0),
               pd.rr_supply_vol_pd = NVL(pd1.rr_supply_vol_pd, 0),
               pd.rr_accrual_vol_base_pd = NVL(pd1.rr_accrual_vol_base_pd, 0),
               pd.rr_accrual_vol_incr_pd = NVL(pd1.rr_accrual_vol_incr_pd, 0),
               pd.rr_accrual_eng_incr_pd = NVL(pd1.rr_accrual_eng_incr_pd, 0),
               pd.rr_accrual_vol_pd = NVL(pd1.rr_accrual_vol_pd, 0),
               pd.rr_accrual_cd_pd = NVL(pd1.rr_accrual_cd_pd, 0),
               pd.rr_accrual_coop_pd = NVL(pd1.rr_accrual_coop_pd, 0),
               pd.rr_accrual_hf_cd_pd = NVL(pd1.rr_accrual_hf_cd_pd, 0),
               pd.rr_accrual_hf_coop_pd = NVL(pd1.rr_accrual_hf_coop_pd, 0),               
               pd.rr_pl_cd_pd = NVL(pd1.rr_pl_cd_pd, 0),
               pd.rr_pl_coop_pd = NVL(pd1.rr_pl_coop_pd, 0),
               pd.rr_pl_hf_cd_pd = NVL(pd1.rr_pl_hf_cd_pd, 0),
               pd.rr_pl_hf_coop_pd = NVL(pd1.rr_pl_hf_coop_pd, 0),
               pd.last_update_date = SYSDATE
               WHERE 1 = 1
               OR NVL(pd.rr_supply_vol_base_pd, 0) <> NVL(pd1.rr_supply_vol_base_pd, 0)
               OR NVL(pd.rr_supply_vol_incr_pd, 0) <> NVL(pd1.rr_supply_vol_incr_pd, 0)
               OR NVL(pd.rr_supply_eng_incr_pd, 0) <> NVL(pd1.rr_supply_eng_incr_pd, 0)
               OR NVL(pd.rr_supply_vol_pd, 0) <> NVL(pd1.rr_supply_vol_pd, 0)
               OR NVL(pd.rr_accrual_vol_base_pd, 0) <> NVL(pd1.rr_accrual_vol_base_pd, 0)
               OR NVL(pd.rr_accrual_vol_incr_pd, 0) <> NVL(pd1.rr_accrual_vol_incr_pd, 0)
               OR NVL(pd.rr_accrual_eng_incr_pd, 0) <> NVL(pd1.rr_accrual_eng_incr_pd, 0)
               OR NVL(pd.rr_accrual_vol_pd, 0) <> NVL(pd1.rr_accrual_vol_pd, 0)
               OR NVL(pd.rr_accrual_cd_pd, 0) <> NVL(pd1.rr_accrual_cd_pd, 0)
               OR NVL(pd.rr_accrual_coop_pd, 0) <> NVL(pd1.rr_accrual_coop_pd, 0)
               OR NVL(pd.rr_pl_cd_pd, 0) <> NVL(pd1.rr_pl_cd_pd, 0)
               OR NVL(pd.rr_pl_coop_pd, 0) <> NVL(pd1.rr_pl_coop_pd, 0)   
               OR NVL(pd.rr_accrual_hf_cd_pd, 0) <> NVL(pd1.rr_accrual_hf_cd_pd, 0)
               OR NVL(pd.rr_accrual_hf_coop_pd, 0) <> NVL(pd1.rr_accrual_hf_coop_pd, 0)
               OR NVL(pd.rr_pl_hf_cd_pd, 0) <> NVL(pd1.rr_pl_hf_cd_pd, 0)
               OR NVL(pd.rr_pl_hf_coop_pd, 0) <> NVL(pd1.rr_pl_hf_coop_pd, 0)';
   
    --dbms_output.put_line(sql_str);
    dynamic_ddl(sql_str);   
    COMMIT;
        ---
    
    sql_str := 'MERGE INTO promotion_matrix pm
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 24) */ pd.promotion_id, 
               --pd.item_id, pd.location_id, 
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_calc_' || p_scn_id || '_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id
               --, pd.item_id, pd.location_id
               ) pm1
               ON(pm.promotion_id = pm1.promotion_id
               --AND pm.item_id = pm1.item_id
               --AND pm.location_id = pm1.location_id
               )
               WHEN MATCHED THEN 
               UPDATE SET pm.promotion_stat_id = pm1.promotion_stat_id, pm.accruals_type_id = pm1.accruals_type_id,
               pm.promotion_data_lud = SYSDATE
               ';
    
    dynamic_ddl(sql_str);   
    COMMIT;            
    ---
    
    sql_str := 'MERGE INTO promotion p
               USING(SELECT /*+ FULL(rpt) PARALLEL(rpt, 24) */ pd.promotion_id, 
               MAX(pd.promotion_stat_id) promotion_stat_id, MAX(pd.accruals_type_id) accruals_type_id
               FROM promotion_data pd,
               rr_promos_calc_' || p_scn_id || '_t rpt
               WHERE rpt.promotion_id = pd.promotion_id
               GROUP BY pd.promotion_id) p1
               ON(p.promotion_id = p1.promotion_id)
               WHEN MATCHED THEN 
               UPDATE SET p.promotion_stat_id = p1.promotion_stat_id,
               p.accruals_type_id = p1.accruals_type_id
               ';
    
    dynamic_ddl(sql_str);   
    COMMIT;    
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END;
    
    PROCEDURE prc_promo_accrual_sync(p_scn_id      NUMBER,
                                     p_sync_flag     NUMBER DEFAULT 3,
                                     p_sync_ms       NUMBER DEFAULT 0)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PROMO_ACCRUAL_SYNC
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   p_sales_sync_rule  VARCHAR2(4000);
   p_alloc_sync_rule  VARCHAR2(4000);
   
   p_start_date     DATE;
   p_end_date     DATE;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_PROMO_ACCRUAL_SYNC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    v_status := 'Step 1 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;    
    ---
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';
    
    /* SELECT NEXT_DAY(from_date, v_day) - 7, NEXT_DAY(until_date, v_day) - 7
    INTO p_start_date, p_end_date
    FROM promotion_dates 
    WHERE promotion_id = p_promotion_id;*/
    
    p_start_date := NEXT_DAY(SYSDATE - (26 *7), v_day) - 7;
    p_end_date := NEXT_DAY(SYSDATE + (104 *7), v_day) - 7;
    
    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);
    
    ---  
    
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    INTO max_sales_date 
    FROM DUAL;
    
    p_alloc_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.alloc_wk6 WHEN -35 THEN rspt.alloc_wk5  WHEN -28 THEN rspt.alloc_wk4 WHEN -21 THEN rspt.alloc_wk3 WHEN -14 THEN rspt.alloc_wk2 WHEN -7 THEN rspt.alloc_wk1 WHEN 0 THEN rspt.alloc_wk0 END ';
    p_sales_sync_rule := ' CASE i.datet - rspt.sales_date WHEN -42 THEN rspt.sync_wk6 WHEN -35 THEN rspt.sync_wk5 WHEN -28 THEN rspt.sync_wk4 WHEN -21 THEN rspt.sync_wk3 WHEN -14 THEN rspt.sync_wk2 WHEN -7 THEN rspt.sync_wk1 WHEN 0 THEN rspt.sync_wk0 END ';

   
/*   check_and_drop('rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t');
   
   
   sql_str := 'CREATE TABLE rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t AS
               SELECT pm.item_id, pm.location_id 
               FROM promotion_matrix pm
               WHERE pm.promotion_id = ' || p_promotion_id;
   
   dynamic_ddl(sql_str);
  */ 
    v_status := 'Step 2 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   check_and_drop('rr_promos_sync_' || p_sync_ms || '_' || p_scn_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promos_sync_' || p_sync_ms  || '_' || p_scn_id || '_t AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id, 
               CASE WHEN TO_CHAR(pdt.from_date, ''DAY'') = ''' || v_day || ''' AND MOD((TRUNC(pdt.until_date, ''DD'') - TRUNC(pdt.from_date, ''DD'')) + 1, 7) = 0 THEN 1 
                    ELSE 1 
               END rr_start_day, 
               CASE WHEN pt.rr_sync_sd = 1 AND pt.rr_sync_sd = 1 THEN 1 ELSE 0 END rr_sync_sd,
               CASE WHEN pt.rr_sync_ad = 1 AND pt.rr_sync_ad = 1 THEN 1 ELSE 0 END rr_sync_ad,
               CASE WHEN pt.rr_sync_vol_sd = 1 AND pt.rr_sync_vol_sd = 1 THEN 1 ELSE 0 END rr_sync_vol_sd,
               CASE WHEN pt.rr_sync_vol_ad = 1 AND pt.rr_sync_vol_ad = 1 THEN 1 ELSE 0 END rr_sync_vol_ad,
               p.promotion_type_id,
               p.promotion_stat_id,
               p.cd_deal_type,
               p.coop_deal_type,
               p.oi_deal_type
               FROM promotion p,
               rr_tmp_scn' || p_scn_id || 'promos_t t,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_id = t.promotion_id
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id <> 265
               AND pt.rr_margin_support = ' || p_sync_ms || '
               AND (1 = 2 ' || 
               CASE WHEN p_sync_flag IN (1, 3) THEN '               
               OR (ps.rr_sync_sd = 1 AND pt.rr_sync_sd = 1)
               ' END ||
               CASE WHEN p_sync_flag IN (2, 3) THEN '
               OR (ps.rr_sync_ad = 1 AND pt.rr_sync_ad = 1)
               ' END || '
               )
               AND p.promotion_id = pdt.promotion_id
               AND (pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
               OR pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''))
               AND EXISTS(SELECT 1 FROM promotion_matrix pm, rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
                          WHERE pm.item_id = rct.item_id AND pm.location_id = rct.location_id
                          AND pm.promotion_id = p.promotion_id)';    
    
    dynamic_ddl (sql_str);
    COMMIT;
    
    v_status := 'Step 3 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);    
   
    check_and_drop('rr_sync_promo_' || p_sync_ms || '_' || p_scn_id || '_t');
    
    sql_str :=('CREATE TABLE rr_sync_promo_' || p_sync_ms || '_' || p_scn_id || '_t(promotion_id, item_id, location_id, sales_date, rr_sync_sd, rr_sync_ad, promotion_type_id, promotion_stat_id,
                                            promo_price, case_buydown, case_oi, rr_oi_p, base_plan, incr_plan, engine_incr, accrual_vol,                                             
                                            sup_base_plan, sup_incr_plan, sup_engine_incr, supply_vol, pl_cd, pl_coop,  pl_hf_cd, pl_hf_coop, 
                                            accrual_cd, accrual_coop, accrual_hf_cd, accrual_hf_coop,                                            
                                            alloc_wk0, alloc_wk1, alloc_wk2, alloc_wk3, alloc_wk4, alloc_wk5,alloc_wk6, 
                                            sync_wk0, sync_wk1, sync_wk2, sync_wk3, sync_wk4, sync_wk5, sync_wk6, rr_promo_offset, rr_ms_vol) TABLESPACE ts_source
         AS SELECT /*+ full(rpt) parallel(rpt,24)  */  
              pd.promotion_id,
              pd.item_id,
              pd.location_id,
              pd.sales_date, 
              rpt.rr_sync_sd,
              rpt.rr_sync_ad,
              pd.promotion_type_id,
              pd.promotion_stat_id,
              NVL(pd.promo_price,0) promo_price,
              NVL(pd.case_buydown,0) case_buydown    ,
              NVL(pd.case_buydownoi,0) case_oi,
              pd.rr_oi_p rr_oi_p,
              pd.rr_accrual_vol_base_pd base_plan,
              pd.rr_accrual_vol_incr_pd incr_plan,
              pd.rr_accrual_eng_incr_pd engine_incr,
              pd.rr_accrual_vol_pd accrual_vol,              
              pd.rr_supply_vol_base_pd sup_base_plan,
              pd.rr_supply_vol_incr_pd sup_incr_plan,
              pd.rr_supply_eng_incr_pd sup_engine_incr,
              pd.rr_supply_vol_pd supply_vol, 
              pd.rr_pl_cd_pd pl_cd,
              pd.rr_pl_coop_pd pl_coop,
              pd.rr_pl_hf_cd_pd pl_hf_cd,
              pd.rr_pl_hf_coop_pd pl_hf_coop,                           
              pd.rr_accrual_cd_pd accrual_cd,
              pd.rr_accrual_coop_pd accrual_coop,
              pd.rr_accrual_hf_cd_pd accrual_hf_cd,
              pd.rr_accrual_hf_coop_pd accrual_hf_coop,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_alloc_wk0 END alloc_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk1 END alloc_wk1,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk2 END alloc_wk2,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk3 END alloc_wk3,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk4 END alloc_wk4,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk5 END alloc_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_alloc_wk6 END alloc_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 1 ELSE pm.rr_sync_wk0 END sync_wk0,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk1 END sync_wk1,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk2 END sync_wk2,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk3 END sync_wk3,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk4 END sync_wk4,
                          CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk5 END sync_wk5,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_sync_wk6 END sync_wk6,
              CASE WHEN rpt.rr_start_day <> 1 THEN 0 ELSE pm.rr_promo_offset END rr_promo_offset,
              NVL(pd.rr_ms_vol, 0) rr_ms_vol
            FROM  promotion_data pd,
                  promotion p,
                  promotion_matrix pm,
                  rr_promos_sync_' || p_sync_ms || '_' || p_scn_id || '_t rpt,
                  rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
            WHERE 1 = 1
            AND     pm.promotion_id         = p.promotion_id
            AND   p.scenario_id     <>  265
            AND     pd.promotion_id         = pm.promotion_id
            AND   pd.item_id                 = pm.item_id
            AND   pd.location_id         = pm.location_id
            AND   pm.item_id = rct.item_id
            AND   pm.location_id = rct.location_id
            AND   pd.sales_date             >= TO_DATE('''|| TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   pd.sales_date             <= TO_DATE('''|| TO_CHAR(v_max, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
            AND   p.promotion_id    = rpt.promotion_id
            '
                );
       ---        
    dynamic_ddl (sql_str);
    COMMIT;
    
    v_status := 'Step 4 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    check_and_drop('rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t');
    
    sql_str :=('CREATE TABLE rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t(promotion_id,
                item_id, location_id, sales_date, plan_date, rr_sync_sd, rr_sync_ad, promotion_type_id, promotion_stat_id, promo_price,
                case_buydown,    case_oi, rr_oi_p,
                base_plan, incr_plan, engine_incr,    accrual_vol,                 
                sup_base_plan, sup_incr_plan, sup_engine_incr,    supply_vol, 
                accrual_cd, accrual_coop, accrual_hf_cd, accrual_hf_coop,                 
                base_plan_a, incr_plan_a, engine_incr_a, accrual_vol_a,                 
                accrual_cd_a, accrual_coop_a, accrual_hf_cd_a, accrual_hf_coop_a,                                 
                base_plan_s, incr_plan_s, engine_incr_s, accrual_vol_s, accrual_cd_s, accrual_coop_s, accrual_hf_cd_s, accrual_hf_coop_s, rr_ms_vol)
         AS SELECT /*+ full(rspt) parallel(rspt,24)  */ rspt.promotion_id,
            rspt.item_id, rspt.location_id, rspt.sales_date, i.datet  plan_date, rspt.rr_sync_sd, rspt.rr_sync_ad, rspt.promotion_type_id,
            rspt.promotion_stat_id, rspt.promo_price, rspt.case_buydown, rspt.case_oi, rspt.rr_oi_p,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.base_plan base_plan,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.incr_plan incr_plan,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.engine_incr engine_incr,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_vol accrual_vol,          
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_base_plan sup_base_plan,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_incr_plan sup_incr_plan,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.sup_engine_incr sup_engine_incr,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.supply_vol supply_vol,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_cd accrual_cd,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_coop accrual_coop,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_cd accrual_hf_cd,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.accrual_hf_coop accrual_hf_coop,
            ' || p_alloc_sync_rule || ' * rspt.base_plan base_plan_a,
            ' || p_alloc_sync_rule || ' * rspt.incr_plan incr_plan_a,
            ' || p_alloc_sync_rule || ' * rspt.engine_incr engine_incr_a,
            ' || p_alloc_sync_rule || ' * rspt.accrual_vol accrual_vol_a,
            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_cd END accrual_cd_a,
            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_coop END accrual_coop_a,
            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_cd END accrual_hf_cd_a,
            ' || p_alloc_sync_rule || ' * CASE WHEN rspt.rr_sync_ad <> 1 THEN 0 ELSE rspt.accrual_hf_coop END accrual_hf_coop_a,    
            ' || p_sales_sync_rule || ' * rspt.sup_base_plan base_plan_s,
            ' || p_sales_sync_rule || ' * rspt.sup_incr_plan incr_plan_s,
            ' || p_sales_sync_rule || ' * rspt.sup_engine_incr engine_incr_s,
            ' || p_sales_sync_rule || ' * rspt.supply_vol accrual_vol_s,
            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_cd END accrual_cd_s,
            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_coop END accrual_coop_s,
            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_cd END accrual_hf_cd_s,
            ' || p_sales_sync_rule || ' * CASE WHEN rspt.rr_sync_sd <> 1 THEN 0 ELSE rspt.pl_hf_coop END accrual_hf_coop_s,
            CASE i.datet - rspt.sales_date WHEN 0 THEN 1 ELSE 0 END * rspt.rr_ms_vol rr_ms_vol
         FROM rr_sync_promo_' || p_sync_ms || '_' || p_scn_id || '_t rspt,
         inputs i
         WHERE i.datet BETWEEN rspt.sales_date - (rspt.rr_promo_offset * 7) AND rspt.sales_date');
    
    dynamic_ddl (sql_str);
    COMMIT;
    
    v_status := 'Step 5 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    sql_str := 'CREATE INDEX rr_sync_promo_off_' || p_sync_ms || '_' || p_scn_id || '_i ON rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t(promotion_id, item_id, location_id, sales_date)';
    dynamic_ddl (sql_str);
    COMMIT;    
    
    v_status := 'Step 6 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    IF p_sync_flag IN (1, 3) THEN
    
         IF p_sync_ms = 0 THEN 
                                        
               
          sql_str:= 'MERGE INTO sales_data sd
               USING(SELECT /*+ full(rct) parallel(rct,24) */ sd.item_id, sd.location_id, sd.sales_date 
                     FROM sales_data sd,
                          rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
                    WHERE sd.sales_date >= TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                    AND sd.sales_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                    AND rct.item_id = sd.item_id
                    AND rct.location_id = sd.location_id
                    AND (   1 = 2
                       OR sd.acc_sales IS NOT NULL
                       OR sd.offset_engine_incr IS NOT NULL
                       OR sd.ebspricelist106 IS NOT NULL
                       OR sd.ebspricelist107 IS NOT NULL
                       OR sd.ebspricelist103 IS NOT NULL
                       OR sd.ebspricelist104 IS NOT NULL
                       OR sd.actual_con_fcst IS NOT NULL
                       OR sd.actuals_ttl IS NOT NULL
                       OR sd.act_part_ord IS NOT NULL
                       OR sd.avail_stock IS NOT NULL
                       OR sd.act_sup IS NOT NULL
                       OR sd.actuals_ttl_dol IS NOT NULL
                       OR sd.advert IS NOT NULL
                       OR sd.aop IS NOT NULL
                       OR sd.a_vol_fcst IS NOT NULL
                       OR sd.backlog IS NOT NULL
                      ))sd1
                ON(sd.item_id = sd1.item_id
                AND sd.location_id = sd1.location_id
                AND sd.sales_date = sd1.sales_date)
                WHEN MATCHED THEN 
                UPDATE SET sd.acc_sales = NULL,
                  sd.offset_engine_incr = NULL,
                  sd.ebspricelist106 = NULL,
                  sd.ebspricelist107 = NULL,
                  sd.ebspricelist103 = NULL,
                  sd.ebspricelist104 = NULL,
                  sd.actual_con_fcst = NULL,
                  sd.actuals_ttl = NULL,
                  sd.act_part_ord = NULL,
                  sd.avail_stock = NULL,
                  sd.act_sup = NULL,
                  sd.actuals_ttl_dol = NULL,
                  sd.advert = NULL,
                  sd.aop = NULL,
                  sd.a_vol_fcst = NULL,
                  sd.backlog = NULL,
                  sd.last_update_date = SYSDATE';
      
            dynamic_ddl(sql_str);
            COMMIT;
            
            v_status := 'Step 7 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
          
            sql_str := 'MERGE INTO sales_data sd 
                        USING(SELECT sd.item_id,
                               sd.location_id,
                               sd.plan_date,
                               MAX(NVL(sd.promo_price, 0)) promo_price,
                               MAX(NVL(sd.case_buydown, 0)) case_buydown,     
                               SUM(sd.base_plan_s) base_plan,
                               SUM(sd.incr_plan_s) incr_vol,
                               SUM(sd.engine_incr_s) engine_vol,    
                               SUM(sd.sup_incr_plan) incr_plan_non_offset,
                               SUM(sd.sup_engine_incr) engine_incr_non_offset,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.case_oi ELSE NULL END, 0)) pl_6_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_6_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 6 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_6_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 6 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_6_accrual_coop,    
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.case_oi ELSE NULL END, 0)) pl_13_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_13_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 13 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_13_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 13 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_13_accrual_coop,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.case_oi ELSE NULL END, 0)) pl_17_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_17_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 17 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_17_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 17 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_17_accrual_coop,
                               SUM (NVL(sd.accrual_hf_cd_s, 0)) pl_17_accrual_hf_cd,
                               SUM (NVL(sd.accrual_hf_coop_s, 0)) pl_13_accrual_hf_coop
                        FROM rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t sd,
                             mdp_matrix          mdp,
                             promotion p,
                             tbl_cd_deal_type cdt,
                             tbl_coop_deal_type coopt,
                             tbl_oi_deal_type oit,
                             rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
                        WHERE sd.item_id                   = mdp.item_id
                        AND   sd.location_id               = mdp.location_id
                        AND   rct.item_id                   = mdp.item_id
                        AND   rct.location_id               = mdp.location_id
                        AND   sd.promotion_id       = p.promotion_id
                        AND   NVL(p.cd_deal_type, 0)        = cdt.cd_deal_type_id
                        AND   NVL(p.coop_deal_type, 0)      = coopt.coop_deal_type_id
                        AND   NVL(p.oi_deal_type, 0)        = oit.oi_deal_type_id
                        AND   sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
                        AND   sd.rr_sync_sd         = 1
                        GROUP BY sd.item_id,sd.location_id,sd.plan_date) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET actuals_incr = sd1.incr_plan_non_offset,
                        acc_sales = sd1.incr_vol,
                        offset_engine_incr = sd1.engine_vol,
                        actuals_ttl_dol = sd1.pl_6_case_oi,
                        act_sup = sd1.pl_6_oi_p,
                        advert = sd1.pl_6_accrual_cd,
                        aop = sd1.pl_6_accrual_coop,
                        actuals_ttl = sd1.pl_13_case_oi,
                        actual_con_fcst = sd1.pl_13_oi_p,
                        act_part_ord = sd1.pl_13_accrual_cd,
                        avail_stock = sd1.pl_13_accrual_coop,
                        ebspricelist104 = sd1.pl_17_case_oi,
                        ebspricelist103 = sd1.pl_17_oi_p,                  
                        ebspricelist106 = sd1.pl_17_accrual_cd,
                        ebspricelist107 = sd1.pl_17_accrual_coop,
                        a_vol_fcst = sd1.pl_17_accrual_hf_cd,
                        backlog = sd1.pl_13_accrual_hf_coop,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN 
                        INSERT(item_id, location_id, sales_date, actuals_incr, acc_sales, offset_engine_incr, actuals_ttl_dol, act_sup, advert, aop,
                               actuals_ttl, actual_con_fcst, act_part_ord, avail_stock, ebspricelist104, ebspricelist103, ebspricelist106, ebspricelist107,
                               a_vol_fcst, backlog, last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.incr_plan_non_offset, sd1.incr_vol, sd1.engine_vol, sd1.pl_6_case_oi, sd1.pl_6_oi_p,
                               sd1.pl_6_accrual_cd, sd1.pl_6_accrual_coop, sd1.pl_13_case_oi, sd1.pl_13_oi_p, sd1.pl_13_accrual_cd, sd1.pl_13_accrual_coop,
                               sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,
                               sd1.pl_17_accrual_hf_cd, sd1.pl_13_accrual_hf_coop, SYSDATE )';
          
            dynamic_ddl (sql_str);
            COMMIT;
            
            v_status := 'Step 8 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
            
            sql_str := 'MERGE INTO sales_data sd
      USING (SELECT /*+ full(rct) parallel(rct,24) */
             sd.item_id, 
             sd.location_id, 
             sd.sales_date, 
             sd.sales_date  - (NVL(mdp.scan_forecast_offset, 0) * 7) plan_date,
             NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, NVL(sd.npi_forecast, 0)))) *(1.00 + NVL(sd.manual_fact,0)) beg_inv, 
             NVL(sd.' || fore_column || '*1, 0) engine_base,
             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price
             FROM sales_data sd, 
             mdp_matrix mdp,
             rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id    
             AND rct.item_id = mdp.item_id
             AND rct.location_id = mdp.location_id
             AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') 
                               AND TO_DATE(''' || TO_CHAR(v_max, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date)
      WHEN MATCHED THEN
         UPDATE
         SET beg_inv = sd1.beg_inv,
             engine_base = sd1.engine_base
         WHERE NVL(sd.beg_inv, 0) <> NVL(sd1.beg_inv, 0)
         OR NVL(sd.engine_base, 0) <>  NVL(sd1.engine_base, 0)        
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, beg_inv, engine_base)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.beg_inv, sd1.engine_base)
         WHERE ABS(NVL(sd1.beg_inv, 0)) + ABS(NVL(sd1.engine_base, 0))  <> 0';
         
         
         --   dynamic_ddl (sql_str);
            COMMIT;
            
            sql_str := 'MERGE INTO MDP_MATRIX mdp
                        USING(SELECT item_id, location_id 
                              FROM rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t) mdp1
                        ON(mdp.item_id = mdp1.item_id
                        AND mdp.location_id = mdp1.location_id)
                        WHEN MATCHED THEN 
                        UPDATE SET last_update_date = SYSDATE, sales_data_lud = SYSDATE';
            
            dynamic_ddl (sql_str);
            COMMIT;
            
      ELSE 
      
            sql_str := 'MERGE INTO sales_data sd 
                        USING(SELECT sd.item_id,
                               sd.location_id,
                               sd.plan_date,
                               MAX(NVL(sd.promo_price, 0)) promo_price,
                               MAX(NVL(sd.case_buydown, 0)) case_buydown,     
                               SUM(sd.base_plan_s) base_plan,
                               SUM(sd.incr_plan_s) incr_vol,
                               SUM(sd.engine_incr_s) engine_vol,    
                               SUM(sd.sup_incr_plan) incr_plan_non_offset,
                               SUM(sd.sup_engine_incr) engine_incr_non_offset,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.case_oi ELSE NULL END, 0)) pl_6_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 6 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_6_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 6 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_6_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 6 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_6_accrual_coop,    
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.case_oi ELSE NULL END, 0)) pl_13_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 13 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_13_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 13 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_13_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 13 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_13_accrual_coop,
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.case_oi ELSE NULL END, 0)) pl_17_case_oi,                                            
                               MAX (NVL(CASE WHEN oit.demantra_pl = 17 THEN sd.rr_oi_p ELSE NULL END, 0)) pl_17_oi_p,
                               SUM (NVL(CASE WHEN cdt.demantra_pl = 17 THEN sd.accrual_cd_s ELSE NULL END, 0)) pl_17_accrual_cd,
                               SUM (NVL(CASE WHEN coopt.demantra_pl = 17 THEN sd.accrual_coop_s ELSE NULL END, 0)) pl_17_accrual_coop,
                               SUM (NVL(sd.accrual_hf_cd_s, 0)) pl_17_accrual_hf_cd,
                               SUM (NVL(sd.accrual_hf_coop_s, 0)) pl_13_accrual_hf_coop
                        FROM rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t sd,
                             mdp_matrix          mdp,
                             promotion p,
                             tbl_cd_deal_type cdt,
                             tbl_coop_deal_type coopt,
                             tbl_oi_deal_type oit,
                             rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
                        WHERE 1 = 1
                        AND   sd.item_id                   = mdp.item_id
                        AND   sd.location_id               = mdp.location_id
                        AND   rct.item_id                   = mdp.item_id
                        AND   rct.location_id               = mdp.location_id
                        AND   sd.promotion_id       = p.promotion_id
                        AND   p.cd_deal_type        = cdt.cd_deal_type_id
                        AND   p.coop_deal_type      = coopt.coop_deal_type_id
                        AND   p.oi_deal_type        = oit.oi_deal_type_id
                        AND   sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
                        AND   sd.rr_sync_sd         = 1
                        GROUP BY sd.item_id,sd.location_id,sd.plan_date) sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.plan_date)
                        WHEN MATCHED THEN
                        UPDATE SET 
                        actuals_ttl = GREATEST(NVL(sd.actuals_ttl, 0), NVL(sd1.pl_13_case_oi, 0)),
                        actual_con_fcst = GREATEST(NVL(sd.actual_con_fcst, 0), NVL(sd1.pl_13_oi_p, 0)),
                        act_part_ord = NVL(sd.act_part_ord, 0) + sd1.pl_13_accrual_cd,
                        avail_stock = NVL(sd.avail_stock, 0) + sd1.pl_13_accrual_coop,
                        ebspricelist104 = GREATEST(NVL(sd.ebspricelist104, 0), NVL(sd1.pl_17_case_oi, 0)),
                        ebspricelist103 = GREATEST(NVL(sd.ebspricelist103, 0), NVL(sd1.pl_17_oi_p, 0)),                  
                        ebspricelist106 = NVL(sd.ebspricelist106, 0) + sd1.pl_17_accrual_cd,
                        ebspricelist107 = NVL(sd.ebspricelist107, 0) + sd1.pl_17_accrual_coop,
                        last_update_date = SYSDATE
                        WHEN NOT MATCHED THEN 
                        INSERT(item_id, location_id, sales_date,
                               actuals_ttl, actual_con_fcst, act_part_ord, avail_stock, 
                               ebspricelist104, ebspricelist103, ebspricelist106, ebspricelist107,
                               last_update_date)
                        VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, 
                               sd1.pl_13_case_oi, sd1.pl_13_oi_p, sd1.pl_13_accrual_cd, sd1.pl_13_accrual_coop,
                               sd1.pl_17_case_oi, sd1.pl_17_oi_p, sd1.pl_17_accrual_cd, sd1.pl_17_accrual_coop,
                               SYSDATE )';      
                               
            dynamic_ddl (sql_str);
            COMMIT;            
            
            sql_str := 'MERGE INTO MDP_MATRIX mdp
                        USING(SELECT item_id, location_id 
                              FROM rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t) mdp1
                        ON(mdp.item_id = mdp1.item_id
                        AND mdp.location_id = mdp1.location_id)
                        WHEN MATCHED THEN 
                        UPDATE SET last_update_date = SYSDATE, sales_data_lud = SYSDATE';
            
            dynamic_ddl (sql_str);
            COMMIT;            
            
            v_status := 'Step 9 ';    
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
      
      END IF;
    
    END IF;

--    IF p_sync_flag IN (2, 3) THEN
--    
--      sql_str := 'MERGE INTO accrual a
--                  USING (SELECT DISTINCT p.promotion_id, p.promotion_code, p.promotion_desc, p.promotion_stat_id, p.promotion_type_id
--                         FROM 
--                         rr_promos_sync_' || p_sync_ms || '_' || p_scn_id || '_t rpt,
--                         promotion p
--                         WHERE p.promotion_id = rpt.promotion_id
--                         AND rpt.rr_sync_ad = 1) a1
--                  ON(a.accrual_id = a1.promotion_id)
--                  WHEN MATCHED THEN 
--                  UPDATE SET accrual_code = a1.promotion_code,
--                             accrual_desc = a1.promotion_desc,
--                             accrual_stat_id = a1.promotion_stat_id,
--                             accrual_type_id = a1.promotion_type_id
--                  WHEN NOT MATCHED THEN 
--                  INSERT (accrual_id, accrual_code, accrual_desc, accrual_stat_id, accrual_type_id)
--                  VALUES (a1.promotion_id, a1.promotion_code, a1.promotion_desc, a1.promotion_stat_id, a1.promotion_type_id)'; 
--   
--      dynamic_ddl (sql_str);
--      COMMIT;
--   
--      sql_str := 'MERGE INTO accrual_matrix am
--                  USING (SELECT /*+ FULL(rct) PARALLEL(rct, 24) */ pm.item_id, pm.location_id, pm.promotion_id, pm.promotion_stat_id, pm.promotion_type_id, pm.from_date - 42 from_date, pm.until_date + 42 until_date
--                  FROM promotion_matrix pm,
--                  rr_promos_sync_' || p_sync_ms || '_' || p_scn_id || '_t rpt,                
--                  rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
--                  WHERE rpt.promotion_id = pm.promotion_id
--                  AND rpt.rr_sync_ad = 1
--                  AND rct.item_id = pm.item_id
--                  AND rct.location_id = pm.location_id
--                  /*AND EXISTS(SELECT 1 FROM rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t rpt1
--                              WHERE rpt1.promotion_id = rpt.promotion_id) */
--                  ) am1
--                  ON(am.accrual_id = am1.promotion_id
--                  AND am.item_id = am1.item_id
--                  AND am.location_id = am1.location_id)
--                  WHEN MATCHED THEN 
--                  UPDATE SET accrual_stat_id = am1.promotion_stat_id, accrual_type_id = am1.promotion_type_id, 
--                  from_date = am1.from_date, until_date = am1.until_date,
--                  accrual_data_lud = SYSDATE
--                  WHERE NVL(am.accrual_stat_id, 0) <> NVL(am1.promotion_stat_id, 0) 
--                  OR NVL(am.accrual_type_id, 0) <> NVL(am1.promotion_type_id, 0)
--                  OR NVL(am.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) <> NVL(am1.from_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))
--                  OR NVL(am.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY'')) <> NVL(am1.until_date, TO_DATE(''01/01/1900'', ''DD/MM/YYYY''))
--                  WHEN NOT MATCHED THEN 
--                  INSERT (accrual_id, item_id, location_id, accrual_stat_id, accrual_type_id, from_date, until_date)
--                  VALUES (am1.promotion_id, am1.item_id, am1.location_id, am1.promotion_stat_id, am1.promotion_type_id, am1.from_date, am1.until_date)'; 
--   
--      dynamic_ddl (sql_str);
--      COMMIT;
--      
--      v_status := 'Step 10 ';    
--    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
--      
--      sql_str := 'MERGE INTO /*+ INDEX(ad, rr_sync_promo_off_' || p_sync_ms || '_' || p_scn_id || '_i) */ rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t ad
--                  USING (SELECT   /*+ full(rct) parallel (rct,24) */
--                      ad.item_id, ad.location_id, ad.accrual_id, ad.sales_date plan_date,
--                      0 rr_accrual_vol_base_ad,                      
--                      0 rr_accrual_vol_incr_ad,                      
--                      0 rr_accrual_vol_eng_incr_ad,                      
--                      0 rr_accrual_vol_ad,                      
--                      0 rr_accrual_cd_ad,
--                      0 rr_accrual_coop_ad,
--                      0 rr_accrual_hf_cd_ad,
--                      0 rr_accrual_hf_coop_ad,    
--                      0 rr_accrual_vol_base_un_ad,                      
--                      0 rr_accrual_vol_incr_un_ad,                      
--                      0 rr_accrual_vol_eng_incr_un_ad,                      
--                      0 rr_accrual_vol_un_ad,                      
--                      0 rr_accrual_cd_un_ad,
--                      0 rr_accrual_coop_un_ad,
--                      0 rr_accrual_hf_cd_un_ad,
--                      0 rr_accrual_hf_coop_un_ad,       
--                      ad.accrual_stat_id promotion_stat_id, 
--                      ad.accrual_type_id promotion_type_id,
--                      1 rr_sync_ad,
--                      0 rr_sync_sd,
--                      0 rr_ms_vol
--                  FROM accrual_data ad,
--                  promotion_stat ps,
--                  promotion_type pt,
--                  rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
--                  WHERE 1 = 1
--                  AND ad.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
--                  AND ad.item_id = rct.item_id
--                  AND ad.location_id = rct.location_id
--                  AND ad.promotion_stat_id = ps.promotion_stat_id
--                  AND ad.promotion_type_id = pt.promotion_type_id
--                  AND pt.rr_margin_support = ' || p_sync_ms || '
--                  AND NVL(pt.rr_sync_ad, 0) + NVL(pt.rr_sync_vol_ad, 0) > 0
--                  AND NVL(ps.rr_sync_ad, 0) + NVL(ps.rr_sync_vol_ad, 0) > 0
--                  AND ABS(NVL(ad.rr_accrual_vol_base_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_vol_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_hf_cd_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_vol_base_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_incr_un_ad, 0)) + ABS(NVL(ad.rr_accrual_vol_eng_incr_un_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_vol_un_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_coop_un_ad, 0)) 
--                  + ABS(NVL(ad.rr_accrual_hf_cd_un_ad, 0)) + ABS(NVL(ad.rr_accrual_hf_coop_un_ad, 0)) 
--                  + ABS(NVL(ad.rr_ms_vol, 0))
--                  <> 0
--                  ) ad1
--                  ON (    ad1.item_id = ad.item_id
--                  AND ad1.location_id = ad.location_id
--                  AND ad1.accrual_id = ad.promotion_id
--                  AND ad1.plan_date = ad.plan_date)
--                  WHEN NOT MATCHED THEN
--                  INSERT(promotion_id, item_id, location_id, sales_date, plan_date, 
--                         base_plan, incr_plan, engine_incr, 
--                         accrual_vol, accrual_cd, accrual_coop, 
--                         accrual_hf_cd, accrual_hf_coop, 
--                         base_plan_a, incr_plan_a, engine_incr_a,
--                         accrual_vol_a, accrual_cd_a, accrual_coop_a, 
--                         accrual_hf_cd_a, accrual_hf_coop_a,                          
--                         promotion_stat_id, promotion_type_id, rr_sync_ad, rr_sync_sd, rr_ms_vol)
--                  VALUES(ad1.accrual_id, ad1.item_id, ad1.location_id, ad1.plan_date, ad1.plan_date, 
--                         ad1.rr_accrual_vol_base_un_ad, ad1.rr_accrual_vol_incr_un_ad, ad1.rr_accrual_vol_eng_incr_un_ad,
--                         ad1.rr_accrual_vol_un_ad, ad1.rr_accrual_cd_un_ad, ad1.rr_accrual_coop_un_ad, 
--                         ad1.rr_accrual_hf_cd_un_ad, ad1.rr_accrual_hf_coop_un_ad, 
--                         ad1.rr_accrual_vol_base_ad, ad1.rr_accrual_vol_incr_ad, ad1.rr_accrual_vol_eng_incr_ad,
--                         ad1.rr_accrual_vol_ad, ad1.rr_accrual_cd_ad, ad1.rr_accrual_coop_ad, 
--                         ad1.rr_accrual_hf_cd_ad, ad1.rr_accrual_hf_coop_ad, 
--                         ad1.promotion_stat_id, ad1.promotion_type_id, ad1.rr_sync_ad, ad1.rr_sync_ad, ad1.rr_ms_vol)';
--   
--      dynamic_ddl (sql_str);
--      COMMIT;
--      
--      v_status := 'Step 11 ';    
--    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
--   
--      sql_str := 'MERGE INTO  accrual_data ad
--                  USING (SELECT   /*+ full(rct) parallel (rct,24) */
--                      ad.item_id, ad.location_id, ad.promotion_id accrual_id, ad.plan_date,
--                      SUM(base_plan) rr_accrual_vol_base_un_ad, SUM(incr_plan) rr_accrual_vol_incr_un_ad, SUM(engine_incr) rr_accrual_vol_eng_incr_un_ad, 
--                      SUM(accrual_vol) rr_accrual_vol_un_ad, SUM(accrual_cd) rr_accrual_cd_un_ad, SUM(accrual_coop) rr_accrual_coop_un_ad, 
--                      SUM(accrual_hf_cd) rr_accrual_hf_cd_un_ad, SUM(accrual_hf_coop) rr_accrual_hf_coop_un_ad,
--                      SUM(base_plan_a) rr_accrual_vol_base_ad, SUM(incr_plan_a) rr_accrual_vol_incr_ad, SUM(engine_incr_a) rr_accrual_vol_eng_incr_ad, 
--                      SUM(accrual_vol_a) rr_accrual_vol_ad, SUM(accrual_cd_a) rr_accrual_cd_ad, SUM(accrual_coop_a) rr_accrual_coop_ad, 
--                      SUM(accrual_hf_cd_a) rr_accrual_hf_cd_ad, SUM(accrual_hf_coop_a) rr_accrual_hf_coop_ad, 
--                      MAX(promotion_stat_id) promotion_stat_id, MAX(promotion_type_id) promotion_type_id, SUM(rr_ms_vol) rr_ms_vol
--                  FROM rr_sync_promo_offset_' || p_sync_ms || '_' || p_scn_id || '_t ad,
--                  rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
--                  WHERE 1 = 1
--                  AND ad.rr_sync_ad = 1
--                  AND ad.item_id = rct.item_id
--                  AND ad.location_id = rct.location_id
--                  AND ad.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_min, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
--                  GROUP BY ad.item_id, ad.location_id, ad.promotion_id, ad.plan_date
--                  ) ad1
--                  ON (  ad1.item_id = ad.item_id
--                  AND ad1.location_id = ad.location_id
--                  AND ad1.accrual_id = ad.accrual_id
--                  AND ad1.plan_date = ad.sales_date)
--                  WHEN MATCHED THEN
--                  UPDATE SET 
--                  ad.rr_accrual_vol_base_un_ad = ad1.rr_accrual_vol_base_un_ad, 
--                  ad.rr_accrual_vol_incr_un_ad = ad1.rr_accrual_vol_incr_un_ad, ad.rr_accrual_vol_eng_incr_un_ad = ad1.rr_accrual_vol_eng_incr_un_ad, 
--                  ad.rr_accrual_vol_un_ad = ad1.rr_accrual_vol_un_ad, ad.rr_accrual_cd_un_ad = ad1.rr_accrual_cd_un_ad, 
--                  ad.rr_accrual_coop_un_ad = ad1.rr_accrual_coop_un_ad,                   
--                  ad.rr_accrual_hf_cd_un_ad = ad1.rr_accrual_hf_cd_un_ad, ad.rr_accrual_hf_coop_un_ad = ad1.rr_accrual_hf_coop_un_ad,
--                  ad.rr_accrual_vol_base_ad = ad1.rr_accrual_vol_base_ad, 
--                  ad.rr_accrual_vol_incr_ad = ad1.rr_accrual_vol_incr_ad, ad.rr_accrual_vol_eng_incr_ad = ad1.rr_accrual_vol_eng_incr_ad, 
--                  ad.rr_accrual_vol_ad = ad1.rr_accrual_vol_ad, ad.rr_accrual_cd_ad = ad1.rr_accrual_cd_ad, ad.rr_accrual_coop_ad = ad1.rr_accrual_coop_ad,                   
--                  ad.rr_accrual_hf_cd_ad = ad1.rr_accrual_hf_cd_ad, ad.rr_accrual_hf_coop_ad = ad1.rr_accrual_hf_coop_ad,
--                  ad.accrual_stat_id = ad1.promotion_stat_id, ad.accrual_type_id = ad1.promotion_type_id,
--                  ad.promotion_stat_id = ad1.promotion_stat_id, ad.promotion_type_id = ad1.promotion_type_id,
--                  ad.rr_ms_vol = ad1.rr_ms_vol,
--                  ad.last_update_date = SYSDATE
--                  WHERE 1 = 1
--                  OR NVL(ad.rr_accrual_vol_base_un_ad, 0) <> NVL(ad1.rr_accrual_vol_base_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_incr_un_ad, 0) <> NVL(ad1.rr_accrual_vol_incr_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_eng_incr_un_ad, 0) <> NVL(ad1.rr_accrual_vol_eng_incr_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_un_ad, 0) <> NVL(ad1.rr_accrual_vol_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_cd_un_ad, 0) <> NVL(ad1.rr_accrual_cd_un_ad, 0)
--                  OR NVL(ad.rr_accrual_coop_un_ad, 0) <> NVL(ad1.rr_accrual_coop_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_hf_cd_un_ad, 0) <> NVL(ad1.rr_accrual_hf_cd_un_ad, 0)
--                  OR NVL(ad.rr_accrual_hf_coop_un_ad, 0) <> NVL(ad1.rr_accrual_hf_coop_un_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_base_ad, 0) <> NVL(ad1.rr_accrual_vol_base_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_incr_ad, 0) <> NVL(ad1.rr_accrual_vol_incr_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_eng_incr_ad, 0) <> NVL(ad1.rr_accrual_vol_eng_incr_ad, 0) 
--                  OR NVL(ad.rr_accrual_vol_ad, 0) <> NVL(ad1.rr_accrual_vol_ad, 0) 
--                  OR NVL(ad.rr_accrual_cd_ad, 0) <> NVL(ad1.rr_accrual_cd_ad, 0)
--                  OR NVL(ad.rr_accrual_coop_ad, 0) <> NVL(ad1.rr_accrual_coop_ad, 0) 
--                  OR NVL(ad.rr_accrual_hf_cd_ad, 0) <> NVL(ad1.rr_accrual_hf_cd_ad, 0)
--                  OR NVL(ad.rr_accrual_hf_coop_ad, 0) <> NVL(ad1.rr_accrual_hf_coop_ad, 0)                   
--                  OR NVL(ad.promotion_stat_id, 0) <> NVL(ad1.promotion_stat_id, 0)
--                  OR NVL(ad.promotion_type_id, 0) <> NVL(ad1.promotion_type_id, 0)
--                  OR NVL(ad.accrual_stat_id, 0) <> NVL(ad1.promotion_stat_id, 0)
--                  OR NVL(ad.accrual_type_id, 0) <> NVL(ad1.promotion_type_id, 0)
--                  OR NVL(ad.rr_ms_vol, 0) <> NVL(ad1.rr_ms_vol, 0)
--                  WHEN NOT MATCHED THEN 
--                  INSERT (accrual_id, item_id, location_id, sales_date, rr_accrual_vol_base_un_ad, 
--                          rr_accrual_vol_incr_un_ad, rr_accrual_vol_eng_incr_un_ad,
--                          rr_accrual_vol_un_ad, rr_accrual_cd_un_ad, rr_accrual_coop_un_ad, 
--                          rr_accrual_hf_cd_un_ad, rr_accrual_hf_coop_un_ad, 
--                          rr_accrual_vol_base_ad, rr_accrual_vol_incr_ad, rr_accrual_vol_eng_incr_ad,
--                          rr_accrual_vol_ad, rr_accrual_cd_ad, rr_accrual_coop_ad, 
--                          rr_accrual_hf_cd_ad, rr_accrual_hf_coop_ad,                           
--                          accrual_stat_id, accrual_type_id, promotion_stat_id, promotion_type_id, rr_ms_vol)
--                  VALUES (ad1.accrual_id, ad1.item_id, ad1.location_id, ad1.plan_date, 
--                          ad1.rr_accrual_vol_base_un_ad, ad1.rr_accrual_vol_incr_un_ad, ad1.rr_accrual_vol_eng_incr_un_ad,
--                          ad1.rr_accrual_vol_un_ad, ad1.rr_accrual_cd_un_ad, ad1.rr_accrual_coop_un_ad, 
--                          ad1.rr_accrual_hf_cd_un_ad, ad1.rr_accrual_hf_coop_un_ad, 
--                          ad1.rr_accrual_vol_base_ad, ad1.rr_accrual_vol_incr_ad, ad1.rr_accrual_vol_eng_incr_ad,
--                          ad1.rr_accrual_vol_ad, ad1.rr_accrual_cd_ad, ad1.rr_accrual_coop_ad, 
--                          ad1.rr_accrual_hf_cd_ad, ad1.rr_accrual_hf_coop_ad, 
--                          ad1.promotion_stat_id, ad1.promotion_type_id, ad1.promotion_stat_id, ad1.promotion_type_id, ad1.rr_ms_vol)';
--   
--      dynamic_ddl (sql_str);
--      COMMIT;
--      
--      v_status := 'Step 12 ';    
--    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
--      
--      sql_str := 'MERGE INTO accrual_matrix am
--                  USING (SELECT ad.item_id, ad.location_id, ad.accrual_id, max(ad.sales_date) until_date, min(ad.sales_date) from_date
--                  FROM accrual_data ad,
--                  rr_promos_sync_' || p_sync_ms || '_' || p_scn_id || '_t rpt,
--                  rr_promo_combs_' || p_sync_ms || '_' || p_scn_id || '_t rct
--                  WHERE rpt.promotion_id = ad.accrual_id
--                  AND rct.item_id = ad.item_id
--                  AND rct.location_id = ad.location_id
--                  GROUP BY ad.item_id, ad.location_id, ad.accrual_id
--                  ) am1
--                  ON(am.accrual_id = am1.accrual_id
--                  AND am.item_id = am1.item_id
--                  AND am.location_id = am1.location_id)
--                  WHEN MATCHED THEN 
--                  UPDATE SET from_date = am1.from_date, until_date = am1.until_date, accrual_data_lud = SYSDATE'; 
--   
--      dynamic_ddl (sql_str);
--      COMMIT;      
--    
--    END IF;
--    
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END;
    
    PROCEDURE prc_margin_support(p_scn_id      NUMBER)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_MARGIN_SUPPORT
   PURPOSE:    Calculates latest margin support
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   p_start_date  DATE;
   p_end_date    DATE;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_MARGIN_SUPPORT';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);    
    
        --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;    
    ---
    
    SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
    INTO v_day
    FROM sys_params
    WHERE pname = 'FIRSTDAYINWEEK';    
    
    p_start_date := NEXT_DAY(SYSDATE - (26 *7), v_day) - 7;
    p_end_date := NEXT_DAY(SYSDATE + (104 *7), v_day) - 7;
    
    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);
    
    ---    
    
/*   check_and_drop('rr_promo_cmb_' || p_scn_id || '_t');
   
   sql_str := 'CREATE TABLE rr_promo_cmb_' || p_scn_id || '_t AS
               SELECT pm.item_id, pm.location_id 
               FROM promotion_matrix pm
               WHERE pm.promotion_id = ' || p_promotion_id;
   
   dynamic_ddl(sql_str);    */
    
    ---
      
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    INTO max_sales_date 
    FROM DUAL;
    
    check_and_drop('rr_promos_ms_calc_' || p_scn_id || '_t');
   
    sql_str := 'CREATE TABLE rr_promos_ms_calc_' || p_scn_id || '_t AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               rr_tmp_scn' || p_scn_id || 'promos_t t,
               promotion_dates pdt,
               promotion_stat ps,
               promotion_type pt
               WHERE 1 = 1
               AND p.promotion_id = t.promotion_id
               AND p.promotion_type_id = pt.promotion_type_id
               AND p.promotion_stat_id = ps.promotion_stat_id
               AND p.scenario_id <> 265
               AND pt.rr_margin_support = 1
               AND NVL(pt.rr_sync_pd, 0) + NVL(pt.rr_sync_vol_pd, 0) > 0
               AND NVL(ps.rr_sync_pd, 0) + NVL(ps.rr_sync_vol_pd, 0) > 0
               AND p.promotion_id = pdt.promotion_id
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               ';    
    
    dynamic_ddl(sql_str);   
    COMMIT;
    
    sql_str:= 'MERGE INTO promotion_data pd
               USING(SELECT pd.promotion_id, pd.item_id, pd.location_id, pd.sales_date,
               DECODE(NVL(sd.demand_type, 0),1,(NVL(sd.exfact_base,0)* (nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)),
               NVL(sd.unexfact_or,(NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || get_fore_col(0, v_eng_profile) || ', sd.npi_forecast)))*
              (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)))) vol
                     FROM promotion p,
                     promotion_data pd,
                     sales_data sd,
                     rr_promos_ms_calc_' || p_scn_id || '_t ms
                     WHERE p.promotion_id = ms.promotion_id
                     AND p.promotion_id = pd.promotion_id
                     AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
                                       AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') 
                     AND pd.item_id = sd.item_id
                     AND pd.location_id = sd.location_id
                     AND pd.sales_date = sd.sales_date) pd1
                ON(pd.promotion_id = pd1.promotion_id
                AND pd.item_id = pd1.item_id 
                AND pd.location_id = pd1.location_id
                AND pd.sales_date = pd1.sales_date )                
                WHEN MATCHED THEN 
                UPDATE SET rr_accrual_cd_pd = pd1.vol * pd.case_buydown,
                rr_accrual_coop_pd = pd.event_cost,
                --rr_accrual_vol_pd = pd1.vol,
                rr_pl_cd_pd = pd1.vol * pd.case_buydown,
                rr_pl_coop_pd = pd.event_cost,
                rr_ms_vol = pd1.vol,
                last_update_date = SYSDATE';
    
    dynamic_ddl(sql_str);   
    COMMIT;    
    
    sql_str := 'UPDATE promotion_matrix
                SET last_update_date = SYSDATE, promotion_data_lud = SYSDATE
                WHERE promotion_id IN (SELECT promotion_id FROM rr_promos_ms_calc_' || p_scn_id || '_t)';

    dynamic_ddl(sql_str);   
    COMMIT;  
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END;

PROCEDURE prc_set_wif_scn_flag(p_scn_id      NUMBER,p_value NUMBER)    
  AS
/******************************************************************************
   NAME:       PRC_SET_WIF_SCN_FLAG
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        26/03/2013  Lakshmi Annapragada

******************************************************************************/

   v_prog_name   VARCHAR2 (100)   := 'PRC_SET_WIF_SCN_FLAG';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   guidelines_val          NUMBER (10);
   p2b_val                 NUMBER (10);
   
  BEGIN
   pre_logon;
   v_status      := 'start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);   

    MERGE INTO /*+ parallel(sd1,16)*/sales_data sd1
    USING(SELECT DISTINCT /*+ parallel(pd,16)*/ pd.item_id,pd.location_id,pd.sales_date 
            FROM promotion_data pd,promotion p
           WHERE pd.promotion_id = p.promotion_id
             AND p.scenario_id = p_scn_id
            )sd2
    ON (sd1.item_id = sd1.item_id
    AND sd1.location_id = sd2.location_id
    AND sd1.sales_date = sd2.sales_date)
    WHEN MATCHED THEN UPDATE SET sd1.wif_scenario_flag = p_value;
    
    COMMIT;
    
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END prc_set_wif_scn_flag;      
END;

/
