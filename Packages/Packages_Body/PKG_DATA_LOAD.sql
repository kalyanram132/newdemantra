--------------------------------------------------------
--  DDL for Package Body PKG_DATA_LOAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_DATA_LOAD" AS

/******************************************************************************
   NAME:         RR_PKG_COMMON_UTILS BODY
   PURPOSE:      All procedures commanly used across modules
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version
   
   ******************************************************************************/


  PROCEDURE prc_disable_logging_sd AS
  /**************************************************************************
  NAME: PRC_DISABLE_LOGGING_SD
  PURPOSE: To disable logging in sales_data table

  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_DISABLE_LOGGING_SD';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    BEGIN 
    EXECUTE IMMEDIATE 'alter table sales_data nologging'; 
    END;

    COMMIT; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END;
  
PROCEDURE prc_enable_logging_sd AS
  /**************************************************************************
  NAME: PRC_DISABLE_LOGGING_SD
  PURPOSE: To disable logging in sales_data table

  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_ENABLE_LOGGING_SD';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    BEGIN 
    EXECUTE IMMEDIATE 'alter table sales_data logging'; 
    END;

    COMMIT; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END;  

PROCEDURE prc_truncate_src 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_TRUNCATE_SRC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



   dynamic_ddl ('truncate table t_src_item_tmpl');
   dynamic_ddl ('truncate table t_src_item_tmpl_err');
   dynamic_ddl ('truncate table t_src_loc_tmpl');
   dynamic_ddl ('truncate table t_src_loc_tmpl_err');
--   dynamic_ddl ('truncate table t_src_sales_tmpl');
--   dynamic_ddl ('truncate table t_src_sales_tmpl_err');
   COMMIT;

   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_truncate_sales 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_TRUNCATE_SALES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   dynamic_ddl ('truncate table t_src_sales_tmpl');
   dynamic_ddl ('truncate table t_src_sales_tmpl_err');
   COMMIT;

   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_cl_truncate_src 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_CL_TRUNCATE_SRC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



   dynamic_ddl ('truncate table t_src_sapl_item_tmpl');   
   DYNAMIC_DDL ('truncate table t_src_sapl_loc_tmpl');
  -- dynamic_ddl ('truncate table t_src_accrual');
   COMMIT;

   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_cl_truncate_sales
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_CL_TRUNCATE_SALES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   dynamic_ddl ('truncate table t_src_accrual');
   COMMIT;

   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_cl_truncate_scan
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_CL_TRUNCATE_SCAN';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl ('truncate table T_SRC_SAPL_SCAN');   
   
   COMMIT;
   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_cl_truncate_claims
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_CL_TRUNCATE_CLAIMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl ('truncate table T_SRC_CLAIMS');   
   
   COMMIT;
   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_replace_apostrophe
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_REPLACE_APOSTROPHE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin replace_apostrophe; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END prc_replace_apostrophe;

PROCEDURE prc_ep_check_items
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_CHECK_ITEMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_check_items; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE; 
        END;
    
END prc_ep_check_items;

PROCEDURE prc_ep_load_items
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_LOAD_ITEMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_load_items; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END prc_ep_load_items;
  
PROCEDURE prc_ep_check_location
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_CHECK_LOCATION';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_check_location; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;
  
PROCEDURE prc_ep_load_locations
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_LOAD_LOCATIONS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_load_locations; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;


PROCEDURE prc_ep_load_sales
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_LOAD_SALES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_load_sales; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;

PROCEDURE prc_mdp_add
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_MDP_ADD';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin mdp_add; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;  


PROCEDURE prc_ep_load_mdp_level
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EP_LOAD_MDP_LEVEL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    EXECUTE IMMEDIATE 'begin ep_load_mdp_level; end;'; 
 
    commit; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END; 

PROCEDURE prc_update_from_till_date
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_FROM_TILL_DATE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   MERGE INTO mdp_matrix mdp
   USING (SELECT mla.item_id, mla.location_id, 
                          NVL(MIN(sd.sales_date), TO_DATE('01/01/2010', 'DD/MM/YYYY')) from_date,
                          NVL(MAX(sd.sales_date), TO_DATE('01/01/2010', 'DD/MM/YYYY')) until_date
              FROM mdp_matrix mla, sales_data sd
              WHERE mla.item_id = sd.item_id (+) 
              AND mla.location_id = sd.location_id (+)
              GROUP BY mla.item_id, mla.location_id) mdp1
    ON(mdp.item_id = mdp1.item_id
    AND  mdp.location_id = mdp1.location_id) 
    WHEN MATCHED THEN 
    UPDATE SET mdp.from_date = mdp1.from_date, mdp.until_date = mdp1.until_date
    WHERE NVL(mdp.from_date, TO_DATE('01/01/2300', 'DD/MM/YYYY')) > mdp1.from_date 
       OR NVL(mdp.until_date, TO_DATE('01/01/1900', 'DD/MM/YYYY')) < mdp1.until_date ;
    
    COMMIT;
    
    UPDATE mdp_matrix 
    SET allocation_wk0 = 1, allocation_wk1 = 0, 
        allocation_wk2 = 0, allocation_wk3 = 0, 
        allocation_wk4 = 0, allocation_wk5 = 0, 
        allocation_wk6 = 0
    WHERE NVL(allocation_wk0, 0) + NVL(allocation_wk1, 0) +
          NVL(allocation_wk2, 0) + NVL(allocation_wk3, 0) +
          NVL(allocation_wk4, 0) + NVL(allocation_wk5, 0) +
          NVL(allocation_wk6, 0) = 0; 

    COMMIT;
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
END;

PROCEDURE prc_update_max_sales_date
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_MAX_SALES_DATE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   UPDATE sys_params
      SET pval = NEXT_DAY (TO_CHAR (SYSDATE, 'MM/DD/YYYY'), 'monday') - 7
    WHERE pname = 'min_fore_sales_date';

   COMMIT;

   UPDATE sys_params
      SET pval = NEXT_DAY (TO_CHAR (SYSDATE, 'MM/DD/YYYY'), 'monday') - 14
    WHERE pname = 'max_sales_date';

   COMMIT;
   
     
   UPDATE init_params_0
   SET value_date = NEXT_DAY (SYSDATE,  'monday') - 14
   WHERE pname='last_date_backup';
   
   COMMIT;
   
   UPDATE init_params_0
   SET value_date = NEXT_DAY (SYSDATE,  'monday') - 14
   WHERE pname='last_date';
   
   COMMIT;
   
   EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;


PROCEDURE prc_delete_load_flag
IS

    v_max_scan_date   DATE;

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_DELETE_LOAD_FLAG';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    dynamic_ddl('TRUNCATE TABLE rr_load_flag');

    COMMIT;
        
    v_status := 'End ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;

PROCEDURE prc_update_src 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_SRC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    
    INSERT INTO t_src_item_tmpl(
    eq_unit, t_ep_i_att_1, t_ep_i_att_1_desc, t_ep_i_att_10, t_ep_i_att_10_desc,  t_ep_i_att_4, t_ep_i_att_4_desc,
    T_EP_I_ATT_5, T_EP_I_ATT_5_DESC, T_EP_I_ATT_6, T_EP_I_ATT_6_DESC,  
    t_ep_p2, t_ep_p2_desc, t_ep_p2a, t_ep_p2a_desc,   t_ep_p2b, t_ep_p2b_desc, t_ep_p3, t_ep_p3_desc,
    t_ep_p4, t_ep_p4_desc, dm_item_code, dm_item_desc,  e1_shipping_uom_multiple, e1_weight_uom_multiple, e1_volume_uom_multiple,
   e1_item_category_3, e1_item_category_3_desc, 
    E1_ITEM_CATEGORY_4, E1_ITEM_CATEGORY_4_DESC,  E1_ITEM_CATEGORY_6, E1_ITEM_CATEGORY_6_DESC, E1_ITEM_CATEGORY_7, E1_ITEM_CATEGORY_7_DESC,
    EBS_PRODUCT_CATEGORY_CODE, EBS_PRODUCT_CATEGORY_DESC, EBS_PRODUCT_FAMILY_CODE, EBS_PRODUCT_FAMILY_DESC ,T_EP_P2A1,T_EP_P2A1_DESC,
    E1_ITEM_CATEGORY_5, E1_ITEM_CATEGORY_5_DESC
     )
    SELECT 
    ltrim(rtrim(eq_unit)), ltrim(rtrim(MATERIAL_CODE)), ltrim(rtrim(MATERIAL_DESC)), ltrim(rtrim(PROD_PACK_LVL2_CODE)), ltrim(rtrim(PROD_PACK_LVL2_DESC)),   ltrim(rtrim(PROD_BRAND_LVL1_CODE)), ltrim(rtrim(PROD_BRAND_LVL1_DESC)),
    ltrim(rtrim(PRODUCT_STATUS_CODE)), ltrim(rtrim(PRODUCT_STATUS_DESC)), ltrim(rtrim(GST)), ltrim(rtrim(GST)),   
    ltrim(rtrim(DIVISION_CODE)), ltrim(rtrim(DIVISION_DESC)), 
    ltrim(rtrim(PRODUCT_SUB_CAT_CODE)), ltrim(rtrim(PRODUCT_SUB_CAT_DESC)),   ltrim(rtrim(PROMOTED_GROUP_CODE)), ltrim(rtrim(PROMOTED_GROUP_DESC)), 
    LTRIM(RTRIM(DIST_CHANNEL_CODE)), LTRIM(RTRIM(DIST_CHANNEL_DESC)), 
    ltrim(rtrim(SALES_ORG_CODE)), ltrim(rtrim(SALES_ORG_DESC)), ltrim(rtrim(MATERIAL_CODE))||'-'||ltrim(rtrim(SALES_ORG_CODE))||ltrim(rtrim(DIST_CHANNEL_CODE))||ltrim(rtrim(DIVISION_CODE)), ltrim(rtrim(MATERIAL_CODE))||'-'||ltrim(rtrim(SALES_ORG_CODE))||ltrim(rtrim(DIST_CHANNEL_CODE))||ltrim(rtrim(DIVISION_CODE)),  ltrim(rtrim(CASES_TO_UNITS)), ltrim(rtrim(CASES_TO_LITRE)),  ltrim(rtrim(CASES_TO_8OUNCE)),
     ltrim(rtrim(COMMERCIAL_AGREEMENT_CODE)), ltrim(rtrim(COMMERCIAL_AGREEMENT_DESC)), 
    LTRIM(RTRIM(PROD_BRAND_LVL2_CODE)), LTRIM(RTRIM(PROD_BRAND_LVL2_DESC)),  LTRIM(RTRIM(PROD_BRAND_LVL3_CODE)), LTRIM(RTRIM(PROD_BRAND_LVL3_DESC)), LTRIM(RTRIM(PROD_PACK_LVL3_CODE)), LTRIM(RTRIM(PROD_PACK_LVL3_DESC)), 
    LTRIM(RTRIM(PROD_PACK_LVL1_CODE)), LTRIM(RTRIM(PROD_PACK_LVL1_DESC)), LTRIM(RTRIM(ORIGINATOR_CODE)), LTRIM(RTRIM(ORIGINATOR_DESC)),
     ltrim(rtrim(PRODUCT_CAT_CODE)), ltrim(rtrim(PRODUCT_CAT_DESC)),
     ltrim(rtrim(PRODUCT_SUB_CAT_CODE)) ||'-'|| ltrim(rtrim(PROMOTED_GROUP_CODE)), ltrim(rtrim(PRODUCT_SUB_CAT_DESC)) ||'-'|| ltrim(rtrim(PROMOTED_GROUP_DESC))
    FROM t_src_sapl_item_tmpl;
  
    COMMIT;

    INSERT INTO t_src_loc_tmpl(     ebs_customer_desc, 
     t_ep_l_att_6,  e1_customer_city, e1_customer_city_desc, e1_customer_state, e1_customer_state_desc, 
    e1_customer_country, e1_customer_country_desc,  
    e1_customer_category_2, e1_customer_category_2_desc, 
    e1_customer_category_4, e1_customer_category_4_desc, ebs_account_code, 
    ebs_account_desc, ebs_customer_code,t_ep_l_att_10, t_ep_l_att_10_desc, t_ep_l_att_2, t_ep_l_att_2_desc,  t_ep_l_att_6_desc,  t_ep_l_att_8, t_ep_l_att_8_desc,  
     DM_SITE_CODE, 
    dm_site_desc,EBS_TP_ZONE_CODE,EBS_TP_ZONE_DESC,EBS_ZONE_CODE, EBS_ZONE_DESC
    )    
    SELECT    ltrim(rtrim(CUSTOMER_LEVEL3_DESC))||' : '|| ltrim(rtrim(CUSTOMER_LEVEL3_CODE)), 
      LTRIM(RTRIM(SALES_OFFICE_CODE)), LTRIM(RTRIM(CUSTOMER_LEVEL2_CODE)),LTRIM(RTRIM(CUSTOMER_LEVEL2_DESC))||' : '||LTRIM(RTRIM(CUSTOMER_LEVEL2_CODE)), 
    ltrim(rtrim(DEM_RECEPIENT_CODE)), ltrim(rtrim(DEM_RECEPIENT_DESC)), ltrim(rtrim(CUST_PRICING_PROC_CODE)), ltrim(rtrim(CUST_PRICING_PROC_DESC)),     
    LTRIM(RTRIM(TIER_CODE)), LTRIM(RTRIM(TIER_DESC)), 
    ltrim(rtrim(CUSTOMER_LEVEL_CODE)), ltrim(rtrim(CUSTOMER_LEVEL_DESC))||' : '||ltrim(rtrim(CUSTOMER_LEVEL_CODE)),   
    ltrim(rtrim(CUSTOMER_LEVEL4_CODE)), ltrim(rtrim(CUSTOMER_LEVEL4_DESC))||' : '||ltrim(rtrim(CUSTOMER_LEVEL4_CODE)), ltrim(rtrim(CUSTOMER_LEVEL3_CODE)),       
    ltrim(rtrim(COUNTRY_CODE)), ltrim(rtrim(COUNTRY_DESC)), ltrim(rtrim(CUSTOMER_LEVEL1_CODE)), ltrim(rtrim(CUSTOMER_LEVEL1_DESC))||' : '||ltrim(rtrim(CUSTOMER_LEVEL1_CODE)), ltrim(rtrim(SALES_OFFICE_DESC)), 
     nvl(LTRIM(RTRIM(CUSTOMER_STATUS_CODE)),'Active'),nvl( LTRIM(RTRIM(CUSTOMER_STATUS_DESC)),'Active'), 
     ltrim(rtrim(CUSTOMER_LEVEL_CODE))||'-'||ltrim(rtrim(SALES_ORG_CODE))||ltrim(rtrim(DIST_CHANNEL_CODE))||ltrim(rtrim(DIVISION_CODE)),  ltrim(rtrim(CUSTOMER_LEVEL_CODE))||'-'||ltrim(rtrim(SALES_ORG_CODE))||ltrim(rtrim(DIST_CHANNEL_CODE))||ltrim(rtrim(DIVISION_CODE))
     , nvl(LTRIM(RTRIM(BANNER_CODE)),'N/A'), nvl(LTRIM(RTRIM(BANNER_DESC)),'N/A')                    -- Sanjiiv for Banner Group CR Change
    ,nvl(ltrim(rtrim(BANNER_STATE_CODE)),'N/A'), nvl(ltrim(rtrim(BANNER_STATE_DESC)),'N/A')            -- Sanjiiv for Banner Group Change
    FROM t_src_sapl_loc_tmpl;
  
    COMMIT;
  


  
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 

PROCEDURE prc_update_sales 
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_SALES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
      INSERT INTO T_SRC_SALES_TMPL(SALES_DATE,dm_site_code,dm_item_code,ACTUAL_QTY,INCR_EVT_DOL_RTL,SDATA15,SDATA4,SDATA7,SDATA9,SDATA10,SDATA11,
       SDATA12,SDATA13,SDATA14,EBS_BOOK_HIST_BOOK_QTY_BD,EBS_BOOK_HIST_BOOK_QTY_RD,
       EBS_BOOK_HIST_REQ_QTY_BD,EBS_BOOK_HIST_REQ_QTY_RD,EBS_SHIP_HIST_REQ_QTY_RD,
       EBS_SHIP_HIST_SHIP_QTY_RD,EBS_SHIP_HIST_SHIP_QTY_SD,T_EP_P1,T_EP_LR1,T_EP_LS1,T_EP_M1,T_EP_M2
      ,E1_ITEM_BRANCH_CATEGORY_1,E1_ITEM_BRANCH_CATEGORY_2 ,E1_ITEM_BRANCH_CATEGORY_3
      ,E1_ITEM_BRANCH_CATEGORY_4 ,E1_ITEM_BRANCH_CATEGORY_5 ,E1_ITEM_BRANCH_CATEGORY_6     
      ,E1_ITEM_BRANCH_CATEGORY_7 ,E1_ITEM_BRANCH_CATEGORY_8 ,E1_ITEM_BRANCH_CATEGORY_9    
      ,E1_ITEM_BRANCH_CATEGORY_10 ,E1_ITEM_BRANCH_CATEGORY_11,E1_ITEM_BRANCH_CATEGORY_12     
      ,E1_ITEM_BRANCH_CATEGORY_13 ,E1_ITEM_BRANCH_CATEGORY_14,E1_ITEM_BRANCH_CATEGORY_15    
      ,E1_ITEM_BRANCH_CATEGORY_16 ,E1_ITEM_BRANCH_CATEGORY_17 ,E1_ITEM_BRANCH_CATEGORY_18   
      ,E1_ITEM_BRANCH_CATEGORY_19 ,E1_ITEM_BRANCH_CATEGORY_20 ,E1_ITEM_BRANCH_CATEGORY_21    
      ,E1_ITEM_BRANCH_CATEGORY_22 ,E1_ITEM_BRANCH_CATEGORY_23 ,EBS_DEMAND_CLASS_CODE     
      ,EBS_SALES_CHANNEL_CODE ,DM_ORG_CODE ,EBS_ITEM_SR_PK ,EBS_ORG_SR_PK ,EBS_SITE_SR_PK
      ,EBS_DEMAND_CLASS_SR_PK ,EBS_SALES_CHANNEL_SR_PK)
      SELECT greatest(next_day(trunc(POSTING_DATE,'DD'),'MONDAY')-7,trunc(POSTING_DATE,'MM') ) POSTING_DATE,CUSTOMER_NO||'-'||SALES_ORG,MATERIAL||'-'||substr(SALES_ORG,0,6)||'01',sum(EXFACT_SALES),sum(LIST_PRICE_SALES),
      sum(WAR_PRICE_SALES),sum(GL_ACC_1),sum(GL_ACC_2),sum(GL_ACC_3),sum(GL_ACC_4),sum(GL_ACC_5),
      sum(GL_ACC_6),sum(GL_ACC_7),sum(GL_ACC_8),sum(GL_ACC_9),sum(GL_ACC_10),sum(GL_ACC_11),
      sum(GL_ACC_12),sum(GL_ACC_13),sum(GL_ACC_14),sum(GL_ACC_15),'N/A','N/A','N/A','N/A','N/A',
      'N/A','N/A','N/A','N/A','N/A','N/A',
      'N/A','N/A','N/A','N/A','N/A','N/A',
      'N/A','N/A','N/A','N/A','N/A','N/A',
      'N/A','N/A','N/A','N/A','N/A','N/A',
      'N/A','N/A',-1,-1,-1,'N/A','N/A'
      FROM t_src_accrual
      GROUP BY greatest(next_day(trunc(POSTING_DATE,'DD'),'MONDAY')-7,trunc(POSTING_DATE,'MM') ),
               CUSTOMER_NO||'-'||SALES_ORG,MATERIAL||'-'||substr(SALES_ORG,0,6)||'01';

    COMMIT;
    

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 

PROCEDURE prc_load_scan
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_SCAN';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl('truncate table biio_scan');
    dynamic_ddl('truncate table biio_scan_err');
--    dynamic_ddl('truncate table biio_nat');
--    dynamic_ddl('truncate table biio_nat_err');
    
    insert into BIIO_SCAN(SDATE, LEVEL1, LEVEL2,LEVEL3,LEVEL4, DEMAND_BASE, ACTUALS_INCR, SDATA15, SHELF_PRICE_SD)
    SELECT scan_date, ltrim(rtrim(material)), ltrim(rtrim(customer_level3)),ltrim(rtrim(nvl(banner,'N/A'))),ltrim(rtrim(sales_office)), sum(demand_base), sum(actuals_incr), max(promo_price), max(rrp) 
    FROM T_SRC_SAPL_SCAN
    GROUP BY scan_date, ltrim(rtrim(material)), ltrim(rtrim(customer_level3)),ltrim(rtrim(nvl(banner,'N/A'))),ltrim(rtrim(sales_office)); 
    
    COMMIT;
    
--    INSERT INTO biio_nat(sdate, level1, level2, demand_base, actuals_incr, sdata15, shelf_price_sd)
--    SELECT sdate, ltrim(rtrim(level1)), ltrim(rtrim(level2)), demand_base, actuals_incr, promo_price, shelf_price_sd 
--    FROM t_src_cl_scan_nat; 
--    
--    COMMIT;
    
    pkg_common_utils.prc_start_wf_schema('biio_scan');
--    pkg_common_utils.prc_start_wf_schema('biio_nat');

--    dynamic_ddl('truncate table t_src_cl_scan_state');
--    dynamic_ddl('truncate table t_src_cl_scan_nat');
    
    pkg_ptp.prc_roll_shelf_price;  


    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 

PROCEDURE prc_load_pricing
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PRICING';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl('truncate table biio_pricing');
    dynamic_ddl('truncate table biio_pricing_err');
    
    INSERT INTO biio_pricing(sdate, level1, level2, bbds, bbds_deferred, bnps, cogs, cust_list_price, distribution, list_price,
    market_price, rebate, spoils, tot_effeciencies, def_efficiencies_rate, rr_roi_rebate, rr_roi_effeciencies)
    SELECT sdate, ltrim(rtrim(level1)), ltrim(rtrim(level2)), bbds, bbds_deferred,
    bnps, cogs, cust_list_price, distribution, list_price,
    market_price, rebate, spoils, tot_effeciencies, def_efficiencies_rate, rr_roi_rebate, rr_roi_effeciencies
    FROM t_src_cl_pricing; 
    
    COMMIT;
    
    pkg_common_utils.prc_lowest_lev_integration('BIIO_PRICING',1);
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 


PROCEDURE prc_load_pricing_loop
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PRICING_LOOP';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl('truncate table biio_pricing');
    dynamic_ddl('truncate table biio_pricing_err');
    
    dynamic_ddl('truncate table rr_price_dates_t');
    
    INSERT INTO rr_price_dates_t
    SELECT DISTINCT sdate, 0 FROM t_src_cl_pricing;
    
    COMMIT;
    
    FOR rec IN (SELECT sdate FROM rr_price_dates_t where processed = 0 ORDER BY sdate)
    LOOP
        ---
        dynamic_ddl('truncate table biio_pricing');
    
        INSERT INTO biio_pricing(sdate, level1, level2, bbds, bbds_deferred, bnps, cogs, cust_list_price, distribution, list_price,
        market_price, rebate, spoils, tot_effeciencies, def_efficiencies_rate, rr_roi_rebate, rr_roi_effeciencies)
        SELECT sdate, ltrim(rtrim(level1)), ltrim(rtrim(level2)), bbds, bbds_deferred,
        bnps, cogs, cust_list_price, distribution, list_price,
        market_price, rebate, spoils, tot_effeciencies, def_efficiencies_rate, rr_roi_rebate, rr_roi_effeciencies
        FROM t_src_cl_pricing
        WHERE sdate = rec.sdate; 
        
        COMMIT;
    
        pkg_common_utils.prc_lowest_lev_integration('BIIO_PRICING',1);
        
        UPDATE rr_price_dates_t SET processed = 1 WHERE sdate = rec.sdate;
        ---
        COMMIT;
        ---
    END LOOP;
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_load_budget
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
        
    min_date       DATE;
    max_date       DATE;

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_BUDGET';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    dynamic_ddl('truncate table biio_budget1');
    dynamic_ddl('truncate table biio_budget1_err');
    
    INSERT INTO biio_budget1(sdate, level1, level2, base_non_per_spnd_b, cogs_b, cust_contribution_b, gross_sales_b, list_sales_b, net_sales_b, non_work_co_mark_b, 
    price_adjust_b, spoils_b, ttl_base_bus_dev_spnd_b, tot_distribution_b, tot_efficiencies_b, ttl_promo_trde_spnd_b, total_trad_b)
    SELECT sdate, ltrim(rtrim(level1)), ltrim(rtrim(level2)), base_non_per_spnd_b, cogs_b, cust_contribution_b, gross_sales_b, list_sales_b, 
    net_sales_b, non_work_co_mark_b, price_adjust_b, spoils_b, ttl_base_bus_dev_spnd_b, tot_distribution_b, tot_efficiencies_b, ttl_promo_trde_spnd_b, total_trad_b
    FROM t_src_cl_budget; 
    
    COMMIT;
    
    SELECT MIN(sdate) min_date, MAX(sdate) max_date 
    INTO min_date, max_date
    FROM biio_budget1;
    
    UPDATE /*+ FULL(sd) PARALLEL(sd, 32) */ sales_data sd
    SET ebspricelist109 = NULL,
    ebspricelist110 = NULL,
    ebspricelist111 = NULL,
    ebspricelist112 = NULL,
    ebspricelist113 = NULL,
    ebspricelist114 = NULL,
    ebspricelist115 = NULL,
    ebspricelist116 = NULL,
    ebspricelist117 = NULL,
    ebspricelist118 = NULL,
    ebspricelist119 = NULL,
    ebspricelist120 = NULL,
    ebspricelist121 = NULL,
    ebspricelist122 = NULL,
    smr_4 = NULL,
    ff_5 = NULL
    WHERE ABS(NVL(ebspricelist109,0)) + 
    ABS(NVL(ebspricelist110,0)) + 
    ABS(NVL(ebspricelist111,0)) + 
    ABS(NVL(ebspricelist112,0)) + 
    ABS(NVL(ebspricelist113,0)) + 
    ABS(NVL(ebspricelist114,0)) + 
    ABS(NVL(ebspricelist115,0)) + 
    ABS(NVL(ebspricelist116,0)) + 
    ABS(NVL(ebspricelist117,0)) + 
    ABS(NVL(ebspricelist118,0)) + 
    ABS(NVL(ebspricelist119,0)) + 
    ABS(NVL(ebspricelist120,0)) + 
    ABS(NVL(ebspricelist121,0)) + 
    ABS(NVL(ebspricelist122,0)) + 
    ABS(NVL(smr_4,0)) + 
    ABS(NVL(ff_5,0)) <> 0
    AND sales_date BETWEEN min_date AND max_date;

    COMMIT;
    
    pkg_common_utils.prc_lowest_lev_integration('biio_budget1');

    dynamic_ddl('truncate table t_src_cl_budget');
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 

PROCEDURE prc_load_payto_vendor
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PAYTO_VENDOR';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    MERGE INTO t_src_vendor v
    USING(SELECT customer_code, MAX(customer_desc) customer_desc,  currency currency,  type
          FROM t_src_cl_vendor
          GROUP BY customer_code, type, currency
          ) v1
    ON(v.customer_code = v1.customer_code
    AND v.type = v1.type
    AND v.currency = v1.currency
    )
    WHEN MATCHED THEN 
    UPDATE SET v.customer_desc = v1.customer_desc
    WHEN NOT MATCHED THEN 
    INSERT(customer_code, customer_desc, currency, type, vendor_id)
    VALUES(v1.customer_code, v1.customer_desc, v1.currency, v1.type, t_src_vendor_seq.nextval);
    
    
    COMMIT;  

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 


PROCEDURE prc_load_claims_au
IS

    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER; 
    sql_str            VARCHAR2 (30000);
   
    v_rec_count        NUMBER;
    v_batch_id         INTEGER;
    v_no_records_exp   INTEGER;

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_CLAIMS_AU';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
   dynamic_ddl ('truncate table T_SRC_CLAIMS_AU_TEMP');
   dynamic_ddl ('truncate table T_SRC_CLAIMS_AU_ERR');

---------------------------------------------------------------
-- Claim Validation
---------------------------------------------------------------

   ---Validate Levels to Demantra Levels. If fail, insert into ERROR table
   INSERT INTO t_src_claims_au_err
               (claim_no, promotion_id, start_date, end_date, claim_type,
                claim_cd, claim_coop, gst_amount, ap_ar_flag,
                pay_to_supplier, pay_to_customer, key_account_state,
                key_account, corporate_customer, customer_name, brand,
                promotion_group, promoted_product, claim_hand_fee, claim_fixed_hand_fee,
                Currency, Cd_Deal_Type, Coop_Deal_Type, Link_Unit_Deal, Link_Case_Deal,
                link_handling_fee, customer_promo_no, err_desc ,  INV_DATE)
      SELECT claim_no, promotion_id, start_date, end_date, claim_type,
                claim_cd, claim_coop, gst_amount, ap_ar_flag,
                pay_to_supplier, pay_to_customer, key_account_state,
                key_account, corporate_customer, customer_name, brand,
                promotion_group, promoted_product, claim_hand_fee, claim_fixed_hand_fee,
                Currency, Cd_Deal_Type, Coop_Deal_Type, Link_Unit_Deal, Link_Case_Deal,
                link_handling_fee, customer_promo_no, NULL err_desc, INV_DATE
        FROM t_src_claims_au c
       WHERE NOT (1 = 1
                  AND UPPER(NVL(LTRIM(RTRIM(c.key_account_state)), 'Default CustomerState')) IN (SELECT  UPPER(cs.ebs_account) FROM t_ep_ebs_account cs)
                  AND UPPER(NVL(LTRIM(RTRIM(c.key_account)), 'Default Customer')) IN (SELECT  UPPER(ec.ebs_customer) FROM t_ep_ebs_customer ec)
                  AND UPPER(NVL(LTRIM(RTRIM(c.corporate_customer)), '0')) IN (SELECT  UPPER(la6.l_att_6) FROM t_ep_l_att_6 la6)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.customer_name)), '0')) IN (SELECT UPPER (cc4.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 cc4)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.brand)), '0')) IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)               
                  AND UPPER(NVL(LTRIM(RTRIM(c.promotion_group)), 'Default')) IN (SELECT UPPER (p2b.p2b) FROM t_ep_p2b p2b)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.promoted_product)), '0')) IN (SELECT UPPER (ia1.i_att_1) FROM t_ep_i_att_1 ia1 where ia1.i_att_1 like '%_40' or t_ep_i_att_1_ep_id = 0)               
                  AND UPPER(NVL(LTRIM(RTRIM(c.claim_type)), 'DEFAULT')) IN (SELECT UPPER (st.settlement_type_code) FROM settlement_type st)                 
                  AND UPPER(LTRIM(RTRIM (c.ap_ar_flag))) IN ('AP', 'AR') 
          --        AND c.end_date < SYSDATE 
                  AND c.end_date >= c.start_date 
                  AND ((LTRIM (RTRIM (c.ap_ar_flag)) = 'AR' AND pay_to_customer IS NOT NULL)
                       OR (LTRIM (RTRIM (c.ap_ar_flag)) = 'AP' AND pay_to_supplier IS NOT NULL)
                      )
                  AND ( UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) IN (SELECT UPPER (pc.customer_code) FROM t_src_vendor pc WHERE pc.TYPE = 'AR' AND upper(pc.currency) = 'AUD')
                        OR  UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) = '0')
                  AND( UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) IN (SELECT UPPER (ps.customer_code) FROM t_src_vendor ps WHERE ps.TYPE = 'AP' AND upper(ps.currency) = 'AUD')    
                        OR  UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) = '0')
                  AND UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) IN (SELECT UPPER (cd.cd_deal_type_desc) FROM tbl_cd_deal_type cd)                                                                                     
                  AND UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) IN (SELECT UPPER (coop.coop_deal_type_desc) FROM tbl_coop_deal_type coop)                                                                                     
                  AND NOT (    c.pay_to_customer IS NOT NULL
                           AND c.pay_to_supplier IS NOT NULL
                          )                  
                  AND (  nvl(c.promotion_id,0) = 0
                       OR c.promotion_id IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
                       )
                );

   COMMIT;

   SELECT COUNT (*)
     INTO v_rec_count
     FROM t_src_claims_au_err;

   IF v_rec_count > 0
   THEN
      --Clear from Staging table the errored lines
      DELETE FROM t_src_claims_au c
            WHERE NOT (1 = 1
                  AND UPPER(NVL(LTRIM(RTRIM(c.key_account_state)), 'Default CustomerState')) IN (SELECT  UPPER(cs.ebs_account) FROM t_ep_ebs_account cs)
                  AND UPPER(NVL(LTRIM(RTRIM(c.key_account)), 'Default Customer')) IN (SELECT  UPPER(ec.ebs_customer) FROM t_ep_ebs_customer ec)
                  AND UPPER(NVL(LTRIM(RTRIM(c.corporate_customer)), '0')) IN (SELECT  UPPER(la6.l_att_6) FROM t_ep_l_att_6 la6)
                  AND UPPER(NVL(LTRIM(RTRIM(c.customer_name)), '0')) IN (SELECT UPPER (cc4.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 cc4)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.brand)), '0')) IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)
                  AND UPPER(NVL(LTRIM(RTRIM(c.promotion_group)), 'Default')) IN (SELECT UPPER (p2b.p2b) FROM t_ep_p2b p2b)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.promoted_product)), '0')) IN (SELECT UPPER (ia1.i_att_1) FROM t_ep_i_att_1 ia1 where ia1.i_att_1 like '%_40' or t_ep_i_att_1_ep_id = 0)               
                  AND UPPER(NVL(LTRIM(RTRIM(c.claim_type)), 'DEFAULT')) IN (SELECT UPPER (st.settlement_type_code) FROM settlement_type st)                 
                  AND UPPER(LTRIM(RTRIM (c.ap_ar_flag))) IN ('AP', 'AR') 
          --        AND c.end_date < SYSDATE 
                  AND c.end_date >= c.start_date 
                  AND ((LTRIM (RTRIM (c.ap_ar_flag)) = 'AR' AND pay_to_customer IS NOT NULL)
                       OR (LTRIM (RTRIM (c.ap_ar_flag)) = 'AP' AND pay_to_supplier IS NOT NULL)
                      )
                  AND ( UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) IN (SELECT UPPER (pc.customer_code) FROM t_src_vendor pc WHERE pc.TYPE = 'AR' AND upper(pc.currency) = 'AUD')
                        OR  UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) = '0')
                  AND( UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) IN (SELECT UPPER (ps.customer_code) FROM t_src_vendor ps WHERE ps.TYPE = 'AP' AND upper(ps.currency) = 'AUD')    
                        OR  UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) = '0')
                  AND UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) IN (SELECT UPPER (cd.cd_deal_type_desc) FROM tbl_cd_deal_type cd)                                                                                     
                  AND UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) IN (SELECT UPPER (coop.coop_deal_type_desc) FROM tbl_coop_deal_type coop)                                                                                     
                  AND NOT (    c.pay_to_customer IS NOT NULL
                           AND c.pay_to_supplier IS NOT NULL
                          )                  
                  AND (  nvl(c.promotion_id,0) = 0
                       OR c.promotion_id IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
                       )
                );

      COMMIT;

----------------------------
--Fill in Error Message in t_src_claims_au_err
----------------------------
      UPDATE t_src_claims_au_err c
         SET err_desc = 'Key Account State does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.key_account_state)), 'Default CustomerState')) NOT IN (SELECT  UPPER(cs.ebs_account) FROM t_ep_ebs_account cs)
         AND err_desc IS NULL;

      COMMIT;
      
       -- Added to handle null value --      
       UPDATE t_src_claims_err c
         SET err_desc = 'CLAIM_NO IS NULL'
       WHERE CLAIM_NO IS NULL
         AND err_desc IS NULL;
         
         COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Key Account does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.key_account)), 'Default Customer')) NOT IN (SELECT  UPPER(ec.ebs_customer) FROM t_ep_ebs_customer ec)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Corporate Customer does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.corporate_customer)), '0')) NOT IN (SELECT  UPPER(la6.l_att_6) FROM t_ep_l_att_6 la6)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Customer Name does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.customer_name)), '0')) NOT IN (SELECT UPPER (cc4.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 cc4)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Brand does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.brand)), '0')) NOT IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)                  
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Promotion Group does not exist in Demantra'
       WHERE  UPPER(NVL(LTRIM(RTRIM(c.promotion_group)), 'Default')) NOT IN (SELECT UPPER (p2b.p2b) FROM t_ep_p2b p2b)                   
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Promoted Product does not exist in Demantra'
       WHERE  UPPER(NVL(LTRIM(RTRIM(c.promoted_product)), '0')) NOT IN (SELECT UPPER (ia1.i_att_1) FROM t_ep_i_att_1 ia1 where ia1.i_att_1 like '%_40' or t_ep_i_att_1_ep_id = 0)
        AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Claim Type does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.claim_type)), 'DEFAULT')) NOT IN (SELECT UPPER (st.settlement_type_code) FROM settlement_type st)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'AP / AR Flag is not valid'
       WHERE UPPER (c.ap_ar_flag) NOT IN ('AP', 'AR') AND err_desc IS NULL;

      COMMIT;

    --  UPDATE t_src_claims_au_err c
    --     SET err_desc = 'Claim Can not be entered for Future'
    --   WHERE NOT c.end_date < SYSDATE AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Start Date can not be greater then the End Date'
       WHERE NOT c.end_date > c.start_date AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc =
                'AR entered with No Pay to Customer or AP was entered with to Pay to Supplier'
       WHERE NOT (   (c.ap_ar_flag = 'AR' AND pay_to_customer IS NOT NULL)
                  OR (c.ap_ar_flag = 'AP' AND pay_to_supplier IS NOT NULL)
                 )
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Pay to Customer does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) NOT IN (SELECT UPPER (pc.customer_CODE) FROM t_src_vendor pc WHERE pc.TYPE = 'AR' AND upper(pc.currency) = 'AUD')
         AND c.ap_ar_flag = 'AR'
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Pay to Supplier does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) NOT IN (SELECT UPPER (ps.customer_CODE) FROM t_src_vendor ps WHERE ps.TYPE = 'AP' AND upper(ps.currency) = 'AUD') 
         AND c.ap_ar_flag = 'AP'
         AND err_desc IS NULL;

      COMMIT;
      
      UPDATE t_src_claims_au_err c
         SET err_desc = 'CD deal type does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) NOT IN (SELECT UPPER (cd.cd_deal_type_desc) FROM tbl_cd_deal_type cd)                                                                                     
         AND err_desc IS NULL;

      COMMIT;
      
      UPDATE t_src_claims_au_err c
         SET err_desc = 'COOP deal type does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) NOT IN (SELECT UPPER (coop.coop_deal_type_desc) FROM tbl_coop_deal_type coop)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc =
                'Both Pay to Customer and Pay to Supplier can not be selected'
       WHERE pay_to_supplier IS NOT NULL
         AND pay_to_customer IS NOT NULL
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_au_err c
         SET err_desc = 'Promotion ID is not an open/Valid promotion'
       WHERE NOT (nvl(c.promotion_id,0) = 0
                       OR c.promotion_id IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
                       )
         AND err_desc IS NULL;

      COMMIT;
   END IF;

---------------------------------------------------------------------
--Convert from t_src_claims to t_src_claims_temp. Convert to ep_ids
---------------------------------------------------------------------
   INSERT INTO t_src_claims_au_temp 
      (SELECT ltrim(rtrim(claim_no)) settlement_desc, promotion_id, start_date, end_date, 
       (SELECT MAX(st.settlement_type_id) FROM settlement_type st WHERE UPPER (st.settlement_type_code) = UPPER(NVL(LTRIM(RTRIM(c.claim_type)), 'DEFAULT'))) settlement_type_id,
         claim_cd claim_case_deal_d, claim_coop co_op_$, gst_amount gst, 
         (SELECT MAX(gl_code_id) FROM gl_code gc WHERE UPPER(gc.gl_code_desc) = UPPER(c.ap_ar_flag)) gl_code_id,
         CASE WHEN ap_ar_flag = 'AR' THEN (SELECT MAX(pc.vendor_id)  FROM t_src_vendor pc WHERE pc.TYPE = 'AR' AND upper(pc.currency) = 'AUD' AND UPPER (pc.customer_code) = UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')))  
              WHEN ap_ar_flag = 'AP' THEN (SELECT MAX(ps.vendor_id) FROM t_src_vendor ps WHERE ps.type = 'AP' AND upper(ps.currency) = 'AUD' AND UPPER (ps.customer_code) = UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0'))) 
              ELSE NULL
          END bill_to,
         (SELECT MAX(cs.t_ep_ebs_account_ep_id)  FROM t_ep_ebs_account cs WHERE UPPER (cs.ebs_account) = UPPER(NVL(LTRIM(RTRIM(c.key_account_state)), 'Default CustomerState')))key_account_state,
         (SELECT MAX(ec.t_ep_ebs_customer_ep_id)  FROM t_ep_ebs_customer ec WHERE UPPER (ec.ebs_customer) = UPPER(NVL(LTRIM(RTRIM(c.key_account)), 'Default Customer'))) key_account, 
         (SELECT MAX(la6.t_ep_l_att_6_ep_id)  FROM t_ep_l_att_6 la6 WHERE UPPER (la6.l_att_6) = UPPER(NVL(LTRIM(RTRIM(c.corporate_customer)), '0'))) corporate_customer, 
         (SELECT MAX(cc4.t_ep_e1_cust_cat_4_ep_id)  FROM t_ep_e1_cust_cat_4 cc4 WHERE UPPER (cc4.e1_cust_cat_4) = UPPER(NVL(LTRIM(RTRIM(c.customer_name)), '0'))) customer_name, 
         (SELECT MAX(ic4.t_ep_e1_item_cat_4_ep_id) FROM t_ep_e1_item_cat_4 ic4 WHERE UPPER (ic4.e1_i_cat_desc_4) = UPPER(NVL(LTRIM(RTRIM(c.brand)), '0'))) brand,
         (SELECT MAX(p2b.t_ep_p2b_ep_id)  FROM t_ep_p2b p2b WHERE UPPER (p2b.p2b) = UPPER(NVL(LTRIM(RTRIM(c.promotion_group)), 'Default'))) promotion_group, 
         (SELECT MAX(ia1.t_ep_i_att_1_ep_id)  FROM t_ep_i_att_1 ia1 WHERE (ia1.i_att_1 like '%_40' or ia1.t_ep_i_att_1_ep_id = 0) AND UPPER (ia1.i_att_1) = UPPER(NVL(LTRIM(RTRIM(c.promoted_product)), '0'))) promoted_product, 
         claim_hand_fee claim_case_deal_hf_d, claim_fixed_hand_fee claim_coop_hf,
         currency, 
         (SELECT MAX(cd.cd_deal_type_id) FROM tbl_cd_deal_type cd WHERE UPPER (cd.cd_deal_type_desc) = UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None'))) deal_type_cd_claim, 
         (SELECT MAX(coop.coop_deal_type_id) FROM tbl_coop_deal_type coop WHERE UPPER (coop.coop_deal_type_desc) = UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None'))) deal_type_coop_claim, 
         Link_Case_Deal case_Deal,Link_Unit_Deal unit_Deal, 
         link_handling_fee unit_hf_deal, customer_promo_no,  INV_DATE, Null, Null
         FROM t_src_claims_au c);

   COMMIT;

------------------------
-- Insert into Settlement Table
------------------------
   INSERT INTO settlement
               (settlement_id, settlement_code, settlement_desc, date_posted,
                start_date, end_date, settlement_type_id,
                settlement_status_id, promotion_id, gl_code_id, 
                bill_to, key_account_state,
                key_account, corporate_customer, customer_name, brand,
                promotion_group, promoted_product, settlement_owner_id,
                claim_case_deal_d, co_op_$, gst,
                settlement_amount, last_update_date,
                claim_case_deal_hf_d, claim_coop_hf, 
                deal_type_cd_claim, deal_type_coop_claim, case_deal, unit_deal,
                unit_hf_deal, customer_promo_no, inv_date,t_ep_L_ATT_10_ep_id)
      (SELECT settlement_seq.NEXTVAL, settlement_seq.NEXTVAL, settlement_desc,
              SYSDATE, start_date, end_date, settlement_type_id, 1,
              NVL(promotion_id,0), gl_code_id, bill_to,key_account_state, --Modified as per INC0011212
              key_account, corporate_customer, customer_name, brand,
              promotion_group, promoted_product, 1, 
              claim_case_deal_d, co_op_$, gst,
              (NVL (claim_case_deal_d, 0) + NVL (co_op_$, 0) + NVL (gst, 0) + NVL(claim_case_deal_hf_d, 0) + NVL(claim_coop_hf, 0)),
              SYSDATE, claim_case_deal_hf_d, claim_coop_hf, 
              deal_type_cd_claim, deal_type_coop_claim, case_deal, unit_deal,
              unit_hf_deal, customer_promo_no, inv_date,17
         FROM t_src_claims_au_temp);

------------------------------------------
---Place Tables into Master Tables
------------------------------------------
   INSERT INTO t_src_claims_au_master                  
                (SELECT v_batch_id
                    claim_no, promotion_id, start_date, end_date, claim_type,
                    claim_cd, claim_coop, gst_amount, ap_ar_flag,
                    pay_to_supplier, pay_to_customer, key_account_state,
                    key_account, corporate_customer, customer_name, brand,
                    promotion_group, promoted_product, claim_hand_fee, claim_fixed_hand_fee,
                    Currency, Cd_Deal_Type, Coop_Deal_Type, Link_Unit_Deal, Link_Case_Deal,
                    link_handling_fee, customer_promo_no, INV_DATE
       FROM t_src_claims_au
      );
      
   COMMIT;

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_src_claims_au;

   UPDATE ExportSummaryMaster SET status = 'EC', RecordsProcessed = v_no_records_exp 
   WHERE batchheaderid = v_batch_id;

---------------------------
--Error Table into Master
---------------------------

   ----- Generate Batch Id
   SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;

   ----- Insert into ExportSummaryMaster
   INSERT INTO ExportSummaryMaster
               VALUES (v_batch_id , SYSDATE, 't_src_claims_au_err', 0, 'ES');
   COMMIT;
   
   INSERT INTO t_src_claims_au_err_master                  
                (SELECT v_batch_id
                    claim_no, promotion_id, start_date, end_date, claim_type,
                    claim_cd, claim_coop, gst_amount, ap_ar_flag,
                    pay_to_supplier, pay_to_customer, key_account_state,
                    key_account, corporate_customer, customer_name, brand,
                    promotion_group, promoted_product, claim_hand_fee, claim_fixed_hand_fee,
                    Currency, Cd_Deal_Type, Coop_Deal_Type, Link_Unit_Deal, Link_Case_Deal,
                    link_handling_fee, customer_promo_no, err_desc, INV_DATE
       FROM t_src_claims_au_err
      );
      
   COMMIT;

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_src_claims_au_err;

   UPDATE ExportSummaryMaster SET status = 'EC', RecordsProcessed = v_no_records_exp
   WHERE batchheaderid = v_batch_id;
   
   
   dynamic_ddl ('drop table T_SRC_CLAIMS_AU_BKP purge');
   dynamic_ddl ('create table T_SRC_CLAIMS_AU_BKP as (select * from T_SRC_CLAIMS_AU)');
   dynamic_ddl ('truncate table T_SRC_CLAIMS_AU');


 --   MERGE INTO settlement s
--    USING(SELECT /*+ full(s) parallel(s,32) full(m) prallel(m, 32) */
--                 settlement_id, 
--                 MAX(m.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
--                 MAX(m.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id, 
--                 MAX(m.t_ep_e1_cust_cat_2_ep_id) t_ep_e1_cust_cat_2_ep_id
--          FROM settlement s,
--          mdp_matrix m
--          WHERE 1 = 1
--          AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
--          AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
--          AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_l_att_6_ep_id, s.corporate_customer)
--          AND m.t_ep_e1_cust_cat_4_ep_id = DECODE (NVL (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
--          AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
--          AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
--          AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
--          AND NVL(s.rr_default_claim, 0) = 0
--          AND ( NVL(s.t_ep_l_att_10_ep_id, 0) = 0
--                OR NVL(s.t_ep_e1_cust_cat_2_ep_id, 0) = 0
--                OR NVL(s.t_ep_ebs_customer_ep_id, 0) = 0
--               )
--          AND   NVL (s.key_account_state, 0)
--                + NVL (s.key_account, 0)
--                + NVL (s.corporate_customer, 0)
--                + NVL (s.customer_name, 0) 
--                + NVL (s.brand, 0) 
--                + NVL (s.promotion_group, 0) 
--                + NVL (s.promoted_product, 0) > 0
--          AND s.settlement_status_id IN (1, 10)
--          GROUP BY s.settlement_id) s1
--    ON(s.settlement_id = s1.settlement_id)
--    WHEN MATCHED THEN 
--    UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id,
--    s.t_ep_e1_cust_cat_2_ep_id = s1.t_ep_e1_cust_cat_2_ep_id,
--    s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id;
    
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 


PROCEDURE prc_load_claims
IS

    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER; 
    sql_str            VARCHAR2 (30000);
   
    v_rec_count        NUMBER;
    v_batch_id         INTEGER;
    v_no_records_exp   INTEGER;
    
    sql_str1   VARCHAR2 (100);
    sql_str2  VARCHAR2 (100);
    sql_str3  VARCHAR2 (100);
    sql_str4  VARCHAR2 (100);

BEGIN

    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_CLAIMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
   dynamic_ddl ('truncate table T_SRC_CLAIMS_TEMP');
   dynamic_ddl ('truncate table T_SRC_CLAIMS_AGG_TEMP');
   dynamic_ddl ('truncate table T_SRC_CLAIMS_ERR');

---------------------------------------------------------------
-- Claim Validation
---------------------------------------------------------------

insert into t_src_claims_master
select * from t_src_claims;

commit;

   ---Validate Levels to Demantra Levels. If fail, insert into ERROR table
   INSERT INTO t_src_claims_err
               (CLAIM_NO,CUSTOMER_CLAIM_NO,START_DATE,END_DATE,CLAIM_TYPE,
                CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT,
                TTL_GST,PAY_TO_CUSTOMER,CURRENCY,LINK_UNIT_DEAL,LINK_CASE_DEAL,
                CUSTOMER_PROMO_NO,INVOICE_NO,INVOICE_DATE,BRAND2,TAX_CLASS_MAT,
                MATERIAL,BANNER,CUSTOMER,CUSTOMER_LEVEL3,CUSTOMER_LEVEL4,
                SALES_OFFICE,PROMO_GROUP)
      SELECT CLAIM_NO,CUSTOMER_CLAIM_NO,START_DATE,END_DATE,CLAIM_TYPE,
                CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT,
                TTL_GST,PAY_TO_CUSTOMER,CURRENCY,LINK_UNIT_DEAL,LINK_CASE_DEAL,
                CUSTOMER_PROMO_NO,INVOICE_NO,INVOICE_DATE,BRAND2,TAX_CLASS_MAT,
                MATERIAL,BANNER,CUSTOMER,CUSTOMER_LEVEL3,CUSTOMER_LEVEL4,
                SALES_OFFICE,PROMO_GROUP
        FROM t_src_claims c
       WHERE NOT (1 = 1
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL4)), '0')) in (select UPPER (CS.EBS_ACCOUNT) from T_EP_EBS_ACCOUNT CS)
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL3)), '0')) in (select UPPER (EC.EBS_CUSTOMER) from T_EP_EBS_CUSTOMER EC)
                  and UPPER(NVL(LTRIM(RTRIM(C.SALES_OFFICE)), '0')) in (select UPPER (LA6.L_ATT_6) from T_EP_L_ATT_6 LA6)                  
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER)), '0')) in (select UPPER (CC4.L_ATT_2) from t_ep_L_ATT_2 CC4)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.brand2)), '0')) IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)               
                  AND UPPER(NVL(LTRIM(RTRIM(C.PROMO_GROUP)), '0')) IN (SELECT UPPER (P2B.P2B) FROM T_EP_P2B P2B)                  
                  and UPPER(NVL(LTRIM(RTRIM(C.MATERIAL)), '0'))||'-30110101' in (select UPPER (IA1.I_ATT_1) from T_EP_I_ATT_1 IA1 /*where ia1.i_att_1 like '%_10' or t_ep_i_att_1_ep_id = 0*/ )               
                  AND UPPER(NVL(LTRIM(RTRIM(C.banner)), '0')) IN (SELECT UPPER (z.ebs_tp_zone) FROM t_ep_ebs_tp_zone z)        --BANNER Changes          
                  AND UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT')) IN (SELECT UPPER (ST.SETTLEMENT_TYPE_CODE) FROM SETTLEMENT_TYPE ST)                 
              --    AND UPPER(LTRIM(RTRIM (c.ap_ar_flag))) IN ('AP', 'AR') 
          --        AND c.end_date < SYSDATE 
                  AND c.end_date >= c.start_date 
--                  AND ((LTRIM (RTRIM (C.AP_AR_FLAG)) = 'AR' AND PAY_TO_CUSTOMER IS NOT NULL)
--                       --OR (LTRIM (RTRIM (c.ap_ar_flag)) = 'AP' AND pay_to_supplier IS NOT NULL)
--                      )
                  AND ( UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) IN (SELECT UPPER (pc.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 pc)
                        OR  UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) = '0')
--                  AND( UPPER(NVL(LTRIM (RTRIM (C.PAY_TO_SUPPLIER)), '0')) IN (SELECT UPPER (PS.CUSTOMER_DESC) FROM T_SRC_VENDOR PS WHERE PS.TYPE = 'AP' AND UPPER(PS.CURRENCY) = 'NZD')    
--                        --OR  UPPER(NVLa(LTRIM (RTRIM (C.PAY_TO_SUPPLIER)), '0')) = '0'
--                        )
--                  AND UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) IN (SELECT UPPER (cd.cd_deal_type_desc) FROM tbl_cd_deal_type cd)                                                                                     
--                  AND UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) IN (SELECT UPPER (coop.coop_deal_type_desc) FROM tbl_coop_deal_type coop)                                                                                     
--                  AND NOT (    C.PAY_TO_CUSTOMER IS NOT NULL
                         --  AND c.pay_to_supplier IS NOT NULL
--                          )       
                   AND NOT (  UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT')) in 'MARGIN SUPPORT' and  nvl(c.coop_amount,0) <> 0 )          
--                  AND (  NVL(C.CUSTOMER_PROMO_NO,0) = 0
--                       OR C.CUSTOMER_PROMO_NO IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
--                       )
                       AND claim_no is NOT NULL
                );

   COMMIT;

   SELECT COUNT (*)
     INTO v_rec_count
     FROM t_src_claims_err;

   IF v_rec_count > 0
   THEN
      --Clear from Staging table the errored lines
      DELETE FROM t_src_claims c
            WHERE NOT (1 = 1
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL4)), '0')) in (select UPPER (CS.EBS_ACCOUNT) from T_EP_EBS_ACCOUNT CS)
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL3)), '0')) in (select UPPER (EC.EBS_CUSTOMER) from T_EP_EBS_CUSTOMER EC)
                  and UPPER(NVL(LTRIM(RTRIM(C.SALES_OFFICE)), '0')) in (select UPPER (LA6.L_ATT_6) from T_EP_L_ATT_6 LA6)                  
                  and UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER)), '0')) in (select UPPER (CC4.L_ATT_2) from t_ep_L_ATT_2 CC4)                  
                  AND UPPER(NVL(LTRIM(RTRIM(c.brand2)), '0')) IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)               
                  AND UPPER(NVL(LTRIM(RTRIM(C.PROMO_GROUP)), '0')) IN (SELECT UPPER (P2B.P2B) FROM T_EP_P2B P2B)                  
                  and UPPER(NVL(LTRIM(RTRIM(C.MATERIAL)), '0'))||'-30110101' in (select UPPER (IA1.I_ATT_1) from T_EP_I_ATT_1 IA1 /*where ia1.i_att_1 like '%_10' or t_ep_i_att_1_ep_id = 0*/ )               
                  AND UPPER(NVL(LTRIM(RTRIM(C.banner)), '0')) IN (SELECT UPPER (z.ebs_tp_zone) FROM t_ep_ebs_tp_zone z)             --BANNER Changes      
                  AND UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT')) IN (SELECT UPPER (ST.SETTLEMENT_TYPE_CODE) FROM SETTLEMENT_TYPE ST)                 
              --    AND UPPER(LTRIM(RTRIM (c.ap_ar_flag))) IN ('AP', 'AR') 
          --        AND c.end_date < SYSDATE 
                  AND c.end_date >= c.start_date 
--                  AND ((LTRIM (RTRIM (C.AP_AR_FLAG)) = 'AR' AND PAY_TO_CUSTOMER IS NOT NULL)
--                       --OR (LTRIM (RTRIM (c.ap_ar_flag)) = 'AP' AND pay_to_supplier IS NOT NULL)
--                      )
                  AND ( UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) IN (SELECT UPPER (pc.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 pc)
                        OR  UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')) = '0')
--                  AND( UPPER(NVL(LTRIM (RTRIM (C.PAY_TO_SUPPLIER)), '0')) IN (SELECT UPPER (PS.CUSTOMER_DESC) FROM T_SRC_VENDOR PS WHERE PS.TYPE = 'AP' AND UPPER(PS.CURRENCY) = 'NZD')    
--                        --OR  UPPER(NVL(LTRIM (RTRIM (C.PAY_TO_SUPPLIER)), '0')) = '0'
--                        )
--                  AND UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) IN (SELECT UPPER (cd.cd_deal_type_desc) FROM tbl_cd_deal_type cd)                                                                                     
--                  AND UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) IN (SELECT UPPER (coop.coop_deal_type_desc) FROM tbl_coop_deal_type coop)                                                                                     
         --         AND NOT (    C.PAY_TO_CUSTOMER IS NOT NULL
                         --  AND c.pay_to_supplier IS NOT NULL
         --                 )       
                   AND NOT (  UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT')) in 'MARGIN SUPPORT' and  nvl(c.coop_amount,0) <> 0 )          
--                  AND (  NVL(C.CUSTOMER_PROMO_NO,0) = 0
--                       OR C.CUSTOMER_PROMO_NO IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
--                       )
                       AND claim_no is NOT NULL
                );

      COMMIT;

----------------------------
--Fill in Error Message in T_SRC_CLAIMS_ERR
----------------------------
      UPDATE t_src_claims_err c
         SET err_desc = 'CUSTOMER LEVEL4 does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.CUSTOMER_LEVEL4)), '0')) NOT IN (SELECT UPPER (cs.ebs_account) FROM t_ep_ebs_account cs)
         AND err_desc IS NULL;

      COMMIT;
      
 -- Added to handle null value --    SR --  
       UPDATE t_src_claims_err c
         SET err_desc = 'CLAIM_NO IS NULL'
       WHERE CLAIM_NO IS NULL
         AND err_desc IS NULL;
         
         COMMIT;

-- SR --

    UPDATE t_src_claims_err c
         SET err_desc = 'CUSTOMER LEVEL3 does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.CUSTOMER_LEVEL3)), '0')) NOT IN (SELECT UPPER (ec.ebs_customer) FROM t_ep_ebs_customer ec)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE T_SRC_CLAIMS_ERR C
         SET err_desc = 'Sales Office does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.sales_office)), '0')) NOT IN (SELECT UPPER (la6.l_att_6) FROM t_ep_l_att_6 la6)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET err_desc = 'Customer Name does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.customer)), '0')) NOT IN (SELECT UPPER (cc4.L_ATT_2) FROM t_ep_L_ATT_2 cc4)
         AND err_desc IS NULL;

      COMMIT;
      
       update T_SRC_CLAIMS_ERR C
         set ERR_DESC = 'Banner Group does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.banner)), '0')) NOT IN (SELECT UPPER (cc4.ebs_tp_zone) FROM t_ep_ebs_tp_zone cc4) --BANNER Changes
         AND err_desc IS NULL;

      COMMIT;

      UPDATE T_SRC_CLAIMS_ERR C
         SET ERR_DESC = 'Product Brand Level 2 does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.brand2)), '0')) NOT IN (SELECT UPPER (ic4.e1_item_cat_4) FROM t_ep_e1_item_cat_4 ic4)                  
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET err_desc = 'Promotion Group does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.PROMO_GROUP)), '0')) NOT IN (SELECT UPPER (p2b.p2b) FROM t_ep_p2b p2b)                  
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET ERR_DESC = 'Material does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM(c.MATERIAL)), '0'))||'-30110101' NOT IN (SELECT UPPER (ia1.i_att_1) FROM t_ep_i_att_1 ia1  where ia1.i_att_1 like '%_10' or ia1.t_ep_i_att_1_ep_id = 0)
         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET err_desc = 'Claim Type does not exist in Demantra'
       WHERE UPPER (NVL (c.claim_type, 'DEFAULT SETTLEMENT TYPE')) NOT IN (SELECT UPPER (st.settlement_type_code) FROM settlement_type st)
         AND err_desc IS NULL;

      COMMIT;

--      UPDATE t_src_claims_err c
--         SET err_desc = 'AP / AR Flag is not valid'
--       WHERE UPPER (c.ap_ar_flag) NOT IN ('AP', 'AR') AND err_desc IS NULL;
--
--      COMMIT;

    --  UPDATE t_src_claims_err c
    --     SET err_desc = 'Claim Can not be entered for Future'
    --   WHERE NOT c.end_date < SYSDATE AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET err_desc = 'Start Date can not be greater than the End Date'
       WHERE NOT c.end_date >= c.start_date AND err_desc IS NULL;

      COMMIT;

--      UPDATE t_src_claims_err c
--         SET err_desc =
--                'AR entered with No Pay to Customer or AP was entered with to Pay to Supplier'
--       WHERE NOT (   (C.AP_AR_FLAG = 'AR' AND PAY_TO_CUSTOMER IS NOT NULL)
--                 -- OR (c.ap_ar_flag = 'AP' AND pay_to_supplier IS NOT NULL)
--                 )
--         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET err_desc = 'Pay to Customer does not exist in Demantra'
       WHERE UPPER(NVL(LTRIM(RTRIM (C.PAY_TO_CUSTOMER)), '0')) NOT IN (SELECT UPPER (PC.e1_cust_cat_4) FROM t_ep_e1_cust_cat_4 PC)
        -- AND c.ap_ar_flag = 'AR'
         AND err_desc IS NULL;

      COMMIT;

--      UPDATE t_src_claims_err c
--         SET err_desc = 'Pay to Supplier does not exist in Demantra'
--       WHERE UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0')) NOT IN (SELECT UPPER (ps.customer_desc) FROM t_src_vendor ps WHERE ps.TYPE = 'AP' AND upper(ps.currency) = 'NZD') 
--         AND c.ap_ar_flag = 'AP'
--         AND err_desc IS NULL;

      COMMIT;
      
--      UPDATE t_src_claims_err c
--         SET err_desc = 'CD deal type does not exist in Demantra'
--       WHERE UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None')) NOT IN (SELECT UPPER (cd.deal_type_desc) FROM tbl_deal_type cd)                                                                                     
--         AND err_desc IS NULL;

      COMMIT;
      
--      UPDATE t_src_claims_err c
--         SET err_desc = 'COOP deal type does not exist in Demantra'
--       WHERE UPPER(NVL(LTRIM (RTRIM (c.coop_deal_type)), 'None')) NOT IN (SELECT UPPER (coop.deal_type_desc) FROM tbl_deal_type coop)
--         AND err_desc IS NULL;

      COMMIT;

      UPDATE t_src_claims_err c
         SET ERR_DESC =
                'Margin Support claims cannot have Lumpsum amount '
       WHERE   UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT')) IN 'MARGIN SUPPORT' 
               and  nvl(c.coop_amount,0) <> 0    
         AND err_desc IS NULL;

      COMMIT;

--      UPDATE t_src_claims_err c
--         SET ERR_DESC = 'Promotion ID is not an open/Valid promotion'
--       WHERE NOT (nvl(c.CUSTOMER_PROMO_NO,0) = 0
--                       OR c.CUSTOMER_PROMO_NO IN (SELECT promotion_id FROM promotion WHERE promotion_stat_id IN (4, 5, 6, 7))
--                       )
--         AND err_desc IS NULL;

      COMMIT;
   END IF;

---------------------------------------------------------------------
--Convert from t_src_claims to t_src_claims_temp. Conver to ep_ids
---------------------------------------------------------------------
   INSERT INTO T_SRC_CLAIMS_TEMP 
   (SELECT LTRIM(RTRIM(CLAIM_NO)) SETTLEMENT_DESC, 
      ltrim(rtrim(CUSTOMER_CLAIM_NO)) rr_remitt_no,customer_promo_no, start_date, end_date, 
       (SELECT MAX(ST.SETTLEMENT_TYPE_ID) FROM SETTLEMENT_TYPE ST WHERE UPPER (ST.SETTLEMENT_TYPE_CODE) = UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT'))) SETTLEMENT_TYPE_ID,
         CASE_DEAL_AMOUNT CLAIM_CASE_DEAL_D, COOP_AMOUNT CO_OP_$, TTL_GST GST, 
          (SELECT MAX(PC.t_ep_e1_cust_cat_4_ep_id)  FROM t_ep_e1_cust_cat_4 PC WHERE UPPER (PC.e1_cust_cat_4) = UPPER(NVL(LTRIM(RTRIM (C.PAY_TO_CUSTOMER)), '0'))) BILL_TO,
         (SELECT MAX(CS.T_EP_EBS_ACCOUNT_EP_ID)  FROM T_EP_EBS_ACCOUNT CS WHERE UPPER (CS.EBS_ACCOUNT) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL4)), '0')))KEY_ACCOUNT_STATE,
         (select max(EC.T_EP_EBS_CUSTOMER_EP_ID)  from T_EP_EBS_CUSTOMER EC where UPPER (EC.EBS_CUSTOMER) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL3)), '0'))) KEY_ACCOUNT, 
         (select max(LA6.T_EP_L_ATT_6_EP_ID)  from T_EP_L_ATT_6 LA6 where UPPER (LA6.L_ATT_6) = UPPER(NVL(LTRIM(RTRIM(C.SALES_OFFICE)), '0'))) SUB_BANNER, 
         (select max(LA6.T_EP_EBS_TP_ZONE_EP_ID)  from T_EP_EBS_TP_ZONE LA6 where UPPER (LA6.EBS_TP_ZONE) = UPPER(NVL(LTRIM(RTRIM(C.BANNER)), '0'))) CORPORATE_CUSTOMER, --BANNER Changes
         (select max(CC4.T_EP_E1_CUST_CAT_4_EP_ID)  from T_EP_e1_cust_cat_4 CC4 where UPPER (CC4.E1_CUST_CAT_4) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER)), '0'))) CUSTOMER_NAME, 
         (SELECT MAX(ic4.t_ep_e1_item_cat_4_ep_id) FROM t_ep_e1_item_cat_4 ic4 WHERE UPPER (ic4.e1_item_cat_4) = UPPER(NVL(LTRIM(RTRIM(c.brand2)), '0'))) brand,
         (SELECT MAX(P2B.T_EP_P2B_EP_ID)  FROM T_EP_P2B P2B WHERE UPPER (P2B.P2B) = UPPER(NVL(LTRIM(RTRIM(C.PROMO_GROUP)), '0'))) PROMOTION_GROUP, 
         (SELECT MAX(IA1.T_EP_I_ATT_1_EP_ID)  FROM T_EP_I_ATT_1 IA1 WHERE UPPER (IA1.I_ATT_1) = UPPER(NVL(LTRIM(RTRIM(C.material))||'-30110101', '0'))) PROMOTED_PRODUCT, 
         (SELECT MAX(cd.deal_type_id) FROM tbl_deal_type cd WHERE UPPER (cd.deal_type_desc) = UPPER(NVL(LTRIM (RTRIM (c.CD_DEAL_TYPE)), 'None'))) deal_type_cd_claim, 
         (SELECT MAX(COOP.DEAL_TYPE_ID) FROM TBL_DEAL_TYPE COOP WHERE UPPER (COOP.DEAL_TYPE_DESC) = UPPER(NVL(LTRIM (RTRIM (C.COOP_DEAL_TYPE)), 'None'))) DEAL_TYPE_COOP_CLAIM, 
         LINK_CASE_DEAL CASE_DEAL,LINK_UNIT_DEAL UNIT_DEAL,
         INVOICE_DATE
         FROM t_src_claims c);
--      (SELECT LTRIM(RTRIM(CLAIM_NO)) SETTLEMENT_DESC, 
--      ltrim(rtrim(CUSTOMER_CLAIM_NO))) rr_remitt_no,customer_promo_no, start_date, end_date, 
--       (SELECT MAX(ST.SETTLEMENT_TYPE_ID) FROM SETTLEMENT_TYPE ST WHERE UPPER (ST.SETTLEMENT_TYPE_DESC) = UPPER(NVL(LTRIM(RTRIM(C.CLAIM_TYPE)), 'DEFAULT SETTLEMENT TYPE'))) SETTLEMENT_TYPE_ID,
--         CASE_DEAL_AMOUNT CLAIM_CASE_DEAL_D, COOP_AMOUNT CO_OP_$, TTL_GST GST, 
--         0 gl_code_id,
--        -- (SELECT MAX(gl_code_id) FROM gl_code gc WHERE UPPER(gc.gl_code_desc) = UPPER(c.ap_ar_flag)) gl_code_id,
----         CASE WHEN ap_ar_flag = 'AR' THEN (SELECT MAX(pc.vendor_id)  FROM t_src_vendor pc WHERE pc.TYPE = 'AR' AND upper(pc.currency) = 'NZD' AND UPPER (pc.customer_desc) = UPPER(NVL(LTRIM(RTRIM (c.pay_to_customer)), '0')))  
----              WHEN ap_ar_flag = 'AP' THEN (SELECT MAX(ps.vendor_id) FROM t_src_vendor ps WHERE ps.type = 'AP' AND upper(ps.currency) = 'NZD' AND UPPER (ps.customer_desc) = UPPER(NVL(LTRIM (RTRIM (c.pay_to_supplier)), '0'))) 
----              ELSE NULL
----          END 
--          (SELECT MAX(PC.VENDOR_ID)  FROM T_SRC_VENDOR PC WHERE PC.TYPE = 'AR' AND UPPER(PC.CURRENCY) = 'NZD' AND UPPER (PC.CUSTOMER_DESC) = UPPER(NVL(LTRIM(RTRIM (C.PAY_TO_CUSTOMER)), '0'))) BILL_TO,
--         (SELECT MAX(CS.T_EP_EBS_ACCOUNT_EP_ID)  FROM T_EP_EBS_ACCOUNT CS WHERE UPPER (CS.EBS_ACCOUNT_DESC) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL4)), 'Default CustomerState')))KEY_ACCOUNT_STATE,
--         (SELECT MAX(EC.T_EP_EBS_CUSTOMER_EP_ID)  FROM T_EP_EBS_CUSTOMER EC WHERE UPPER (EC.EBS_CUSTOMER_DESC) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER_LEVEL3)), 'Default Customer'))) KEY_ACCOUNT, 
--         (SELECT MAX(la6.t_ep_l_att_6_ep_id)  FROM t_ep_l_att_6 la6 WHERE UPPER (la6.l_att_6_desc) = UPPER(NVL(LTRIM(RTRIM(c.sales_office)), 'unassosiated'))) corporate_customer, 
--         (SELECT MAX(CC4.T_EP_E1_CUST_CAT_4_EP_ID)  FROM T_EP_E1_CUST_CAT_4 CC4 WHERE UPPER (CC4.E1_CUST_CAT_4) = UPPER(NVL(LTRIM(RTRIM(C.CUSTOMER)), '0'))) CUSTOMER_NAME, 
--         (SELECT MAX(ic4.t_ep_e1_item_cat_4_ep_id) FROM t_ep_e1_item_cat_4 ic4 WHERE UPPER (ic4.e1_i_cat_desc_4) = UPPER(NVL(LTRIM(RTRIM(c.brand2)), 'unassociated'))) brand,
--         (SELECT MAX(p2b.t_ep_p2b_ep_id)  FROM t_ep_p2b p2b WHERE UPPER (p2b.p2b_desc) = UPPER(NVL(LTRIM(RTRIM(c.promo_group)), 'Default'))) promotion_group, 
--         (SELECT MAX(IA1.T_EP_I_ATT_1_EP_ID)  FROM T_EP_I_ATT_1 IA1 WHERE (IA1.I_ATT_1 LIKE '%_10' OR IA1.T_EP_I_ATT_1_EP_ID = 0) AND UPPER (IA1.I_ATT_1_DESC) = UPPER(NVL(LTRIM(RTRIM(C.material)), '0'))) PROMOTED_PRODUCT, 
--         --claim_hand_fee claim_case_deal_hf_d, --claim_fixed_hand_fee claim_coop_hf,
--         0,0,
--         currency, 
--         (SELECT MAX(cd.cd_deal_type_id) FROM tbl_cd_deal_type cd WHERE UPPER (cd.cd_deal_type_desc) = UPPER(NVL(LTRIM (RTRIM (c.cd_deal_type)), 'None'))) deal_type_cd_claim, 
--         (SELECT MAX(COOP.COOP_DEAL_TYPE_ID) FROM TBL_COOP_DEAL_TYPE COOP WHERE UPPER (COOP.COOP_DEAL_TYPE_DESC) = UPPER(NVL(LTRIM (RTRIM (C.COOP_DEAL_TYPE)), 'None'))) DEAL_TYPE_COOP_CLAIM, 
--         LINK_CASE_DEAL CASE_DEAL,LINK_UNIT_DEAL UNIT_DEAL, 
--         0,
--         --LINK_HANDLING_FEE UNIT_HF_DEAL, 
--         customer_promo_no,  INVoice_DATE
--         FROM t_src_claims c);

   COMMIT;
-------------------------
-- Aggregate Claim Lines
-------------------------
   INSERT INTO T_SRC_CLAIMS_AGG_TEMP
   select settlement_desc,RR_REMITT_NO,customer_promo_no,start_date,end_date,settlement_type_id,
          sum(claim_case_deal_d),sum(co_op_$),sum(gst),bill_to,
          KEY_ACCOUNT_STATE,KEY_ACCOUNT,SUB_BANNER,CORPORATE_CUSTOMER,CUSTOMER_NAME,
          brand,promotion_group,promoted_product,DEAL_TYPE_CD_CLAIM,DEAL_TYPE_COOP_CLAIM,
          case_deal,UNIT_DEAL,invoice_date
     from T_SRC_CLAIMS_TEMP
 group by settlement_desc,RR_REMITT_NO,customer_promo_no,start_date,end_date,settlement_type_id,
          bill_to, KEY_ACCOUNT_STATE,KEY_ACCOUNT,SUB_BANNER,CORPORATE_CUSTOMER,CUSTOMER_NAME,
          brand,promotion_group,promoted_product,DEAL_TYPE_CD_CLAIM,DEAL_TYPE_COOP_CLAIM,
          case_deal,UNIT_DEAL,invoice_date;
   

------------------------
-- Insert into Settlement Table
------------------------
   INSERT INTO settlement
               (settlement_id, settlement_code, settlement_desc, date_posted,
                start_date, end_date, settlement_type_id,
                settlement_status_id,  gl_code_id, 
                BILL_TO, KEY_ACCOUNT_STATE,rr_remit_num,
                key_account, corporate_customer, customer_name, brand,sub_banner,
                promotion_group, promoted_product, settlement_owner_id,
                claim_case_deal_d, co_op_$, gst,
                settlement_amount, last_update_date, 
                deal_type_cd_claim, deal_type_coop_claim, case_deal, unit_deal,
                 customer_promo_no, inv_date,t_ep_L_ATT_10_ep_id,t_ep_ebs_customer_ep_id)
      (SELECT settlement_seq.NEXTVAL, settlement_seq.NEXTVAL, settlement_desc,
              SYSDATE, start_date, end_date, settlement_type_id, 1,
               0, BILL_TO,KEY_ACCOUNT_STATE, rr_remitt_no,
              key_account, corporate_customer, customer_name, brand,sub_banner,
              promotion_group, promoted_product, 1, 
              CLAIM_CASE_DEAL_D, CO_OP_$, GST,
              (NVL (claim_case_deal_d, 0) + NVL (co_op_$, 0) + NVL (gst, 0)), 
              SYSDATE, DEAL_TYPE_CD_CLAIM, DEAL_TYPE_COOP_CLAIM, CASE_DEAL, UNIT_DEAL,
               customer_promo_no, invoice_date,1, key_account
         FROM T_SRC_CLAIMS_AGG_TEMP);
---------------------------------------------------------------
--Updating Cateogory and sub category
---------------------------------------------------------------

--MERGE INTO settlement s
--     USING (SELECT                            /*+ parallel(sm, 32) full(sm) */
--                  s.settlement_id,
--                   MAX (i.t_ep_p2a1_ep_id) t_ep_p2a1_ep_id,
--                   MAX (i.t_ep_p2a_ep_id) t_ep_p2a_ep_id
--              FROM settlement s, items i        --rr_settlement_matrix sm
--             WHERE 1 = 1
--                   AND NVL (s.brand, 0) = CASE NVL (s.brand, 0)
--                          WHEN 0 THEN  NVL (s.brand, 0)
--                          ELSE  i.t_ep_e1_item_cat_4_ep_id
--                       END
--                   AND NVL (s.promotion_group, 0)=CASE NVL (s.promotion_group, 0)
--                          WHEN 0 THEN NVL (s.promotion_group, 0)
--                          ELSE  i.t_ep_p2b_ep_id
--                       END
--                   AND NVL (s.promoted_product, 0) = CASE NVL (s.promoted_product, 0)
--                          WHEN 0 THEN NVL (s.promoted_product, 0)
--                          ELSE  i.t_ep_i_att_1_ep_id
--                       END
--                   AND TRUNC (s.LAST_UPDATE_DATE) BETWEEN TRUNC (SYSDATE - 1)
--                                                    AND TRUNC (SYSDATE)
--                   AND (   NVL (s.brand, 0) <> 0
--                        OR NVL (s.promotion_group, 0) <> 0
--                        OR NVL (s.promoted_product, 0) <> 0) group by s.settlement_id) s1
--        ON (s.settlement_id = s1.settlement_id)
--WHEN MATCHED
--THEN
--   UPDATE SET
--      s.t_ep_p2a1_ep_id = s1.t_ep_p2a1_ep_id,
--      s.t_ep_p2a_ep_id = s1.t_ep_p2a_ep_id;
--      
--      commit;
/*FOR rec IN (SELECT settlement_id,
                      promoted_product,
                      promotion_group,
                      brand
                 FROM settlement s
                WHERE TRUNC (LAST_UPDATE_DATE) between  TRUNC (SYSDATE -1) and TRUNC (SYSDATE) and  (NVL (s.brand, 0) <> 0
                     or NVL (s.promotion_group, 0) <> 0
                     or NVL (s.promoted_product, 0) <> 0)
                ) --this condition so that we run it for the claims load same day only
   LOOP
      SELECT CASE
                WHEN NVL (rec.promoted_product, 0) = 0 THEN NULL
                ELSE ' AND s.promoted_product = i.t_ep_i_att_1_ep_id '
             END
        INTO sql_str1
        FROM DUAL;

      SELECT CASE
                WHEN NVL (rec.promotion_group, 0) = 0 THEN NULL
                ELSE ' AND s.promotion_group = i.t_ep_p2b_ep_id '
             END
        INTO sql_str2
        FROM DUAL;

      SELECT CASE
                WHEN NVL (rec.brand, 0) = 0 THEN NULL
                ELSE ' AND s.brand = i.t_ep_e1_item_cat_4_ep_id '
             END
        INTO sql_str3
        FROM DUAL;

      SELECT CASE
                WHEN     NVL (rec.brand, 0) = 0
                     AND NVL (rec.promotion_group, 0) = 0
                     AND NVL (rec.promoted_product, 0) = 0
                THEN
                   'And 1=2'
                ELSE
                   NULL
             END
        INTO sql_str4
        FROM DUAL;

      sql_str :=
         'MERGE INTO settlement s1
                 USING (SELECT      
                              max(i.t_ep_p2a1_ep_id) t_ep_p2a1_ep_id, max(i.t_ep_p2a_ep_id) T_EP_P2A_EP_ID, s.settlement_id
                          FROM settlement s, items i
                         WHERE s.settlement_id = ';
      sql_str := sql_str || rec.settlement_id;
      sql_str := sql_str || sql_str1 || sql_str2 || sql_str3 || sql_str4;
      sql_str :=
         sql_str
         || ' group by s.settlement_id ) s2
                    ON (s1.settlement_id = s2.settlement_id)
            WHEN MATCHED
            THEN
               UPDATE SET
                  s1 .t_ep_p2a1_ep_id= s2.t_ep_p2a1_ep_id,
                  s1.t_ep_p2a_ep_id = s2.t_ep_p2a_ep_id';
      dynamic_ddl (sql_str);
      COMMIT;
   END LOOP;
*/
------------------------------------------
---Place Tables into Master Tables
------------------------------------------
--   INSERT INTO t_src_claims_master                  
--                (SELECT v_batch_id
--                    claim_no, promotion_id, start_date, end_date, claim_type,
--                    claim_cd, claim_coop, gst_amount, ap_ar_flag,
--                    pay_to_supplier, pay_to_customer, key_account_state,
--                    key_account, corporate_customer, customer_name, brand,
--                    promotion_group, promoted_product, claim_hand_fee, claim_fixed_hand_fee,
--                    CURRENCY, CD_DEAL_TYPE, COOP_DEAL_TYPE, LINK_UNIT_DEAL, LINK_CASE_DEAL,
--                    --link_handling_fee, 
--                    customer_promo_no, INVOICE_DATE
--       FROM t_src_claims
--      );
      
   COMMIT;

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_src_claims;

   UPDATE ExportSummaryMaster SET status = 'EC', RecordsProcessed = v_no_records_exp 
   WHERE batchheaderid = v_batch_id;

---------------------------
--Error Table into Master
---------------------------

   ----- Generate Batch Id
   SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;

   ----- Insert into ExportSummaryMaster
   INSERT INTO ExportSummaryMaster
               VALUES (v_batch_id , SYSDATE, 't_src_claims_err', 0, 'ES');
   COMMIT;
   
   INSERT INTO T_SRC_CLAIMS_ERR_MASTER(BATCH_ID,CLAIM_NO,CUSTOMER_CLAIM_NO,START_DATE,END_DATE,
                                       CLAIM_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,
                                       COOP_AMOUNT,TTL_GST,PAY_TO_CUSTOMER,CURRENCY,LINK_UNIT_DEAL,
                                       LINK_CASE_DEAL,CUSTOMER_PROMO_NO,INVOICE_NO,INVOICE_DATE,
                                       BRAND2,TAX_CLASS_MAT,MATERIAL,BANNER,CUSTOMER,CUSTOMER_LEVEL3,
                                       CUSTOMER_LEVEL4,SALES_OFFICE,PROMO_GROUP,ERR_DESC)                  
                (SELECT v_batch_id batch_id,
                    CLAIM_NO,CUSTOMER_CLAIM_NO,START_DATE,END_DATE,
                                       CLAIM_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,
                                       COOP_AMOUNT,TTL_GST,PAY_TO_CUSTOMER,CURRENCY,LINK_UNIT_DEAL,
                                       LINK_CASE_DEAL,CUSTOMER_PROMO_NO,INVOICE_NO,INVOICE_DATE,
                                       BRAND2,TAX_CLASS_MAT,MATERIAL,BANNER,CUSTOMER,CUSTOMER_LEVEL3,
                                       CUSTOMER_LEVEL4,SALES_OFFICE,PROMO_GROUP,ERR_DESC
       FROM t_src_claims_err
      );
      
   COMMIT;

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_src_claims_err;

   UPDATE ExportSummaryMaster SET status = 'EC', RecordsProcessed = v_no_records_exp
   WHERE batchheaderid = v_batch_id;
   
   
   dynamic_ddl ('drop table T_SRC_CLAIMS_BKP purge');
   dynamic_ddl ('create table T_SRC_CLAIMS_BKP as (select * from T_SRC_CLAIMS)');
   dynamic_ddl ('truncate table T_SRC_CLAIMS');


 --   MERGE INTO settlement s
--    USING(SELECT /*+ full(s) parallel(s,32) full(m) prallel(m, 32) */
--                 settlement_id, 
--                 MAX(m.t_ep_ebs_customer_ep_id) t_ep_ebs_customer_ep_id,
--                 MAX(m.t_ep_l_att_10_ep_id) t_ep_l_att_10_ep_id, 
--                 MAX(m.t_ep_e1_cust_cat_2_ep_id) t_ep_e1_cust_cat_2_ep_id
--          FROM settlement s,
--          mdp_matrix m
--          WHERE 1 = 1
--          AND m.t_ep_ebs_account_ep_id = DECODE (NVL (s.key_account_state, 0), 0, m.t_ep_ebs_account_ep_id, s.key_account_state)
--          AND m.t_ep_ebs_customer_ep_id = DECODE (NVL (s.key_account, 0), 0, m.t_ep_ebs_customer_ep_id, s.key_account)
--          AND m.t_ep_l_att_6_ep_id = DECODE (NVL (s.corporate_customer, 0), 0, m.t_ep_l_att_6_ep_id, s.corporate_customer)
--          AND m.t_ep_e1_cust_cat_4_ep_id = DECODE (NVL (s.customer_name, 0), 0, m.t_ep_e1_cust_cat_4_ep_id, s.customer_name)
--          AND m.t_ep_e1_item_cat_4_ep_id = DECODE (NVL (s.brand, 0), 0, m.t_ep_e1_item_cat_4_ep_id, s.brand)
--          AND m.t_ep_p2b_ep_id = DECODE (NVL (s.promotion_group, 0), 0, m.t_ep_p2b_ep_id, s.promotion_group)
--          AND m.t_ep_i_att_1_ep_id = DECODE (NVL (s.promoted_product, 0), 0, m.t_ep_i_att_1_ep_id, s.promoted_product)
--          AND NVL(s.rr_default_claim, 0) = 0
--          AND ( NVL(s.t_ep_l_att_10_ep_id, 0) = 0
--                OR NVL(s.t_ep_e1_cust_cat_2_ep_id, 0) = 0
--                OR NVL(s.t_ep_ebs_customer_ep_id, 0) = 0
--               )
--          AND   NVL (s.key_account_state, 0)
--                + NVL (s.key_account, 0)
--                + NVL (s.corporate_customer, 0)
--                + NVL (s.customer_name, 0) 
--                + NVL (s.brand, 0) 
--                + NVL (s.promotion_group, 0) 
--                + NVL (s.promoted_product, 0) > 0
--          AND s.settlement_status_id IN (1, 10)
--          GROUP BY s.settlement_id) s1
--    ON(s.settlement_id = s1.settlement_id)
--    WHEN MATCHED THEN 
--    UPDATE SET s.t_ep_l_att_10_ep_id = s1.t_ep_l_att_10_ep_id,
--    s.t_ep_e1_cust_cat_2_ep_id = s1.t_ep_e1_cust_cat_2_ep_id,
--    s.t_ep_ebs_customer_ep_id = s1.t_ep_ebs_customer_ep_id;
    
    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END; 


  PROCEDURE prc_disable_logging AS
  /**************************************************************************
  NAME: PRC_DISABLE_LOGGING
  PURPOSE: To disable logging in sales_data table

  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_DISABLE_LOGGING';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    BEGIN 
    EXECUTE IMMEDIATE 'alter table sales_data nologging'; 
    EXECUTE IMMEDIATE 'alter table promotion_data nologging'; 
    EXECUTE IMMEDIATE 'alter table mdp_matrix nologging';     
    EXECUTE IMMEDIATE 'alter table promotion_matrix nologging'; 
    END;

    COMMIT; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END;


  PROCEDURE prc_enable_logging AS
  /**************************************************************************
  NAME: PRC_DISABLE_LOGGING
  PURPOSE: To disable logging in sales_data table

  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_ENABLE_LOGGING';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    BEGIN 
    EXECUTE IMMEDIATE 'alter table sales_data logging'; 
    EXECUTE IMMEDIATE 'alter table promotion_data logging'; 
    EXECUTE IMMEDIATE 'alter table mdp_matrix logging';     
    EXECUTE IMMEDIATE 'alter table promotion_matrix logging'; 
    END;

    COMMIT; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END;

PROCEDURE prc_cl_trunc_apo_fcst_stg
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER; 
    v_cnt NUMBER;

BEGIN

    pre_logon;
    
    select count(1) into v_cnt from T_SRC_SAPL_APO_FCST_TMPL;
    
    V_STATUS := 'start Num rows : = '||v_cnt;
    v_prog_name := 'PRC_CL_TRUNC_APO_FCST_STG';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    
    
   dynamic_ddl ('truncate table T_SRC_SAPL_APO_FCST_TMPL');   
   
   
   COMMIT;
   
   select count(1) into v_cnt from T_SRC_SAPL_APO_FCST_TMPL;
   
   v_status := 'end Num Rows : = '||v_cnt;
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;


PROCEDURE prc_insert_new_apo_combs
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;   
    v_count NUMBER;

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_INSERT_NEW_APO_COMBS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



   dynamic_ddl ('truncate table BIIO_APO_FCST');   
   dynamic_ddl ('truncate table BIIO_APO_FCST_ERR');
   
   COMMIT;
   
   check_and_drop('RR_BIIO_APO_FCST');
dynamic_ddl('create table RR_BIIO_APO_FCST AS
   SELECT SKU_CODE||''-30110101'' sku_code ,CL3_CODE,SALES_OFFICE,SUM(APO_BASELINE_FCST) RR_APO_FCST_OFFSET
   FROM T_SRC_SAPL_APO_FCST_TMPL /*where nvl(apo_baseline_fcst,0) <> 0 */ group by SKU_CODE ,CL3_CODE,SALES_OFFICE');
   
  dynamic_ddl('INSERT INTO BIIO_APO_FCST(SDATE,LEVEL1,LEVEL2,LEVEL3,RR_APO_FCST_OFFSET)
   SELECT t1.WEEK_START_DATE,t1.SKU_CODE||''-30110101'' ,t1.CL3_CODE,t1.SALES_OFFICE,SUM(t1.APO_BASELINE_FCST)
   FROM T_SRC_SAPL_APO_FCST_TMPL t1
   where t1.rr_apo_fcst_offset <> 0 group by t1.WEEK_START_DATE,t1.SKU_CODE ,t1.CL3_CODE,t1.SALES_OFFICE');
   
   COMMIT;
   
    check_and_drop('rr_apo_npd_combs_t');
dynamic_ddl ('CREATE TABLE rr_apo_npd_combs_t AS
             SELECT
           /*+ FULL(imp) PARALLEL(imp, 64) */
           DISTINCT i.item_id,
           l.t_ep_ebs_customer_ep_id,
           l.t_ep_l_att_6_ep_id
         FROM RR_BIIO_APO_FCST imp,
           items i,
           location l,
           t_ep_item itm,
           t_ep_ebs_customer cl3,
           t_ep_l_att_6 l6
         WHERE 1                         = 1
         AND imp.sku_code                = itm.item
         AND imp.cl3_Code                = CL3.EBS_CUSTOMER
         AND imp.sales_office            = l6.l_att_6
         AND itm.t_ep_item_ep_id         = i.t_ep_item_ep_id
         AND cl3.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
         AND l6.t_Ep_l_att_6_ep_id       = l.t_ep_l_att_6_ep_id
         AND imp.RR_APO_FCST_OFFSET <> 0
         AND NOT EXISTS
           (SELECT 1
           FROM mdp_matrix m
           WHERE m.item_id   = i.item_id
           AND m.t_ep_ebs_customer_ep_id = l.t_ep_ebs_customer_ep_id
           AND m.t_Ep_l_att_6_ep_id = l.t_Ep_l_att_6_ep_id
           )');
COMMIT;
       
check_and_drop('rr_tmp_apo_new_loc_t');
dynamic_ddl ('     
         CREATE TABLE rr_tmp_apo_new_loc_t AS
         SELECT DISTINCT t_ep_ebs_customer_ep_id,
           t_ep_l_att_6_ep_id,
           location_id
         FROM
           (SELECT t_ep_ebs_customer_ep_id,
             t_ep_l_att_6_ep_id,
             location_id,
             RANK() OVER ( PARTITION BY t_ep_ebs_customer_ep_id,t_ep_l_att_6_ep_id ORDER BY num_rows DESC) row_rank
           FROM
             (SELECT t.t_ep_ebs_customer_ep_id,
               t.t_ep_l_att_6_ep_id,
               sd.location_id,
               COUNT(1) num_rows
             FROM sales_data sd,
               mdp_matrix m,
               rr_apo_npd_combs_t t
             WHERE sd.item_id   = m.item_id
             AND sd.location_id = m.location_id
               --and sd.item_id = t.item_id
             AND m.t_ep_l_att_6_ep_id      = t.t_ep_l_att_6_ep_id
             AND m.t_ep_ebs_customer_ep_id = t.t_ep_ebs_customer_ep_id
             AND sd.sales_date BETWEEN sysdate-52*7 AND sysdate
             GROUP BY t.t_ep_ebs_customer_ep_id,
               t.t_ep_l_att_6_ep_id,
               sd.location_id
             ORDER BY COUNT(1) DESC
             )
           )
         WHERE row_rank = 1 ')
;
   COMMIT;        
   dynamic_ddl ('truncate table mdp_load_assist');
          dynamic_ddl
          ( 'INSERT /*+ APPEND NOLOGGING */ INTO mdp_load_assist(item_id, location_id, is_fictive, boolean_qty)
            (SELECT t2.item_id,
                    t1.location_id,
                    0,0
             FROM rr_tmp_apo_new_loc_t t1,
               rr_apo_npd_combs_t t2
             WHERE t1.t_ep_ebs_customer_ep_id = t2.t_ep_ebs_customer_ep_id
               AND t1.t_ep_l_att_6_ep_id        = t2.t_ep_l_att_6_ep_id
            )'
           );
           
          COMMIT;

          SELECT COUNT (1)
          INTO v_count
          FROM mdp_load_assist;
          
          --dbms_output.put_line('Count is ' || v_count);
           v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  'Count is '|| v_count || SYSDATE);

          IF v_count > 0
          THEN
            PKG_COMMON_UTILS.PRC_MDP_ADD ('mdp_load_assist');
      --      dynamic_ddl ('truncate table mdp_load_assist');
          END IF;


   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END PRC_INSERT_NEW_APO_COMBS;


PROCEDURE prc_cl_truncate_apo_fcst
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN

    pre_logon;
    V_STATUS := 'start ';
    v_prog_name := 'PRC_CL_TRUNCATE_APO_FCST';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



   dynamic_ddl ('truncate table BIIO_APO_FCST');   
   dynamic_ddl ('truncate table BIIO_APO_FCST_ERR');
   
   COMMIT;
   
   INSERT INTO BIIO_APO_FCST(SDATE,LEVEL1,LEVEL2,LEVEL3,RR_APO_FCST_OFFSET)
   SELECT WEEK_START_DATE,SKU_CODE||'-30110101' ,CL3_CODE,SALES_OFFICE,SUM(APO_BASELINE_FCST)
   FROM T_SRC_SAPL_APO_FCST_TMPL where nvl(apo_baseline_fcst,0) <> 0  group by WEEK_START_DATE,SKU_CODE ,CL3_CODE,SALES_OFFICE;

   COMMIT;
   
--   UPDATE BIIO_APO_FCST SET LEVEL1 = LEVEL1||'-30110101';
--   
--   COMMIT;
--   
   v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name,  v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;

END;

PROCEDURE prc_load_apo_fcst
IS

    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    

BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_APO_FCST';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
    pkg_common_utils.prc_start_wf_schema('ImportAPO');
     
    commit; 
    
   dynamic_ddl('TRUNCATE TABLE T_SRC_SAPL_APO_FCST_TMPL_BK2');
   
   dynamic_ddl('insert into T_SRC_SAPL_APO_FCST_TMPL_BK2 select * from T_SRC_SAPL_APO_FCST_TMPL');
   commit;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
END;

PROCEDURE prc_load_gst_rate AS
  /**************************************************************************
  NAME: PRC_LOAD_GST_RATE
  PURPOSE: To populate gst rate

  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_GST_RATE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   

    MERGE INTO t_ep_item i  
    USING(SELECT t.t_ep_i_att_6_ep_id , t.i_att_6, decode(t.i_att_6,0,0,1,s.pval) gst
          FROM t_ep_i_att_6 t, sys_params s
          WHERE s.pname='RR_GST_RATE'
          AND t.i_att_6 in ('0','1')) t6
    ON (i.t_ep_i_att_6_ep_id = t6.t_ep_i_att_6_ep_id)
    WHEN MATCHED THEN 
    UPDATE SET i.e1_prim_plan_factor = t6.gst;

    COMMIT; 
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END; 
  
  PROCEDURE prc_update_rates_roll AS
  /**************************************************************************
  NAME: PRC_UPDATE_RATES_ROLL
  PURPOSE: To populate rates into the future
  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    v_max_sales_date    DATE;
    
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_UPDATE_RATES_ROLL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
       get_param ('sys_params', 'max_sales_date', v_max_sales_date);
   
    
       -- To popoulate List Price into future --

     --   dynamic_ddl('TRUNCATE TABLE rr_list_price_roll_t');
                         
    --    INSERT /*+ APPEND NOLOGGING */ INTO rr_list_price_roll_t       
    --    SELECT   sd1.item_id, sd1.location_id, 
    --    MAX (sales_date) sales_date         
    --    FROM sales_data sd1
    --    WHERE (sd1.INCR_EVT_D_RTL_SD > 0
    --    AND nvl(sd1.actual_quantity,0) > 0)
    --    AND sales_date BETWEEN v_max_sales_date - (52 * 7)  and  v_max_sales_date
    --    GROUP BY sd1.item_id, sd1.location_id; 
        
     --   COMMIT;

        MERGE /*+ FULL(sd) PARALLEL(sd, 16) */ INTO sales_data sd
        USING(SELECT   sd.item_id, sd.location_id, sd.sales_date, sd.INCR_EVT_D_RTL_SD/sd.actual_quantity list_price
        FROM rr_list_price_roll_t s, 
        sales_data sd
        WHERE s.item_id = sd.item_id 
        AND s.location_id = sd.location_id
        AND s.sales_date = sd.sales_date 
         ) sd1
        ON(sd.item_id = sd1.item_id
        AND sd.location_id = sd1.location_id
        AND sd.sales_date between sd1.sales_date +(78*7) + 1 and sd1.sales_date + (105 * 7))
        WHEN MATCHED THEN 
        UPDATE SET sd.ebspricelist109 = sd1.list_price,
                     sd.last_update_date = sysdate
       WHERE nvl(sd.ebspricelist109, 0) <> nvl(sd1.list_price, 0);

       COMMIT;
       
   --           dynamic_ddl('TRUNCATE TABLE rr_war_price_roll_t');
                         
   --     INSERT /*+ APPEND NOLOGGING */ INTO rr_war_price_roll_t       
   --     SELECT   sd1.item_id, sd1.location_id, 
   --     MAX (sales_date) sales_date         
   --     FROM sales_data sd1
   --     WHERE (sd1.sdata15 > 0
   --     AND nvl(sd1.actual_quantity,0) > 0)
   --     AND sales_date BETWEEN v_max_sales_date - (52 * 7)  and  v_max_sales_date
   --     GROUP BY sd1.item_id, sd1.location_id;
        
   --     COMMIT;

        MERGE /*+ FULL(sd) PARALLEL(sd, 16) */ INTO sales_data sd
        USING(SELECT   sd.item_id, sd.location_id, sd.sales_date, sd.sdata15/sd.actual_quantity war_price
        FROM rr_war_price_roll_t s, 
        sales_data sd
        WHERE s.item_id = sd.item_id 
        AND s.location_id = sd.location_id
        AND s.sales_date = sd.sales_date 
         ) sd1
        ON(sd.item_id = sd1.item_id
        AND sd.location_id = sd1.location_id
        AND sd.sales_date between sd1.sales_date + (78*7) + 1 and sd1.sales_date + (105 * 7))
        WHEN MATCHED THEN 
        UPDATE SET sd.ebspricelist110 = sd1.war_price,
                     sd.last_update_date = sysdate
       WHERE nvl(sd.ebspricelist110, 0) <> nvl(sd1.war_price, 0);

       COMMIT;
    
      
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END; 
  
  PROCEDURE prc_load_price_gsv AS
  /**************************************************************************
  NAME: PRC_UPDATE_RATES_ROLL
  PURPOSE: To populate rates into the future
  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
  --AND to_char(sd.sales_date,''MONYYYY'' ) = '''||rec.month||rechdr.years||''' )
  --AND sd.sales_date between to_date(''01-'||rec.month||'-'||rechdr.years||''','''||'DD-MON-YYYY'||''')- 7 AND  to_date(''28-'||rec.month||'-'||rechdr.years||''','''||'DD-MON-YYYY'||''') + 10)  
**************************************************************************/
    v_prog_name            VARCHAR2 (100);
    v_status               VARCHAR2 (100);
    v_proc_log_id          NUMBER;    
    v_max_sales_date    DATE;
    vn_batch_id            NUMBER;
    vn_user_id            NUMBER;
    vn_count            NUMBER;
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PRICE_GSV';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'max_sales_date', v_max_sales_date);
    
    -- populate the batch_id,record_id
    SELECT COUNT(1) INTO vn_count FROM RR_PRICE_GSV;
    --
    IF vn_count > 0 THEN
        SELECT CONTRACT_BATCH_ID.nextval INTO vn_batch_id FROM DUAL;
        SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
        -- Update the batch_id/Reccord_id
        UPDATE /*+ full(ct) parallel(ct,16) */ RR_PRICE_GSV ct
        SET batch_id             = vn_batch_id,
            created_by           = vn_user_id,
            record_id            = CONTRACT_RECORD_ID.nextval,
            creation_date        = sysdate,
            last_updated_by      = vn_user_id,
            last_update_date     = sysdate;
        --
    END IF;
    
    check_and_drop('rr_update_comb_t1');
    dynamic_ddl ('CREATE TABLE rr_update_comb_t1 (item_id number,location_id number,sales_date date,price number) ');
    COMMIT;
    
    FOR rechdr IN (select distinct years from RR_PRICE_GSV order by years)
    LOOP
        v_status := 'LOAD Start - '|| rechdr.years ||'';
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
        --
        FOR rec IN (SELECT month from RR_MONTH)
        LOOP
        --
        -- create temp table to hold the month wise data
        dynamic_ddl ('INSERT INTO rr_update_comb_t1
        SELECT
                     m.item_id,
                     m.location_id,
                     i.datet sales_date,
                     g.'||rec.month||' gsv
          FROM RR_PRICE_GSV         g, 
               inputs               i,
               mdp_matrix           m,
               t_ep_ebs_customer    cl3,
               t_ep_L_ATT_6         so,
               t_ep_item            mt
          WHERE to_char(i.datet,''MONYYYY'' ) = '''||rec.month||rechdr.years||'''     
          AND m.t_ep_ebs_customer_ep_id       = cl3.t_ep_ebs_customer_ep_id
          AND m.t_ep_item_ep_id               = mt.t_ep_item_ep_id
          AND m.t_ep_L_ATT_6_ep_id            = so.t_ep_L_ATT_6_ep_id
          AND cl3.ebs_customer                = g.CL3
          AND mt.dm_item_desc                 = g.PACKL4||''-30110101''
          AND so.L_ATT_6                      = g.salesoffice
          AND g.years                           = '''||rechdr.years||'''
          ');
        --
        COMMIT;
        --
        END LOOP;
        --
    END LOOP;

    dynamic_ddl('MERGE /*+ APPEND PARALLEL index_ffs(sd sales_data_pk) parallel_index(sd sales_data_pk) */ INTO SALES_DATA sd 
    USING(SELECT item_id,location_id,sales_date,max(price) price from rr_update_comb_t1 group by item_id,location_id,sales_date)tmp
    ON (sd.location_id         = tmp.location_id
        AND sd.item_id         = tmp.item_id 
        AND sd.sales_date     = tmp.sales_date)
    WHEN MATCHED THEN UPDATE SET
    ebspricelist110         = tmp.price,
    last_update_date         = sysdate
    WHERE nvl(ebspricelist110,0) <> tmp.price');

    commit;
    --    
    -- Archive the Processed batch
    INSERT INTO RR_PRICE_GSV_ARCHIVE
    SELECT /*+ full(ct) parallel(ct,16) */ * FROM RR_PRICE_GSV ct;
    
    DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_GSV');
    
    COMMIT;
    --
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_GSV');
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END; 
  
  PROCEDURE prc_load_price_LP AS
/**************************************************************************
  NAME: PRC_UPDATE_RATES_ROLL
  PURPOSE: To populate rates into the future
  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
**************************************************************************/
    v_prog_name            VARCHAR2 (100);
    v_status               VARCHAR2 (100);
    v_proc_log_id          NUMBER;    
    v_max_sales_date    DATE;
    vn_batch_id            NUMBER;
     vn_user_id                NUMBER;
     vn_count                NUMBER;
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PRICE_LP';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'max_sales_date', v_max_sales_date);
    
    -- populate the batch_id,record_id
    SELECT COUNT(1) INTO vn_count FROM RR_PRICE_LP;
    --
    IF vn_count > 0 THEN
        SELECT CONTRACT_BATCH_ID.nextval INTO vn_batch_id FROM DUAL;
        SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
        -- Update the batch_id/Reccord_id
        UPDATE /*+ full(ct) parallel(ct,16) */ RR_PRICE_LP ct
        SET batch_id             = vn_batch_id,
            created_by           = vn_user_id,
            record_id            = CONTRACT_RECORD_ID.nextval,
            creation_date        = sysdate,
            last_updated_by      = vn_user_id,
            last_update_date     = sysdate;
        --
    END IF;
    check_and_drop('rr_update_comb_t2');
    dynamic_ddl ('CREATE TABLE rr_update_comb_t2 (item_id number,location_id number,sales_date date,price number)');
    COMMIT;
    
    FOR rechdr IN (select distinct years from RR_PRICE_LP order by years)
    LOOP
        v_status := 'LOAD Start - '|| rechdr.years ||'';
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
        --
        FOR rec IN (SELECT month from RR_MONTH)
        LOOP
        --
        -- create temp table to hold the month wise data
      dynamic_ddl ('INSERT INTO rr_update_comb_t2
        SELECT 
                     m.item_id,
                     m.location_id,
                     i.datet sales_date,
                     g.'||rec.month||' lp
          FROM RR_PRICE_LP             g, 
               inputs               i,
               mdp_matrix           m,
               t_ep_ebs_customer    cl3,
               t_ep_L_ATT_6         so,
               t_ep_item            mt
          WHERE to_char(i.datet,''MONYYYY'' ) = '''||rec.month||rechdr.years||'''     
          AND m.t_ep_ebs_customer_ep_id       = cl3.t_ep_ebs_customer_ep_id
          AND m.t_ep_item_ep_id               = mt.t_ep_item_ep_id
          AND m.t_ep_L_ATT_6_ep_id            = so.t_ep_L_ATT_6_ep_id
          AND cl3.ebs_customer                = g.CL3
          AND mt.dm_item_desc                 = g.PACKL4||''-30110101''
          AND so.L_ATT_6                      = g.salesoffice
          AND g.years                                        = '''||rechdr.years||'''
          ');
        --
        COMMIT;
    
        --
        END LOOP;
        --
    END LOOP;

    dynamic_ddl('MERGE /*+ APPEND PARALLEL index_ffs(sd sales_data_pk) parallel_index(sd sales_data_pk) */ INTO SALES_DATA sd 
    USING(SELECT item_id,location_id,sales_date,max(price) price from rr_update_comb_t2 group by item_id,location_id,sales_date)tmp
    ON (sd.location_id         = tmp.location_id
        AND sd.item_id         = tmp.item_id 
        AND sd.sales_date     = tmp.sales_date)
    WHEN MATCHED THEN UPDATE SET
    ebspricelist109         = tmp.price,
    last_update_date         = sysdate
    WHERE nvl(ebspricelist109,0) <> tmp.price');

    commit;
    --    
    -- Archive the Processed batch
    INSERT INTO RR_PRICE_LP_ARCHIVE
    SELECT /*+ full(ct) parallel(ct,16) */ * FROM RR_PRICE_LP ct;
    
    DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_LP');
    
    COMMIT;
    --
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_LP');
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END; 
  
  PROCEDURE prc_load_price_sdc AS
  /**************************************************************************
  NAME: PRC_UPDATE_RATES_ROLL
  PURPOSE: To populate rates into the future
  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
**************************************************************************/
    v_prog_name    VARCHAR2 (100);
    v_status       VARCHAR2 (100);
    v_proc_log_id  NUMBER;    
    v_max_sales_date    DATE;
    vn_count            NUMBER;
    vn_batch_id            NUMBER;
    vn_user_id            NUMBER;
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_LOAD_PRICE_SDC';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'max_sales_date', v_max_sales_date);
 
    -- populate the batch_id,record_id
    SELECT COUNT(1) INTO vn_count FROM RR_PRICE_SDC;
    --
    IF vn_count > 0 THEN
        SELECT CONTRACT_BATCH_ID.nextval INTO vn_batch_id FROM DUAL;
        SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
        -- Update the batch_id/Reccord_id
        UPDATE /*+ full(ct) parallel(ct,16) */ RR_PRICE_SDC ct
        SET batch_id             = vn_batch_id,
            created_by           = vn_user_id,
            record_id            = CONTRACT_RECORD_ID.nextval,
            creation_date        = sysdate,
            last_updated_by      = vn_user_id,
            last_update_date     = sysdate;
        --
    END IF;
    check_and_drop('rr_update_comb_t');
    dynamic_ddl ('CREATE TABLE rr_update_comb_t (item_id number,location_id number,sales_date date,price number) ');
    COMMIT;
    
    FOR rechdr IN (select distinct years from RR_PRICE_SDC order by years)
    LOOP
        --
        v_status := 'LOAD Start - '|| rechdr.years ||'';
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);    
        --
        FOR rec IN (SELECT month from RR_MONTH)
        LOOP
        
        -- create temp table to hold the month wise data
        dynamic_ddl ('INSERT INTO rr_update_comb_t
        SELECT 
             m.item_id,
             m.location_id,
             i.datet sales_date, 
             g.'||rec.month||' sdc
        FROM RR_PRICE_SDC         g,
            inputs                 i,
            mdp_matrix             m,
            t_ep_L_ATT_6         so,
            t_ep_item             mt
        WHERE to_char(i.datet,''MONYYYY'' ) = '''||rec.month||rechdr.years||'''  
        AND m.t_ep_item_ep_id                 = mt.t_ep_item_ep_id
        AND m.t_ep_L_ATT_6_ep_id             = so.t_ep_L_ATT_6_ep_id
        AND mt.dm_item_desc                 = g.PACKL4||''-30110101''
        AND so.L_ATT_6                      = g.salesoffice
        AND g.years                         = '''||rechdr.years||'''
        ');
        
        COMMIT;
        --
        END LOOP;
        --
    END LOOP;
            
    dynamic_ddl('MERGE /*+ APPEND PARALLEL index_ffs(sd sales_data_pk) parallel_index(sd sales_data_pk) */ INTO SALES_DATA sd 
    USING(SELECT item_id,
    location_id,
    sales_date,
    max(price) price
    from rr_update_comb_t
    group by item_id,location_id,sales_date
    )g
    on (sd.location_id = g.location_id
    AND sd.item_id        = g.item_id 
    AND sd.sales_date  = g.sales_date)
    WHEN MATCHED THEN UPDATE SET
    ebspricelist126 = g.price,
    last_update_date = sysdate
    WHERE nvl(ebspricelist126,0) <> g.price');
    --
    COMMIT;
 
    -- Archive the Processed batch
    INSERT INTO RR_PRICE_SDC_ARCHIVE
    SELECT /*+ full(ct) parallel(ct,16) */ * FROM RR_PRICE_SDC ct;
    
    DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_SDC');
    
    COMMIT;
 
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        DYNAMIC_DDL('TRUNCATE TABLE RR_PRICE_SDC');
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END; 

  PROCEDURE prc_export_ts_copa AS
  /**************************************************************************
  NAME: prc_export_ts_copa
  PURPOSE: To populate ts into the future for copa
  Ver Date Author
--------- ---------- -------------------------------------------------
  1.0 
 
  **************************************************************************/
    --
    v_prog_name            VARCHAR2 (100);
    v_status               VARCHAR2 (100);
    v_proc_log_id          NUMBER;    
    v_max_sales_date    DATE;
    vn_count            NUMBER;
    vn_batch_id            NUMBER;
    vn_user_id            NUMBER;
    vd_start_date        VARCHAR2(25);    
    vd_end_date            VARCHAR2(25);
    --
  BEGIN
    pre_logon;
    v_status := 'start ';
    v_prog_name := 'PRC_EXPORT_TS_COPA';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    get_param ('sys_params', 'max_sales_date', v_max_sales_date);
    
    SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
    
    select to_char(min(trunc(sysdate,'MM')),'MM-DD-YYYY'),to_char(last_day(add_months(trunc(sysdate),18)),'MM-DD-YYYY') into vd_start_date,vd_end_date from dual;
    
    dynamic_ddl('TRUNCATE TABLE RR_TS_COPA_TEMP');
    -- Populate the export data in the TEmp Table 
    dynamic_ddl('INSERT /*+ NOLOGGING APPEND */ INTO RR_TS_COPA_TEMP
    select /*+ APPEND PARALLEL index_ffs(sd sales_data_pk) parallel_index(sd sales_data_pk) */
    min(sales_date) sdate,
    tec.EBS_CUSTOMER level1, -- cl3 
    a6.L_ATT_6  level2,         -- salesofffice
    c4.E1_ITEM_CAT_4 level3,        -- brand2
    c7.E1_ITEM_CAT_7 level4,          -- pack3 
    safe_division(
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0))
    * greatest(nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)))        
    +
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0))
    * (nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist114, 0)))
    +        
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) *  
    greatest 
    ((LEAST (nvl(sd.ebspricelist110, 0) - 
              greatest (
                  greatest (nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)) + nvl(sd.ebspricelist113, 0),
                  ((nvl(sd.ebspricelist110, 0) - nvl(sd.ebspricelist113, 0)) * nvl(sd.ebspricelist114, 0)) + nvl(sd.ebspricelist113, 0)),
      nvl(sd.ebspricelist104, 999999) ) 
    *  nvl(sd.ebspricelist118, 0) ) , nvl(sd.ebspricelist117, 0))+ nvl(sd.ebspricelist116,0))        
    +
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) 
    * (greatest
      (least (nvl(sd.ebspricelist110, 0) - 
        greatest(
          greatest( nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)) + nvl(sd.ebspricelist113, 0) , 
          ((nvl(sd.ebspricelist110, 0) - nvl(sd.ebspricelist113, 0)) * nvl(sd.ebspricelist114,0)) + nvl(sd.ebspricelist113, 0)),
      nvl(sd.ebspricelist104, 999999))  * nvl(sd.mkt_acc3, 0) , nvl(sd.cust_input3, 0))) + nvl( sd.mkt_acc2, 0 )  )
    +
    sum( nvl(sd.ebspricelist115, 0)),
     sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) * nvl(sd.ebspricelist110, 0)),0)   RR_COPA_FIX_TRD,
     safe_division(
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) * 
    greatest(
    least (nvl(sd.ebspricelist110, 0) - 
        greatest(
          greatest( nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)) + nvl(sd.ebspricelist113, 0) , 
          ((nvl(sd.ebspricelist110, 0) - nvl(sd.ebspricelist113, 0)) * nvl(sd.ebspricelist114,0)) + nvl(sd.ebspricelist113, 0)),
      nvl(sd.ebspricelist104, 999999))  
    * nvl(sd.ff_input1, 0) , nvl(sd.ebspricelist122, 0)) + nvl( sd.ebspricelist121,0))
    +
    sum(nvl(sd.ebspricelist106, 0) + nvl(sd.ebspricelist107, 0) + 
    ((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) 
    * ( least (nvl(sd.ebspricelist110, 0) - 
        greatest(
          greatest( nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)) + nvl(sd.ebspricelist113, 0) , 
          ((nvl(sd.ebspricelist110, 0) - nvl(sd.ebspricelist113, 0)) * nvl(sd.ebspricelist114,0)) + nvl(sd.ebspricelist113, 0)),
      nvl(sd.ebspricelist104, 999999)) - 
           least (least (nvl(sd.ebspricelist110, 0) - 
        greatest(
          greatest( nvl(sd.ebspricelist110, 0) * nvl(sd.ebspricelist111, 0), nvl(sd.ebspricelist112, 0)) + nvl(sd.ebspricelist113, 0) , 
          ((nvl(sd.ebspricelist110, 0) - nvl(sd.ebspricelist113, 0)) * nvl(sd.ebspricelist114,0)) + nvl(sd.ebspricelist113, 0)),
      nvl(sd.ebspricelist104, 999999)) , nvl(sd.ebspricelist104, 999999)))))
    +
    sum(nvl(sd.si_acc2, 0) )
    +
    sum(nvl(sd.cust_input2, 0) + nvl(sd.tiv ,0))
    +  
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) 
    * greatest(0, nvl(sd.mkt_acc1, 0)))  
    +
    sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0))
    * nvl(sd.ebspricelist113, 0))
    +
    sum(nvl(sd.mkt_input3, 0) +
    (nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0))
    * (nvl(sd.mkt_input1, 0) + nvl(sd.mkt_input2, 0) )
    )
    ,sum((nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)) * nvl(sd.ebspricelist110, 0)),0)  RR_COPA_VAR_SPEND
    from sales_data sd,
    MDP_MATRIX             mdp,
    T_EP_EBS_CUSTOMER tec,
    T_EP_L_ATT_6  a6,
    T_EP_E1_ITEM_CAT_4 c4,
    T_EP_E1_ITEM_CAT_7 c7
    where  sd.item_id = mdp.item_id
    and sd.location_id = mdp.location_id
    and mdp.T_EP_EBS_CUSTOMER_EP_ID = tec.T_EP_EBS_CUSTOMER_EP_ID
    AND mdp.T_EP_L_ATT_6_EP_ID = a6.T_EP_L_ATT_6_EP_ID
    AND mdp.T_EP_E1_ITEM_CAT_4_EP_ID = c4.T_EP_E1_ITEM_CAT_4_EP_ID
    AND mdp.T_EP_E1_ITEM_CAT_7_EP_ID = c7.T_EP_E1_ITEM_CAT_7_EP_ID
    and sd.sales_date >= to_date('''||vd_start_date||''',''MM-DD-YYYY'')
    and sd.sales_date <= to_date('''||vd_end_date||''',''MM-DD-YYYY'')
    --and (sd.item_id,sd.location_id) In (
    --select item_id,location_id from mdp_matrix 
    --where T_EP_EBS_CUSTOMER_EP_ID = 59
    --and T_EP_L_ATT_6_EP_ID = 4
    --and T_EP_E1_ITEM_CAT_4_EP_ID = 58
    --and T_EP_E1_ITEM_CAT_7_EP_ID = 77)
    group by  to_char(sd.sales_date,''MMYYYY''),tec.EBS_CUSTOMER, a6.L_ATT_6,c4.E1_ITEM_CAT_4,c7.E1_ITEM_CAT_7
    ');
    --
    dynamic_ddl('TRUNCATE TABLE RR_EXPORT_TS_COPA');
    
    COMMIT;
    
    v_status := 'Insert Table over ';
    v_prog_name := 'PRC_EXPORT_TS_COPA';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    -- Push the data from the BIEO table to the staging table
    SELECT COUNT(1) INTO vn_count FROM RR_TS_COPA_TEMP;
    --
    IF vn_count > 0 THEN
        --
        FOR REC IN (select distinct to_char(trunc(sdate,'YYYY'),'YYYY') years from RR_TS_COPA_TEMP)
        LOOP
            --
            FOR rec1 IN (SELECT month from RR_MONTH)
            LOOP
                --
                dynamic_ddl('MERGE /*+ full(rr) parallel(rr,32) */ INTO RR_EXPORT_TS_COPA  rr
                USING (select level1,            -- CL3
                level2,                            -- sales office
                level3,                            -- BD2
                level4,                            -- pack3
                ''F''                              term,
                NVL(RR_COPA_FIX_TRD,0)             ts    
                from RR_TS_COPA_TEMP  b
                where to_char(b.sdate,''MONYYYY'') = '''||rec1.month||rec.years||''' 
                UNION
                select level1,                    -- CL3
                level2,                            -- sales office
                level3,                            -- BD2
                level4,                            -- pack3
                ''V''                             term,
                NVL(RR_COPA_VAR_SPEND,0)        ts    
                from RR_TS_COPA_TEMP b
                where to_char(b.sdate,''MONYYYY'') = '''||rec1.month||rec.years||'''
                ) tmp
                ON(rr.years                 = '''||rec.years||'''
                AND rr.CL3                     = tmp.level1                        -- cl3
                AND rr.salesoffice            = tmp.level2
                AND rr.brand2                 = tmp.level3
                AND rr.packl3                 = tmp.level4
                AND rr.term                 = tmp.term)
                WHEN MATCHED THEN UPDATE SET
                rr.'||rec1.month||'         = tmp.ts,
                last_update_date            = sysdate,
                last_updated_by                = '||vn_user_id||'
                --
                WHEN NOT MATCHED THEN INSERT
                (YEARS,CL3,PACKL3,SALESOFFICE,BRAND2,term,'||rec1.month||',CREATION_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY)
                VALUES
                ('''||rec.years||''',tmp.level1,tmp.level4,tmp.level2,tmp.level3,TMP.term,tmp.ts,SYSDATE,'||vn_user_id||',SYSDATE,'||vn_user_id||')
                ');
                --
                COMMIT;
                --
            END LOOP;
            --
        END LOOP;    
        --
    END IF;
    --- 
    v_status := 'Merge over ';
    v_prog_name := 'PRC_EXPORT_TS_COPA';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    -- populate the batch_id,record_id
    SELECT COUNT(1) INTO vn_count FROM RR_EXPORT_TS_COPA;
    --
    IF vn_count > 0 THEN
        SELECT CONTRACT_BATCH_ID.nextval INTO vn_batch_id FROM DUAL;
        --SELECT user_id INTO vn_user_id FROM user_id where user_name = 'EDI';
        -- Update the batch_id/Reccord_id
        UPDATE /*+ full(ct) parallel(ct,16) */ RR_EXPORT_TS_COPA ct
        SET batch_id             = vn_batch_id,
            record_id            = CONTRACT_RECORD_ID.nextval;
        --
        -- Archive the Processed batch
        INSERT /*+ NOLOGGING APPEND */ INTO RR_EXPORT_TS_COPA_ARCHIVE 
        SELECT /*+ full(ct) parallel(ct,16) */ * FROM RR_EXPORT_TS_COPA ct;
    
    END IF;
    
    COMMIT;
    --
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
        END;
    
  END;
 
END;

/
