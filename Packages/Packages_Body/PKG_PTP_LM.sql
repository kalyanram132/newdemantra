--------------------------------------------------------
--  DDL for Package Body PKG_PTP_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_PTP_LM" 
AS
/******************************************************************************
   NAME:         PKG_PTP_LM  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version
   
   ******************************************************************************/

  PROCEDURE prc_reopen_promo(user_id   NUMBER DEFAULT NULL,
                             level_id  NUMBER DEFAULT NULL,
                             member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_REOPEN_PROMO
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_day         VARCHAR2(10);
   v_eng_profile NUMBER := 1;

   v_prog_name    VARCHAR2 (100) := 'PRC_REOPEN_PROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;   
   
   v_count        NUMBER;
   
   v_batch_date     DATE := SYSDATE;
   
   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_REOPEN_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   SELECT COUNT(1) 
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);
   
   IF v_count <> 0 THEN
   
   UPDATE promotion
      SET promotion_stat_id = 7,
          last_update_date = SYSDATE,
          PROM_REOPEN_CLOSE_USR = user_id,
          PROM_REOPEN_CLOSE_DATE  = sysdate,
          rr_export_close = 0
   WHERE promotion_id = member_id
     AND scenario_id IN (22, 262, 162);

   UPDATE promotion_data
      SET promotion_stat_id = 7,
          last_update_date = SYSDATE
   WHERE promotion_id = member_id
     AND scenario_id IN (22, 262, 162);

   UPDATE promotion_matrix
      SET promotion_stat_id = 7,
          last_update_date = SYSDATE,
          promotion_stat_lud = SYSDATE,
          promotion_data_lud = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id IN (22, 262, 162);

   UPDATE accrual 
      SET accrual_stat_id = 7,
          last_update_date = SYSDATE
    WHERE accrual_id = member_id;   

   UPDATE accrual_matrix
      SET accrual_stat_id = 7,
          last_update_date = SYSDATE,
          accrual_data_lud = SYSDATE
    WHERE accrual_id = member_id;

   UPDATE accrual_data
      SET rr_accrual_close_cd_post_gl = 0 - rr_accrual_close_cd_ad_gl,
          rr_accrual_close_coop_post_gl = 0 - rr_accrual_close_coop_ad_gl,
          rr_accrual_close_hf_cd_post_gl = 0 - rr_accrual_close_hf_cd_ad_gl,
          rr_accrual_close_hf_cp_post_gl = 0 - rr_accrual_close_hf_coop_ad_gl,
          last_update_date = SYSDATE
    WHERE accrual_id = member_id;
    
    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;
    
    
    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');
    
    EXECUTE IMMEDIATE 'insert into T_EXP_GL(BATCH_ID, MATERIAL, CUSTOMER_NO, PROMOTION_ID,SALES_ORG,DIST_CHANNEL,DIVISION, SALES_DATE, CASE_DEAL_AMOUNT, COOP_AMOUNT, 
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    select '||V_BATCH_ID||', I.I_ATT_1 , S.E1_CUST_CAT_4, P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,    
    pd.sales_date, pd.rr_accrual_close_cd_post_gl, pd.rr_accrual_close_coop_post_gl,
    pt.promotion_type_desc,
    cdt.deal_type_code, coopt.deal_type_code, TO_DATE(''' || TO_CHAR(v_batch_date, 'DD/MM/YYYY HH24:MI:SS') || ''', ''DD/MM/YYYY HH24:MI:SS'') , ''CLOSE'',rec_cust.e1_cust_cat_4
    FROM T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    t_ep_p3 dc,
    PROMOTION P,
    activity ac,
    t_ep_e1_cust_cat_4 rec_cust,
    accrual_data pd,
    promotion_type pt,
    mdp_matrix mdp,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt 
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
    AND p.scenario_id IN (22, 262, 162)
    and p.activity_id = ac.activity_id
    and p.recipient = rec_cust.t_ep_e1_cust_cat_4_ep_id
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND accrual_id = ' || member_id || '
    AND p.promotion_type_id = pt.promotion_type_id
    AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
    AND ABS(NVL(pd.rr_accrual_close_cd_post_gl, 0) ) + 
        ABS(NVL(pd.rr_accrual_close_coop_post_gl, 0) )  <> 0';
   
   UPDATE accrual_data
      SET rr_accrual_close_cd_ad = 0,
          rr_accrual_close_coop_ad = 0,
          rr_accrual_close_hf_cd_ad = 0,
          rr_accrual_close_hf_coop_ad = 0,
          rr_accrual_close_cd_ad_gl = 0,
          rr_accrual_close_coop_ad_gl = 0,
          rr_accrual_close_hf_cd_ad_gl = 0,
          rr_accrual_close_hf_coop_ad_gl = 0,
          accrual_stat_id = 7,
          last_update_date = SYSDATE
    WHERE accrual_id = member_id;
    
    INSERT INTO t_exp_gl_master                   
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER 
                 FROM t_exp_gl WHERE batch_id = v_batch_id);

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

   COMMIT;
   
   END IF;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  
  PROCEDURE prc_close_promotion (user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL
)
IS
/******************************************************************************
   NAME:       PRC_CLOSE_PROMOTION
   PURPOSE:    Close the Promotion when user runs Close Promotion Method.


   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Inital

******************************************************************************/
   sql_str          VARCHAR2 (5000);
   v_prog_name      VARCHAR2 (100)  := 'PRC_CLOSE_PROMOTION';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   
   case_deal_oi     NUMBER;
   new_close_date   DATE;
   
   v_count          NUMBER;
   
BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CLOSE_PROMOTION';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

   SELECT COUNT(1) 
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);
   
   IF v_count <> 0 THEN

   UPDATE promotion
      SET promotion_stat_id = '8',
          last_update_date = SYSDATE,     
          promo_close_user = user_id,
          promo_close_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE promotion_data
      SET promotion_stat_id = '8',
          last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE promotion_matrix
      SET promotion_data_lud = SYSDATE,
          promotion_stat_id = '8', 
          last_update_date = SYSDATE 
    WHERE promotion_id = member_id
      AND scenario_id <> 265; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;
   
    UPDATE accrual
      SET accrual_stat_id = 8,
          last_update_date = SYSDATE
    WHERE accrual_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE accrual_data
      SET promotion_stat_id = 8,
          accrual_stat_id = 8,
          last_update_date = SYSDATE
    WHERE accrual_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   UPDATE accrual_matrix
      SET accrual_data_lud = SYSDATE,
          accrual_stat_id = 8, 
          last_update_date = SYSDATE 
    WHERE accrual_id = member_id; --AND promotion_stat_id IN (4, 5, 6, 7);

   COMMIT;

   -- Only delete rows for promotion that have not been posted to GL --
   FOR rec IN
      (SELECT DISTINCT m.item_id, m.location_id,
                       ((CASE
                            WHEN rr_alloc_wk6 > 0
                               THEN 42
                            WHEN rr_alloc_wk5 > 0
                               THEN 35
                            WHEN rr_alloc_wk4 > 0
                               THEN 28
                            WHEN rr_alloc_wk3 > 0
                               THEN 21
                            WHEN rr_alloc_wk2 > 0
                               THEN 14
                            WHEN rr_alloc_wk1 > 0
                               THEN 7
                            ELSE 0
                         END
                        )
                       ) allocation_date
                  FROM promotion_matrix m
                 WHERE (item_id, location_id, promotion_id) IN (
                                          SELECT DISTINCT item_id,
                                                          location_id,
                                                          promotion_id
                                                     FROM promotion_data
                                                    WHERE promotion_id =
                                                                     member_id
                                                      AND scenario_id <> 265))
   LOOP
      DELETE      /*+ nologging */FROM promotion_data pd
            WHERE promotion_id = member_id
              AND pd.item_id = rec.item_id
              AND pd.location_id = rec.location_id
              AND pd.sales_date > SYSDATE + rec.allocation_date
              AND (pd.promotion_id, pd.sales_date) NOT IN (
                     SELECT DISTINCT accrual_id, sales_date
                                FROM accrual_data
                               WHERE promotion_id = member_id
                                 AND (   ABS(NVL(rr_accrual_cd_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_coop_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_hf_cd_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_hf_coop_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_os_cd_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_os_coop_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_os_hf_cd_ad_gl, 0))
                                      + ABS(NVL(rr_accrual_os_hf_coop_ad_gl, 0))
                                      + ABS(NVL(rr_approved_claim_cd_ad, 0))
                                      + ABS(NVL(rr_approved_claim_coop_ad, 0))
                                      + ABS(NVL(rr_approved_claim_hf_cd_ad, 0))
                                      + ABS(NVL(rr_approved_claim_hf_coop_ad, 0))
                                     ) <> 0);
   END LOOP;

   MERGE INTO promotion_dates pd
      USING (SELECT   promotion_id, MAX (sales_date) new_close_date
                 FROM promotion_data
                WHERE promotion_id = member_id
                  AND scenario_id <> 265
             GROUP BY promotion_id) pd1
      ON (pd.promotion_id = pd1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET pd.until_date = NEXT_DAY (TO_CHAR (pd1.new_close_date, 'mm/dd/yyyy'), 'sunday')
            WHERE pd.until_date > TO_CHAR (SYSDATE, 'mm/dd/yyyy')
         ;
   COMMIT;
   
   
   MERGE INTO promotion_matrix pm
      USING (SELECT   promotion_id, location_id, item_id,
                      MAX (sales_date) new_close_date
                 FROM promotion_data
                WHERE promotion_id = member_id
                  AND scenario_id <> 265
             GROUP BY promotion_id, location_id, item_id) pd1
      ON (    pm.promotion_id = pd1.promotion_id
          AND pm.item_id = pd1.item_id
          AND pm.location_id = pd1.location_id)
      WHEN MATCHED THEN
         UPDATE
            SET pm.until_date = NEXT_DAY (TO_CHAR (pd1.new_close_date, 'mm/dd/yyyy'), 'SUNDAY')
            WHERE pm.until_date > TO_CHAR (SYSDATE, 'mm/dd/yyyy')
         ;
         
         COMMIT;
         
         
          FOR rec IN
      (SELECT DISTINCT m.item_id, m.location_id,
                       ((CASE
                            WHEN allocation_wk6 > 0
                               THEN 42
                            WHEN allocation_wk5 > 0
                               THEN 35
                            WHEN allocation_wk4 > 0
                               THEN 28
                            WHEN allocation_wk3 > 0
                               THEN 21
                            WHEN allocation_wk2 > 0
                               THEN 14
                            WHEN allocation_wk1 > 0
                               THEN 7
                            ELSE 0
                         END
                        )
                       ) allocation_date
                  FROM promotion_matrix m
                 WHERE (item_id, location_id, promotion_id) IN (
                            SELECT DISTINCT item_id, location_id,
                                            promotion_id
                                       FROM promotion_data
                                      WHERE promotion_id = member_id
                                        AND scenario_id <> 265))
   LOOP
      DELETE      /*+ nologging */FROM accrual_data pd
            WHERE pd.accrual_id = member_id
              AND pd.item_id = rec.item_id
              AND pd.location_id = rec.location_id
              AND sales_date > SYSDATE + rec.allocation_date
              AND (   ABS(NVL(rr_accrual_cd_ad_gl, 0))
                      + ABS(NVL(rr_accrual_coop_ad_gl, 0))
                      + ABS(NVL(rr_accrual_hf_cd_ad_gl, 0))
                      + ABS(NVL(rr_accrual_hf_coop_ad_gl, 0))
                      + ABS(NVL(rr_accrual_os_cd_ad_gl, 0))
                      + ABS(NVL(rr_accrual_os_coop_ad_gl, 0))
                      + ABS(NVL(rr_accrual_os_hf_cd_ad_gl, 0))
                      + ABS(NVL(rr_accrual_os_hf_coop_ad_gl, 0))
                      + ABS(NVL(rr_approved_claim_cd_ad, 0))
                      + ABS(NVL(rr_approved_claim_coop_ad, 0))
                      + ABS(NVL(rr_approved_claim_hf_cd_ad, 0))
                      + ABS(NVL(rr_approved_claim_hf_coop_ad, 0))
                      ) <> 0;
   END LOOP;
   
   END IF;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_create_promo_details (user_id     NUMBER DEFAULT NULL,
                                      level_id    NUMBER DEFAULT NULL,
                                      member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
   NAME:       PRC_CREATE_PROMO_DETAILS
   PURPOSE:    Create Promotion Runs this Procedure to Populate Who created the
               Promotion and When

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Initial

******************************************************************************/
   sql_str          VARCHAR2 (5000);
   v_prog_name      VARCHAR2 (100)  := 'PRC_CREATE_PROMO_DETAILS';
   v_status         VARCHAR2 (100);
   v_proc_log_id    NUMBER;
   v_cd_deal_type   NUMBER;
   v_coop_deal_type NUMBER;
   v_oi_deal_type   NUMBER;

BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CREATE_PROMO_DETAILS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

    SELECT cd_deal_type_id, coop_deal_type_id, oi_deal_type_id
    INTO v_cd_deal_type, v_coop_deal_type, v_oi_deal_type
    FROM promotion_type 
    WHERE promotion_type_id = (SELECT promotion_type_id FROM promotion WHERE promotion_id = member_id);
    
    UPDATE promotion
       SET promo_create_date = sysdate,
           promo_create_user = user_id,
           cd_deal_type = v_cd_deal_type,
           coop_deal_type = v_coop_deal_type,
           oi_deal_type = v_oi_deal_type
     WHERE promotion_id = member_id
       AND scenario_id <> 265;
     COMMIT;
     
     UPDATE promotion_data
       SET last_update_date = sysdate,
           activity_type_id = promotion_type_id
     WHERE promotion_id = member_id
       AND scenario_id <> 265;
     COMMIT;
     
     
   prc_set_pg_id(user_id, level_id, member_id);
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;


  PROCEDURE prc_make_promo_slob(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
   NAME:       PRC_MAKE_PROMO_SLOB
   PURPOSE:    Create Promotion Runs this Procedure to Populate Who created the
               Promotion and When

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Initial

******************************************************************************/
   
   v_prog_name      VARCHAR2 (100)  := 'PRC_MAKE_PROMO_SLOB';
   v_status         VARCHAR2 (100);
   v_proc_log_id    NUMBER;
   v_posted_accrual NUMBER;
   v_stat_id        NUMBER;
   
   v_cd_deal_type   NUMBER;
   v_coop_deal_type NUMBER;
   v_oi_deal_type   NUMBER;

BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_MAKE_PROMO_SLOB';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);    
    
        
    SELECT cd_deal_type_id, coop_deal_type_id, oi_deal_type_id
    INTO v_cd_deal_type, v_coop_deal_type, v_oi_deal_type
    FROM promotion_type 
    WHERE promotion_type_id = 13;
        
    
    BEGIN
        SELECT NVL(SUM(ABS(NVL(rr_accrual_cd_ad_gl, 0)) + ABS(NVL(rr_accrual_coop_ad_gl, 0)) 
               + ABS(NVL(rr_accrual_hf_cd_ad_gl, 0)) + ABS(NVL(rr_accrual_hf_coop_ad_gl, 0))), 0)
        INTO v_posted_accrual
        FROM accrual_data 
        WHERE accrual_id = member_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_posted_accrual := 0;
    WHEN OTHERS THEN      
      RAISE;
    END;
    
    SELECT promotion_stat_id 
    INTO v_stat_id
    FROM promotion 
    WHERE promotion_id = member_id
      AND scenario_id <> 265;
    
    IF v_posted_accrual <> 0 OR v_stat_id > 5 THEN
        RETURN;
    END IF;          
    
    UPDATE promotion_data
       SET promotion_type_id = 13,
           activity_type_id = 13,
           volume_base_ttl = 0,          
           last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;
    
    UPDATE promotion_matrix
       SET promotion_type_id = 13,
           promotion_data_lud = SYSDATE,
           promotion_lud = SYSDATE,
           last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;  
    
    UPDATE promotion
       SET promotion_type_id = 13,
           approval = 0,
           cd_deal_type = v_cd_deal_type,
           coop_deal_type = v_coop_deal_type,
           oi_deal_type = v_oi_deal_type,
           last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;      
    
    COMMIT;         

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_approve_slob_promo(user_id     NUMBER DEFAULT NULL,
                                   level_id    NUMBER DEFAULT NULL,
                                   member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
   NAME:       PRC_APPROVE_SLOB_PROMO
   PURPOSE:    Create Promotion Runs this Procedure to Populate Who created the
               Promotion and When

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Initial

******************************************************************************/
   
   v_prog_name      VARCHAR2 (100)  := 'PRC_APPROVE_SLOB_PROMO';
   v_status         VARCHAR2 (100);
   v_proc_log_id    NUMBER;
   v_approval       NUMBER;
   
   v_stat_id        NUMBER;
   v_type_id        NUMBER;

BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_APPROVE_PROMO_SLOB';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);    
    
    SELECT promotion_stat_id, promotion_type_id, approval
    INTO v_stat_id, v_type_id, v_approval
    FROM promotion
    WHERE promotion_id = member_id
      AND scenario_id <> 265;
    
    IF v_stat_id < 4
    THEN 
        RETURN; 
    ELSIF v_type_id <> 13
    THEN 
        RETURN;
    ELSIF v_approval = 1
    THEN
        RETURN;
    END IF;
    
    UPDATE promotion
       SET approval = 1,
           promo_app_user = user_id,
           promo_app_date = SYSDATE,
           last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;    
    
    
    COMMIT;         

    UPDATE promotion_matrix
      SET last_update_date = SYSDATE,
          promotion_lud = SYSDATE,
          promotion_data_lud = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;   
      
   COMMIT;      

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;


  PROCEDURE prc_load_shelf_price(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
   NAME:       PRC_LOAD_SHELF_PRICE
   PURPOSE:    Create Promotion Runs this Procedure to Populate Who created the
               Promotion and When

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Initial

******************************************************************************/
   
   v_prog_name      VARCHAR2 (100)  := 'PRC_LOAD_SHELF_PRICE';
   v_status         VARCHAR2 (100);
   v_proc_log_id    NUMBER;
   
   v_stat_id        NUMBER;
   v_type_id        NUMBER;

BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_LOAD_SHELF_PRICE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);    
    
   MERGE INTO promotion_data pd
   USING(SELECT pd.item_id, pd.location_id, pd.sales_date, pd.promotion_id , NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0))ed_price
         FROM promotion_data pd,
         sales_data sd
         WHERE sd.item_id = pd.item_id 
         AND sd.location_id = pd.location_id
         AND sd.sales_date = pd.sales_date
         --AND pd.scenario_id <> 265
         AND pd.promotion_id = member_id
         AND NVL(pd.ed_price, 0) <> NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0))) pd1
   ON(pd.item_id = pd1.item_id
   AND pd.location_id = pd1.location_id
   AND pd.sales_date = pd1.sales_date
   AND pd.promotion_id = pd1.promotion_id)
   WHEN MATCHED THEN 
   UPDATE SET pd.ed_price = pd1.ed_price,
              pd.last_update_date = SYSDATE;
   
   COMMIT;
   
   UPDATE promotion_matrix pm
   SET last_update_date = SYSDATE, 
   promotion_data_lud = SYSDATE
   WHERE promotion_id = member_id
     --AND scenario_id <> 265
     ;

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_create_claim(user_id     NUMBER DEFAULT NULL,
                              level_id    NUMBER DEFAULT NULL,
                              member_id   NUMBER DEFAULT NULL)
IS
/******************************************************************************
   NAME:       PRC_CREATE_CLAIM
   PURPOSE:    Create Promotion Runs this Procedure to Populate Who created the
               Promotion and When

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/12/2012  Luke Pocock/Red Rock Consulting - Initial

******************************************************************************/
   
   v_prog_name        VARCHAR2 (100)  := 'PRC_CREATE_CLAIM';
   v_status           VARCHAR2 (100);
   v_proc_log_id      NUMBER;
   
   new_claim_no       VARCHAR2(100);
   new_product_level  NUMBER; 
   new_customer_level NUMBER;
   new_claim_owner    NUMBER; 
   new_start_date     DATE; 
   new_end_date       DATE; 
   new_gl_code        NUMBER;
   new_type           NUMBER;
   new_bill_to        NUMBER;
   
   v_day              VARCHAR2(10);
   
   sql_str                 VARCHAR2 (20000);
   
   product_ep_field        VARCHAR2 (100);
   customer_ep_field       VARCHAR2 (100);
   prod_column             VARCHAR2 (100);
   cust_column             VARCHAR2 (100);
   
   v_count                 NUMBER;
  
BEGIN
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CREATE_CLAIM';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);    
      
   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK'; 
   
   SELECT COUNT(1) 
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);
   
   IF v_count <> 0 THEN
    
   SELECT claim_number, claim_product_level, claim_customer_level,
          claim_owner, claim_start_date, claim_end_date, claim_type, bill_to
     INTO new_claim_no, new_product_level, new_customer_level,
          new_claim_owner, new_start_date, new_end_date, new_type,  new_bill_to
     FROM promotion
    WHERE promotion_id = member_id
      AND scenario_id <> 265;
   
   new_gl_code := 0; --Account Type is always 'AR' for Schweppes --lannapra 8Oct2015
   
   SELECT id_field
     INTO product_ep_field
     FROM group_tables
    WHERE group_table_id = new_product_level;

   SELECT id_field
     INTO customer_ep_field
     FROM group_tables
    WHERE group_table_id = new_customer_level;
   
   IF new_product_level = 362 THEN 
      prod_column := 'promoted_product';
   ELSIF new_product_level = 12 THEN    
      prod_column := 'promotion_group';
   ELSIF new_product_level = 430 THEN    
      prod_column := 'brand';
   END IF;
   
   IF new_customer_level = 478 THEN 
      cust_column := 'customer_name';        ----       t_ep_e1_cust_cat_4_EP_ID 
   ELSIF new_customer_level = 492 THEN    
      cust_column := 'corporate_customer';   ---- Metcash level   t_ep_ebs_zone_EP_ID
   ELSIF new_customer_level = 490 THEN    
      cust_column := 'key_account';          ---- CL3   t_ep_ebs_customer_EP_ID
   ELSIF new_customer_level = 491 THEN    
      cust_column := 'key_account_state';     ---- CL4  t_ep_ebs_account_EP_ID
   END IF;
         
   sql_str :=
         (   ' 
BEGIN
   FOR rec IN
      (SELECT          m.' || product_ep_field || ' AS prod_id,
                       m.' || customer_ep_field || ' AS cust_id,
                       m.t_ep_l_att_10_ep_id,
                       m.t_ep_e1_cust_cat_2_ep_id,
                       m.t_ep_e1_cust_cat_4_EP_ID, 
                       m.t_ep_ebs_zone_EP_ID,
                       m.t_ep_ebs_customer_EP_ID,
                       m.t_ep_ebs_account_EP_ID,
                       pm.from_date,
                       pm.until_date, 
                       p.cd_deal_type,
                       p.coop_deal_type,
                       MAX (pd.case_buydown) case_deal,
                       MAX (pd.rr_handling_d) case_hf_deal,
                       SUM (NVL(pd.rr_accrual_cd_pd, 0)) case_amount,
                       SUM (NVL(pd.rr_accrual_coop_pd, 0)) coop_amount,
                       SUM (NVL(pd.rr_accrual_hf_cd_pd, 0)) case_hf_amount,
                       SUM (NVL(pd.rr_accrual_hf_coop_pd, 0)) coop_hf_amount
                  FROM promotion_dates pm, mdp_matrix m, promotion_data pd, promotion p
                 WHERE pd.item_id = m.item_id
                   AND pd.location_id = m.location_id
                   AND pd.promotion_id = pm.promotion_id
                   AND p.promotion_id = ' || member_id || '
                   AND pd.promotion_id = p.promotion_id
                   AND p.scenario_id <> 265
                   AND pd.sales_date BETWEEN NVL(NEXT_DAY(TO_DATE(''' || new_start_date || ''', ''MM/DD/YYYY HH24:MI:SS''), ''' || v_day || ''') - 7, pd.sales_date) 
                                         AND NVL(NEXT_DAY(TO_DATE( ''' || new_end_date || ''', ''MM/DD/YYYY HH24:MI:SS''), ''' || v_day || ''') - 1, pd.sales_date)
                   AND pd.is_self = 1
                   AND ABS(NVL(pd.rr_accrual_cd_pd, 0)) +
                       ABS(NVL(pd.rr_accrual_coop_pd, 0)) +
                       ABS(NVL(pd.rr_accrual_hf_cd_pd, 0)) + 
                       ABS(NVL(pd.rr_accrual_hf_coop_pd, 0)) > 0
              GROUP BY m.'|| product_ep_field || ', 
                       m.'|| customer_ep_field || ',
                       m.t_ep_l_att_10_ep_id,
                       m.t_ep_e1_cust_cat_2_ep_id,
                       m.t_ep_e1_cust_cat_4_EP_ID, 
                       m.t_ep_ebs_zone_EP_ID,
                       m.t_ep_ebs_customer_EP_ID,
                       m.t_ep_ebs_account_EP_ID,
                       pm.from_date,
                       pm.until_date,
                       p.cd_deal_type,
                       p.coop_deal_type)
   LOOP
      
      INSERT INTO settlement
                  (settlement_id, settlement_code, settlement_desc,
                  date_posted, start_date, end_date,
                  settlement_type_id, settlement_status_id, ' || prod_column || ', ' || cust_column || ',
                  t_ep_e1_cust_cat_2_ep_id, t_ep_l_att_10_ep_id, t_ep_e1_cust_cat_4_EP_ID, t_ep_ebs_zone_EP_ID, t_ep_ebs_customer_EP_ID, t_ep_ebs_account_EP_ID,
                  settlement_owner_id, 
                  case_deal, case_hf_deal, claim_case_deal_d, co_op_$, claim_case_deal_hf_d, claim_coop_hf,
                  last_update_date, promotion_id, gl_code_id,bill_to)
           VALUES (settlement_seq.NEXTVAL, settlement_seq.NEXTVAL, ''' || new_claim_no || ''', 
                   SYSDATE, rec.from_date, rec.until_date,
                   ' || new_type || ',                        -- 1=Case Deal, 2=OI, 3=Deduction
                   8,                     --8=LINKED
                   rec.prod_id, rec.cust_id, -- 0 Account, sub_banner, promo group
                   rec.t_ep_e1_cust_cat_2_ep_id, rec.t_ep_l_att_10_ep_id, rec.t_ep_e1_cust_cat_4_EP_ID, rec.t_ep_ebs_zone_EP_ID, rec.t_ep_ebs_customer_EP_ID, rec.t_ep_ebs_account_EP_ID, 
                   ' || new_claim_owner || ',
                   0, 0, 0, 0, 0, 0,
                   --rec.case_deal, rec.case_hf_deal, rec.case_amount, rec.coop_amount, rec.case_hf_amount, rec.coop_hf_amount, 
                   SYSDATE, ' || member_id || ', ' || new_gl_code || ', ' || new_bill_to || '
                  );
   END LOOP;
   
   COMMIT;
END;'
         );
      dynamic_ddl (sql_str);
      COMMIT;
   
   UPDATE promotion
      SET claim_number_bk = claim_number,
          claim_product_level_bk = claim_product_level,
          claim_customer_level_bk = claim_customer_level,
          claim_owner_bk = claim_owner,
          claim_start_date_bk = claim_start_date,
          claim_end_date_bk = claim_end_date,
          claim_type_bk = claim_type,
          claim_gl_code_bk = claim_gl_code
    WHERE promotion_id = member_id
      AND scenario_id <> 265;

   UPDATE promotion
      SET claim_number = NULL,
          claim_product_level = NULL,
          claim_customer_level = NULL,
          claim_owner = NULL,
          claim_start_date = NULL,
          claim_end_date = NULL,
          claim_type = NULL,
          claim_gl_code = NULL,
          last_update_date = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;    
   
   UPDATE promotion_matrix
      SET promotion_lud = SYSDATE
    WHERE promotion_id = member_id
      AND scenario_id <> 265;

   COMMIT;
   
   END IF;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_sync_deals(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_SYNC_DEALS
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);

   v_day         VARCHAR2(10);
   v_eng_profile NUMBER := 1;

   v_prog_name    VARCHAR2 (100) := 'PRC_SYNC_DEALS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   
   v_ms           NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_SYNC_DEALS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   pkg_accrual_lm.prc_fix_promo_type(user_id, member_id);
   
   SELECT pt.rr_margin_support
   INTO v_ms
   FROM promotion_type pt,
   promotion p
   WHERE p.promotion_type_id = pt.promotion_type_id
   AND p.promotion_id = member_id
   AND scenario_id <> 265;
   
   v_ms := 0;
      
   pkg_accrual_lm.prc_set_mdp_allocation(user_id, member_id);   
   
   IF v_ms = 0 THEN 
   
      pkg_accrual_lm.prc_calc_accrual(user_id, member_id);
      pkg_accrual_lm.prc_promo_accrual_sync(user_id, member_id, 3, 0);
      pkg_accrual_lm.prc_margin_support(user_id, member_id);
      pkg_accrual_lm.prc_promo_accrual_sync(user_id, member_id, 3, 1);
      
   END IF;
   
   IF v_ms = 1 THEN 
      
      pkg_accrual_lm.prc_calc_accrual(user_id, member_id);
      pkg_accrual_lm.prc_promo_accrual_sync(user_id, member_id, 3, 0);
      pkg_accrual_lm.prc_margin_support(user_id, member_id);
      pkg_accrual_lm.prc_promo_accrual_sync(user_id, member_id, 3, 1);
   
   END IF;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_reload_base(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  AS
/******************************************************************************
   NAME:       PRC_CORRECT_PD
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);
   
   
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_RELOAD_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_RELOAD_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

      
   sql_str := ('MERGE INTO promotion_data pd2
      USING (SELECT
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
             CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') + 7
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END 
                  ELSE pd.volume_base_ttl
             END volume_base_ttl,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END evt_vol_act,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END incr_evt_vol_act,
             sd.ebspricelist128 list_price_p, 
             sd.ebspricelist123 cust_list_price_p, 
             sd.ebspricelist100 tot_efficiencies_p, 
             sd.ebspricelist124 market_price_p, 
             sd.ebspricelist101 frieght_deduction_p, 
             sd.ebspricelist125 bnps_p, 
             sd.bdf_base_rate_corp bbds_p, 
             sd.bdf_fixed_funds_corp rebate_p, 
             sd.ebspricelist126 cogs_p, 
             sd.ebspricelist102 distribution_p, 
             sd.ebspricelist127 spoils_p, 
             sd.ebspricelist129 def_efficiencies_rate_p,
             sd.ebs_sh_ship_qty_rd list_sales_a, 
             sd.ebs_sh_req_qty_rd tot_efficiencies_a, 
             sd.sdata12 price_adjust_a, 
             sd.sdata10 gross_sales_a, 
             sd.sdata11 base_non_per_spnd_a, 
             sd.ebs_bh_req_qty_rd ttl_base_bus_dev_spnd_a, 
             sd.ebs_sh_ship_qty_sd ttl_promo_trde_spnd_a, 
             sd.sdata13 total_trad_a, 
             sd.ebs_bh_book_qty_rd net_sales_a, 
             sd.ebs_bh_req_qty_bd cogs_a, 
             sd.ebs_bh_book_qty_bd tot_distribution_a, 
             sd.sdata14 spoils_a, 
             sd.sdata4 non_work_co_mark_a, 
             sd.sdata9 cust_contribution_a,
             i.e1_ship_uom_mult units,
             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price
             FROM sales_data sd, 
             promotion_data pd, 
             promotion_type pt, 
             promotion_stat ps, 
             promotion_matrix pm,
             t_ep_l_att_6 latt6,
             mdp_matrix mdp,
             t_ep_item i
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.item_id =  pd.item_id
             AND mdp.location_id = pd.location_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id
             AND pm.scenario_id IN (22, 262, 162)
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             AND pd.is_self = 1
             AND pd.promotion_id = ' || member_id || '
             AND (NVL(CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') 
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0 
                        END 
                  ELSE pd.volume_base_ttl
             END, 0) <> NVL(pd.volume_base_ttl, 0)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END, -99899899899) <> NVL(pd.evt_vol_act, -99899899899)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END, -99899899899) <> NVL(pd.incr_evt_vol_act, -99899899899)
             /*OR NVL(sd.ebspricelist128, 0) <> NVL(pd.list_price_p, 0) 
             OR NVL(sd.ebspricelist123, 0) <> NVL(pd.cust_list_price_p, 0) 
             OR NVL(sd.ebspricelist100, 0) <> NVL(pd.tot_efficiencies_p, 0) 
             OR NVL(sd.ebspricelist124, 0) <> NVL(pd.market_price_p, 0) 
             OR NVL(sd.ebspricelist101, 0) <> NVL(pd.frieght_deduction_p, 0) 
             OR NVL(sd.ebspricelist125, 0) <> NVL(pd.bnps_p, 0) 
             OR NVL(sd.bdf_base_rate_corp, 0) <> NVL(pd.bbds_p, 0) 
             OR NVL(sd.bdf_fixed_funds_corp, 0) <> NVL(pd.rebate_p, 0) 
             OR NVL(sd.ebspricelist126, 0) <> NVL(pd.cogs_p, 0) 
             OR NVL(sd.ebspricelist102, 0) <> NVL(pd.distribution_p, 0) 
             OR NVL(sd.ebspricelist127, 0) <> NVL(pd.spoils_p, 0) 
             OR NVL(sd.ebspricelist129, 0) <> NVL(pd.def_efficiencies_rate_p, 0)
             OR NVL(sd.ebs_sh_ship_qty_rd, 0) <> NVL(pd.list_sales_a, 0) 
             OR NVL(sd.ebs_sh_req_qty_rd, 0) <> NVL(pd.tot_efficiencies_a, 0) 
             OR NVL(sd.sdata12, 0) <> NVL(pd.price_adjust_a, 0) 
             OR NVL(sd.sdata10, 0) <> NVL(pd.gross_sales_a, 0) 
             OR NVL(sd.sdata11, 0) <> NVL(pd.base_non_per_spnd_a, 0) 
             OR NVL(sd.ebs_bh_req_qty_rd, 0) <> NVL(pd.ttl_base_bus_dev_spnd_a, 0) 
             OR NVL(sd.ebs_sh_ship_qty_sd, 0) <> NVL(pd.ttl_promo_trde_spnd_a, 0) 
             OR NVL(sd.sdata13, 0) <> NVL(pd.total_trad_a, 0) 
             OR NVL(sd.ebs_bh_book_qty_rd, 0) <> NVL(pd.net_sales_a, 0) 
             OR NVL(sd.ebs_bh_req_qty_bd, 0) <> NVL(pd.cogs_a, 0) 
             OR NVL(sd.ebs_bh_book_qty_bd, 0) <> NVL(pd.tot_distribution_a, 0) 
             OR NVL(sd.sdata14, 0) <> NVL(pd.spoils_a, 0) 
             OR NVL(sd.sdata4, 0) <> NVL(pd.non_work_co_mark_a, 0) 
             OR NVL(sd.sdata9, 0) <> NVL(pd.cust_contribution_a, 0) */
             OR NVL(i.e1_ship_uom_mult, 0) <> NVL(pd.units, 0)   
             /*OR NVL(NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)), 0) <> NVL(pd.ed_price, 0) */
             )
             ) pd1
      ON (pd1.promotion_id = pd2.promotion_id
          AND pd1.item_id = pd2.item_id
          AND pd1.location_id = pd2.location_id
          AND pd1.sales_date = pd2.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd2.volume_base_ttl = pd1.volume_base_ttl,
                pd2.evt_vol_act = pd1.evt_vol_act,
                pd2.incr_evt_vol_act = pd1.incr_evt_vol_act,
                /*pd2.list_price_p = pd1.list_price_p,
                pd2.cust_list_price_p = pd1.cust_list_price_p,
                pd2.tot_efficiencies_p = pd1.tot_efficiencies_p,
                pd2.market_price_p = pd1.market_price_p,
                pd2.frieght_deduction_p = pd1.frieght_deduction_p,
                pd2.bnps_p = pd1.bnps_p,
                pd2.bbds_p = pd1.bbds_p,
                pd2.rebate_p = pd1.rebate_p,
                pd2.cogs_p = pd1.cogs_p,
                pd2.distribution_p = pd1.distribution_p,
                pd2.spoils_p = pd1.spoils_p,
                pd2.def_efficiencies_rate_p = pd1.def_efficiencies_rate_p,
                pd2.list_sales_a = pd1.list_sales_a,
                pd2.tot_efficiencies_a = pd1.tot_efficiencies_a,
                pd2.price_adjust_a = pd1.price_adjust_a,
                pd2.gross_sales_a = pd1.gross_sales_a,
                pd2.base_non_per_spnd_a = pd1.base_non_per_spnd_a,
                pd2.ttl_base_bus_dev_spnd_a = pd1.ttl_base_bus_dev_spnd_a,
                pd2.ttl_promo_trde_spnd_a = pd1.ttl_promo_trde_spnd_a,
                pd2.total_trad_a = pd1.total_trad_a,
                pd2.net_sales_a = pd1.net_sales_a,
                pd2.cogs_a = pd1.cogs_a,
                pd2.tot_distribution_a = pd1.tot_distribution_a,
                pd2.spoils_a = pd1.spoils_a,
                pd2.non_work_co_mark_a = pd1.non_work_co_mark_a,
                pd2.cust_contribution_a = pd1.cust_contribution_a, */
                pd2.units = pd1.units,
                --pd2.ed_price = pd1.ed_price,
                pd2.last_update_date = SYSDATE
                ');
         
   dynamic_ddl(sql_str);
         
   COMMIT;
   
   UPDATE promotion_matrix
   SET last_update_date = SYSDATE,
   promotion_data_lud = SYSDATE
   WHERE promotion_id = member_id
     AND scenario_id IN (22, 262, 162);
   
   COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_reload_all_base(user_id   NUMBER DEFAULT NULL,
                                level_id  NUMBER DEFAULT NULL,
                                member_id NUMBER DEFAULT NULL)
  AS
/******************************************************************************
   NAME:       PRC_CORRECT_PD
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);
   
   
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_RELOAD_ALL_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_RELOAD_ALL_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;
   
   check_and_drop('tbl_promo_group_' || user_id || '_t');

   sql_str := 'CREATE TABLE tbl_promo_group_' || user_id || '_t
               AS SELECT DISTINCT mdp.t_ep_p2b_ep_id FROM promotion_matrix pm, mdp_matrix mdp
                  WHERE pm.item_id = mdp.item_id
                  AND pm.location_id = mdp.location_id
                  AND pm.scenario_id IN (22, 262, 162)
                  AND pm.promotion_id = ' || member_id ;
                  
   dynamic_ddl(sql_str);
   
   sql_str := ('MERGE INTO promotion_data pd
      USING (SELECT 
             pd.promotion_id, sd.item_id, sd.location_id, sd.sales_date,
             CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') 
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0 
                        END 
                  ELSE pd.volume_base_ttl
             END volume_base_ttl,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END evt_vol_act,
             CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END incr_evt_vol_act,
             sd.ebspricelist128 list_price_p, 
             sd.ebspricelist123 cust_list_price_p, 
             sd.ebspricelist100 tot_efficiencies_p, 
             sd.ebspricelist124 market_price_p, 
             sd.ebspricelist101 frieght_deduction_p, 
             sd.ebspricelist125 bnps_p, 
             sd.bdf_base_rate_corp bbds_p, 
             sd.bdf_fixed_funds_corp rebate_p, 
             sd.ebspricelist126 cogs_p, 
             sd.ebspricelist102 distribution_p, 
             sd.ebspricelist127 spoils_p, 
             sd.ebspricelist129 def_efficiencies_rate_p,
             sd.ebs_sh_ship_qty_rd list_sales_a, 
             sd.ebs_sh_req_qty_rd tot_efficiencies_a, 
             sd.sdata12 price_adjust_a, 
             sd.sdata10 gross_sales_a, 
             sd.sdata11 base_non_per_spnd_a, 
             sd.ebs_bh_req_qty_rd ttl_base_bus_dev_spnd_a, 
             sd.ebs_sh_ship_qty_sd ttl_promo_trde_spnd_a, 
             sd.sdata13 total_trad_a, 
             sd.ebs_bh_book_qty_rd net_sales_a, 
             sd.ebs_bh_req_qty_bd cogs_a, 
             sd.ebs_bh_book_qty_bd tot_distribution_a, 
             sd.sdata14 spoils_a, 
             sd.sdata4 non_work_co_mark_a, 
             sd.sdata9 cust_contribution_a,
             i.e1_ship_uom_mult units,
             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price
             FROM sales_data sd, 
             promotion_data pd, 
             promotion_type pt, 
             promotion_stat ps, 
             promotion_matrix pm,
             t_ep_l_att_6 latt6,
             mdp_matrix mdp,
             t_ep_item i,
             tbl_promo_group_' || user_id || '_t pg
             WHERE sd.item_id = pd.item_id
             AND sd.location_id = pd.location_id
             AND sd.sales_date = pd.sales_date
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.item_id = sd.item_id
             AND mdp.location_id = sd.location_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pm.item_id = pd.item_id
             AND pm.location_id = pd.location_id
             AND pm.promotion_id = pd.promotion_id
             AND pm.scenario_id IN (22, 262, 162)
             AND pd.promotion_type_id = pt.promotion_type_id
             AND pd.promotion_stat_id = ps.promotion_stat_id
             AND pd.is_self = 1
             AND pg.t_ep_p2b_ep_id = mdp.t_ep_p2b_ep_id
             AND (NVL(CASE WHEN pd.sales_date >= TO_DATE('''|| get_max_date ||''', ''MM-DD-YYYY HH24:MI:SS'') 
                                                                + CASE
                                                                      WHEN pm.rr_alloc_wk6 > 0 THEN 42
                                                                      WHEN pm.rr_alloc_wk5 > 0 THEN 35
                                                                      WHEN pm.rr_alloc_wk4 > 0 THEN 28
                                                                      WHEN pm.rr_alloc_wk3 > 0 THEN 21
                                                                      WHEN pm.rr_alloc_wk2 > 0 THEN 14
                                                                      WHEN pm.rr_alloc_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0
                        END 
                  ELSE pd.volume_base_ttl
             END, 0) <> NVL(pd.volume_base_ttl, 0)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END, -99899899899) <> NVL(pd.evt_vol_act, -99899899899)
             OR NVL(CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END, -99899899899) <> NVL(pd.incr_evt_vol_act, -99899899899)
             /* OR NVL(sd.ebspricelist128, 0) <> NVL(pd.list_price_p, 0) 
             OR NVL(sd.ebspricelist123, 0) <> NVL(pd.cust_list_price_p, 0) 
             OR NVL(sd.ebspricelist100, 0) <> NVL(pd.tot_efficiencies_p, 0) 
             OR NVL(sd.ebspricelist124, 0) <> NVL(pd.market_price_p, 0) 
             OR NVL(sd.ebspricelist101, 0) <> NVL(pd.frieght_deduction_p, 0) 
             OR NVL(sd.ebspricelist125, 0) <> NVL(pd.bnps_p, 0) 
             OR NVL(sd.bdf_base_rate_corp, 0) <> NVL(pd.bbds_p, 0) 
             OR NVL(sd.bdf_fixed_funds_corp, 0) <> NVL(pd.rebate_p, 0) 
             OR NVL(sd.ebspricelist126, 0) <> NVL(pd.cogs_p, 0) 
             OR NVL(sd.ebspricelist102, 0) <> NVL(pd.distribution_p, 0) 
             OR NVL(sd.ebspricelist127, 0) <> NVL(pd.spoils_p, 0) 
             OR NVL(sd.ebspricelist129, 0) <> NVL(pd.def_efficiencies_rate_p, 0)
             OR NVL(sd.ebs_sh_ship_qty_rd, 0) <> NVL(pd.list_sales_a, 0) 
             OR NVL(sd.ebs_sh_req_qty_rd, 0) <> NVL(pd.tot_efficiencies_a, 0) 
             OR NVL(sd.sdata12, 0) <> NVL(pd.price_adjust_a, 0) 
             OR NVL(sd.sdata10, 0) <> NVL(pd.gross_sales_a, 0) 
             OR NVL(sd.sdata11, 0) <> NVL(pd.base_non_per_spnd_a, 0) 
             OR NVL(sd.ebs_bh_req_qty_rd, 0) <> NVL(pd.ttl_base_bus_dev_spnd_a, 0) 
             OR NVL(sd.ebs_sh_ship_qty_sd, 0) <> NVL(pd.ttl_promo_trde_spnd_a, 0) 
             OR NVL(sd.sdata13, 0) <> NVL(pd.total_trad_a, 0) 
             OR NVL(sd.ebs_bh_book_qty_rd, 0) <> NVL(pd.net_sales_a, 0) 
             OR NVL(sd.ebs_bh_req_qty_bd, 0) <> NVL(pd.cogs_a, 0) 
             OR NVL(sd.ebs_bh_book_qty_bd, 0) <> NVL(pd.tot_distribution_a, 0) 
             OR NVL(sd.sdata14, 0) <> NVL(pd.spoils_a, 0) 
             OR NVL(sd.sdata4, 0) <> NVL(pd.non_work_co_mark_a, 0) 
             OR NVL(sd.sdata9, 0) <> NVL(pd.cust_contribution_a, 0) */
             OR NVL(i.e1_ship_uom_mult, 0) <> NVL(pd.units, 0)   
             /*OR NVL(NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)), 0) <> NVL(pd.ed_price, 0)*/
             )
             ) pd1
      ON (pd1.promotion_id = pd.promotion_id
          AND pd1.item_id = pd.item_id
          AND pd1.location_id = pd.location_id
          AND pd1.sales_date = pd.sales_date)
      WHEN MATCHED THEN
         UPDATE
            SET pd.volume_base_ttl = pd1.volume_base_ttl,
                pd.evt_vol_act = pd1.evt_vol_act,
                pd.incr_evt_vol_act = pd1.incr_evt_vol_act,
                /*pd.list_price_p = pd1.list_price_p,
                pd.cust_list_price_p = pd1.cust_list_price_p,
                pd.tot_efficiencies_p = pd1.tot_efficiencies_p,
                pd.market_price_p = pd1.market_price_p,
                pd.frieght_deduction_p = pd1.frieght_deduction_p,
                pd.bnps_p = pd1.bnps_p,
                pd.bbds_p = pd1.bbds_p,
                pd.rebate_p = pd1.rebate_p,
                pd.cogs_p = pd1.cogs_p,
                pd.distribution_p = pd1.distribution_p,
                pd.spoils_p = pd1.spoils_p,
                pd.def_efficiencies_rate_p = pd1.def_efficiencies_rate_p,
                pd.list_sales_a = pd1.list_sales_a,
                pd.tot_efficiencies_a = pd1.tot_efficiencies_a,
                pd.price_adjust_a = pd1.price_adjust_a,
                pd.gross_sales_a = pd1.gross_sales_a,
                pd.base_non_per_spnd_a = pd1.base_non_per_spnd_a,
                pd.ttl_base_bus_dev_spnd_a = pd1.ttl_base_bus_dev_spnd_a,
                pd.ttl_promo_trde_spnd_a = pd1.ttl_promo_trde_spnd_a,
                pd.total_trad_a = pd1.total_trad_a,
                pd.net_sales_a = pd1.net_sales_a,
                pd.cogs_a = pd1.cogs_a,
                pd.tot_distribution_a = pd1.tot_distribution_a,
                pd.spoils_a = pd1.spoils_a,
                pd.non_work_co_mark_a = pd1.non_work_co_mark_a,
                pd.cust_contribution_a = pd1.cust_contribution_a, */
                pd.units = pd1.units,
                --pd.ed_price = pd1.ed_price,
                pd.last_update_date = SYSDATE
                ');
         
   dynamic_ddl(sql_str);
         
   COMMIT;
   
   UPDATE promotion_matrix
   SET last_update_date = SYSDATE,
   promotion_data_lud = SYSDATE
   WHERE (item_id, location_id) IN (select item_id, promotion_id FROM promotion_matrix where promotion_id = member_id)
   AND promotion_stat_id <> 8
   AND scenario_id IN (22, 262, 162);
   
   COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_check_promo(user_id     NUMBER DEFAULT NULL,
                            level_id    NUMBER DEFAULT NULL,
                            member_id   NUMBER DEFAULT NULL)
  IS
/******************************************************************************
   NAME:       PRC_CHECK_PROMO
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   max_sales_date   VARCHAR2 (100);
   min_date         VARCHAR2 (100);
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (5000);
   
   v_no_weeks       NUMBER := 52;
   v_promo_week_range NUMBER := 2;

   v_lvl_id         NUMBER := 366;   
   v_id_field       VARCHAR2(100);
   v_gtable_name     VARCHAR2(100);
   
   v_gt_id          VARCHAR2(100);
   v_unique_count   VARCHAR2(1000);
   v_unique_sel     VARCHAR2(1000);   
   v_where_pct      VARCHAR2(2000);
   v_where_lvl      VARCHAR2(2000);
   v_unique_count_pct  VARCHAR2(1000);
   v_from_clause    VARCHAR2(2000);
   v_gtable         VARCHAR2(100);

   v_day         VARCHAR2(10);
   v_eng_profile NUMBER := 1;
   
   v_group_id1   VARCHAR2(2000);
   v_group_id2   VARCHAR2(2000);

   v_prog_name    VARCHAR2 (100) := 'PRC_CHECK_PROMO';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;   
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CHECK_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   dynamic_ddl ('TRUNCATE TABLE t_ep_promotion_conflicts');
   
   SELECT gtable, id_field 
   INTO v_gtable_name, v_id_field
   FROM group_tables
   WHERE group_table_id = v_lvl_id;

   UPDATE /*+ parallel(p,24) */ promotion p
   SET t_ep_promo_conflict_id = 0
   WHERE t_ep_promo_conflict_id = 1
     AND scenario_id <> 265;

   COMMIT;
   
   UPDATE t_ep_ebs_customer 
   SET t_ep_ebs_customer_ep_id_l = t_ep_ebs_customer_ep_id
   WHERE t_ep_ebs_customer_ep_id_l IS NULL;
   
   COMMIT;

   UPDATE /*+ parallel(pd,24) */ promotion_data pd
   SET pd.promo_conflict = 0,
       pd.t_ep_promo_conflict_id = 0
   WHERE NVL(pd.promo_conflict, 0) + NVL(pd.t_ep_promo_conflict_id, 0) <> 0
     AND scenario_id <> 265;

   COMMIT;

   UPDATE /*+ parallel(pm,24) */ promotion_matrix pm
   SET pm.t_ep_promo_conflict_id = 0
   WHERE pm.t_ep_promo_conflict_id = 1
     AND scenario_id <> 265;

   COMMIT;
   
   UPDATE /*+ parallel(p,24) nologging */promotion p
   SET conflict_01 = NULL,
       conflict_02 = NULL,
       conflict_03 = NULL,
       conflict_04 = NULL,
       conflict_05 = NULL,
       conflict_06 = NULL,
       conflict_07 = NULL,
       conflict_08 = NULL,
       conflict_09 = NULL,
       conflict_10 = NULL
  WHERE scenario_id <> 265;
       
   COMMIT;
       
   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS'), 'MM/DD/YYYY')
   INTO max_sales_date
   FROM dual;
   
   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS') + (1 * 7), 'MM/DD/YYYY')
   INTO min_date
   FROM dual; 
   
-- Set max check parameter for 8 weeks into future
   SELECT TO_CHAR (TO_DATE (get_max_date, 'MM-DD-YYYY HH24:MI:SS') + (v_no_weeks * 7), 'MM/DD/YYYY')
   INTO max_date
   FROM dual;   
   
   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';
   
   dynamic_ddl('TRUNCATE TABLE t_ep_promotion_conflicts');   
   
   FOR rec IN (SELECT DISTINCT l10.group_id1, l2.group_id2 
               FROM t_ep_l_att_10 l10, t_ep_l_att_2 l2, location l, conflict_groups_t cgt
               WHERE l10.group_id1 <> 0
               AND l2.group_id2 <> 0
               AND l10.group_id1 = cgt.group_id1
               AND l2.group_id2 = cgt.group_id2
               AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
               AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
   LOOP
   
      v_group_id1 := '(-1';
      FOR temp_1 IN (SELECT DISTINCT l10.t_ep_l_att_10_ep_id
                     FROM location l, t_ep_l_att_10 l10, t_ep_l_att_2 l2 
                     WHERE l10.group_id1 = rec.group_id1
                     AND l2.group_id2 = rec.group_id2
                     AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
                     AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
      LOOP
      
          v_group_id1 := v_group_id1 || ', ' || temp_1.t_ep_l_att_10_ep_id;
      
      END LOOP;
      v_group_id1 := v_group_id1 || ')';
      
      v_group_id2 := '(-1';
      FOR temp_2 IN (SELECT DISTINCT l2.t_ep_l_att_2_ep_id
                     FROM location l, t_ep_l_att_10 l10, t_ep_l_att_2 l2 
                     WHERE l10.group_id1 = rec.group_id1
                     AND l2.group_id2 = rec.group_id2
                     AND l.t_ep_l_att_10_ep_id = l10.t_ep_l_att_10_ep_id
                     AND l.t_ep_l_att_2_ep_id = l2.t_ep_l_att_2_ep_id)
      LOOP
      
          v_group_id2 := v_group_id2 || ', ' || temp_2.t_ep_l_att_2_ep_id;
      
      END LOOP;
      v_group_id2 := v_group_id2 || ')';
   
      v_unique_count := '';
      v_unique_sel := '';
      v_from_clause := '';
      v_where_pct := '';
      v_where_lvl := '';
      v_unique_count_pct := '';
            
      
      FOR rec1 IN (SELECT group_table_id FROM conflict_groups_t WHERE group_id1 = rec.group_id1 AND group_id2 = rec.group_id2)
      LOOP
      
          SELECT gtable, id_field INTO v_gtable, v_gt_id FROM group_tables WHERE group_table_id = rec1.group_table_id;
          
          v_unique_count := v_unique_count || ', ' || v_gtable || '.' || v_gt_id || '_l ' || v_gt_id || '_l';    
          v_unique_sel := v_unique_sel || ' || ''-'' || ' || v_gtable || '.' || v_gt_id || '_l';
          v_from_clause := v_from_clause || ',' || v_gtable || ' ' || v_gtable;
          v_where_pct := v_where_pct || ' AND pct.' || v_gt_id || '_l = ' || v_gtable || '.' || v_gt_id ;
          v_where_lvl := v_where_lvl || ' AND ' || v_gtable || '.' || v_gt_id || ' = m.' || v_gt_id ;
          v_unique_count_pct := v_unique_count_pct || ', pct.' || v_gt_id || '_l ' || v_gt_id || '_l';    
      
      END LOOP;
      
      v_unique_count := LTRIM(v_unique_count, ',');
      
      check_and_drop('promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2); 
   
-- Only checking for Promotions of Type Catalogue that are conflicting--
      sql_str :=
         'CREATE TABLE promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2 || ' AS          
          (SELECT /*+ parallel(pm, 12) */ DISTINCT pm.promotion_id, pm.item_id, m.t_ep_l_att_10_ep_id, m.t_ep_l_att_2_ep_id, i.datet sales_date, ' || v_unique_count || ', 
           CASE WHEN COUNT(DISTINCT 1 ' || v_unique_sel || ') OVER (PARTITION BY pm.item_id, m.t_ep_l_att_10_ep_id, m.t_ep_l_att_2_ep_id, m.t_ep_e1_cust_state_ep_id, id.datet) > 1 
           THEN 2 ELSE 1 END count_1, m.t_ep_e1_cust_state_ep_id
           FROM promotion p,
           promotion_matrix pm, 
           mdp_matrix m, 
           promotion_dates pdt, 
           promotion_type pt, 
           promotion_stat ps,  
           scenario s,
           inputs i,
           inputs_daily id,
           ' || v_gtable_name || ' lvl
           ' || v_from_clause || '
           WHERE pm.item_id = m.item_id
           AND pm.location_id = m.location_id
           AND pm.promotion_id = p.promotion_id
           AND p.promotion_type_id = pt.promotion_type_id
           AND p.promotion_id = pm.promotion_id
           AND p.scenario_id <> 265
           AND p.scenario_id = s.scenario_id           
           AND pt.rr_check_promo = 1
           AND ps.rr_check_promo = 1
           AND s.rr_check_promo = 1
           AND p.ignore_promo_conflict = 0
           AND pdt.promotion_id = p.promotion_id
           AND m.t_ep_l_att_10_ep_id IN  ' || v_group_id1 || ' 
           AND m.t_ep_l_att_2_ep_id IN ' || v_group_id2 || ' 
           AND  m.' || v_id_field || ' =  lvl.' || v_id_field || '
           AND pdt.until_date - pdt.from_date < ( ' || v_promo_week_range || ' * 7) + 1
           ' || v_where_lvl || '
           AND lvl.rr_promo_overlap <> 0
           AND p.promotion_stat_id = ps.promotion_stat_id
           AND pm.is_self = 1
           AND i.datet BETWEEN pdt.from_date AND pdt.until_date 
           AND id.datet BETWEEN pdt.from_date + DECODE(MOD((pdt.until_date - pdt.from_date) + 1, 7), 0, lvl.rr_promo_offset_start, 0) AND pdt.until_date + DECODE(MOD((pdt.until_date - pdt.from_date) + 1, 7), 0, lvl.rr_promo_offset_start, 0)
           AND i.datet BETWEEN TO_DATE(''' || min_date || ''', ''MM/DD/YYYY'') AND TO_DATE(''' || max_date || ''', ''MM/DD/YYYY'')           
           )';
    
      
      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);
      COMMIT;
      
      dynamic_ddl(' TRUNCATE TABLE cg_members_t'); 
      
      check_and_drop('cg_members_t_' || rec.group_id1 || '_' || rec.group_id2);
      dynamic_ddl('CREATE TABLE cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || ' AS SELECT * FROM cg_members_t WHERE 1 = 2');
      
      sql_str :=
         'INSERT /*+ APPEND NOLOGGING */ INTO cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || '           
          (SELECT /*+ parallel(pm, 12) */ DISTINCT pm.promotion_id, pm.item_id, pm.location_id, pct.sales_date, pdt.from_date, pdt.until_date, lvl.rr_promo_overlap, lvl.rr_promo_offset_start
           ' || v_unique_count_pct || ', pct.t_ep_e1_cust_state_ep_id
           FROM promotion p,
           promotion_matrix pm, 
           promotion_dates pdt,       
           inputs i,
           mdp_matrix m, 
           promotion_type pt, 
           promotion_stat ps,
           scenario s,        
           ' || v_gtable_name || ' lvl,
           promo_conflict_t_' || rec.group_id1 || '_' || rec.group_id2 || ' pct
           ' || v_from_clause || '
           WHERE pm.promotion_id = p.promotion_id
           AND pct.promotion_id = p.promotion_id
           AND pm.item_id = m.item_id
           AND pm.location_id = m.location_id 
           AND p.scenario_id <> 265
           AND p.ignore_promo_conflict = 0
           AND p.promotion_type_id = pt.promotion_type_id           
           AND pt.rr_check_promo = 1
           AND p.promotion_stat_id = ps.promotion_stat_id           
           AND ps.rr_check_promo = 1
           AND p.scenario_id = s.scenario_id   
           AND s.rr_check_promo = 1           
           AND pdt.promotion_id = p.promotion_id           
           AND pdt.until_date - pdt.from_date < (' || v_promo_week_range || ' * 7) + 1        
           AND m.t_ep_l_att_10_ep_id IN  ' || v_group_id1 || ' 
           AND m.t_ep_l_att_2_ep_id IN ' || v_group_id2 || ' 
           AND  m.' || v_id_field || ' =  lvl.' || v_id_field || '
           AND lvl.rr_promo_overlap <> 0
           AND m.t_ep_l_att_10_ep_id = pct.t_ep_l_att_10_ep_id
           AND m.t_ep_l_att_2_ep_id = pct.t_ep_l_att_2_ep_id
           AND pm.is_self = 1
           ' || v_where_pct || '
           AND m.item_id = pct.item_id
           AND i.datet = pct.sales_date
           AND ((pct.count_1 > 1))
           )';
    
      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);
      COMMIT;   
      
      dynamic_ddl('INSERT /*+ APPEND NOLOGGING */  INTO  cg_members_t (SELECT * FROM cg_members_t_' || rec.group_id1 || '_' || rec.group_id2 || ')');
      
      COMMIT;
      
      sql_str :=
         'INSERT /*+ APPEND NOLOGGING */ INTO t_ep_promotion_conflicts(promotion_id, conflicting_promo_id)           
          (SELECT /*+ parallel(pm, 12) */ DISTINCT m1.promotion_id, m2.promotion_id
          FROM cg_members_t m1,
          cg_members_t m2
          WHERE m1.promotion_id <> m2.promotion_id
          AND m1.item_id = m2.item_id
          AND (m1.level1 <> m2.level1 AND m1.t_ep_e1_cust_state_ep_id = m2.t_ep_e1_cust_state_ep_id)
          -- AND m1.location_id = m2.location_id
          AND (m1.sales_date + m1.rr_promo_offset_start BETWEEN m2.sales_date + m2.rr_promo_offset_start AND m2.sales_date + (m2.rr_promo_offset_start + 6)
          		OR
          		m1.sales_date + (m1.rr_promo_offset_start + 6) BETWEEN m2.sales_date + m2.rr_promo_offset_start AND m2.sales_date + (m2.rr_promo_offset_start + 6) 
          	  )
          )
         ';

      dynamic_ddl (sql_str);
      COMMIT;
      
      MERGE INTO promotion p
      USING(SELECT DISTINCT promotion_id FROM cg_members_t WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) p1
      ON(p.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, last_update_date = sysdate;
      
      COMMIT;
      
      MERGE INTO promotion_matrix pm
      USING(SELECT DISTINCT promotion_id, item_id, location_id FROM cg_members_t WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) pm1
      ON(pm.promotion_id = pm1.promotion_id
      AND pm.item_id = pm1.item_id
      AND pm.location_id = pm1.location_id)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, last_update_date = sysdate;
      
      COMMIT;      
      
      MERGE INTO promotion_data pd
      USING(SELECT DISTINCT promotion_id, item_id, location_id, sales_date FROM cg_members_t WHERE promotion_id IN (SELECT DISTINCT promotion_id FROM t_ep_promotion_conflicts)) pd1
      ON(pd.promotion_id = pd1.promotion_id
      AND pd.item_id = pd1.item_id
      AND pd.location_id = pd1.location_id
      AND pd.sales_date = pd1.sales_date)
      WHEN MATCHED THEN
      UPDATE SET t_ep_promo_conflict_id = 1, promo_conflict = 1, last_update_date = sysdate;
      
      COMMIT;                    
            
----------------------
--Setup Promo Conflict
----------------------   

   --RBURTON 3/5/2011 Added Activity Type & Promo Price to Conflict Details --
   MERGE INTO t_ep_promotion_conflicts s1
      USING (SELECT   /*+ parallel(s,24) parallel(pd,24) parallel(p,24) */
                      s.conflicting_promo_id, MIN(pt.promotion_type_desc) promotion_type_desc,
                      MIN(ec.ebs_customer_desc) account_name,
                      ROUND (AVG (pd.promo_price), 2) promo_price
                 FROM promotion_type pt,
                      t_ep_promotion_conflicts s,
                      promotion_data pd,
                      promotion p,
                      location l, 
                      t_ep_ebs_customer ec
                WHERE s.conflicting_promo_id = pd.promotion_id
                  AND p.promotion_id = pd.promotion_id
                  AND p.promotion_id = s.conflicting_promo_id
                  AND p.promotion_type_id = pt.promotion_type_id
                  AND pd.location_id = l.location_id
                  AND l.t_ep_ebs_customer_ep_id = ec.t_ep_ebs_customer_ep_id
             GROUP BY s.conflicting_promo_id) p1
      ON (p1.conflicting_promo_id = s1.conflicting_promo_id)
      WHEN MATCHED THEN
         UPDATE
            SET s1.activity_type = p1.promotion_type_desc,
                s1.promo_price = p1.promo_price,
                s1.account_name = p1.account_name
         ;
   COMMIT; 

   END LOOP;
   

   
     --Update Promotion with conflicting data --
      FOR rec IN (SELECT          /*+parallel(c,12) */
                         DISTINCT promotion_id, conflicting_promo_id,
                                  activity_type,
                                  account_name,
                                  LTRIM (TO_CHAR (promo_price, '99.99')
                                        ) promo_price,
                                  RANK() OVER (PARTITION BY promotion_id ORDER BY conflicting_promo_id) rank
                             FROM t_ep_promotion_conflicts c)
      LOOP
         UPDATE promotion p
            SET conflict_01 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 1;

         UPDATE promotion p
            SET conflict_02 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 2;

         UPDATE promotion p
            SET conflict_03 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 3;

         UPDATE promotion p
            SET conflict_04 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 4;

         UPDATE promotion p
            SET conflict_05 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 5;

         UPDATE promotion p
            SET conflict_06 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 6;

         UPDATE promotion p
            SET conflict_07 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 7;

         UPDATE promotion p
            SET conflict_08 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 8;

         UPDATE promotion p
            SET conflict_09 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 9;

         UPDATE promotion p
            SET conflict_10 =
                      rec.conflicting_promo_id
                   || ' : '
                   || rec.account_name
                   || ' : '
                   || rec.activity_type
                   || ' : '
                   || rec.promo_price
                   , last_update_date = sysdate
          WHERE p.promotion_id = rec.promotion_id AND rec.rank = 10;
          
      END LOOP;
   
   COMMIT;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_refresh_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL)
  AS
  
  sql_str         VARCHAR2 (32000);
  v_id_field      VARCHAR2 (100);
  v_search_table  VARCHAR2(20);
  v_gtable  VARCHAR2(300);
  cnt             NUMBER;
  
  v_prog_name    VARCHAR2 (100) := 'PRC_REFRESH_PROMO';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;   
  v_step         VARCHAR2(50);
  
  v_prod_cnt     NUMBER := 120;
  v_pg_cnt       NUMBER := 120;
  v_custno_cnt   NUMBER := 30;
  v_kad_cnt      NUMBER := 30;
  v_ka_cnt       NUMBER := 30;
  v_comb_cnt     NUMBER := 10000;
  
  v_accruals_type_id        NUMBER; 
  v_promotion_type_id       NUMBER; 
  v_t_ep_out_of_strategy_id NUMBER;  
  v_t_ep_promo_conflict_id  NUMBER;  
  v_promotion_stat_id       NUMBER; 
  v_promotion_guidelines_id NUMBER;  
  v_scenario_id             NUMBER; 
  
  p_user_id   NUMBER;
  p_member_id NUMBER;
  p_level_id  NUMBER;
  v_error     NUMBER;
  
  v_p_lvl_cnt NUMBER := 0;
  v_c_lvl_cnt NUMBER := 0;
  
  v_from_date   DATE;
  v_until_date  DATE;
  
  v_count       NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_REFRESH_PROMO';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);   

   COMMIT;
   
   SELECT COUNT(1) 
   INTO v_count
   FROM promotion
   WHERE promotion_id = member_id
   AND scenario_id IN (22, 262, 162);
   
   IF v_count <> 0 THEN
   
   v_step := '1 ';
   dbex (v_step || SYSDATE, v_prog_name);
   
   p_user_id := user_id;
   p_member_id := member_id;
   p_level_id := level_id;
   
   SELECT from_date, until_date
   INTO v_from_date, v_until_date
   FROM promotion_dates
   WHERE promotion_id = p_member_id;   
   
   SELECT COUNT(1) 
   INTO v_p_lvl_cnt
   FROM promotion_levels 
   WHERE level_id IN (12, 424, 362)
   AND promotion_id = p_member_id;
   
   SELECT COUNT(1) 
   INTO v_c_lvl_cnt
   FROM promotion_levels 
   WHERE level_id IN (426, 490, 491, 478, 366)
   AND promotion_id = p_member_id;
   
   IF v_p_lvl_cnt = 0 OR v_c_lvl_cnt = 0 THEN
    
    UPDATE promotion 
    SET oi_combs_status = 1
    WHERE promotion_id = p_member_id
      AND scenario_id <> 265;
    
    COMMIT;
    
    GOTO end_proc;
   
   END IF;
   
   SELECT accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
          promotion_guidelines_id, scenario_id
      INTO v_accruals_type_id, v_promotion_type_id, v_t_ep_out_of_strategy_id, v_t_ep_promo_conflict_id, v_promotion_stat_id,
        v_promotion_guidelines_id, v_scenario_id
   FROM promotion 
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;

   check_and_drop('tmpcombs_' || p_user_id || '_' || p_member_id);
   
   dynamic_ddl('CREATE TABLE tmpcombs_' || p_user_id || '_' || p_member_id || '(promotion_id NUMBER, item_id NUMBER, location_id NUMBER, 
                accruals_type_id NUMBER, promotion_type_id NUMBER, t_ep_out_of_strategy_id NUMBER, 
                t_ep_promo_conflict_id NUMBER, promotion_stat_id NUMBER, promotion_guidelines_id NUMBER, scenario_id NUMBER)');
   
   sql_str :=
            'MERGE /*+FULL(m1) PARALLEL(m1,16)*/  INTO tmpcombs_' || p_user_id || '_' || p_member_id || ' pm
                                      USING(SELECT '
         || p_member_id
         || ' promotion_id, i.item_id, l.location_id, '
         || v_accruals_type_id || ' accruals_type_id, '
         || v_promotion_type_id || ' promotion_type_id, '
         || v_t_ep_out_of_strategy_id || ' t_ep_out_of_strategy_id, '
         || v_t_ep_promo_conflict_id || ' t_ep_promo_conflict_id, '
         || v_promotion_stat_id || ' promotion_stat_id, '
         || v_promotion_guidelines_id  || ' promotion_guidelines_id, '
         || v_scenario_id || ' scenario_id
                            FROM items i,
                            location l                            
                            WHERE 1 = 1
                                                    ';

  FOR rec1 IN (SELECT DISTINCT level_id FROM
                   (SELECT DISTINCT usp.level_id
                              FROM user_security_population usp
                             WHERE usp.user_id = p_user_id  
                             AND usp.permission_id IN (3, 4)
                   UNION ALL
                   SELECT DISTINCT pl.level_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id)
                   WHERE level_id IN (SELECT group_table_id  
                                      FROM group_tables
                                      WHERE 1 = 1
                                      AND lower(search_table) IN ('items', 'location'))
                   )
   LOOP
         SELECT gtable, search_table, id_field
           INTO v_gtable, v_search_table, v_id_field
           FROM group_tables
          WHERE group_table_id = rec1.level_id
          AND lower(search_table) IN ('items', 'location');

         sql_str := sql_str || 'AND ' || CASE LOWER(v_search_table)
                                         WHEN 'items' THEN 'i'
                                         WHEN 'location' THEN 'l'
                                         END || '.' || v_id_field || ' IN (-1 ';

         cnt := 0;
         
         FOR rec2 IN (SELECT DISTINCT usp.member_id
                                 FROM user_security_population usp
                                WHERE usp.user_id = p_user_id
                                AND usp.permission_id IN (3, 4)
                                AND usp.level_id = rec1.level_id)
         LOOP
            sql_str := sql_str || ',' || rec2.member_id;
            cnt := cnt + 1;
         END LOOP;
         
         FOR rec2 IN (SELECT DISTINCT pl.level_id, pl.filter_id
                              FROM promotion_levels pl
                             WHERE pl.promotion_id = p_member_id AND pl.level_id = rec1.level_id)
        LOOP         

         FOR rec3 IN (SELECT DISTINCT pm.member_id
                                 FROM promotion_members pm
                                WHERE pm.filter_id = rec2.filter_id)
         LOOP
            sql_str := sql_str || ',' || rec3.member_id;
            cnt := cnt + 1;
         END LOOP;

         END LOOP;
               
        IF rec1.level_id IN (12, 362) AND cnt > v_prod_cnt THEN      
            v_error := 1;        
        ELSIF rec1.level_id = 424 AND cnt > v_pg_cnt THEN
            v_error := 1;
        ELSIF rec1.level_id IN (426, 478) AND cnt > v_custno_cnt THEN
            v_error := 1;  
        ELSIF rec1.level_id = 490 AND cnt > v_kad_cnt THEN
            v_error := 1;    
        ELSIF rec1.level_id IN (491, 366) AND cnt > v_ka_cnt THEN
            v_error := 1;        
        END IF;

         sql_str :=
            sql_str
            || ')
                  ';
      END LOOP;   
      
      IF v_error = 1 THEN 
        
        UPDATE promotion
        SET oi_combs_status = 2
        WHERE promotion_id = p_member_id
          AND scenario_id <> 265;
    
        COMMIT;    
        GOTO end_proc;
        
      END IF;
      
      sql_str :=
            sql_str
         || '
         AND NOT EXISTS (SELECT 1 FROM promotion_matrix pm2 WHERE pm2.promotion_id = ' || p_member_id || 
                        ' AND pm2.item_id = i.item_id AND pm2.location_id = l.location_id
                          AND pm2.scenario_id <> 265))pm1
                      ON(pm.promotion_id = pm1.promotion_id
                         AND pm.item_id = pm1.item_id
                         AND pm.location_id = pm1.location_id)
                                      WHEN NOT MATCHED THEN
                      INSERT(promotion_id, item_id, location_id, accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, t_ep_promo_conflict_id, promotion_stat_id,
                             promotion_guidelines_id, scenario_id)
                      VALUES(pm1.promotion_id, pm1.item_id, pm1.location_id, pm1.accruals_type_id, pm1.promotion_type_id, pm1.t_ep_out_of_strategy_id, pm1.t_ep_promo_conflict_id, 
                             pm1.promotion_stat_id, pm1.promotion_guidelines_id, pm1.scenario_id)';
      
      dbms_output.put_line(sql_str);
      dynamic_ddl (sql_str);      
      
      --EXECUTE IMMEDIATE sql_str;
      COMMIT;
   
   cnt := 0;
   
   EXECUTE IMMEDIATE 'SELECT COUNT(1) FROM tmpcombs_' || p_user_id || '_' || p_member_id
   INTO cnt;
   
   IF cnt > v_comb_cnt THEN
   
        UPDATE promotion
        SET oi_combs_status = 3
        WHERE promotion_id = p_member_id
        AND scenario_id <> 265;
    
        COMMIT;
    
        GOTO end_proc;
   
   END IF;  

   dynamic_ddl('INSERT INTO mdp_load_assist(item_id, location_id)
                SELECT DISTINCT item_id, location_id FROM tmpcombs_' || p_user_id || '_' || p_member_id);
                
   COMMIT;
   
   pkg_common_utils.prc_mdp_add('mdp_load_assist', v_from_date , v_until_date);
   
   dynamic_ddl('INSERT INTO promotion_matrix(promotion_id, item_id, location_id, 
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, 
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id, from_date, until_date)
                SELECT x.promotion_id, x.item_id, x.location_id, 
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id, 
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id, 
                TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),
                TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id');
   
   COMMIT;
   
   dynamic_ddl('INSERT INTO promotion_data(promotion_id, item_id, location_id, sales_date,
                accruals_type_id, promotion_type_id, t_ep_out_of_strategy_id, 
                t_ep_promo_conflict_id, promotion_stat_id, promotion_guidelines_id, scenario_id)
                SELECT x.promotion_id, x.item_id, x.location_id, i.datet sales_date,
                x.accruals_type_id, x.promotion_type_id, x.t_ep_out_of_strategy_id, 
                x.t_ep_promo_conflict_id, x.promotion_stat_id, x.promotion_guidelines_id, x.scenario_id
                FROM tmpcombs_' || p_user_id || '_' || p_member_id || ' x,
                mdp_matrix m,
                inputs i
                WHERE m.item_id = x.item_id
                AND m.location_id = x.location_id
                AND i.datet BETWEEN NEXT_DAY(TO_DATE(''' || TO_CHAR(v_from_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7
                AND NEXT_DAY(TO_DATE(''' || TO_CHAR(v_until_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MONDAY'') - 7');
   
   COMMIT;   
   
   UPDATE promotion
   SET oi_combs_status = 10
   WHERE promotion_id = p_member_id
     AND scenario_id <> 265;
    
   COMMIT;
   
   END IF;
   
<<end_proc>>   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            --RAISE;
            UPDATE promotion
            SET oi_combs_status = 9
            WHERE promotion_id = p_member_id
            AND scenario_id <> 265;
            
            COMMIT;
            
         END;
    END;
    
    PROCEDURE prc_edit_promo_desc(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  AS
/******************************************************************************
   NAME:       prc_edit_promo_desc
   PURPOSE:    Update Promotion Description When editing Promotions

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/05/2013  Luke Pocock

******************************************************************************/

   
   v_prog_name   VARCHAR2 (100)   := 'PRC_EDIT_PROMO_DESC';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   MERGE INTO promotion p1
      USING (SELECT          /*+ parallel(p,24) */
                    DISTINCT p.promotion_id,
                                TO_CHAR (from_date,
                                         'yyyy-mm-dd'
                                        )
                             || ' : '
                             || p.promotion_id || ' : NEW' AS promo_desc
                        FROM promotion_dates pt, promotion p
                       WHERE p.promotion_id = pt.promotion_id
                         AND p.promotion_id = member_id
                       ) p2
      ON (p2.promotion_id = p1.promotion_id)
      WHEN MATCHED THEN
         UPDATE
            SET p1.promotion_code = p2.promotion_id,
                p1.promotion_desc = p2.promo_desc, last_update_date = SYSDATE
         ;
   
   COMMIT;
   
   prc_set_pg_id(user_id, level_id, member_id);
   
   prc_set_promo_type(user_id, level_id, member_id);
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_set_pg_id(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL)
  AS
/******************************************************************************
   NAME:       prc_edit_promo_desc
   PURPOSE:    Update Promotion Description When editing Promotions

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/05/2013  Luke Pocock

******************************************************************************/

   
   v_prog_name   VARCHAR2 (100)   := 'PRC_SET_PG_ID';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
   v_lvl_count      NUMBER;
   v_member_id      NUMBER;
   
   v_level1         NUMBER; 
   v_level2         NUMBER;
   v_pg_id          NUMBER;
   v_scenario_id    NUMBER;
   v_year           VARCHAR2(4);

   
  BEGIN
  
   pre_logon;
   V_STATUS := 'Start ';
   --V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || sysdate);
     V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME,V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, USER_ID);

     v_member_id := member_id;
             
     v_pg_id := -1;
     
     SELECT scenario_id 
     INTO v_scenario_id
     FROM promotion 
     WHERE promotion_id = v_member_id; 
     
     IF v_scenario_id = 265 THEN 
        v_pg_id := 0;
        
     ELSE 
      
        v_lvl_count := 1;
        
        /* SELECT COUNT(DISTINCT pl.level_id)     
        INTO v_lvl_count 
        FROM promotion_levels pl
        WHERE 1 = 1
        AND pl.promotion_id = member_id
        AND pl.level_id IN (SELECT group_table_id FROM group_tables WHERE search_table IN ('items')); */
        
        IF 1 = 2 AND v_lvl_count <> 1 THEN 
        
            v_pg_id := 0;

        ELSE 
        
            --v_lvl_count := 0;
            
             SELECT COUNT(1)     
             INTO v_lvl_count 
             FROM promotion_levels pl, promotion_members pm
             WHERE 1 = 1
             AND pl.promotion_id = v_member_id
             AND pl.level_id = 12
             AND pl.filter_id = pm.filter_id;      
            
            IF 1 = 2 AND v_lvl_count <> 1 THEN 
        
                v_pg_id := 0;
        
            ELSE 
            
                SELECT COUNT(1) INTO v_lvl_count
                FROM (SELECT DISTINCT p2b.t_ep_p2b_ep_id, la6.t_ep_ebs_customer_ep_id
                FROM promotion_matrix pm, 
                mdp_matrix mdp,
                t_ep_p2b p2b,
                t_ep_ebs_customer la6
                WHERE la6.t_ep_ebs_customer_ep_id = mdp.t_ep_ebs_customer_ep_id
                AND p2b.t_ep_p2b_ep_id = mdp.t_ep_p2b_ep_id
                AND pm.promotion_id = v_member_id
                AND pm.item_id = mdp.item_id
                AND pm.location_id = mdp.location_id);
        
                IF v_lvl_count <> 1 THEN 
          
                    v_pg_id := 0;
                
                ELSE
                
                    SELECT DISTINCT p2b.t_ep_p2b_ep_id, la6.t_ep_ebs_customer_ep_id
                    INTO v_level1, v_level2
                    FROM promotion_matrix pm, 
                    mdp_matrix mdp,
                    t_ep_p2b p2b,
                    t_ep_ebs_customer la6
                    WHERE la6.t_ep_ebs_customer_ep_id = mdp.t_ep_ebs_customer_ep_id
                    AND p2b.t_ep_p2b_ep_id = mdp.t_ep_p2b_ep_id
                    AND pm.promotion_id = v_member_id
                    AND pm.item_id = mdp.item_id
                    AND pm.location_id = mdp.location_id;
                    
                    SELECT TO_CHAR(from_date, 'YYYY') INTO v_year 
                    FROM promotion_dates 
                    WHERE promotion_id = v_member_id;
                    
                    BEGIN 
                    SELECT promotion_guidelines_id INTO v_pg_id 
                    FROM promotion_guidelines 
                    WHERE level1 = v_level1
                    AND level2 = v_level2
                    AND promotion_guidelines_code LIKE '%' || v_year ;
                    
                    EXCEPTION 
                    WHEN OTHERS THEN 
                    v_pg_id := 0;
                    END;
          
                END IF;                      
                    
            END IF;
            
        END IF;
     
     END IF;
     
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_pg_id  || SYSDATE);
     
     UPDATE promotion 
     SET promotion_guidelines_id = v_pg_id 
     WHERE promotion_id = v_member_id
     AND promotion_guidelines_id <> v_pg_id;
     
     UPDATE promotion_data
     SET promotion_guidelines_id = v_pg_id 
     WHERE promotion_id = v_member_id
     AND promotion_guidelines_id <> v_pg_id;
     
     UPDATE promotion_matrix 
     SET promotion_guidelines_id = v_pg_id 
     WHERE promotion_id = v_member_id
     AND promotion_guidelines_id <> v_pg_id;
     
     COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
        
   PROCEDURE prc_approve_gr    (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL)
  AS
  
  sql_str         VARCHAR2 (32000);
  v_id_field      VARCHAR2 (100);
  v_search_table  VARCHAR2(20);
  v_gtable  VARCHAR2(300);
  cnt             NUMBER;
  
  v_prog_name    VARCHAR2 (100) := 'PRC_APPROVE_GR';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;   
  v_step         VARCHAR2(50);
 
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);   

    update promotion_data branch_data
    set
        branch_data.gr_shelf_price = nvl(branch_data.ed_price,0),
        branch_data.gr_promo_price = nvl(branch_data.promo_price,0),
        branch_data.gr_case_deal = nvl(branch_data.case_buydown,0),
        branch_data.gr_case_deal_oi = nvl(branch_data.case_buydownoi,0),
        branch_data.gr_case_deal_oi_per = nvl(branch_data.rr_oi_p,0),
        last_update_date = sysdate
    where promotion_id = member_id;
    
    commit;
    
    update promotion
    set ignore_guardrail_rules = 1,
        gr_approve_date = sysdate,
        gr_approve_user = user_id,
        last_update_date = sysdate
    where promotion_id = member_id;
    
    commit;
    
    UPDATE promotion_matrix
      SET last_update_date = SYSDATE,
          promotion_lud = SYSDATE,
          promotion_data_lud = SYSDATE
    WHERE promotion_id = member_id;   
    
    commit;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
                     
         END;
    END;      

  FUNCTION fcn_get_ge(p_gondola_end_id     NUMBER DEFAULT NULL,
                      p_activity_type_id    NUMBER DEFAULT NULL)
  RETURN NUMBER
  AS
  
  v_prog_name    VARCHAR2 (100) := 'FCN_GET_GE';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;   
  v_step         VARCHAR2(50);
 
  BEGIN

   --pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || p_gondola_end_id || p_activity_type_id  || SYSDATE);   

   IF p_gondola_end_id = 0 THEN 
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   RETURN 1;
   
   END IF;
   
   IF p_activity_type_id IN (4, 5, 17, 9) THEN  
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   RETURN 1; 
   
   END IF;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   
   RETURN 0;
        
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
                     
         END;
    END;      

 
  PROCEDURE prc_set_promo_type(user_id   NUMBER DEFAULT NULL,
                                level_id  NUMBER DEFAULT NULL,
                                member_id NUMBER DEFAULT NULL)
  AS
  
  
  v_gondola_end_id  NUMBER;
  v_activity_id  NUMBER;
  v_advertising_id  NUMBER;
  v_PROMO_TACTIC_ID NUMBER;
  v_promo_multiplier_id NUMBER;
  
  v_promotion_type_id NUMBER; 
  
  v_prog_name    VARCHAR2 (100) := 'PRC_SET_PROMO_TYPE';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;   
  v_step         VARCHAR2(50);
 
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);   
   
   SELECT gondola_end_id, activity_id, advertising_id, promo_multiplier_id, PROMO_TACTIC_ID
   INTO v_gondola_end_id, v_activity_id, v_advertising_id, v_promo_multiplier_id, v_PROMO_TACTIC_ID
   FROM promotion 
   WHERE promotion_id = member_id;
   
   SELECT promotion_type_id 
   INTO v_promotion_type_id
   FROM promotion_type 
   WHERE gondola_end_id = v_gondola_end_id
   AND activity_id = v_activity_id
   AND advertising_id = v_advertising_id;
   
   UPDATE promotion 
   SET promotion_type_id = v_promotion_type_id, last_update_date = sysdate
   WHERE promotion_id = member_id;
   
   UPDATE promotion_matrix 
   SET promotion_type_id = v_promotion_type_id, activity_id = v_activity_id, gondola_end_id = v_gondola_end_id,
       advertising_id = v_advertising_id, promo_multiplier_id = v_promo_multiplier_id, PROMO_TACTIC_ID = v_PROMO_TACTIC_ID, last_update_date = sysdate
   WHERE promotion_id = member_id;
   
   UPDATE promotion_data 
   SET promotion_type_id = v_promotion_type_id, activity_id = v_activity_id, gondola_end_id = v_gondola_end_id,
       advertising_id = v_advertising_id, promo_multiplier_id = v_promo_multiplier_id, PROMO_TACTIC_ID = v_PROMO_TACTIC_ID, last_update_date = sysdate
   WHERE promotion_id = member_id;

   COMMIT;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
                     
         END;
    END;      
       
  PROCEDURE prc_cancel_promotion(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL     
) AS

   v_prog_name      VARCHAR2 (100);
   v_status         VARCHAR2 (100);
   v_proc_log_id     NUMBER;
   v_activity_id NUMBER;
   
BEGIN
  pre_logon;
  v_status := 'start ';
  v_prog_name := 'PRC_CANCEL_PROMOTION';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE, level_id, member_id, user_id);
  
  
  select activity_id into v_activity_id from promotion where promotion_id = member_id;
  
  if V_ACTIVITY_ID <> 2 
  THEN
  update PROMOTION 
  set PROMOTION_STAT_ID = 9, LAST_UPDATE_DATE = sysdate, RR_PROMO_CANCELED_DATE = sysdate
  WHERE promotion_id = member_id;
 
  UPDATE promotion_matrix 
  SET promotion_stat_id = 9, last_update_date = SYSDATE, promotion_lud = SYSDATE 
  WHERE promotion_id = member_id;
  
  UPDATE promotion_data 
  SET promotion_stat_id = 9, last_update_date = SYSDATE
  WHERE promotion_id = member_id;
  
  UPDATE accrual_data 
  SET promotion_stat_id = 9
  WHERE ACCRUAL_ID = MEMBER_ID;
  ELSIF V_ACTIVITY_ID = 2 
  THEN
  update PROMOTION 
  SET promotion_stat_id = 8, RR_EXP_OI_FLAG = 3, last_update_date = SYSDATE, rr_promo_canceled_date = SYSDATE
  where PROMOTION_ID = MEMBER_ID;
 
  update PROMOTION_MATRIX 
  SET promotion_stat_id = 8, last_update_date = SYSDATE, promotion_lud = SYSDATE 
  WHERE promotion_id = member_id;
  
  UPDATE promotion_data 
  SET promotion_stat_id = 8, last_update_date = SYSDATE
  WHERE promotion_id = member_id;
  
  update ACCRUAL_DATA 
  SET PROMOTION_STAT_ID = 8
  WHERE ACCRUAL_ID = MEMBER_ID;
  END IF;
  
  
  commit;
 
  
  
  
v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
     BEGIN
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
     END;
  
END;  

 PROCEDURE prc_cancel_oi_promo(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL     
) AS

   v_prog_name      VARCHAR2 (100);
   v_status         VARCHAR2 (100);
   v_proc_log_id     NUMBER;
   v_activity_id NUMBER;
   
BEGIN
  PRE_LOGON;
  V_STATUS := 'start ';
  v_prog_name := 'PRC_CANCEL_OI_PROMO';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE, level_id, member_id, user_id);
  
  
  select activity_id into v_activity_id from promotion where promotion_id = member_id;
  
  IF V_ACTIVITY_ID = 2
  then 
    update PROMOTION 
  SET promotion_stat_id = 8, RR_EXP_OI_FLAG = 3, last_update_date = SYSDATE, rr_promo_canceled_date = SYSDATE
  where PROMOTION_ID = MEMBER_ID;
  end if;
 
  update PROMOTION_MATRIX 
  SET promotion_stat_id = 8, last_update_date = SYSDATE, promotion_lud = SYSDATE 
  WHERE promotion_id = member_id;
  
  UPDATE promotion_data 
  SET promotion_stat_id = 8, last_update_date = SYSDATE
  WHERE promotion_id = member_id;
  
  update ACCRUAL_DATA 
  SET promotion_stat_id = 8
  WHERE accrual_id = member_id;
   
  commit;
  
v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
     BEGIN
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
     END;
  
END;  
  
  PROCEDURE prc_create_oi_promo(user_id   NUMBER DEFAULT NULL,
                                LEVEL_ID  NUMBER DEFAULT NULL,
                                MEMBER_ID NUMBER DEFAULT NULL)
  AS

   v_prog_name      VARCHAR2 (100);
   v_status         VARCHAR2 (100);
   V_PROC_LOG_ID     number;
      
   v_access_seq_id NUMBER;
   seq access_seq%rowtype;
   v_cnt            NUMBER;
   v_pc_lvl_chk     NUMBER :=1;
   V_COMB_PATH varchar2(400);
   
BEGIN
  pre_logon;
  v_status := 'start ';
  v_prog_name := 'PRC_CREATE_OI_PROMO';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, USER_ID);
  
 -- V_COMB_PATH := COMB_PATH;
  
  SELECT access_seq_id into v_access_seq_id from comp_type_seq where comp_type_seq_id in (select comp_type_seq_id from promotion where promotion_id = member_id);
  
  select * into seq from access_seq where access_seq_id = v_access_seq_id;
  
------------------------------------------------
---- Key Combinations Level Validation ----------
------------------------------------------------

  if nvl(v_access_seq_id,0) <> 0
  then
        if seq.level1 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level1 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level2 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level2 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level3 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level3 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level4 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level4 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level5 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level5 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
       if seq.level6 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level6 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;  
        if seq.level7 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level7 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level8 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level8 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level9 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level9 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level10 is not null
        then
          select count(1) into v_cnt from promotion_levels where level_id = seq.level10 and promotion_id = member_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
  end if;
  
  if (v_pc_lvl_chk = 0)    then
  
     update promotion set method_Status = 11, oi_promo_status = 2,approval = 0, ACCESS_SEQ = v_access_seq_id, promo_create_user = user_id
     where promotion_id = member_id;
     COMMIT;
     
  else
    
     update PROMOTION set method_status = 3, OI_PROMO_STATUS = 1,ACCESS_SEQ = v_access_seq_id, promo_create_user = user_id where PROMOTION_ID = MEMBER_ID;
     commit;
     
     ----Delete Levels that dont match the Access Sequence
     
     select nvl(level1,0) || ',' || nvl(level2,0) || ',' || nvl(level3,0) || ',' || nvl(level4,0) || ',' || nvl(level5,0) || ',' || 
     nvl(level6,0) || ','  || nvl(level7,0) || ',' || nvl(level8,0) || ',' || nvl(level9,0) || ',' || nvl(level10,0)  into V_COMB_PATH
     from access_seq 
     where access_seq_id = v_access_seq_id;
     
     DYNAMIC_DDL('DELETE FROM promotion_levels
     WHERE level_id not in ( ' || V_COMB_PATH ||' ) 
     and promotion_id = ' || member_id);
     commit;
     
     PRC_SET_PG_ID(USER_ID, LEVEL_ID, MEMBER_ID);
     prc_set_promo_type(USER_ID, LEVEL_ID, MEMBER_ID);

  end if;                          

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
     BEGIN
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
     END;
  
END;  
    
 PROCEDURE prc_chg_oi_promo_dates(user_id   NUMBER DEFAULT NULL,
                                LEVEL_ID  NUMBER DEFAULT NULL,
                                MEMBER_ID NUMBER DEFAULT NULL)
 AS

   v_prog_name      VARCHAR2 (100);
   v_status         VARCHAR2 (100);
   V_PROC_LOG_ID     number;
   v_start_date DATE;    
   V_END_DATE  date;
   v_new_end_Date DATE;
   seq access_seq%rowtype;
   v_cnt            NUMBER;
   v_pc_lvl_chk     NUMBER :=1;
   V_COMB_PATH varchar2(400);
   
begin

  pre_logon;
  V_STATUS := 'start ';
  v_prog_name := 'PRC_CHG_OI_PROMO_DATES';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, USER_ID);
  

  select FROM_DATE,UNTIL_DATE into V_START_DATE,V_END_DATE from PROMOTION_DATES where PROMOTION_ID = MEMBER_ID;
  
  select new_end_Date into v_new_end_Date from promotion where promotion_id = member_id;
  
  if V_NEW_END_DATE is not null 
  then
  if V_END_DATE < V_START_DATE 
  then 
  update PROMOTION set METHOD_STATUS = 16,LAST_UPDATE_DATE = sysdate where PROMOTION_ID = MEMBER_ID ;
  elsif V_NEW_END_DATE > V_END_DATE
  then 
   update PROMOTION set METHOD_STATUS = 12,LAST_UPDATE_DATE = sysdate where PROMOTION_ID = MEMBER_ID ;
   else
  update PROMOTION set METHOD_STATUS = 3,RR_EXP_OI_FLAG = 2,LAST_UPDATE_DATE = sysdate where PROMOTION_ID = MEMBER_ID ;
  update promotion_dates set until_Date = v_new_end_Date where promotion_id = member_id;
  end if;
  end if;
 commit;                             
   v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
     BEGIN
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
     END;
  
END;  

  FUNCTION fcn_get_fund_type(p_fund_type_id   NUMBER DEFAULT NULL)
  return number
  AS
  
  V_PROG_NAME    varchar2 (100) := 'FCN_GET_FUND_TYPE';
  v_status       VARCHAR2 (100);
  v_proc_log_id  NUMBER;   
  V_STEP         varchar2(50);
  v_oi_flag NUMBER;
 
  BEGIN

   --pre_logon;
   v_status := 'Start ';
   V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || p_fund_type_id || sysdate);   

   select nvl(OI_FLAG,0) into V_OI_FLAG from COMP_TYPE where COMP_TYPE_ID = P_FUND_TYPE_ID;
   
   return v_oi_flag; 
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   
   RETURN 0;
        
   
   EXCEPTION
      WHEN OTHERS
      then
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
                     
         END;
    END; 
    
 PROCEDURE PRC_FIX_PROMOTION(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL     
) AS

   v_prog_name      VARCHAR2 (100);
   v_status         VARCHAR2 (100);
   v_proc_log_id     NUMBER;

   v_check_flag     NUMBER;
   
   
   
BEGIN
  pre_logon;
  v_status := 'start ';
  v_prog_name := 'PRC_FIX_PROMOTION';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_status || SYSDATE, level_id, member_id, user_id);
  
  SELECT COUNT(1) 
  INTO v_check_flag
  FROM promotion_data 
  WHERE sales_date < (SELECT GREATEST(NEXT_DAY(from_date, 'MONDAY') -7, TRUNC(from_date, 'MM'))
                       FROM promotion_dates WHERE promotion_id = member_id) 
  AND promotion_id = member_id;
  
  IF v_check_flag = 0 THEN 
    GOTO end_proc;
  END IF;
  
  MERGE INTO promotion_data pd
  USING(SELECT TRUNC(pdt.from_date, 'MM') sales_date, pd.item_id, pd.location_id, pd.promotion_id, 
  pd.event_cost
 -- pd.rr_planned_mktg_spend,
 -- pd.rr_incr_promo_vol_ovrd,
 -- pd.rr_notional_coop_fund,
 -- pd.rr_sch_mkt_act_cost
  FROM promotion_data pd,
  promotion_dates pdt
  WHERE pdt.promotion_id = member_id
  AND pdt.promotion_id = pd.promotion_id 
  AND pd.sales_date = NEXT_DAY(pdt.from_date, 'MONDAY') -7) pd1
  ON(pd.promotion_id = pd1.promotion_id
  AND pd.sales_date = pd1.sales_date
  AND pd.item_id = pd1.item_id 
  AND pd.location_id = pd1.location_id)
  when matched then 
  update set event_cost = nvl(pd.event_cost, 0) + nvl(pd1.event_cost, 0);
 -- rr_planned_mktg_spend = NVL(pd.rr_planned_mktg_spend, 0) + NVL(pd1.rr_planned_mktg_spend, 0),
 -- rr_incr_promo_vol_ovrd = NVL(pd.rr_incr_promo_vol_ovrd, 0) + NVL(pd1.rr_incr_promo_vol_ovrd, 0), 
 -- rr_notional_coop_fund = NVL(pd.rr_notional_coop_fund, 0) + NVL(pd1.rr_notional_coop_fund, 0),
 -- rr_sch_mkt_act_cost = NVL(pd.rr_sch_mkt_act_cost, 0) + NVL(pd1.rr_sch_mkt_act_cost, 0);
    
  
  DELETE FROM promotion_data 
  WHERE sales_date < (SELECT GREATEST(NEXT_DAY(from_date, 'MONDAY') -7, TRUNC(from_date, 'MM'))
                       FROM promotion_dates WHERE promotion_id = member_id) 
  AND promotion_id = member_id;
  
  
  COMMIT;
  
<<end_proc>>  
  
  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_status || SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
     BEGIN
       v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
        RAISE;
     END;
  
end prc_fix_promotion;  
   

END;

/
