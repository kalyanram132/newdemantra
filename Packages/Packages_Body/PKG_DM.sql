--------------------------------------------------------
--  DDL for Package Body PKG_DM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_DM" 
AS
/******************************************************************************
   NAME:         PKG_DM  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version
   
   ******************************************************************************/

  PROCEDURE prc_update_dem_type
  IS
/******************************************************************************
   NAME:       PRC_UPDATE_DEM_TYPE
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   maxdate        DATE;
   
   v_day          VARCHAR2(10);

   v_prog_name    VARCHAR2 (100) := 'PRC_UPDATE_DEM_TYPE';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER; 
   sql_str1   VARCHAR2 (20000);
   sql_str2  VARCHAR2 (20000);
   sql_str3  VARCHAR2 (20000);
   
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_DEM_TYPE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   maxdate := NEXT_DAY(to_char(SYSDATE, 'mm/dd/yyyy'), 'MONDAY');
 
 -- SR-- NZ LTF Enhancement --
   
 --  UPDATE /*+ FULL(mdp) PARALLEL(mdp, 32) */ mdp_matrix mdp
 --  SET demand_type_id = CASE WHEN t_ep_e1_cust_cat_2_ep_id = 1 AND t_ep_l_att_10_ep_id = 17 THEN 1 ELSE 0 END
 --  WHERE demand_type_id <> CASE WHEN t_ep_e1_cust_cat_2_ep_id = 1 AND t_ep_l_att_10_ep_id = 17 THEN 1 ELSE 0 END;
         
--   UPDATE /*+ FULL(mdp) PARALLEL(mdp, 32) */ mdp_matrix mdp
--   SET demand_type_id = CASE WHEN t_ep_e1_cust_cat_2_ep_id = 1 AND t_ep_l_att_10_ep_id in ( 12,17) THEN 1 ELSE 0 END
--   WHERE demand_type_id <> CASE WHEN t_ep_e1_cust_cat_2_ep_id = 1 AND t_ep_l_att_10_ep_id in ( 12,17) THEN 1 ELSE 0 END;

--  COMMIT;

-- Final step - update SD demand_type to use either Exfactory or Scan --

-- commenting it out for temp issue INC0046165  -- SR


--   MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
--    USING (SELECT /*+ full(sd) parallel(sd, 32) */ sd.item_id, sd.location_id, sd.sales_date,
/*                             m.dem_stream
                        FROM mdp_matrix m, sales_data sd
                       WHERE m.item_id = sd.item_id
                         AND m.location_id = sd.location_id
                         AND m.dem_stream <> sd.demand_type
                         ) sd1
      ON (    sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.sales_date = sd.sales_date
          )
      WHEN MATCHED THEN
         UPDATE
            SET sd.demand_type = sd1.dem_stream, last_update_date = SYSDATE
         ;
 */
 
 check_and_drop('RR_UPDATE_DEM_TEMP');
 -- New code for temp issue INC0046165  -- SR
   
    sql_str1 := 'create table RR_UPDATE_DEM_TEMP as SELECT /*+ full(sd) parallel(sd, 32) */ sd.item_id, sd.location_id, sd.sales_date,
                             m.dem_stream
                        FROM mdp_matrix m, sales_data sd
                       WHERE m.item_id = sd.item_id
                         AND m.location_id = sd.location_id
                         AND m.dem_stream <> sd.demand_type';
    dynamic_ddl (sql_str1);
   
   
   sql_str2 := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
                USING (
                        select * from RR_UPDATE_DEM_TEMP
                         ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.sales_date = sd.sales_date
          )
      WHEN MATCHED THEN
         UPDATE
            SET sd.demand_type = sd1.dem_stream, last_update_date = SYSDATE';
    dynamic_ddl (sql_str2);       
   COMMIT;
   
   
    sql_str3 := 'drop table RR_UPDATE_DEM_TEMP';
    dynamic_ddl (sql_str3);
    COMMIT;
   
   
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_inv_adjust
  AS
/******************************************************************************
   NAME:       PRC_INV_ADJUST
   PURPOSE:    procedure to populate inventory adjustment

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_day            VARCHAR2(10);
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_INV_ADJUST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_INV_ADJUST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';
   
   dynamic_ddl('TRUNCATE TABLE tbl_promo_dates');
   dynamic_ddl('TRUNCATE TABLE tbl_sd_inv');
   


   INSERT /*APPEND NOLOGGING */ INTO tbl_promo_dates(promotion_id, sales_date, inv_adj, inv_adj_flag)
   SELECT p.promotion_id,
          i.datet sales_date,
   CASE WHEN i.datet = (NEXT_DAY(pdt.from_date, v_day) -7) AND i.datet = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100
     WHEN i.datet = (NEXT_DAY(pdt.from_date, v_day) -7) THEN NVL(p.start_inv_adj, 100)/100
     WHEN i.datet > (NEXT_DAY(pdt.from_date, v_day) - 7) AND i.datet < (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1
     WHEN i.datet = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.end_inv_adj, 100)/100
     ELSE CASE WHEN (CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                    - CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1 ELSE 2 END
                    - ((i.datet - (NEXT_DAY(pdt.until_date, v_day) - 7))/ 7) >= 0 THEN 0
               ELSE  1 - ((CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                          - TRUNC( (CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END)))
               END
   END inv_adj,
   p.inv_adj_flag
   FROM promotion_dates pdt,
   inputs i, 
   promotion p
   WHERE p.promotion_id = pdt.promotion_id
   AND NVL(p.inv_adj_flag, 0) <> 0
   AND pdt.until_date > TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')
   AND i.datet BETWEEN NEXT_DAY(pdt.from_date, v_day) - 7 AND (NEXT_DAY(pdt.until_date, v_day) - 7) + ((CEIL(CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                                                                          - CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1 ELSE 2 END) * 7);

   COMMIT; 

   INSERT /* APPEND NOLOGGING */ INTO tbl_sd_inv(item_id, location_id, sales_date, inv_adj)
   SELECT pm.item_id, pm.location_id, CASE WHEN p.inv_adj_flag = 1 THEN p.sales_date
                                           WHEN p.inv_adj_flag = 2 THEN p.sales_date - (NVL(mdp.scan_forecast_offset, 0) * 7)
                                           ELSE p.sales_date END sales_date, 
   MAX(p.inv_adj) inv_adj 
   FROM promotion_matrix pm, 
   tbl_promo_dates p,
   mdp_matrix mdp
   WHERE p.promotion_id = pm.promotion_id
   AND mdp.item_id = pm.item_id
   AND mdp.location_id = pm.location_id
   GROUP BY pm.item_id, pm.location_id, CASE WHEN p.inv_adj_flag = 1 THEN p.sales_date
                                           WHEN p.inv_adj_flag = 2 THEN p.sales_date - (NVL(mdp.scan_forecast_offset, 0) * 7)
                                           ELSE p.sales_date END;
  
   COMMIT;


   UPDATE /*+ PARALLEL(sd) */ sales_data sd
   SET sd.rr_invest_p = 1
   WHERE sd.sales_date BETWEEN TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS') AND TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS') + (104 * 7)
   AND NVL(sd.rr_invest_p, 1) <> 1;
   
   COMMIT;
   
  
MERGE /* INDEX(sd, SALES_DATA_PK) */ INTO sales_data sd
   USING(SELECT /*+ FULL(sd) PARALLEL(sd_inv,32 ) INDEX(sd, SALES_DATA_PK)  */ 
        sd.item_id, sd.location_id, sd.sales_date, NVL(sd_inv.inv_adj, 1) inv_adj
        FROM sales_data sd,
        tbl_sd_inv sd_inv
        WHERE sd.sales_date = sd_inv.sales_date 
        AND sd.item_id = sd_inv.item_id 
        AND sd.location_id = sd_inv.location_id
        AND sd_inv.sales_date > TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')
        AND NVL(sd.rr_invest_p, 1) <> NVL(sd_inv.inv_adj, 1) ) sd1
   ON(sd.item_id = sd1.item_id
   AND sd.location_id = sd1.location_id
   AND sd.sales_date = sd1.sales_date)
   WHEN MATCHED THEN 
   UPDATE SET sd.rr_invest_p = sd1.inv_adj,
              sd.last_update_date = sysdate;
  
   COMMIT;
        
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_sync_mdp_2_mdp(p_query_name VARCHAR2,
                               p_dem_alloc NUMBER)
  IS
/******************************************************************************
   NAME:       PRC_CORRECT_PD_VOL_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        03/09/2012  Lakshmi Annapragada / Redrock Consulting - Initial Version

******************************************************************************/
   start_date    DATE;
   end_date      DATE;

   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);
   
   v_eng_profile NUMBER := 203;

   v_prog_name    VARCHAR2 (100);
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER; 
   
   v_query_name   VARCHAR2 (200);
   v_sd_col_name   VARCHAR2 (200);
     
   v_query_id     NUMBER; 
   v_sql_str1     VARCHAR2(32767);
   v_sql_str2     VARCHAR2(32767);
   v_sql_str3     VARCHAR2(32767);
   v_sql_str4     VARCHAR2(32767);
   v_sql_str5     VARCHAR2(32767);
   v_sql_str6     VARCHAR2(32767);
   v_sql_str7     VARCHAR2(32767);
   v_sql_str8     VARCHAR2(32767);
   v_sql_str9     VARCHAR2(32767);
   
   v_sql          VARCHAR2(32767);
      
   v_start_date   VARCHAR2(200);

   v_end_date     VARCHAR2(200);
   v_day          VARCHAR2(10) := 'MONDAY';
   v_weeks        NUMBER := 78;
   
  BEGIN

   pre_logon;
   v_status := 'start ';
   v_prog_name := 'PRC_SYNC_MDP_2_MDP';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   --v_day := rr_pkg_common_utils.prc_day_of_week;

   v_query_name := p_query_name;
   
   IF v_query_name IS NULL
   THEN v_status := 'Enter worksheet name';
   raise_application_error(-20001, 'Enter Worksheet Name');
   ELSE
    
    BEGIN 
    SELECT query_id into v_query_id from queries where query_name like v_query_name;
    EXCEPTION 
    WHEN no_data_found THEN
    --v_query_id := dynamic_number('select query_id from queries where query_name like '''''||v_query_name||'''''');
          v_status := 'Wrong Worksheet Name';
          raise_application_error(-20001, 'Wrong Worksheet Name');
    WHEN OTHERS THEN RAISE;
    END;
    END IF;
    
    v_sql_str1 := '';
    v_sql_str2 := 'MERGE INTO mdp_matrix mdp USING (SELECT /*+full(m2) parallel(m2,32)*/  ';
    v_sql_str3 := '' ;
    v_sql_str4 := ' FROM mdp_matrix mdp WHERE 1 = 1 ';
    v_sql_str5 := ' 1 = 1';    
    v_sql_str6 := '';
    v_sql_str7 := '';
    v_sql_str8 := '';
    v_sql_str9 := '';
  
    FOR rec IN 
        (SELECT f.filter_id,
                gt.id_field
          FROM filters f,            
              group_tables gt
          WHERE 1 = 1
            AND gt.group_table_id = f.group_table_id
            AND f.query_id        = v_query_id)
     LOOP 
       
         v_sql_str4 := v_sql_str4 ||' AND mdp.'||rec.id_field||' IN (';
         v_sql_str7 := v_sql_str7 ||' AND mdp.'||rec.id_field||' IN (';
       
         FOR rec1 IN (SELECT fv.filter_value FROM filter_values fv WHERE fv.filter_id = rec.filter_id)
         LOOP
       
            v_sql_str4 := v_sql_str4 || ' ' || rec1.filter_value || ',';
            v_sql_str7 := v_sql_str7 || ' ' || rec1.filter_value || ',';
       
         END LOOP;
       
         v_sql_str4 := RTRIM(v_sql_str4, ',');
         v_sql_str4 := v_sql_str4 || ')'; 
         
         v_sql_str7 := RTRIM(v_sql_str7, ',');
         v_sql_str7 := v_sql_str7 || ')';     
         
     END LOOP;
     
     v_sql_str4 := v_sql_str4||' GROUP BY 1';
   
     FOR rec IN 
        (SELECT gt.gtable,gt.id_field 
        FROM groups g,group_tables gt 
        WHERE g.group_table_id = gt.group_table_id
          AND g.query_id = v_query_id)
     LOOP 
                  
         v_sql_str4 := v_sql_str4 || ', mdp.' || rec.id_field;
         v_sql_str5 := v_sql_str5 ||' AND mdp1.' || rec.id_field || ' = mdp.' || rec.id_field;
         v_sql_str6 := v_sql_str6 || ' mdp.' || rec.id_field||',';
         
     END LOOP;       
     
     v_sql_str6 := RTRIM(v_sql_str6, ',');    
 
    v_sql_str1 := v_sql_str2 || v_sql_str6 || CASE WHEN p_dem_alloc IN (1) THEN  ', max(nvl(mdp.dem_stream,0)) dem_stream ' 
                  WHEN p_dem_alloc IN (2) THEN  '
                  , max(nvl(mdp.allocation_wk0,0)) allocation_wk0
                  , max(nvl(mdp.allocation_wk1,0)) allocation_wk1
                  , max(nvl(mdp.allocation_wk2,0)) allocation_wk2
                  , max(nvl(mdp.allocation_wk3,0)) allocation_wk3
                  , max(nvl(mdp.allocation_wk4,0)) allocation_wk4
                  , max(nvl(mdp.allocation_wk5,0)) allocation_wk5
                  , max(nvl(mdp.allocation_wk6,0)) allocation_wk6' 
                  WHEN p_dem_alloc IN (3) THEN  '
                  , max(nvl(mdp.rr_stan_pp,0)) rr_stan_pp 
                  , max(nvl(mdp.rr_advert_pp,0)) rr_advert_pp
                  , max(nvl(mdp.rr_gr_rrp,0)) rr_gr_rrp
                  , max(nvl(mdp.rr_stan_max,0)) rr_stan_max
                  , max(nvl(mdp.rr_advert_max,0)) rr_advert_max'                 
                  ELSE '' END || v_sql_str4 || ') mdp1
                  ON( ' || v_sql_str5 || v_sql_str7 || ')
                  WHEN MATCHED THEN 
                  UPDATE SET ' || CASE WHEN p_dem_alloc IN (1) THEN  'dem_stream = mdp1.dem_stream  '
                  WHEN p_dem_alloc IN (2) THEN  '
                  allocation_wk0 = mdp1.allocation_wk0
                  ,allocation_wk1 = mdp1.allocation_wk1
                  ,allocation_wk2 = mdp1.allocation_wk2
                  ,allocation_wk3 = mdp1.allocation_wk3
                  ,allocation_wk4 = mdp1.allocation_wk4
                  ,allocation_wk5 = mdp1.allocation_wk5
                  ,allocation_wk6 = mdp1.allocation_wk6' 
                  WHEN p_dem_alloc = 3 THEN  '
                  rr_stan_pp = mdp1.rr_stan_pp
                  ,rr_advert_pp = mdp1.rr_advert_pp
                  ,rr_gr_rrp = mdp1.rr_gr_rrp
                  ,rr_stan_max = mdp1.rr_stan_max
                  ,rr_stan_max = mdp1.rr_stan_max'  
                  ELSE '' END || ' 
                  WHERE ( 1 = 2      
                  ' || CASE WHEN p_dem_alloc IN (1) THEN  ' OR mdp.dem_stream <> mdp1.dem_stream  '
                  WHEN p_dem_alloc IN (2) THEN  '
                  OR mdp.allocation_wk0 <> mdp1.allocation_wk0
                  OR mdp.allocation_wk1 <> mdp1.allocation_wk1
                  OR mdp.allocation_wk2 <> mdp1.allocation_wk2
                  OR mdp.allocation_wk3 <> mdp1.allocation_wk3
                  OR mdp.allocation_wk4 <> mdp1.allocation_wk4
                  OR mdp.allocation_wk5 <> mdp1.allocation_wk5
                  OR mdp.allocation_wk6 <> mdp1.allocation_wk6' 
                  WHEN p_dem_alloc IN (3) THEN  '
                  OR mdp.rr_stan_pp <> mdp1.rr_stan_pp
                  OR mdp.rr_advert_pp <> mdp1.rr_advert_pp
                  OR mdp.rr_gr_rrp <> mdp1.rr_gr_rrp
                  OR mdp.rr_stan_max <> mdp1.rr_stan_max
                  OR mdp.rr_advert_max <> mdp1.rr_advert_max'
                  ELSE '' END || '
                  )
                  ';
 
    -- dbms_output.put_line(v_sql_str2||v_sql_str8||v_sql_str5||v_sql_str1);
    rr_pkg_proc_log.prc_proc_sql_log (v_proc_log_id, v_sql_str1);
    dynamic_ddl(v_sql_str1);
     
     COMMIT;           
   
   v_status := 'end ';   
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);   
   
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;

 PROCEDURE prc_copy_exfactory_fcst
   IS
/******************************************************************************
   NAME:       PRC_COPY_EXFACTORY_FCST
   PURPOSE:    Procedure to copy exfactory forecast

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada / Red Rock Consulting

******************************************************************************/
   v_prog_name    VARCHAR2 (100) ;
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;
   v_max_sales_date DATE;
   v_ltf_au_retail NUMBER;
   v_ltf_nz_retail NUMBER; 
   v_fore_col VARCHAR2(30);
   v_st_eng_profile NUMBER := 1;
   v_lt_eng_profile NUMBER := 1;
   v_fore_col_lt VARCHAR2(30);
   
   v_sql_str     VARCHAR2(32000);
   
   v_no_weeks  NUMBER := 156;
   
  BEGIN

   pre_logon;
   v_status := 'start ';
   v_prog_name := 'PRC_COPY_EXFACTORY_FCST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   SELECT to_date(pval,'mm-dd-yyyy hh24:mi:ss') INTO v_max_sales_date FROM sys_params 
   WHERE pname LIKE 'max_sales_date';
   
   v_ltf_au_retail := get_ltf_au_retail;
   v_ltf_nz_retail := get_ltf_nz_retail;
   
   SELECT get_fore_col (0, v_st_eng_profile) INTO v_fore_col FROM DUAL;
   SELECT get_fore_col (0, v_lt_eng_profile) INTO v_fore_col_lt FROM DUAL;
   
   v_sql_str := 'MERGE /*+ INDEX(sd1, sales_data_pk) */ INTO sales_data sd1 
   USING(
   SELECT /*+ FULL(sd) PARALLEL(sd,32) */
     sd.item_id,
     sd.location_id,
     sd.sales_date,
     nvl(sd.unexfact_or, case when grp_ch.e1_cust_cat_2 = ''40'' AND reg.l_att_10 = ''01'' AND sd.sales_date >= TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (' ||  v_ltf_au_retail || '*7)
     then nvl(sd.exp_raw_order,(nvl(sd.'||v_fore_col ||'*1,0)))*(1+nvl(sd.ff_1,0))
     when grp_ch.e1_cust_cat_2 = ''40'' AND reg.l_att_10 = ''02'' AND sd.sales_date >= TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (' || v_ltf_nz_retail || '*7)
     then nvl(sd.exp_raw_order,(nvl(sd.'||v_fore_col ||',0)))*(1+nvl(sd.ff_1,0))
     else decode(nvl(sd.demand_type,0),1,
(nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1)))) + nvl(sd.final_pos_fcst,0),
(nvl(sd.manual_stat, nvl(sd.sim_val_182,nvl(sd.'||v_fore_col ||', sd.npi_forecast)))*(1.00+nvl(sd.manual_fact,0)) + nvl(sd.sd_uplift,0))) end) final_exf_fcst
   FROM sales_data sd,
        t_ep_e1_cust_cat_2 grp_ch,
        t_ep_l_att_10 reg,
        location l
   WHERE
        sd.location_id = l.location_id
    AND l.t_ep_l_att_10_ep_id = reg.t_ep_l_att_10_ep_id 
    AND l.t_ep_e1_cust_cat_2_ep_id = grp_ch.t_ep_e1_cust_cat_2_ep_id 
    AND nvl(nvl(case when sd.ff_3 = 0 then null else sd.ff_3 end, nvl(sd.unexfact_or, case when grp_ch.e1_cust_cat_2 = ''40'' AND reg.l_att_10 = ''01'' AND sd.sales_date >= TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (' ||  v_ltf_au_retail || '*7)
     then nvl(sd.exp_raw_order,(nvl(sd.'||v_fore_col ||'*1,0)))*(1+nvl(sd.ff_1,0))
     when grp_ch.e1_cust_cat_2 = ''40'' AND reg.l_att_10 = ''02'' AND sd.sales_date >= TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') + (' || v_ltf_nz_retail || '*7)
     then nvl(sd.exp_raw_order,(nvl(sd.'||v_fore_col ||',0)))*(1+nvl(sd.ff_1,0))
     else decode(nvl(sd.demand_type,0),1,
(nvl(sd.exfact_base,0)*(nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1)))) + nvl(sd.final_pos_fcst,0),
(nvl(sd.manual_stat, nvl(sd.sim_val_182,nvl(sd.'||v_fore_col ||', sd.npi_forecast)))*(1.00+nvl(sd.manual_fact,0)) + nvl(sd.sd_uplift,0))) end)), 0) <> 0
    AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')  AND TO_DATE(''' || TO_CHAR(v_max_sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')  + (' || v_no_weeks || '*7))sd2
   ON(  sd1.item_id = sd2.item_id
    AND sd1.location_id = sd2.location_id
    AND sd1.sales_date = sd2.sales_date)
   WHEN MATCHED THEN UPDATE SET sd1.ff_3 = sd2.final_exf_fcst
   WHERE NVL(sd1.ff_3, 0) <> NVL(sd2.final_exf_fcst, 0)';
   
   --dbms_output.put_line(v_sql_str);
   dynamic_ddl(v_sql_str);
   COMMIT;
   
   v_status := 'end ';   
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
  EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
 END prc_copy_exfactory_fcst;
 
 PROCEDURE prc_set_do_not_forecast
  AS
/******************************************************************************
   NAME:       PRC_SET_DO_NOT_FORECAST
   PURPOSE:    procedure to set do not forecast

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_ids           VARCHAR2(1000);
   v_cust_ids      VARCHAR2(1000);
   
   v_prog_name     VARCHAR2 (100)   := 'PRC_SET_DO_NOT_FORECAST';
   v_status        VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
   sql_str         VARCHAR2(32767);
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_SET_DO_NOT_FORECAST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   v_ids := NULL;
   
   FOR rec IN (SELECT t_ep_i_att_5_ep_id FROM t_ep_i_att_5 WHERE i_att_5 not in ('Z3','N/A'))
   LOOP
   
      v_ids := v_ids || rec.t_ep_i_att_5_ep_id || ',';
   
   END LOOP;
   
   FOR rec IN (SELECT t_ep_L_ATT_8_ep_id FROM t_ep_L_ATT_8 WHERE L_ATT_8 not IN ('XX'))
   LOOP
   
      v_cust_ids  := v_cust_ids || rec.t_ep_l_att_8_ep_id || ',';
   
   END LOOP;
   
   v_ids := RTRIM(v_ids, ',');

   sql_str := 'UPDATE /*+ FULL(mdp) PARALLEL(mdp, 32) */ mdp_matrix mdp
               SET do_fore = CASE WHEN t_ep_i_att_5_ep_id IN (' || v_ids || '-1) THEN 0
                                  WHEN t_ep_L_ATT_8_ep_id IN (' || v_cust_ids || '-1) THEN 0
                                  ELSE do_fore END
               WHERE do_fore <> CASE WHEN t_ep_i_att_5_ep_id IN (' || v_ids || '-1) THEN 0
                                  WHEN t_ep_L_ATT_8_ep_id IN (' || v_cust_ids || '-1) THEN 0
                                  ELSE do_fore END';
  
   dynamic_ddl(sql_str);
   
   COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;

 PROCEDURE prc_update_sim_cols
  AS
/******************************************************************************
   NAME:       PRC_UPDATE_SIM_COLS
   PURPOSE:    procedure to set do not forecast

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_prog_name     VARCHAR2 (100)   := 'PRC_UPDATE_SIM_COLS';
   v_status        VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
   sql_str         VARCHAR2(32767);
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_SIM_COLS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   UPDATE /*+ FULL(sd) PARALLEL(sd, 64) */ sales_data sd
   SET sim_val_182 = NULL 
   WHERE sim_val_182 IS NOT NULL;
   
   COMMIT;

   UPDATE /*+ FULL(pd) PARALLEL(pd, 64) */ promotion_data pd
   SET fore_5_uplift = NULL 
   WHERE fore_5_uplift IS NOT NULL;
   
   COMMIT;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  
 PROCEDURE prc_update_manual_stat
  AS
/******************************************************************************
   NAME:       PRC_UPDATE_MANUAL_STAT
   PURPOSE:    procedure to zero out manual_stat

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  

******************************************************************************/

   v_ids           VARCHAR2(100);
   
   v_prog_name     VARCHAR2 (100)   := 'PRC_UPDATE_MANUAL_STAT';
   v_status        VARCHAR2(100);
   v_proc_log_id   NUMBER;
   max_sales_date   DATE;
   sql_str         VARCHAR2(32767);
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_UPDATE_MANUAL_STAT';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
   
   
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    INTO max_sales_date 
    FROM DUAL;
  
  UPDATE /*+ FULL(pd) PARALLEL(sd, 64) */ sales_data sd
  SET manual_stat = 0
  WHERE nvl(manual_stat, -1) <> 0
  and sales_date between  max_sales_date and max_sales_date + (2 * 52 * 7);
   
   COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
 
END;

/
