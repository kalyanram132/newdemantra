--------------------------------------------------------
--  DDL for Package Body PKG_BUDGET
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_BUDGET" 
AS
/******************************************************************************
   NAME:         PKG_BUDGET  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0      
   
   ******************************************************************************/

  procedure prc_pnl_ss1(p_start_date  DATE,
                        p_end_date    DATE)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PNL_SS1
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   
   v_eng_profile    NUMBER := 1;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_PNL_SS1';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;    
    ---
    
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    into max_sales_date 
    FROM DUAL;
    
   
    
    sql_str := 'UPDATE /*+ FULL(sd) PARALLEL(sd, 24) */ sales_data sd
    SET
    rr_stock_hand	=	demand_type,
    rtl_rsv_l	=	unexfact_or,
    pl_actuals_cbm	=	exfact_base,
    rr_ttl_trad_ls_exmpa	=	rr_invest_or,
    pl_actuals_ttl_trad_per_lsv	=	rr_invest_p,
    rr_activity_discount	=	final_pos_fcst,
    fix_cost_price_dec_42	=	manual_stat,
    mapa_m1_account	=	sim_val_182,
    constrained_sm_gsv	=	'||fore_column||',
    night_const_sm_fcst	=	npi_forecast,
    lock_out_oi	=	manual_fact,
    manual_stat_bk	=	sd_uplift,
    abs_err_app_plan_m1	=	ebspricelist103,
    dummy_npi_forecast	=	ebspricelist104,
    total_stores	=	ebspricelist106,
    distribution_percent	=	ebspricelist107,
    upspw	=	ebspricelist109,
    sf_17	=	ebspricelist110,
    dsv	=	ebspricelist111,
    bf_17	=	ebspricelist112,
    copy_of_sales_fcst	=	ebspricelist113,
    gsv_ytd	=	ebspricelist114,
    lock_fcst	=	ebspricelist115,
    DEMAND_LY2 = ebspricelist116,
    inv_actual	=	ebspricelist117,
    demand_base_copy	=	ebspricelist118,
    LEVEL4_SR_PK	=	ebspricelist121,
    pck_lock	=	ebspricelist122,
    pck_unlock	=	ebspricelist126,
    diff_percentage	=	mkt_acc1,
    diff	=	mkt_acc2,
    order_fcst	=	mkt_acc3,
    cogs_rsp_oncost	=	cust_input1,
    last_year_sales	=	cust_input2,
    plan_vol_c	=	cust_input3,
    case_deal_sd	=	ff_input1,
    case_list_price	=	tiv,
    discount_d	=	si_acc2,
    causal5	=	mkt_input_perc,
    pos_oride	=	mkt_input1,
    sf_4	=	mkt_input2,
    sf_2	=	mkt_input3,
    pos_pseudo	=	fin_ord_plan,
    inv_plan = shelf_price_sd
    WHERE 
    sales_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')  AND
    sales_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND 
    (nvl(	rr_stock_hand	, 0) <> nvl(	demand_type	,0) OR
    nvl(	rtl_rsv_l	, 0) <> nvl(	unexfact_or	,0) OR
    nvl(	pl_actuals_cbm	, 0) <> nvl(	exfact_base	,0) OR
    nvl(	rr_ttl_trad_ls_exmpa	, 0) <> nvl(	rr_invest_or	,0) OR
    nvl(	pl_actuals_ttl_trad_per_lsv	, 0) <> nvl(	rr_invest_p	,0) OR
    nvl(	rr_activity_discount	, 0) <> nvl(	final_pos_fcst	,0) OR
    nvl(	fix_cost_price_dec_42	, 0) <> nvl(	manual_stat	,0) OR
    nvl(	mapa_m1_account	, 0) <> nvl(	sim_val_182	,0) OR
    nvl(	constrained_sm_gsv	, 0) <> nvl(	'||fore_column||'	,0) OR
    nvl(	night_const_sm_fcst	, 0) <> nvl(	npi_forecast	,0) OR
    nvl(	lock_out_oi	, 0) <> nvl(	manual_fact	,0) OR
    nvl(	manual_stat_bk	, 0) <> nvl(	sd_uplift	,0) OR
    nvl(	abs_err_app_plan_m1	, 0) <> nvl(	ebspricelist103	,0) OR
    nvl(	dummy_npi_forecast	, 0) <> nvl(	ebspricelist104	,0) OR
    nvl(	total_stores	, 0) <> nvl(	ebspricelist106	,0) OR
    nvl(	distribution_percent	, 0) <> nvl(	ebspricelist107	,0) OR
    nvl(	upspw	, 0) <> nvl(	ebspricelist109	,0) OR
    nvl(	sf_17	, 0) <> nvl(	ebspricelist110	,0) OR
    nvl(	dsv	, 0) <> nvl(	ebspricelist111	,0) OR
    nvl(	bf_17	, 0) <> nvl(	ebspricelist112	,0) OR
    nvl(	copy_of_sales_fcst	, 0) <> nvl(	ebspricelist113	,0) OR
    nvl(	gsv_ytd	, 0) <> nvl(	ebspricelist114	,0) OR
    nvl(	lock_fcst	, 0) <> nvl(	ebspricelist115	,0) OR
    nvl(	DEMAND_LY2	, 0) <> nvl(	ebspricelist116	,0) OR
    nvl(	inv_actual	, 0) <> nvl(	ebspricelist117	,0) OR
    nvl(	demand_base_copy	, 0) <> nvl(	ebspricelist118	,0) OR
    nvl(	LEVEL4_SR_PK	, 0) <> nvl(	ebspricelist121	,0) OR    
    nvl(	pck_lock	, 0) <> nvl(	ebspricelist122	,0) OR
    nvl(	pck_unlock	, 0) <> nvl(	ebspricelist126	,0) OR
    nvl(	diff_percentage	, 0) <> nvl(	mkt_acc1	,0) OR
    nvl(	diff	, 0) <> nvl(	mkt_acc2	,0) OR
    nvl(	order_fcst	, 0) <> nvl(	mkt_acc3	,0) OR
    nvl(	cogs_rsp_oncost	, 0) <> nvl(	cust_input1	,0) OR
    nvl(	last_year_sales	, 0) <> nvl(	cust_input2	,0) OR
    nvl(	plan_vol_c	, 0) <> nvl(	cust_input3	,0) OR
    nvl(	case_deal_sd	, 0) <> nvl(	ff_input1	,0) OR
    nvl(	case_list_price	, 0) <> nvl(	tiv	,0) OR
    nvl(	discount_d	, 0) <> nvl(	si_acc2	,0) OR
    nvl(	causal5	, 0) <> nvl(	mkt_input_perc	,0) OR
    nvl(	pos_oride	, 0) <> nvl(	mkt_input1	,0) OR
    nvl(	sf_4	, 0) <> nvl(	mkt_input2	,0) OR
    nvl(	sf_2	, 0) <> nvl(	mkt_input3	,0) OR
    nvl(	pos_pseudo	, 0) <> nvl(	fin_ord_plan	,0) OR
    nvl(  inv_plan , 0) <> nvl( shelf_price_sd, 0 ))';

    dynamic_ddl(sql_str);   
    COMMIT;

    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
    END;

  procedure prc_pnl_ss2(p_start_date  DATE,
                        p_end_date    DATE)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PNL_SS2
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   
   v_eng_profile    NUMBER := 1;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_PNL_SS2';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);
    
    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;    
    ---
    
    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss') 
    into max_sales_date 
    FROM DUAL;
    
   
    
    sql_str := 'UPDATE /*+ FULL(sd) PARALLEL(sd, 24) */ sales_data sd
    SET 
    demand_ly2	=	demand_type	,
    manual_fcst	=	unexfact_or	,
    manual_split	=	exfact_base	,
    causal1	=	rr_invest_or	,
    causal2	=	rr_invest_p	,
    causal3	=	final_pos_fcst	,
    causal4	=	manual_stat	,
    e_hol	=	sim_val_182	,
    level4_sr_pk	=	'||fore_column||'	,
    ebspricelist105	=	npi_forecast	,
    ebspricelist0	=	manual_fact	,
    ebs_return_history	=	sd_uplift	,
    week1_forecast	=	ebspricelist103	,
    week2_forecast	=	ebspricelist104	,
    week3_forecast	=	ebspricelist106	,
    week4_forecast	=	ebspricelist107	,
    week5_forecast	=	ebspricelist109	,
    week6_forecast	=	ebspricelist110	,
    week7_forecast	=	ebspricelist111	,
    week8_forecast	=	ebspricelist112	,
    week9_forecast	=	ebspricelist113	,
    week10_forecast	=	ebspricelist114	,
    week11_forecast	=	ebspricelist115	,
    BEG_INV = ebspricelist116
    week12_forecast	=	ebspricelist117	,
    week13_forecast	=	ebspricelist118	,
    CP_DOLLARS	=	ebspricelist121,
    demand_priority	=	ebspricelist122	,
    abs_pct_error	=	ebspricelist126	,
    abs_deviation	=	mkt_acc1	,
    price_for_item	=	mkt_acc2	,
    supplier_plan	=	mkt_acc3	,
    ff_acc_1	=	cust_input1	,
    revenue_k_dol	=	cust_input2	,
    cp_dollars	=	cust_input3	,
    budget_dollars	=	ff_input1	,
    doh	=	tiv	,
    con_plan_1	=	si_acc2	,
    sup_acc_2	=	mkt_input_perc	,
    sup_acc_3	=	mkt_input1	,
    sup_plan_1	=	mkt_input2	,
    sup_plan_2	=	mkt_input3	,
    sup_plan_3	=	fin_ord_plan ,
    cum_diff_as_sp = shelf_price_sd
    WHERE
    sales_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')  AND
    sales_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') AND 
   ( nvl(	demand_ly2	, 0) <> nvl(	demand_type	, 0) OR
    nvl(	manual_fcst	, 0) <> nvl(	unexfact_or	, 0) OR
    nvl(	manual_split	, 0) <> nvl(	exfact_base	, 0) OR
    nvl(	causal1	, 0) <> nvl(	rr_invest_or	, 0) OR
    nvl(	causal2	, 0) <> nvl(	rr_invest_p	, 0) OR
    nvl(	causal3	, 0) <> nvl(	final_pos_fcst	, 0) OR
    nvl(	causal4	, 0) <> nvl(	manual_stat	, 0) OR
    nvl(	e_hol	, 0) <> nvl(	sim_val_182	, 0) OR
    nvl(	level4_sr_pk	, 0) <> nvl('||fore_column||', 0) OR
    nvl(	ebspricelist105	, 0) <> nvl(	npi_forecast	, 0) OR
    nvl(	ebspricelist0	, 0) <> nvl(	manual_fact	, 0) OR
    nvl(	ebs_return_history	, 0) <> nvl(	sd_uplift	, 0) OR
    nvl(	week1_forecast	, 0) <> nvl(	ebspricelist103	, 0) OR
    nvl(	week2_forecast	, 0) <> nvl(	ebspricelist104	, 0) OR
    nvl(	week3_forecast	, 0) <> nvl(	ebspricelist106	, 0) OR
    nvl(	week4_forecast	, 0) <> nvl(	ebspricelist107	, 0) OR
    nvl(	week5_forecast	, 0) <> nvl(	ebspricelist109	, 0) OR
    nvl(	week6_forecast	, 0) <> nvl(	ebspricelist110	, 0) OR
    nvl(	week7_forecast	, 0) <> nvl(	ebspricelist111	, 0) OR
    nvl(	week8_forecast	, 0) <> nvl(	ebspricelist112	, 0) OR
    nvl(	week9_forecast	, 0) <> nvl(	ebspricelist113	, 0) OR
    nvl(	week10_forecast	, 0) <> nvl(	ebspricelist114	, 0) OR
    nvl(	week11_forecast	, 0) <> nvl(	ebspricelist115	, 0) OR
    nvl(	BEG_INV	, 0) <> nvl(	ebspricelist116	, 0) OR
    nvl(	week12_forecast	, 0) <> nvl(	ebspricelist117	, 0) OR
    nvl(	week13_forecast	, 0) <> nvl(	ebspricelist118	, 0) OR
    nvl(	CP_DOLLARS	, 0) <> nvl(	ebspricelist121	, 0) OR
    nvl(	demand_priority	, 0) <> nvl(	ebspricelist122	, 0) OR
    nvl(	abs_pct_error	, 0) <> nvl(	ebspricelist126	, 0) OR
    nvl(	abs_deviation	, 0) <> nvl(	mkt_acc1	, 0) OR
    nvl(	price_for_item	, 0) <> nvl(	mkt_acc2	, 0) OR
    nvl(	supplier_plan	, 0) <> nvl(	mkt_acc3	, 0) OR
    nvl(	ff_acc_1	, 0) <> nvl(	cust_input1	, 0) OR
    nvl(	revenue_k_dol	, 0) <> nvl(	cust_input2	, 0) OR
    nvl(	cp_dollars	, 0) <> nvl(	cust_input3	, 0) OR
    nvl(	budget_dollars	, 0) <> nvl(	ff_input1	, 0) OR
    nvl(	doh	, 0) <> nvl(	tiv	, 0) OR
    nvl(	con_plan_1	, 0) <> nvl(	si_acc2	, 0) OR
    nvl(	sup_acc_2	, 0) <> nvl(	mkt_input_perc	, 0) OR
    nvl(	sup_acc_3	, 0) <> nvl(	mkt_input1	, 0) OR
    nvl(	sup_plan_1	, 0) <> nvl(	mkt_input2	, 0) OR
    nvl(	sup_plan_2	, 0) <> nvl(	mkt_input3	, 0) OR
    nvl(	sup_plan_3	, 0) <> nvl(	fin_ord_plan	, 0) OR
    nvl(  cum_diff_as_sp , 0) <> nvl( shelf_price_sd, 0 ))';

    dynamic_ddl(sql_str);   
    COMMIT;

    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         end;
    END;
     
 procedure prc_budget_ss_monthly
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PNL_SS1
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   V_WEEKS          number := 6;
   V_START_DATE date;
   v_end_date date;
   v_eng_profile    NUMBER := 1;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_BUDGET_SS_MONTHLY';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || sysdate);
    
    V_START_DATE := TO_DATE(TRUNC(sysdate,'YY'));
    v_end_date := to_date(trunc(sysdate+365,'YY'))-1;
    
    PKG_BUDGET.PRC_PNL_SS1(V_START_DATE,V_END_DATE);
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         end;
    end;

 procedure prc_budget_ss_qtrly
  IS
/*****************************************************************************************************************************
   NAME:       PRC_PNL_SS1
   PURPOSE:    Calculates latest accrual 
               
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------   
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting
   
*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;   
   sql_str          VARCHAR2 (32000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   V_WEEKS          number := 6;
   V_START_DATE date;
   v_end_date date;
   v_eng_profile    NUMBER := 1;
   
   ---
   BEGIN
    
    pre_logon;
    
    v_status := 'Start ';
    v_prog_name := 'PRC_BUDGET_SS_QTRLY';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || sysdate);
    
    V_START_DATE := TO_DATE(TRUNC(sysdate,'YY'));
    v_end_date := to_date(trunc(sysdate+365,'YY'))-1;
    
    PKG_BUDGET.PRC_PNL_SS2(V_START_DATE,V_END_DATE);
    
    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
    
    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         end;
    END;    
end;

/
