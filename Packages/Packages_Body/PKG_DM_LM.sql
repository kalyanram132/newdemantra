--------------------------------------------------------
--  DDL for Package Body PKG_DM_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_DM_LM" 
AS
/******************************************************************************
   NAME:         PKG_DM  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version
   
   ******************************************************************************/

  PROCEDURE prc_create_combinations(user_id    NUMBER,
                                    level_id   NUMBER,
                                    member_id  NUMBER)
  IS
/******************************************************************************
   NAME:       PRC_CREATE_COMBINATIONS
   PURPOSE:    Procedure to check promotion conflicts

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Bhaskar Rampalli / Red Rock Consulting

******************************************************************************/

   maxdate        DATE;  

   v_prog_name    VARCHAR2 (100) := 'PRC_CREATE_COMBINATIONS';
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;  
   
   v_day          VARCHAR2(10);
   
   v_product_code NUMBER;
   v_customer_no  NUMBER; 
   v_channel      NUMBER; 
   v_customer     NUMBER;
   v_item_id      NUMBER;
   v_region       NUMBER;
      
  BEGIN

   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_CREATE_COMBINATIONS';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   
   maxdate := NEXT_DAY(to_char(TRUNC(SYSDATE, 'DD'), 'mm/dd/yyyy'), 'MONDAY');
   
   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';
   
      --Retrieve Level Method Values
   SELECT rr_product_code, rr_customer_no, rr_channel, rr_customer, rr_region
     INTO v_product_code, v_customer_no, v_channel, v_customer, v_region
     FROM t_ep_item
    WHERE t_ep_item_ep_id = member_id;

   --Retrieve Item_id for Product Code
   SELECT MAX (item_id)
     INTO v_item_id
     FROM items
    WHERE t_ep_item_ep_id = v_product_code;

   -- If lowest level Selected, only create 1 sales record
   
   dynamic_ddl('TRUNCATE TABLE mdp_load_assist');
   
   IF v_product_code IS NOT NULL AND v_customer_no IS NOT NULL
   THEN 
   
       INSERT INTO mdp_load_assist(item_id, location_id)
       SELECT DISTINCT v_item_id item_id, l.location_id                             
                           FROM location l
                          WHERE 1 = 1
                            AND l.t_ep_site_ep_id = NVL(v_customer_no, l.t_ep_site_ep_id)
                            AND l.t_ep_l_att_10_ep_id = v_region;
       
       COMMIT;
   
   ELSE        
       
       INSERT INTO mdp_load_assist(item_id, location_id)
       SELECT DISTINCT v_item_id item_id, l.location_id                             
                           FROM location l, sales_data s
                          WHERE s.location_id = l.location_id
                            AND l.t_ep_site_ep_id = NVL(v_customer_no, l.t_ep_site_ep_id)
                            AND l.t_ep_e1_cust_cat_2_ep_id = NVL(v_channel, l.t_ep_e1_cust_cat_2_ep_id)
                            AND l.t_ep_ebs_customer_ep_id = NVL(v_customer, l.t_ep_ebs_customer_ep_id)
                            AND l.t_ep_l_att_10_ep_id = v_region
                            AND s.sales_date > ADD_MONTHS (SYSDATE, -6)
                            AND s.sales_date <= TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                            AND (   s.actual_quantity IS NOT NULL
                                 OR s.sdata5 IS NOT NULL
                                 OR s.sdata6 IS NOT NULL
                                );
       
       COMMIT;
       
       END IF;
       
       pkg_common_utils.prc_mdp_add('mdp_load_assist', 
                                    TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS') - (26 *7), 
                                    TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS'));
       
   IF v_product_code IS NOT NULL AND v_customer_no IS NOT NULL
   THEN 
   
       MERGE INTO sales_data s
         USING (SELECT DISTINCT v_item_id item_id, l.location_id  , i.datet sales_date, 0 actual_quantity                          
                           FROM location l, inputs i
                          WHERE 1 = 1
                            AND l.t_ep_site_ep_id = NVL(v_customer_no, l.t_ep_site_ep_id)
                            AND l.t_ep_l_att_10_ep_id = v_region
                            AND i.datet > ADD_MONTHS (SYSDATE, -6)
                            AND i.datet <= TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')) s1
         ON (    s.location_id = s1.location_id
             AND s.item_id = s1.item_id
             AND s.sales_date = s1.sales_date)
         WHEN NOT MATCHED THEN
            INSERT (s.item_id, s.location_id, s.sales_date, s.actual_quantity,
                    s.last_update_date)
            VALUES (s1.item_id, s1.location_id, s1.sales_date,
                    s1.actual_quantity, SYSDATE);

      COMMIT;   
   
   ELSE       
   
       MERGE INTO sales_data s
         USING (SELECT DISTINCT v_item_id item_id, l.location_id,
                                (NEXT_DAY (TRUNC(SYSDATE, 'DD'), v_day) - 7
                                ) sales_date, 0 actual_quantity,
                                SYSDATE last_update_date
                           FROM LOCATION l, sales_data s
                          WHERE s.location_id = l.location_id
                            AND l.t_ep_site_ep_id = NVL(v_customer_no, l.t_ep_site_ep_id)
                            AND l.t_ep_e1_cust_cat_2_ep_id = NVL(v_channel, l.t_ep_e1_cust_cat_2_ep_id)
                            AND l.t_ep_ebs_customer_ep_id = NVL(v_customer, l.t_ep_ebs_customer_ep_id)
                            AND l.t_ep_l_att_10_ep_id = v_region                            
                            AND s.sales_date > ADD_MONTHS (SYSDATE, -6)
                            AND s.sales_date <= TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')
                            AND (   s.actual_quantity IS NOT NULL
                                 OR s.sdata5 IS NOT NULL
                                 OR s.sdata6 IS NOT NULL
                                )) s1
         ON (    s.location_id = s1.location_id
             AND s.item_id = s1.item_id
             AND s.sales_date = s1.sales_date)
         WHEN NOT MATCHED THEN
            INSERT (s.item_id, s.location_id, s.sales_date, s.actual_quantity,
                    s.last_update_date)
            VALUES (s1.item_id, s1.location_id, s1.sales_date,
                    s1.actual_quantity, s1.last_update_date);

      COMMIT;
      
      END IF;


   --Reset Attributes back to NULL
   UPDATE t_ep_item
      SET rr_product_code = NULL,
          rr_customer_no = NULL,
          rr_channel = NULL,
          rr_customer = NULL,
          rr_region = NULL,
          last_update_date = SYSDATE
    WHERE t_ep_item_ep_id = member_id;

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
  PROCEDURE prc_inv_adjust(user_id    NUMBER,
                           level_id   NUMBER,
                           member_id  NUMBER)
    AS
/******************************************************************************
   NAME:       PRC_INV_ADJUST
   PURPOSE:    procedure to populate inventory adjustment

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/

   v_day            VARCHAR2(10);
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_INV_ADJUST';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_INV_ADJUST';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';
   
   dynamic_ddl('TRUNCATE TABLE tbl_promo_dates');
   dynamic_ddl('TRUNCATE TABLE tbl_sd_inv');
   dynamic_ddl('TRUNCATE TABLE tbl_inv_adjust');
   
   INSERT /*APPEND NOLOGGING */ INTO tbl_inv_adjust(item_id, location_id)
   SELECT item_id, location_id
   FROM promotion_matrix 
   WHERE promotion_id = member_id;

   INSERT /*APPEND NOLOGGING */ INTO tbl_promo_dates(promotion_id, sales_date, inv_adj, inv_adj_flag)
   SELECT p.promotion_id,
   i.datet sales_date,
   CASE WHEN i.datet = (NEXT_DAY(pdt.from_date, v_day) -7) AND i.datet = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100
     WHEN i.datet = (NEXT_DAY(pdt.from_date, v_day) -7) THEN NVL(p.start_inv_adj, 100)/100
     WHEN i.datet > (NEXT_DAY(pdt.from_date, v_day) - 7) AND i.datet < (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1
     WHEN i.datet = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.end_inv_adj, 100)/100
     ELSE CASE WHEN (CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                    - CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1 ELSE 2 END
                    - ((i.datet - (NEXT_DAY(pdt.until_date, v_day) - 7))/ 7) >= 0 THEN 0
               ELSE  1 - ((CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                          - TRUNC( (CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END)))
               END
   END inv_adj,
   p.inv_adj_flag
   FROM promotion_dates pdt,
   inputs i, 
   promotion p
   WHERE p.promotion_id = pdt.promotion_id
   AND NVL(p.inv_adj_flag, 0) <> 0
   AND EXISTS(SELECT 1 FROM promotion_matrix pm, tbl_inv_adjust ia 
              WHERE pm.promotion_id = p.promotion_id 
              AND pm.item_id = ia.item_id
              AND pm.location_id = ia.location_id)
   AND pdt.until_date > TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')
   AND i.datet BETWEEN NEXT_DAY(pdt.from_date, v_day) - 7 AND (NEXT_DAY(pdt.until_date, v_day) - 7) + ((CEIL(CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN NVL(p.start_inv_adj, 100)/100 ELSE (NVL(p.end_inv_adj, 100)/100) + (NVL(p.start_inv_adj, 100)/100) END) 
                                                                          - CASE WHEN (NEXT_DAY(pdt.from_date, v_day) -7) = (NEXT_DAY(pdt.until_date, v_day) - 7) THEN 1 ELSE 2 END) * 7);

   COMMIT; 

   INSERT /* APPEND NOLOGGING */ INTO tbl_sd_inv(item_id, location_id, sales_date, inv_adj)
   SELECT pm.item_id, pm.location_id, CASE WHEN p.inv_adj_flag = 1 THEN p.sales_date
                                           WHEN p.inv_adj_flag = 2 THEN p.sales_date - (NVL(mdp.scan_forecast_offset, 0) * 7)
                                           ELSE p.sales_date END sales_date, 
   MAX(p.inv_adj) inv_adj 
   FROM promotion_matrix pm, 
   tbl_promo_dates p,
   mdp_matrix mdp
   WHERE p.promotion_id = pm.promotion_id
   AND mdp.item_id = pm.item_id
   AND mdp.location_id = pm.location_id
   GROUP BY pm.item_id, pm.location_id, CASE WHEN p.inv_adj_flag = 1 THEN p.sales_date
                                           WHEN p.inv_adj_flag = 2 THEN p.sales_date - (NVL(mdp.scan_forecast_offset, 0) * 7)
                                           ELSE p.sales_date END;
  
   COMMIT;
  
   MERGE INTO sales_data sd
   USING(SELECT sd.item_id, sd.location_id, sd.sales_date, NVL(sd_inv.inv_adj, 1) inv_adj
        FROM sales_data sd,
        tbl_sd_inv sd_inv,
        tbl_inv_adjust ia
        WHERE sd.sales_date = sd_inv.sales_date (+)
        AND sd.item_id = sd_inv.item_id (+) 
        AND sd.location_id = sd_inv.location_id (+)
        AND sd.item_id = ia.item_id
        AND sd.location_id = ia.location_id
        AND sd.sales_date > TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS')) sd1
   ON(sd.item_id = sd1.item_id
   AND sd.location_id = sd1.location_id
   AND sd.sales_date = sd1.sales_date)
   WHEN MATCHED THEN 
   UPDATE SET sd.rr_invest_p = sd1.inv_adj,
              sd.last_update_date = sysdate
   WHERE NVL(sd.rr_invest_p, 1) <> sd1.inv_adj;
  
   COMMIT;
   
   MERGE INTO mdp_matrix mdp
   USING (SELECT item_id, location_id FROM tbl_inv_adjust ia) mdp1
   ON(mdp.item_id = mdp1.item_id
   AND mdp.location_id = mdp1.location_id)
   WHEN MATCHED THEN 
   UPDATE SET sales_data_lud = sysdate, last_update_date = sysdate;
   
   COMMIT;
         
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;

PROCEDURE       "PRC_SYNC_SCAN_OFFSET" 
(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL
)
IS
/**************************************************************************
   NAME:       RR_SYNC_SCAN_OFFSET
   PURPOSE:    Syncronises AC Neilson Scan to validate Phasing Rules

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  -------------------------------------------------
   1.0        15/04/2013  Luke Pocock - Inital
  
**************************************************************************/
 
   max_date         VARCHAR2 (100);
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2 (100);
   v_prog_name      VARCHAR2 (100)   := 'JS_SYNC_SCAN_OFFSET';
   v_step           VARCHAR2 (100);
   v_table          VARCHAR2 (100);
   v_table_id       VARCHAR2 (100);  
   v_min            DATE;
   v_max            DATE;
BEGIN
  
   v_step := 'start ';
   dbex (v_step || SYSDATE, v_prog_name);
   pre_logon;
   
    v_min :=  ADD_MONTHS (NEXT_DAY (TO_CHAR (SYSDATE, 'mm/dd/yyyy'), 'monday'),
                     -24);
    v_max := sysdate;
   
   SELECT gtable, g.id_field
   INTO v_table, v_table_id
   FROM group_tables g 
   WHERE group_table_id = level_id;
   

  sql_str :=
      ( 'UPDATE sales_data s SET s.actuals_ttl_offset = null 
  WHERE s.actuals_ttl_offset is not null
  AND s.sales_date between ''' || v_min ||'''  and ''' || v_max || '''
  AND (location_id, item_id) in (
  select location_id, item_id
  from mdp_matrix
  where ' || v_table_id || ' = ' || member_id || '
   and dem_stream = 1
  )')
  ;
     dynamic_ddl (sql_str);
   commit;
 
  IF get_is_table_exists ( 'RR_SCAN_OFF_SYNC_' || user_id) = 1
   THEN
      act_dinamic_sql_0 ('DROP TABLE RR_SCAN_OFF_SYNC_' || user_id);
   END IF;
 
 sql_str :=
      ('CREATE TABLE /*+ nologging */ RR_SCAN_OFF_SYNC_' || user_id || '
        (item_id,location_id, plan_date, scan_ttl_wk0, scan_ttl_wk1, scan_ttl_wk2, scan_ttl_wk3, scan_ttl_wk4, scan_ttl_wk5, scan_ttl_wk6
                     )
        AS SELECT /*+ full(f) parallel(f,24) */ s.item_id,s.location_id, s.sales_date,
        ((sdata5 + sdata6) * nvl(allocation_wk0,1)) scan_ttl_wk0,
        ((sdata5 + sdata6) * allocation_wk1) scan_ttl_wk1,
        ((sdata5 + sdata6) * allocation_wk2) scan_ttl_wk2,
        ((sdata5 + sdata6) * allocation_wk3) scan_ttl_wk3,
        ((sdata5 + sdata6) * allocation_wk4) scan_ttl_wk4,
        ((sdata5 + sdata6) * allocation_wk5) scan_ttl_wk5,
        ((sdata5 + sdata6) * allocation_wk6) scan_ttl_wk6
        FROM sales_data s, mdp_matrix m
         WHERE s.location_id = m.location_id
         AND s.item_id = m.item_id
         AND m.dem_stream = 1
         AND s.sales_date between ''' || v_min ||'''  and ''' || v_max || '''
         AND m.' || v_table_id || ' = ' || member_id || ''
        );
   dynamic_ddl (sql_str);
   commit;

  IF get_is_table_exists ( 'RR_SCAN_OFF_TTL_' || user_id) = 1
   THEN
      act_dinamic_sql_0 ('DROP TABLE RR_SCAN_OFF_TTL_' || user_id);
   END IF;

      sql_str :=
         ('CREATE TABLE  /*+ nologging  */ RR_SCAN_OFF_TTL_' || user_id || '
                   (ITEM_ID          NUMBER(10)                      NOT NULL,
                    LOCATION_ID      NUMBER(10)                      NOT NULL,
                    PLAN_DATE        VARCHAR2(10 BYTE),
                    SCAN_OFFSET_TTL  NUMBER(20,2))
                    TABLESPACE TS_SALES_DATA
                    NOLOGGING
                    CACHE
                    INITRANS 20');
      dynamic_ddl (sql_str);
      COMMIT;
      
   sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk0)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
      sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 7) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk1)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 7) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
         sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 14) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk2)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 14) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
         sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 21) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk3)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 21) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
          sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 28) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk4)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 28) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
          sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 35) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk5)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 35) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
          sql_str :=
   (' INSERT       /*+ nologging append */ INTO RR_SCAN_OFF_TTL_' || user_id || '
      SELECT /*+ full(s) parallel(s,2) */  item_id, location_id,
               TO_CHAR (TO_DATE (plan_date - 42) , ''MM/DD/YYYY''),
               SUM (scan_ttl_wk6)
          FROM RR_SCAN_OFF_SYNC_' || user_id || ' s
      GROUP BY item_id,
               location_id,
               TO_CHAR (TO_DATE (plan_date - 42) , ''MM/DD/YYYY'')');
   dynamic_ddl (sql_str);
   COMMIT;
   
       sql_str :=
   ('MERGE INTO sales_data sd using (
   select ITEM_ID, LOCATION_ID, PLAN_DATE, sum(SCAN_OFFSET_TTL) SCAN_OFFSET_TTL
   from  RR_SCAN_OFF_TTL_' || user_id || '
   GROUP BY ITEM_ID, LOCATION_ID, PLAN_DATE
   )s
   on (sd.item_id = s.item_id
   and sd.location_id = s.location_id
   and sd.sales_date = s.plan_date)
   when matched then update set
   sd.actuals_ttl_offset = s.SCAN_OFFSET_TTL,
   sd.last_update_date = sysdate');
   dynamic_ddl (sql_str);
   COMMIT;
   
     sql_str :=
   ('UPDATE mdp_matrix set last_update_date = sysdate
   where ' || v_table_id || ' = ' || member_id || '
   and dem_stream = 1');
   dynamic_ddl (sql_str);
   COMMIT;

   v_step := 'end ';
   dbex (v_step || SYSDATE, v_prog_name);
 EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         dbex (   'Fatal Error in Step: '
               || v_step
               || '. SQLCODE = '
               || SQLCODE
               || ' '
               || SQLERRM,
               v_prog_name
              );
         RAISE;
      END; 
END; 
 
   PROCEDURE prc_pop_exfact_base(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL)
  AS
/******************************************************************************
   NAME:       PRC_POP_EXFACT_BASE
   PURPOSE:    Temp procedure to ensure VOL_BASE_TTL is updated in PD

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        18/08/2007  Sammy Kolt / Demantra US

******************************************************************************/
   start_date    DATE;
   end_date      DATE;
   counter       NUMBER           := 1;
   sql_str       VARCHAR2 (20000);
   fore_column   VARCHAR2 (200);
   
   
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;
   v_day            VARCHAR2(10);
   
   v_eng_profile    NUMBER := 1;
   
   v_prog_name   VARCHAR2 (100)   := 'PRC_POP_EXFACT_BASE';
   v_status         VARCHAR2(100);
   v_proc_log_id   NUMBER;
   p_start_date    DATE := NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7;
   p_end_date      DATE := NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') + (104 * 7);
   
   v_id_field     VARCHAR2(250);
   
  BEGIN
  
   pre_logon;
   v_status := 'Start ';
   v_prog_name := 'PRC_POP_EXFACT_BASE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';    

   SELECT get_fore_col (0, v_eng_profile)
   INTO fore_column
   FROM DUAL;

   start_date := p_start_date;
   end_date := p_end_date;
   
   SELECT LOWER(id_field) INTO v_id_field
   FROM group_tables 
   WHERE group_table_id = level_id;
   
   v_id_field := v_id_field || ' = ' || member_id;  
   
   
   sql_str := 'MERGE /*+ INDEX(sd, sales_data_pk) */ INTO sales_data sd
      USING (SELECT /*+ FULL(sd) PARALLEL(sd, 8) */
             sd.item_id, 
             sd.location_id, 
             sd.sales_date, 
             sd.sales_date  - (NVL(mdp.scan_forecast_offset, 0) * 7) plan_date,
             DECODE(mdp.dem_stream, 0, 0, NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, NVL(sd.npi_forecast, 0)))) *(1.00 + NVL(sd.manual_fact,0))) exfact_base, 
             DECODE(mdp.dem_stream, 0, 0, NVL(sd.' || fore_column || '*1, 0)) engine_base,
             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price,
             mdp.dem_stream demand_type
             FROM sales_data sd, 
             mdp_matrix mdp
             WHERE sd.item_id = mdp.item_id
             AND sd.location_id = mdp.location_id 
             --AND mdp.dem_stream = 1 
             AND mdp.' || v_id_field || '
             AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') 
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'')
             ) sd1
      ON (sd1.item_id = sd.item_id
          AND sd1.location_id = sd.location_id
          AND sd1.plan_date = sd.sales_date
          AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(p_start_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') - (6 * 7)
                               AND TO_DATE(''' || TO_CHAR(p_end_date, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') + (6 * 7))
      WHEN MATCHED THEN
         UPDATE
         SET exfact_base = sd1.exfact_base,
             engine_base = sd1.engine_base,
             demand_type = sd1.demand_type,
             last_update_date = SYSDATE
         WHERE NVL(sd.exfact_base, 0) <> NVL(sd1.exfact_base, 0)
         OR NVL(sd.engine_base, 0) <>  NVL(sd1.engine_base, 0)        
         OR NVL(sd.demand_type, 0) <>  NVL(sd1.demand_type, 0) 
      WHEN NOT MATCHED THEN
         INSERT(item_id, location_id, sales_date, exfact_base, engine_base, demand_type, last_update_date)
         VALUES(sd1.item_id, sd1.location_id, sd1.plan_date, sd1.exfact_base, sd1.engine_base, sd1.demand_type, SYSDATE)
         WHERE ABS(NVL(sd1.exfact_base, 0)) + ABS(NVL(sd1.engine_base, 0))  <> 0';
         
   dynamic_ddl(sql_str);
         
   COMMIT;
   
   sql_str := 'UPDATE mdp_matrix 
               SET sales_data_lud = SYSDATE,
               last_update_date = SYSDATE
               WHERE 1 = 1
               AND dem_stream = 1
               AND ' || v_id_field;
         
   dynamic_ddl(sql_str);
         
   COMMIT;
   
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, user_id, level_id, member_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, 
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace    
                          );
            RAISE;
         END;
  END;
  
END;

/
