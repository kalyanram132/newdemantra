--------------------------------------------------------
--  DDL for Package Body PKG_COMMON_UTILS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_COMMON_UTILS" 
AS
/******************************************************************************
   NAME:         RR_PKG_COMMON_UTILS BODY
   PURPOSE:      All procedures commanly used across modules
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

   ******************************************************************************/
   v_prog_name    VARCHAR2 (100);
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

   PROCEDURE prc_lowest_lev_integration (is_imp_table VARCHAR2,
                                         ins_or_upd   NUMBER DEFAULT 0)
   IS
/**************************************************************************

   NAME:       prc_lowest_lev_integration
   PURPOSE:    Lowest level integration interface code for pl/sql load.

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  -------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

**************************************************************************/
      vi_query_id            INTEGER;
      vs_query_name          VARCHAR2 (100);
      vs_sqlstr              VARCHAR2 (32000);
      vi_exists              INTEGER;
      vs_ins_sd_flds         VARCHAR2 (2000);
      vs_src_sd_flds         VARCHAR2 (2000);
      vs_upd_sd_flds         VARCHAR2 (2000);
      vs_src_sel_flds        VARCHAR2 (2000);
      vs_ins_con_flds        VARCHAR2 (2000);
      vs_upd_con_flds        VARCHAR2 (2000);
      vs_mdp_flds            VARCHAR2 (2000);
      vs_src_mdp_flds        VARCHAR2 (2000);
      vs_proc_name           VARCHAR2 (30);
      vi_lowest_lev_query    INTEGER;
      vs_from                VARCHAR2 (2000);
      vs_where               VARCHAR2 (2000);
      vs_where_combs         VARCHAR2 (2000);
      vi_presentation_type   NUMBER (1);
      vi_counter             NUMBER (1)       := 0;
      vs_levels              VARCHAR2 (200);
      vi_tg_res_id           INTEGER;
      vs_time_res            VARCHAR2 (200);
      v_count                NUMBER;
      vi_ins_new_combs       NUMBER;
      v_day                  VARCHAR2(10);
      v_low_tim              NUMBER;
      v_date_exp             VARCHAR2(100);

   BEGIN
      pre_logon;
      v_status := 'start ';
      v_prog_name := 'PRC_LOWEST_LEV_INTEGRATION';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      SELECT DECODE(LOWER(pval), 'day', 1, 'week', 2, 'month', 3, 1)
      INTO v_low_tim
      FROM sys_params
      WHERE pname = 'Timeresolution';

      IF  v_low_tim = 1 THEN
          v_date_exp := 'sdate sdate_ld';
      ELSIF v_low_tim = 2 THEN
          SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
          INTO v_day
          FROM sys_params
          WHERE pname = 'FIRSTDAYINWEEK';

          v_date_exp := 'NEXT_DAY(sdate, ''' || v_day || ''') - 7 sdate_ld';
       ELSIF v_low_tim = 3 THEN
          v_date_exp := 'TRUNC(sdate, ''MM'') sdate_ld';
       END IF;

      BEGIN
         SELECT query_name, presentation_type, insertnewcombinations
           INTO vs_query_name, vi_presentation_type, vi_ins_new_combs
           FROM transfer_query
          WHERE UPPER (table_name) = UPPER (is_imp_table);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            raise_application_error
                 (-20001,
                     'There is no integration profile with import table = '''
                  || is_imp_table
                  || ''''
                 );
      END;

      BEGIN
         SELECT query_id
           INTO vi_query_id
           FROM queries
          WHERE query_name = vs_query_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            raise_application_error (-20000, 'This query doesn''t exist');
      END;

      SELECT DECODE (COUNT (*), 0, 1, 0)
        INTO vi_lowest_lev_query
        FROM queries q, GROUPS g, group_tables gt
       WHERE q.query_id = vi_query_id
         AND q.query_id = g.query_id
         AND g.group_table_id = gt.group_table_id
         AND LOWER (search_table) IN ('items', 'location', 'mdp_matrix')
         AND father_id <> 0;

      IF vi_lowest_lev_query = 0
      THEN
         raise_application_error (-20002,
                                  'The query is not on the lowest level'
                                 );
      END IF;

      SELECT tg_res_id
        INTO vi_tg_res_id
        FROM queries
       WHERE query_id = vi_query_id;

      get_param ('sys_params', 'Timeresolution', vs_time_res);

      SELECT COUNT (*)
        INTO vi_exists
        FROM tgroup_res
       WHERE tg_res_id = vi_tg_res_id AND LOWER (tg_res) = LOWER (vs_time_res);

      IF vi_exists = 0
      THEN
         raise_application_error (-20002, 'No time aggregation allowed');
      END IF;

      FOR rec IN (SELECT   gtable,
                           DECODE (vi_presentation_type,
                                   1, cust_code_field,
                                   0, data_field
                                  ) fld,
                           search_table
                      FROM queries q, GROUPS g, group_tables gt
                     WHERE q.query_id = vi_query_id
                       AND q.query_id = g.query_id
                       AND g.group_table_id = gt.group_table_id
                       AND LOWER (search_table) IN ('items', 'location')
                       AND father_id = 0
                  ORDER BY gorder)
      LOOP
         vi_counter := vi_counter + 1;
         vs_from := vs_from || ',' || rec.gtable;
         vs_where :=
               vs_where
            || '
        AND m.'
            || rec.gtable
            || '_ep_id = '
            || rec.gtable
            || '.'
            || rec.gtable
            || '_ep_id
        AND UPPER('
            || rec.gtable
            || '.'
            || rec.fld
            || ') = UPPER(imp.level'
            || vi_counter
            || ')';
         vs_where_combs :=
               vs_where_combs
            || '
        AND '
            || CASE
                  WHEN rec.search_table = 'items'
                     THEN 'i'
                  WHEN rec.search_table = 'location'
                     THEN 'l'
               END
            || '.'
            || rec.gtable
            || '_ep_id = '
            || rec.gtable
            || '.'
            || rec.gtable
            || '_ep_id
        AND UPPER('
            || rec.gtable
            || '.'
            || rec.fld
            || ') = UPPER(imp.level'
            || vi_counter
            || ')';
         vs_levels := vs_levels || ',level' || vi_counter;
      END LOOP;

      vs_levels := LTRIM (vs_levels, ',');

      IF(vi_ins_new_combs = 1) THEN

          dynamic_ddl ('truncate table mdp_load_assist');
          dynamic_ddl
          ( 'INSERT /*+ APPEND NOLOGGING */ INTO mdp_load_assist(item_id, location_id, is_fictive, boolean_qty)
            (SELECT /*+ FULL(imp) PARALLEL(imp, 64) */ i.item_id, l.location_id, 0, 0
            FROM '
          || is_imp_table
          || ' imp, items i, location l'
          || vs_from
          || '
            WHERE 1 = 1
            '
          || vs_where_combs
          || '
            AND NOT EXISTS(SELECT 1 FROM mdp_matrix m WHERE m.item_id = i.item_id AND m.location_id = l.location_id)
            )'
           );

           dbms_output.put_line( 'INSERT /*+ APPEND NOLOGGING */ INTO mdp_load_assist(item_id, location_id, is_fictive, boolean_qty)
            (SELECT /*+ FULL(imp) PARALLEL(imp, 64) */ i.item_id, l.location_id, 0, 0
            FROM '
          || is_imp_table
          || ' imp, items i, location l'
          || vs_from
          || '
            WHERE 1 = 1
            '
          || vs_where_combs
          || '
            AND NOT EXISTS(SELECT 1 FROM mdp_matrix m WHERE m.item_id = i.item_id AND m.location_id = l.location_id)
            )');

          COMMIT;

          SELECT COUNT (1)
          INTO v_count
          FROM mdp_load_assist;

          dbms_output.put_line('Count is ' || v_count);

          IF v_count > 0
          THEN
            pkg_common_utils.prc_mdp_add ('mdp_load_assist');
            --dynamic_ddl ('truncate table mdp_load_assist');
          END IF;

      END IF;

      check_and_drop (is_imp_table || '_tmp');
      /*dbms_output.put_line('CREATE TABLE '
          || is_imp_table
          || '_tmp AS
      SELECT /*+ FULL(imp) PARALLEL(imp, 16) m.item_id,m.location_id,imp.*
      FROM '
          || is_imp_table
          || ' imp,mdp_matrix m'
          || vs_from
          || '
      WHERE 1 = 1 '
          || vs_where); */

      dbms_output.put_line('CREATE TABLE '
          || is_imp_table
          || '_tmp AS
      SELECT /*+ FULL(imp) PARALLEL(imp, 64) INDEX(m, mdp_pk_2nd) */ m.item_id,m.location_id,imp.*, /*COUNT(1) OVER (PARTITION BY m.item_id, m.location_id, imp.sdate)*/ 1 rec_cnt, ' || v_date_exp || '
      FROM '
          || is_imp_table
          || ' imp,mdp_matrix m'
          || vs_from
          || '
      WHERE 1 = 1 '
          || vs_where);

      dynamic_ddl
         (   'CREATE TABLE '
          || is_imp_table
          || '_tmp AS
      SELECT /*+ FULL(imp) PARALLEL(imp, 64) INDEX(m, mdp_pk_2nd) */ m.item_id,m.location_id,imp.*, /* COUNT(1) OVER (PARTITION BY m.item_id, m.location_id, imp.sdate)*/ 1 rec_cnt, ' || v_date_exp || '
      FROM '
          || is_imp_table
          || ' imp,mdp_matrix m'
          || vs_from
          || '
      WHERE 1 = 1 '
          || vs_where
         );
      dynamic_ddl (   'CREATE INDEX '
                   || is_imp_table
                   || '_xxx ON
      '
                   || is_imp_table
                   || '_tmp (item_id,location_id,sdate)'
                  );
      dynamic_ddl (   'CREATE INDEX '
                   || is_imp_table
                   || '_xxy ON
      '
                   || is_imp_table
                   || '_tmp (item_id,location_id)'
                  );
      dynamic_ddl (   'CREATE INDEX '
                   || is_imp_table
                   || '_xyz ON
      '
                   || is_imp_table
                   || '_tmp (level1, level2)'
                  );
      vs_proc_name :=
               'dyn_integ_' || TO_CHAR (SYSTIMESTAMP, 'mm_dd_yyyy_hh24_mi_ss');

      FOR rec IN (SELECT   dbname, data_table_name, utc.column_name,
                           int_aggr_func ifunc
                      FROM active_series acs,
                           computed_fields cf,
                           user_tab_columns utc
                     WHERE acs.forecast_type_id = cf.forecast_type_id
                       AND acs.query_id = vi_query_id
                       AND utc.table_name = UPPER (is_imp_table)
                       AND utc.column_name NOT LIKE 'LEVEL%'
                       AND utc.column_name <> 'SDATE'
                       AND utc.column_name = UPPER (computed_name)
                  ORDER BY serias_id)
      LOOP
         IF LOWER (rec.data_table_name) = 'branch_data'
         THEN
            vs_src_sd_flds := vs_src_sd_flds || ', s1.' || rec.column_name;
            vs_src_sel_flds := vs_src_sel_flds || ',' || rec.ifunc || '(i.' || rec.column_name || ') ' || rec.column_name ;
            vs_upd_sd_flds := vs_upd_sd_flds || rec.dbname || ' = s1.' || rec.column_name || ',';
            vs_ins_sd_flds := vs_ins_sd_flds || ',' || rec.dbname ;
            vs_upd_con_flds := vs_upd_con_flds || ' OR NVL(s1.' || rec.column_name || ', 0) <> NVL(s.' || rec.dbname || ', 0)';
            vs_ins_con_flds := vs_ins_con_flds || ' OR NVL(s1.' || rec.column_name || ', 0) <> 0';
         END IF;

         IF LOWER (rec.data_table_name) = 'mdp_matrix'
         THEN
            vs_mdp_flds := vs_mdp_flds || ',' || rec.dbname;
            vs_src_mdp_flds := vs_src_mdp_flds || ',' || rec.column_name;
         END IF;
      END LOOP;

      vs_mdp_flds := LTRIM (vs_mdp_flds, ',');
      vs_upd_sd_flds := RTRIM (vs_upd_sd_flds, ',');
      vs_ins_sd_flds := LTRIM (vs_ins_sd_flds, ',');
      vs_src_mdp_flds := LTRIM (vs_src_mdp_flds, ',');
      vs_sqlstr :=
            ' CREATE OR REPLACE PROCEDURE '
         || vs_proc_name
         || '
IS

v_prog_name      VARCHAR2 (100) := '''
         || vs_proc_name
         || ''';
v_step           VARCHAR2 (100);

BEGIN

   pre_logon;

   v_step := ''start '';
   dbex (v_step || SYSDATE, v_prog_name);';

      IF vs_ins_sd_flds IS NOT NULL
      THEN
         vs_sqlstr :=
               vs_sqlstr
            || '
   MERGE /*+ INDEX(s, SALES_DATA_PK) */ INTO sales_data s
   USING(SELECT /*+ FULL(i) PARALLEL(i, 64) */ i.item_id, i.location_id, i.sdate_ld sales_date'
            || vs_src_sel_flds
            || '
       FROM '
            || is_imp_table
            || '_tmp i
       WHERE 1 = 1
       --AND i.rec_cnt < 2
       GROUP BY i.item_id, i.location_id, i.sdate_ld) s1
   ON(s.item_id = s1.item_id
   AND s.location_id = s1.location_id
   AND s.sales_date = s1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET '
            || vs_upd_sd_flds || '
   WHERE 1 = 1' || vs_upd_con_flds ;

      IF ins_or_upd = 0 THEN
         vs_sqlstr :=
               vs_sqlstr
            || '
   WHEN NOT MATCHED THEN
   INSERT /*+ APPEND NOLOGGING */ (item_id, location_id, sales_date, '
            || vs_ins_sd_flds
            || ')
   VALUES (s1.item_id, s1.location_id, s1.sales_date'
            || vs_src_sd_flds || ')
   WHERE 1 = 1 ' || vs_ins_con_flds ;

      END IF;

       vs_sqlstr :=
               vs_sqlstr || ';
            COMMIT;';

      END IF;

      IF vs_mdp_flds IS NOT NULL
      THEN
         vs_sqlstr :=
               vs_sqlstr
            || '
      UPDATE mdp_matrix f
      SET ('
            || vs_mdp_flds
            || ')=
         (SELECT '
            || vs_src_mdp_flds
            || '
         FROM '
            || is_imp_table
            || '_tmp
         WHERE item_id = f.item_id
         AND location_id = f.location_id)
      WHERE EXISTS(SELECT 1
                   FROM '
            || is_imp_table
            || '_tmp i
                   WHERE i.item_id = f.item_id
                   AND i.location_id = f.location_id);
      COMMIT;';
      END IF;

      vs_sqlstr :=
            vs_sqlstr
         || '
    DELETE /*+ FULL(imp) PARALLEL(imp, 64) */ '
         || is_imp_table
         || ' imp
    WHERE ('
         || vs_levels
         || ',sdate) IN
         (SELECT '
         || vs_levels
         || ',sdate FROM '
         || is_imp_table
         || '_tmp WHERE 1 = 1
                  --AND rec_cnt < 2
          );
    COMMIT;

    INSERT INTO '
         || is_imp_table
         || '_err
    (SELECT /*+ FULL(imp) PARALLEL(imp, 64) */ CASE WHEN COUNT(1) OVER (PARTITION BY '
         || vs_levels
         || ',sdate) < 2 THEN ''Error - Item or Location does not exist''
         ELSE ''Error - Duplicate data'' END, imp.*
     FROM '
         || is_imp_table
         || ' imp);

    COMMIT;

    DELETE /*+ FULL(imp) PARALLEL(imp, 64) */ FROM '
         || is_imp_table
         || ' imp;

    COMMIT;

    v_step := ''end '';
    dbex (v_step || SYSDATE, v_prog_name);

EXCEPTION
  WHEN OTHERS
  THEN
     BEGIN
        dbex (   ''Fatal Error in Step: ''
              || v_step
              || ''. SQLCODE = ''
              || SQLCODE
              || '' ''
              || SQLERRM,
              v_prog_name
             );
        RAISE;
     END;

END;
';
      dynamic_ddl (vs_sqlstr);
      dynamic_ddl ('BEGIN ' || vs_proc_name || '; END;');
      check_and_drop (vs_proc_name);
      v_status := 'end ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
   END prc_lowest_lev_integration;


PROCEDURE prc_run_wf_schema (
      v_schema     VARCHAR2,
      v_terminate  VARCHAR2 DEFAULT NULL,
      v_term_err   VARCHAR2 DEFAULT NULL,
      v_server     VARCHAR2 DEFAULT NULL,
      v_user       VARCHAR2 DEFAULT NULL,
      v_password   VARCHAR2 DEFAULT NULL
   )
   AS
      servlet              VARCHAR2 (20000);
      buff                 VARCHAR2 (32767);
      cnt                  INTEGER;
      is_done              BOOLEAN;
      v_schema_id          INTEGER;
      vi_server            VARCHAR2 (100);
      vi_user              VARCHAR2 (100);
      vi_password          VARCHAR2 (300);
      vs_appserverurl      VARCHAR2 (200);
      vs_devappserverurl   VARCHAR2 (200);
      v_process_id         NUMBER;

   BEGIN

      pre_logon;
      v_prog_name := 'PRC_RUN_WF_SCHEMA';
      v_status := 'start ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      get_param ('sys_params', 'AppServerURL', vs_appserverurl);
      get_param ('sys_params', 'DevAppServerURL', vs_devappserverurl);
      --dbms_output.put_line(v_server);
      vi_server := NVL (v_server, NVL (vs_appserverurl, vs_devappserverurl));
      vi_user := NVL (v_user, 'ptp');
      SELECT password INTO vi_password FROM user_id WHERE user_name = vi_user;
      vi_password := NVL (v_password, vi_password);
      servlet := 'WorkflowServer?action=run_proc';
      servlet := servlet || '&user=' || vi_user || '&password=' || vi_password;
      servlet := servlet || '&schema=' || v_schema;

      SELECT schema_id
        INTO v_schema_id
        FROM wf_schemas
       WHERE schema_name = v_schema;

      SELECT COUNT (*)
           INTO cnt
           FROM wf_process_log
          WHERE schema_id = v_schema_id AND status = 1;

      IF cnt <> 0 THEN
        raise_application_error(-20013, 'Couldnt start the workflow. A previous instance of the workflow already running.');
      END IF;

      -- dbms_output.put_line(vi_server||'/'||servlet);
      SELECT UTL_HTTP.request (vi_server || '/' || servlet)
        INTO buff
        FROM DUAL;

      BEGIN
      v_process_id := TO_NUMBER(LTRIM(RTRIM(buff)));
      EXCEPTION
      WHEN OTHERS THEN
          raise_application_error(-20010, 'Couldnt start the workflow. URL may be wrong.');
      END;

      IF v_process_id = -1 THEN

          raise_application_error(-20011, 'Couldnt start the workflow. Process id is -1.');

      END IF;

    -- This procedure will wait until the named workflow schema is complete
    -- Schema name must match exactly  - the procedure will end if no match is found
      is_done := FALSE;

      WHILE is_done = FALSE
      LOOP
         dbms_lock.sleep (180);

         SELECT COUNT (*)
           INTO cnt
           FROM wf_process_log
          WHERE schema_id = v_schema_id AND status = 1;

         is_done := (cnt = 0);

         IF v_terminate IS NOT NULL AND cnt <> 0 THEN

            SELECT COUNT(1)
            INTO cnt
            FROM portal_task
            WHERE LOWER(description) LIKE '%process%' || v_process_id ||'%schema%' || v_schema_id || '%';

            IF cnt = 1 THEN

                  IF v_term_err IS NOT NULL THEN
                      raise_application_error(-20013, 'Workflow Process id ' || v_process_id || ' terminated with errors');
                  ELSE

                      UPDATE portal_task
                      SET status = 3, seen = 1
                      WHERE LOWER(description) LIKE '%process%' || v_process_id ||'%schema%' || v_schema_id || '%';

                      COMMIT;

                  END IF;

            END IF;

         END IF;

      END LOOP;

      v_status := 'end ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
   END prc_run_wf_schema;


PROCEDURE prc_start_wf_schema (
      v_schema     VARCHAR2,
      v_server     VARCHAR2 DEFAULT NULL,
      v_user       VARCHAR2 DEFAULT NULL,
      v_password   VARCHAR2 DEFAULT NULL
   )
   AS
      servlet              VARCHAR2 (20000);
      buff                 VARCHAR2 (32767);
      CNT                  integer;
      err_cnt              INTEGER;
      is_done              BOOLEAN;
      v_schema_id          INTEGER;
      vi_server            VARCHAR2 (100);
      vi_user              VARCHAR2 (100);
      vi_password          VARCHAR2 (300);
      vs_appserverurl      VARCHAR2 (200);
      vs_devappserverurl   VARCHAR2 (200);

   BEGIN

      pre_logon;
      v_prog_name := 'PRC_START_WF_SCHEMA';
      v_status := 'start ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

      get_param ('sys_params', 'AppServerURL', vs_appserverurl);
      get_param ('sys_params', 'DevAppServerURL', vs_devappserverurl);
      --dbms_output.put_line(v_server);
      vi_server := NVL (v_server, NVL (vs_appserverurl, vs_devappserverurl));
      vi_user := NVL (v_user, 'ptp');
      SELECT password INTO vi_password FROM user_id WHERE user_name = vi_user;
      vi_password := NVL (v_password, vi_password);
      servlet := 'WorkflowServer?action=run_proc';
      servlet := servlet || '&user=' || vi_user || '&password=' || vi_password;
      servlet := servlet || '&schema=' || v_schema;

      -- dbms_output.put_line(vi_server||'/'||servlet);
      SELECT UTL_HTTP.request (vi_server || '/' || servlet)
        INTO buff
        FROM DUAL;

    -- This procedure will wait until the named workflow schema is complete
    -- Schema name must match exactly  - the procedure will end if no match is found
      is_done := FALSE;

      SELECT schema_id
        INTO v_schema_id
        FROM wf_schemas
       WHERE schema_name = v_schema;

      WHILE is_done = FALSE
      LOOP
         dbms_lock.sleep (180);

--          select COUNT (*)
--           INTO err_cnt
--           from WF_PROCESS_LOG
--          where SCHEMA_ID = V_SCHEMA_ID and STATUS = -1;
--      if  ERR_CNT = 1
--       then   RAISE_APPLICATION_ERROR(-20013, 'Workflow Schema ' || v_schema || ' terminated with errors');
--      else
         SELECT COUNT (*)
           INTO cnt
           FROM wf_process_log
          WHERE schema_id = v_schema_id AND status = 1;

         IS_DONE := (CNT = 0);

--        END IF;
      END LOOP;

      v_status := 'end ';
      v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
   END prc_start_wf_schema;


PROCEDURE prc_run_full_proport_1 AS

BEGIN

  pre_logon;
  v_status := 'start ';
  v_prog_name := 'PRC_RULL_FULL_PROPORT_1';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

  UPDATE /*+ FULL(mdp) PARALLEL(mdp, 32) */ mdp_matrix mdp
  SET prop_changes = 1;

  COMMIT;

  proport('ma',NULL,1);

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
    RAISE;
  END;

END;

PROCEDURE prc_run_full_proport_102 AS

BEGIN

  pre_logon;
  v_status := 'start ';
  v_prog_name := 'PRC_RULL_FULL_PROPORT_102';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

  UPDATE /*+ FULL(mdp) PARALLEL(mdp, 32) */ mdp_matrix mdp
  SET prop_changes = 1;

  COMMIT;

  proport('ma',NULL,102);

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
    RAISE;
  END;

END;

PROCEDURE prc_mdp_add(is_tbl_name               VARCHAR2  DEFAULT NULL,
      start_date                  DATE DEFAULT NULL,
      end_date                    DATE DEFAULT NULL,
      ii_proport_run            NUMBER    DEFAULT NULL
   )
IS
   vi_is_exists                     INTEGER;
   vs_sql                           VARCHAR2(4000);
   vi_top_level                     NUMBER(2);
   vi_hist_glob_prop                NUMBER;
   vd_last_date                     DATE;
   vi_cannibalism                   NUMBER(1) := 0;
   vi_aggri_98                      INT;
   vi_aggri_99                      INT;
   vd_start_date                    DATE;
   vs_quantity_form                 init_params_0.value_string%TYPE;
   vs_sql_proc                      VARCHAR2(32000);
   vs_fields                        VARCHAR2(10000);
   vs_values                        VARCHAR2(10000);
   vi_num_rows                      INTEGER;
   vi_add_zeroes                    INTEGER;
   vs_tbl_name                      VARCHAR2(30);
   vs_distinct                      VARCHAR2(30) := ' DISTINCT ';
   vi_rows                          NUMBER;
   vi_run_proport                   INTEGER;
   vs_limit_code                    VARCHAR2(200);
   vi_matrix_limit                  INTEGER;
   vi_serias_id                     NUMBER(10);
   vd_max_sales_date                DATE;
   vi_mdp_analyze_threshold         INT := 0;

   vs_sql_insert_mdp_matrix         VARCHAR2(32000);
   vs_sql_where                     VARCHAR2(32000);
   vs_sql_pop_temp                  VARCHAR2(32000);
   vs_temp_table_name               VARCHAR2(30);

   vs_is_fictive_exp                VARCHAR2(100) := '0';

   vs_update_is_fictive             VARCHAR2(30);
   vs_update_is_linked              VARCHAR2(30);
   vs_delete_unlinked_mdp_matrix    VARCHAR2(30);

   vs_use_parallel_dml              VARCHAR2(30)  := '';
   vs_use_parallel_hint             VARCHAR2(30)  := '';
   vs_db_hint_mdp_insert            VARCHAR2(200) := '';
   vs_where_uses_in                 VARCHAR2(30)  := '';
   vs_db_hint_mdp_ins_in_tbl        VARCHAR2(200) := '';
   vs_db_hint_mdp_ins_in_mdp        VARCHAR2(200) := '';
   vs_db_hint_mdp_count             VARCHAR2(200) := '';
   vs_db_hint_mdp_insert_select     VARCHAR2(200) := '';

   vs_db_hint_update_is_fictive_m   VARCHAR2(200) := '';
   vs_db_hint_update_is_fictive_0   VARCHAR2(200) := '';
   vs_db_hint_update_is_fictive_2   VARCHAR2(200) := '';

   vs_db_hint_delete_mdp            VARCHAR2(200) := '';
   vs_db_hint_delete_mdp_from_sd    VARCHAR2(200) := '';
   vs_db_hint_delete_mdp_from_pd    VARCHAR2(200) := '';

   vs_use_mdp_cache_merge           VARCHAR2(30)  := '';

   vs_db_hint_merge_mdp_from_sd     VARCHAR2(200) := '';

   vi_logging_level                 INTEGER;
   vs_log_table                     VARCHAR2(30);
   vs_main_proc                     VARCHAR2(60);
   vs_proc_source                   VARCHAR2(60);
   vs_log_msg                       VARCHAR2(2000);

BEGIN

   pre_logon;
   v_status := 'start ';
   v_prog_name := 'PRC_MDP_ADD';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   /*********************/
   /* Initialize LOG_IT */
   /*********************/
   vs_main_proc    := 'MDP_ADD';


   IF is_tbl_name IS NULL THEN
      vs_tbl_name := 'sales_data';

   ELSIF LOWER(is_tbl_name) <> 'mdp_load_assist' THEN

      RAISE_APPLICATION_ERROR(-20001,'Wrong parameter - is_tbl_name = '||is_tbl_name);

   ELSE
      vs_tbl_name := is_tbl_name;
      -- We should do a distinct for mdp_load_Assist as well
      -- vs_distinct := '';
   END IF;


   vi_matrix_limit := decrypt(vs_limit_code);

   IF vi_matrix_limit >= 999999000 THEN

      SELECT COUNT(*)
      INTO   vi_rows
      FROM   mdp_matrix
      WHERE  ROWNUM < 2;

   ELSE

      SELECT COUNT(*)
      INTO   vi_rows
      FROM   mdp_matrix;

      IF vi_rows >= vi_matrix_limit THEN

         dbex('The number of allowed combination has been exceeded.
         Please contact System Administrator','mdp_add');
         --mail_pkg.send

         RAISE_APPLICATION_ERROR (-20099,
             'The number of allowed combination has been exceeded.
              Please contact System Administrator');
      END IF;
   END IF;


   SELECT num_rows
   INTO   vi_num_rows
   FROM   user_tables
   WHERE  table_name = 'MDP_MATRIX';


   get_param('db_params','Mdp_AddUpdateIs_Fictive', vs_update_is_fictive, 'TRUE');

   IF vs_update_is_fictive <> 'FALSE' THEN
      vs_update_is_fictive := 'TRUE';
   END IF;

   get_param('db_params','Mdp_AddUpdateIs_Linked', vs_update_is_linked, 'TRUE');

   IF vs_update_is_linked <> 'FALSE' THEN
      vs_update_is_linked := 'TRUE';
   END IF;

   get_param('db_params','Mdp_AddDeleteUnlinkedMdp_Matrix', vs_delete_unlinked_mdp_matrix, 'FALSE');

   IF vs_delete_unlinked_mdp_matrix <> 'TRUE' THEN
      vs_delete_unlinked_mdp_matrix := 'FALSE';
   END IF;

   get_param('db_params','DBHintMdp_AddUseMdpCacheMerge', vs_use_mdp_cache_merge, 'TRUE');

   IF vs_use_mdp_cache_merge <> 'TRUE' THEN
      vs_use_mdp_cache_merge := 'FALSE';
   END IF;

   get_param('init_params_0','top_level'            , vi_top_level);
   get_param('init_params_0','hist_glob_prop'       , vi_hist_glob_prop);
   get_param('init_params_0','cannibalism'          , vi_cannibalism);
   get_param('init_params_0','quantity_form'        , vs_quantity_form);
   get_param('init_params_0','add_zero_combs_to_mdp', vi_add_zeroes);

   get_param('sys_params','max_sales_date'    , vd_max_sales_date);

   SELECT DECODE(vi_cannibalism,2,1,vi_cannibalism),
          DECODE(vi_cannibalism,2,0,vi_cannibalism)
   INTO   vi_aggri_98,
          vi_aggri_99
   FROM   dual;

   get_param('db_params','DBHintMdp_AddUseParallelDML', vs_use_parallel_dml, 'FALSE');
   get_param('db_params','DBHintMdp_AddUseParallel'     , vs_use_parallel_hint, 'TRUE');

   get_param('db_params','DBHintMdp_AddMdpInsInMdpUseIn', vs_where_uses_in    , 'TRUE');

   vs_db_hint_mdp_insert := '/*+ append */';

   IF vs_use_parallel_hint = 'TRUE' THEN


      get_param('db_params','DBHintMdp_AddInsert'              , vs_db_hint_mdp_insert         , '');
      get_param('db_params','DBHintMdp_AddInsertSelect'        , vs_db_hint_mdp_insert_select  , '' );

      get_param('db_params','DBHintMdp_AddMdpInsInTbl'         , vs_db_hint_mdp_ins_in_tbl     , '');
      get_param('db_params','DBHintMdp_AddMdpInsInMdp'         , vs_db_hint_mdp_ins_in_mdp     , '');

      get_param('db_params','DBHintMdp_AddMdpCount'            , vs_db_hint_mdp_count          , '');

      get_param('db_params','DBHintMdp_AddUpdateIs_Fictive0MDP', vs_db_hint_update_is_fictive_m , '');
      get_param('db_params','DBHintMdp_AddUpdateIs_Fictive0SD' , vs_db_hint_update_is_fictive_0 , '');
      get_param('db_params','DBHintMdp_AddUpdateIs_Fictive2SD' , vs_db_hint_update_is_fictive_2 , '');

      get_param('db_params','DBHintMdp_AddDeleteMdp'           , vs_db_hint_delete_mdp         , '');
      get_param('db_params','DBHintMdp_AddDeleteMdpNotInSD'    , vs_db_hint_delete_mdp_from_sd , '');
      get_param('db_params','DBHintMdp_AddDeleteMdpNotInPD'    , vs_db_hint_delete_mdp_from_pd , '');

      get_param('db_params','DBHintMdp_AddMergeSelectSD'       , vs_db_hint_merge_mdp_from_sd  , '');

      vs_db_hint_mdp_insert_select       := REPLACE(vs_db_hint_mdp_insert_select, '@TBL_NAME@'   , vs_tbl_name);

      vs_db_hint_mdp_insert            := '/*' || vs_db_hint_mdp_insert            ||'*/';
      vs_db_hint_mdp_insert_select     := '/*' || vs_db_hint_mdp_insert_select     ||'*/';

      vs_db_hint_mdp_ins_in_tbl        := '/*' || vs_db_hint_mdp_ins_in_tbl        ||'*/';
      vs_db_hint_mdp_ins_in_mdp        := '/*' || vs_db_hint_mdp_ins_in_mdp        ||'*/';

      vs_db_hint_mdp_count             := '/*' || vs_db_hint_mdp_count             ||'*/';

      vs_db_hint_update_is_fictive_m   := '/*' || vs_db_hint_update_is_fictive_m   ||'*/ ';
      vs_db_hint_update_is_fictive_0   := '/*' || vs_db_hint_update_is_fictive_0   ||'*/ ';
      vs_db_hint_update_is_fictive_2   := '/*' || vs_db_hint_update_is_fictive_2   ||'*/ ';

      vs_db_hint_delete_mdp            := '/*' || vs_db_hint_delete_mdp            ||'*/ ';
      vs_db_hint_delete_mdp_from_sd    := '/*' || vs_db_hint_delete_mdp_from_sd    ||'*/ ';
      vs_db_hint_delete_mdp_from_pd    := '/*' || vs_db_hint_delete_mdp_from_pd    ||'*/ ';

      vs_db_hint_merge_mdp_from_sd     := '/*' || vs_db_hint_merge_mdp_from_sd     ||'*/ ';

      vs_db_hint_mdp_insert            := REPLACE( vs_db_hint_mdp_insert       , '/**/','');
      vs_db_hint_mdp_insert_select     := REPLACE( vs_db_hint_mdp_insert_select, '/**/','');

      vs_db_hint_mdp_ins_in_tbl        := REPLACE( vs_db_hint_mdp_ins_in_tbl   , '/**/','');
      vs_db_hint_mdp_ins_in_mdp        := REPLACE( vs_db_hint_mdp_ins_in_mdp   , '/**/','');

      vs_db_hint_mdp_count             := REPLACE( vs_db_hint_mdp_count        , '/**/','');

      vs_db_hint_update_is_fictive_m   := REPLACE( vs_db_hint_update_is_fictive_m, '/**/','');
      vs_db_hint_update_is_fictive_0   := REPLACE( vs_db_hint_update_is_fictive_0, '/**/','');
      vs_db_hint_update_is_fictive_2   := REPLACE( vs_db_hint_update_is_fictive_2, '/**/','');

      vs_db_hint_delete_mdp            := REPLACE( vs_db_hint_delete_mdp        , '/**/','');
      vs_db_hint_delete_mdp_from_sd    := REPLACE( vs_db_hint_delete_mdp_from_sd, '/**/','');
      vs_db_hint_delete_mdp_from_pd    := REPLACE( vs_db_hint_delete_mdp_from_pd, '/**/','');

      vs_db_hint_merge_mdp_from_sd     := REPLACE( vs_db_hint_merge_mdp_from_sd,  '/**/','');

      IF LOWER(is_tbl_name) <> 'mdp_load_assist' THEN
         vs_db_hint_mdp_insert_select := REPLACE( LOWER(vs_db_hint_mdp_insert_select), 'mdp_load_assist','sales_data');
      END IF;


   END IF;

   vs_fields := NULL;
   vs_values := NULL;

    FOR rec IN (SELECT DISTINCT
                      search_field fld,search_table,
                      search_table||'.'||lower(id_field) id_field,
                      DECODE(LOWER(search_table),'items','item_id','location','location_id') id,
                      RTRIM(LTRIM(gtable)) || '_LUD'  AS lud_col
               FROM   group_tables
               WHERE  LOWER(search_table) IN ('items','location')
               AND    LOWER(gtable)   NOT IN ('items','location')
               AND    search_field        IS NOT NULL
               AND    search_table        IS NOT NULL
               AND    id_field            IS NOT NULL
               AND    network_level       IS NULL)
   LOOP

      vs_fields := vs_fields||','||rec.fld;
      vs_values := vs_values||','||rec.id_field;

   END LOOP;



   vs_sql_proc := 'CREATE OR REPLACE PROCEDURE rr_prc_dynamic_mdp_add
IS
   vi_exists       INTEGER;
   vi_count_insert_mdp_matrix    INTEGER;
   vi_count_update_mdp_matrix    INTEGER;

   vi_logging_level              INTEGER;
   vs_log_table                  VARCHAR2(30);
   vs_main_proc                  VARCHAR2(60);
   vs_proc_source                VARCHAR2(60);
   vs_log_msg                    VARCHAR2(2000);

   v_prog_name    VARCHAR2 (100);
   v_status       VARCHAR2 (100);
   v_proc_log_id  NUMBER;

BEGIN
   pre_logon;
   v_status := ''start '';
   v_prog_name := ''RR_PRC_DYNAMIC_MDP_ADD'';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, ''' || v_package_name || ''', v_status || SYSDATE);

   vs_main_proc    := ''RR_PRC_DYNAMIC_MDP_ADD'';

   vi_count_insert_mdp_matrix := 0;
';

   IF vs_use_parallel_dml = 'TRUE' THEN

      vs_sql_proc := vs_sql_proc ||'

   vs_log_msg := ''ALTER SESSION ENABLE PARALLEL DML''; LOG_IT (vs_log_table, vi_logging_level ,2 , vs_main_proc, ''M'', vs_log_msg , NULL , NULL);

   EXECUTE IMMEDIATE (''ALTER SESSION ENABLE PARALLEL DML'');
';
   END IF;

   /*********************************************************/
   /* Define The IS_FICTIVE for the following INSERT SELECT */
   /*********************************************************/

   IF LOWER(is_tbl_name) = 'mdp_load_assist' THEN

      vs_is_fictive_exp := vs_tbl_name    ||'.is_fictive';
   ELSE

      vs_is_fictive_exp := '0';

   END IF;


   vs_sql_insert_mdp_matrix := '
   INSERT '|| vs_db_hint_mdp_insert ||' INTO mdp_matrix
            (item_id, location_id,
             is_fictive,
             level_id, models, item_node, loc_node, glob_prop,
             prediction_status, intv_prop, hist_glob_prop, updb_date, prop_changes,
             opti_time, aggri_98, aggri_99, psum_98, psum_99'||vs_fields||')
   SELECT '|| vs_db_hint_mdp_insert_select || vs_distinct ||'
          '|| vs_tbl_name       ||'.item_id,'||vs_tbl_name||'.location_id,
          '|| vs_is_fictive_exp ||',
          '|| vi_top_level      ||',''none'',0,0,0,98,1,
          '|| vi_hist_glob_prop ||','''|| vd_last_date ||''', 1,''1-1-1900 0:0:0'',
          '|| vi_aggri_98       ||',
          '|| vi_aggri_99       ||',
          '|| vi_cannibalism    ||',0'||vs_values||'
   FROM   '|| vs_tbl_name       ||',location,items
   WHERE ';


   vs_sql_where := '';

   IF vi_add_zeroes = 1 THEN

      vs_sql_where := vs_sql_where ||' 1 ';

   ELSIF LOWER(vs_tbl_name) = 'mdp_load_assist' THEN

      vs_sql_where := vs_sql_where || ' boolean_qty ';
   ELSE

      vs_sql_where := vs_sql_where || vs_quantity_form;
   END IF;

   IF vs_where_uses_in = 'TRUE' THEN


      vs_sql_where := vs_sql_where || ' > 0
   AND    '|| vs_tbl_name ||'.item_id     = items.item_id
   AND    '|| vs_tbl_name ||'.location_id = location.location_id
   AND   ('|| vs_tbl_name ||'.item_id, '||vs_tbl_name||'.location_id) IN
         (SELECT '|| vs_db_hint_mdp_ins_in_tbl ||' item_id, location_id
          FROM   '|| vs_tbl_name ||' a
          MINUS
          SELECT '|| vs_db_hint_mdp_ins_in_mdp ||' item_id, location_id
          FROM   mdp_matrix b);

   vi_count_insert_mdp_matrix := vi_count_insert_mdp_matrix + NVL(sql%rowcount,0);

   COMMIT;

';
   ELSE

      vs_sql_where := vs_sql_where || ' > 0
      AND '||vs_tbl_name||'.item_id     = items.item_id
      AND '||vs_tbl_name||'.location_id = location.location_id
      AND NOT EXISTS (SELECT 1
                      FROM   mdp_matrix
                      WHERE  item_id     = '||vs_tbl_name||'.item_id
                      AND    location_id = '||vs_tbl_name||'.location_id );

      vi_count_insert_mdp_matrix := vi_count_insert_mdp_matrix + NVL(sql%rowcount,0);

      COMMIT;

';
   END IF;


   get_param('sys_params', 'mdp_analyze_threshold', vi_mdp_analyze_threshold, 0);


   IF vi_num_rows > vi_mdp_analyze_threshold THEN

      vs_sql_proc := vs_sql_proc || vs_sql_insert_mdp_matrix || vs_sql_where;

   ELSE


      vs_sql_proc := vs_sql_proc || vs_sql_insert_mdp_matrix || ' ROWNUM < ' || TO_CHAR(vi_mdp_analyze_threshold) || '
   AND   ' || vs_sql_where;

      vs_sql_proc := vs_sql_proc || vs_sql_insert_mdp_matrix || vs_sql_where;

   END IF;

   vs_sql_proc := vs_sql_proc ||'
   INSERT INTO glob_mdp_add SELECT DISTINCT item_id FROM '|| vs_tbl_name ||';

   UPDATE items
   SET    is_linked = 1
   WHERE  item_id IN (SELECT id FROM glob_mdp_add);

   COMMIT;

   DELETE FROM glob_mdp_add;

   INSERT INTO glob_mdp_add SELECT DISTINCT location_id FROM '|| vs_tbl_name ||';

   UPDATE location
   SET    is_linked = 1
   WHERE  location_id IN (SELECT id FROM glob_mdp_add);


   COMMIT;
   ';



   IF vs_update_is_fictive = 'TRUE' THEN

      vs_sql_proc := vs_sql_proc ||'
   UPDATE '|| vs_db_hint_update_is_fictive_m ||' mdp_matrix m
   SET    is_fictive = 2
   WHERE  NOT EXISTS (SELECT '|| vs_db_hint_update_is_fictive_2 ||' 1
                      FROM   sales_data s
                      WHERE  sales_date <= ''' || vd_max_sales_date || '''
                      AND    s.item_id     = m.item_id
                      AND    s.location_id = m.location_id
                      AND    NVL('|| vs_quantity_form ||', 0) > 0)
   AND    m.is_fictive = 0
   AND EXISTS(SELECT 1 FROM mdp_load_assist m1 WHERE 1 = 1 AND m1.item_id = m.item_id
                      AND m1.location_id = m.location_id);

   COMMIT;
   ';

   END IF;


   IF vs_update_is_fictive = 'TRUE' THEN


      vs_sql_proc := vs_sql_proc ||'
   UPDATE '|| vs_db_hint_update_is_fictive_m ||' mdp_matrix m
   SET    is_fictive = 0
   WHERE  EXISTS (SELECT '|| vs_db_hint_update_is_fictive_0 ||' 1
                  FROM   sales_data s
                  WHERE  s.item_id     = m.item_id
                  AND    s.location_id = m.location_id
                  AND    NVL('||vs_quantity_form||', 0) > 0)
   AND    m.is_fictive = 2
   AND EXISTS(SELECT 1 FROM mdp_load_assist m1 WHERE 1 = 1 AND m1.item_id = m.item_id
                      AND m1.location_id = m.location_id);

   COMMIT;
';

   END IF;

    IF vs_use_parallel_dml = 'TRUE' THEN


      vs_sql_proc := vs_sql_proc ||'

   EXECUTE IMMEDIATE (''ALTER SESSION DISABLE PARALLEL DML'');

';
   END IF;


   vs_sql_proc := vs_sql_proc ||'

   dynamic_ddl(''TRUNCATE TABLE glob_mdp_add'');

   MERGE INTO mdp_matrix mdp
   USING (SELECT mla.item_id, mla.location_id,
                          NVL(MIN(sd.sales_date), TO_DATE(''' || TO_CHAR(NVL(start_date, TO_DATE('01/01/2010', 'DD/MM/YYYY')), 'DD-MM-YYYY HH24:MI:SS') || ''', ''DD-MM-YYYY HH24:MI:SS'')) from_date,
                          NVL(MAX(sd.sales_date), TO_DATE(''' || TO_CHAR(NVL(end_date, TO_DATE('01/01/2010', 'DD/MM/YYYY')), 'DD-MM-YYYY HH24:MI:SS') || ''', ''DD-MM-YYYY HH24:MI:SS'')) until_date
              FROM ' || is_tbl_name || ' mla, sales_data sd
              WHERE mla.item_id = sd.item_id (+)
              AND mla.location_id = sd.location_id (+)
              GROUP BY mla.item_id, mla.location_id) mdp1
    ON(mdp.item_id = mdp1.item_id
    AND  mdp.location_id = mdp1.location_id)
    WHEN MATCHED THEN
    UPDATE SET mdp.from_date = mdp1.from_date, mdp.until_date = mdp1.until_date;

    COMMIT;

    v_status := ''end '';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, ''' || v_package_name || ''', v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, ''' || v_package_name || ''', ''Fatal Error in Step: '' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
  RAISE;
  END;
END;
';


   dynamic_ddl(vs_sql_proc,'rr_prc_dynamic_mdp_add',1);

   dynamic_proc ('rr_prc_dynamic_mdp_add');

   IF get_is_table_exists ('TEMP_MDP_ADD') = 1 THEN

      dynamic_ddl('DROP TABLE TEMP_MDP_ADD');

   END IF;

   COMMIT;
   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
  RAISE;
  END;
END;

PROCEDURE prc_export_interface(p_ii_table VARCHAR2)
IS

/******************************************************************************
   NAME:       PRC_EXPORT_INTERFACE
   PURPOSE:    Exports integration interface to the export table

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        09/18/2007  Bhaskar Rampalli/Red Rock Consulting

******************************************************************************/

      vi_query_id            INTEGER;
      vs_query_name          VARCHAR2 (100);
      vs_sqlstr              VARCHAR2 (32000);
      vi_exists              INTEGER;
      vs_sd_flds             VARCHAR2 (2000);
      vs_src_sd_flds         VARCHAR2 (2000);
      vs_mdp_flds            VARCHAR2 (2000);
      vs_src_mdp_flds        VARCHAR2 (2000);
      vs_proc_name           VARCHAR2 (30);
      vi_lowest_lev_query    INTEGER;
      vs_from                VARCHAR2 (2000);
      vs_where               VARCHAR2 (2000);
      vi_presentation_type   NUMBER (1);
      vi_counter             NUMBER (1)       := 0;
      vs_levels              VARCHAR2 (200);
      vi_tg_res_id           INTEGER;
      vs_time_res            VARCHAR2 (200);
      v_count                NUMBER;
      vi_ins_new_combs       NUMBER;
      vi_unit_id             NUMBER;

      fore_column            VARCHAR (30);
      max_sales_date         DATE;
      v_export_table         VARCHAR2(100);

      v_prog_name            VARCHAR2 (100);
      v_status               VARCHAR2 (100);
      v_proc_log_id          NUMBER;
      v_sel_cols             VARCHAR2 (2000);

      vs_relative_from_date  NUMBER;
      vs_relative_until_date NUMBER;
      vs_from_date           DATE;
      vs_until_date          DATE;

BEGIN

   pre_logon;
   v_status := 'start ';
   v_prog_name := 'PRC_EXPORT_INTERFACE';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   get_param ('sys_params', 'max_sales_date', max_sales_date);

   v_export_table := 'rr_' || p_ii_table || '_t';

   BEGIN

      SELECT query_name, presentation_type, insertnewcombinations, unit_id, relative_from_date, relative_until_date
      INTO vs_query_name, vi_presentation_type, vi_ins_new_combs, vi_unit_id, vs_relative_from_date, vs_relative_until_date
      FROM transfer_query
      WHERE UPPER (table_name) = UPPER (p_ii_table);

   EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      raise_application_error(-20001, 'There is no integration profile with import table = ''' || p_ii_table || '''' );
   END;

   vs_from_date := max_sales_date + (7 * vs_relative_from_date);
   vs_until_date := max_sales_date + (7 * vs_relative_until_date);

   BEGIN

      SELECT query_id
      INTO vi_query_id
      FROM queries
      WHERE query_name = vs_query_name;

   EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      raise_application_error (-20000, 'This query doesn''t exist');
   END;

   v_sel_cols := 'branch_data.item_id, branch_data.location_id, branch_data.sales_date';

   FOR rec IN (SELECT   table_label, gtable,
                           DECODE (vi_presentation_type,
                                   1, cust_code_field,
                                   0, data_field
                                  ) fld,
                           search_table
                      FROM queries q, GROUPS g, group_tables gt
                     WHERE q.query_id = vi_query_id
                       AND q.query_id = g.query_id
                       AND g.group_table_id = gt.group_table_id
                       AND LOWER (search_table) IN ('items', 'location', 'mdp_matrix')
                 --      AND father_id = 0
                  ORDER BY gorder)
   LOOP
         vi_counter := vi_counter + 1;
         vs_from := vs_from || ',' || rec.gtable || ' ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '');
         vs_where := vs_where || '
                    AND m.'|| rec.gtable || '_ep_id = ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '.' || rec.gtable || '_ep_id ';

         vs_levels := vs_levels || ',level' || vi_counter;
         v_sel_cols := v_sel_cols || ', ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '.'|| rec.fld || ' ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || ', '
                                  || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '.' || rec.gtable || '_ep_id ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '_id'
                                  || ', ' || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '.'|| rec.fld || ' level' || vi_counter || ', '
                                  || REPLACE(REPLACE(LOWER(rec.table_label), ' ', '_'), '/', '') || '.' || rec.gtable || '_ep_id level' || vi_counter || '_id';

   END LOOP;

   vs_levels := LTRIM (vs_levels, ',');

   /*dbms_output.put_line(vs_levels);
   dbms_output.put_line(vs_from);
   dbms_output.put_line(vs_where);
   dbms_output.put_line(v_sel_cols); */

   FOR rec IN (SELECT exp_template, data_table_name, computed_name
               FROM active_series acs,
                    computed_fields cf
               WHERE acs.forecast_type_id = cf.forecast_type_id
               AND acs.query_id = vi_query_id
               ORDER BY serias_id)
   LOOP

      vs_sd_flds := vs_sd_flds || ',' || rec.exp_template  || ' ' || rec.computed_name;

   END LOOP;

   vs_sd_flds := LTRIM (vs_sd_flds, ',');

   vs_sd_flds := REPLACE(vs_sd_flds, '#UNIT#', 1);
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  'mdp_matrix.', 'm.');
   vs_sd_flds := get_exp_with_no_token(vs_sd_flds);
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  ', fore_', ', branch_data.fore_');
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  ',fore_', ', branch_data.fore_');
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  'sum(', '(');
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  'max(', '(');
   vs_sd_flds := REPLACE(LOWER(vs_sd_flds),  'min(', '(');

   --dbms_output.put_line(vs_sd_flds);

   vs_sqlstr := 'DROP TABLE ' || v_export_table || ' PURGE';

   --dbms_output.put_line(vs_sqlstr);

   dynamic_ddl (vs_sqlstr);

   vs_sqlstr := 'CREATE TABLE ' || v_export_table || ' AS
      (SELECT /*+ FULL(branch_data) PARALLEL(branch_data, 16) */ '
            || v_sel_cols || ', ' || vs_sd_flds
            || '
       FROM  sales_data branch_data, mdp_matrix m ' || vs_from || '
       WHERE  1 = 1
       AND branch_data.item_id = m.item_id
       AND branch_data.location_id = m.location_id
       AND branch_data.sales_date BETWEEN TO_DATE(' || TO_CHAR(vs_from_date, 'DD/MM/YYYY') || ', ''DD/MM/YYYY'') AND ' || ' TO_DATE(' || TO_CHAR(vs_until_date, 'DD/MM/YYYY') || ', ''DD/MM/YYYY'')
       ' || vs_where || ')';

   --dbms_output.put_line(vs_sqlstr);

   dynamic_ddl (vs_sqlstr);

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

   EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
      RAISE;
      END;

END;

PROCEDURE prc_do_nothing(
       user_id     NUMBER DEFAULT NULL,
       level_id    NUMBER DEFAULT NULL,
       member_id   NUMBER DEFAULT NULL
   ) AS

BEGIN
  NULL;
END;

 PROCEDURE prc_update_series_groups
AS
/******************************************************************************
   NAME:       PRC_EXPORT_INTERFACE
   PURPOSE:    Updates Series Groups. Helps debug worksheets

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        20/12/2012  Luke Pocock/Red Rock Consulting

******************************************************************************/
   v_all_series_group     NUMBER := 0;
   v_sd_series_group      NUMBER := 529;
   v_pd_series_group      NUMBER := 530;
   v_ad_series_group      NUMBER := 469;
   v_claim_series_group   NUMBER := 209;
BEGIN
---Clear all groups and rebuild
   DELETE from series_groups_m
         WHERE group_id in (v_sd_series_group,v_pd_series_group, v_ad_series_group, v_claim_series_group);

   COMMIT;
---All Series Group
   MERGE INTO series_groups_m g
      USING (SELECT f.forecast_type_id
               FROM computed_fields f) c
      ON (c.forecast_type_id = g.series_id
          AND g.GROUP_ID = v_all_series_group)
      WHEN NOT MATCHED THEN
         INSERT (g.GROUP_ID, g.series_id)
         VALUES (v_all_series_group, c.forecast_type_id);
   COMMIT;
   ---Sales_data Series
   MERGE INTO series_groups_m g
      USING (SELECT forecast_type_id
               FROM computed_fields
              WHERE LOWER (data_table_name) = 'branch_data') c
      ON (c.forecast_type_id = g.series_id AND g.GROUP_ID = v_sd_series_group)
      WHEN NOT MATCHED THEN
         INSERT (g.GROUP_ID, g.series_id)
         VALUES (v_sd_series_group, c.forecast_type_id);
   COMMIT;
   ---Accrual_data Series
   MERGE INTO series_groups_m g
      USING (SELECT forecast_type_id
               FROM computed_fields
              WHERE LOWER (data_table_name) = 'accrual_data') c
      ON (c.forecast_type_id = g.series_id AND g.GROUP_ID = v_ad_series_group)
      WHEN NOT MATCHED THEN
         INSERT (g.GROUP_ID, g.series_id)
         VALUES (v_ad_series_group, c.forecast_type_id);
   COMMIT;
   ---Accrual_data Series
   MERGE INTO series_groups_m g
      USING (SELECT forecast_type_id
               FROM computed_fields
              WHERE LOWER (data_table_name) = 'promotion_data') c
      ON (c.forecast_type_id = g.series_id AND g.GROUP_ID = v_pd_series_group)
      WHEN NOT MATCHED THEN
         INSERT (g.GROUP_ID, g.series_id)
         VALUES (v_pd_series_group, c.forecast_type_id);
   COMMIT;
   ---Claims Series
   MERGE INTO series_groups_m g
      USING (SELECT forecast_type_id
               FROM computed_fields
              WHERE LOWER (data_table_name) = 'settlement') c
      ON (    c.forecast_type_id = g.series_id
          AND g.GROUP_ID = v_claim_series_group)
      WHEN NOT MATCHED THEN
         INSERT (g.GROUP_ID, g.series_id)
         VALUES (v_claim_series_group, c.forecast_type_id);
   COMMIT;
END;


   PROCEDURE prc_update_accountmgr
   AS

    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_UPDATE_ACCOUNTMGR';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   MERGE INTO settlement_owner e
      USING (SELECT DISTINCT u.user_id, REPLACE (u.user_name, '_ptp', '') account_mgr
               FROM  user_id u, user_groups ug, user_security_group usg
              WHERE ug.user_id = u.user_id
                AND ug.group_id = usg.group_id
                AND usg.group_id IN (347, 348,580)
                and u.user_name not in ('SBSUSER1','SBSUSER2')
                AND UPPER(u.user_name) NOT LIKE '%USER_%_PTP%'
                AND UPPER(u.user_name) NOT LIKE '%USER_%_DM%') u
      ON (u.user_id = e.settlement_owner_id)
      WHEN MATCHED THEN
         UPDATE
            SET e.settlement_owner_code = u.user_id,
                e.settlement_owner_Desc = u.account_mgr,
                e.last_update_date = sysdate
      WHEN NOT MATCHED THEN INSERT
      (settlement_owner_id,settlement_owner_Code,settlement_owner_Desc,last_update_date)
      values
      (u.user_id,u.user_id,u.account_mgr,sysdate);

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;
   END;

   PROCEDURE prc_set_lm_fail_status(user_id     NUMBER DEFAULT NULL,
                                    level_id    NUMBER DEFAULT NULL,
                                    member_id   NUMBER DEFAULT NULL)
   AS
   v_table      VARCHAR2 (100);
   v_id_field   VARCHAR2 (100);
   BEGIN
   SELECT gtable, id_field
     INTO v_table, v_id_field
     FROM group_tables
    WHERE group_table_id = level_id;

   EXECUTE IMMEDIATE    'UPDATE '
                     || v_table
                     || '
               SET method_status = 4 WHERE '
                     || v_id_field
                     || ' = '
                     || member_id;

   COMMIT;

   END;

   PROCEDURE prc_set_lm_success_status(user_id     NUMBER DEFAULT NULL,
                                       level_id    NUMBER DEFAULT NULL,
                                       member_id   NUMBER DEFAULT NULL)
   AS
   v_table      VARCHAR2 (100);
   v_id_field   VARCHAR2 (100);
   BEGIN

   SELECT gtable, id_field
     INTO v_table, v_id_field
     FROM group_tables
    WHERE group_table_id = level_id;

   EXECUTE IMMEDIATE    'UPDATE '
                     || v_table
                     || '
               SET method_status = 3 WHERE '
                     || v_id_field
                     || ' = '
                     || member_id;

   COMMIT;

   END;


   PROCEDURE prc_lock_users
   IS
/******************************************************************************
   NAME:       JS_LOCK_USERS
   PURPOSE:    To lock users out of Demantra when processing Batch Jobs
   NOTE:        Must be review when 7.3.1 is applied

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        09/09/2010  Luke Pocock - Red Rock Consulting
   1.1        16/09/2010  Luke Pocock - Red Rock Consulting - Filter Super Users

******************************************************************************/
   maxdate       DATE;
   midmin        DATE;
   mindate       DATE;
   sql_str       VARCHAR2 (30000);

   v_schema_id          INTEGER;
   vi_server            VARCHAR2 (100);
   vi_user              VARCHAR2 (100);
   vi_password          VARCHAR2 (100);
   vs_appserverurl      VARCHAR2 (200);
   vs_devappserverurl   VARCHAR2 (200);
   v_schema             VARCHAR2 (2000);
   v_server             VARCHAR2 (2000);
   v_user               VARCHAR2 (2000);
   v_password           VARCHAR2 (2000);
   servlet              VARCHAR2 (2000);
   buff                 VARCHAR2 (2000);

    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_LOCK_USERS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


    UPDATE user_id
    SET is_locked = 1
    WHERE user_name NOT IN ('dm','ptp','rburton_ptp', 'rburton_dm');
    --not in (select user_name from rr_user_id );--

    COMMIT;

    get_param ('sys_params', 'AppServerURL', vs_appserverurl);
    get_param ('sys_params', 'DevAppServerURL', vs_devappserverurl);

    FOR rec IN (SELECT user_id FROM user_id)
    LOOP

      --dbms_output.put_line(v_server);
      vi_server := NVL (v_server, NVL (vs_appserverurl, vs_devappserverurl));
      servlet := 'NotificationServlet?NotificationType=2&UAK='|| encryption.get_uak('ptp') || '&userID=' || rec.user_id  || '&operation=update';

      SELECT UTL_HTTP.request (vi_server || '/' || servlet)
        INTO buff
        FROM DUAL;

    END LOOP;


    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

  END;

  PROCEDURE prc_unlock_users
   IS
/******************************************************************************
   NAME:       JS_LOCK_USERS
   PURPOSE:    To lock users out of Demantra when processing Batch Jobs
   NOTE:        Must be review when 7.3.1 is applied

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        09/09/2010  Luke Pocock - Red Rock Consulting
   1.1        16/09/2010  Luke Pocock - Red Rock Consulting - Filter Super Users

******************************************************************************/
   maxdate       DATE;
   midmin        DATE;
   mindate       DATE;
   sql_str       VARCHAR2 (30000);

   v_schema_id          INTEGER;
   vi_server            VARCHAR2 (100);
   vi_user              VARCHAR2 (100);
   vi_password          VARCHAR2 (100);
   vs_appserverurl      VARCHAR2 (200);
   vs_devappserverurl   VARCHAR2 (200);
   v_schema             VARCHAR2 (2000);
   v_server             VARCHAR2 (2000);
   v_user               VARCHAR2 (2000);
   v_password           VARCHAR2 (2000);
   servlet              VARCHAR2 (2000);
   buff                 VARCHAR2 (2000);

    v_prog_name        VARCHAR2 (100);
    v_status           VARCHAR2 (100);
    v_proc_log_id      NUMBER;

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_UNLOCK_USERS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


    UPDATE user_id
    SET is_locked = 0
    WHERE user_name not in (select user_name from rr_user_id );--NOT IN ('dm','ptp','rburton_ptp', 'rburton_dm');

    COMMIT;

    get_param ('sys_params', 'AppServerURL', vs_appserverurl);
    get_param ('sys_params', 'DevAppServerURL', vs_devappserverurl);

    FOR rec IN (SELECT user_id FROM user_id)
    LOOP

      --dbms_output.put_line(v_server);
      vi_server := NVL (v_server, NVL (vs_appserverurl, vs_devappserverurl));
      servlet := 'NotificationServlet?NotificationType=2&UAK='|| encryption.get_uak('ptp') || '&userID=' || rec.user_id  || '&operation=update';

      SELECT UTL_HTTP.request (vi_server || '/' || servlet)
        INTO buff
        FROM DUAL;

    END LOOP;


    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
    WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

  END;

  PROCEDURE prc_disconnect_sessions
  IS
/**************************************************************************
   NAME:       PRC_DISCONNECT_SESSIONS
   PURPOSE:    Disconnects Toad and SQL Developer Sessions from Schema

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  -------------------------------------------------
   1.0        09/09/2011  Luke Pocock/Red Rock Consulting - Initial Code


**************************************************************************/

   v_schema      VARCHAR2 (100) := 'SM';
   v_program_1   VARCHAR2 (100) := 'toad.exe';
   v_program_2   VARCHAR2 (100) := 'SQL Developer';
   v_prog_name        VARCHAR2 (100);
   v_status           VARCHAR2 (100);
   v_proc_log_id      NUMBER;

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_DISCONNECT_SESSIONS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);


   FOR rec IN (SELECT s.inst_id, s.SID, s.serial#, p.spid, s.username,
                      s.program
                 FROM gv$session s JOIN gv$process p
                      ON p.addr = s.paddr AND p.inst_id = s.inst_id
                WHERE s.program in (v_program_1,v_program_2)
                  AND s.username = v_schema)
   LOOP
      dynamic_ddl (   'ALTER SYSTEM KILL SESSION '''
                   || rec.SID
                   || ','
                   || rec.serial#
                   || ''' IMMEDIATE'
                  );
   END LOOP;

   COMMIT;

   v_status := 'end ';
   v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

   EXCEPTION
   WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;
  END;

PROCEDURE prc_update_mdp_matrix_dates
IS

   v_status           VARCHAR2 (100);
   v_proc_log_id      NUMBER;

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_UPDATE_MDP_MATRIX_DATES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

  MERGE /*+ FULL(mdp) PARALLEL(mdp, 16) */ INTO mdp_matrix mdp
  USING ( SELECT /*+ FULL(sd) PARALLEL(sd, 16) */ item_id, location_id,
          MIN(sales_date) from_date,
          MAX(sales_date) until_date
          FROM sales_data sd
          GROUP BY item_id, location_id) mdp1
  ON ( mdp.item_id = mdp1.item_id
  AND mdp.location_id = mdp1.location_id)
  WHEN MATCHED THEN
  UPDATE SET mdp.from_date = mdp1.from_date, mdp.until_date = mdp1.until_date;

  COMMIT;

 /* MERGE INTO mdp_matrix mdp
  USING ( SELECT + FULL(mdp) PARALLEL(mdp, 32)  mdp.item_id, mdp.location_id,
          ADD_MONTHS(TRUNC(SYSDATE, 'MM'), 12) from_date,
          ADD_MONTHS(TRUNC(SYSDATE, 'YYYY'), 23) until_date
          FROM mdp_matrix mdp
          WHERE t_ep_e1_it_br_cat_6_id IN (22)
          AND (NVL(aop_yee_st_dt, TRUNC(SYSDATE, 'DD')) <> ADD_MONTHS(TRUNC(SYSDATE, 'MM'), 12) OR
               NVL(aop_yee_ed_dt, TRUNC(SYSDATE, 'DD')) <> ADD_MONTHS(TRUNC(SYSDATE, 'YYYY'), 23))
          ) mdp1
  ON ( mdp.item_id = mdp1.item_id
  AND mdp.location_id = mdp1.location_id)
  WHEN MATCHED THEN
  UPDATE SET mdp.aop_yee_st_dt = mdp1.from_date, mdp.aop_yee_ed_dt = mdp1.until_date;

  COMMIT;

  --rampallib: commented out the above
   */

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

  EXCEPTION
   WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;
END;

PROCEDURE prc_set_max_sales_date AS

   vs_server             VARCHAR2(2000);
   vs_err_msg            VARCHAR2(500);
   vs_sys_params_servlet VARCHAR2(1000);
   vs_id                 VARCHAR2(50);
   vs_uak                VARCHAR2(1000);
   vs_buff               VARCHAR2(2000);
   vs_servlet            VARCHAR2(1000);

    v_status           VARCHAR2 (100);
   v_proc_log_id      NUMBER;

   v_engine_week_lag   NUMBER := 7;
   v_max_scan_date     VARCHAR2(50):= TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 35, 'MM-DD-YYYY');

   BEGIN
    pre_logon;
    v_status  := 'start ';
    v_prog_name := 'PRC_SET_MAX_SALES_DATE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

  UPDATE sys_params SET pval = TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 14, 'MM-DD-YYYY') || ' 00:00:00' WHERE pname = 'max_sales_date';

  UPDATE sys_params SET pval = TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 7, 'MM-DD-YYYY') || ' 00:00:00' WHERE pname = 'min_fore_sales_date';

  UPDATE sys_params SET pval = v_max_scan_date || ' 00:00:00' WHERE pname = 'max_scan_date';

  UPDATE init_params_0 SET value_date = NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 14 - (v_engine_week_lag*7) WHERE pname = 'last_date_backup';

  COMMIT;

  ----UPDATE Function for MAX Scan Date---

 dynamic_ddl( 'create or replace FUNCTION GET_MAX_SCANDATE
   RETURN VARCHAR2
IS
   ret_date    VARCHAR2(20);
BEGIN

   ret_date := ''' || v_max_scan_date || ' 00:00:00'';

    RETURN ret_date;
END;');


   /*********************************/
   /* Notify the Application Server */
   /*********************************/
   get_param('sys_params','AppServerURL',vs_server);

   IF vs_server IS NULL THEN
      vs_err_msg := 'FAILURE: SYS_PARAMS parameter AppServerURL is not set';
      RAISE_APPLICATION_ERROR(-20000, vs_err_msg);
   END IF;

   get_param('sys_params','AppServerSysParamServlet',vs_sys_params_servlet);

   IF vs_sys_params_servlet IS NULL THEN
      vs_err_msg := 'FAILURE: SYS_PARAMS parameter AppServerSysParamServlet is not set';
      RAISE_APPLICATION_ERROR(-20000, vs_err_msg);
   END IF;

   /********************************/
   /* Get the UAK for the sge user */
   /********************************/

   vs_id := '0,';
   vs_uak := encryption.get_uak('sge');

   /************************************/
   /* Build the notification HTTP call */
   /************************************/

   vs_servlet := vs_sys_params_servlet ;
   vs_servlet := vs_servlet || vs_uak;

   BEGIN
      vs_buff := HELPER.get_response_from_url (vs_server || vs_servlet, v_prog_name, 'THROW_ERROR');
   EXCEPTION
      WHEN OTHERS THEN
         IF (sqlcode = -29273) THEN
             DB_WARN('Could not notify the applications server that the max sales date has changed.',v_prog_name);
         ELSE
            RAISE;
         END IF;
   END;

   /**********************************************************/
   /* Check the message returned from the Application Server */
   /**********************************************************/
   IF vs_buff <> '1' and vs_buff IS NOT NULL THEN

         vs_err_msg := 'FAILURE: AppServ Notification for SYS_PARAMS Failed Return value: ' || vs_buff;

         RAISE_APPLICATION_ERROR(-20003, vs_err_msg);

   END IF;

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
   WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

END;

PROCEDURE prc_set_engine_date AS

   vs_server             VARCHAR2(2000);
   vs_err_msg            VARCHAR2(500);
   vs_sys_params_servlet VARCHAR2(1000);
   vs_id                 VARCHAR2(50);
   vs_uak                VARCHAR2(1000);
   vs_buff               VARCHAR2(2000);
   vs_servlet            VARCHAR2(1000);

   V_Status           Varchar2 (100);
   V_Proc_Log_Id       Number;

   v_engine_week_lag   NUMBER := 7;

   BEGIN
    pre_logon;
    V_Status  := 'start ';
    v_prog_name := 'PRC_SET_ENGINE_DATE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

  UPDATE sys_params SET pval = TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 14 - (v_engine_week_lag*7), 'MM-DD-YYYY') || ' 00:00:00' WHERE pname = 'max_sales_date';

  UPDATE sys_params SET pval = TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 7 - (v_engine_week_lag*7), 'MM-DD-YYYY') || ' 00:00:00' WHERE pname = 'min_fore_sales_date';

  UPDATE init_params_0 SET value_date = NEXT_DAY(TRUNC(SYSDATE, 'DD') + 1, 'MONDAY') - 14 - (v_engine_week_lag*7) WHERE pname = 'last_date_backup';

  COMMIT;

   /*********************************/
   /* Notify the Application Server */
   /*********************************/
   get_param('sys_params','AppServerURL',vs_server);

   IF vs_server IS NULL THEN
      vs_err_msg := 'FAILURE: SYS_PARAMS parameter AppServerURL is not set';
      RAISE_APPLICATION_ERROR(-20000, vs_err_msg);
   END IF;

   get_param('sys_params','AppServerSysParamServlet',vs_sys_params_servlet);

   IF vs_sys_params_servlet IS NULL THEN
      vs_err_msg := 'FAILURE: SYS_PARAMS parameter AppServerSysParamServlet is not set';
      RAISE_APPLICATION_ERROR(-20000, vs_err_msg);
   END IF;

   /********************************/
   /* Get the UAK for the sge user */
   /********************************/

   vs_id := '0,';
   vs_uak := encryption.get_uak('sge');

   /************************************/
   /* Build the notification HTTP call */
   /************************************/

   vs_servlet := vs_sys_params_servlet ;
   vs_servlet := vs_servlet || vs_uak;

   BEGIN
      vs_buff := HELPER.get_response_from_url (vs_server || vs_servlet, v_prog_name, 'THROW_ERROR');
   EXCEPTION
      WHEN OTHERS THEN
         IF (sqlcode = -29273) THEN
             DB_WARN('Could not notify the applications server that the max sales date has changed.',v_prog_name);
         ELSE
            RAISE;
         END IF;
   END;

   /**********************************************************/
   /* Check the message returned from the Application Server */
   /**********************************************************/
   IF vs_buff <> '1' and vs_buff IS NOT NULL THEN

         vs_err_msg := 'FAILURE: AppServ Notification for SYS_PARAMS Failed Return value: ' || vs_buff;

         RAISE_APPLICATION_ERROR(-20003, vs_err_msg);

   END IF;

  v_status := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
   WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

END;

PROCEDURE prc_rebuild_tables AS

   vs_server             VARCHAR2(2000);
   vs_err_msg            VARCHAR2(500);
   vs_sys_params_servlet VARCHAR2(1000);
   vs_id                 VARCHAR2(50);
   vs_uak                VARCHAR2(1000);
   vs_buff               VARCHAR2(2000);
   vs_servlet            VARCHAR2(1000);

   V_Status           Varchar2 (100);
   V_Proc_Log_Id       Number;

   v_engine_week_lag   NUMBER := 7;

   BEGIN
    pre_logon;
    V_Status  := 'start ';
    v_prog_name := 'PRC_REBUILD_TABLES';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

-- cmos 02-Mar-2017 (px stats gathering)
-- cmos 07-Mar-2017 (no need to gather index stats after rebuilding indexes)
     rebuild_indexes('PROMOTION_DATA');
 dbms_stats.GATHER_TABLE_STATS (OWNNAME => NULL, TABNAME => 'PROMOTION_DATA', estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE, CASCADE => false, degree=> 16);

    rebuild_indexes('SALES_DATA');
 DBMS_STATS.GATHER_TABLE_STATS (OWNNAME => null, TABNAME => 'SALES_DATA', ESTIMATE_PERCENT => DBMS_STATS.AUTO_SAMPLE_SIZE, cascade => false, degree=> 16);

  REBUILD_INDEXES('ACCRUAL_DATA');
 dbms_stats.GATHER_TABLE_STATS (OWNNAME => NULL, TABNAME => 'ACCRUAL_DATA', estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE, CASCADE => false, degree=> 16);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);


    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

EXCEPTION
   WHEN OTHERS THEN
        BEGIN
        v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
        RAISE;
        END;

END;

END;

/
