--------------------------------------------------------
--  DDL for Package Body PKG_CONTRACTS_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_CONTRACTS_LM" 
AS
  /******************************************************************************
  NAME:       PKG_PTP
  PURPOSE:    All procedures commanly used for claims module
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
  1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version
  ******************************************************************************/

PROCEDURE prc_create_contract(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;
  v_new_contract_id NUMBER;
  v_c_level         NUMBER;
  v_c_level_1       NUMBER;
  v_c_level_2       NUMBER;
  v_c_level_3       NUMBER;
  v_c_level_4       NUMBER;
  v_C_SALE_ORG      NUMBER;
  v_payment_frequency    NUMBER;
  v_contract_vol    NUMBER;
  v_contract_vol_mix NUMBER;
  v_period_start    DATE;
  v_period_end      DATE;
  v_filter_id       NUMBER;
  v_chk             NUMBER :=0;
  -- Sanjiiv for CR3B 
  v_contract_notes  VARCHAR2(200); 
begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_CREATE_CONTRACT';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
 
----Get all new contract details
 select 
    period_start_new,period_end_new,nvl(c_level_1,0),nvl(c_level_2,0),
    nvl(c_level_3,0),nvl(c_level_4,0),nvl(c_level,0),nvl(C_SALE_ORG,0),
    contract_vol_new,contract_vol_mix_new,contract_notes                        -- Sanjiiv CR3B
    INTO  v_period_start,v_period_end,v_c_level_1,v_c_level_2,
     v_c_level_3,v_c_level_4,v_c_level,v_C_SALE_ORG,
     v_contract_vol,v_contract_vol_mix,v_contract_notes                            -- Sanjiiv CR3B
    from contract_group where contract_group_id = member_id; 
    
    
 --------Validation-------------
 
  if v_period_start >= v_period_end then
     v_chk := 1;
     
     update contract_group 
        set method_status = 14   
      WHERE contract_group_id = member_id;
          
      commit;
  end if;
 ---------End Validation-------
    
  if v_chk = 0 then  

-----Get new Contract Parent ID
  select contract_group_seq.nextval into v_new_contract_id from dual;

insert into contract_group
 (contract_group_id,contract_group_code,contract_group_desc,
 period_start,period_end,t_cont_lvl1_ep_id,t_cont_lvl2_ep_id,
 t_cont_lvl3_ep_id,t_cont_lvl4_ep_id,t_cont_lvl_ep_id,t_cont_lvl5_ep_id,
 recipient_cl3,recipient_cl4,
contract_vol,contract_vol_mix,created_user,last_update_date,rr_contracts_notes                -- Sanjiiv CR3B
)values
 ( v_new_contract_id,v_new_contract_id,to_char(v_period_start,'yyyy-mm-dd') || ' : ' ||v_new_contract_id,
 v_period_start,v_period_end,nvl(v_c_level_1,0),nvl(v_c_level_2,0),
 nvl(v_c_level_3,0),nvl(v_c_level_4,0),nvl(v_c_level,0),nvl(v_C_SALE_ORG,0),
 case nvl(v_c_level_3,0) when 0 then null else nvl(v_c_level_3,0) end,case nvl(v_c_level_4,0) when 0 then null else nvl(v_c_level_4,0) end,
 v_contract_vol,v_contract_vol_mix,user_id,sysdate,v_contract_notes)                        -- Sanjiiv CR3B
 ; 
      
  COMMIT;
  
---Update any missing Higher levels

  MERGE INTO contract_group c using
  (
     SELECT max(t_ep_L_ATT_2_EP_ID) t_cont_lvl1_ep_id, max(t_ep_e1_cust_city_EP_ID) t_cont_lvl2_ep_id, max(t_ep_ebs_customer_EP_ID) t_cont_lvl3_ep_id, 
            max(t_ep_ebs_account_EP_ID) t_cont_lvl4_ep_id
       FROM LOCATION
      WHERE t_ep_e1_cust_cat_4_EP_ID = nvl(v_c_level,0)
        AND nvl(v_c_level,0) <> 0
  )a
  ON(c.contract_group_id = v_new_contract_id)
  WHEN MATCHED THEN UPDATE
  SET c.t_cont_lvl1_ep_id = a.t_cont_lvl1_ep_id,
      c.t_cont_lvl2_ep_id = a.t_cont_lvl2_ep_id,
      c.t_cont_lvl3_ep_id = a.t_cont_lvl3_ep_id,
      c.t_cont_lvl4_ep_id = a.t_cont_lvl4_ep_id
  WHERE c.t_cont_lvl_ep_id <> 0
     AND (nvl(c.t_cont_lvl1_ep_id,0) = 0 
     OR nvl(c.t_cont_lvl2_ep_id,0) = 0
     OR nvl(c.t_cont_lvl3_ep_id,0) = 0
     OR nvl(c.t_cont_lvl4_ep_id,0) = 0);
     
  MERGE INTO contract_group c using
  (
     SELECT max(t_ep_L_ATT_2_EP_ID) t_cont_lvl1_ep_id, max(t_ep_e1_cust_city_EP_ID) t_cont_lvl2_ep_id, max(t_ep_ebs_customer_EP_ID) t_cont_lvl3_ep_id
       FROM LOCATION
      WHERE t_ep_ebs_account_EP_ID = nvl(v_c_level_4,0)
        AND nvl(v_c_level_4,0) <> 0
  )a
  ON(c.contract_group_id = v_new_contract_id)
  WHEN MATCHED THEN UPDATE
  SET c.t_cont_lvl1_ep_id = a.t_cont_lvl1_ep_id,
      c.t_cont_lvl2_ep_id = a.t_cont_lvl2_ep_id,
      c.t_cont_lvl3_ep_id = a.t_cont_lvl3_ep_id
  WHERE c.t_cont_lvl4_ep_id <> 0
     AND (nvl(c.t_cont_lvl1_ep_id,0) = 0 
     OR nvl(c.t_cont_lvl2_ep_id,0) = 0
     OR nvl(c.t_cont_lvl3_ep_id,0) = 0);
 
   MERGE INTO contract_group c using
  (
     SELECT max(t_ep_L_ATT_2_EP_ID) t_cont_lvl1_ep_id, max(t_ep_e1_cust_city_EP_ID) t_cont_lvl2_ep_id
       FROM LOCATION
      WHERE t_ep_ebs_customer_EP_ID = nvl(v_c_level_3,0)
        AND nvl(v_c_level_3,0) <> 0
  )a
  ON(c.contract_group_id = v_new_contract_id)
  WHEN MATCHED THEN UPDATE
  SET c.t_cont_lvl1_ep_id = a.t_cont_lvl1_ep_id,
      c.t_cont_lvl2_ep_id = a.t_cont_lvl2_ep_id
  WHERE c.t_cont_lvl3_ep_id <> 0
     AND (nvl(c.t_cont_lvl1_ep_id,0) = 0 
     OR nvl(c.t_cont_lvl2_ep_id,0) = 0); 
     
  MERGE INTO contract_group c using
  (
     SELECT max(t_ep_L_ATT_2_EP_ID) t_cont_lvl1_ep_id
       FROM LOCATION
      WHERE t_ep_e1_cust_city_EP_ID = nvl(v_c_level_2,0)
        AND nvl(v_c_level_2,0) <> 0
  )a
  ON(c.contract_group_id = v_new_contract_id)
  WHEN MATCHED THEN UPDATE
  SET c.t_cont_lvl1_ep_id = a.t_cont_lvl1_ep_id
  WHERE c.t_cont_lvl2_ep_id <> 0
     AND (nvl(c.t_cont_lvl1_ep_id,0) = 0);
     
    -- Sanjiiv CR3B -- Copy the contract notes to new contract group created..
    UPDATE CONTRACT_GROUP
    SET    contract_notes    = (select contract_notes from CONTRACT_GROUP where contract_group_id = member_id)
    WHERE  contract_group_id = v_new_contract_id;  
     
     COMMIT;

----Get the new level ID's----------

 select 
    nvl(t_cont_lvl1_ep_id,0),nvl(t_cont_lvl2_ep_id,0),
    nvl(t_cont_lvl3_ep_id,0),nvl(t_cont_lvl4_ep_id,0),nvl(t_cont_lvl_ep_id,0)
    INTO v_c_level_1,v_c_level_2,
     v_c_level_3,v_c_level_4,v_c_level
    from contract_group where contract_group_id = v_new_contract_id; 

  
  --Insert Population Details
  INSERT INTO INVESTMENT_SETUP_DATES (contract_group_id,from_date,until_date)
  values(v_new_contract_id,v_period_start,v_period_end);
    COMMIT;
    
  -------------------------------------------------------------
  ----Populate SALES ORG ans Sales distribution as a default---
  ----Remove when NEW SALES orgs apply----
  -------------------------------------------------------------
  select investment_setup_levels_seq.nextval into v_filter_id from dual;
  
  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(v_new_contract_id,6,v_filter_id,1);
  
    commit;
    
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,1);
  
    commit;
  
  select investment_setup_levels_seq.nextval into v_filter_id from dual;

  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(v_new_contract_id,7,v_filter_id,2);
  
    commit;
  
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,1);
  
    commit;
    -----Active Customer
  select investment_setup_levels_seq.nextval into v_filter_id from dual;

  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(v_new_contract_id,367,v_filter_id,3);
  
    commit;
  
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,4);
  
    commit;
    
 -----Active Product
  select investment_setup_levels_seq.nextval into v_filter_id from dual;

  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(v_new_contract_id,360,v_filter_id,4);
  
    commit;
  
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,4);
  
    commit;


    
----Create Fictive Line

INSERT into contract_line (contract_line_id,contract_line_code,contract_line_desc,is_fictive,contract_id,contract_payment_id,contract_stat_id,contract_group_id,
 t_cont_lvl1_ep_id,t_cont_lvl2_ep_id, t_cont_lvl3_ep_id,t_cont_lvl4_ep_id,t_cont_lvl5_ep_id,t_cont_lvl_ep_id,last_update_date) values
(contract_line_seq.nextval,contract_line_seq.nextval,'Fictive',1,0,0,1,v_new_contract_id,
 nvl(v_c_level_1,0),nvl(v_c_level_2,0), nvl(v_c_level_3,0),nvl(v_c_level_4,0),nvl(v_C_SALE_ORG,0),nvl(v_c_level,0),sysdate);

commit;

---Clear Attribute

   update contract_group 
      set period_start_new = null,
          period_end_new = null,
          c_level_1 = null,
          c_level_2 = null,
          c_level_3 = null,
          c_level_4 = null,
          c_level = null,
          C_SALE_ORG = null,
          contract_vol_new = null,
          contract_vol_mix_new = null,
          last_update_date = null,
          contract_notes = null,                    -- Sanjiiv CR3B
          method_status = 3
    WHERE contract_group_id = member_id; 
          
          commit;
  
  end if;
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_create_contract;

PROCEDURE prc_create_investment(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;
  v_new_cont_id     NUMBER;
  v_method_status   NUMBER := 0;
  v_invest_start    DATE;
  v_recipient_chk   NUMBER :=0;
  v_sponsor_chk     NUMBER :=0;
  v_oi_flag         NUMBER :=0;
  v_trading_allow_chk        NUMBER :=0;                -- Sanjiiv CR4
begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_CREATE_INVESTMENT';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
  
   
   select contract_seq.nextval into v_new_cont_id from dual;
   
   ----------------------------
   --Recipient Validation-----
   ---------------------------
   select count(1) into v_recipient_chk 
   from contract_group c, comp_type t 
   where c.comp_type_id = t.comp_type_id 
   and c.contract_group_id = member_id
   and recipient is null 
   and t.recipient_req = 1;
   
   ----------------------------
   --Sponshoship Validation-----
   ---------------------------
   select count(1) into v_sponsor_chk  
   from contract_group c
   where c.contract_group_id = member_id
   and c.comp_type_id <> 7
   or c.comp_type_id = 7
   and c.contract_group_id = member_id
   and (SUBSTR(investment_details, 1, 2) = 'Z:'
   or SUBSTR(investment_details, 1, 2) = 'T:'
   or SUBSTR(investment_details, 1, 2) = 'C:'); 
   
    ----------------------------
   --Trading Allowance Validation----- Sanjiiv CR4
   -- If Investment Type other than - Trading Allowance (%) (OI) - ZBG% & Trading Allowance ($) (OI) - ZBG$ DONT have SAP investment desc then flag it as Validation error
   ---------------------------
   select count(1) into v_trading_allow_chk
   from contract_group c
   where c.contract_group_id = member_id
   and c.comp_type_id IN (1,2)
   or c.comp_type_id NOT IN (1,2)
   and c.contract_group_id = member_id
   and investment_details is NOT NULL;
   
   if v_recipient_chk = 0 and v_sponsor_chk > 0 and v_trading_allow_chk > 0 then                 -- -- Sanjiiv CR4
   
    select from_date into v_invest_start from INVESTMENT_SETUP_DATES where contract_group_id = member_id;
    select oi_flag into v_oi_flag from comp_type where comp_type_id in (select comp_type_id from contract_group where contract_group_id = member_id)  ;
      

    INSERT into contract (contract_id,contract_code,contract_desc,is_fictive,
   new_contract_group_id,comp_type_seq_id,comp_type_id,rebate_value,payment_frequency,
   recipient_cl3,recipient_cl4,recipient,recipient_sales_office,payment_comments,tax_consideration,investment_details,created_user)
   select v_new_cont_id,v_new_cont_id, to_char(v_invest_start,'yyyy-mm-dd') || ' : ' || v_new_cont_id,0,
   contract_group_id,comp_type_seq_id,comp_type_id,rebate_value,payment_frequency,
   recipient_cl3,recipient_cl4,decode(v_oi_flag,1,null,recipient),recipient_sales_office,payment_comments,tax_consideration,investment_details, user_id
   from contract_group where contract_group_id = member_id;
   

    commit;
  
    --Crate the Contract Lines
    prc_create_investment_lines( user_id, 4263, v_new_cont_id,v_method_status);
  
    ---If Unsuccessfull, remove investment
    if v_method_status <> 3 then
       delete from contract where contract_id = v_new_cont_id;
       commit;
    end if;
  elsif v_sponsor_chk = 0  then
    v_method_status:= 18;  -- Fail for Sponsorhsip
  elsif v_trading_allow_chk = 0  then                            -- Sanjiiv CR4
    v_method_status:= 19;  -- Fail for Trading Allowance
  else
    v_method_status:= 13;  -- Fail for Recipient
  end if;
  
  update contract_group
  set method_status = v_method_status,rebate_value = null,  last_update_date = sysdate
  where contract_group_id = member_id;
  
  commit;
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_create_investment;

PROCEDURE prc_create_investment_lines(
    user_id   NUMBER DEFAULT NULL,
    level_id  NUMBER DEFAULT NULL,
    member_id NUMBER DEFAULT NULL,
    method_status OUT NUMBER)
AS
  v_prog_name   VARCHAR2 (100);
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_ttl_glob_fact NUMBER;
  rec contract%rowtype;
  seq access_seq%rowtype;
  cgp contract_group%rowtype;
  
  v_pc_date_chk    NUMBER :=0;
  v_pc_lvl_chk     NUMBER :=1;
  v_cont_v_inv     NUMBER :=0;
  v_insert_sql     VARCHAR2(32000);
  v_select_sql     VARCHAR2(32000);
  V_SUB_SELECT_SQL varchar2(32000);
  v_where_sql      VARCHAR2(32000);
  V_INPUTS_COL     varchar2(100);
  V_DEAL_TYPE      number;
  v_cnt            NUMBER;
  v_line_id        NUMBER;
  v_ff_val         NUMBER;  
  v_amortize_val   NUMBER;
  v_amortize       NUMBER := 0;
  
  v_sequence_ep_id_desc VARCHAR2(32000);
  v_sequence_ep_id_merge VARCHAR2(32000);
  v_ep_id_desc VARCHAR2(400);
  v_member_value      VARCHAR2(32000);
  sql_str VARCHAR2(32000);
  
BEGIN
  pre_logon;
  v_status      := 'start ';
  v_prog_name   := 'PRC_CREATE_INVESTMENT_LINES';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE,  level_id, member_id, user_id);
  
  
  --Get all Contract Row Data
  SELECT * INTO rec FROM contract WHERE contract_id = member_id;
  
  select * into seq from access_seq where access_seq_id in (select max(access_seq_id) from comp_type_seq where comp_type_seq_id = rec.comp_type_seq_id);
  
  SELECT * INTO cgp FROM contract_group WHERE contract_group_id = rec.new_contract_group_id;

  SELECT pval into v_amortize_val from sys_params where pname = 'cont_lump_amortize_amt'; 

------------------------------------------
---- Investment Date Validation ----------
-----------------------------------------

  ---Check Investment Dates are between Contract Dates
  select count(1) into v_pc_date_chk 
  from INVESTMENT_SETUP_DATES d, contract_group g 
  where d.contract_group_id = rec.new_contract_group_id 
  and d.contract_group_id = g.contract_group_id
  and d.from_date >= g.period_start
  and d.until_date <= g.period_end;
   
------------------------------------------------
---- Access Sequence Level Validation ----------
------------------------------------------------

  if nvl(rec.comp_type_seq_id,0) <> 0
  then
        if seq.level1 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level1 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level2 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level2 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level3 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level3 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level4 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level4 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level5 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level5 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
       if seq.level6 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level6 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;  
        if seq.level7 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level7 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level8 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level8 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level9 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level9 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
        if seq.level10 is not null
        then
          select count(1) into v_cnt from investment_setup_levels where level_id = seq.level10 and contract_group_id = rec.new_contract_group_id;
          if v_cnt = 0 then
            v_pc_lvl_chk := 0;
          end if;
        end if;
  end if;
  
----------------------------------------------------------------
---- Validate Contract Customer vs Investment Details ----------
----------------------------------------------------------------
  

  
  if nvl(rec.comp_type_seq_id,0) <> 0 and v_pc_date_chk <> 0 and v_pc_lvl_chk <> 0
  then
    ---Get Population condition
    -- CR20-1 NA combinations -- Sanjiiv on 25052017  
    -- FOR rec1 IN (select distinct g.ID_FIELD,filter_id from investment_setup_levels pl, GROUP_TABLES g
    FOR rec1 IN (select distinct g.ID_FIELD,filter_id,level_id from investment_setup_levels pl, GROUP_TABLES g
                  where pl.LEVEL_ID = g.group_table_id
                  and pl.contract_group_id = rec.new_contract_group_id
    )
    LOOP
    ----Create the IN statement to filter MDP_MATRIX
      v_where_sql:= v_where_sql ||  ' AND '|| rec1.id_field || ' in ( -1';
          /*
          FOR rec2 IN (select distinct member_id 
                        from investment_setup_MEMBERS 
                       where filter_id = rec1.filter_id)
          LOOP  
          */        
           -- Changed Sanjiiv for the CR change members with NO 0 and NA
          FOR rec2 IN (select distinct member_id 
                        from investment_setup_MEMBERS a
                        where  filter_id = rec1.filter_id
                        and not exists (select 1 from RR_EXCLUDE_EP_IDS B where b.level_id = rec1.level_id and a.member_id = b.ep_id))
          LOOP    
          
              v_where_sql := v_where_sql ||  ',' || rec2.member_id;
          END LOOP;
      v_where_sql  :=  v_where_sql  ||  ')';
     end loop;
     
     check_and_drop('v_tmp_cl_comb_t_' || user_id);
     DYNAMIC_DDL('create table v_tmp_cl_comb_t_' || user_id ||  ' as
                  select 1 cnt from dual where 1=2');
--     DYNAMIC_DDL_ret_rows('insert into v_tmp_cl_comb_t_' || user_id ||'
--                   select 1 cnt 
--                    FROM mdp_matrix m,contract_group c
--                    where c.contract_group_id = '||rec.new_contract_group_id ||'
--                      and c.t_cont_lvl_ep_id = case nvl(c.t_cont_lvl_ep_id,0) when 0 then t_cont_lvl_ep_id else m.t_ep_e1_cust_cat_4_EP_ID end
--                      and c.t_cont_lvl1_ep_id = case nvl(c.t_cont_lvl1_ep_id,0) when 0 then t_cont_lvl1_ep_id else m.t_ep_L_ATT_2_EP_ID end
--                      and c.t_cont_lvl2_ep_id = case nvl(c.t_cont_lvl2_ep_id,0) when 0 then t_cont_lvl2_ep_id else m.t_ep_L_ATT_6_EP_ID end
--                      and c.t_cont_lvl3_ep_id = case nvl(c.t_cont_lvl3_ep_id,0) when 0 then t_cont_lvl3_ep_id else m.t_ep_ebs_customer_EP_ID end
--                      and c.t_cont_lvl4_ep_id = case nvl(c.t_cont_lvl4_ep_id,0) when 0 then t_cont_lvl4_ep_id else m.t_ep_ebs_account_EP_ID end' || v_where_sql,
--                      null,null,null,v_cont_v_inv);
  
       DYNAMIC_DDL_ret_rows('insert into v_tmp_cl_comb_t_' || user_id ||'
                   select 1 cnt 
                     FROM location m, items l, contract_group c
                    where c.contract_group_id = '||rec.new_contract_group_id ||'
                      and c.t_cont_lvl_ep_id = case nvl(c.t_cont_lvl_ep_id,0) when 0 then t_cont_lvl_ep_id else m.t_ep_e1_cust_cat_4_EP_ID end
                      and c.t_cont_lvl1_ep_id = case nvl(c.t_cont_lvl1_ep_id,0) when 0 then t_cont_lvl1_ep_id else m.t_ep_L_ATT_2_EP_ID end
                      and c.t_cont_lvl2_ep_id = case nvl(c.t_cont_lvl2_ep_id,0) when 0 then t_cont_lvl2_ep_id else m.t_ep_L_ATT_6_EP_ID end
                      and c.t_cont_lvl3_ep_id = case nvl(c.t_cont_lvl3_ep_id,0) when 0 then t_cont_lvl3_ep_id else m.t_ep_ebs_customer_EP_ID end
                      and c.t_cont_lvl4_ep_id = case nvl(c.t_cont_lvl4_ep_id,0) when 0 then t_cont_lvl4_ep_id else m.t_ep_ebs_account_EP_ID end' || v_where_sql,
                      null,null,null,v_cont_v_inv);  
  
  end if;
     
     v_cont_v_inv :=1;  ---Temporary Fix
   
-------------------------------------
---- Begin Create Process  ----------
-------------------------------------
    if (v_pc_date_chk <> 0 and v_pc_lvl_chk <> 0 and v_cont_v_inv <> 0)    then
    
    
    /*Delete Fictive contract Lines - Created When Contract is created on its own*/
   DELETE FROM CONTRACT_LINE WHERE CONTRACT_group_ID = rec.new_contract_group_id AND IS_FICTIVE = 1;
  COMMIT;
    
    
----------------------------
-- Get all Date combinations
----------------------------
  select INPUTS_COLUMN into V_INPUTS_COL from TBL_CONTRACT_PAY_FREQ where CONTRACT_PAY_FREQ_ID = REC.PAYMENT_FREQUENCY;

    check_and_drop('v_tmp_cl_dates_t_' || user_id);
    DYNAMIC_DDL('create table v_tmp_cl_dates_t_' || user_id || ' as
                 select  min(i.datet) start_date, last_day(max(i.datet)) end_date, min(i.datet) sales_date,
                 d.from_date invest_from_date,d.until_date invest_until_date, 0 contract_payment_id
                from inputs i,INVESTMENT_SETUP_DATES d 
                -- where trunc(i.datet,''dd'') between trunc(d.from_date,''dd'') and trunc(d.until_date,''dd'')
                where trunc(i.datet,''dd'') between GREATEST(TRUNC(d.from_date ,''MM''),NEXT_DAY(trunc(d.from_date,''dd'') ,''MONDAY'')-7) and trunc(d.until_date,''dd'')   -- From Luke Fix for Contract Start Date bug
                  and contract_group_id = '|| rec.new_contract_group_id ||'
                group by d.from_date,d.until_date,'|| v_inputs_col );

 commit;

   DYNAMIC_DDL('update v_tmp_cl_dates_t_' || user_id || '
  set CONTRACT_PAYMENT_ID = contract_payment_seq.nextval, 
  start_date = case when start_date > invest_from_date then start_date else invest_from_date end,
  end_date = case when end_date < invest_until_date then end_date else invest_until_date end
  ');
 
 commit;
 
 ----------------------------
-- Get all Customer/Product combinations
----------------------------

 ---Retrieve all of the EP_ID column names to be used below
  v_sequence_ep_id_desc := ''; 
        if seq.level1 is not null
        then
         select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level1;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level2 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level2;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level3 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level3;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level4 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level4;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level5 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level5;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
       if seq.level6 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level6;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;  
        if seq.level7 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level7;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level8 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level8;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level9 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level9;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;
        if seq.level10 is not null
        then
          select id_field into v_ep_id_desc from group_tables where group_table_id = seq.level10;
          v_sequence_ep_id_desc := v_sequence_ep_id_desc || ',' || v_ep_id_desc;
          v_sequence_ep_id_merge := v_sequence_ep_id_merge || ' and a.'|| v_ep_id_desc || ' = b.'|| v_ep_id_desc;
        end if;

 ---------The following code filters all combinations based on the Investement Setup population---------        
      --FOR rec1 IN (select distinct g.ID_FIELD,filter_id from investment_setup_levels pl, GROUP_TABLES g
      -- CR20-1 NA combinations -- Sanjiiv on 25052017
      FOR rec1 IN (select distinct g.ID_FIELD,filter_id,level_id from investment_setup_levels pl, GROUP_TABLES g
                  where pl.LEVEL_ID = g.group_table_id
                  and pl.contract_group_id = rec.new_contract_group_id
    )
    LOOP
    ----Create the IN statement to filter MDP_MATRIX
      v_where_sql:= v_where_sql ||  ' AND '|| rec1.id_field || ' in ( -1';
          /*
          FOR rec2 IN (select distinct member_id 
                        from investment_setup_MEMBERS 
                       where filter_id = rec1.filter_id)
          LOOP   
          */
          -- -- Changed Sanjiiv for the CR20-1 NA combinations
          FOR rec2 IN (select distinct member_id 
                        from investment_setup_MEMBERS a
                       where filter_id = rec1.filter_id
                       and not exists (select 1 from RR_EXCLUDE_EP_IDS B where b.level_id = rec1.level_id and a.member_id = b.ep_id))
          LOOP        
              v_where_sql := v_where_sql ||  ',' || rec2.member_id;
          END LOOP;
      v_where_sql  :=  v_where_sql  ||  ')';
     end loop;
     
    check_and_drop('v_tmp_cl_comb_t_' || user_id);
--    DYNAMIC_DDL('create table v_tmp_cl_comb_t_' || user_id ||  ' as
--                 select sum(glob_prop) glob_prop ' || v_sequence_ep_id_desc || ' 
--                   FROM mdp_matrix
--                   where 1=1' || v_where_sql || '
--                   group by 1 ' || v_sequence_ep_id_desc);
                   
    DYNAMIC_DDL('create table v_tmp_cl_comb_t_' || user_id ||  ' as
                 select 0 glob_prop ' || v_sequence_ep_id_desc || ' 
                   FROM location, items
                   where 1=1' || v_where_sql || '
                   group by 1 ' || v_sequence_ep_id_desc);
                   
    --Update Glob Prop               
    DYNAMIC_DDL('merge into v_tmp_cl_comb_t_' || user_id ||  ' a using (
                 select sum(glob_prop) glob_prop ' || v_sequence_ep_id_desc ||  
                  ' FROM mdp_matrix ' ||
                  ' where 1=1' || v_where_sql ||
                  ' group by 1 ' || v_sequence_ep_id_desc || 
                  ')b on ( 1 = 1 ' || v_sequence_ep_id_merge ||
                  ' )' ||
                  ' when matched then update '||
                  ' set a.glob_prop = b.glob_prop');                         
 
    commit; 
 ----------------Create SAP IDS-------------
 
 DYNAMIC_DDL('insert into contract_payment (contract_payment_id,contract_payment_code,is_fictive,contract_payment_desc,created_user)
              select distinct contract_payment_id,contract_payment_id,0,
              to_char(start_date,''yyyy-mm-dd'') || '' : '' || contract_payment_id,' || user_id || ' from v_tmp_cl_dates_t_' || user_id);
 
 commit;
 
 -------------------Insert into Contract Lines Table

    v_insert_sql := 'INSERT INTO contract_line
                    ( contract_line_id, contract_line_code, contract_line_desc,
                      contract_group_id, CONTRACT_ID, CONTRACT_PAYMENT_ID, CONTRACT_STAT_ID,is_fictive,
                      access_seq_id, COMP_TYPE_ID, COMP_TYPE_SEQ_ID, CREATED_USER,
                      start_date,end_date,t_cont_lvl1_ep_id,t_cont_lvl2_ep_id, t_cont_lvl3_ep_id,
                      t_cont_lvl4_ep_id,t_cont_lvl5_ep_id,t_cont_lvl_ep_id,prop_factor
                    ' || v_sequence_ep_id_desc || ')';
  
---------The following code filters all combinations based on the promotion population---------        
    v_select_sql :=  ' select contract_line_seq.nextval,contract_line_seq.nextval,to_char(d.start_date,''yyyy-mm-dd'') ||'' : '' ||contract_line_seq.nextval, ' || 
                       rec.new_contract_group_id ||', '|| member_id ||', d.contract_payment_id, 1,0, ' || 
                       seq.access_seq_id || ', ' || rec.COMP_TYPE_ID || ',' || rec.COMP_TYPE_SEQ_ID || ', '|| user_id || ',' ||
                       'd.start_date,d.end_date' || ',' || cgp.t_cont_lvl1_ep_id || ',' || cgp.t_cont_lvl2_ep_id || ',' || cgp.t_cont_lvl3_ep_id || ',' ||
                       cgp.t_cont_lvl4_ep_id || ',' || cgp.t_cont_lvl5_ep_id || ','|| cgp.t_cont_lvl_ep_id || ',glob_prop' ||
                       v_sequence_ep_id_desc || '
                      from v_tmp_cl_comb_t_' || user_id ||  ', v_tmp_cl_dates_t_' || user_id ||  ' d';
     v_where_sql :=  'where 1=1';
      V_SUB_SELECT_SQL :='';


    DYNAMIC_DDL(v_insert_sql || ' ' ||  v_select_sql || ' ' || v_where_sql);
     commit;

---------------------------------------     
-------Populate the Rebate Value-------
---------------------------------------

   v_cnt:=0;
  select DEAL_TYPE into V_DEAL_TYPE from COMP_TYPE where COMP_TYPE_ID = REC.COMP_TYPE_ID;
  
  SELECT COUNT(1),sum(PROP_FACTOR) INTO V_CNT,v_ttl_glob_fact FROM CONTRACT_LINE WHERE contract_id = member_id;
  
  ------If All Glob Prop is Zero, set all glob_fact to 1
  if v_ttl_glob_fact = 0 then
     update CONTRACT_LINE set prop_factor = 1 WHERE contract_id = member_id;
     commit;
     v_ttl_glob_fact := v_cnt;
  end if;
  
  if v_cnt <> 0 then 
  
  ----Fixed Fund 
  if V_DEAL_TYPE = 3 then
     ----Set Amortization rules if Sponsorship and is over threshold
     if v_amortize_val <= rec.rebate_value and rec.COMP_TYPE_ID = 7 then
         v_amortize := 1;
     end if;
     
 
  DYNAMIC_DDL('update CONTRACT_LINE SET lumpsum= round(' || rec.rebate_value || '*(prop_factor/' || v_ttl_glob_fact || '),2), RR_CL_AMORTIZE=' || v_amortize  || ', last_update_date = sysdate where contract_id = '||member_id);

  commit;
  
  --Clean up any rounding issues
  select sum(lumpsum),min(contract_line_id) into v_ff_val,v_line_id from contract_line where contract_id = member_id;

  update contract_line set lumpsum = lumpsum + (rec.rebate_value - v_ff_val) where contract_line_id = v_line_id;

  commit;
  
   ELSE
  ----Case Deal or Percentage
   DYNAMIC_DDL('update CONTRACT_LINE SET '||CASE WHEN V_DEAL_TYPE = 1 THEN ' case_deal_perc ' WHEN V_DEAL_TYPE = 2 THEN ' case_deal_amt '  END ||
  ' = '|| case when v_deal_type in (1) 
            then rec.rebate_value/100 
        when v_deal_type in (2) 
            then rec.rebate_value 
        end
        ||' , last_update_date = sysdate where contract_id = '||member_id);
     
     commit;
  
    end if;
  ---Clean Up-----
  check_and_drop('v_tmp_cl_dates_t_' || user_id );
  check_and_drop('v_tmp_cl_comb_t_' || user_id );
  
  method_status := 3; ---Success
  
  end if;
  
  else
  
  --------------------------
  ---Validation Errors------
  --------------------------
  
    if v_pc_date_chk = 0 then  --If Date Validation
      method_status := 10;
    elsif v_pc_lvl_chk = 0 then  -- Level Check
      method_status := 11;
    elsif v_cont_v_inv = 0 then  --Compare Contract to Investment
      method_status := 15;
    end if;
  
  end if;
    
  v_status      := 'end ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_create_investment_lines;

PROCEDURE prc_createas_contract_line(
    user_id   NUMBER DEFAULT NULL,
    level_id  NUMBER DEFAULT NULL,
    member_id NUMBER DEFAULT NULL)
AS
  v_prog_name   VARCHAR2 (100) := 'PRC_CREATEAS_CONTRACT_LINE';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
 
BEGIN
  pre_logon;
  v_status      := 'Start ';
  v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
  INSERT INTO CONTRACT_LINE(CONTRACT_LINE_ID,CONTRACT_LINE_CODE,CONTRACT_LINE_DESC,CONTRACT_ID,CONTRACT_PAYMENT_ID,
                            CONTRACT_STAT_ID,ACCESS_SEQ_ID,COMP_TYPE_ID,COMP_TYPE_SEQ_ID,T_EP_P4_EP_ID,T_EP_SITE_EP_ID,
                            T_EP_EBS_CUSTOMER_EP_ID,T_EP_P3_EP_ID,T_EP_E1_ITEM_CAT_4_EP_ID,T_EP_E1_ITEM_CAT_6_EP_ID,T_EP_EBS_PROD_CAT_EP_ID,
                            T_EP_E1_ITEM_CAT_7_EP_ID,T_EP_P2_EP_ID,T_EP_I_ATT_6_EP_ID,T_EP_E1_CUST_CAT_2_EP_ID,T_EP_ITEM_EP_ID,T_EP_I_ATT_10_EP_ID,
                            T_EP_EBS_ACCOUNT_EP_ID,created_user)
  SELECT CONTRACT_LINE_SEQ.NEXTVAL,CONTRACT_LINE_SEQ.CURRVAL,CONTRACT_LINE_SEQ.CURRVAL,CONTRACT_ID,CONTRACT_PAYMENT_ID,
                            CONTRACT_STAT_ID,ACCESS_SEQ_ID,COMP_TYPE_ID,COMP_TYPE_SEQ_ID,T_EP_P4_EP_ID,T_EP_SITE_EP_ID,
                            T_EP_EBS_CUSTOMER_EP_ID,T_EP_P3_EP_ID,T_EP_E1_ITEM_CAT_4_EP_ID,T_EP_E1_ITEM_CAT_6_EP_ID,T_EP_EBS_PROD_CAT_EP_ID,
                            T_EP_E1_ITEM_CAT_7_EP_ID,T_EP_P2_EP_ID,T_EP_I_ATT_6_EP_ID,T_EP_E1_CUST_CAT_2_EP_ID,T_EP_ITEM_EP_ID,T_EP_I_ATT_10_EP_ID,
                            T_EP_EBS_ACCOUNT_EP_ID, user_id
                  FROM CONTRACT_LINE where contract_line_id = member_id;
  COMMIT;                
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_createas_contract_line;

PROCEDURE prc_chg_cl_unplanned(
    user_id   NUMBER DEFAULT NULL,
    level_id  NUMBER DEFAULT NULL,
    member_id NUMBER DEFAULT NULL)
AS
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_UNPLANNED';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;

BEGIN
  pre_logon;
  v_status      := 'Start ';
  v_prog_name   := 'PRC_CHG_CL_UNPLANNED';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  if level_id = 4261 then ---Investment Line
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_line_id = member_id AND contract_stat_id in (2,3,4,9));
    
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 1 , LAST_UPDATE_DATE = SYSDATE, approved_user = null, approved_date = null
    WHERE contract_line_id = member_id AND contract_stat_id in (2,3,4,9)    
    AND CONTRACT_EXP_STAT_ID <> 1;
    COMMIT;         
  elsif level_id = 4263 then ---Investment
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_id = member_id AND contract_stat_id in (2,3,4,9) AND CONTRACT_EXP_STAT_ID <> 1);
  
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 1 , LAST_UPDATE_DATE = SYSDATE, approved_user = null, approved_date = null
    WHERE contract_id = member_id AND contract_stat_id in (2,3,4,9)    
    AND CONTRACT_EXP_STAT_ID <> 1;
    COMMIT;         
  elsif level_id = 4264 then ---SAP Payment
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_payment_id = member_id AND contract_stat_id in (2,3,4,9) AND CONTRACT_EXP_STAT_ID <> 1);
   
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 1 , LAST_UPDATE_DATE = SYSDATE, approved_user = null, approved_date = null
    WHERE contract_payment_id = member_id AND contract_stat_id in (2,3,4,9)    
    AND CONTRACT_EXP_STAT_ID <> 1;
    COMMIT;         
  elsif level_id = 4366 then ---Contract
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_group_id = member_id AND contract_stat_id in (2,3,4,9) AND CONTRACT_EXP_STAT_ID <> 1);
   
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 1 , LAST_UPDATE_DATE = SYSDATE, approved_user = null, approved_date = null
    WHERE contract_group_id = member_id AND contract_stat_id in (2,3,4,9)    
    AND CONTRACT_EXP_STAT_ID <> 1;
    COMMIT;         
  end if;
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_chg_cl_unplanned;

PROCEDURE prc_chg_cl_planned(
    user_id   NUMBER DEFAULT NULL,
    level_id  NUMBER DEFAULT NULL,
    member_id NUMBER DEFAULT NULL)
AS
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_PLANNED';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;

BEGIN
  pre_logon;
  v_status      := 'Start ';
  v_prog_name   := 'PRC_CHG_CL_PLANNED';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  if level_id = 4261 then ---Investment Line
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 2 , LAST_UPDATE_DATE = SYSDATE, planned_Date = sysdate , planned_user = user_id
    WHERE contract_line_id = member_id AND contract_stat_id in (1,9);
    COMMIT;         
  
  elsif level_id = 4263 then ---Investment
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 2 , LAST_UPDATE_DATE = SYSDATE, planned_Date = sysdate , planned_user = user_id
    WHERE contract_id = member_id AND contract_stat_id in (1,9);
    COMMIT;         
   elsif level_id = 4264 then ---SAP Payment
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 2 , LAST_UPDATE_DATE = SYSDATE, planned_Date = sysdate , planned_user = user_id
    WHERE contract_payment_id = member_id AND contract_stat_id in (1,9);
    COMMIT;         
   elsif level_id = 4366 then ---Contract
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 2 , LAST_UPDATE_DATE = SYSDATE, planned_Date = sysdate , planned_user = user_id
    WHERE contract_group_id = member_id AND contract_stat_id in (1,9);
    COMMIT;         
  end if;
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_chg_cl_planned;

PROCEDURE prc_chg_cl_req_approval(
    user_id   NUMBER DEFAULT NULL,
    level_id  NUMBER DEFAULT NULL,
    member_id NUMBER DEFAULT NULL)
AS
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_REQ_APPROVAL';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;

BEGIN
  pre_logon;
  v_status      := 'Start ';
  v_prog_name   := 'PRC_CHG_CL_REQ_APPROVAL';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  if level_id = 4261 then ---Investment Line
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 3 , LAST_UPDATE_DATE = SYSDATE, rfa_Date = sysdate , rfa_user = user_id
    WHERE contract_line_id = member_id AND contract_stat_id in (1,2,9);
    COMMIT;  
  
  elsif level_id = 4263 then ---Investment
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 3 , LAST_UPDATE_DATE = SYSDATE, rfa_Date = sysdate , rfa_user = user_id
    WHERE contract_id = member_id AND contract_stat_id in (1,2,9);
    COMMIT;         
  
  elsif level_id = 4264 then ---SAP Payment
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 3 , LAST_UPDATE_DATE = SYSDATE, rfa_Date = sysdate , rfa_user = user_id
    WHERE contract_payment_id = member_id AND contract_stat_id in (1,2,9);
    COMMIT;         
  elsif level_id = 4366 then ---Contract
    UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 3 , LAST_UPDATE_DATE = SYSDATE, rfa_Date = sysdate , rfa_user = user_id
    WHERE contract_group_id = member_id AND contract_stat_id in (1,2,9);
    COMMIT;         
  end if;
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), dbms_utility.format_error_stack, dbms_utility.format_error_backtrace );
    RAISE;
  END;
END prc_chg_cl_req_approval;

PROCEDURE prc_chg_cl_approved(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_APPROVED';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_app_valid   NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_CHG_CL_APPROVED';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE,  LEVEL_ID, MEMBER_ID, user_id);
 
 if level_id = 4261 then ---Investment Line
 
   select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_line_id = member_id
     AND contract_stat_id = 3;
   
   if v_app_valid <> 0 then
 
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 4 , LAST_UPDATE_DATE = SYSDATE, approved_Date = sysdate , approved_user = user_id, method_status = 3
      WHERE contract_line_id = member_id AND contract_stat_id = 3;
      
      COMMIT; 
   else
     
      UPDATE CONTRACT_LINE SET  method_status = 9, last_update_date = sysdate
      WHERE contract_line_id = member_id;
      COMMIT;
      
   end if;
      
  elsif level_id = 4263 then ---Investment
  
   select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_id = member_id
     AND contract_stat_id = 3;
   
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 4 , LAST_UPDATE_DATE = SYSDATE, approved_Date = sysdate , approved_user = user_id
      WHERE contract_id = member_id AND contract_stat_id = 3;
      
      UPDATE CONTRACT SET  method_status = 3, last_update_date = sysdate
      WHERE contract_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT SET  method_status = 9, last_update_date = sysdate
      WHERE contract_id = member_id;
      COMMIT;
      
   end if;
      
  elsif level_id = 4264 then ---SAP Payment
  
  select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_payment_id = member_id
     AND contract_stat_id = 3;
  
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 4 , LAST_UPDATE_DATE = SYSDATE, approved_Date = sysdate , approved_user = user_id
      WHERE contract_payment_id = member_id AND contract_stat_id = 3;
      COMMIT; 
      
       UPDATE CONTRACT_payment SET  method_status = 3, last_update_date = sysdate
      WHERE contract_payment_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT_PAYMENT SET  method_status = 9, last_update_date = sysdate
      WHERE contract_payment_id = member_id;
      COMMIT;
      
   end if;
  elsif level_id = 4366 then ---Contract
  
  select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_group_id = member_id
     AND contract_stat_id = 3;
  
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 4 , LAST_UPDATE_DATE = SYSDATE, approved_Date = sysdate , approved_user = user_id
      WHERE contract_group_id = member_id AND contract_stat_id = 3;
      COMMIT; 
      
       UPDATE CONTRACT_GROUP SET  method_status = 3, last_update_date = sysdate
      WHERE contract_group_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT_GROUP SET  method_status = 9, last_update_date = sysdate
      WHERE contract_group_id = member_id;
      COMMIT;
      
   end if;
  end if;  
   
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END PRC_CHG_CL_APPROVED;

PROCEDURE prc_chg_cl_decline(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_DECLINE';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_app_valid   NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE,  LEVEL_ID, MEMBER_ID, user_id);
 
 if level_id = 4261 then ---Investment Line
 
   select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_line_id = member_id 
     AND contract_stat_id = 3;
   
   if v_app_valid <> 0 then
 
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 9 , LAST_UPDATE_DATE = SYSDATE, method_status = 3, contract_dec_msg = contract_dec_msg_new
      WHERE contract_line_id = member_id AND contract_stat_id = 3;
      
      COMMIT; 
   else
     
      UPDATE CONTRACT_LINE SET  method_status = 9, last_update_date = sysdate, contract_dec_msg = null
      WHERE contract_line_id = member_id;
      COMMIT;
      
   end if;
      
  elsif level_id = 4263 then ---Investment
  
   select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_id = member_id
     AND contract_stat_id = 3;
   
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 9 , contract_dec_msg = (select contract_dec_msg_new from contract where contract_id = member_id),
      LAST_UPDATE_DATE = SYSDATE
      WHERE contract_id = member_id AND contract_stat_id = 3;
      
      UPDATE CONTRACT SET  method_status = 3, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT SET  method_status = 9, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_id = member_id;
      COMMIT;
      
   end if;
      
  elsif level_id = 4264 then ---SAP Payment
  
  select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_payment_id = member_id
     AND contract_stat_id = 3;
  
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 9, contract_dec_msg = (select contract_dec_msg_new from contract_payment where contract_payment_id = member_id),
      LAST_UPDATE_DATE = SYSDATE
      WHERE contract_payment_id = member_id AND contract_stat_id = 3;
      COMMIT; 
      
       UPDATE CONTRACT_payment SET  method_status = 3, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_payment_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT_PAYMENT SET  method_status = 9, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_payment_id = member_id;
      COMMIT;
      
   end if;
  elsif level_id = 4366 then ---Contract
  
  select count(1) into v_app_valid 
   from approve_matrix a, contract_line l
   where a.approve_matrix_id = l.created_user
     and a.AH_APPROVER_NAME = user_id
     and l.contract_group_id = member_id
     AND contract_stat_id = 3;
  
   if v_app_valid <> 0 then
      UPDATE CONTRACT_LINE SET CONTRACT_STAT_ID = 9, contract_dec_msg = (select contract_dec_msg_new from contract_group where contract_group_id = member_id),
      LAST_UPDATE_DATE = SYSDATE
      WHERE contract_group_id = member_id AND contract_stat_id = 3;
      COMMIT; 
      
       UPDATE CONTRACT_GROUP SET  method_status = 3, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_group_id = member_id;
      COMMIT;
      
   else
     
      UPDATE CONTRACT_GROUP SET  method_status = 9, last_update_date = sysdate, contract_dec_msg_new= null
      WHERE contract_group_id = member_id;
      COMMIT;
      
   end if;
  end if;  
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END PRC_CHG_CL_DECLINE;


PROCEDURE prc_chg_cl_cancelled(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_CANCELLED';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_oi_type     NUMBER;
  v_inv_start   DATE;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_CHG_CL_CANCELLED';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  if level_id = 4261 then ---Investment Line
  
  select nvl(t.oi_flag,0),start_date into v_oi_type,v_inv_start from contract_line l, comp_type t WHERE l.comp_type_id = t.comp_type_id and contract_line_id = member_id;
  
  if v_oi_type = 1 and v_inv_start <= sysdate then
   
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_line_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1)
      AND sales_date > sysdate;
      
    update CONTRACT_LINE set CONTRACT_STAT_ID = 6 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID,
                             end_date = least(greatest(sysdate,start_date ),end_date)
    WHERE contract_line_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    commit;
    
   else 
    
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_line_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1);
    
  --  DELETE FROM contract_line WHERE contract_line_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
    
    update CONTRACT_LINE set CONTRACT_STAT_ID = 7 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID
    WHERE contract_line_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    COMMIT; 
    
   end if ;
  end if;  
  
  if level_id = 4263 then ---Investment
  
   select max(nvl(t.oi_flag,0)),min(start_date) into v_oi_type,v_inv_start from contract_line l, comp_type t WHERE l.comp_type_id = t.comp_type_id and contract_id = member_id;
  
   if v_oi_type = 1 and v_inv_start <= sysdate then
  
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1)
    AND sales_date > sysdate;
    
  --  DELETE FROM contract_line WHERE contract_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 6 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID,
                             end_date = least(greatest(sysdate,start_date ),end_date)
    WHERE contract_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
   
    commit;
   
   else
   
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1);
    
  --  DELETE FROM contract_line WHERE contract_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 7 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID
    WHERE contract_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    
    COMMIT;    
   
   end if;
    
  end if;
  
  if level_id = 4264 then ---SAP Payment
  
   select max(nvl(t.oi_flag,0)),min(start_date) into v_oi_type,v_inv_start from contract_line l, comp_type t WHERE l.comp_type_id = t.comp_type_id and contract_payment_id = member_id;
  
   if v_oi_type = 1 and v_inv_start <= sysdate then
   
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_payment_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1)
    AND sales_date > sysdate;
    
 --   DELETE FROM contract_line WHERE contract_payment_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 6 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID,
                             end_date = least(greatest(sysdate,start_date ),end_date)
    WHERE contract_payment_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
   
    commit;
   
   else
   
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_payment_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1);
    
 --   DELETE FROM contract_line WHERE contract_payment_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 7 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID
    WHERE contract_payment_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    
    COMMIT;
  
   end if; 
    
  elsif level_id = 4366 then ---Contract
  
   select max(nvl(t.oi_flag,0)),min(start_date) into v_oi_type,v_inv_start from contract_line l, comp_type t WHERE l.comp_type_id = t.comp_type_id and contract_group_id = member_id;
  
   if v_oi_type = 1 and v_inv_start <= sysdate then
   
     UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_group_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1)
    AND sales_date > sysdate;
  
 --   DELETE FROM contract_line WHERE contract_group_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 6 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID,
                             end_date = least(greatest(sysdate,start_date ),end_date)
    WHERE contract_group_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    
    COMMIT; 
   
   else
  
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    WHERE contract_line_id in (select contract_line_id from contract_line WHERE contract_group_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1);
  
 --   DELETE FROM contract_line WHERE contract_group_id = member_id AND CONTRACT_EXP_STAT_ID = 0;
  
    update CONTRACT_LINE set CONTRACT_STAT_ID = 7 ,CONTRACT_EXP_STAT_ID = 0, LAST_UPDATE_DATE = sysdate, CANCELLED_DATE = sysdate , CANCELLED_USER = USER_ID
    WHERE contract_group_id = member_id AND contract_stat_id in (4,8,10) AND CONTRACT_EXP_STAT_ID = 1; 
    
    
    COMMIT;      
  
   end if;
  
  end if;
  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END PRC_CHG_CL_CANCELLED;

PROCEDURE prc_chg_cl_dates(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name   VARCHAR2 (100) := 'PRC_CHG_CL_DATES';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;
  v_new_date    DATE;
  v_sd_ed_val         NUMBER := 0;
  v_past_date         NUMBER := 0;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
 
  if level_id = 4264 then ---SAP Payment
  
    select new_end_date into v_new_date from contract_payment where contract_payment_id = member_id;
    
    select count(1) into v_sd_ed_val from contract_line where contract_payment_id = member_id and end_date <= v_new_date;
    
    select count(1) into v_past_date from contract_line where contract_payment_id = member_id and v_new_date < start_date;
    
    if v_past_date = 0 and v_sd_ed_val = 0  then
  
        update CONTRACT_LINE set CONTRACT_STAT_ID = decode(CONTRACT_STAT_ID,4,10,CONTRACT_STAT_ID) ,CONTRACT_EXP_STAT_ID = 0, end_date = v_new_date, LAST_UPDATE_DATE = sysdate, 
               DATE_CHG_DATE = sysdate , DATE_CHG_USER = USER_ID
        WHERE contract_payment_id = member_id 
          AND contract_stat_id in (4,8,10)
          AND CONTRACT_EXP_STAT_ID = 1
          AND end_date >= v_new_date
          AND v_new_date > start_date; 
        
        COMMIT;      
        
        update contract_payment set method_status = 3, last_update_date = sysdate
        where contract_payment_id = member_id;
        
        commit;
        
    elsif v_past_date = 0 then
    
        update contract_payment set method_status = 12, last_update_date = sysdate
        where contract_payment_id = member_id;
        
        commit;
    
    else 
    
        update contract_payment set method_status = 16, last_update_date = sysdate
        where contract_payment_id = member_id;
        
        commit;
    
    end if;
  elsif  level_id = 4261 then ---Investment Line
  
    select new_end_date into v_new_date from contract_line where contract_line_id = member_id;
    
    select count(1) into v_sd_ed_val from contract_line where contract_line_id = member_id and end_date <= v_new_date;
    
    select count(1) into v_past_date from contract_line where contract_line_id = member_id and v_new_date < start_date;
    
    if v_past_date = 0 and v_sd_ed_val = 0  then
  
        update CONTRACT_LINE set CONTRACT_STAT_ID = decode(CONTRACT_STAT_ID,4,10,CONTRACT_STAT_ID) ,CONTRACT_EXP_STAT_ID = 0, end_date = v_new_date, LAST_UPDATE_DATE = sysdate, 
               DATE_CHG_DATE = sysdate , DATE_CHG_USER = USER_ID
        WHERE contract_line_id = member_id 
          AND contract_stat_id in (4,8,10)
          AND CONTRACT_EXP_STAT_ID = 1
          AND end_date >= v_new_date
          AND v_new_date > start_date; 
        
        COMMIT;      
        
        update contract_line set method_status = 3, last_update_date = sysdate
        where contract_line_id = member_id;
        
        commit;
        
    elsif v_past_date = 0 then
    
        update contract_line set method_status = 12, last_update_date = sysdate
        where contract_line_id = member_id;
        
        commit;
    
    else 
    
        update contract_line set method_status = 16, last_update_date = sysdate
        where contract_line_id = member_id;
        
        commit;
    
    end if;
  end if;
  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_chg_cl_dates;

PROCEDURE prc_delete(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  v_prog_name   VARCHAR2 (100) := 'PRC_DELETE';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  if level_id = 4261 then ---Investment Line
    UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    where contract_line_id in ( select contract_line_id 
                                                            from contract_line 
                                                           where contract_line_id = member_id 
                                                           and contract_stat_id not in (4,8,10));
                                                           
     delete from contract_line where contract_line_id = member_id and contract_stat_id not in (4,8,10);
     commit;
  elsif level_id = 4263 then ---Investment
     --Only Delete if no lines have been exported
     UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    where contract_id in (select contract_id from contract 
                                                      where contract_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_id = member_id 
                                                                    and contract_stat_id in (4,8,10)));     
     
     delete from contract where contract_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_id = member_id 
                                                                    and contract_stat_id in (4,8,10));
     
     delete from contract_line where contract_id = member_id and contract_stat_id not in (4,8,10);                                                               
                                                                    
     commit;
  elsif level_id = 4366 then ---Contract
     --Only Delete if no lines have been exported
     UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    where contract_group_id in ( select contract_group_id 
                                                                  from contract_group 
                                                                  where contract_group_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_group_id = member_id 
                                                                    and contract_stat_id in (4,8,10))
     
     );
     
     delete from contract_group where contract_group_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_group_id = member_id 
                                                                    and contract_stat_id in (4,8,10));
                                                                    
     delete from contract_line where contract_group_id = member_id and contract_stat_id not in (4,8,10);                                                               
                                                                    
        commit;                                                            
  elsif level_id = 4264 then ---SAP Payment
     --Only Delete if no lines have been exported
   UPDATE CONTRACT_DATA 
    SET case_deal_amt = null,case_deal_perc = null, lumpsum = null, last_update_date = sysdate
    where contract_payment_id in ( select contract_payment_id 
                                                               from contract_payment
                                                               where contract_payment_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_payment_id = member_id 
                                                                    and contract_stat_id in (4,8,10)));
     
     delete from contract_payment where contract_payment_id = member_id and 0 in (select count(1) 
                                                                    from contract_line 
                                                                  where contract_payment_id = member_id 
                                                                    and contract_stat_id in (4,8,10));
                                                                    
     delete from contract_line where contract_payment_id = member_id and contract_stat_id not in (4,8,10);     
                                                                    
     commit;
  end if;
  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END PRC_DELETE;

PROCEDURE prc_dup_contract(
    user_id   NUMBER DEFAULT NULL,
    LEVEL_ID  number default null,
    member_id NUMBER DEFAULT NULL)
as
  V_PROG_NAME   VARCHAR2 (100) := 'PRC_DUP_CONTRACT';
  v_status      VARCHAR2 (100);
  V_PROC_LOG_ID NUMBER;
  V_CONTRACT_GROUP_ID NUMBER;
  v_offset_months NUMBER;
  V_FILTER_ID NUMBER;
  v_cont_pay_id NUMBER;
  v_cont_id NUMBER;
  

begin
  pre_logon;
  V_STATUS      := 'Start ';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
 V_CONTRACT_GROUP_ID := CONTRACT_GROUP_SEQ.NEXTVAL;
 
   select offset_months into v_offset_months from contract_group where contract_group_id = member_id;
 
 --------------------------------
 -------Create New Contract------
 --------------------------------
 
   check_and_drop('v_tmp_dup_cg_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_cg_' || user_id || ' as
                 select * from contract_group where contract_group_id = ' || member_id);
    
    ---Update with new details             
    DYNAMIC_DDL('update v_tmp_dup_cg_' || user_id || ' 
                 set contract_group_id = '|| V_CONTRACT_GROUP_ID ||',
                     contract_group_code = '|| V_CONTRACT_GROUP_ID ||',
                     contract_group_desc = to_char(add_months(period_start, '|| v_offset_months ||'),''yyyy-mm-dd'') || '' : '' || '|| V_CONTRACT_GROUP_ID ||',
                     period_start = add_months(period_start, '|| v_offset_months ||') ,
                     period_end = add_months(period_end, '|| v_offset_months ||') ,
                     creation_date = sysdate,
                     offset_months = null,
                     created_user = '|| user_id ||',
                     last_update_date = sysdate'
                     );     
    
    commit;
    
    DYNAMIC_DDL('insert into contract_group
      select * from v_tmp_dup_cg_' || user_id);
    
     commit;
     
     -------------------------------
     ---Setup Population -----------
     -------------------------------
     
     insert into investment_setup_dates (contract_group_id, from_date,until_date)
     select V_CONTRACT_GROUP_ID, add_months(from_date,v_offset_months),add_months(until_date,v_offset_months)
       from investment_setup_dates
      where contract_group_id = member_id;
      
  -------------------------------------------------------------
  ----Populate SALES ORG ans Sales distribution as a default---
  ----Remove when NEW SALES orgs apply----
  -------------------------------------------------------------
  select investment_setup_levels_seq.nextval into v_filter_id from dual;
  
  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(V_CONTRACT_GROUP_ID,6,v_filter_id,1);
  
    commit;
    
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,1);
  
    commit;
  
  select investment_setup_levels_seq.nextval into v_filter_id from dual;

  INSERT INTO INVESTMENT_SETUP_LEVELS (contract_group_id,level_id,filter_id,level_order)
  values(V_CONTRACT_GROUP_ID,7,v_filter_id,2);
  
    commit;
  
  INSERT INTO investment_setup_members (filter_id,member_id)
  values(v_filter_id,1);
  
    commit;  
    
    -------------------------------
    ------Contract Payment---------
    -------------------------------
                 
    check_and_drop('v_tmp_dup_cp_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_cp_' || user_id || ' as
                 select * from contract_payment 
                 where contract_payment_id in (select contract_payment_id from contract_line where contract_group_id = ' || member_id || ')'); 
   
   check_and_drop('v_tmp_dup_cpid_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_cpid_' || user_id || ' as
                 select contract_payment_id, contract_payment_seq.nextval contract_payment_id_new
                 from contract_payment 
                 where contract_payment_id in (select contract_payment_id from contract_line where contract_group_id = ' || member_id || ')'); 
                 
   
    DYNAMIC_DDL('update v_tmp_dup_cp_' || user_id || ' a 
                 set a.contract_payment_id = (select b.contract_payment_id_new 
                                              from v_tmp_dup_cpid_' || user_id || ' b 
                                              where b.contract_payment_id = a.contract_payment_id),
                     a.contract_payment_code = (select b.contract_payment_id_new 
                                              from v_tmp_dup_cpid_' || user_id || ' b 
                                              where b.contract_payment_id = a.contract_payment_id),   
                     a.contract_payment_desc = (select b.contract_payment_id_new 
                                              from v_tmp_dup_cpid_' || user_id || ' b 
                                              where b.contract_payment_id = a.contract_payment_id),                          
                     creation_date = sysdate,
                     created_user = '|| user_id ||',
                     last_update_date = sysdate'
                     ); 
                     
                     
 --   DYNAMIC_DDL('MERGE INTO v_tmp_dup_cp_' || user_id || ' a using
 --                ( select * from v_tmp_dup_cpid_' || user_id || '
 --                 )b
 --                 on (a.contract_payment_id = b.contract_payment_id)
 --                 when matched then update
 --                 set a.contract_payment_id = b.contract_payment_id_new
 --                 ');    
   commit;               
   
    DYNAMIC_DDL('insert into contract_payment
      select * from v_tmp_dup_cp_' || user_id);
                 
    commit;      
   -------------------------
   --------Contract---------            
   -------------------------
   
    check_and_drop('v_tmp_dup_c_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_c_' || user_id || ' as
                 select * from contract 
                 where contract_id in (select contract_id from contract_line where contract_group_id = ' || member_id || ')');
     
    check_and_drop('v_tmp_dup_cid_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_cid_' || user_id || ' as
                 select contract_id, contract_seq.nextval contract_id_new
                 from contract 
                 where contract_id in (select contract_id from contract_line where contract_group_id = ' || member_id || ')'); 
   
    DYNAMIC_DDL('update v_tmp_dup_c_' || user_id || ' a
                 set contract_id = (select b.contract_id_new 
                                              from v_tmp_dup_cid_' || user_id || ' b 
                                              where b.contract_id = a.contract_id),
                     contract_code = (select b.contract_id_new 
                                              from v_tmp_dup_cid_' || user_id || ' b 
                                              where b.contract_id = a.contract_id),
                     contract_desc = (select b.contract_id_new 
                                              from v_tmp_dup_cid_' || user_id || ' b 
                                              where b.contract_id = a.contract_id),
                     creation_date = sysdate,
                     created_user = '|| user_id ||',
                     last_update_date = sysdate'
                     ); 
   
    DYNAMIC_DDL('insert into contract
      select * from v_tmp_dup_c_' || user_id);             
    
    commit;  
    
    -------------------------------
    -------  Contract Lines--------
    -------------------------------
      
    check_and_drop('v_tmp_dup_cl_' || user_id);
    DYNAMIC_DDL('create table v_tmp_dup_cl_' || user_id || ' as
                 select * from contract_line where contract_stat_id in (1,2,3,4,6,8) and contract_group_id = ' || member_id);
                 
   DYNAMIC_DDL('update v_tmp_dup_cl_' || user_id || ' a
                 set contract_id = (select b.contract_id_new 
                                              from v_tmp_dup_cid_' || user_id || ' b 
                                              where b.contract_id = a.contract_id),
                     contract_group_id = ' ||  V_CONTRACT_GROUP_ID || ',
                     contract_payment_id = (select b.contract_payment_id_new 
                                              from v_tmp_dup_cpid_' || user_id || ' b 
                                              where b.contract_payment_id = a.contract_payment_id),
                     contract_line_id = contract_line_seq.nextval,
                     contract_line_code = contract_line_seq.nextval,
                     contract_line_desc = to_char(add_months(start_date, '|| v_offset_months ||'),''yyyy-mm-dd'') || '' : '' || contract_line_seq.nextval,
                     contract_stat_id = 1,
                     start_date = add_months(start_date, '|| v_offset_months ||'),
                     end_date = add_months(end_date, '|| v_offset_months ||'),
                     creation_date = sysdate,
                     created_user = '|| user_id ||',
                     last_update_date = sysdate,
                     date_chg_user = null,
                     date_chg_date = null,
                     approved_user = null,
                     approved_date = null,
                     rfa_user = null,
                     rfa_date = null,
                     planned_user = null,
                     planned_date = null,
                     contract_exp_stat_id = 0,
                     T_CONT_LVL1_LUD = sysdate,
                     T_CONT_LVL2_LUD = sysdate,
                     T_CONT_LVL3_LUD = sysdate,
                     T_CONT_LVL4_LUD = sysdate,
                     T_CONT_LVL_LUD = sysdate'
                     ); 
   
    DYNAMIC_DDL('insert into contract_line
      select * from v_tmp_dup_cl_' || user_id); 
     
     commit; 
   
   ----------------------------------------------------   
   ------Update Descriptions of all new levels --------   
   ----------------------------------------------------
   merge into contract c using
   (
      select l.contract_id, to_char(min(l.start_date),'yyyy-mm-dd') || ' : ' || l.contract_id contract_desc
        from contract_line l
       where contract_group_id = V_CONTRACT_GROUP_ID
       group by l.contract_id
   )b
   on(c.contract_id = b.contract_id)
   when matched then update 
   set c.contract_desc = b.contract_desc,
       c.last_update_date = sysdate;
       
   merge into contract_payment c using
   (
      select l.contract_payment_id, to_char(min(l.start_date),'yyyy-mm-dd') || ' : ' || l.contract_payment_id contract_payment_desc
        from contract_line l
       where contract_group_id = V_CONTRACT_GROUP_ID
       group by l.contract_payment_id
   )b
   on(c.contract_payment_id = b.contract_payment_id)
   when matched then update 
   set c.contract_payment_desc = b.contract_payment_desc,
       c.last_update_date = sysdate;    
       
  commit;     
  -----------------------------------    
  -------CLEAN UP Temp Tables--------
  -----------------------------------
   
    check_and_drop('v_tmp_dup_cg_' || user_id);

    check_and_drop('v_tmp_dup_cp_' || user_id);
       
    check_and_drop('v_tmp_dup_cpid_' || user_id);
   
    check_and_drop('v_tmp_dup_c_' || user_id);
   
    check_and_drop('v_tmp_dup_cid_' || user_id);
   
    check_and_drop('v_tmp_dup_cl_' || user_id); 
    
    
    update contract_group
       set offset_months = null, last_update_date = sysdate
     where contract_group_id = member_id;
     
     commit;
       
    
  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE, level_id, member_id, user_id);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
end prc_dup_contract;


PROCEDURE prc_sync_contract_to_sd(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null)
as
  v_prog_name       VARCHAR2 (100);
  v_status          VARCHAR2 (100);
  v_proc_log_id     NUMBER;
  v_dummy_prod_id   NUMBER := 0;
  v_mdp_assist      NUMBER :=0;
  v_col_names       VARCHAR2(32000);
  v_insert_sql     VARCHAR2(32000);
  v_merge_sql      VARCHAR2(32000);
  v_select_sql     VARCHAR2(32000);
  v_from_sql       VARCHAR2(32000);
  v_col_where_sql  VARCHAR2(32000);
  v_cust_col_where_sql VARCHAR2(32000);
  v_where_sql      VARCHAR2(32000);
  v_extra_where_SQL varchar2(32000);
  v_group_by_SQL   varchar2(32000);
  sql_str        varchar2(32000);   
  
  v_last_run_date  DATE;
  v_contract_group_id  NUMBER;
  p_start_date         DATE;
  p_end_date           DATE;
  p_full_load          NUMBER:= 0;
 
begin
  pre_logon;
  V_STATUS      := 'Start ';
  v_prog_name   := 'PRC_SYNC_CONTRACT_TO_SD';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE, LEVEL_ID, MEMBER_ID, user_id);
 
  --Get CONTRACT_GROUP_ID from Level Method
  if level_id = 4366 then
    select max(contract_group_id),min(greatest(start_date,sysdate)),max(end_date) 
    into v_contract_group_id, p_start_date, p_end_date from contract_line where contract_group_id = member_id;
  elsif level_id = 4261 then
    select max(contract_group_id),min(greatest(start_date,sysdate)),max(end_date) 
    into v_contract_group_id, p_start_date, p_end_date from contract_line where contract_line_id = member_id;
  elsif level_id = 4263 then
    select max(contract_group_id),min(greatest(start_date,sysdate)),max(end_date) 
    into v_contract_group_id, p_start_date, p_end_date from contract_line where contract_id = member_id;
  elsif level_id = 4264 then
    select max(contract_group_id),min(greatest(start_date,sysdate)),max(end_date)
    into v_contract_group_id, p_start_date, p_end_date from contract_line where contract_payment_id = member_id;
  end if;



 -----------------------
 ----Mode Parameters----
 -----------------------
 -- 0 = Partial Load
 -- 1 = Full Load
 -----------------------
  
  --Truncate for a full load to ensure all combinations are sync correctly
  if p_full_load = 1 then
     DYNAMIC_DDL('TRUNCATE TABLE CONTRACT_DATA');
  end if;
  
 ---Get last Sync Date
 
    SELECT TO_DATE(pval, 'mm-dd-yyyy hh24:mi:ss') 
    INTO v_last_run_date
    FROM sys_params
    WHERE pname = 'last_contract_sync';
    
 
  v_col_names := '';
  v_where_sql := ' WHERE 1=1'; 
  
---------------------------------------------------
----Sync CONTRACT_LINES to CONTRACT_DATA-----------
---------------------------------------------------

  ----Retrieve column names for Heriarachies used in Contract Management-----
   FOR rec1 IN (select distinct id_field 
                  from group_Tables g, access_seq s
                 where g.GROUP_TABLE_ID = s.level1
                    or g.GROUP_TABLE_ID = s.level2
                    or g.GROUP_TABLE_ID = s.level3
                    or g.GROUP_TABLE_ID = s.level4
                    or g.GROUP_TABLE_ID = s.level5
                    or g.GROUP_TABLE_ID = s.level6
                    or g.GROUP_TABLE_ID = s.level7
                    or g.GROUP_TABLE_ID = s.level8
                    or g.GROUP_TABLE_ID = s.level9
                    or g.GROUP_TABLE_ID = s.level10
    )
    LOOP
        v_col_where_sql  :=   v_col_where_sql || ' AND m.' || rec1.id_field || ' = nvl(c.' || rec1.id_field || ', m.' || rec1.id_field ||  ')';
     end loop;
     
      ----Retrieve all Customer column names for Heriarachies used in Contract Management-----
   FOR rec1 IN (select distinct id_field 
                  from group_Tables g, access_seq s
                 where lower(g.search_table) = 'location'
                    and (g.GROUP_TABLE_ID = s.level1
                    or g.GROUP_TABLE_ID = s.level2
                    or g.GROUP_TABLE_ID = s.level3
                    or g.GROUP_TABLE_ID = s.level4
                    or g.GROUP_TABLE_ID = s.level5
                    or g.GROUP_TABLE_ID = s.level6
                    or g.GROUP_TABLE_ID = s.level7
                    or g.GROUP_TABLE_ID = s.level8
                    or g.GROUP_TABLE_ID = s.level9
                    or g.GROUP_TABLE_ID = s.level10)
                    
    )
    LOOP
        v_cust_col_where_sql := v_cust_col_where_sql || ' AND m.' || rec1.id_field || ' = nvl(c.' || rec1.id_field || ', m.' || rec1.id_field ||  ')';
     end loop;


   ----Create Lower Level Rows for Case Deal and Percentage------
       --Create Temp Table
   v_select_sql :=  'select /*+ parallel(4) */ m.location_id, m.item_id, i.datet sales_date, c.contract_group_id,' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id,' ||
                           'c.case_deal_amt, c.case_deal_perc, t.deal_type, t.comp_type_id ';
   v_from_sql :=  ' FROM mdp_matrix m, contract_line c, inputs i, comp_type_seq s, comp_type t ';
   v_where_sql  :=  ' WHERE i.datet between c.start_date and c.end_date '||
                                     'AND c.contract_group_id = ' || v_contract_group_id || ' '||
                                     'AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                                         'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                                     'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                                     'AND c.contract_stat_id in (2,3,4,6,8,10) '||
                                  --   'AND c.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                                  --   'AND c.start_date < TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                                     'AND t.comp_type_id = s.comp_type_id ' || v_col_where_sql;
   v_extra_where_SQL := ' AND t.deal_type in (1,2)';
   if p_full_load = 0 then 
     v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
   end if;
   
   check_and_drop('v_tmp_cont_data_' || user_id);                                  
   DYNAMIC_DDL('create table v_tmp_cont_data_' || user_id || ' as ' ||v_select_sql||v_from_sql || v_where_sql || v_extra_where_SQL  );
  
  
    V_STATUS      := 'Step 2 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
  
  

     --Merge Temp Table into CONTRACT_DATA
   v_extra_where_SQL := '';
   DYNAMIC_DDL( 'MERGE /*+ parallel(4) */ INTO contract_data cd using (select /*+ parallel(4) */  * from v_tmp_cont_data_' || user_id || ' where 1=1 '||  v_extra_where_SQL ||') a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.case_deal_amt = a.case_deal_amt, '||
                  'cd.case_deal_perc = a.case_deal_perc, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.case_deal_amt, cd.case_deal_perc,cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.case_deal_amt, a.case_deal_perc, sysdate) ');


  commit;
  
      V_STATUS      := 'Step 3 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
  
 ----Create Lower Level Rows for Lumpsum Except Amortization------
    --Update Glob Prop in contract_line
   
     v_select_sql := 'SELECT /*+ parallel(4) */ c.contract_line_id, sum(case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end) glob_prop ';
     v_from_sql :=  ' FROM mdp_matrix m, contract_line c, comp_type_seq s, comp_type t ';
     v_where_SQL := 'WHERE c.contract_stat_id in (2,3,4,6,8,10) '||
                                     'AND c.contract_group_id = ' || v_contract_group_id ||
                                     ' AND GREATEST(TRUNC(c.start_date ,''MM''),NEXT_DAY(c.start_date ,''MONDAY'')-7) '|| 
                                          ' < TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                                     'AND c.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                                     'AND t.comp_type_id = s.comp_type_id ' ||
                                     'AND t.deal_type in (3) ' || v_col_where_sql; 
     v_group_by_sql := ' GROUP BY c.contract_line_id ';
     if p_full_load = 0 then 
        v_extra_where_SQL := ' AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
     end if;
     
     check_and_drop('v_tmp_cont_data_' || user_id);                                  
     DYNAMIC_DDL('create table v_tmp_cont_data_' || user_id || ' as ' ||v_select_sql||v_from_sql || v_where_SQL || v_col_where_sql || v_extra_where_SQL || v_group_by_sql );
     
     
    V_STATUS      := 'Step 4 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
     
     
     DYNAMIC_DDL( 'MERGE /*+ parallel(4) */ INTO CONTRACT_LINE cl using (select /*+ parallel(4) */  * from v_tmp_cont_data_' || user_id || ')a '||
                     'on (cl.contract_line_id = a.contract_line_id) '||
                     'when matched then update '||
                     'set cl.prop_factor = a.glob_prop ');
 
     commit;
 
     V_STATUS      := 'Step 5 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
 
 
   --Create Temp Table for updating Contract_data
     v_select_sql :=  'select /*+ parallel(4) */ m.location_id, m.item_id, GREATEST(TRUNC(c.start_date ,''MM''),NEXT_DAY(c.start_date ,''MONDAY'')-7) sales_date, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, (c.lumpsum * (case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end) / c.prop_factor) lumpsum';
     v_extra_where_SQL := ' AND nvl(c.RR_CL_AMORTIZE,0) = 0'; 
     if p_full_load = 0 then 
       v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') '; 
     end if;
     
     check_and_drop('v_tmp_cont_data_' || user_id);                                  
     DYNAMIC_DDL('create table v_tmp_cont_data_' || user_id || ' as ' ||v_select_sql||v_from_sql || v_where_sql ||  v_extra_where_SQL  );

     V_STATUS      := 'Step 6 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);

   --Merge into Contract_data

   DYNAMIC_DDL( 'MERGE /*+ parallel(4) */ INTO contract_data cd using (select /*+ parallel(4) */ * from v_tmp_cont_data_' || user_id || ') a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');


  commit;
  
    V_STATUS      := 'Step 7 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
    
    
 ----Create Lower Level Rows for Lumpsum Amortization------
    --Get number of periods to split by
     merge into /*+ parallel(4) */ contract_line l using
     (select /*+ parallel(4) */ c.contract_line_id, count(distinct calendar_month) AMORTIZE_PERIODS 
          from contract_line c, comp_type_seq s, comp_type t, inputs i
         WHERE c.contract_stat_id in (2,3,4,6,8,10) 
               AND c.contract_group_id =  v_contract_group_id 
               --AND i.datet between GREATEST(TRUNC(p_start_date, 'MM'), NEXT_DAY(p_start_date,'MONDAY')-7)  and p_end_date
               AND (c.last_update_date >= v_last_run_date or p_full_load = 1)
               AND c.start_date < p_end_date
               and c.end_date > p_start_date
               AND c.comp_type_seq_id = s.comp_type_seq_id
               AND t.comp_type_id = s.comp_type_id 
               AND t.deal_type in (3) 
               AND nvl(c.RR_CL_AMORTIZE,0) = 1
               AND i.datet between GREATEST(TRUNC(c.start_date, 'MM'), NEXT_DAY(c.start_date,'MONDAY')-7) 
                               AND c.end_date
         GROUP BY c.contract_line_id
      )a
     on (l.contract_line_id = a.contract_line_id)
     when matched then update
     set l.AMORTIZE_PERIODS = a.AMORTIZE_PERIODS;
     
     commit;
    
        V_STATUS      := 'Step 8 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
    
    --Create Temp Table for updating Contract_data
     v_select_sql :=  'select /*+ parallel(4) */ m.location_id, m.item_id, min(i.datet) sales_date, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, '||
                           'max(c.lumpsum * case nvL(m.glob_prop,0)*m.do_fore when 0 then 0.0000000001 else m.glob_prop end / (c.prop_factor / AMORTIZE_PERIODS)) lumpsum, ' ||
                           'i.calendar_month ';
     v_from_sql :=  ' FROM mdp_matrix m, contract_line c, inputs i, comp_type_seq s, comp_type t  ';
     v_where_SQL := ' WHERE i.datet between c.start_date and c.end_date '||
                     'AND c.contract_group_id = ' || v_contract_group_id ||
                     ' AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                   'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                   'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                     'AND c.contract_stat_id in (2,3,4,6,8,10) '||
                     'AND c.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                     'AND c.comp_type_seq_id = s.comp_type_seq_id ' ||
                     'AND t.comp_type_id = s.comp_type_id ' || 
                     'AND t.deal_type in (3) ' ||v_col_where_sql;
     v_extra_where_SQL := ' AND nvl(c.RR_CL_AMORTIZE,0) = 1 '; 
     if p_full_load = 0 then 
        v_extra_where_SQL := v_extra_where_SQL || 'AND c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'') ' ;
     end if;
     v_group_by_sql    := ' GROUP BY m.location_id, m.item_id, c.contract_group_id, t.comp_type_id, ' ||
                           'c.contract_id, c.contract_payment_id, c.contract_line_id, c.comp_type_seq_id, i.calendar_month';
     check_and_drop('v_tmp_cont_data_' || user_id );                                  
     DYNAMIC_DDL('create table v_tmp_cont_data_' || user_id || ' as ' ||v_select_sql||v_from_sql || v_where_sql ||  v_extra_where_SQL  || v_group_by_sql );
  
    V_STATUS      := 'Step 9 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
    
    --Merge into Contract_data
   DYNAMIC_DDL( 'MERGE /*+ parallel(4) */ INTO contract_data cd using (select /*+ parallel(4) */ * from v_tmp_cont_data_' || user_id || ') a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');
  
  
      commit;
  
      
------------------------------------------------------------
-----Lumpsum combinations that have no combinations  ------
------------------------------------------------------------
      
    V_STATUS      := 'Step 10 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
      
-----Get all Lumpsum combinations that have no combinations      
     check_and_drop('v_tmp_cont_miss_' || user_id || '');                                  
     DYNAMIC_DDL('create table v_tmp_cont_miss_' || user_id || ' as 
                  select l.* 
                  from contract_line l, comp_type c
                 where l.comp_type_id = c.comp_type_id
                   AND l.contract_group_id = ' || v_contract_group_id || '
                   and deal_type = 3
                   and l.contract_stat_id in (2,3,4,6,8,10)
                   AND l.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                   and nvl(l.lumpsum,0) <> 0
                   and l.contract_line_id not in (select contract_line_id from contract_data ) ' ); 
 
    V_STATUS      := 'Step 11 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);      

 ----Get all Combinations that have a dummy product against it
    check_and_drop('v_tmp_cont_comb_' || user_id || '');    
     DYNAMIC_DDL('create table v_tmp_cont_comb_' || user_id || ' as ' ||
                 ' select /*+parallel */ c.contract_line_id, max(m.location_id) location_id, min(m.item_id) item_id, 0 new_comb ' ||
                 ' FROM mdp_matrix m, v_tmp_cont_miss_' || user_id || ' c, t_ep_item i ' ||
                 ' WHERE m.t_ep_item_ep_id = i.t_ep_item_ep_id' ||
                 ' AND i.item like ''999999999999999999%''' ||
                 v_cust_col_where_sql ||
                 ' GROUP BY contract_line_id');
   
   select min(item_id) into v_dummy_prod_id 
     from items i, t_ep_item mt
    where i.t_ep_item_ep_id = mt.t_ep_item_ep_id
      AND mt.item like '999999999999999999%';
   
    V_STATUS      := 'Step 12 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
   
 ----Create all othe Customer combinations that do not have a Dummy Product
  
     DYNAMIC_DDL(' INSERT INTO v_tmp_cont_comb_' || user_id || ' ' ||
                 ' select /*+parallel */ c.contract_line_id, max(m.location_id) location_id, '|| v_dummy_prod_id || ' item_id, 1 new_comb ' ||
                 ' FROM location m, v_tmp_cont_miss_' || user_id || ' c ' ||
                 ' WHERE c.contract_line_id not in (select contract_line_id from v_tmp_cont_comb_' || user_id || ' ) ' ||
                 v_cust_col_where_sql ||
                 ' GROUP BY contract_line_id');
                 
    commit;
 
    V_STATUS      := 'Step 13 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
    
    
        --Merge into Contract_data for Non Amortised Contracts
   DYNAMIC_DDL( 'MERGE /*+parallel */ INTO contract_data cd using ( ' ||
                 'select /*+parallel */ b.location_id,b.item_id,  GREATEST(TRUNC(a.start_date ,''MM''),NEXT_DAY(a.start_date ,''MONDAY'')-7) sales_date, '||
                 ' a.contract_group_id, a.contract_id, a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum ' ||
                 '   from v_tmp_cont_miss_' || user_id || ' a,v_tmp_cont_comb_' || user_id || ' b ' ||
                  'where a.contract_line_id = b.contract_line_id ' ||
                  '  and nvl(a.RR_CL_AMORTIZE,0) = 0 ) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');
  
  
      commit;
      
    V_STATUS      := 'Step 14 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
      
              --Merge into Contract_data for All Amortised Contracts
   DYNAMIC_DDL( 'MERGE /*+parallel */ INTO contract_data cd using ( ' ||
                 'select /*+parallel */ b.location_id,b.item_id, min(i.datet) sales_date,a.contract_group_id, ' ||
                 ' a.contract_id, a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, max(a.lumpsum) / max(a.AMORTIZE_PERIODS) lumpsum, i.calendar_month ' ||
                 '   from v_tmp_cont_miss_' || user_id || ' a,v_tmp_cont_comb_' || user_id || ' b, inputs i ' ||
                  'where a.contract_line_id = b.contract_line_id ' ||
                   ' and i.datet between a.start_date and a.end_date '||
                   ' AND i.datet between GREATEST(TRUNC(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''), ''MM''), '||
                                   'NEXT_DAY(TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY''),''MONDAY'')-7) '||
                                   'AND TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '||
                    ' AND a.end_date > TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'') '|| 
                  '  and nvl(a.RR_CL_AMORTIZE,0) = 1 ' ||
                  '  GROUP BY b.location_id,b.item_id,a.contract_group_id, a.contract_id, a.contract_payment_id, ' ||
                 '  a.contract_line_id, a.comp_type_seq_id, a.lumpsum,i.calendar_month) a ' ||
                 'ON (cd.location_id = a.location_id '||
                  'and cd.item_id = a.item_id '||
                  'and cd.sales_date = a.sales_date '||
                  'and cd.contract_line_id = a.contract_line_id) '||
                 'when matched then '||
                 'update set '||
                  'cd.lumpsum = a.lumpsum, '||
                  'cd.last_update_date = sysdate '||
                 'when not matched then '||
                 'insert (cd.location_id, cd.item_id, cd.sales_date, cd.contract_group_id, cd.contract_id,'||
                  'cd.contract_payment_id, cd.contract_line_id, cd.comp_type_seq_id, cd.lumpsum, cd.last_update_date) '||
                 'VALUES (a.location_id, a.item_id, a.sales_date, a.contract_group_id, a.contract_id,'||
                  'a.contract_payment_id, a.contract_line_id, a.comp_type_seq_id, a.lumpsum, sysdate) ');
                  
                  
        commit;
      
  
    V_STATUS      := 'Step 15 = SD ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
    
    
  ----------------------------------------------------------------------
  ------- Sync CONCTRACT_DATA to SD ------------------------------------
  ----------------------------------------------------------------------
    
    
    v_extra_where_SQL:='';
    
    
    ---If Partial Sync, get all locaton_id/item_id's that need to be sync)
    if p_full_load = 0  then 
       check_and_drop('v_tmp_cont_sync_ids_' || user_id);
 
       DYNAMIC_DDL('CREATE TABLE v_tmp_cont_sync_ids_' || user_id || ' as ' ||
         'select /*+ parallel(4) */ distinct location_id, item_id ' ||
           'from contract_data c ' ||
          'WHERE c.last_update_date >= TO_DATE(''' || TO_CHAR(v_last_run_date, 'YYYY/MM/DD HH24:MI:SS') || ''', ''YYYY/MM/DD HH24:MI:SS'')' ) ;
--        v_extra_where_SQL := ' AND (cd.location_id,item_id) in (select c.location_id,c.item_id from v_tmp_cont_sync_ids c) ' ;
     
     elsif p_full_load = 1 then
     
        ---Clear all values in SD
       DYNAMIC_DDL(' MERGE /*+ parallel(4) */ INTO sales_data sd using(
            select /*+ parallel(4) */ location_id,item_id,sales_date,
                        null ebspricelist104,
                        null ebspricelist111,
                        null ebspricelist112,
                        null ebspricelist113,
                        null ebspricelist114,
                        null ebspricelist115,
                        null ebspricelist116,
                        null ebspricelist117,
                        null ebspricelist118,
                        null ebspricelist121,
                        null ebspricelist122,
                        null mkt_acc1,
                        null mkt_acc3,
                        null ff_input1,
                        null cust_input3,
                        null mkt_acc2                        
            from sales_data
           WHERE sales_date between p_start_date and p_end_date
             AND (location_id,item_id,sales_date) not in (select distinct location_id,item_id,sales_date from contract_data)
             and     ABS(NVL(ebspricelist104, 0))
                   + ABS(NVL(ebspricelist111, 0))
                   + ABS(NVL(ebspricelist112, 0))
                   + ABS(NVL(ebspricelist113, 0))
                   + ABS(NVL(ebspricelist114, 0))
                   + ABS(NVL(ebspricelist115, 0))
                   + ABS(NVL(ebspricelist116, 0))
                   + ABS(NVL(ebspricelist117, 0))
                   + ABS(NVL(ebspricelist118, 0))
                   + ABS(NVL(ebspricelist121, 0))
                   + ABS(NVL(ebspricelist122, 0))
                   + ABS(NVL(mkt_acc1, 0))
                   + ABS(NVL(mkt_acc3, 0))
                   + ABS(NVL(ff_input1, 0))
                   + ABS(NVL(cust_input3, 0))
                   + ABS(NVL(mkt_acc2, 0)) <> 0
        )sd1
        on ( sd.location_id = sd1.location_id
         and sd.item_id = sd1.item_id
         and sd.sales_date = sd1.sales_date)
         WHEN MATCHED THEN UPDATE
                    set sd.ebspricelist104 = sd1.ebspricelist104,
                        sd.ebspricelist111 = sd1.ebspricelist111,
                        sd.ebspricelist112 = sd1.ebspricelist112,
                        sd.ebspricelist113 = sd1.ebspricelist113,
                        sd.ebspricelist114 = sd1.ebspricelist114,
                        sd.ebspricelist115 = sd1.ebspricelist115,
                        sd.ebspricelist116 = sd1.ebspricelist116,
                        sd.ebspricelist117 = sd1.ebspricelist117,
                        sd.ebspricelist118 = sd1.ebspricelist118,
                        sd.ebspricelist121 = sd1.ebspricelist121,
                        sd.ebspricelist122 = sd1.ebspricelist122,
                        sd.mkt_acc1 = sd1.mkt_acc1,
                        sd.mkt_acc3 = sd1.mkt_acc3,
                        sd.ff_input1 = sd1.ff_input1,
                        sd.cust_input3 = sd1.cust_input3,
                        sd.mkt_acc2 = sd1.mkt_acc2,
                        last_update_date = SYSDATE') ;
        
        commit;  
                        
     end if;
     
     
    V_STATUS      := 'Step 16 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE); 
 
  -- Cteate temporary table of SD data
  check_and_drop('RR_CON_D_SYNC_TEMP_' || user_id || '');
  sql_str := 'CREATE TABLE RR_CON_D_SYNC_TEMP_' || user_id || ' as ' ||
              'SELECT /*+ parallel(4) */ cd.item_id, cd.location_id, cd.sales_date,'||
              'max(CASE WHEN s.comp_type_id in (1) THEN cd.case_deal_perc ELSE NULL END) ebspricelist111,' ||
              'max(CASE WHEN s.comp_type_id in (2) THEN cd.case_deal_amt ELSE NULL END) ebspricelist112,' ||
              'max(CASE WHEN s.comp_type_id in (3) THEN cd.case_deal_amt ELSE NULL END) ebspricelist113,' ||
              'max(CASE WHEN s.comp_type_id in (4) THEN cd.case_deal_perc ELSE NULL END) ebspricelist114,' ||
              'sum(CASE WHEN s.comp_type_id in (5) THEN cd.lumpsum ELSE NULL END) ebspricelist116,' ||
              'sum(CASE WHEN s.comp_type_id in (6) THEN cd.case_deal_amt ELSE NULL END) ebspricelist117,' ||
              'sum(CASE WHEN s.comp_type_id in (7) THEN cd.lumpsum ELSE NULL END) ebspricelist115,' ||
              'sum(CASE WHEN s.comp_type_id in (8) THEN cd.lumpsum ELSE NULL END) ebspricelist121,' ||
              'sum(CASE WHEN s.comp_type_id in (9) THEN cd.case_deal_amt ELSE NULL END) mkt_acc1,' ||
              'sum(CASE WHEN s.comp_type_id in (10) THEN cd.case_deal_perc ELSE NULL END) ebspricelist118,' ||
              'sum(CASE WHEN s.comp_type_id in (11) THEN cd.case_deal_amt ELSE NULL END) ebspricelist122,' ||
              'sum(CASE WHEN s.comp_type_id in (12) THEN cd.case_deal_perc ELSE NULL END) ff_input1,' ||
              'sum(CASE WHEN s.comp_type_id in (13) THEN cd.case_deal_amt ELSE NULL END) cust_input3,' ||
              'sum(CASE WHEN s.comp_type_id in (14) THEN cd.case_deal_perc ELSE NULL END) mkt_acc3,' ||
              'min(CASE WHEN s.comp_type_id in (15) THEN cd.case_deal_amt ELSE NULL END) ebspricelist104,' ||
              'sum(CASE WHEN s.comp_type_id in (16) THEN cd.case_deal_perc ELSE NULL END) mkt_acc2 ' ||
              'FROM contract_data cd, comp_type_seq s ' ||
              'WHERE cd.contract_group_id = ' || v_contract_group_id ||
              ' AND cd.comp_type_seq_id = s.comp_type_seq_id '||
                v_extra_where_SQL ||
              'GROUP BY cd.item_id, cd.location_id, cd.sales_date';

            dynamic_ddl (sql_str);
            COMMIT;
        

    V_STATUS      := 'Step 17 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);  
  
     --Merge DATA into SD       
            sql_str := 'MERGE /*+ parallel(4) */ INTO sales_data sd 
                        USING(SELECT /*+ parallel(4) */ * FROM RR_CON_D_SYNC_TEMP_' || user_id || ') sd1
                        ON (sd.item_id = sd1.item_id
                        AND sd.location_id = sd1.location_id
                        AND sd.sales_date  = sd1.sales_date)
                        WHEN MATCHED THEN
                        UPDATE SET  
                        sd.ebspricelist104 = sd1.ebspricelist104,
                        sd.ebspricelist111 = sd1.ebspricelist111,
                        sd.ebspricelist112 = sd1.ebspricelist112,
                        sd.ebspricelist113 = sd1.ebspricelist113,
                        sd.ebspricelist114 = sd1.ebspricelist114,
                        sd.ebspricelist115 = sd1.ebspricelist115,
                        sd.ebspricelist116 = sd1.ebspricelist116,
                        sd.ebspricelist117 = sd1.ebspricelist117,
                        sd.ebspricelist118 = sd1.ebspricelist118,
                        sd.ebspricelist121 = sd1.ebspricelist121,
                        sd.ebspricelist122 = sd1.ebspricelist122,
                        sd.mkt_acc1 = sd1.mkt_acc1,
                        sd.mkt_acc3 = sd1.mkt_acc3,
                        sd.ff_input1 = sd1.ff_input1,
                        sd.cust_input3 = sd1.cust_input3,
                        sd.mkt_acc2 = sd1.mkt_acc2,
                        sd.last_update_date = sysdate
                        WHERE
                        nvl(sd.ebspricelist104,0) <> nvl(sd1.ebspricelist104,0) or
                        nvl(sd.ebspricelist111,0) <> nvl(sd1.ebspricelist111,0) or 
                        nvl(sd.ebspricelist112,0) <> nvl(sd1.ebspricelist112,0) or
                        nvl(sd.ebspricelist113,0) <> nvl(sd1.ebspricelist113,0) or
                        nvl(sd.ebspricelist114,0) <> nvl(sd1.ebspricelist114,0) or
                        nvl(sd.ebspricelist115,0) <> nvl(sd1.ebspricelist115,0) or
                        nvl(sd.ebspricelist116,0) <> nvl(sd1.ebspricelist116,0) or
                        nvl(sd.ebspricelist117,0) <> nvl(sd1.ebspricelist117,0) or
                        nvl(sd.ebspricelist118,0) <> nvl(sd1.ebspricelist118,0) or
                        nvl(sd.ebspricelist121,0) <> nvl(sd1.ebspricelist121,0) or
                        nvl(sd.ebspricelist122,0) <> nvl(sd1.ebspricelist122,0) or
                        nvl(sd.mkt_acc1,0) <> nvl(sd1.mkt_acc1,0) or
                        nvl(sd.mkt_acc3,0) <> nvl(sd1.mkt_acc3,0) or
                        nvl(sd.ff_input1,0) <> nvl(sd1.ff_input1,0) or
                        nvl(sd.cust_input3,0) <> nvl(sd1.cust_input3,0) or
                        nvl(sd.mkt_acc2,0) <> nvl(sd1.mkt_acc2,0)
                        WHEN NOT MATCHED THEN 
                        INSERT(item_id, location_id, sales_date, ebspricelist104, ebspricelist111, ebspricelist112, ebspricelist113, 
                        ebspricelist114, ebspricelist115, ebspricelist116, ebspricelist117, ebspricelist118, 
                        ebspricelist121, ebspricelist122, mkt_acc1, mkt_acc3, ff_input1, cust_input3,mkt_acc2, last_update_date
                        )
                        VALUES(sd1.item_id, sd1.location_id, sd1.sales_date, sd1.ebspricelist104, sd1.ebspricelist111, sd1.ebspricelist112, sd1.ebspricelist112, 
                        sd1.ebspricelist114, sd1.ebspricelist115, sd1.ebspricelist116, sd1.ebspricelist117, sd1.ebspricelist118,
                        sd1.ebspricelist121, sd1.ebspricelist122, sd1.mkt_acc1, sd1.mkt_acc3, sd1.ff_input1, sd1.cust_input3, sd1.mkt_acc2, sysdate)';   

      dynamic_ddl (sql_str);
      COMMIT;
         
 ------------------------------------------
-- Add in New Combinations
------------------------------------------
      
          V_STATUS      := 'Step 18 ';
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);  
     
      
   dynamic_ddl ('Truncate TABLE mdp_load_assist');
   dynamic_ddl ('INSERT INTO mdp_load_assist(item_id, location_id)
                SELECT DISTINCT item_id, location_id FROM v_tmp_cont_comb_' || user_id || ' where new_comb = 1');

   COMMIT;
   
   select count(*) into v_mdp_assist from mdp_load_assist;
   
   if v_mdp_assist <> 0 then
   
      pkg_common_utils.prc_mdp_add ('mdp_load_assist', p_start_date, p_end_date);
      
   end if;
   
   
   --------------------------------------
   ---Clear all Temp Tables
   -------------------------------------
    
    check_and_drop('v_tmp_cont_data_' || user_id || '');        
    check_and_drop('RR_CON_D_SYNC_TEMP_' || user_id || '');
    check_and_drop('v_tmp_cont_sync_ids_' || user_id);  
    check_and_drop('v_tmp_cont_miss_comb_' || user_id || ''); 
    check_and_drop('v_tmp_cont_miss_' || user_id || ''); 
    check_and_drop('v_tmp_cont_comb_' || user_id || '');
    
                  
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_sync_contract_to_sd;


PROCEDURE prc_sync_approve_matrix
as
  v_prog_name   VARCHAR2 (100) := 'PRC_SYNC_APPROVE_MATRIX';
  v_status      VARCHAR2 (100);
  v_proc_log_id NUMBER;

begin
  pre_logon;
  V_STATUS      := 'Start ';
  V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX(V_PROG_NAME, V_PACKAGE_NAME, V_STATUS || SYSDATE);
  
   
   MERGE INTO approve_matrix m using (
     select * from user_id where dcm_product_id = 158 and user_id not in (389,469,6740,6920,7455)
   ) u
   on (m.approve_matrix_id = u.user_id)
   when matched then update
   set approve_matrix_desc = user_name,
       last_update_date = sysdate
   WHERE approve_matrix_desc <> user_name
   WHEN NOT MATCHED THEN INSERT
   (approve_matrix_id, approve_matrix_code, approve_matrix_desc,last_update_date)
   VALUES
   (u.user_id,u.user_id,u.user_name,sysdate);
   
   commit;
 
  v_status             := 'end ';
  v_proc_log_id        := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    V_PROC_LOG_ID := RR_PKG_PROC_LOG.FCN_DBEX (V_PROG_NAME, V_PACKAGE_NAME, 'Fatal Error in Step: ' || V_STATUS, TO_CHAR(SQLCODE), TO_CHAR(SQLCODE), DBMS_UTILITY.FORMAT_ERROR_STACK, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    RAISE;
  end;
END prc_sync_approve_matrix;

END;

/
