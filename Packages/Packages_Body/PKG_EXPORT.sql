--------------------------------------------------------
--  DDL for Package Body PKG_EXPORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEMANTRA"."PKG_EXPORT" 
AS
/******************************************************************************
   NAME:         PKG_EXPORT  BODY
   PURPOSE:      All procedures commanly used for PTP module
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial Version

   ******************************************************************************/

  PROCEDURE prc_export_accrual(p_start_date  DATE,
                               p_end_date    DATE,
                               p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_ACCRUAL
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;

   v_eng_profile    NUMBER := 1;

   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_ACCRUAL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;


    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss')
    INTO max_sales_date
    FROM DUAL;

   MERGE INTO accrual_data ad
   USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_vol_ad, 0), 2) - NVL(ad.rr_accrual_vol_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_vol_ad_gl, 0)
                END rr_accrual_vol_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_cd_ad, 0), 2) - NVL(ad.rr_accrual_cd_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_cd_ad_gl, 0)
                END rr_accrual_cd_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_coop_ad, 0), 2) - NVL(ad.rr_accrual_coop_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_coop_ad_gl, 0)
                END rr_accrual_coop_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_hf_cd_ad, 0), 2) - NVL(ad.rr_accrual_hf_cd_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_hf_cd_ad_gl, 0)
                END rr_accrual_hf_cd_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_hf_coop_ad, 0), 2) - NVL(ad.rr_accrual_hf_coop_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_hf_coop_ad_gl, 0)
                END rr_accrual_hf_coop_post_gl
         FROM accrual_data ad,
              promotion_stat ps,
              promotion_type pt,
              promotion p
         WHERE 1 = 1
         AND ad.accrual_type_id = pt.promotion_type_id
         AND ad.accrual_stat_id = ps.promotion_stat_id
         AND p.promotion_id (+) = ad.accrual_id
         AND ps.rr_export_exclude = 0
         AND p.scenario_id IN (22, 262, 162)
         --AND pt.rr_export_accrual = 1
         --AND ps.rr_export_accrual = 1
         AND ad.sales_date BETWEEN p_start_date AND p_end_date) ad1
   ON(ad.accrual_id = ad1.accrual_id
   AND ad.item_id = ad1.item_id
   AND ad.location_id = ad1.location_id
   AND ad.sales_date = ad1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET ad.rr_accrual_vol_post_gl = ad1.rr_accrual_vol_post_gl,
   ad.rr_accrual_cd_post_gl = ad1.rr_accrual_cd_post_gl, ad.rr_accrual_coop_post_gl = ad1.rr_accrual_coop_post_gl,
   ad.rr_accrual_hf_cd_post_gl = ad1.rr_accrual_hf_cd_post_gl, ad.rr_accrual_hf_coop_post_gl = ad1.rr_accrual_hf_coop_post_gl;



    --- Export table code to be inserted here

    insert into T_EXP_GL(BATCH_ID, MATERIAL, CUSTOMER_NO, PROMOTION_ID,SALES_ORG,DIST_CHANNEL,DIVISION, SALES_DATE, CASE_DEAL_AMOUNT, COOP_AMOUNT,
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    select V_BATCH_ID, I.I_ATT_1 , S.E1_CUST_CAT_4, P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,
    PD.SALES_DATE, PD.RR_ACCRUAL_CD_POST_GL, PD.RR_ACCRUAL_COOP_POST_GL,
     pt.promotion_type_desc, cdt.deal_type_code, coopt.deal_type_code, v_batch_date, 'ACCRUAL',rec_cust.e1_cust_cat_4
    from T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    t_ep_p3 dc,
    PROMOTION P,
    activity ac,
    t_ep_e1_cust_cat_4 rec_cust,
    accrual_data pd,
    promotion_type pt,
    promotion_stat ps,
    MDP_MATRIX MDP,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
    and P.SCENARIO_ID in (22, 262, 162)
    and p.activity_id = ac.activity_id
    and p.recipient = rec_cust.t_ep_e1_cust_cat_4_ep_id
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND ps.promotion_stat_id = p.promotion_stat_id
    AND ps.rr_export_exclude = 0
    AND pd.sales_date BETWEEN p_start_date AND p_end_date
    and P.PROMOTION_TYPE_ID = PT.PROMOTION_TYPE_ID
    AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
  --  AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
    AND ABS(NVL(pd.rr_accrual_cd_post_gl, 0) ) +
        ABS(NVL(pd.rr_accrual_coop_post_gl, 0) )
         <> 0;

   MERGE INTO accrual_data ad
   USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_vol_ad, 0), 2)
                ELSE 0
                END rr_accrual_vol_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_cd_ad, 0), 2)
                ELSE 0
                END rr_accrual_cd_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_coop_ad, 0), 2)
                ELSE 0
                END rr_accrual_coop_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_hf_cd_ad, 0), 2)
                ELSE 0
                END rr_accrual_hf_cd_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_hf_coop_ad, 0), 2)
                ELSE 0
                END rr_accrual_hf_coop_ad_gl
         FROM accrual_data ad,
              promotion_stat ps,
              promotion_type pt,
              promotion p
         WHERE 1 = 1
         AND ad.accrual_type_id = pt.promotion_type_id
         AND ad.accrual_stat_id = ps.promotion_stat_id
         AND ps.rr_export_exclude = 0
         AND p.promotion_id (+) = ad.accrual_id
         AND p.scenario_id IN (22, 262, 162)
         --AND pt.rr_export_accrual = 1
         --AND ps.rr_export_accrual = 1
         AND ad.sales_date BETWEEN p_start_date AND p_end_date) ad1
   ON(ad.accrual_id = ad1.accrual_id
   AND ad.item_id = ad1.item_id
   AND ad.location_id = ad1.location_id
   AND ad.sales_date = ad1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET ad.rr_accrual_vol_ad_gl = ad1.rr_accrual_vol_ad_gl,
   ad.rr_accrual_cd_ad_gl = ad1.rr_accrual_cd_ad_gl, ad.rr_accrual_coop_ad_gl = ad1.rr_accrual_coop_ad_gl,
   ad.rr_accrual_hf_cd_ad_gl = ad1.rr_accrual_hf_cd_ad_gl, ad.rr_accrual_hf_coop_ad_gl = ad1.rr_accrual_hf_coop_ad_gl,
   ad.accrual_export_date = CASE WHEN NVL(ad.rr_accrual_vol_ad_gl, 0) <> NVL(ad1.rr_accrual_vol_ad_gl, 0)
                                  OR NVL(ad.rr_accrual_cd_ad_gl, 0) <> NVL(ad1.rr_accrual_cd_ad_gl, 0)
                                  OR NVL(ad.rr_accrual_coop_ad_gl, 0) <> NVL(ad1.rr_accrual_coop_ad_gl, 0)
                                  OR NVL(ad.rr_accrual_hf_cd_ad_gl, 0) <> NVL(ad1.rr_accrual_hf_cd_ad_gl, 0)
                                  OR NVL(ad.rr_accrual_hf_coop_ad_gl, 0) <> NVL(ad1.rr_accrual_hf_coop_ad_gl, 0)
                               THEN SYSDATE
                               ELSE ad.accrual_export_date END;


    INSERT INTO t_exp_gl_master
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER
                 FROM t_exp_gl WHERE batch_id = v_batch_id);


   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

 PROCEDURE prc_export_oi(p_start_date  DATE DEFAULT NULL,
                                 p_end_date    DATE DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_OI
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;

   v_eng_profile    NUMBER := 1;

   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   v_insert_sql varchar2(32762);
v_merge_sql varchar2(32762);
v_select_sql1 varchar2(32762);
v_from_sql1 varchar2(32762);
v_extra_from_sql varchar2(32762);
v_oi_seq_val number;
   V_VAKEY          VARCHAR2(300);
   v_sql_str VARCHAR2(32000);
   V_SQL_STR1 VARCHAR2(32000);
   v_sql_str2 VARCHAR2(32000);
   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_OI';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;


   -- dynamic_ddl('TRUNCATE TABLE t_exp_oi');

    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_oi', 0, 'ES');

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    ---

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss')
    INTO max_sales_date
    FROM DUAL;

--- Export table code to be inserted here

--    INSERT INTO t_exp_oi(batch_id, product_code, customer_no, promotion_id, sales_date,
--                         promo_start_date, promo_end_date, case_deal_oi, case_deal_oi_per, oi_deal_type,
--                         min_vol, max_vol, posted_date)
--    SELECT /*+ FULL(pd) PARALLEL(pd, 64) */ v_batch_id, i.item, s.site, p.promotion_code,
--    pd.sales_date, pdt.from_date,  pdt.until_date,
--    pd.case_buydownoi, pd.rr_oi_p, oit.oi_deal_type_desc,
--    pd.rr_minvol_oi, decode(nvl(pd.rr_maxvol_oi,0),0,9999999999999.99,pd.rr_maxvol_oi), v_batch_date
--    FROM t_ep_item i,
--    t_ep_site s,
--    promotion p,
--    promotion_data pd,
--    mdp_matrix mdp,
--    tbl_oi_deal_type oit,
--    promotion_dates pdt,
--    promotion_stat ps
--    WHERE pd.item_id = mdp.item_id
--    AND pd.location_id = mdp.location_id
--    AND pd.promotion_id = p.promotion_id
--    AND pdt.promotion_id = p.promotion_id
--    AND p.promotion_stat_id =ps.promotion_stat_id
--    AND ps.rr_export_oi =1
--    AND p.scenario_id IN (22, 262, 162)
--    AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
--    AND mdp.t_ep_site_ep_id = s.t_ep_site_ep_id
--    AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
--    AND pd.sales_date BETWEEN TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS') + 7 AND TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS') + 14
--    AND ABS(NVL(pd.case_buydownoi, 0)) + ABS(NVL(pd.rr_oi_p, 0)) <> 0;
--
--    COMMIT;
--
    /*INSERT INTO t_exp_oi_master
                (SELECT v_batch_id, product_code, customer_no, activity_type, promotion_id, sales_date,
                         promo_start_date, promo_end_date, case_deal_oi, case_deal_oi_per,
                         min_vol, max_vol, posted_date
                 FROM t_exp_gl WHERE batch_id = v_batch_id);*/

--   SELECT COUNT (*)
--     INTO v_no_records_exp
--     FROM t_exp_oi
--     WHERE batch_id = v_batch_id;
 for rec in
  (SELECT  p.promotion_id
  FROM promotion p,
    promotion_stat ps
  where P.ACTIVITY_ID     = 2
   AND P.OI_PROMO_STATUS = 1
   and (P.RR_EXP_OI_FLAG in (0,1,2,3) or (p.promotion_stat_id = 4 and rr_Exp_oi_flag = 0))
  and p.promotion_stat_id = ps.promotion_stat_id
  --AND ps.rr_export_oi     =1
  )
  loop
  v_insert_sql := 'INSERT INTO rr_exp_oi_t(promotion_id ';

      v_select_sql1 := 'select distinct pm.promotion_id ';
      v_from_sql1 := '  mdp_matrix m,promotion_matrix pm,promotion p
      where m.item_id = pm.item_id
      and m.location_id = pm.location_id
      and pm.promotion_id= p.promotion_id
      and pm.promotion_id = '||rec.promotion_id;
      v_extra_from_sql := '';
    for rec1 in
    (SELECT pl.level_id,
      gt.id_field
    FROM promotion_levels pl,
      group_tables gt
    WHERE pl.level_id     = gt.group_table_id
    AND pl.promotion_id = rec.promotion_id
    )
    loop

      v_insert_sql := v_insert_sql||','||rec1.id_field;
      v_select_sql1 := v_select_sql1||',m.'||rec1.id_field;
      v_extra_from_sql := v_extra_from_sql||' and m.'||rec1.id_field||' = t.'||rec1.id_field ;
      v_from_sql1 := v_from_sql1 || ' AND m.'||rec1.id_field||' IN (';

      for rec2 in (select pm.member_id
      from promotion_members pm, promotion_levels pl where pm.filter_id  = pl.filter_id
      and pl.level_id = rec1.level_id
      and pl.promotion_id = rec.promotion_id)
      loop
          v_from_sql1 := v_from_sql1 ||rec2.member_id ||',';
      end loop;

      v_from_sql1 := rtrim(v_from_sql1,',') ||')' ;

--       RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,'merge INTO promotion_matrix pm1 USING
--      ( '||v_select_sql1||', pm.item_id,pm.location_id from rr_exp_oi_t t,'||v_from_sql1||' and t.promotion_id = pm.promotion_id '||v_extra_from_sql||'
--      )
--      pm2 ON (pm1.item_id = pm2.item_id AND pm1.location_id = pm2.location_id AND pm1.promotion_id = pm2.promotion_id )
--    WHEN matched THEN
--      UPDATE SET pm1.seq_id = pm2.oi_seq_id');
      END LOOP;
    --   v_from_sql1 := v_from_sql1||' AND not exists (select 1 from rr_Exp_oi_t tmp where tmp.promotion_id = p.promotion_id)';
      V_INSERT_SQL := V_INSERT_SQL ||',sales_org_id,dist_chan_id,div_id )';
             dbms_output.put_line(v_insert_sql||v_select_sql1||',m.t_ep_p4_ep_id,m.t_ep_p3_ep_id,m.t_ep_p2_ep_id'||' from '||v_from_sql1);
             RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,V_INSERT_SQL||V_SELECT_SQL1||',m.t_ep_p4_ep_id,m.t_ep_p3_ep_id,m.t_ep_p2_ep_id'||' from '||V_FROM_SQL1||' AND not exists (select 1 from rr_Exp_oi_t tmp where tmp.promotion_id = p.promotion_id)');
             DYNAMIC_DDL(V_INSERT_SQL||V_SELECT_SQL1||',m.t_ep_p4_ep_id,m.t_ep_p3_ep_id,m.t_ep_p2_ep_id'||' from '||V_FROM_SQL1||' AND not exists (select 1 from rr_Exp_oi_t tmp where tmp.promotion_id = p.promotion_id)');

             COMMIT;

              RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,'merge INTO promotion_matrix pm1 USING
      ( '||v_select_sql1||', pm.item_id,pm.location_id, t.oi_seq_id from rr_exp_oi_t t,'||v_from_sql1||' and t.promotion_id = pm.promotion_id '||v_extra_from_sql||'
      )
      pm2 ON (pm1.item_id = pm2.item_id AND pm1.location_id = pm2.location_id AND pm1.promotion_id = pm2.promotion_id )
    WHEN matched THEN
      UPDATE SET pm1.oi_seq_id = pm2.oi_seq_id');

      dynamic_ddl('merge INTO promotion_matrix pm1 USING
      ( '||v_select_sql1||', pm.item_id,pm.location_id, t.oi_seq_id from rr_exp_oi_t t,'||v_from_sql1||' and t.promotion_id = pm.promotion_id '||v_extra_from_sql||'
      )
      pm2 ON (pm1.item_id = pm2.item_id AND pm1.location_id = pm2.location_id AND pm1.promotion_id = pm2.promotion_id )
    WHEN matched THEN
      UPDATE SET pm1.oi_seq_id = pm2.oi_seq_id');
COMMIT;
END LOOP;

sql_str := 'merge into promotion_matrix pm1 using (
select pm.promotion_id,pm.item_id.pm.location_id,t.oi_seq_id from promotion_matrix pm,rr_exp_oi_t t,mdp_matrix m
where m.item_id = pm.item_id
and m.location_id = pm.location_id
and t.promotion_id = pm.promotion_id
and nvl(t.t_ep_p4_ep_id,m.t_ep_p4_ep_id) = m.t_ep_p4_ep_id
and nvl(t.t_ep_site_ep_id,m.t_ep_site_ep_id) = m.t_ep_site_ep_id
and nvl(t.t_ep_ebs_customer_ep_id,m.t_ep_ebs_customer_ep_id) = m.t_ep_ebs_customer_ep_id
and nvl(t.t_ep_p3_ep_id,m.t_ep_p3_ep_id) = m.t_ep_p3_ep_id
and nvl(t.t_ep_e1_item_cat_4_ep_id,m.t_ep_e1_item_cat_4_ep_id) = m.t_ep_e1_item_cat_4_ep_id
and nvl(t.t_ep_e1_item_cat_6_ep_id,m.t_ep_e1_item_cat_6_ep_id) = m.t_ep_e1_item_cat_6_ep_id
and nvl(t.t_ep_ebs_prod_cat_ep_id,m.t_ep_ebs_prod_cat_ep_id) = m.t_ep_ebs_prod_cat_ep_id
and nvl(t.t_ep_e1_item_cat_7_ep_id,m.t_ep_e1_item_cat_7_ep_id) = m.t_ep_e1_item_cat_7_ep_id
and nvl(t.t_ep_p2_ep_id,m.t_ep_p2_ep_id) = m.t_ep_p2_ep_id
and nvl(t.t_ep_i_att_6_ep_id,m.t_ep_i_att_6_ep_id) = m.t_ep_i_att_6_ep_id
and nvl(t.t_ep_e1_cust_cat_2_ep_id,m.t_ep_e1_cust_cat_2_ep_id) = m.t_ep_e1_cust_cat_2_ep_id
and nvl(t.t_ep_item_ep_id,m.t_ep_item_ep_id) = m.t_ep_item_ep_id
and nvl(t.t_ep_i_att_10_ep_id,m.t_ep_i_att_10_ep_id) = m.t_ep_i_att_10_ep_id
and nvl(t.t_ep_ebs_account_ep_id,m.t_ep_ebs_account_ep_id) = m.t_ep_ebs_account_ep_id
and nvl(t.t_ep_e1_cust_ctry_ep_id,m.t_ep_e1_cust_ctry_ep_id) = m.t_ep_e1_cust_ctry_ep_id
and nvl(t.t_ep_e1_cust_city_ep_id,m.t_ep_e1_cust_city_ep_id) = m.t_ep_e1_cust_city_ep_id
and nvl(t.t_ep_e1_cust_cat_4_ep_id,m.t_ep_e1_cust_cat_4_ep_id) = m.t_ep_e1_cust_cat_4_ep_id
and nvl(t.t_ep_l_att_6_ep_id,m.t_ep_l_att_6_ep_id) = m.t_ep_l_att_6_ep_id) pm2
on(pm1.item_id = pm2.item_id
and pm1.location_id = pm2.location_id
and pm1.promotion_id = pm2.promotion_id)
when matched then update set pm1.oi_seq_id = pm2.oi_seq_id';


FOR rec IN (SELECT DISTINCT p.access_Seq,
                 DECODE(NVL (T_EP_P4_EP_ID, 0), 0, 0, 1) p4_flag,
                 DECODE(NVL (t_ep_e1_cust_cat_4_EP_ID, 0), 0, 0, 1) site_flag,
                 DECODE(NVL (T_EP_EBS_CUSTOMER_EP_ID, 0), 0, 0, 1) ebs_cust_flag,
                 DECODE(NVL (T_EP_P3_EP_ID, 0), 0, 0, 1) p3_flag,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_4_EP_ID, 0), 0, 0, 1) E1_I4_FLAG,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_6_EP_ID, 0), 0, 0, 1) e1_i6_flag,
                 DECODE(NVL (T_EP_EBS_PROD_CAT_EP_ID, 0), 0, 0, 1) ebs_pc_flag,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_7_EP_ID, 0), 0, 0, 1) e1_i7_flag,
                 DECODE(NVL (T_EP_P2_EP_ID, 0), 0, 0, 1) p2_flag,
                 DECODE(NVL (T_EP_I_ATT_6_EP_ID, 0), 0, 0, 1) i_att6_flag,
                 DECODE(NVL (T_EP_E1_CUST_CAT_2_EP_ID, 0), 0, 0, 1) e1_cust2_flag,
                 DECODE(NVL (T_EP_ITEM_EP_ID, 0), 0, 0, 1) item_flag,
                 DECODE(NVL (T_EP_I_ATT_10_EP_ID, 0), 0, 0, 1) I_ATT10_FLAG,
                 DECODE(NVL (T_EP_EBS_ACCOUNT_EP_ID, 0), 0, 0, 1) ACNT_FLAG,
                 DECODE(NVL (T_EP_E1_CUST_CTRY_EP_ID, 0), 0, 0, 1) CTRY_FLAG,
                  DECODE(NVL (t_ep_L_ATT_6_ep_id, 0), 0, 0, 1) l_att6_FLAG,
                 DECODE(NVL (t_ep_e1_cust_city_ep_id, 0), 0, 0, 1) city_FLAG
                from rr_exp_oi_t t,promotion p
                where p.activity_id = 2
               -- and CONTRACT_GROUP_ID = 68
                and t.promotion_id = p.promotion_id
              --  and contract_exp_Stat_id = 0
                AND   NVL(T_EP_P4_EP_ID,0)+
                NVL(T_EP_SITE_EP_ID,0)+
                nvl(T_EP_EBS_CUSTOMER_EP_ID,0)+
                NVL(T_EP_P3_EP_ID,0)+
                nvl(T_EP_E1_ITEM_CAT_4_EP_ID,0)+
                nvl(T_EP_E1_ITEM_CAT_6_EP_ID,0)+
                nvl(T_EP_EBS_PROD_CAT_EP_ID,0)+
                NVL(T_EP_E1_ITEM_CAT_7_EP_ID,0)+
                nvl(T_EP_P2_EP_ID,0)+
                nvl(T_EP_I_ATT_6_EP_ID,0)+
                NVL(T_EP_E1_CUST_CAT_2_EP_ID,0)+
                NVL(T_EP_ITEM_EP_ID,0)+
                NVL(T_EP_I_ATT_10_EP_ID,0)+
                NVL(T_EP_EBS_ACCOUNT_EP_ID,0)  +
                NVL(T_EP_E1_CUST_CTRY_EP_ID,0)+
                nvl(t_ep_L_ATT_6_ep_id,0) +
                nvl(t_ep_e1_cust_city_ep_id,0) > 0
                /*Add export_status condition*/
                )
    LOOP

    for REC1 in (
    SELECT CASE WHEN ASQ.LEVEL1 IS NOT NULL THEN CASE WHEN GT1.TRAILING_SPACES = 1 THEN 'RPAD('||GT1.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt1.num_chars_limit else GT1.NUM_CHARS end||','' '')'
     WHEN GT1.LEADING_ZEROES = 1 THEN 'lPAD('||GT1.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt1.num_chars_limit else GT1.NUM_CHARS end||',''0'')' END END||'||'||
     CASE WHEN ASQ.LEVEL2 IS NOT NULL THEN  CASE WHEN GT2.TRAILING_SPACES = 1 THEN 'RPAD('||GT2.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt2.num_chars_limit else GT2.NUM_CHARS end||','' '')'
     WHEN GT2.LEADING_ZEROES = 1 THEN 'lPAD('||case when asq.char_limit_flag = 1 then gt2.num_chars_limit else GT1.NUM_CHARS end||','||GT2.NUM_CHARS||',''0'')' END ||'||' END||
       CASE WHEN ASQ.LEVEL3 IS NOT NULL THEN  CASE WHEN GT3.TRAILING_SPACES = 1 THEN 'RPAD('||GT3.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt3.num_chars_limit else GT3.NUM_CHARS end||','' '')'
     WHEN GT3.LEADING_ZEROES = 1 THEN 'lPAD('||GT3.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt3.num_chars_limit else GT3.NUM_CHARS end||',''0'')' END ||'||' end||
       CASE WHEN ASQ.LEVEL4 IS NOT NULL THEN  CASE WHEN GT4.TRAILING_SPACES = 1 THEN 'RPAD('||GT4.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt4.num_chars_limit else GT4.NUM_CHARS end||','' '')'
     WHEN GT4.LEADING_ZEROES = 1 THEN 'lPAD('||GT4.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt4.num_chars_limit else GT4.NUM_CHARS end||',''0'')' END ||'||'end||
     CASE WHEN ASQ.LEVEL5 IS NOT NULL THEN  CASE WHEN GT5.TRAILING_SPACES = 1 THEN 'RPAD('||GT5.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt5.num_chars_limit else GT5.NUM_CHARS end||','' '')'
     WHEN GT5.LEADING_ZEROES = 1 THEN 'lPAD('||GT5.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt5.num_chars_limit else GT5.NUM_CHARS end||',''0'')' END||'||'  end||
     CASE WHEN ASQ.LEVEL6 IS NOT NULL THEN  CASE WHEN GT6.TRAILING_SPACES = 1 THEN 'RPAD('||GT6.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt6.num_chars_limit else GT6.NUM_CHARS end||','' '')'
     WHEN GT6.LEADING_ZEROES = 1 THEN 'lPAD('||GT6.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt6.num_chars_limit else GT6.NUM_CHARS end||',''0'')' END||'||'  end||
     CASE WHEN ASQ.LEVEL7 IS NOT NULL THEN  CASE WHEN GT7.TRAILING_SPACES = 1 THEN 'RPAD('||GT7.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt7.num_chars_limit else GT7.NUM_CHARS end||','' '')'
     WHEN GT7.LEADING_ZEROES = 1 THEN 'lPAD('||GT7.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt7.num_chars_limit else GT7.NUM_CHARS end||',''0'')' END END  VAKEY
     --     gt2.code_field, gt3.code_field,gt4.code_Field
    FROM ACCESS_SEQ ASQ, rr_vakey_ref_t GT1,rr_vakey_ref_t GT2,rr_vakey_ref_t GT3,rr_vakey_ref_t GT4,rr_vakey_ref_t GT5,rr_vakey_ref_t GT6,rr_vakey_ref_t GT7
    WHERE NVL(ASQ.LEVEL1,1) = GT1.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL2,1) = GT2.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL3,1) = GT3.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL4,1) = GT4.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL5,1) = GT5.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL6,1) = GT6.GROUP_TABLE_ID
    AND NVL(ASQ.LEVEL7,1) = GT7.GROUP_TABLE_ID
    and asq.access_seq_id = rec.access_seq)
    LOOP

    V_VAKEY := rtrim(REC1.VAKEY,'||');


    v_sql_str1 := ' INSERT INTO T_EXP_OI(REBATE_PROMO_ID ,CONTRACT_COMP_TYPE,CONTRACT_COMP_LINE_ID,START_DATE
                        ,END_DATE,CONTRACT_PROMO_FLAG,MATERIAL_CODE,PROD_PACK_LVL3_CODE
                        ,PROD_PACK_LVL2_CODE,PROD_PACK_LVL1_CODE,PROD_BRAND_LVL3_CODE,PROD_BRAND_LVL2_CODE
                        ,PROD_BRAND_LVL1_CODE,CUSTOMER_CODE,CUSTOMER_LEVEL4_CODE,CUSTOMER_LEVEL3_CODE
                        ,CUSTOMER_LEVEL2_CODE,CUSTOMER_LEVEL1_CODE,SALES_OFFICE_CODE,TIER_CODE,SALES_ORG
                        ,DIST_CHANNEL,DIVISION,CUST_PRICING_PROC,TAX_CLASS_MAT,DEAL_TYPE,SAP_CONDITION_TABLE
                        ,PRICE_POINT1,PRICE_POINT2,PRICE_POINT3,CONDITION1,CONDITION2,CONDITION3,LUMP_SUM
                        ,STATUS,SAP_CONDITION_KEY,RECEPIENT_CUSTOMER,REBATE_PAYMENT_COMMENTS
                        ,REBATE_REFERENCE,TAX_CODE,CONT_SALES_ORG,CONT_DIST_CHANNEL,CONT_DIVISION)
                 SELECT to_char(p.promotion_id) ,pT.promotion_TYPE_DESC, t.oi_seq_id, pdt.from_DATE,
                        pdt.until_DATE,''PROMO'','|| CASE WHEN REC.ITEM_FLAG = 0 THEN 'NULL' ELSE 'LPAD(MAT.ITEM,18,''0'')' END || ','||
                        CASE WHEN REC.E1_I7_FLAG = 0 THEN 'NULL' ELSE ' RPAD(PROD_PACK3.E1_ITEM_CAT_7,18,'' '')'  END || ','||
                        CASE WHEN REC.I_ATT10_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK2.I_ATT_10,18,'' '')'  END || ','||
                        CASE WHEN REC.EBS_PC_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK1.EBS_PROD_CAT,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND3.E1_ITEM_CAT_6,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND2.E1_ITEM_CAT_4,18,'' '')' END || ',
                        NULL,'||CASE WHEN REC.SITE_FLAG = 0 THEN 'NULL' ELSE 'LPAD(SITE.e1_cust_cat_4,10,''0'')'  END || ','||
                         CASE WHEN REC.ACNT_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL4.EBS_ACCOUNT,10,''0'')'  END || ','||
                         CASE WHEN REC.EBS_CUST_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL3.EBS_CUSTOMER,10,''0'')' END || ','||
                         CASE WHEN REC.city_FLAG = 0 THEN 'NULL' ELSE 'RPAD(CUST_LVL2.e1_cust_city,10,'' '')'  END || ',
                        NULL,'||CASE WHEN REC.l_att6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_OFF.L_ATT_6,4,'' '')'  END || ','||
                        CASE WHEN REC.E1_CUST2_FLAG = 0 THEN 'NULL' ELSE 'LPAD(TIER.E1_CUST_CAT_2,2,''0'')'  END || ','||
                        CASE WHEN REC.P4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_ORG.P4,4,'' '')'  END || ','||
                        CASE WHEN REC.P3_FLAG = 0 THEN 'NULL' ELSE 'rpad(DIST_CHAN.P3,2,'' '')'  END || ','||
                        CASE WHEN REC.P2_FLAG = 0 THEN 'NULL' ELSE 'rpad(DIV.P2,2,'' '') '  END || ','||
                        case when REC.CTRY_FLAG = 0 then 'NULL' else 'rpad(PRIC_PRC.E1_CUST_CTRY,1,'' '') '  end || ','||
                        CASE WHEN REC.I_ATT6_FLAG = 0 THEN 'NULL' ELSE 'lpad(TAX.I_ATT_6,1,''0'') '  END || ', ''ZPM1'',ACC.SAP_TABLE,
                        max(pd.rr_minvol_oi),max(pd.rr_vol_oi_lvl2),max(pd.rr_maxvol_oi),
                       DECODE(max(nvl(pd.case_buydownOI,0)),0,max(nvl(pd.rr_oi_p,0)),max(nvl(pd.case_buydownOI,0))),DECODE(max(NVL(PD.CD_OI_LVL2,0)),0,
                       max(NVL(PD.CD_OI_P_LVL2,0)),max(NVL(PD.CD_OI_LVL2,0))),
DECODE(max(nvl(pd.cd_oi_lvl3,0)),0,max(nvl(pd.cd_oi_p_lvl3,0)),max(nvl(pd.cd_oi_lvl3,0))),0,
                        rt.rr_exp_oi_flag_desc ,NULL /*'||V_VAKEY||'*/,decode(cust_num.e1_cust_cat_4,0,null,cust_num.e1_cust_cat_4),p.PAYMENT_COMMENTS,
                        p.investment_details ,tbl.tax_consid_desc, SALES_ORG1.P4,DIST_CHAN1.P3,DIV1.P2
                 FROM promotion p ,promotion_data pd,promotion_matrix pm,
                 promotion_dates pdt,promotion_type pt,
                 rr_Exp_oi_t t, COMP_TYPE CT , tbl_rr_exp_oi_flag_t rt,promotion_STAT ps,  ';
                   v_sql_str2 :='    tbl_tax_consideration tbl,t_ep_e1_cust_cat_4 cust_num,  '||
                       CASE WHEN REC.ITEM_FLAG = 0 THEN NULL ELSE 'T_EP_ITEM MAT,' END ||
                       CASE WHEN REC.E1_I7_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_7 PROD_PACK3, ' END ||
                       case when REC.I_ATT10_FLAG = 0 then null else 'T_EP_I_ATT_10 PROD_PACK2, ' end ||
                       CASE WHEN REC.ebs_pc_flag = 0 THEN NULL ELSE 'T_EP_EBS_PROD_CAT PROD_PACK1, ' END ||
                       CASE WHEN REC.E1_I6_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_6 PROD_BRAND3, ' END ||
                       CASE WHEN REC.E1_I4_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_4 PROD_BRAND2, ' END ||
                       CASE WHEN REC.SITE_FLAG = 0 THEN NULL ELSE 't_ep_e1_cust_cat_4 SITE,' END ||
                       CASE WHEN REC.ACNT_FLAG = 0 THEN NULL ELSE 'T_EP_EBS_ACCOUNT CUST_LVL4,' END ||
                       CASE WHEN REC.EBS_CUST_FLAG = 0 THEN NULL ELSE 'T_EP_EBS_CUSTOMER CUST_LVL3,' END ||
                       CASE WHEN REC.CITY_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CITY CUST_LVL2,' END ||
                       CASE WHEN REC.l_att6_FLAG = 0 THEN NULL ELSE 't_ep_L_ATT_6 SALES_OFF, ' END ||
                       CASE WHEN REC.E1_CUST2_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CAT_2 TIER,' END ||
                       CASE WHEN REC.P4_FLAG = 0 THEN NULL ELSE 'T_EP_P4 SALES_ORG,' END ||
                       CASE WHEN REC.P3_FLAG = 0 THEN NULL ELSE 'T_EP_P3 DIST_CHAN,' END ||
                       CASE WHEN REC.P2_FLAG = 0 THEN NULL ELSE 'T_EP_P2 DIV,' END ||
                       CASE WHEN REC.ctry_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CTRY PRIC_PRC,' END ||
                       CASE WHEN REC.I_ATT6_FLAG = 0 THEN NULL ELSE 'T_EP_I_ATT_6 tax,' END ||
                       ' T_EP_P4 SALES_ORG1, T_EP_P3 DIST_CHAN1, T_EP_P2 DIV1, access_seq acc
                 WHERE p.promotion_id = pm.promotion_id
--                 and pm.item_id = m.item_id
--                 and pm.location_id = m.location_id
                 and pm.item_id = pd.item_id
                 and p.rr_exp_oi_flag = rt.rr_exp_oi_flag_id
                 and p.rr_exp_oi_flag in (0,1,2,3)
                 and pm.location_id = pd.location_id
                 and pd.promotion_id = p.promotion_id
                 and pdt.promotion_id = p.promotion_id
                 AND p.access_Seq = acc.access_Seq_id
                 and t.oi_seq_id = pm.oi_seq_id
                 and p.promotion_id = t.promotion_id
                 and pt.promotion_type_id = p.promotion_type_id
                 and p.promotion_stat_id = ps.promotion_stat_id
                 AND ps.rr_export_oi     = 1
                 and acc.access_seq_id = '||rec.access_seq||'
                --   and cl.contract_exp_Stat_id = 0
                  AND tbl.tax_consid_id = 1 --c.TAX_CONSIDERATION
                   AND nvl(p.recipient,0) = cust_num.t_ep_e1_cust_cat_4_ep_id '||
                   CASE WHEN REC.ITEM_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_ITEM_EP_ID = MAT.T_EP_ITEM_EP_ID' END ||
                   case when rec.e1_i7_flag = 0 then null else ' and t.T_EP_E1_ITEM_CAT_7_EP_ID = PROD_PACK3.T_EP_E1_ITEM_CAT_7_EP_ID ' end ||
                   case when REC.I_ATT10_FLAG = 0 then null else '  AND t.T_EP_I_ATT_10_EP_ID = PROD_PACK2.T_EP_I_ATT_10_EP_ID ' end ||
                   case when REC.EBS_PC_FLAG = 0 then null else '  AND t.T_EP_EBS_PROD_CAT_EP_ID = PROD_PACK1.T_EP_EBS_PROD_CAT_EP_ID ' end ||
                   case when REC.E1_I6_FLAG = 0 then null else '  AND t.T_EP_E1_ITEM_CAT_6_EP_ID = PROD_BRAND3.T_EP_E1_ITEM_CAT_6_EP_ID ' end ||
                   CASE WHEN REC.E1_I4_FLAG = 0 THEN NULL ELSE '  AND t.T_EP_E1_ITEM_CAT_4_EP_ID = PROD_BRAND2.T_EP_E1_ITEM_CAT_4_EP_ID ' END ||
                   case when REC.SITE_FLAG = 0 then null else '  AND t.t_ep_e1_cust_cat_4_EP_ID = SITE.t_ep_e1_cust_cat_4_EP_ID ' end ||
                   CASE WHEN REC.ACNT_FLAG = 0 THEN NULL ELSE '  AND t.T_EP_EBS_ACCOUNT_EP_ID = CUST_LVL4.T_EP_EBS_ACCOUNT_EP_ID ' END ||
                   CASE WHEN REC.EBS_CUST_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_EBS_CUSTOMER_EP_ID = CUST_LVL3.T_EP_EBS_CUSTOMER_EP_ID ' END ||
                   CASE WHEN REC.city_FLAG = 0 THEN NULL ELSE ' AND t.t_ep_e1_cust_city_ep_id = cust_lvl2.t_ep_e1_cust_city_ep_id  ' END ||
                   CASE WHEN REC.l_att6_FLAG = 0 THEN NULL ELSE ' AND t.t_ep_L_ATT_6_ep_id = sales_off.t_ep_L_ATT_6_ep_id  ' END ||
                   CASE WHEN REC.E1_CUST2_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_E1_CUST_CAT_2_EP_ID = TIER.T_EP_E1_CUST_CAT_2_EP_ID ' END ||
                   CASE WHEN REC.P4_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_P4_EP_ID = SALES_ORG.T_EP_P4_EP_ID ' END ||
                   CASE WHEN REC.P3_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_P3_EP_ID = DIST_CHAN.T_EP_P3_EP_ID ' END ||
                   CASE WHEN REC.P2_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID ' END ||
                   CASE WHEN REC.ctry_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_E1_CUST_CTRY_EP_ID = PRIC_PRC.T_EP_E1_CUST_CTRY_EP_ID ' END ||
                   CASE WHEN REC.i_att6_FLAG = 0 THEN NULL ELSE ' AND t.T_EP_I_ATT_6_EP_ID = TAX.T_EP_I_ATT_6_EP_ID                   ' END ||
                   ' AND t.sales_org_id = SALES_ORG1.T_EP_P4_EP_ID
                    AND t.dist_chan_ID = DIST_CHAN1.T_EP_P3_EP_ID
                    AND t.DIV_ID = DIV1.T_EP_P2_EP_ID
                    group by to_char(p.promotion_id) ,pT.promotion_TYPE_DESC, t.oi_seq_id, pdt.from_DATE,
                        pdt.until_DATE,''PROMO'','|| CASE WHEN REC.ITEM_FLAG = 0 THEN 'NULL' ELSE 'LPAD(MAT.ITEM,18,''0'')' END || ','||
                        CASE WHEN REC.E1_I7_FLAG = 0 THEN 'NULL' ELSE ' RPAD(PROD_PACK3.E1_ITEM_CAT_7,18,'' '')'  END || ','||
                        CASE WHEN REC.I_ATT10_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK2.I_ATT_10,18,'' '')'  END || ','||
                        CASE WHEN REC.EBS_PC_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK1.EBS_PROD_CAT,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND3.E1_ITEM_CAT_6,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND2.E1_ITEM_CAT_4,18,'' '')' END || ',
                        NULL,'||CASE WHEN REC.SITE_FLAG = 0 THEN 'NULL' ELSE 'LPAD(SITE.e1_cust_cat_4,10,''0'')'  END || ','||
                         CASE WHEN REC.ACNT_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL4.EBS_ACCOUNT,10,''0'')'  END || ','||
                         CASE WHEN REC.EBS_CUST_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL3.EBS_CUSTOMER,10,''0'')' END || ','||
                         CASE WHEN REC.city_FLAG = 0 THEN 'NULL' ELSE 'RPAD(CUST_LVL2.e1_cust_city,10,'' '')'  END || ',
                        NULL,'||CASE WHEN REC.l_att6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_OFF.L_ATT_6,4,'' '')'  END || ','||
                        CASE WHEN REC.E1_CUST2_FLAG = 0 THEN 'NULL' ELSE 'LPAD(TIER.E1_CUST_CAT_2,2,''0'')'  END || ','||
                        CASE WHEN REC.P4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_ORG.P4,4,'' '')'  END || ','||
                        CASE WHEN REC.P3_FLAG = 0 THEN 'NULL' ELSE 'rpad(DIST_CHAN.P3,2,'' '')'  END || ','||
                        CASE WHEN REC.P2_FLAG = 0 THEN 'NULL' ELSE 'rpad(DIV.P2,2,'' '') '  END || ','||
                        case when REC.CTRY_FLAG = 0 then 'NULL' else 'rpad(PRIC_PRC.E1_CUST_CTRY,1,'' '') '  end || ','||
                        CASE WHEN REC.I_ATT6_FLAG = 0 THEN 'NULL' ELSE 'lpad(TAX.I_ATT_6,1,''0'') '  END || ', ''ZPM1'',ACC.SAP_TABLE,
                        rt.rr_exp_oi_flag_desc  ,NULL/*'||V_VAKEY||'*/,decode(cust_num.e1_cust_cat_4,0,null,cust_num.e1_cust_cat_4),p.PAYMENT_COMMENTS,
                        p.investment_details ,tbl.tax_consid_desc, SALES_ORG1.P4,DIST_CHAN1.P3,DIV1.P2'
                   ;
           RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,V_SQL_STR1||v_sql_str2);
         dynamic_ddl(V_SQL_STR1||v_sql_str2);

--         check_and_drop('RR_EXP_OI_PROMOS_T');
--
--         DYNAMIC_DDL('create table rr_exp_oi_promos_t as select distinct t.promotion_id from rr_Exp_oi_t t,t_exp_oi i
--         where t.oi_seq_id = i.contract_comp_line_id and i.process_flag = 0');

         UPDATE PROMOTION SET RR_EXP_OI_FLAG = 4 WHERE PROMOTION_ID IN (SELECT DISTINCT T.PROMOTION_ID FROM RR_EXP_OI_T T,T_EXP_OI I
         where t.oi_seq_id = i.contract_comp_line_id and i.process_flag = 0);

         COMMIT;

         -- update promotion set
        --  update contract_line set contract_exp_Stat_id = 1 where contract_exp_Stat_id = 0 and contract_line_id in (select CONTRACT_COMP_LINE_ID from T_EXP_OI where process_flag = 0);
    COMMIT;
       end loop;
    end loop;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;


    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

  PROCEDURE prc_export_accrual_overspend(p_start_date  DATE,
                                          p_end_date    DATE,
                                          p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_ACCRUAL_OVERSPEND
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);
   fore_column      VARCHAR2(100);
   v_min            DATE;
   v_max            DATE;
   max_sales_date   DATE;
   v_weeks          NUMBER := 6;

   v_eng_profile    number := 1;
   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_ACCRUAL_OVERSPEND';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    --- Select PE Forecast Column --
    SELECT get_fore_col (0, v_eng_profile) INTO fore_column FROM DUAL;
    ---

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;

    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');

    v_min := p_start_date - (v_weeks * 7);
    v_max := p_end_date + (v_weeks * 7);

    SELECT TO_DATE(get_max_date, 'mm-dd-yyyy hh24:mi:ss')
    INTO max_sales_date
    FROM DUAL;

   MERGE INTO accrual_data ad
   USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_cd_ad, 0), 2) - NVL(ad.rr_accrual_os_cd_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_os_cd_ad_gl, 0)
                END rr_accrual_os_cd_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_coop_ad, 0), 2) - NVL(ad.rr_accrual_os_coop_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_os_coop_ad_gl, 0)
                END rr_accrual_os_coop_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_hf_cd_ad, 0), 2) - NVL(ad.rr_accrual_os_hf_cd_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_os_hf_cd_ad_gl, 0)
                END rr_accrual_os_hf_cd_post_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_hf_coop_ad, 0), 2) - NVL(ad.rr_accrual_os_hf_coop_ad_gl, 0)
                ELSE 0 - NVL(ad.rr_accrual_os_hf_coop_ad_gl, 0)
                END rr_accrual_os_hf_coop_post_gl
         FROM accrual_data ad,
              promotion_stat ps,
              promotion_type pt,
              promotion p
         WHERE 1 = 1
         AND ad.accrual_type_id = pt.promotion_type_id
         AND ad.accrual_stat_id = ps.promotion_stat_id
         AND p.promotion_id (+) = ad.accrual_id
         AND p.scenario_id IN (22, 262, 162)
         AND ps.rr_export_exclude = 0
         --AND pt.rr_export_accrual = 1
         --AND ps.rr_export_accrual = 1
         AND ad.sales_date BETWEEN p_start_date AND p_end_date) ad1
   ON(ad.accrual_id = ad1.accrual_id
   AND ad.item_id = ad1.item_id
   AND ad.location_id = ad1.location_id
   AND ad.sales_date = ad1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET ad.rr_accrual_os_cd_post_gl = ad1.rr_accrual_os_cd_post_gl, ad.rr_accrual_os_coop_post_gl = ad1.rr_accrual_os_coop_post_gl,
   ad.rr_accrual_os_hf_cd_post_gl = ad1.rr_accrual_os_hf_cd_post_gl, ad.rr_accrual_os_hf_coop_post_gl = ad1.rr_accrual_os_hf_coop_post_gl;

    COMMIT;

    --- Export table code to be inserted here
   insert into T_EXP_GL(BATCH_ID, MATERIAL, CUSTOMER_NO, PROMOTION_ID,SALES_ORG,DIST_CHANNEL,DIVISION, SALES_DATE, CASE_DEAL_AMOUNT, COOP_AMOUNT,
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    SELECT v_batch_id, I.I_ATT_1 , S.E1_CUST_CAT_4,  P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,
    pd.sales_date, pd.rr_accrual_os_cd_post_gl, pd.rr_accrual_os_coop_post_gl,
     PT.PROMOTION_TYPE_DESC,
    cdt.deal_type_code, coopt.deal_type_code, v_batch_date, 'OVERSPENT',rec_cust.e1_cust_cat_4
    FROM T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    T_EP_P3 DC,
    t_ep_e1_cust_cat_4 rec_cust,
    promotion p,
    promotion_stat ps, --INC0017321
    accrual_data pd,
    promotion_type pt,
    MDP_MATRIX MDP,
    activity ac,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
    AND ps.promotion_stat_id = p.promotion_stat_id --INC0017321
    AND ps.rr_export_exclude = 0 --INC0017321
    and P.SCENARIO_ID in (22, 262, 162)
     and p.activity_id = ac.activity_id
    and P.RECIPIENT = REC_CUSt.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND p.promotion_type_id = pt.promotion_type_id
   AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
  --  AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
    AND pd.sales_date BETWEEN p_start_date AND p_end_date
    AND ABS(NVL(pd.rr_accrual_os_cd_post_gl, 0) ) +
        ABS(NVL(pd.rr_accrual_os_coop_post_gl, 0) )  <> 0;

   MERGE INTO accrual_data ad
   USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_cd_ad, 0), 2)
                ELSE 0
                END rr_accrual_os_cd_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_coop_ad, 0), 2)
                ELSE 0
                END rr_accrual_os_coop_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_hf_cd_ad, 0), 2)
                ELSE 0
                END rr_accrual_os_hf_cd_ad_gl,
                CASE WHEN NVL(p.approval, 0) = 1 AND pt.rr_export_accrual = 1 AND ps.rr_export_accrual = 1
                THEN ROUND(NVL(ad.rr_accrual_os_hf_coop_ad, 0), 2)
                ELSE 0
                END rr_accrual_os_hf_coop_ad_gl
         FROM accrual_data ad,
              promotion_stat ps,
              promotion_type pt,
              promotion p
         WHERE 1 = 1
         AND ad.accrual_type_id = pt.promotion_type_id
         AND ad.accrual_stat_id = ps.promotion_stat_id
         AND ps.rr_export_exclude = 0
         AND p.promotion_id (+) = ad.accrual_id
         AND p.scenario_id IN (22, 262, 162)
         --AND pt.rr_export_accrual = 1
         --AND ps.rr_export_accrual = 1
         AND ad.sales_date BETWEEN p_start_date AND p_end_date) ad1
   ON(ad.accrual_id = ad1.accrual_id
   AND ad.item_id = ad1.item_id
   AND ad.location_id = ad1.location_id
   AND ad.sales_date = ad1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET ad.rr_accrual_os_cd_ad_gl = ad1.rr_accrual_os_cd_ad_gl, ad.rr_accrual_os_coop_ad_gl = ad1.rr_accrual_os_coop_ad_gl,
   ad.rr_accrual_os_hf_cd_ad_gl = ad1.rr_accrual_os_hf_cd_ad_gl, ad.rr_accrual_os_hf_coop_ad_gl = ad1.rr_accrual_os_hf_coop_ad_gl;


    INSERT INTO t_exp_gl_master
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER
                 FROM t_exp_gl WHERE batch_id = v_batch_id);

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

  PROCEDURE prc_export_accrual_close(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_ACCRUAL_CLOSE
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);

   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_ACCRUAL_CLOSE';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;


    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');

    check_and_drop('rr_promos_exp_close_t');

    sql_str := 'CREATE TABLE rr_promos_exp_close_t AS
               SELECT /*+ FULL(p) PARALLEL(p, 24) */ p.promotion_id
               FROM promotion p,
               promotion_stat ps,
               promotion_type pt,
               promotion_dates pdt
               WHERE 1 = 1
               AND pt.promotion_type_id = p.promotion_type_id
               AND ps.promotion_stat_id = p.promotion_stat_id
               AND p.promotion_id = pdt.promotion_id
               AND pt.rr_accrual_close = 1
               AND ps.rr_accrual_close = 1
               AND p.rr_export_close = 0
               AND p.scenario_id IN (22, 262, 162)
               AND pdt.from_date <= TO_DATE(''' || TO_CHAR(p_end_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
               AND pdt.until_date >= TO_DATE(''' || TO_CHAR(p_start_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')';

    dynamic_ddl (sql_str);
    COMMIT;

   sql_str := 'MERGE INTO accrual_data ad
   USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                ROUND(NVL(ad.rr_accrual_close_cd_ad, 0), 2)  rr_accrual_close_cd_post_gl,
                ROUND(NVL(ad.rr_accrual_close_coop_ad, 0), 2)  rr_accrual_close_coop_post_gl,
                ROUND(NVL(ad.rr_accrual_close_hf_cd_ad, 0), 2)  rr_accrual_close_hf_cd_post_gl,
                ROUND(NVL(ad.rr_accrual_close_hf_coop_ad, 0), 2)  rr_accrual_close_hf_cp_post_gl
         FROM accrual_data ad,
              rr_promos_exp_close_t rpt
         WHERE 1 = 1
         AND ad.accrual_id = rpt.promotion_id) ad1
   ON(ad.accrual_id = ad1.accrual_id
   AND ad.item_id = ad1.item_id
   AND ad.location_id = ad1.location_id
   AND ad.sales_date = ad1.sales_date)
   WHEN MATCHED THEN
   UPDATE SET ad.rr_accrual_close_cd_post_gl = ad1.rr_accrual_close_cd_post_gl,
   ad.rr_accrual_close_coop_post_gl = ad1.rr_accrual_close_coop_post_gl,
   ad.rr_accrual_close_hf_cd_post_gl = ad1.rr_accrual_close_hf_cd_post_gl,
   ad.rr_accrual_close_hf_cp_post_gl = ad1.rr_accrual_close_hf_cp_post_gl';

   dynamic_ddl (sql_str);
    --- Export table code to be inserted here

    EXECUTE IMMEDIATE 'INSERT INTO t_exp_gl(batch_id, material, customer_no, promotion_id,sales_org,dist_channel,division, sales_date, case_deal_amount, coop_amount,
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    SELECT ' || v_batch_id || ',  I.I_ATT_1 , S.E1_CUST_CAT_4, P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,
    pd.sales_date, pd.rr_accrual_close_cd_post_gl, pd.rr_accrual_close_coop_post_gl,
     pt.promotion_type_desc,
    cdt.deal_type_code, coopt.deal_type_code, TO_DATE(''' || TO_CHAR(v_batch_date, 'DD/MM/YYYY HH24:MI:SS') || ''', ''DD/MM/YYYY HH24:MI:SS'') , ''CLOSE'',rec_cust.e1_cust_cat_4
    FROM T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    t_ep_p3 dc,
    t_ep_e1_cust_cat_4 rec_cust,
    promotion p,
    accrual_data pd,
    promotion_type pt,
    mdp_matrix mdp,
    activity ac,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt ,
    rr_promos_exp_close_t rpt
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
    AND rpt.promotion_id = p.promotion_id
     and p.activity_id = ac.activity_id
    and p.recipient = rec_cust.t_ep_e1_cust_cat_4_ep_id
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND p.scenario_id IN (22, 262, 162)
    AND p.promotion_type_id = pt.promotion_type_id
    AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
  --  AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
    AND ABS(NVL(pd.rr_accrual_close_cd_post_gl, 0) ) +
        ABS(NVL(pd.rr_accrual_close_coop_post_gl, 0) ) <> 0';

   sql_str := 'MERGE INTO accrual_data ad
    USING(SELECT ad.accrual_id, ad.item_id, ad.location_id, ad.sales_date,
                ROUND(NVL(ad.rr_accrual_close_cd_ad, 0), 2) rr_accrual_close_cd_ad_gl,
                ROUND(NVL(ad.rr_accrual_close_coop_ad, 0), 2) rr_accrual_close_coop_ad_gl,
                ROUND(NVL(ad.rr_accrual_close_hf_cd_ad, 0), 2) rr_accrual_close_hf_cd_ad_gl,
                ROUND(NVL(ad.rr_accrual_close_hf_coop_ad, 0), 2) rr_accrual_close_hf_coop_ad_gl
         FROM accrual_data ad,
              rr_promos_exp_close_t rpt
         WHERE 1 = 1
         AND ad.accrual_id = rpt.promotion_id) ad1
    ON(ad.accrual_id = ad1.accrual_id
    AND ad.item_id = ad1.item_id
    AND ad.location_id = ad1.location_id
    AND ad.sales_date = ad1.sales_date)
    WHEN MATCHED THEN
    UPDATE SET ad.rr_accrual_close_cd_ad_gl = ad1.rr_accrual_close_cd_ad_gl,
    ad.rr_accrual_close_coop_ad_gl = ad1.rr_accrual_close_coop_ad_gl,
    ad.rr_accrual_close_hf_cd_ad_gl = ad1.rr_accrual_close_hf_cd_ad_gl,
    ad.rr_accrual_close_hf_coop_ad_gl = ad1.rr_accrual_close_hf_coop_ad_gl,
    ad.accrual_close_export_date = SYSDATE,
    ad.last_update_date = SYSDATE';

    dynamic_ddl (sql_str);
    COMMIT;

    sql_str := 'MERGE INTO promotion p
    USING(SELECT promotion_id
          FROM rr_promos_exp_close_t) p1
    ON(p.promotion_id = p1.promotion_id)
    WHEN MATCHED THEN
    UPDATE SET rr_export_close = 1,
    last_update_date = SYSDATE';

    dynamic_ddl (sql_str);

   insert into T_EXP_GL_MASTER
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER
                 FROM t_exp_gl WHERE batch_id = v_batch_id);


   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

    PROCEDURE prc_export_accrual_cancel(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_extra_where VARCHAR2 DEFAULT NULL)
  IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_ACCRUAL_CANCEL
   PURPOSE:    EXport latest accrual

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        31/10/2012  Bhaskar Rampalli // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100) ;
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   sql_str          VARCHAR2 (20000);

   v_batch_date     DATE := case when next_day(trunc(sysdate,'dd'),'MONDAY')-7 = trunc(sysdate,'dd') then sysdate else sysdate -1 end;

   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;

   ---
   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_ACCRUAL_CANCEL';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;


    INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_gl', 0, 'ES');


    EXECUTE IMMEDIATE ' INSERT INTO t_exp_gl(batch_id, material, customer_no, promotion_id,sales_org,dist_channel,division, sales_date, case_deal_amount, coop_amount,
                  ACTIVITY_TYPE, CD_DEAL_TYPE, COOP_DEAL_TYPE, GL_POST_DATE, TRANSACTION_TYPE,RECEPIENT_CUSTOMER)
    SELECT ' || v_batch_id || ',  I.I_ATT_1 , S.E1_CUST_CAT_4, P.PROMOTION_CODE,
    SORG.P4,DC.P3,DIV.P2,
    pd.sales_date, pd.rr_accrual_cd_post_gl, pd.rr_accrual_coop_post_gl, pt.promotion_type_desc,
    cdt.deal_type_code, coopt.deal_type_code, TO_DATE(''' || TO_CHAR(v_batch_date, 'DD/MM/YYYY HH24:MI:SS') || ''', ''DD/MM/YYYY HH24:MI:SS'') , ''CANCEL'',rec_cust.e1_cust_cat_4
    FROM T_EP_I_ATT_1 I,
    T_EP_E1_CUST_CAT_4 S,
    T_EP_P4 SORG,
    T_EP_P2 DIV,
    t_ep_p3 dc,
    t_ep_e1_cust_cat_4 rec_cust,
    promotion p,
    accrual_data pd,
    promotion_type pt,
    mdp_matrix mdp,
    activity ac,
    TBL_DEAL_TYPE CDT,
 --      TBL_OI_DEAL_TYPE OIT,
    tbl_deal_type coopt
    WHERE pd.item_id = mdp.item_id
    AND pd.location_id = mdp.location_id
    AND pd.accrual_id = p.promotion_id
     and p.activity_id = ac.activity_id
    and p.recipient = rec_cust.t_ep_e1_cust_cat_4_ep_id
    and MDP.T_EP_I_ATT_1_EP_ID = I.T_EP_I_ATT_1_EP_ID
    and MDP.T_EP_E1_CUST_CAT_4_EP_ID = S.T_EP_E1_CUST_CAT_4_EP_ID
    and MDP.T_EP_P3_EP_ID = DC.T_EP_P3_EP_ID
    and MDP.T_EP_P4_EP_ID = SORG.T_EP_P4_EP_ID
    AND MDP.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID
    AND p.scenario_id IN (22, 262, 162)
    AND p.promotion_type_id = pt.promotion_type_id
    AND p.rr_export_cancel = 0 and p.promotion_stat_id = 9
   AND NVL(ac.cd_deal_type, 0) = cdt.deal_type_id
    and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
    AND ABS(NVL(pd.rr_accrual_cd_post_gl, 0) ) +
        ABS(NVL(pd.rr_accrual_coop_post_gl, 0) ) <> 0
  --  AND NVL(p.oi_deal_type, 0) = oit.oi_deal_type_id
    ';

   --COMMIT;

    sql_str := 'MERGE INTO promotion p
    USING(SELECT promotion_id
          FROM promotion where rr_Export_cancel = 0 and promotion_stat_id = 9) p1
    ON(p.promotion_id = p1.promotion_id)
    WHEN MATCHED THEN
    UPDATE SET rr_export_cancel = 1,
    last_update_date = SYSDATE';

    dynamic_ddl (sql_str);

   insert into T_EXP_GL_MASTER
                (SELECT v_batch_id, MATERIAL,CUSTOMER_NO,PROMOTION_ID,SALES_DATE,CASE_DEAL_AMOUNT,COOP_AMOUNT,ACTIVITY_TYPE,CD_DEAL_TYPE,COOP_DEAL_TYPE,GL_POST_DATE,
TRANSACTION_TYPE,LOAD_DATE,SALES_ORG,DIVISION,DIST_CHANNEL,RECEPIENT_CUSTOMER
                 FROM t_exp_gl WHERE batch_id = v_batch_id);


   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM t_exp_gl
     WHERE batch_id = v_batch_id;

   UPDATE ExportSummaryMaster SET status = 'EC' ,RecordsProcessed = v_no_records_exp WHERE batchheaderid = v_batch_id;

    COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
    END;

    PROCEDURE prc_export_claims
    IS
/******************************************************************************
   NAME:       PRC_EXPORT_CLAIMS
   PURPOSE:    Exports Claim $ on a daily basis

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        06/21/2008  Richard Burton/Jigsaw Services Pty
   1.1        02/03/2009  Richard Burton/ Jigsaw Services Pty - Included Ttl Claim $
   1.2        26/10/2009 RB - Addition to export to t_exp_claims_gl  as well
   1.3        30/10/2009 RB -logging
   1.4        28/01/2011  Luke Pocock/Red Rcok Consulting - gl_exp_claim_detail flag condition added
   1.5        21/11/2011  Luke Pocock/Red Rock Consulting - Change Group By to ensure AP/AR is not aggregated.
   1.6        20/02/2012  Luke Pocock/Red Rock Consulting - Fix to handle a Null GST.

******************************************************************************/
   maxdate       DATE;
   mindate       DATE;
   v_prog_name   VARCHAR2 (100) := 'PRC_EXPORT_CLAIMS';
   v_status      VARCHAR2 (100);
   v_load_date   DATE;
   v_batch_id         NUMBER;
   v_no_records_exp   NUMBER;
   v_day          VARCHAR2(10);

   v_proc_log_id  NUMBER;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_CLAIMS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

   SELECT DECODE(pval, 1, 'SUNDAY', 2, 'MONDAY', 3, 'TUESDAY', 4, 'WEDNESDAY', 5, 'THRUSDAY', 6, 'FRIDAY', 7, 'SATURDAY')
   INTO v_day
   FROM sys_params
   WHERE pname = 'FIRSTDAYINWEEK';

   v_load_date := SYSDATE-1;
   maxdate := NEXT_DAY (TRUNC (SYSDATE, 'DD'), v_day);
   mindate := ADD_MONTHS (maxdate, -12);

    ----- Generate Batch Id
   SELECT exportsummarymasterid.NEXTVAL
     INTO v_batch_id
     FROM DUAL;

   ----- Insert into ExportSummaryMaster
   INSERT INTO ExportSummaryMaster VALUES (v_batch_id, SYSDATE, 't_exp_claims_ap_ar', 0, 'ES');
   COMMIT;

-- Export to T_EXP_CLAIMS for Claims which have not been exported yet--
--   INSERT INTO t_exp_claims_ap_ar
--      (SELECT v_batch_id, s.settlement_desc, v.customer_code, v.currency, s.promotion_id,
--              s.start_date, s.end_date, cdt.cd_deal_type_code, coopt.coop_deal_type_code,
--              SUM (claim_case_deal_d) claim_cd,
--              SUM (s.claim_case_deal_hf_d) claim_hf_cd,
--              SUM (s.co_op_$) claim_coop,
--              SUM (s.claim_coop_hf) claim_hf_coop,
--              SUM (s.gst),
--              SUM (NVL(s.claim_case_deal_d, 0) + NVL(s.co_op_$, 0) + NVL(s.claim_case_deal_hf_d, 0)
--                            + NVL(s.claim_coop_hf, 0)) claim_amount,
--              SYSDATE,
--              MAX (glc.gl_code_code) ar_ap_flag,
--              MAX(s.inv_number) invoice_no,
--              MAX(s.inv_date) invoice_date
--              FROM settlement s,
--              t_src_vendor v,
--              gl_code glc,
--              tbl_cd_deal_type cdt,
--              tbl_coop_deal_type coopt
--              WHERE s.gl_post_close IS NULL
--                   AND s.gl_exp_claim <> 1
--                   AND s.bill_to = v.vendor_id
--                   AND s.gl_code_id = glc.gl_code_id
--                   AND glc.gl_code_code = v.type
--                   AND NVL(s.deal_type_cd_claim, 0) = cdt.cd_deal_type_id
--                   AND NVL(s.deal_type_coop_claim, 0) = coopt.coop_deal_type_id
--                   AND s.gl_exp_claim_detail = 1     -- LP 28/01/11  -- Export only if details have exported
--                   AND s.promotion_id > 0
--                   AND s.settlement_status_id IN (4, 7)
--              GROUP BY s.settlement_desc, v.customer_code, v.currency, s.promotion_id,
--                       s.start_date, s.end_date, cdt.cd_deal_type_desc, coopt.coop_deal_type_desc);

--   COMMIT;                                                  ---renable here...

insert into T_EXP_GL_CLAIMS(CLAIM_NO,CUSTOMER_CLAIM_NO,CLAIM_ID,ACCOUNT_TYPE,PAY_TO_CUSTOMER,
SALES_ORG,DIST_CHANNEL,DIVISION,PROMOTION_ID,CLAIM_START_DATE,CLAIM_END_DATE,CASE_DEAL_AMOUNT_TF,
COOP_AMOUNT_TF,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT,TTL_GST,TTL_AMOUNT,
LOAD_DATE,CLAIM_DATE,PAYMENT_TYPE)
      (select  S.SETTLEMENT_DESC,nvl(S.rr_remit_num,S.SETTLEMENT_DESC),S.SETTLEMENT_ID,'AR', V.E1_CUST_CAT_4, '3011','01','01',
       S.PROMOTION_ID,              S.START_DATE, S.END_DATE,
              SUM (NVL(S.CLAIM_CASE_DEAL_D,0)-(DECODE(NVL(S.CLAIM_CASE_DEAL_D,0),0,0, NVL(S.GST,0)*10))) CASE_DEAL_AMOUNT_TF,
              SUM (NVL(S.CO_OP_$,0)-decode(NVL(S.CO_OP_$,0),0,0,nvl(S.gst,0)*10)) COOP_AMOUNT_TF,
              CDT.DEAL_TYPE_CODE, COOPT.DEAL_TYPE_CODE,
              SUM (DECODE(NVL(S.CLAIM_CASE_DEAL_D,0),0,0, NVL(S.GST,0)*10)) CASE_DEAL_AMOUNT,
              SUM (decode(NVL(S.CO_OP_$,0),0,0,nvl(S.gst,0)*10)) COOP_AMOUNT,
              SUM (NVL(S.GST,0)) TTL_GST,
              SUM (NVL(s.claim_case_deal_d, 0) + NVL(s.co_op_$, 0) + nvl(s.gst,0) +  (DECODE(NVL(S.CLAIM_CASE_DEAL_D,0),0,0, NVL(S.GST,0)*10)) + (decode(NVL(S.CO_OP_$,0),0,0,nvl(S.gst,0)*10)) ) TTL_AMOUNT,
              v_load_date,
              max(S.INV_DATE) INVOICE_DATE,
              cl.payment_type_code
              from SETTLEMENT S,
              t_ep_e1_cust_cat_4 v,
              TBL_DEAL_TYPE cdt,
              TBL_DEAL_TYPE COOPT,
              rr_cl_payment_type_t cl, promotion p, activity ac
              WHERE s.gl_post_close IS NULL
                   and S.GL_EXP_CLAIM <> 1
                   and S.BILL_TO = V.T_EP_E1_CUST_CAT_4_EP_ID
--                   AND NVL(s.deal_type_cd_claim, 0) = cdt.deal_type_id
--                   and NVL(S.DEAL_TYPE_COOP_CLAIM, 0) = COOPT.DEAL_TYPE_ID
                   AND s.gl_exp_claim_detail = 1     -- LP 28/01/11  -- Export only if details have exported
                   and S.PROMOTION_ID > 0
                   and s.promotion_id = p.promotion_id
                   and P.ACTIVITY_ID = AC.ACTIVITY_ID
                   and NVL(AC.CD_DEAL_TYPE, 0) = CDT.DEAL_TYPE_ID
                  and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
                   and cl.payment_type_id = s.payment_type
                   --and S.PROMOTION_ID in (1010509,1010686,1010687,1010696,1010688)
                   --and s.settlement_desc like 'POC%'
                   and S.SETTLEMENT_STATUS_ID in (4, 7)
              GROUP BY s.settlement_desc,S.rr_remit_num,s.settlement_id, v.E1_CUST_CAT_4,  s.promotion_id,
                       S.START_DATE, S.END_DATE, CDT.DEAL_TYPE_CODE, COOPT.DEAL_TYPE_CODE,cl.payment_type_code);
COMMIT;

   MERGE INTO settlement s1
      USING (SELECT DISTINCT s.settlement_id
              FROM settlement s,
              t_ep_e1_cust_cat_4 v,
              TBL_DEAL_TYPE cdt,
              TBL_DEAL_TYPE coopt,
              promotion p, activity ac
              WHERE s.gl_post_close IS NULL
                   AND s.gl_exp_claim <> 1
                   AND s.bill_to = v.T_EP_E1_CUST_CAT_4_EP_ID
                   --AND glc.gl_code_code = v.type
                   and s.promotion_id = p.promotion_id
                   and P.ACTIVITY_ID = AC.ACTIVITY_ID
                   and NVL(AC.CD_DEAL_TYPE, 0) = CDT.DEAL_TYPE_ID
                   and NVL(ac.COOP_DEAL_TYPE, 0) = COOPT.DEAL_TYPE_ID
                   AND s.gl_exp_claim_detail = 1     -- LP 28/01/11  -- Export only if details have exported
                   AND s.promotion_id > 0
                   AND s.settlement_status_id IN (4, 7)
                         ) s2
      ON (s1.settlement_id = s2.settlement_id)
      WHEN MATCHED THEN
         UPDATE
            SET s1.gl_exp_claim = 1,
             claim_export_date = SYSDATE,
             export_claim_date = SYSDATE
         ;
   COMMIT;

   INSERT INTO T_EXP_GL_CLAIMS_master(CLAIM_NO,CUSTOMER_CLAIM_NO,CLAIM_ID,ACCOUNT_TYPE,PAY_TO_CUSTOMER,
SALES_ORG,DIST_CHANNEL,DIVISION,PROMOTION_ID,CLAIM_START_DATE,CLAIM_END_DATE,CASE_DEAL_AMOUNT_TF,
COOP_AMOUNT_TF,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT,TTL_GST,TTL_AMOUNT,
LOAD_DATE,CLAIM_DATE,PAYMENT_TYPE)
   (SELECT CLAIM_NO,CUSTOMER_CLAIM_NO,CLAIM_ID,ACCOUNT_TYPE,PAY_TO_CUSTOMER,
SALES_ORG,DIST_CHANNEL,DIVISION,PROMOTION_ID,CLAIM_START_DATE,CLAIM_END_DATE,CASE_DEAL_AMOUNT_TF,
COOP_AMOUNT_TF,CD_DEAL_TYPE,COOP_DEAL_TYPE,CASE_DEAL_AMOUNT,COOP_AMOUNT,TTL_GST,TTL_AMOUNT,
LOAD_DATE,CLAIM_DATE,PAYMENT_TYPE
     FROM T_EXP_GL_CLAIMS);

   COMMIT;

   SELECT COUNT (*)
     INTO v_no_records_exp
     FROM T_EXP_GL_CLAIMS;

  UPDATE ExportSummaryMaster
  SET status = 'EC'
      ,RecordsProcessed = v_no_records_exp
  WHERE batchheaderid = v_batch_id;

   COMMIT;


       v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_export_exfactory_fcst
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_EXFACTORY_FCST
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_start_date DATE;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_EXFACTORY_FCST';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT MIN(datet) INTO v_start_date
    FROM inputs WHERE period_445 = (SELECT period_445 FROM inputs
                                    WHERE datet = NEXT_DAY(TRUNC(SYSDATE,'DD'),'SUNDAY')-6);

    dynamic_ddl('TRUNCATE TABLE t_exp_fcst');

    INSERT /*+ APPEND NOLOGGING */ INTO t_exp_fcst
    SELECT /*+ FULL(sd) PARALLEL(sd, 32) */ itm.item,site.site,reg.l_att_10,company.e1_item_cat_6,sd.sales_date,
    sd.exfact_base*NVL(sd.rr_invest_or,NVL(sd.rr_invest_p,1)),
    sd.final_pos_fcst, sd.ff_3, sd.order_fill_rate, NVL(sd.ff_3, 0) + NVL(sd.order_fill_rate, 0)
    FROM sales_data sd,
         mdp_matrix m,
         t_ep_e1_item_cat_6 company,
         t_ep_item itm,
         t_ep_site site,
         t_ep_l_att_10 reg
    WHERE
         sd.item_id = m.item_id
     AND sd.location_id = m.location_id
     AND m.t_ep_e1_item_cat_6_ep_id = company.t_ep_e1_item_cat_6_ep_id
     AND m.t_ep_item_ep_id = itm.t_ep_item_ep_id
     AND m.t_ep_site_ep_id = site.t_ep_site_ep_id
     AND m.t_ep_l_att_10_ep_id = reg.t_ep_l_att_10_ep_id
     AND itm.item like '%_40'
     AND site.site like '%_40'
     AND ABS(NVL(sd.exfact_base * NVL(sd.rr_invest_or, NVL(sd.rr_invest_p,1)), 0)) +
         ABS(NVL(sd.final_pos_fcst, 0)) +
         ABS(NVL(sd.ff_3, 0)) +
         ABS(NVL(sd.order_fill_rate, 0)) <> 0
     AND sd.sales_date BETWEEN v_start_date AND v_start_date + (52*3*7) ;

     COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END prc_export_exfactory_fcst;

  PROCEDURE prc_export_fcst

/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_FCST
   PURPOSE:    Export Demantra Offset fcst FOR APO
   Date Created : 27/08/2015
*****************************************************************************************************************************/

    IS

   v_prog_name      VARCHAR2(100);
   v_status         varchar2(100);
   v_proc_log_id    NUMBER;
   v_start_date     DATE;
   v_package_name   VARCHAR2(100);
   v_fore_col       VARCHAR2(100);
   SQL_SCRIPT       varchar2(7000);
   v_weeks integer := 104;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_FCST';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



    select min(DATET) into V_START_DATE
    FROM inputs WHERE trunc(next_day(sysdate,'MONDAY'),'DD')-7 = datet;

    dynamic_ddl('TRUNCATE TABLE t_exp_fcst_daily');

    select get_fore_col(0, 1) into v_fore_col from dual;

    --Sales Fcst = Apo Baseline + Offset Incremental Forecast
    -- sum((nvl(sd.manual_stat,0)*(1.00+nvl(sd.manual_fact,0)) + nvl(sd.final_pos_fcst,0)))

    -- cmos 21-Feb-2017 (px insert)
    -- cmos 25-Feb-2017 (use px index instead of fts on sd)
    sql_script := 'INSERT /*+ enable_parallel_dml parallel(32) nologging */ INTO t_exp_fcst_daily
                (SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_OFFSET_BASE_FCST,
                DMTRA_INCR_FCST,SCAN_ACTUALS_OFFSET)
    SELECT  /*+ index(sd,sales_data_pk) parallel(32) */ next_day(sd.sales_Date,''MONDAY'')-7 sdate,substr(itm.item,1,18), cl3.ebs_customer,
    soff.l_att_6,
    null,
    --sum(sd.engine_base),
    sum(sd.final_pos_fcst),
    sum(sd.act_part_ord)
    FROM sales_data sd,
         mdp_matrix m,
         t_ep_ebs_customer cl3,
         t_ep_item itm,
         t_ep_l_att_6 soff
    WHERE 1 = 1
     AND sd.item_id = m.item_id
     AND sd.location_id = m.location_id
     AND m.t_ep_ebs_customer_ep_id = cl3.t_ep_ebs_customer_ep_id
     AND m.t_ep_item_ep_id = itm.t_ep_item_ep_id
     AND m.t_ep_l_att_6_ep_id = soff.t_ep_l_att_6_ep_id
     AND ABS(NVL(sd.final_pos_fcst, 0)) +
--         ABS(NVL(sd.engine_base, 0)) +
         ABS(NVL(sd.act_part_ord, 0))  <> 0
     AND sd.sales_date BETWEEN to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'') -8*7
     AND to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'') + ('||v_weeks||'*7)
--     and cl3.ebs_customer IN /*(''0900000043'') --*/ (SELECT CL3 FROM RR_APO_CL3_T)
--     AND substr(itm.item,1,18) IN /*(''000000000010003391-30110101'') --*/ (SELECT ITEM FROM RR_APO_items_t)
   --and cl3.ebs_customer IN (SELECT CL3_CODE FROM t_src_sapl_apo_fcst_tmpl)
  --AND substr(itm.item,1,18) IN  (SELECT SKU_CODE FROM t_src_sapl_apo_fcst_tmpl)
     --and soff.l_att_6 in (''AF02'',''AF08'')
     group by next_day(sd.sales_Date,''MONDAY'')-7 ,substr(itm.item,1,18), cl3.ebs_customer,
    soff.l_att_6 ';

     dynamic_ddl ( sql_script );

     COMMIT;

    sql_script := ' MERGE INTO  T_EXP_FCST_DAILY T1
     using (select t.sku_code,t.cl3_code,t.sales_office,next_day(i.datet,''MONDAY'')-7 sdate
     from t_exp_Fcst_daily t,inputs i
     WHERE I.DATET  BETWEEN TO_DATE('''||to_char(v_start_date,'DD/MM/YYYY') ||''',''DD/MM/YYYY'') -8*7
     AND to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'') + ('||v_weeks||'*7)
     group by t.sku_code,t.cl3_code,t.sales_office,next_day(i.datet,''MONDAY'')-7)t2
     on(t1.sku_code = t2.sku_code
     and t1.cl3_code = t2.cl3_code
     and t1.sales_office = t2.sales_office
     and t1.sdate = t2.sdate)
     when matched then update set t1.process_flag = 0
     when not matched then insert(sdate,sku_code,cl3_code,sales_office,dmtra_offset_base_fcst,dmtra_incr_fcst,scan_actuals_offset,process_flag)
     values(t2.sdate,t2.sku_code,t2.cl3_code,t2.sales_office,null,null,null,0)';

--     DYNAMIC_DDL ( SQL_SCRIPT );
     COMMIT;
       DYNAMIC_DDL('insert into t_exp_fcst_daily_archive(SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_OFFSET_BASE_FCST,
                          DMTRA_INCR_FCST,SALES_FCST,SCAN_ACTUALS_OFFSET,PROCESS_FLAG,LOAD_DATE)
       select SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_OFFSET_BASE_FCST,
       DMTRA_INCR_FCST,SALES_FCST,SCAN_ACTUALS_OFFSET,PROCESS_FLAG,SYSDATE
       from t_exp_fcst_daily');

       COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;

  END prc_export_fcst;

  PROCEDURE prc_export_fcst_daily

/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_FCST
   PURPOSE:    Export Demantra Offset fcst FOR APO
   Date Created : 27/08/2015
*****************************************************************************************************************************/

    IS

   v_prog_name      VARCHAR2(100);
   v_status         varchar2(100);
   v_proc_log_id    NUMBER;
   v_start_date     DATE;
   v_package_name   VARCHAR2(100);
   v_fore_col       VARCHAR2(100);
   SQL_SCRIPT       VARCHAR2(7000);
   v_weeks integer := 104;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_FCST';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);



    select min(DATET) into V_START_DATE
    FROM inputs WHERE trunc(next_day(sysdate,'MONDAY'),'DD')-7 = datet;

    dynamic_ddl('TRUNCATE TABLE t_exp_fcst_daily');

    select get_fore_col(0, 1) into v_fore_col from dual;

    --Sales Fcst = Apo Baseline + Offset Incremental Forecast
    -- sum((nvl(sd.manual_stat,0)*(1.00+nvl(sd.manual_fact,0)) + nvl(sd.final_pos_fcst,0)))

    sql_script := 'INSERT /*+ APPEND NOLOGGING */ INTO t_exp_fcst_daily
                (SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_INCR_FCST)
    SELECT  /*+ FULL(sd) PARALLEL(sd, 32) */ next_day(sd.sales_Date,''MONDAY'')-7 sdate,substr(itm.item,1,18), cl3.ebs_customer,
    soff.l_att_6,
    sum(sd.final_pos_fcst)
    FROM sales_data sd,
         mdp_matrix m,
         t_ep_ebs_customer cl3,
         t_ep_item itm,
         t_ep_l_att_6 soff
    WHERE 1 = 1
     AND sd.item_id = m.item_id
     AND sd.location_id = m.location_id
     AND m.t_ep_ebs_customer_ep_id = cl3.t_ep_ebs_customer_ep_id
     AND m.t_ep_item_ep_id = itm.t_ep_item_ep_id
     AND m.t_ep_l_att_6_ep_id = soff.t_ep_l_att_6_ep_id
     AND ABS(NVL(sd.final_pos_fcst, 0))   <> 0
     AND sd.sales_date BETWEEN to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'')
     AND to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'') + ('||v_weeks||'*7)
--     and cl3.ebs_customer IN /*(''0900000043'') --*/ (SELECT CL3 FROM RR_APO_CL3_T)
--     AND substr(itm.item,1,18) IN /*(''000000000010003391-30110101'') --*/ (SELECT ITEM FROM RR_APO_items_t)
   --and cl3.ebs_customer IN (SELECT CL3_CODE FROM t_src_sapl_apo_fcst_tmpl)
  --AND substr(itm.item,1,18) IN  (SELECT SKU_CODE FROM t_src_sapl_apo_fcst_tmpl)
     --and soff.l_att_6 in (''AF02'',''AF08'')
     group by next_day(sd.sales_Date,''MONDAY'')-7 ,substr(itm.item,1,18), cl3.ebs_customer,
    soff.l_att_6 ';

     dynamic_ddl ( sql_script );

     COMMIT;

    sql_script := ' MERGE INTO  T_EXP_FCST_DAILY T1
     using (select t.sku_code,t.cl3_code,t.sales_office,next_day(i.datet,''MONDAY'')-7 sdate
     from t_exp_Fcst_daily t,inputs i
     WHERE I.DATET  BETWEEN TO_DATE('''||to_char(v_start_date,'DD/MM/YYYY') ||''',''DD/MM/YYYY'')
     AND to_date('''||to_char(v_start_date,'dd/mm/yyyy') ||''',''dd/mm/yyyy'') + ('||v_weeks||'*7)
     group by t.sku_code,t.cl3_code,t.sales_office,next_day(i.datet,''MONDAY'')-7)t2
     on(t1.sku_code = t2.sku_code
     and t1.cl3_code = t2.cl3_code
     and t1.sales_office = t2.sales_office
     and t1.sdate = t2.sdate)
     when matched then update set t1.process_flag = 0
     when not matched then insert(sdate,sku_code,cl3_code,sales_office,dmtra_incr_fcst,process_flag)
     values(t2.sdate,t2.sku_code,t2.cl3_code,t2.sales_office,null,0)';

--     DYNAMIC_DDL ( SQL_SCRIPT );
     COMMIT;
       DYNAMIC_DDL('insert into t_exp_fcst_daily_archive(SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_OFFSET_BASE_FCST,
                          DMTRA_INCR_FCST,SALES_FCST,SCAN_ACTUALS_OFFSET,PROCESS_FLAG,LOAD_DATE)
       select SDATE,SKU_CODE,CL3_CODE,SALES_OFFICE,DMTRA_OFFSET_BASE_FCST,
       DMTRA_INCR_FCST,SALES_FCST,SCAN_ACTUALS_OFFSET,PROCESS_FLAG,SYSDATE
       from t_exp_fcst_daily');

       COMMIT;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;

  END prc_export_fcst_daily;




  PROCEDURE prc_export_bi_profit_ka(p_start_offset  NUMBER,
                                    p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_BI_PROFIT_KA
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   v_fore_col       VARCHAR2(10);

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_BI_PROFIT_KA';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('t_exp_pl_ka_tmp');

    check_and_drop('t_exp_pl_ka');

    v_sql := 'CREATE TABLE t_exp_pl_ka_tmp AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              branch_data.sales_date AS sdate
              ,t_ep_item.item AS product_no
              ,t_ep_e1_item_cat_6.e1_item_cat_6 AS company
              ,t_ep_ebs_customer.ebs_customer AS key_account
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist128, 0))
                                      AS rr_list_sales_f
              ,sum(branch_data.cust_input3)
                                      AS rr_log_efficiencies_f
              ,sum(nvl(branch_data.cust_input3, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.ebspricelist129, 0)))
                                      AS rr_tot_efficiencies_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))))+ nvl(branch_data.mkt_input2, 0) + nvl(branch_data.mkt_input3, 0)+ (greatest(nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))* decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_price_adjust_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_market_price_adjusts_f
              ,sum(nvl((nvl(branch_data.ebspricelist125, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))), 0) + nvl(branch_data.tiv, 0) + nvl(branch_data.tiv_per_actual, 0))
                                      AS tt_base_non_per_spend_f
              ,sum(nvl((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.bdf_base_rate_corp, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.rev_diff, 0)), 0)+ nvl(branch_data.mkt_acc3, 0) + nvl(branch_data.si_acc1, 0) +  (greatest(nvl(branch_data.mkt_acc2, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0))  * nvl(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))), 0)))
                                      AS rr_ttl_base_bus_dev_spnd_f
              ,sum(nvl((((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.bdf_fixed_funds_corp, 0)) , 0)+ nvl(branch_data.ebspricelist106, 0)+ nvl(branch_data.ebspricelist107, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * greatest(nvl(branch_data.ebspricelist104, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0)))+ nvl(branch_data.cust_input1, 0) + nvl(branch_data.cust_input2, 0))
                                      AS rr_ttl_promo_trde_spnd_f
              ,0
                                      AS rr_total_trad_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist126, 0))
                                     AS rr_cogs_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,nvl(branch_data.unexfact_or,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1)))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) )
                                      AS rr_pl_fcst_vol
              ,sum(decode(nvl(branch_data.wif_scenario_flag,0), 0,((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))) *(greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))+ nvl(branch_data.mkt_acc3, 0) + nvl(branch_data.si_acc1, 0) + nvl(branch_data.ebspricelist106, 0) + nvl(branch_data.ebspricelist107, 0) + nvl(branch_data.mkt_input2, 0) + nvl(branch_data.mkt_input3, 0) + nvl(branch_data.tiv, 0) + nvl(branch_data.tiv_per_actual, 0)+ nvl(branch_data.cust_input2, 0) + nvl(branch_data.cust_input1, 0),((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.beg_inv,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.acc_sales,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.actuals_incr,0))))) *(greatest(nvl(branch_data.calc_leads, 0), nvl(branch_data.actuals_ttl, 0), nvl(branch_data.actuals_ttl_dol, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.qualified_leads, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.actual_con_fcst, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.act_sup, 0))))+ nvl(branch_data.act_part_ord, 0) + nvl(branch_data.avail_stock, 0) + nvl(branch_data.forecast_sum, 0) + nvl(branch_data.firm_leads, 0) + nvl(branch_data.advert, 0) + nvl(branch_data.aop, 0) + nvl(branch_data.dol_deduct_onine, 0) + nvl(branch_data.dol_deduct_thirty, 0)+ nvl(branch_data.backlog, 0) + nvl(branch_data.a_vol_fcst, 0)))
                                      AS ttl_var_promo_trde_spend_f
              FROM sales_data branch_data
              ,mdp_matrix
              ,t_ep_item
              ,t_ep_site
              ,t_ep_e1_item_cat_6
              ,t_ep_ebs_customer
              ,t_ep_l_att_2
              WHERE branch_data.item_id = mdp_matrix.item_id
              AND branch_data.location_id = mdp_matrix.location_id
              AND mdp_matrix.t_ep_item_ep_id = t_ep_item.t_ep_item_ep_id
              AND mdp_matrix.t_ep_site_ep_id = t_ep_site.t_ep_site_ep_id
              AND mdp_matrix.t_ep_ebs_customer_ep_id = t_ep_ebs_customer.t_ep_ebs_customer_ep_id
              AND mdp_matrix.t_ep_e1_item_cat_6_ep_id = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
              AND mdp_matrix.t_ep_l_att_2_ep_id = t_ep_l_att_2.t_ep_l_att_2_ep_id
              AND CASE WHEN SUBSTR(t_ep_site.site, -3, 3) = ''_10'' THEN t_ep_l_att_2.flag_nz ELSE t_ep_l_att_2.flag_au END = 1
              AND branch_data.sales_date BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              GROUP BY
              branch_data.sales_date
              ,t_ep_item.item
              ,t_ep_e1_item_cat_6.e1_item_cat_6
              ,t_ep_ebs_customer.ebs_customer';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE t_exp_pl_ka AS
              SELECT sdate,
              product_no,
              company,
              key_account,
              rr_list_sales_f,
              rr_log_efficiencies_f,
              rr_tot_efficiencies_f,
              rr_price_adjust_f,
              rr_market_price_adjusts_f,
              tt_base_non_per_spend_f,
              rr_ttl_base_bus_dev_spnd_f,
              rr_ttl_promo_trde_spnd_f,
              NVL(tt_base_non_per_spend_f, 0) + NVL(rr_ttl_base_bus_dev_spnd_f, 0) + NVL(rr_ttl_promo_trde_spnd_f, 0) rr_total_trad_f,
              rr_cogs_f,
              rr_pl_fcst_vol
              FROM t_exp_pl_ka_tmp
              WHERE ABS(NVL(rr_list_sales_f, 0)) +
                    ABS(NVL(rr_log_efficiencies_f, 0)) +
                    ABS(NVL(rr_tot_efficiencies_f, 0)) +
                    ABS(NVL(rr_price_adjust_f, 0)) +
                    ABS(NVL(rr_market_price_adjusts_f, 0)) +
                    ABS(NVL(tt_base_non_per_spend_f, 0)) +
                    ABS(NVL(rr_ttl_base_bus_dev_spnd_f, 0)) +
                    ABS(NVL(rr_ttl_promo_trde_spnd_f, 0)) +
                    ABS(NVL(rr_total_trad_f, 0)) +
                    ABS(NVL(rr_cogs_f, 0)) +
                    ABS(NVL(rr_pl_fcst_vol, 0))  <> 0';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


 PROCEDURE prc_export_bi_profit_kad(p_start_offset  NUMBER,
                                     p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_BI_PROFIT_KA
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);
   v_sql1           VARCHAR2(32767);
   v_fore_col       VARCHAR2(10);

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_BI_PROFIT_KAD';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

 --   pkg_common_utils.prc_run_wf_schema('RR_BI_CUBE', 'Y','Y');

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('t_exp_pl_kad_tmp');

    check_and_drop('t_exp_pl_kad');

    v_sql := 'CREATE TABLE t_exp_pl_kad_tmp AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              branch_data.sales_date AS sdate
              ,t_ep_item.item AS product_no
              ,t_ep_e1_item_cat_6.e1_item_cat_6 AS company
              ,max(t_ep_site.site) AS prom_region
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist128, 0))
                                      AS rr_list_sales_f
              ,sum(branch_data.cust_input3)
                                      AS rr_log_efficiencies_f
              ,sum(nvl(branch_data.cust_input3, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.ebspricelist129, 0)))
                                      AS rr_tot_efficiencies_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))))+ nvl(branch_data.mkt_input2, 0) + nvl(branch_data.mkt_input3, 0)+ (greatest(nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))* decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_price_adjust_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_market_price_adjusts_f
              ,sum(nvl((nvl(branch_data.ebspricelist125, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))), 0) + nvl(branch_data.tiv, 0) + nvl(branch_data.tiv_per_actual, 0))
                                      AS tt_base_non_per_spend_f
              ,sum(nvl((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.bdf_base_rate_corp, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.rev_diff, 0)), 0)+ nvl(branch_data.mkt_acc3, 0) + nvl(branch_data.si_acc1, 0) +  (greatest(nvl(branch_data.mkt_acc2, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0))  * nvl(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))), 0)))
                                      AS rr_ttl_base_bus_dev_spnd_f
              ,sum(nvl((((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.bdf_fixed_funds_corp, 0)) , 0)+ nvl(branch_data.ebspricelist106, 0)+ nvl(branch_data.ebspricelist107, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * greatest(nvl(branch_data.ebspricelist104, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0)))+ nvl(branch_data.cust_input1, 0) + nvl(branch_data.cust_input2, 0))
                                      AS rr_ttl_promo_trde_spnd_f
              ,0
                                      AS rr_total_trad_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist126, 0))
                                     AS rr_cogs_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,nvl(branch_data.unexfact_or,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1)))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) )
                                      AS rr_pl_fcst_vol
              ,sum(((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.beg_inv,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.acc_sales,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.actuals_incr,0))))) *(greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.actuals_ttl, 0), nvl(branch_data.actuals_ttl_dol, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.actual_con_fcst, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.act_sup, 0))))+ nvl(branch_data.act_part_ord, 0) + nvl(branch_data.avail_stock, 0) + nvl(branch_data.ebspricelist106, 0) + nvl(branch_data.ebspricelist107, 0) + nvl(branch_data.advert, 0) + nvl(branch_data.aop, 0) + nvl(branch_data.dol_deduct_onine, 0) + nvl(branch_data.dol_deduct_thirty, 0)+ nvl(branch_data.backlog, 0) + nvl(branch_data.a_vol_fcst, 0))
                                     AS ttl_var_promo_trde_spend_f
              ,sum(branch_data.cust_input3) AS freight_deduction_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0)-nvl(branch_data.ebspricelist124, 0)))
                                      AS rr_list_sales_exp_mpa_f
              FROM sales_data branch_data
              ,mdp_matrix
              ,t_ep_item
              ,t_ep_site
              ,t_ep_e1_item_cat_6
              ,t_ep_ebs_account
              ,t_ep_l_att_2
              WHERE branch_data.item_id = mdp_matrix.item_id
              AND branch_data.location_id = mdp_matrix.location_id
              AND mdp_matrix.t_ep_item_ep_id = t_ep_item.t_ep_item_ep_id
              AND mdp_matrix.t_ep_site_ep_id = t_ep_site.t_ep_site_ep_id
              AND mdp_matrix.t_ep_ebs_account_ep_id = t_ep_ebs_account.t_ep_ebs_account_ep_id
              AND mdp_matrix.t_ep_e1_item_cat_6_ep_id = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
              AND mdp_matrix.t_ep_l_att_2_ep_id = t_ep_l_att_2.t_ep_l_att_2_ep_id
              AND CASE WHEN SUBSTR(t_ep_site.site, -3, 3) = ''_10'' THEN t_ep_l_att_2.flag_nz ELSE t_ep_l_att_2.flag_au END = 1
              AND branch_data.sales_date BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              GROUP BY
              branch_data.sales_date
              ,t_ep_item.item
              ,t_ep_e1_item_cat_6.e1_item_cat_6
              ,t_ep_l_att_2.l_att_2
              ,t_ep_ebs_account.ebs_account';

/*SR -- REQ0019365 -- Anthony request to include only the active customers in the export */

    dynamic_ddl(v_sql);

v_sql1 := 'CREATE TABLE t_exp_pl_kad_tmp AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              branch_data.sales_date AS sdate
              ,t_ep_item.item AS product_no
              ,t_ep_e1_item_cat_6.e1_item_cat_6 AS company
              ,nvl(max(rr_bi_cube_kad.promotion_region), max(t_ep_site.site)) AS prom_region
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist128, 0))
                                      AS rr_list_sales_f
              ,sum(branch_data.cust_input3)
                                      AS rr_log_efficiencies_f
              ,sum(nvl(branch_data.cust_input3, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.ebspricelist129, 0)))
                                      AS rr_tot_efficiencies_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))))+ nvl(branch_data.mkt_input2, 0) + nvl(branch_data.mkt_input3, 0)+ (greatest(nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))* decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_price_adjust_f
              ,sum((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_market_price_adjusts_f
              ,sum(nvl((nvl(branch_data.ebspricelist125, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))), 0) + nvl(branch_data.tiv, 0) + nvl(branch_data.tiv_per_actual, 0))
                                      AS tt_base_non_per_spend_f
              ,sum(nvl((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.bdf_base_rate_corp, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.rev_diff, 0)), 0)+ nvl(branch_data.mkt_acc3, 0) + nvl(branch_data.si_acc1, 0) +  (greatest(nvl(branch_data.mkt_acc2, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0))  * nvl(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))), 0)))
                                      AS rr_ttl_base_bus_dev_spnd_f
              ,sum(nvl((((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.bdf_fixed_funds_corp, 0)) , 0)+ nvl(branch_data.ebspricelist106, 0)+ nvl(branch_data.ebspricelist107, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * greatest(nvl(branch_data.ebspricelist104, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0)))+ nvl(branch_data.cust_input1, 0) + nvl(branch_data.cust_input2, 0))
                                      AS rr_ttl_promo_trde_spnd_f
              ,0
                                      AS rr_total_trad_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist126, 0))
                                     AS rr_cogs_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,nvl(branch_data.unexfact_or,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1)))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) )
                                      AS rr_pl_fcst_vol
              ,sum(((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.beg_inv,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.acc_sales,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.actuals_incr,0))))) *(greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.actuals_ttl, 0), nvl(branch_data.actuals_ttl_dol, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.actual_con_fcst, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.act_sup, 0))))+ nvl(branch_data.act_part_ord, 0) + nvl(branch_data.avail_stock, 0) + nvl(branch_data.ebspricelist106, 0) + nvl(branch_data.ebspricelist107, 0) + nvl(branch_data.advert, 0) + nvl(branch_data.aop, 0) + nvl(branch_data.dol_deduct_onine, 0) + nvl(branch_data.dol_deduct_thirty, 0)+ nvl(branch_data.backlog, 0) + nvl(branch_data.a_vol_fcst, 0))
                                     AS ttl_var_promo_trde_spend_f
              ,sum(branch_data.cust_input3) AS freight_deduction_f
              ,sum(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0)-nvl(branch_data.ebspricelist124, 0)))
                                      AS rr_list_sales_exp_mpa_f
              FROM sales_data branch_data
              ,mdp_matrix
              ,t_ep_item
              ,t_ep_site
              ,t_ep_e1_item_cat_6
              ,t_ep_ebs_account
              ,t_ep_l_att_2
              ,rr_bi_cube_kad
              WHERE branch_data.item_id = mdp_matrix.item_id
              AND branch_data.location_id = mdp_matrix.location_id
              AND mdp_matrix.t_ep_item_ep_id = t_ep_item.t_ep_item_ep_id
              AND mdp_matrix.t_ep_site_ep_id = t_ep_site.t_ep_site_ep_id
              AND mdp_matrix.t_ep_ebs_account_ep_id = t_ep_ebs_account.t_ep_ebs_account_ep_id
              AND mdp_matrix.t_ep_e1_item_cat_6_ep_id = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
              AND mdp_matrix.t_ep_l_att_2_ep_id = t_ep_l_att_2.t_ep_l_att_2_ep_id
              AND rr_bi_cube_kad.key_account_district = t_ep_ebs_account.ebs_Account
              AND CASE WHEN SUBSTR(t_ep_site.site, -3, 3) = ''_10'' THEN t_ep_l_att_2.flag_nz ELSE t_ep_l_att_2.flag_au END = 1
              AND branch_data.sales_date BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              GROUP BY
              branch_data.sales_date
              ,t_ep_item.item
              ,t_ep_e1_item_cat_6.e1_item_cat_6
              ,t_ep_l_att_2.l_att_2
              ,t_ep_ebs_account.ebs_account';

--                 dynamic_ddl(v_sql1);



    v_sql := 'CREATE TABLE t_exp_pl_kad AS
              SELECT sdate,
              product_no,
              company,
              prom_region,
              rr_list_sales_f,
              rr_log_efficiencies_f,
              rr_tot_efficiencies_f,
              rr_price_adjust_f,
              rr_market_price_adjusts_f,
              tt_base_non_per_spend_f,
              rr_ttl_base_bus_dev_spnd_f,
              rr_ttl_promo_trde_spnd_f,
              NVL(tt_base_non_per_spend_f, 0) + NVL(rr_ttl_base_bus_dev_spnd_f, 0) + NVL(rr_ttl_promo_trde_spnd_f, 0) rr_total_trad_f,
              rr_cogs_f,
              rr_pl_fcst_vol ,
              ttl_var_promo_trde_spend_f,
              freight_deduction_f,
              rr_list_sales_exp_mpa_f
              FROM t_exp_pl_kad_tmp
              WHERE ABS(NVL(rr_list_sales_f, 0)) +
                    ABS(NVL(rr_log_efficiencies_f, 0)) +
                    ABS(NVL(rr_tot_efficiencies_f, 0)) +
                    ABS(NVL(rr_price_adjust_f, 0)) +
                    ABS(NVL(rr_market_price_adjusts_f, 0)) +
                    ABS(NVL(tt_base_non_per_spend_f, 0)) +
                    ABS(NVL(rr_ttl_base_bus_dev_spnd_f, 0)) +
                    ABS(NVL(rr_ttl_promo_trde_spnd_f, 0)) +
                    ABS(NVL(rr_total_trad_f, 0)) +
                    ABS(NVL(rr_cogs_f, 0)) +
                    ABS(NVL(rr_pl_fcst_vol, 0))+
                    ABS(NVL(ttl_var_promo_trde_spend_f, 0))+
                    ABS(NVL(freight_deduction_f,0))+
                    ABS(NVL(rr_list_sales_exp_mpa_f,0))<> 0';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


 PROCEDURE prc_export_bi_profit_pr(p_start_offset  NUMBER,
                                    p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_BI_PROFIT_PR
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   v_fore_col       VARCHAR2(10);

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_BI_PROFIT_PR';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('t_exp_pl_pr_tmp');

    check_and_drop('t_exp_pl_pr');

    v_sql := 'CREATE TABLE t_exp_pl_pr_tmp AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              branch_data.sales_date AS sdate
              ,t_ep_item.item AS product_no
              ,t_ep_e1_item_cat_6.e1_item_cat_6 AS company
              ,t_ep_site.site AS prom_region
              ,(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist128, 0))
                                      AS rr_list_sales_f
              ,(branch_data.cust_input3)
                                      AS rr_log_efficiencies_f
              ,(nvl(branch_data.cust_input3, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.ebspricelist129, 0)))
                                      AS rr_tot_efficiencies_f
              ,((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))))+ nvl(branch_data.mkt_input2, 0) + nvl(branch_data.mkt_input3, 0)+ (greatest(nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))* decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_price_adjust_f
              ,((nvl(branch_data.ebspricelist124, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))))
                                      AS rr_market_price_adjusts_f
              ,(nvl((nvl(branch_data.ebspricelist125, 0) * decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0))))), 0) + nvl(branch_data.tiv, 0) + nvl(branch_data.tiv_per_actual, 0))
                                      AS tt_base_non_per_spend_f
              ,(nvl((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.bdf_base_rate_corp, 0))+ (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.rev_diff, 0)), 0)+ nvl(branch_data.mkt_acc3, 0) + nvl(branch_data.si_acc1, 0) +  (greatest(nvl(branch_data.mkt_acc2, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0))  * nvl(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))), 0)))
                                      AS rr_ttl_base_bus_dev_spnd_f
              ,(nvl((((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0), nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.bdf_fixed_funds_corp, 0)) , 0)+ nvl(branch_data.ebspricelist106, 0)+ nvl(branch_data.ebspricelist107, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * greatest(nvl(branch_data.ebspricelist104, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0)))+ nvl(branch_data.cust_input1, 0) + nvl(branch_data.cust_input2, 0))
                                      AS rr_ttl_promo_trde_spnd_f
              ,0
                                      AS rr_total_trad_f
              ,(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * nvl(branch_data.ebspricelist126, 0))
                                     AS rr_cogs_f
              ,(decode(nvl(branch_data.demand_type,0),1,nvl(branch_data.unexfact_or,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1)))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) )
                                      AS rr_pl_fcst_vol
             ,(((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.beg_inv,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.acc_sales,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.actuals_incr,0))))) *(greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.actuals_ttl, 0), nvl(branch_data.actuals_ttl_dol, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.actual_con_fcst, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.act_sup, 0))))+ nvl(branch_data.act_part_ord, 0) + nvl(branch_data.avail_stock, 0) + nvl(branch_data.ebspricelist106, 0) + nvl(branch_data.ebspricelist107, 0) + nvl(branch_data.advert, 0) + nvl(branch_data.aop, 0) + nvl(branch_data.dol_deduct_onine, 0) + nvl(branch_data.dol_deduct_thirty, 0)+ nvl(branch_data.backlog, 0) + nvl(branch_data.a_vol_fcst, 0))
                                     AS ttl_var_promo_trde_spend_f
             ,(branch_data.cust_input3) AS freight_deduction_f
              ,(decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col ||'*1, branch_data.npi_forecast)))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0)-nvl(branch_data.ebspricelist124, 0)))
                                     AS rr_list_sales_exp_mpa_f
              FROM sales_data branch_data
              ,mdp_matrix
              ,t_ep_item
              ,t_ep_site
              ,t_ep_e1_item_cat_6
              ,t_ep_ebs_customer
              ,t_ep_l_att_2
              WHERE branch_data.item_id = mdp_matrix.item_id
              AND branch_data.location_id = mdp_matrix.location_id
              AND mdp_matrix.t_ep_item_ep_id = t_ep_item.t_ep_item_ep_id
              AND mdp_matrix.t_ep_site_ep_id = t_ep_site.t_ep_site_ep_id
              AND mdp_matrix.t_ep_ebs_customer_ep_id = t_ep_ebs_customer.t_ep_ebs_customer_ep_id
              AND mdp_matrix.t_ep_e1_item_cat_6_ep_id = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
              AND mdp_matrix.t_ep_l_att_2_ep_id = t_ep_l_att_2.t_ep_l_att_2_ep_id
              AND CASE WHEN SUBSTR(t_ep_site.site, -3, 3) = ''_10'' THEN t_ep_l_att_2.flag_nz ELSE t_ep_l_att_2.flag_au END = 2
              AND branch_data.sales_date BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              ';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE t_exp_pl_pr AS
              SELECT sdate,
              product_no,
              company,
              prom_region,
              rr_list_sales_f,
              rr_log_efficiencies_f,
              rr_tot_efficiencies_f,
              rr_price_adjust_f,
              rr_market_price_adjusts_f,
              tt_base_non_per_spend_f,
              rr_ttl_base_bus_dev_spnd_f,
              rr_ttl_promo_trde_spnd_f,
              NVL(tt_base_non_per_spend_f, 0) + NVL(rr_ttl_base_bus_dev_spnd_f, 0) + NVL(rr_ttl_promo_trde_spnd_f, 0) rr_total_trad_f,
              rr_cogs_f,
              rr_pl_fcst_vol,
              ttl_var_promo_trde_spend_f,
              freight_deduction_f,
              rr_list_sales_exp_mpa_f
              FROM t_exp_pl_pr_tmp
              WHERE ABS(NVL(rr_list_sales_f, 0)) +
                    ABS(NVL(rr_log_efficiencies_f, 0)) +
                    ABS(NVL(rr_tot_efficiencies_f, 0)) +
                    ABS(NVL(rr_price_adjust_f, 0)) +
                    ABS(NVL(rr_market_price_adjusts_f, 0)) +
                    ABS(NVL(tt_base_non_per_spend_f, 0)) +
                    ABS(NVL(rr_ttl_base_bus_dev_spnd_f, 0)) +
                    ABS(NVL(rr_ttl_promo_trde_spnd_f, 0)) +
                    ABS(NVL(rr_total_trad_f, 0)) +
                    ABS(NVL(rr_cogs_f, 0)) +
                    ABS(NVL(rr_pl_fcst_vol, 0)) +
                    ABS(NVL(ttl_var_promo_trde_spend_f, 0))+
                    ABS(NVL(freight_deduction_f,0))+
                   ABS(NVL(rr_list_sales_exp_mpa_f,0))
                    <> 0';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_export_tradespend_nonpro(p_start_offset  NUMBER,
                                         p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_BI_TRADESPEND_NONPRO
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   v_fore_col       VARCHAR2(10);

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_BI_TS_NONPRO';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('bieo_tradespend_nonpro_tmp');

    check_and_drop('bieo_tradespend_nonpro');

    v_sql := 'CREATE TABLE bieo_tradespend_nonpro_tmp AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              branch_data.sales_date AS sdate
              ,t_ep_item.item product_no
              ,t_ep_e1_item_cat_6.e1_item_cat_6 company
              ,t_ep_ebs_customer.ebs_customer key_account
              ,SUM(NVL(  branch_data.volume_base_ttl , 0 )) AS volume_base_ttl_db
              ,SUM((NVL(branch_data.manual_stat, NVL(branch_data.sim_val_182,NVL(branch_data.' || v_fore_col || ' *1, branch_data.npi_forecast)))*(1.00+NVL(branch_data.manual_fact,0)) + NVL(branch_data.sd_uplift,0))) AS sales_plan
              ,SUM(branch_data.sd_uplift) AS sd_uplift
              ,SUM(NVL(branch_data.sim_val_182,branch_data.' || v_fore_col || ' *1)) AS pe_baseline
              ,MAX(NVL( branch_data.rr_rrp_oride,NVL(branch_data.shelf_price_sd,0))) AS rr_rrp_final
              ,MAX(branch_data.ebspricelist124) AS market_price
              ,MAX(branch_data.bdf_base_rate_corp) AS bbds
              ,MAX(branch_data.ebspricelist126) AS cogs
              ,MAX(branch_data.ebspricelist102) AS distribution
              ,MAX(branch_data.ebspricelist127) AS spoils
              ,MAX(branch_data.ebspricelist128) AS list_price
              ,MAX(branch_data.ebspricelist123) AS cust_list_price
              ,MAX(branch_data.ebspricelist100) AS tot_efficiencies
              ,MAX(branch_data.rev_diff) AS bbds_deferred
              ,MAX(NVL( t_ep_item.e1_prim_plan_factor ,0)) AS gst_rate
              ,MAX(branch_data.ff_4) AS rr_roi_rebate
              ,MAX(branch_data.ff_2) AS rr_roi_effciencies
              ,sum(case when get_max_date>=branch_data.sales_date then branch_data.ebs_sh_req_qty_rd else
              nvl(branch_data.cust_input3, 0)+ (decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)
              *(nvl(branch_data.rr_invest_or,nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),
              nvl(branch_Data.unexfact_or,(nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1,
              nvl(branch_data.npi_forecast, 0))))*(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) *
              (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) * nvl(branch_data.ebspricelist100, 0))
              + (((decode(nvl(branch_data.demand_type,0),1,(nvl(branch_data.exfact_base,0)*(nvl(branch_data.rr_invest_or,
              nvl(branch_data.rr_invest_p,1))) + nvl(branch_data.final_pos_fcst,0)),nvl(branch_Data.unexfact_or,
              (nvl(branch_data.manual_stat, nvl(branch_data.sim_val_182,nvl(branch_data.' || v_fore_col || ' *1, nvl(branch_data.npi_forecast, 0))))
              *(1.00+nvl(branch_data.manual_fact,0)) + nvl(branch_data.sd_uplift,0)))) * (nvl(branch_data.ebspricelist128, 0)
              - nvl(branch_data.ebspricelist124, 0) - ((nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0))
               * nvl(branch_data.ebspricelist100, 0)) - (greatest(nvl(branch_data.ebspricelist104, 0), nvl(branch_data.mkt_acc2, 0)
               , nvl(branch_data.mkt_input1, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) *
               nvl(branch_data.ebspricelist103, 0), (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0))
               * nvl(branch_data.mkt_acc1, 0),  (nvl(branch_data.ebspricelist128, 0) - nvl(branch_data.ebspricelist124, 0)) *
              nvl(branch_data.si_acc2, 0))))) - nvl(branch_data.mkt_acc3, 0)) * nvl(branch_data.ebspricelist129, 0))end) AS rr_tot_efficiencies_pj,
              MAX(branch_data.shelf_price_sd) shelf_price_sd
              FROM sales_data branch_data
              ,mdp_matrix
              ,t_ep_item
              ,t_ep_site
              ,t_ep_e1_item_cat_6
              ,t_ep_ebs_customer
              WHERE branch_data.item_id = mdp_matrix.item_id
              AND branch_data.location_id = mdp_matrix.location_id
              AND mdp_matrix.t_ep_item_ep_id = t_ep_item.t_ep_item_ep_id
              AND mdp_matrix.t_ep_site_ep_id = t_ep_site.t_ep_site_ep_id
              AND mdp_matrix.t_ep_e1_item_cat_6_ep_id = t_ep_e1_item_cat_6.t_ep_e1_item_cat_6_ep_id
              AND mdp_matrix.t_ep_ebs_customer_ep_id = t_ep_ebs_customer.t_ep_ebs_customer_ep_id
              AND branch_data.sales_date BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              GROUP BY
              branch_data.sales_date
              ,t_ep_item.item
              ,t_ep_e1_item_cat_6.e1_item_cat_6
              ,t_ep_ebs_customer.ebs_customer
              ';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE bieo_tradespend_nonpro AS
              SELECT  /*+ FULL(tmp_tab) PARALLEL(tmp_tab, 64) */ * FROM bieo_tradespend_nonpro_tmp tmp_tab
              WHERE ABS(NVL(volume_base_ttl_db, 0)) +
                    ABS(NVL(sales_plan, 0)) +
                    ABS(NVL(sd_uplift, 0)) +
                    ABS(NVL(rr_rrp_final, 0)) +
                    ABS(NVL(market_price, 0)) +
                    ABS(NVL(bbds, 0)) +
                    ABS(NVL(cogs, 0)) +
                    ABS(NVL(distribution, 0)) +
                    ABS(NVL(spoils, 0)) +
                    ABS(NVL(list_price, 0)) +
                    ABS(NVL(cust_list_price, 0)) +
                    ABS(NVL(tot_efficiencies, 0)) +
                    ABS(NVL(rr_roi_rebate, 0)) +
                    ABS(NVL(rr_roi_effciencies, 0)) +
                    ABS(NVL(bbds_deferred, 0)) +
                    ABS(NVL(rr_tot_efficiencies_pj, 0)) +
                    ABS(NVL(shelf_price_sd, 0)) <> 0';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;


  PROCEDURE prc_export_tradespend_pro(p_start_offset  NUMBER,
                                         p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_BI_TRADESPEND_NONPRO
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   v_fore_col       VARCHAR2(10);

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_BI_TS_PRO';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('bieo_tradespend_pro_tmp');

    check_and_drop('bieo_tradespend_pro');

    v_sql := 'CREATE TABLE BIEO_TRADESPEND_PRO_TMP AS
              SELECT /*+ FULL(BRANCH_DATA) PARALLEL(BRANCH_DATA, 64) */
              BRANCH_DATA.SALES_DATE AS SDATE
              ,PROMOTION.PROMOTION_CODE LEVEL1
              ,T_EP_ITEM.ITEM LEVEL2
              ,T_EP_E1_ITEM_CAT_6.E1_ITEM_CAT_6 LEVEL3
              ,T_EP_SITE.SITE LEVEL4
              ,SCENARIO.SCENARIO_CODE LEVEL5
              ,PROMOTION_STAT.PROMOTION_STAT_DESC AS PROMOTION_STATUS
              ,PROMOTION_TYPE.PROMOTION_TYPE_DESC AS ACTIVITY_TYPE_ID
              ,TBL_CD_DEAL_TYPE.CD_DEAL_TYPE_DESC AS CD_DEAL_TYPE
              ,TBL_COOP_DEAL_TYPE.COOP_DEAL_TYPE_DESC AS COOP_DEAL_TYPE
              ,TBL_OI_DEAL_TYPE.OI_DEAL_TYPE_DESC AS OI_DEAL_TYPE
              ,CEIL(((NEXT_DAY(PROMOTION_DATES.UNTIL_DATE,''MONDAY'')-NEXT_DAY(PROMOTION_DATES.FROM_DATE,''MONDAY''))/7 +1)) AS NUM_WEEKS
              ,BRANCH_DATA.PROMO_PRICE AS PROMO_PRICE
              ,NVL(BRANCH_DATA.EVT_VOL_ACT,BRANCH_DATA.VOLUME_BASE_TTL)  AS EVENT_BASE
              ,0 AS EVENT_LIFT_FACTOR
              ,NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, NVL(BRANCH_DATA.INCR_EVT_VOL_OR, NVL( FORE_5_UPLIFT,NVL( BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) AS INCR_EVENT_VOL
              ,0 AS OI_SPEND
              ,0 AS EVENT_CPIC
              ,PROMOTION_DATES.FROM_DATE START_PROMO
              ,NVL( T_EP_ITEM.E1_PRIM_PLAN_FACTOR ,0) GST_RATE
              ,0 AS TTL_EVT_TS_ROI
              ,(((NVL(BRANCH_DATA.LIST_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) - (NVL(BRANCH_DATA.MARKET_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)) * NVL(BRANCH_DATA.RR_ROI_EFFECIENCIES, 0)) RR_ROI_EFFECIENCIES_PD
              ,( CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END * (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0) - ((NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0)) * NVL(BRANCH_DATA.TOT_EFFICIENCIES_P, 0)) - (GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0) , (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))*NVL(BRANCH_DATA.RR_OI_P, 0)))) * NVL(BRANCH_DATA.RR_ROI_REBATE, 0) ) ROI_REBATE_PD
              ,BRANCH_DATA.RR_HANDLING_COOP AS RR_HANDLING_COOP
              ,BRANCH_DATA.RR_HANDLING_D AS RR_HANDLING_D
              ,BRANCH_DATA.CASE_BUYDOWN AS CASE_BUYDOWN
              ,0 AS PERC_TRADE_SPEND
              ,BRANCH_DATA.ED_PRICE AS ED_PRICE
              ,BRANCH_DATA.CASE_BUYDOWNOI AS CASE_BUYDOWNOI
              ,SAFE_DIVISION(BRANCH_DATA.CASE_BUYDOWN, BRANCH_DATA.UNITS, 0)  AS CASE_DEAL_PD
              ,PROMOTION.PROMO_NAME AS PROMO_NAME
              ,BRANCH_DATA.RR_ROI_REBATE RR_ROI_REBATE
              ,0 AS TTL_EVT_NSV
              ,BRANCH_DATA.RR_OI_P AS RR_OI_P
              ,BRANCH_DATA.RR_REDEMP_P AS RR_REDEMP_P
              ,(((NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0) - ((NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0)) * NVL(BRANCH_DATA.TOT_EFFICIENCIES_P, 0)) - (GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)))) *  NVL(BRANCH_DATA.REBATE_P, 0)  * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) +  NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,   NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) +  ((GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)) + NVL(BRANCH_DATA.CASE_BUYDOWN, 0)  + (NVL(BRANCH_DATA.RR_HANDLING_D, 0) * NVL(BRANCH_DATA.UNITS, 0))) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)  + (NVL(BRANCH_DATA.EVENT_COST, 0)) + (NVL(BRANCH_DATA.RR_HANDLING_COOP, 0))) AS TTL_EVT_SPEND_D_PD
              ,0 AS INCR_EVT_NSV_D_PD
              ,0 AS INCR_EVT_CBM_D_PD
              ,0 AS TTL_EVT_CBM_D_PD
              ,(CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) +  NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END * NVL(BRANCH_DATA.UNITS, 0) * ((NVL(BRANCH_DATA.PROMO_PRICE, 0) ) ) * (1/(1 + NVL(T_EP_ITEM.E1_PRIM_PLAN_FACTOR,0)))) RTL_SALES_VALUE_OUT_GST_PD
              ,0 AS TTL_CBM_NSV_P
              ,0 AS NETT_MARGIN_D_DOL
              ,0 AS NET_MARGIN_RD_DOL
              ,0 AS NETT_MARGIN_RDC_DOL
              ,( ((NVL(BRANCH_DATA.LIST_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) - (NVL(BRANCH_DATA.MARKET_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END))) RRT_TTL_EVT_LSV_LESS_MPA
              ,((NVL(BRANCH_DATA.LIST_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) - (NVL(BRANCH_DATA.MARKET_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)) AS RR_INCR_LSV_LESS_MPA
              ,0 AS RR_PERC_TRADE_PSEND_GSV
              ,NVL(BRANCH_DATA.EVENT_COST, 0) AS EVENT_COST
              ,(CASE WHEN BRANCH_DATA.IS_SELF = 0 THEN 2 ELSE 1 END) AS IS_PROMOTION
              ,2 RECORD_TYPE
              ,BRANCH_DATA.LIST_PRICE_P
              ,BRANCH_DATA.MARKET_PRICE_P
              ,((NVL(BRANCH_DATA.LIST_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) - (NVL(BRANCH_DATA.MARKET_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END))-(CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END * NVL(BRANCH_DATA.TOT_EFFICIENCIES_P, 0) * (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))- CASE WHEN BRANCH_DATA.ACTIVITY_TYPE_ID = 13 THEN  ((GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)) + NVL(BRANCH_DATA.CASE_BUYDOWN, 0)  + (NVL(BRANCH_DATA.RR_HANDLING_D, 0) * NVL(BRANCH_DATA.UNITS, 0))) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)  + (NVL(BRANCH_DATA.EVENT_COST, 0)) + (NVL(BRANCH_DATA.RR_HANDLING_COOP, 0)) ELSE 0 END) AS TOTAL_EVENT_DOLLARS
              ,(((NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0) - ((NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0)) * NVL(BRANCH_DATA.TOT_EFFICIENCIES_P, 0)) - (GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)))) *  NVL(BRANCH_DATA.REBATE_P, 0)  * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,   NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) +  ((GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)) + NVL(BRANCH_DATA.CASE_BUYDOWN, 0)  + (NVL(BRANCH_DATA.RR_HANDLING_D, 0) * NVL(BRANCH_DATA.UNITS, 0))) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)  + (NVL(BRANCH_DATA.EVENT_COST, 0)) + (NVL(BRANCH_DATA.RR_HANDLING_COOP, 0))) AS TTL_EVT_INCR_SPEND_D_PD
              ,((NVL(BRANCH_DATA.LIST_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) - (NVL(BRANCH_DATA.MARKET_PRICE_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END))-(CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END * NVL(BRANCH_DATA.TOT_EFFICIENCIES_P, 0) * (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))- CASE WHEN BRANCH_DATA.ACTIVITY_TYPE_ID = 13 THEN  ((GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0))  * NVL(BRANCH_DATA.RR_OI_P, 0)) + NVL(BRANCH_DATA.CASE_BUYDOWN, 0)  + (NVL(BRANCH_DATA.RR_HANDLING_D, 0) * NVL(BRANCH_DATA.UNITS, 0))) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)  + (NVL(BRANCH_DATA.EVENT_COST, 0)) + (NVL(BRANCH_DATA.RR_HANDLING_COOP, 0)) ELSE 0 END) AS INCR_EVENT_DOLLARS
              ,((NVL(BRANCH_DATA.COGS_P, 0)  * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE  NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) +(NVL(BRANCH_DATA.DISTRIBUTION_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)   + (NVL(BRANCH_DATA.SPOILS_P, 0))+ (NVL(BRANCH_DATA.NON_WORK_CO_MARK_A, 0))) AS INCR_CBM_PART_2
              ,((NVL(BRANCH_DATA.CASE_BUYDOWN, 0) + GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), (NVL(BRANCH_DATA.LIST_PRICE_P, 0) - NVL(BRANCH_DATA.MARKET_PRICE_P, 0)) * NVL(BRANCH_DATA.RR_OI_P, 0))) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END ) TOTAL_OI_CASE_DEAL
              ,(((NVL(BRANCH_DATA.CASE_BUYDOWN, 0) * NVL(BRANCH_DATA.RR_REDEMP_P, 0) ) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) + (GREATEST(NVL(BRANCH_DATA.CASE_BUYDOWNOI, 0), NVL(BRANCH_DATA.LIST_PRICE_P, 0) * NVL(BRANCH_DATA.RR_OI_P, 0)) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)  + NVL(BRANCH_DATA.EVENT_COST, 0)) TOTAL_OI_CASE_DEAL_REB_COOP
              ,((NVL(BRANCH_DATA.COGS_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END) +(NVL(BRANCH_DATA.DISTRIBUTION_P, 0) * CASE WHEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) <> 0 THEN NVL(BRANCH_DATA.EVT_VOL_ACT, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_ACT, 0) ELSE NVL(BRANCH_DATA.VOLUME_BASE_TTL, 0) + NVL(BRANCH_DATA.INCR_EVT_VOL_OR,  NVL(BRANCH_DATA.RR_ACCRUAL_ENG_INCR_COMMIT, NVL(BRANCH_DATA.FORE_5_UPLIFT, NVL(BRANCH_DATA.' || v_fore_col || '_UPLIFT,0)))) END)   + (NVL(BRANCH_DATA.SPOILS_P, 0))+ (NVL(BRANCH_DATA.NON_WORK_CO_MARK_A, 0))) CBM_PART_2_PD
              FROM PROMOTION_DATA BRANCH_DATA
              ,PROMOTION_MATRIX
              ,PROMOTION
              ,PROMOTION_DATES
              ,T_EP_ITEM
              ,MDP_MATRIX
              ,T_EP_SITE
              ,SCENARIO
              ,T_EP_E1_ITEM_CAT_6
              ,PROMOTION_TYPE
              ,PROMOTION_STAT
              ,TBL_CD_DEAL_TYPE
              ,TBL_COOP_DEAL_TYPE
              ,TBL_OI_DEAL_TYPE
              WHERE PROMOTION.PROMOTION_ID = PROMOTION_MATRIX.PROMOTION_ID
              AND NVL(PROMOTION.CD_DEAL_TYPE, 0) = TBL_CD_DEAL_TYPE.CD_DEAL_TYPE_ID
              AND NVL(PROMOTION.COOP_DEAL_TYPE, 0) = TBL_COOP_DEAL_TYPE.COOP_DEAL_TYPE_ID
              AND NVL(PROMOTION.OI_DEAL_TYPE, 0) = TBL_OI_DEAL_TYPE.OI_DEAL_TYPE_ID
              AND PROMOTION.PROMOTION_ID = PROMOTION_DATES.PROMOTION_ID
              AND PROMOTION.SCENARIO_ID = SCENARIO.SCENARIO_ID
              AND PROMOTION.SCENARIO_ID IN (22, 262, 162)
              AND PROMOTION.PROMOTION_STAT_ID = PROMOTION_STAT.PROMOTION_STAT_ID
              AND PROMOTION_MATRIX.PROMOTION_ID = BRANCH_DATA.PROMOTION_ID
              AND PROMOTION_MATRIX.ITEM_ID = BRANCH_DATA.ITEM_ID
              AND PROMOTION_MATRIX.LOCATION_ID = BRANCH_DATA.LOCATION_ID
              AND PROMOTION_MATRIX.ITEM_ID = MDP_MATRIX.ITEM_ID
              AND PROMOTION_MATRIX.LOCATION_ID = MDP_MATRIX.LOCATION_ID
              AND BRANCH_DATA.ACTIVITY_TYPE_ID = PROMOTION_TYPE.PROMOTION_TYPE_ID
              AND MDP_MATRIX.T_EP_SITE_EP_ID = T_EP_SITE.T_EP_SITE_EP_ID
              AND MDP_MATRIX.T_EP_ITEM_EP_ID = T_EP_ITEM.T_EP_ITEM_EP_ID
              AND MDP_MATRIX.T_EP_E1_ITEM_CAT_6_EP_ID = T_EP_E1_ITEM_CAT_6.T_EP_E1_ITEM_CAT_6_EP_ID
              AND BRANCH_DATA.SALES_DATE BETWEEN TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_start_offset || ' * 7)
                                            AND TO_DATE(get_max_date, ''MM-DD-YYYY HH24:MI:SS'') + (' || p_end_offset || ' * 7)
              ';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE BIEO_TRADESPEND_PRO AS
              SELECT /*+ FULL(tmp_tab) PARALLEL(tmp_tab, 64) */
              SDATE
              ,LEVEL1
              ,LEVEL2
              ,LEVEL3
              ,LEVEL4
              ,LEVEL5
              ,PROMOTION_STATUS
              ,ACTIVITY_TYPE_ID
              ,CD_DEAL_TYPE
              ,COOP_DEAL_TYPE
              ,OI_DEAL_TYPE
              ,NUM_WEEKS
              ,PROMO_PRICE
              ,EVENT_BASE
              ,SAFE_DIVISION(NVL(EVENT_BASE, 0) + NVL(INCR_EVENT_VOL, 0), NVL(EVENT_BASE, 0), 1) EVENT_LIFT_FACTOR
              ,INCR_EVENT_VOL
              ,(NVL(EVENT_BASE, 0) + NVL(INCR_EVENT_VOL, 0)) * GREATEST(NVL(CASE_BUYDOWNOI, 0), (NVL(LIST_PRICE_P, 0) - NVL(MARKET_PRICE_P, 0)) * NVL(RR_OI_P, 0) ) OI_SPEND
              ,TOTAL_EVENT_DOLLARS
              ,EVENT_CPIC
              ,START_PROMO
              ,GST_RATE
              ,SAFE_DIVISION(NVL(INCR_EVT_NSV_D_PD, 0) - NVL(INCR_CBM_PART_2, 0), TTL_EVT_SPEND_D_PD,0) TTL_EVT_TS_ROI
              ,RR_ROI_EFFECIENCIES_PD
              ,ROI_REBATE_PD
              ,RR_HANDLING_COOP
              ,RR_HANDLING_D
              ,CASE_BUYDOWN
              ,SAFE_DIVISION(TTL_EVT_SPEND_D_PD, RRT_TTL_EVT_LSV_LESS_MPA, 0) PERC_TRADE_SPEND
              ,ED_PRICE
              ,CASE_BUYDOWNOI
              ,CASE_DEAL_PD
              ,PROMO_NAME
              ,RR_ROI_REBATE
              ,NVL(TOTAL_EVENT_DOLLARS, 0) - NVL(TTL_EVT_SPEND_D_PD, 0) TTL_EVT_NSV
              ,RR_OI_P
              ,RR_REDEMP_P
              ,TTL_EVT_SPEND_D_PD
              ,NVL(INCR_EVENT_DOLLARS, 0) - NVL(INCR_EVENT_DOLLARS, 0) INCR_EVT_NSV_D_PD
              ,NVL(INCR_EVENT_DOLLARS, 0) - NVL(INCR_EVENT_DOLLARS, 0) - NVL(INCR_CBM_PART_2, 0) INCR_EVT_CBM_D_PD
              ,NVL(TOTAL_EVENT_DOLLARS, 0) - NVL(TTL_EVT_SPEND_D_PD, 0) - NVL(CBM_PART_2_PD, 0) TTL_EVT_CBM_D_PD
              ,RTL_SALES_VALUE_OUT_GST_PD
              ,SAFE_DIVISION(NVL(TOTAL_EVENT_DOLLARS, 0) - NVL(TTL_EVT_SPEND_D_PD, 0) - NVL(CBM_PART_2_PD, 0), NVL(TOTAL_EVENT_DOLLARS, 0) - NVL(TTL_EVT_SPEND_D_PD, 0), 0) TTL_CBM_NSV_P
              ,NVL(RTL_SALES_VALUE_OUT_GST_PD, 0) - NVL(RRT_TTL_EVT_LSV_LESS_MPA, 0) + NVL(RR_ROI_EFFECIENCIES_PD, 0) + NVL(TOTAL_OI_CASE_DEAL, 0) NETT_MARGIN_D_DOL
              ,NVL(RTL_SALES_VALUE_OUT_GST_PD, 0) - NVL(RRT_TTL_EVT_LSV_LESS_MPA, 0) + NVL(RR_ROI_EFFECIENCIES_PD, 0) + NVL(ROI_REBATE_PD, 0) + NVL(TOTAL_OI_CASE_DEAL, 0) NET_MARGIN_RD_DOL
              ,NVL(RTL_SALES_VALUE_OUT_GST_PD, 0) - NVL(RRT_TTL_EVT_LSV_LESS_MPA, 0) + NVL(RR_ROI_EFFECIENCIES_PD, 0) + NVL(ROI_REBATE_PD, 0) + NVL(TOTAL_OI_CASE_DEAL_REB_COOP, 0) NETT_MARGIN_RDC_DOL
              ,RRT_TTL_EVT_LSV_LESS_MPA
              ,RR_INCR_LSV_LESS_MPA
              ,SAFE_DIVISION(TOTAL_EVENT_DOLLARS, TTL_EVT_SPEND_D_PD, 0) RR_PERC_TRADE_PSEND_GSV
              ,EVENT_COST
              ,IS_PROMOTION
              ,RECORD_TYPE
              FROM BIEO_TRADESPEND_PRO_TMP tmp_tab';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            RAISE;
         END;
  END;

  PROCEDURE prc_export_ts_nonpro(p_start_offset  NUMBER,
                                         p_end_offset    NUMBER)
    IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_TS_NONPRO
   PURPOSE:    Export exfactory fcst

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        27/02/2013  Lakshmi Annapragada // Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   v_fore_col       VARCHAR2(10);

   v_prev_fy_start    DATE;
   v_curr_fy_start    DATE;
   v_next_fy_end      DATE;

   v_fy_id            NUMBER;
   v_scan_start       NUMBER := 2;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_TS_NONPRO';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

    SELECT t_ep_fiscal_yr_id INTO v_fy_id
    FROM inputs
    WHERE datet = TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS');

    SELECT MIN(datet) INTO v_prev_fy_start
    FROM inputs
    WHERE t_ep_fiscal_yr_id = v_fy_id - 1;

    SELECT MIN(datet) INTO v_curr_fy_start
    FROM inputs
    WHERE t_ep_fiscal_yr_id = v_fy_id;

    SELECT MAX(datet) INTO v_next_fy_end
    FROM inputs
    WHERE t_ep_fiscal_yr_id = v_fy_id + 1;

    SELECT LOWER(get_fore_col(0, 1)) INTO v_fore_col
    FROM dual;

    check_and_drop('bieo_ts_nonpro_tmp1');

    check_and_drop('bieo_ts_nonpro_fcst_tmp');

    check_and_drop('bieo_ts_nonpro_scan_tmp');

    check_and_drop('bieo_ts_nonpro_fcst');

    check_and_drop('bieo_ts_nonpro_scan');

    v_sql := 'CREATE TABLE  bieo_ts_nonpro_tmp1 AS
              SELECT /*+ FULL(pd) PARALLEL(pd, 32) */  pd.item_id,
              pd.location_id,
              pd.sales_date,
              CASE WHEN MAX(pd.promotion_stat_id) = 8 THEN 1 ELSE 0 END promotion_stat,
              safe_division(SUM(CASE WHEN NVL(pd.promo_price, 0) <> 0 AND NVL(pd.rr_accrual_vol_pd, 0) <> 0
                                     THEN NVL(pd.promo_price, 0) *  NVL(pd.rr_accrual_vol_pd, 0) ELSE 0 END),
                            SUM(CASE WHEN NVL(pd.promo_price, 0) <> 0 AND NVL(pd.rr_accrual_vol_pd, 0) <> 0
                                     THEN NVL(pd.rr_accrual_vol_pd, 0) ELSE 0 END), 0) promo_price,
              SUM(CASE WHEN NVL(pd.rr_accrual_cd_pd, 0) <> 0 THEN  pd.rr_accrual_vol_pd ELSE 0 END) rr_accrual_vol_pd,
              SUM(pd.rr_accrual_cd_pd) rr_accrual_cd_pd,
              SUM(CASE WHEN NVL(p.coop_deal_type, 0) <> 9 THEN  pd.rr_accrual_coop_pd ELSE 0 END) rr_accrual_coop_pd,
              SUM(CASE WHEN NVL(p.coop_deal_type, 0) = 9 THEN  pd.rr_accrual_coop_pd ELSE 0 END) rr_accrual_inc_coop_pd,
              SUM(DECODE(pd.promotion_type_id, 13,  CASE WHEN NVL(pd.rr_accrual_cd_pd, 0) <> 0 THEN  pd.rr_accrual_vol_pd ELSE 0 END, 0)) rr_accrual_vol_pd_06,
              SUM(DECODE(pd.promotion_type_id, 13,  pd.rr_accrual_cd_pd, 0)) rr_accrual_cd_pd_06,
              SUM(DECODE(pd.promotion_type_id, 13,  pd.rr_accrual_coop_pd, 0)) rr_accrual_coop_pd_06,
              SUM(pd.rr_approved_claim_cd_pd) rr_approved_claim_cd_pd,
              SUM(CASE WHEN NVL(p.coop_deal_type, 0) <> 9 THEN  pd.rr_approved_claim_coop_pd ELSE 0 END) rr_approved_claim_coop_pd,
              SUM(CASE WHEN NVL(p.coop_deal_type, 0) = 9 THEN  pd.rr_approved_claim_coop_pd ELSE 0 END) rr_approved_claim_inc_coop_pd,
              SUM(DECODE(pd.promotion_type_id, 13,  pd.rr_approved_claim_cd_pd, 0)) rr_approved_claim_cd_pd_06,
              SUM(DECODE(pd.promotion_type_id, 13,  pd.rr_approved_claim_coop_pd, 0)) rr_approved_claim_coop_pd_06
              FROM promotion_data pd,
              --tbl_coop_deal_type coop,
              promotion p
              WHERE pd.scenario_id IN (22, 162, 262)
              --AND NVL(p.coop_deal_type, 0) = coop.coop_deal_type_id
              AND p.promotion_id = pd.promotion_id
              AND pd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_prev_fy_start, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                    AND TO_DATE(''' || TO_CHAR(v_next_fy_end, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
              --AND 1 = 2
              GROUP BY pd.item_id,
              pd.location_id,
              pd.sales_date
              ';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE UNIQUE INDEX bieo_ts_nonpro_tmp1_idx ON bieo_ts_nonpro_tmp1( item_id, location_id, sales_date)';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE  bieo_ts_nonpro_fcst_tmp AS
              SELECT /*+ FULL(sd) PARALLEL(sd, 32) */ sd.sales_date sdate,
              i.item product_no,
              ic6.e1_item_cat_6 company,
              ec.ebs_customer key_account,
              SUM(NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0)) volume_base_ttl_db,
              SUM(NVL(sd.sd_uplift, 0)) sd_uplift,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))) total_fcst_eff,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              - (greatest(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0)
                 + NVL(sd.sd_uplift, 0)))) * (NVL(sd.bdf_fixed_funds_corp, 0)))  total_fcst_rebate,
              SUM(NVL(sd.ebspricelist102, 0) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              total_fcst_distribution,
              SUM(NVL(sd.ebspricelist127, 0) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              total_fcst_spoils,
              SUM(NVL(sd.ebspricelist126, 0) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              total_fcst_cogs,
              SUM(NVL(sd.ff_input1, 0)) noncowork,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              - (greatest(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0)
                 + NVL(sd.sd_uplift, 0))))) total_fcst_niv,
              SUM(((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ff_2, 0)) *
              (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))) total_fcst_roi_eff,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)))
              - (greatest(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0)
                 + NVL(sd.sd_uplift, 0)))) * (NVL(sd.ff_4, 0)))  total_fcst_roi_rebate ,
              SUM((NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))
              * NVL(sd.ebspricelist128, 0)) total_fcst_lsv,
              SUM((NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))
              * NVL(sd.ebspricelist124, 0)) total_fcst_mpa,
              MAX(NVL(i.e1_prim_plan_factor, 0)) gst,
              SUM((NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0)) *
              safe_division(pd.rr_accrual_cd_pd_06, pd.rr_accrual_vol_pd_06, 0)) total_fcst_cd_06,
              SUM(NVL(pd.rr_accrual_coop_pd_06, 0)) total_fcst_coop_06,
              SUM(GREATEST(NVL(sd.mkt_input1, 0), (NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * (NVL(sd.si_acc2, 0))) *
              (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))) total_fcst_oi_06,
              SUM(NVL(pd.rr_accrual_cd_pd, 0)) total_fcst_cd,
              SUM(NVL(pd.rr_accrual_vol_pd, 0)) total_fcst_cd_vol,
              SUM(NVL(pd.rr_accrual_coop_pd, 0)) total_fcst_coop,
              SUM(NVL(pd.rr_accrual_inc_coop_pd, 0)) total_fcst_inc_coop,
              SUM((greatest(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) * (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0)
                 + NVL(sd.sd_uplift, 0)))) total_fcst_oi,
              SUM(NVL(i.e1_ship_uom_mult, 0) * NVL( sd.rr_rrp_oride, NVL(sd.shelf_price_sd, 0)) *
              (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))) total_shelf_dol,
              SUM(NVL(i.e1_ship_uom_mult, 0) * CASE WHEN NVL(pd.promo_price, 0) = 0 THEN NVL( sd.rr_rrp_oride, NVL(sd.shelf_price_sd, 0)) ELSE NVL(pd.promo_price, 0) END *
              (NVL(NVL(sd.manual_stat, NVL(sd.sim_val_182,nvl(sd.' || v_fore_col || ', sd.npi_forecast)))*(1.00+ NVL(sd.manual_fact,0)), 0) + NVL(sd.sd_uplift, 0))) total_promo_dol
              FROM sales_data sd,
              bieo_ts_nonpro_tmp1 pd,
              mdp_matrix mdp,
              t_ep_item i,
              t_ep_ebs_customer ec,
              t_ep_e1_item_cat_6 ic6
              WHERE sd.item_id = mdp.item_id
              AND sd.location_id = mdp.location_id
              AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_curr_fy_start, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                    AND TO_DATE(''' || TO_CHAR(v_next_fy_end, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
              AND sd.item_id = pd.item_id(+)
              AND sd.location_id= pd.location_id(+)
              AND sd.sales_date = pd.sales_date(+)
              AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
              AND mdp.t_ep_ebs_customer_ep_id = ec.t_ep_ebs_customer_ep_id
              AND mdp.t_ep_e1_item_cat_6_ep_id = ic6.t_ep_e1_item_cat_6_ep_id
              --AND 1 = 2
              GROUP BY sd.sales_date,
              i.item,
              ic6.e1_item_cat_6,
              ec.ebs_customer';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE  bieo_ts_nonpro_scan_tmp AS
              SELECT /*+ FULL(sd) PARALLEL(sd, 32) */ sd.sales_date sdate,
              i.item product_no,
              ic6.e1_item_cat_6 company,
              ec.ebs_customer key_account,
              SUM(NVL(sd.sdata5, 0)) scan_base,
              SUM(NVL(sd.sdata6, 0)) scan_incr,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_eff,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))
              - (GREATEST(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) *
                 (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))) * (NVL(sd.bdf_fixed_funds_corp, 0)))  total_scan_rebate,
              SUM(NVL(sd.ebspricelist102, 0) * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_distribution,
              SUM(NVL(sd.ebspricelist127, 0) * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_spoils,
              SUM(NVL(sd.ebspricelist126, 0) * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_cogs,
              SUM(NVL(sd.ff_input1, 0)) noncowork,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))
              - (GREATEST(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))))) total_scan_niv,
              SUM(((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ff_2, 0)) *
              (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_roi_eff,
              SUM((((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0) - ((NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * NVL(sd.ebspricelist100, 0) ))
              * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))
              - (greatest(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 nvl(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - nvl(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0)) *
                 (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))) * (NVL(sd.ff_4, 0)))  total_scan_roi_rebate ,
              SUM((NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))
              * NVL(sd.ebspricelist128, 0)) total_scan_lsv,
              SUM((NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))
              * NVL(sd.ebspricelist124, 0)) total_scan_mpa,
              MAX(NVL(i.e1_prim_plan_factor, 0)) gst,
              SUM((NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)) *
              safe_division(pd.rr_accrual_cd_pd_06, pd.rr_accrual_vol_pd_06, 0)) total_scan_cd_06,
              SUM(NVL(pd.rr_accrual_coop_pd_06, 0)) total_scan_coop_06,
              SUM(GREATEST(NVL(sd.mkt_input1, 0), (NVL(sd.ebspricelist128, 0) - NVL(sd.ebspricelist124, 0)) * (NVL(sd.si_acc2, 0))) *
              (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_scan_oi_06,
              SUM(NVL(pd.rr_accrual_cd_pd, 0)) total_scan_cd,
              SUM(NVL(pd.rr_accrual_vol_pd, 0)) total_scan_cd_vol,
              SUM(NVL(pd.rr_accrual_coop_pd, 0)) total_scan_coop,
              SUM(NVL(pd.rr_accrual_inc_coop_pd, 0)) total_scan_inc_coop,
              SUM((GREATEST(nvl(sd.ebspricelist104, 0), nvl(sd.mkt_acc2, 0), nvl(sd.mkt_input1, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) *
                 NVL(sd.ebspricelist103, 0), (nvl(sd.ebspricelist128, 0) - nvl(sd.ebspricelist124, 0)) * nvl(sd.mkt_acc1, 0),  (nvl(sd.ebspricelist128, 0)
                 - NVL(sd.ebspricelist124, 0)) * nvl(sd.si_acc2, 0))
                 * (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0)))) total_scan_oi,
              SUM(NVL(i.e1_ship_uom_mult, 0) * NVL(sd.shelf_price_sd, 0) *
              (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_act_shelf_dol,
              SUM(NVL(i.e1_ship_uom_mult, 0) * CASE WHEN NVL(sd.sdata15, 0) = 0 THEN NVL(sd.shelf_price_sd, 0) ELSE NVL(sd.sdata15, 0) END *
              (NVL(sd.sdata5, 0) + NVL(sd.sdata6, 0))) total_act_promo_dol,
              MIN(CASE WHEN NVL(pd.promotion_stat, -1) = -1 THEN 1 ELSE pd.promotion_stat END) promotion_stat,
              SUM(NVL(rr_approved_claim_cd_pd, 0)) approved_claim_cd,
              SUM(NVL(pd.rr_approved_claim_coop_pd, 0)) approved_claim_coop,
              SUM(NVL(pd.rr_approved_claim_inc_coop_pd, 0)) approved_claim_inc_coop,
              SUM(NVL(rr_approved_claim_cd_pd_06, 0)) approved_claim_cd_06,
              SUM(NVL(pd.rr_approved_claim_coop_pd_06, 0)) approved_claim_coop_06
              FROM sales_data sd,
              bieo_ts_nonpro_tmp1 pd,
              mdp_matrix mdp,
              t_ep_item i,
              t_ep_ebs_customer ec,
              t_ep_e1_item_cat_6 ic6
              WHERE sd.item_id = mdp.item_id
              AND sd.location_id = mdp.location_id
              AND sd.sales_date BETWEEN TO_DATE(''' || TO_CHAR(v_prev_fy_start, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                    AND TO_DATE(''' || TO_CHAR(TO_DATE(get_max_date, 'MM-DD-YYYY HH24:MI:SS'), 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')
                                                - ( ' || v_scan_start || ' *7)
              AND sd.item_id = pd.item_id(+)
              AND sd.location_id= pd.location_id(+)
              AND sd.sales_date = pd.sales_date(+)
              AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
              AND mdp.t_ep_ebs_customer_ep_id = ec.t_ep_ebs_customer_ep_id
              AND mdp.t_ep_e1_item_cat_6_ep_id = ic6.t_ep_e1_item_cat_6_ep_id
              --AND 1 = 2
              GROUP BY sd.sales_date,
              i.item,
              ic6.e1_item_cat_6,
              ec.ebs_customer';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE bieo_ts_nonpro_fcst AS
              SELECT /*+ FULL(tmp_tab) PARALLEL(tmp_tab, 32) */  * FROM bieo_ts_nonpro_fcst_tmp tmp_tab
              WHERE
              ABS(NVL(total_promo_dol, 0)) +
              ABS(NVL(total_shelf_dol, 0)) +
              ABS(NVL(total_fcst_oi, 0)) +
              ABS(NVL(total_fcst_coop, 0)) +
              ABS(NVL(total_fcst_inc_coop, 0)) +
              ABS(NVL(total_fcst_cd, 0)) +
              ABS(NVL(total_fcst_cd_vol, 0)) +
              ABS(NVL(total_fcst_oi_06, 0)) +
              ABS(NVL(total_fcst_coop_06, 0)) +
              ABS(NVL(total_fcst_cd_06, 0)) +
              ABS(NVL(total_fcst_mpa, 0)) +
              ABS(NVL(total_fcst_lsv, 0)) +
              ABS(NVL(total_fcst_roi_rebate, 0)) +
              ABS(NVL(total_fcst_roi_eff, 0)) +
              ABS(NVL(total_fcst_niv, 0)) +
              ABS(NVL(noncowork, 0)) +
              ABS(NVL(total_fcst_cogs, 0)) +
              ABS(NVL(total_fcst_spoils, 0)) +
              ABS(NVL(total_fcst_distribution, 0)) +
              ABS(NVL(total_fcst_rebate, 0)) +
              ABS(NVL(total_fcst_eff, 0)) +
              ABS(NVL(sd_uplift, 0)) +
              ABS(NVL(volume_base_ttl_db, 0)) <> 0';

    dynamic_ddl(v_sql);

    v_sql := 'CREATE TABLE bieo_ts_nonpro_scan AS
              SELECT /*+ FULL(tmp_tab) PARALLEL(tmp_tab, 32) */  * FROM bieo_ts_nonpro_scan_tmp tmp_tab
              WHERE
              ABS(NVL(approved_claim_coop_06, 0)) +
              ABS(NVL(approved_claim_cd_06, 0)) +
              ABS(NVL(approved_claim_coop, 0)) +
              ABS(NVL(approved_claim_inc_coop, 0)) +
              ABS(NVL(approved_claim_cd, 0)) +
              ABS(NVL(total_act_promo_dol, 0)) +
              ABS(NVL(total_act_shelf_dol, 0)) +
              ABS(NVL(total_scan_oi, 0)) +
              ABS(NVL(total_scan_coop, 0)) +
              ABS(NVL(total_scan_inc_coop, 0)) +
              ABS(NVL(total_scan_cd, 0)) +
              ABS(NVL(total_scan_cd_vol, 0)) +
              ABS(NVL(total_scan_oi_06, 0)) +
              ABS(NVL(total_scan_coop_06, 0)) +
              ABS(NVL(total_scan_cd_06, 0)) +
              ABS(NVL(total_scan_mpa, 0)) +
              ABS(NVL(total_scan_lsv, 0)) +
              ABS(NVL(total_scan_roi_rebate, 0)) +
              ABS(NVL(total_scan_roi_eff, 0)) +
              ABS(NVL(total_scan_niv, 0)) +
              ABS(NVL(noncowork, 0)) +
              ABS(NVL(total_scan_cogs, 0)) +
              ABS(NVL(total_scan_spoils, 0)) +
              ABS(NVL(total_scan_distribution, 0)) +
              ABS(NVL(total_scan_rebate, 0)) +
              ABS(NVL(total_scan_eff, 0)) +
              ABS(NVL(scan_incr, 0)) +
              ABS(NVL(scan_base, 0))  <> 0';

    dynamic_ddl(v_sql);

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            -- RAISE;
         END;
  END;


  PROCEDURE prc_export_contracts(P_START_OFFSET  NUMBER,
                                  p_end_offset    NUMBER)
                                   IS
/*****************************************************************************************************************************
   NAME:       PRC_EXPORT_CONTRACTS
   PURPOSE:    Export contracts(SAP Rebates)

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ----------------------------------------------------------------------------------------------------
   1.0        14/08/2015  Lakshmi Annapragada // UXC Red Rock Consulting

*****************************************************************************************************************************/
   ---
   v_prog_name      VARCHAR2(100);
   v_status         VARCHAR2(100);
   v_proc_log_id    NUMBER;
   v_sql            VARCHAR2(32767);

   V_FORE_COL       varchar2(10);
   V_VAKEY          VARCHAR2(300);
   v_sql_str VARCHAR2(32000);
--   v_prev_fy_start    DATE;
--   v_curr_fy_start    DATE;
--   v_next_fy_end      DATE;
--
--   v_fy_id            NUMBER;
--   v_scan_start       NUMBER := 2;

   BEGIN

    pre_logon;

    v_status := 'Start ';
    v_prog_name := 'PRC_EXPORT_CONTRACTS';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex(v_prog_name, v_package_name, v_status || SYSDATE);

     FOR rec IN (SELECT DISTINCT access_Seq_id,
                 DECODE(NVL (T_EP_P4_EP_ID, 0), 0, 0, 1) p4_flag,
                 DECODE(NVL (t_ep_e1_cust_cat_4_EP_ID, 0), 0, 0, 1) site_flag,
                 DECODE(NVL (T_EP_EBS_CUSTOMER_EP_ID, 0), 0, 0, 1) ebs_cust_flag,
                 DECODE(NVL (T_EP_P3_EP_ID, 0), 0, 0, 1) p3_flag,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_4_EP_ID, 0), 0, 0, 1) E1_I4_FLAG,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_6_EP_ID, 0), 0, 0, 1) e1_i6_flag,
                 DECODE(NVL (T_EP_EBS_PROD_CAT_EP_ID, 0), 0, 0, 1) ebs_pc_flag,
                 DECODE(NVL (T_EP_E1_ITEM_CAT_7_EP_ID, 0), 0, 0, 1) e1_i7_flag,
                 DECODE(NVL (T_EP_P2_EP_ID, 0), 0, 0, 1) p2_flag,
                 DECODE(NVL (T_EP_I_ATT_6_EP_ID, 0), 0, 0, 1) i_att6_flag,
                 DECODE(NVL (T_EP_E1_CUST_CAT_2_EP_ID, 0), 0, 0, 1) e1_cust2_flag,
                 DECODE(NVL (T_EP_ITEM_EP_ID, 0), 0, 0, 1) item_flag,
                 DECODE(NVL (T_EP_I_ATT_10_EP_ID, 0), 0, 0, 1) I_ATT10_FLAG,
                 DECODE(NVL (T_EP_EBS_ACCOUNT_EP_ID, 0), 0, 0, 1) ACNT_FLAG,
                 DECODE(NVL (T_EP_E1_CUST_CTRY_EP_ID, 0), 0, 0, 1) CTRY_FLAG,
                 DECODE(NVL (t_ep_L_ATT_6_ep_id, 0), 0, 0, 1) l_att6_FLAG,
                 DECODE(NVL (t_ep_e1_cust_city_ep_id, 0), 0, 0, 1) city_FLAG
                from CONTRACT_LINE
                WHERE NVL(IS_FICTIVE, 0) = 0
               -- and CONTRACT_ID in (106)
                --and contract_payment_id = 206
                and CONTRACT_STAT_ID in (4,6,7,10)
                and contract_exp_Stat_id = 0
                AND   NVL(T_EP_P4_EP_ID,0)+
                NVL(T_EP_SITE_EP_ID,0)+
                nvl(T_EP_EBS_CUSTOMER_EP_ID,0)+
                NVL(T_EP_P3_EP_ID,0)+
                nvl(T_EP_E1_ITEM_CAT_4_EP_ID,0)+
                nvl(T_EP_E1_ITEM_CAT_6_EP_ID,0)+
                nvl(T_EP_EBS_PROD_CAT_EP_ID,0)+
                NVL(T_EP_E1_ITEM_CAT_7_EP_ID,0)+
                nvl(T_EP_P2_EP_ID,0)+
                nvl(T_EP_I_ATT_6_EP_ID,0)+
                NVL(T_EP_E1_CUST_CAT_2_EP_ID,0)+
                NVL(T_EP_ITEM_EP_ID,0)+
                NVL(T_EP_I_ATT_10_EP_ID,0)+
                NVL(T_EP_EBS_ACCOUNT_EP_ID,0)  +
                NVL(T_EP_E1_CUST_CTRY_EP_ID,0)+
                nvl(t_ep_L_ATT_6_ep_id,0) +
                nvl(t_ep_e1_cust_city_ep_id,0) > 0
                /*Add export_status condition*/
                order by 1 desc,2 desc,3 desc,4 desc,5 desc,6 desc,7 desc,8 desc,9 desc,10 desc,11 desc,12 desc,13 desc,14 desc,15 desc,16 desc,17 desc,18 desc
                )
    LOOP

    for REC1 in (
    SELECT CASE WHEN ASQ.LEVEL1 IS NOT NULL THEN CASE WHEN GT1.TRAILING_SPACES = 1 THEN 'RPAD('||GT1.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt1.num_chars_limit else GT1.NUM_CHARS end||','' '')'
     WHEN GT1.LEADING_ZEROES = 1 THEN 'lPAD('||GT1.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt1.num_chars_limit else GT1.NUM_CHARS end||',''0'')' END END||'||'||
     CASE WHEN ASQ.LEVEL2 IS NOT NULL THEN  CASE WHEN GT2.TRAILING_SPACES = 1 THEN 'RPAD('||GT2.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt2.num_chars_limit else GT2.NUM_CHARS end||','' '')'
     WHEN GT2.LEADING_ZEROES = 1 THEN 'lPAD('||case when asq.char_limit_flag = 1 then gt2.num_chars_limit else GT1.NUM_CHARS end||','||GT2.NUM_CHARS||',''0'')' END ||'||' END||
       CASE WHEN ASQ.LEVEL3 IS NOT NULL THEN  CASE WHEN GT3.TRAILING_SPACES = 1 THEN 'RPAD('||GT3.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt3.num_chars_limit else GT3.NUM_CHARS end||','' '')'
     WHEN GT3.LEADING_ZEROES = 1 THEN 'lPAD('||GT3.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt3.num_chars_limit else GT3.NUM_CHARS end||',''0'')' END ||'||' end||
       CASE WHEN ASQ.LEVEL4 IS NOT NULL THEN  CASE WHEN GT4.TRAILING_SPACES = 1 THEN 'RPAD('||GT4.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt4.num_chars_limit else GT4.NUM_CHARS end||','' '')'
     WHEN GT4.LEADING_ZEROES = 1 THEN 'lPAD('||GT4.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt4.num_chars_limit else GT4.NUM_CHARS end||',''0'')' END ||'||'end||
     CASE WHEN ASQ.LEVEL5 IS NOT NULL THEN  CASE WHEN GT5.TRAILING_SPACES = 1 THEN 'RPAD('||GT5.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt5.num_chars_limit else GT5.NUM_CHARS end||','' '')'
     WHEN GT5.LEADING_ZEROES = 1 THEN 'lPAD('||GT5.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt5.num_chars_limit else GT5.NUM_CHARS end||',''0'')' END||'||'  end||
     CASE WHEN ASQ.LEVEL6 IS NOT NULL THEN  CASE WHEN GT6.TRAILING_SPACES = 1 THEN 'RPAD('||GT6.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt6.num_chars_limit else GT6.NUM_CHARS end||','' '')'
     WHEN GT6.LEADING_ZEROES = 1 THEN 'lPAD('||GT6.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt6.num_chars_limit else GT6.NUM_CHARS end||',''0'')' END||'||'  end||
     CASE WHEN ASQ.LEVEL7 IS NOT NULL THEN  CASE WHEN GT7.TRAILING_SPACES = 1 THEN 'RPAD('||GT7.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt7.num_chars_limit else GT7.NUM_CHARS end||','' '')'
     WHEN GT7.LEADING_ZEROES = 1 THEN 'lPAD('||GT7.CODE_FIELD||','||case when asq.char_limit_flag = 1 then gt7.num_chars_limit else GT7.NUM_CHARS end||',''0'')' END END  VAKEY
--     gt2.code_field, gt3.code_field,gt4.code_Field
    FROM ACCESS_SEQ ASQ, rr_vakey_ref_t GT1,rr_vakey_ref_t GT2,rr_vakey_ref_t GT3,rr_vakey_ref_t GT4,rr_vakey_ref_t GT5,rr_vakey_ref_t GT6,rr_vakey_ref_t GT7
    WHERE NVL(ASQ.LEVEL1,1) = GT1.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL2,1) = GT2.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL3,1) = GT3.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL4,1) = GT4.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL5,1) = GT5.GROUP_TABLE_ID
    AND nvl(ASQ.LEVEL6,1) = GT6.GROUP_TABLE_ID
    AND NVL(ASQ.LEVEL7,1) = GT7.GROUP_TABLE_ID
    and asq.access_seq_id = rec.access_seq_id)
    LOOP

    V_VAKEY := rtrim(REC1.VAKEY,'||');


    v_sql_str := ' INSERT INTO T_EXP_OI(REBATE_PROMO_ID ,CONTRACT_COMP_TYPE,CONTRACT_COMP_LINE_ID,START_DATE
                        ,END_DATE,CONTRACT_PROMO_FLAG,MATERIAL_CODE,PROD_PACK_LVL3_CODE
                        ,PROD_PACK_LVL2_CODE,PROD_PACK_LVL1_CODE,PROD_BRAND_LVL3_CODE,PROD_BRAND_LVL2_CODE
                        ,PROD_BRAND_LVL1_CODE,CUSTOMER_CODE,CUSTOMER_LEVEL4_CODE,CUSTOMER_LEVEL3_CODE
                        ,CUSTOMER_LEVEL2_CODE,CUSTOMER_LEVEL1_CODE,SALES_OFFICE_CODE,TIER_CODE,SALES_ORG
                        ,DIST_CHANNEL,DIVISION,CUST_PRICING_PROC,TAX_CLASS_MAT,DEAL_TYPE,SAP_CONDITION_TABLE
                        ,PRICE_POINT1,PRICE_POINT2,PRICE_POINT3,CONDITION1,CONDITION2,CONDITION3,LUMP_SUM
                        ,STATUS,SAP_CONDITION_KEY,RECEPIENT_CUSTOMER,REBATE_PAYMENT_COMMENTS
                        ,REBATE_REFERENCE,TAX_CODE,CONT_SALES_ORG,CONT_DIST_CHANNEL,CONT_DIVISION)
                 SELECT case when ct.oi_flag = 0 then to_char(CL.CONTRACT_PAYMENT_ID) else to_char(cl.contract_payment_id)||''-''|| to_char(CL.CONTRACT_LINE_ID) end,CT.COMP_TYPE_DESC, CL.CONTRACT_LINE_ID, CL.START_DATE,
                        CL.END_DATE,''CONTRACT'','|| CASE WHEN REC.ITEM_FLAG = 0 THEN 'NULL' ELSE 'LPAD(MAT.ITEM,18,''0'')' END || ','||
                        CASE WHEN REC.E1_I7_FLAG = 0 THEN 'NULL' ELSE ' RPAD(PROD_PACK3.E1_ITEM_CAT_7,18,'' '')'  END || ','||
                        CASE WHEN REC.I_ATT10_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK2.I_ATT_10,18,'' '')'  END || ','||
                        CASE WHEN REC.EBS_PC_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_PACK1.EBS_PROD_CAT,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND3.E1_ITEM_CAT_6,18,'' '')'  END || ','||
                        CASE WHEN REC.E1_I4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(PROD_BRAND2.E1_ITEM_CAT_4,18,'' '')' END || ',
                        NULL,'||CASE WHEN REC.SITE_FLAG = 0 THEN 'NULL' ELSE 'LPAD(SITE.e1_cust_cat_4,10,''0'')'  END || ','||
                         CASE WHEN REC.ACNT_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL4.EBS_ACCOUNT,10,''0'')'  END || ','||
                         CASE WHEN REC.EBS_CUST_FLAG = 0 THEN 'NULL' ELSE 'LPAD(CUST_LVL3.EBS_CUSTOMER,10,''0'')' END || ','||
                         CASE WHEN REC.city_FLAG = 0 THEN 'NULL' ELSE 'RPAD(CUST_LVL2.e1_cust_city,10,'' '')'  END || ',
                        NULL,'||CASE WHEN REC.l_att6_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_OFF.L_ATT_6,4,'' '')'  END || ','||
                        CASE WHEN REC.E1_CUST2_FLAG = 0 THEN 'NULL' ELSE 'LPAD(TIER.E1_CUST_CAT_2,2,''0'')'  END || ','||
                        CASE WHEN REC.P4_FLAG = 0 THEN 'NULL' ELSE 'RPAD(SALES_ORG.P4,4,'' '')'  END || ','||
                        case when REC.P3_FLAG = 0 then 'NULL' else 'rpad(DIST_CHAN.P3,2,'' '')'  end || ','||
                        CASE WHEN REC.P2_FLAG = 0 THEN 'NULL' ELSE 'rpad(DIV.P2,2,'' '') '  END || ','||
                        CASE WHEN REC.CTRY_FLAG = 0 THEN 'NULL' ELSE 'rpad(PRIC_PRC.E1_CUST_CTRY,1,'' '') '  END || ','||
                        case when REC.I_ATT6_FLAG = 0 then 'NULL' else 'lpad(TAX.I_ATT_6,1,''0'') '  end || ', CT.COMP_TYPE_CODE,ACC.SAP_TABLE,
                        CL.TIER_MAX_VAL1,CL.TIER_MAX_VAL2,CL.TIER_MAX_VAL3,
                        DECODE(CT.DEAL_TYPE,1,CL.CASE_DEAL_PERC*100,2,CL.CASE_DEAL_AMT,0),DECODE(CT.DEAL_TYPE,1,CL.CASE_DEAL_PERC_L2*100,2,CL.CASE_DEAL_AMT_L2,0),
                        DECODE(CT.DEAL_TYPE,1,CL.CASE_DEAL_PERC_L3*100,2,CL.CASE_DEAL_AMT_L3,0),CL.LUMPSUM,
                        cs.contract_exp_stat_code , NULL  -- '||V_VAKEY||'
                        ,cust_num.e1_cust_cat_4,c.PAYMENT_COMMENTS,
                        c.investment_details ,tbl.tax_consid_desc,
                        so.sales_org,so.sales_dist,so.sales_division
                 FROM CONTRACT_LINE CL,COMP_TYPE CT , CONTRACT_STAT cs,contract c, t_cont_lvl5 so,
                      tbl_tax_consideration tbl,t_ep_e1_cust_cat_4 cust_num,  '||
                       CASE WHEN REC.ITEM_FLAG = 0 THEN NULL ELSE 'T_EP_ITEM MAT,' END ||
                       CASE WHEN REC.E1_I7_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_7 PROD_PACK3, ' END ||
                       case when REC.I_ATT10_FLAG = 0 then null else 'T_EP_I_ATT_10 PROD_PACK2, ' end ||
                       CASE WHEN REC.ebs_pc_flag = 0 THEN NULL ELSE 'T_EP_EBS_PROD_CAT PROD_PACK1, ' END ||
                       CASE WHEN REC.E1_I6_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_6 PROD_BRAND3, ' END ||
                       CASE WHEN REC.E1_I4_FLAG = 0 THEN NULL ELSE 'T_EP_E1_ITEM_CAT_4 PROD_BRAND2, ' END ||
                       CASE WHEN REC.SITE_FLAG = 0 THEN NULL ELSE 't_ep_e1_cust_cat_4 SITE,' END ||
                       CASE WHEN REC.ACNT_FLAG = 0 THEN NULL ELSE 'T_EP_EBS_ACCOUNT CUST_LVL4,' END ||
                       CASE WHEN REC.EBS_CUST_FLAG = 0 THEN NULL ELSE 'T_EP_EBS_CUSTOMER CUST_LVL3,' END ||
                       CASE WHEN REC.CITY_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CITY CUST_LVL2,' END ||
                       CASE WHEN REC.l_att6_FLAG = 0 THEN NULL ELSE 't_ep_L_ATT_6 SALES_OFF, ' END ||
                       CASE WHEN REC.E1_CUST2_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CAT_2 TIER,' END ||
                       CASE WHEN REC.P4_FLAG = 0 THEN NULL ELSE 'T_EP_P4 SALES_ORG,' END ||
                       CASE WHEN REC.P3_FLAG = 0 THEN NULL ELSE 'T_EP_P3 DIST_CHAN,' END ||
                       CASE WHEN REC.P2_FLAG = 0 THEN NULL ELSE 'T_EP_P2 DIV,' END ||
                       CASE WHEN REC.ctry_FLAG = 0 THEN NULL ELSE 'T_EP_E1_CUST_CTRY PRIC_PRC,' END ||
                       case when REC.I_ATT6_FLAG = 0 then null else 'T_EP_I_ATT_6 tax,' end ||
                       ' access_seq acc
                 WHERE CL.COMP_TYPE_ID = CT.COMP_TYPE_ID
                   AND cl.access_Seq_id = acc.access_Seq_id
                    and acc.access_seq_id = '||rec.access_seq_id||'
                   AND cs.contract_stat_id = cl.contract_stat_id
                   AND cl.contract_id = c.contract_id
                   and cl.CONTRACT_STAT_ID in (4,6,7,10)
                   and cl.contract_exp_Stat_id = 0
                   --- Changed Sanjiiv on 25/05/2017 
                   --- CR Future Lumpsum contract lines not to be included in the export
                   and not exists (select 1 from COMP_TYPE CT1
                                   where  CL.COMP_TYPE_ID = CT.COMP_TYPE_ID
                                   and ct.deal_type = 3                                                                                
                                   and trunc(cl.start_date) > sysdate - 1)
                   --- end of Change
                   AND tbl.tax_consid_id = c.TAX_CONSIDERATION
                   AND cl.t_cont_lvl5_ep_id = so.t_cont_lvl5_ep_id
                   AND c.recipient = cust_num.t_ep_e1_cust_cat_4_ep_id(+) '||
                   case when REC.ITEM_FLAG = 0 then null else ' AND CL.T_EP_ITEM_EP_ID = MAT.T_EP_ITEM_EP_ID' end ||
                   case when REC.E1_I7_FLAG = 0 then null else ' and CL.T_EP_E1_ITEM_CAT_7_EP_ID = PROD_PACK3.T_EP_E1_ITEM_CAT_7_EP_ID ' end ||
                   case when REC.I_ATT10_FLAG = 0 then null else '  AND CL.T_EP_I_ATT_10_EP_ID = PROD_PACK2.T_EP_I_ATT_10_EP_ID ' end ||
                   case when REC.ebs_pc_FLAG = 0 then null else '  AND CL.T_EP_EBS_PROD_CAT_EP_ID = PROD_PACK1.T_EP_EBS_PROD_CAT_EP_ID ' end ||
                   case when REC.E1_I6_FLAG = 0 then null else '  AND CL.T_EP_E1_ITEM_CAT_6_EP_ID = PROD_BRAND3.T_EP_E1_ITEM_CAT_6_EP_ID ' end ||
                   case when REC.E1_I4_FLAG = 0 then null else '  AND CL.T_EP_E1_ITEM_CAT_4_EP_ID = PROD_BRAND2.T_EP_E1_ITEM_CAT_4_EP_ID ' end ||
                   case when REC.SITE_FLAG = 0 then null else '  AND CL.t_ep_e1_cust_cat_4_EP_ID = SITE.t_ep_e1_cust_cat_4_EP_ID ' end ||
                   CASE WHEN REC.ACNT_FLAG = 0 THEN NULL ELSE '  AND CL.T_EP_EBS_ACCOUNT_EP_ID = CUST_LVL4.T_EP_EBS_ACCOUNT_EP_ID ' END ||
                   CASE WHEN REC.EBS_CUST_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_EBS_CUSTOMER_EP_ID = CUST_LVL3.T_EP_EBS_CUSTOMER_EP_ID ' END ||
                   CASE WHEN REC.city_FLAG = 0 THEN NULL ELSE ' AND cl.t_ep_e1_cust_city_ep_id = cust_lvl2.t_ep_e1_cust_city_ep_id  ' END ||
                   CASE WHEN REC.l_att6_FLAG = 0 THEN NULL ELSE ' AND cl.t_ep_L_ATT_6_ep_id = sales_off.t_ep_L_ATT_6_ep_id  ' END ||
                   CASE WHEN REC.E1_CUST2_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_E1_CUST_CAT_2_EP_ID = TIER.T_EP_E1_CUST_CAT_2_EP_ID ' END ||
                   CASE WHEN REC.P4_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_P4_EP_ID = SALES_ORG.T_EP_P4_EP_ID ' END ||
                   CASE WHEN REC.P3_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_P3_EP_ID = DIST_CHAN.T_EP_P3_EP_ID ' END ||
                   CASE WHEN REC.P2_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_P2_EP_ID = DIV.T_EP_P2_EP_ID ' END ||
                   CASE WHEN REC.ctry_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_E1_CUST_CTRY_EP_ID = PRIC_PRC.T_EP_E1_CUST_CTRY_EP_ID ' END ||
                   CASE WHEN REC.i_att6_FLAG = 0 THEN NULL ELSE ' AND CL.T_EP_I_ATT_6_EP_ID = TAX.T_EP_I_ATT_6_EP_ID                   ' END
                   ;
           RR_PKG_PROC_LOG.PRC_PROC_SQL_LOG(V_PROC_LOG_ID,V_SQL_STR);
          dynamic_ddl(v_sql_str);

         update contract_line set contract_exp_Stat_id = 1 where contract_exp_Stat_id = 0 and contract_line_id in (select CONTRACT_COMP_LINE_ID from T_EXP_OI where process_flag = 0);
    COMMIT;
       END loop;
    END LOOP;

--    FOR REC IN ( SELECT distinct condition_table from t_exp_oi)
--    LOOP
--
--    END LOOP;

    v_status := 'end ';
    v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, v_status || SYSDATE);

    EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            v_proc_log_id := rr_pkg_proc_log.fcn_dbex (v_prog_name, v_package_name, 'Fatal Error in Step: ' || v_status,
                            TO_CHAR(SQLCODE), TO_CHAR(SQLCODE),
                            dbms_utility.format_error_stack,
                            dbms_utility.format_error_backtrace
                          );
            -- RAISE;
         END;
  END;

END;

/
