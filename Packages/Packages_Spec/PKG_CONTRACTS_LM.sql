--------------------------------------------------------
--  DDL for Package PKG_CONTRACTS_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_CONTRACTS_LM" 
AS
 
/******************************************************************************
   NAME:       PKG_CONTRACTS_LM
   PURPOSE:    All level methods commanly used for Contracts solution
   REVISIONS:
 
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        02/07/2015  Lakshmi Annapragada/ UXC Redrock Consulting - Initial version
 
   ******************************************************************************/
 
   v_package_name VARCHAR2(100) := 'PKG_CONTRACTS_LM';
 
   
   PROCEDURE prc_create_contract(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL  );  
      
   PROCEDURE prc_create_investment(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL  );  
 
   PROCEDURE prc_create_investment_lines(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL,
   method_status OUT NUMBER);
   
   PROCEDURE prc_createas_contract_line(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL  );
 
   PROCEDURE prc_chg_cl_unplanned(
   user_id     NUMBER DEFAULT NULL,
   LEVEL_ID    number default null,
   member_id   NUMBER DEFAULT NULL  );
   
   PROCEDURE prc_chg_cl_planned(
   user_id     NUMBER DEFAULT NULL,
   LEVEL_ID    number default null,
   member_id   NUMBER DEFAULT NULL  );
   
    PROCEDURE prc_chg_cl_req_approval(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL  );
   
    PROCEDURE prc_chg_cl_approved(
   user_id     NUMBER DEFAULT NULL,
   LEVEL_ID    number default null,
   member_id   NUMBER DEFAULT NULL  );
   
    PROCEDURE prc_chg_cl_decline(
   user_id     NUMBER DEFAULT NULL,
   LEVEL_ID    number default null,
   member_id   NUMBER DEFAULT NULL  );
   
    PROCEDURE prc_chg_cl_cancelled(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null  );
   
   PROCEDURE prc_chg_cl_dates(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null  );
   
   PROCEDURE prc_delete(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null  );
   
   PROCEDURE prc_dup_contract(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null  );
   
   PROCEDURE prc_sync_contract_to_sd(   
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null);
   
   PROCEDURE prc_sync_approve_matrix;
    
END;

/
