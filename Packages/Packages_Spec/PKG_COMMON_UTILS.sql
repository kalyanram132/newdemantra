--------------------------------------------------------
--  DDL for Package PKG_COMMON_UTILS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_COMMON_UTILS" 
AS
/******************************************************************************
   NAME:       PKG_COMMON_UTILS 
   PURPOSE:    Procedures for 
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version
   
   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_COMMON_UTILS';
   
   PROCEDURE prc_lowest_lev_integration (is_imp_table VARCHAR2,
                                         ins_or_upd   NUMBER DEFAULT 0);
   
   PROCEDURE prc_start_wf_schema (
      v_schema     VARCHAR2,
      v_server     VARCHAR2 DEFAULT NULL,
      v_user       VARCHAR2 DEFAULT NULL,
      v_password   VARCHAR2 DEFAULT NULL
   );
    PROCEDURE prc_run_wf_schema (
      v_schema     VARCHAR2,
      v_terminate  VARCHAR2 DEFAULT NULL,
      v_term_err   VARCHAR2 DEFAULT NULL,
      v_server     VARCHAR2 DEFAULT NULL,
      v_user       VARCHAR2 DEFAULT NULL,
      v_password   VARCHAR2 DEFAULT NULL
   );
   
   PROCEDURE prc_run_full_proport_1;   
   
   PROCEDURE prc_run_full_proport_102;  
   
   PROCEDURE prc_set_max_sales_date;
   
   PROCEDURE prc_set_engine_date;
   
   PROCEDURE prc_update_mdp_matrix_dates;
   
   PROCEDURE prc_mdp_add(is_tbl_name               VARCHAR2  DEFAULT NULL,
      start_date                  DATE DEFAULT NULL,
      end_date                    DATE DEFAULT NULL,                
      ii_proport_run            NUMBER    DEFAULT NULL
   );
   
   PROCEDURE prc_export_interface(p_ii_table VARCHAR2);
   
   PROCEDURE prc_do_nothing(
       user_id     NUMBER DEFAULT NULL,
       level_id    NUMBER DEFAULT NULL,
       member_id   NUMBER DEFAULT NULL
   );

   PROCEDURE prc_update_series_groups;

   PROCEDURE prc_update_accountmgr;
   
   PROCEDURE prc_lock_users;
   
   PROCEDURE prc_unlock_users;
   
   PROCEDURE prc_disconnect_sessions;
   
   PROCEDURE prc_rebuild_tables;
   
   PROCEDURE prc_set_lm_fail_status(user_id     NUMBER DEFAULT NULL,
                                    level_id    NUMBER DEFAULT NULL,
                                    member_id   NUMBER DEFAULT NULL);
                                    
   PROCEDURE prc_set_lm_success_status(user_id     NUMBER DEFAULT NULL,
                                       level_id    NUMBER DEFAULT NULL,
                                       member_id   NUMBER DEFAULT NULL);                                    
   
END;

/
