--------------------------------------------------------
--  DDL for Package PKG_PTP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_PTP" 
AS

/******************************************************************************
   NAME:       PKG_PTP
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_PTP';

   PROCEDURE prc_check_promo;
   
   PROCEDURE prc_update_promo;   

   PROCEDURE prc_out_of_strategy;
   
   PROCEDURE prc_roll_shelf_price;
   
   PROCEDURE prc_refresh_promo_cache;
   
   PROCEDURE prc_pop_exfact_base(p_start_date DATE,
                                 P_END_DATE   date);
   
   PROCEDURE prc_pop_apo_fcst(p_start_date DATE,
                                 P_END_DATE   date);
                                    
   procedure prc_offset_scan(p_start_date date,
                                 p_end_date   DATE);
                                 
  procedure prc_offset_dm_fcst(p_start_date date,
                                 p_end_date   DATE);                                 
   
   PROCEDURE prc_correct_pd_vol_base(p_start_date DATE,
                                     p_end_date   DATE);
   
   PROCEDURE prc_correct_pd_pl_actuals(p_start_date DATE,
                                       p_end_date   DATE);    
   
   PROCEDURE prc_check_scan;
   
   PROCEDURE prc_fix_promo_type;
   
   PROCEDURE prc_refresh_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL);
   
   PROCEDURE prc_correct_pd(p_start_date DATE,
                            p_end_date   DATE);   
  
   PROCEDURE prc_refresh_past_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL);                       
                            
   PROCEDURE prc_past_promo; 
    PROCEDURE prc_past_promo_oneoff; 
    
   PROCEDURE prc_refresh_past_promo_oneoff (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL); 
                               
   PROCEDURE prc_promo_commit_ss;
   
   PROCEDURE prc_update_promo_program;
   
   PROCEDURE prc_update_promo_freq;
   

END;

/
