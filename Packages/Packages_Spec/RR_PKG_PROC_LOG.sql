--------------------------------------------------------
--  DDL for Package RR_PKG_PROC_LOG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."RR_PKG_PROC_LOG" AS

/**************************************************************************

   NAME:       rr_pkg_proc_log
   PURPOSE:    Package spec for all the FUNCTIONs used for FUNCTIONs and packages logging 

   REVISIONS:
   Ver        Date        Author
   ---------  ----------  -------------------------------------------------
   1.0        18/01/2012  rampallib/Red Rock Consulting

**************************************************************************/

v_rr_proc_debug NUMBER;
v_rr_proc_error  NUMBER;
v_rr_proc_sql     NUMBER;

FUNCTION fcn_dbex (p_status                      VARCHAR2)
RETURN NUMBER;

FUNCTION fcn_dbex (p_status                      VARCHAR2,
                                p_orc_sqlcode             VARCHAR2,
                                p_orc_sqlerrm             VARCHAR2,
                                p_orc_err_stack           VARCHAR2,
                                p_orc_err_backtrace     VARCHAR2)
RETURN NUMBER;                                                            
                                  
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_status                      VARCHAR2)
RETURN NUMBER;                                
                                  
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_package_name         VARCHAR2,  
                                p_status                      VARCHAR2)
RETURN NUMBER;                                                                  

FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_status                      VARCHAR2,
                                p_orc_sqlcode             VARCHAR2,
                                p_orc_sqlerrm             VARCHAR2,
                                p_orc_err_stack           VARCHAR2,
                                p_orc_err_backtrace     VARCHAR2)
RETURN NUMBER;                                
                                  
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_package_name         VARCHAR2,  
                                p_status                      VARCHAR2,
                                p_orc_sqlcode             VARCHAR2,
                                p_orc_sqlerrm             VARCHAR2,
                                p_orc_err_stack           VARCHAR2,
                                p_orc_err_backtrace     VARCHAR2 )
RETURN NUMBER;                                                                 

FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_status                      VARCHAR2,
                                p_level_id                    NUMBER,
                                p_member_id              NUMBER,
                                p_user_id                    NUMBER)
RETURN NUMBER;                                
                                  
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_package_name         VARCHAR2,  
                                p_status                      VARCHAR2,                                
                                p_level_id                    NUMBER,
                                p_member_id              NUMBER,
                                p_user_id                    NUMBER)
RETURN NUMBER;                                
                                
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_status                      VARCHAR2,
                                p_level_id                    NUMBER,
                                p_member_id              NUMBER,
                                p_user_id                    NUMBER,                                
                                p_orc_sqlcode             VARCHAR2,
                                p_orc_sqlerrm             VARCHAR2,
                                p_orc_err_stack           VARCHAR2,
                                p_orc_err_backtrace     VARCHAR2)
RETURN NUMBER;                                
                                  
FUNCTION fcn_dbex (p_proc_name              VARCHAR2,
                                p_package_name         VARCHAR2,  
                                p_status                      VARCHAR2,
                                p_level_id                    NUMBER,
                                p_member_id              NUMBER,
                                p_user_id                    NUMBER,                                
                                p_orc_sqlcode             VARCHAR2,
                                p_orc_sqlerrm             VARCHAR2,
                                p_orc_err_stack           VARCHAR2,
                                p_orc_err_backtrace     VARCHAR2 )
RETURN NUMBER;             

PROCEDURE prc_proc_sql_log(p_proc_log_id    NUMBER,
                                            p_proc_code VARCHAR2);                                                                    

FUNCTION fcn_get_proc_debug
RETURN NUMBER;

FUNCTION fcn_get_proc_error
RETURN NUMBER;

FUNCTION fcn_get_proc_sql
RETURN NUMBER;

PROCEDURE prc_set_proc_debug(p_val NUMBER);

PROCEDURE prc_set_proc_error(p_val NUMBER);

PROCEDURE prc_set_proc_sql(p_val NUMBER);

PROCEDURE prc_refresh_params;

END; 
 

/
