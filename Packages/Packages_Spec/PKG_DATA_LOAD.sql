--------------------------------------------------------
--  DDL for Package PKG_DATA_LOAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_DATA_LOAD" 
AS
/******************************************************************************
   NAME:         PKG_DATA_LOAD 
   PURPOSE:    Procedures for 
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version
   
   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_DATA_LOAD';
   
       
   PROCEDURE prc_disable_logging_sd;
   
   PROCEDURE prc_enable_logging_sd;
   
   PROCEDURE prc_disable_logging;
   
   PROCEDURE prc_enable_logging;
   
   PROCEDURE prc_truncate_src;
   
   PROCEDURE prc_truncate_sales;
   
   PROCEDURE prc_cl_truncate_src;
   
   PROCEDURE prc_cl_truncate_sales;
   
   PROCEDURE prc_cl_truncate_scan;
   
   PROCEDURE prc_cl_truncate_claims;
   
   PROCEDURE prc_replace_apostrophe;
   
   PROCEDURE prc_ep_check_items;
   
   PROCEDURE prc_ep_load_items;
   
   PROCEDURE prc_ep_check_location;
   
   PROCEDURE prc_ep_load_locations;
    
   PROCEDURE prc_ep_load_sales;
   
   PROCEDURE prc_mdp_add;
   
   PROCEDURE prc_ep_load_mdp_level;  
   
   PROCEDURE prc_update_from_till_date;
   
   PROCEDURE prc_update_max_sales_date;    
   
   PROCEDURE prc_delete_load_flag;
   
   PROCEDURE prc_update_src;
   
   PROCEDURE prc_update_sales;
   
   PROCEDURE prc_load_scan;
   
   PROCEDURE prc_load_pricing;
   
   PROCEDURE prc_load_pricing_loop;
   
   PROCEDURE prc_load_budget;
   
   PROCEDURE prc_load_claims;
   
   PROCEDURE prc_load_claims_au;
   
   PROCEDURE prc_load_payto_vendor;
   
   PROCEDURE PRC_CL_TRUNC_APO_FCST_STG;
   
    PROCEDURE prc_insert_new_apo_combs;

   PROCEDURE prc_cl_truncate_apo_fcst;
   
   PROCEDURE prc_load_apo_fcst;
   
   PROCEDURE prc_load_gst_rate;
   
   PROCEDURE prc_update_rates_roll;
   
   -- Added Sanjiiv for COPA integration migration 
   PROCEDURE prc_load_price_gsv;
   
   PROCEDURE prc_load_price_sdc;
    
    PROCEDURE prc_load_price_lp;
   
   PROCEDURE prc_export_ts_copa;
   
END;

/
