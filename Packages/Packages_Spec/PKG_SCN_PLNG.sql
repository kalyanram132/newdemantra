--------------------------------------------------------
--  DDL for Package PKG_SCN_PLNG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_SCN_PLNG" 
AS

/******************************************************************************
   NAME:       PKG_SCN_PLNG
   PURPOSE:    All procedures commanly used for Scenario Planning
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_SCN_PLNG'; 
   
   PROCEDURE prc_get_scn_promos(p_scn_id IN NUMBER);  
   
   PROCEDURE prc_fix_promo_type(p_scn_id NUMBER);
   
   PROCEDURE prc_set_mdp_allocation(p_scn_id NUMBER);
   
   PROCEDURE prc_calc_accrual(p_scn_id NUMBER);                            

   PROCEDURE prc_margin_support(p_scn_id NUMBER);

   PROCEDURE prc_promo_accrual_sync(p_scn_id  NUMBER,
                                    p_sync_flag     NUMBER DEFAULT 3,
                                    p_sync_ms       NUMBER DEFAULT 0);
   
   PROCEDURE prc_set_wif_scn_flag(p_scn_id      NUMBER,p_value NUMBER);   
   
   PROCEDURE prc_delete_wif_scn(p_scn_id NUMBER);
   
   PROCEDURE prc_del_old_wif_scn;       
   
   PROCEDURE prc_full_freeze;
   
   PROCEDURE prc_purge_scenario;

END;

/
