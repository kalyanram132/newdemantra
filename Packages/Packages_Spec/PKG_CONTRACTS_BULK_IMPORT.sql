--------------------------------------------------------
--  DDL for Package PKG_CONTRACTS_BULK_IMPORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_CONTRACTS_BULK_IMPORT" 
AS
/******************************************************************************
  NAME:       PKG_CONTRCATS_BULK_IMPORT
  PURPOSE:    All procedures commanly used for contracts bulk import
  REVISIONS:
  Ver        Date        Author
  ---------  ----------  ---------------------------------------------------
  1.0        01/02/2016  Sanjiiv Chavaan / Redrock Consulting - Initial version
******************************************************************************/
PROCEDURE PRC_ALL_PRE_PROCESSING;
PROCEDURE PRC_VALIDATE_CONTRACTS;
PROCEDURE PRC_IMPORT_CONTRACTS;
PROCEDURE PRC_ALL_POST_PROCESSING;
--
END PKG_CONTRACTS_BULK_IMPORT;

/
