--------------------------------------------------------
--  DDL for Package PKG_APO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_APO" 
AS

/******************************************************************************
   NAME:       PKG_APO
   PURPOSE:    All procedures commanly used for APO
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/02/2016  Lakshmi Annapragada/ UXC Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_APO';

    V_WEEKLY_FLAG number := 0;
    
   PROCEDURE   PRC_SYNC_FULL_INCREMENTALS   (P_START_DATE  DATE,
                              P_END_DATE    date);
   procedure PRC_INSERT_NEW_APO_COMBS;
   
   procedure PRC_UPD_APO_STG_DAILY;
   PROCEDURE prc_upd_apo_stg_weekly;
   PROCEDURE prc_pop_exfact_base(p_start_date DATE,
                                 P_END_DATE   date);
   
   PROCEDURE prc_pop_apo_fcst(p_start_date DATE,
                                 P_END_DATE   date);
                                    
   procedure prc_offset_scan(p_start_date date,
                                 p_end_date   DATE);
                                 
   procedure prc_offset_dm_fcst(p_start_date date,
                                 p_end_date   date);  
   procedure prc_offset_incr_fcst(p_start_date date,
                                 p_end_date   date);                                      
   
   procedure prc_compare_apo_prev_load;
   

END;

/
