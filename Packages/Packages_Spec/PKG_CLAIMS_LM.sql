--------------------------------------------------------
--  DDL for Package PKG_CLAIMS_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_CLAIMS_LM" 
AS

/******************************************************************************
   NAME:       PKG_CLAIMS_LM
   PURPOSE:    All procedures commanly used for Claims Level Methods
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_CLAIMS_LM';

   PROCEDURE prc_reverse_claim(user_id     NUMBER DEFAULT NULL,
                               level_id    NUMBER DEFAULT NULL,
                               member_id   NUMBER DEFAULT NULL);
                               
   PROCEDURE prc_reverse_group_claim(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL);
                                     
   PROCEDURE prc_reset_approved_claim(user_id     NUMBER DEFAULT NULL,
                                      level_id    NUMBER DEFAULT NULL,
                                      member_id   NUMBER DEFAULT NULL);
                                      
   PROCEDURE prc_reset_group_claims(user_id     NUMBER DEFAULT NULL,
                                    level_id    NUMBER DEFAULT NULL,
                                    member_id   NUMBER DEFAULT NULL);
                                    
   PROCEDURE prc_createas_settlement(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL); 

   PROCEDURE prc_delete_claims(user_id     NUMBER DEFAULT NULL,
                               level_id    NUMBER DEFAULT NULL,
                               member_id   NUMBER DEFAULT NULL);
                                     
   PROCEDURE prc_link_claim (user_id     NUMBER,
                             level_id    NUMBER,
                             member_id   NUMBER);
   
   PROCEDURE prc_validate_claim(user_id   NUMBER,
                                level_id  NUMBER,
                                member_id NUMBER);
                                            
   PROCEDURE prc_delete_group_claims(user_id     NUMBER DEFAULT NULL,
                                     level_id    NUMBER DEFAULT NULL,
                                     member_id   NUMBER DEFAULT NULL);
                                     
END;

/
