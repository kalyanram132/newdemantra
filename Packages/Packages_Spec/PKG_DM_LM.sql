--------------------------------------------------------
--  DDL for Package PKG_DM_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_DM_LM" 
AS

/******************************************************************************
   NAME:       PKG_DM_LM
   PURPOSE:    All procedures commanly used for DM module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_DM_LM';

   PROCEDURE prc_inv_adjust(user_id    NUMBER,
                            level_id   NUMBER,
                            member_id  NUMBER);
   
   PROCEDURE prc_create_combinations(user_id    NUMBER,
                                     level_id   NUMBER,
                                     member_id  NUMBER);
  

   PROCEDURE prc_pop_exfact_base(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL);
   
   PROCEDURE PRC_SYNC_SCAN_OFFSET(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL);


END;

/
