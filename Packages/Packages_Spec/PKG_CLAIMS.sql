--------------------------------------------------------
--  DDL for Package PKG_CLAIMS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_CLAIMS" 
AS

/******************************************************************************
   NAME:       PKG_PTP
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_CLAIMS';

   PROCEDURE prc_link_claim;
   
   PROCEDURE prc_approve_claim;
   
   PROCEDURE prc_split_claim;
   
   PROCEDURE prc_default_claims;
   
   PROCEDURE prc_update_days_out;
   
   PROCEDURE prc_check_duplicate_claim;
   
  PROCEDURE prc_create_claim_combos;

END;

/
