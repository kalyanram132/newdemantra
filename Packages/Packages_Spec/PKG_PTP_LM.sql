--------------------------------------------------------
--  DDL for Package PKG_PTP_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_PTP_LM" 
AS

/******************************************************************************
   NAME:       PKG_PTP_LM
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_PTP_LM';

   PROCEDURE prc_close_promotion (user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL);   
   
   PROCEDURE prc_reopen_promo(user_id   NUMBER DEFAULT NULL,
                              level_id  NUMBER DEFAULT NULL,
                              member_id NUMBER DEFAULT NULL);
                              
                              
   PROCEDURE prc_create_promo_details(user_id     NUMBER DEFAULT NULL,
                                      level_id    NUMBER DEFAULT NULL,
                                      member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_make_promo_slob(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_approve_slob_promo(user_id     NUMBER DEFAULT NULL,
                                    level_id    NUMBER DEFAULT NULL,
                                    member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_load_shelf_price(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_reload_base(user_id   NUMBER DEFAULT NULL,
                             level_id  NUMBER DEFAULT NULL,
                             member_id NUMBER DEFAULT NULL);
   
   PROCEDURE prc_reload_all_base(user_id   NUMBER DEFAULT NULL,
                                 level_id  NUMBER DEFAULT NULL,
                                 member_id NUMBER DEFAULT NULL);
                             
   PROCEDURE prc_sync_deals(user_id     NUMBER DEFAULT NULL,
                            level_id    NUMBER DEFAULT NULL,
                            member_id   NUMBER DEFAULT NULL);
   
   PROCEDURE prc_check_promo(user_id     NUMBER DEFAULT NULL,
                             level_id    NUMBER DEFAULT NULL,
                             member_id   NUMBER DEFAULT NULL);
   
   PROCEDURE prc_refresh_promo (user_id   NUMBER DEFAULT NULL,
                               level_id  NUMBER DEFAULT NULL,
                               member_id NUMBER DEFAULT NULL);
   
   PROCEDURE prc_create_claim(user_id     NUMBER DEFAULT NULL,
                              level_id    NUMBER DEFAULT NULL,
                              member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_set_pg_id(user_id   NUMBER DEFAULT NULL,
                           level_id  NUMBER DEFAULT NULL,
                           member_id NUMBER DEFAULT NULL);
   
   PROCEDURE prc_edit_promo_desc(user_id     NUMBER DEFAULT NULL,
                                 level_id    NUMBER DEFAULT NULL,
                                 member_id   NUMBER DEFAULT NULL);
                                 
   PROCEDURE prc_approve_gr(user_id     NUMBER DEFAULT NULL,
                            level_id    NUMBER DEFAULT NULL,
                            member_id   NUMBER DEFAULT NULL);                             

   FUNCTION fcn_get_ge(p_gondola_end_id     NUMBER DEFAULT NULL,
                      p_activity_type_id    NUMBER DEFAULT NULL)
   RETURN NUMBER;                             
   
   PROCEDURE prc_set_promo_type(user_id   NUMBER DEFAULT NULL,
                                level_id  NUMBER DEFAULT NULL,
                                member_id NUMBER DEFAULT NULL);

     PROCEDURE prc_cancel_promotion(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   NUMBER DEFAULT NULL     
   );            
   
     PROCEDURE prc_cancel_oi_promo(
   USER_ID     number default null,
   level_id    NUMBER DEFAULT NULL,
   MEMBER_ID   number default null     
   );            
       
  PROCEDURE prc_create_oi_promo(user_id   NUMBER DEFAULT NULL,
                                LEVEL_ID  NUMBER DEFAULT NULL,
                                MEMBER_ID NUMBER DEFAULT NULL); 
                                
  PROCEDURE prc_chg_oi_promo_dates(user_id   NUMBER DEFAULT NULL,
                                LEVEL_ID  NUMBER DEFAULT NULL,
                                MEMBER_ID number default null);                                 

  function FCN_GET_FUND_TYPE(P_FUND_TYPE_ID   number default null)
  return number;  
  
  PROCEDURE PRC_FIX_PROMOTION(
   user_id     NUMBER DEFAULT NULL,
   level_id    NUMBER DEFAULT NULL,
   member_id   number default null     
   );


END;

/
