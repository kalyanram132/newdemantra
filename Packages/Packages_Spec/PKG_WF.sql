--------------------------------------------------------
--  DDL for Package PKG_WF
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_WF" 
AS
v_package_name VARCHAR2(100) := 'PKG_WF';

 PROCEDURE prc_wf_epload;
  PROCEDURE prc_wf_master_data_load;
  
  PROCEDURE prc_wf_sales_load;
      
  PROCEDURE PRC_WF_LOAD_SCAN;
    PROCEDURE prc_wf_mdp_add;
    PROCEDURE prc_wf_load_claims;
 
  procedure PRC_WF_PRE_LOAD_FCST;

   PROCEDURE PRC_WF_LOAD_FCST_DAILY;
   
   PROCEDURE prc_wf_load_fcst_weekly;
  
  PROCEDURE prc_wf_processclaim;

  PROCEDURE prc_wf_lockusers;

  
  PROCEDURE prc_wf_unlockusers;
  
  PROCEDURE PRC_WF_REFRESHPROMO;

  PROCEDURE PRC_WF_PROMOACCRUAL;
  
  PROCEDURE PRC_WF_PROMOACCRUAL_Q1;

  PROCEDURE PRC_WF_PROMOACCRUALOS; 
   PROCEDURE prc_wf_accrual_cancel;
  procedure PRC_WF_EXPORT_FCST;  
  PROCEDURE prc_wf_export_fcst_weekly;  
  PROCEDURE prc_maintenance; 
  PROCEDURE prc_maintenance_month;
  
PROCEDURE prc_wf_roll_shelf_price;
  
  PROCEDURE prc_export_contracts;

PROCEDURE prc_export_claims;
  
 procedure PRC_WF_PRE_ENGINE;
 
  procedure PRC_WF_POST_ENGINE;
  

  
END;

/
