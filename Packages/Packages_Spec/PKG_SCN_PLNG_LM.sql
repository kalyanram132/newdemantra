--------------------------------------------------------
--  DDL for Package PKG_SCN_PLNG_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_SCN_PLNG_LM" 
AS
/******************************************************************************
   NAME:       PKG_SCN_PLNG_LM
   PURPOSE:    All procedures for level methods related to Scenario Planning
   REVISIONS:
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        20/03/2013  Lakshmi Annapragada / Redrock Consulting - Initial Version
   
   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_SCN_PLNG_LM';
   
   PROCEDURE prc_duplicate_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);
   
   PROCEDURE prc_freeze_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);

   PROCEDURE prc_unfreeze_scenario(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);
   
   PROCEDURE prc_sync_scn_deals(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);    
                                  
   PROCEDURE prc_promote_wif_scn(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);  
                                  
   PROCEDURE prc_delete_wif_scn_lm(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);
                                  
   PROCEDURE prc_reload_sales_base(user_id     NUMBER DEFAULT NULL,
                                  level_id    NUMBER DEFAULT NULL,
                                  member_id   NUMBER DEFAULT NULL);
                                   
END; 

/
