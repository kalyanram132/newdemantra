--------------------------------------------------------
--  DDL for Package PKG_EXPORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_EXPORT" 
AS

/******************************************************************************
   NAME:       PKG_EXPORT
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_EXPORT';

   PROCEDURE prc_export_accrual_close(p_start_date  DATE,
                                      p_end_date    DATE,
                                      p_extra_where VARCHAR2 DEFAULT NULL);
   PROCEDURE prc_export_accrual_cancel(p_start_date  DATE,
                                      p_end_date    DATE,
                                      p_extra_where VARCHAR2 DEFAULT NULL);
   PROCEDURE prc_export_accrual(p_start_date  DATE,
                                p_end_date    DATE,
                                p_extra_where VARCHAR2 DEFAULT NULL);      
                                
   PROCEDURE prc_export_claims;
   
   PROCEDURE prc_export_oi(p_start_date  DATE DEFAULT NULL,
                           p_end_date    DATE DEFAULT NULL
                           );                                 
   
   PROCEDURE prc_export_accrual_overspend(p_start_date  DATE,
                                          p_end_date    DATE,
                                          p_extra_where VARCHAR2 DEFAULT NULL);  
   
   PROCEDURE prc_export_exfactory_fcst; 
   
   procedure PRC_EXPORT_FCST;    --- Added -- SR
   
   PROCEDURE prc_export_fcst_daily;
   
   PROCEDURE prc_export_bi_profit_ka(p_start_offset  NUMBER,
                                    p_end_offset    NUMBER);
   
   PROCEDURE prc_export_bi_profit_kad(p_start_offset  NUMBER,
                                      p_end_offset    NUMBER);
   
   PROCEDURE prc_export_bi_profit_pr(p_start_offset  NUMBER,
                                    p_end_offset    NUMBER);
                                    
   PROCEDURE prc_export_tradespend_nonpro(p_start_offset  NUMBER,
                                          p_end_offset    NUMBER);
   
   PROCEDURE prc_export_tradespend_pro(p_start_offset  NUMBER,
                                       p_end_offset    NUMBER);   
   
   PROCEDURE prc_export_ts_nonpro(p_start_offset  NUMBER,
                                  p_end_offset    NUMBER);
                                  
   PROCEDURE PRC_EXPORT_CONTRACTS(P_START_OFFSET  NUMBER,
                                  p_end_offset    NUMBER);
                                            
END;

/
