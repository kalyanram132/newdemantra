--------------------------------------------------------
--  DDL for Package PKG_CONTRACTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_CONTRACTS" 
AS
 
/******************************************************************************
   NAME:       PKG_CONTRACTS_LM
   PURPOSE:    All level methods commanly used for Contracts solution
   REVISIONS:
 
   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        02/07/2015  Lakshmi Annapragada/ UXC Redrock Consulting - Initial version
 
   ******************************************************************************/
 
   v_package_name VARCHAR2(100) := 'PKG_CONTRACTS';
 
   PROCEDURE prc_sync_contract_to_sd( 
   p_start_date    DATE,
   p_end_date      DATE,
   p_full_load     NUMBER DEFAULT 0
   );  
      
      PROCEDURE PRC_SYNC_CONTRACT_TO_SD_DAILY;
      PROCEDURE prc_sync_contract_to_sd_weekly;
    
END;

/
