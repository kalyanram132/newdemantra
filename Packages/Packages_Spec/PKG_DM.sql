--------------------------------------------------------
--  DDL for Package PKG_DM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_DM" 
AS

/******************************************************************************
   NAME:       PKG_DM
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_DM';

   PROCEDURE prc_update_dem_type;
   
   PROCEDURE prc_inv_adjust;
   
   PROCEDURE prc_set_do_not_forecast;
   
   PROCEDURE prc_sync_mdp_2_mdp(p_query_name VARCHAR2,
                                p_dem_alloc NUMBER); 
 
   PROCEDURE prc_copy_exfactory_fcst;                                  
   
   PROCEDURE prc_update_sim_cols; 
   
   PROCEDURE prc_update_manual_stat;

END;

/
