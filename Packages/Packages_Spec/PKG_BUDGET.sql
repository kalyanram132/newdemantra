--------------------------------------------------------
--  DDL for Package PKG_BUDGET
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_BUDGET" 
AS

/******************************************************************************
   NAME:       PKG_BUDGET
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_BUDGET'; 
   
   procedure prc_pnl_ss1 (p_start_date  date,
                          p_end_date    date); 
                          
   procedure prc_pnl_ss2 (p_start_date  date,
                          p_end_date    DATE); 
  
   procedure prc_budget_ss_monthly; 
                          
   procedure prc_budget_ss_qtrly; 
                                                     

END;

/
