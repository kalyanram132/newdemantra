--------------------------------------------------------
--  DDL for Package PKG_ACCRUAL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_ACCRUAL" 
AS

/******************************************************************************
   NAME:       RR_PKG_PTP
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_ACCRUAL';

   PROCEDURE prc_clear_accrual(p_start_date  DATE,
                              p_end_date    DATE,
                              p_extra_where VARCHAR2 DEFAULT NULL);
   
   PROCEDURE prc_set_mdp_allocation(p_start_date  DATE,
                                    p_end_date    DATE);
   
   PROCEDURE prc_calc_accrual(p_start_date  DATE,
                              p_end_date    DATE,
                              p_extra_where VARCHAR2 DEFAULT NULL);                            

   PROCEDURE prc_margin_support(p_start_date  DATE,
                                p_end_date    DATE,
                                p_extra_where VARCHAR2 DEFAULT NULL);

   PROCEDURE prc_promo_accrual_sync(p_start_date  DATE,
                                    p_end_date    DATE,
                                    p_sync_flag   NUMBER DEFAULT 3,
                                    p_sync_ms     NUMBER DEFAULT 0,
                                    p_extra_where VARCHAR2 DEFAULT NULL);                             

   PROCEDURE prc_accrual_over_spend(p_start_date  DATE,
                                     p_end_date    DATE,
                                     p_extra_where VARCHAR2 DEFAULT NULL);
                                     
                                     
   PROCEDURE prc_calc_accrual_close(p_start_date  DATE,
                                    p_end_date    DATE,
                                    p_extra_where VARCHAR2 DEFAULT NULL); 

END;

/
