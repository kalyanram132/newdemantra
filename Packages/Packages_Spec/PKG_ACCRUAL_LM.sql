--------------------------------------------------------
--  DDL for Package PKG_ACCRUAL_LM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "DEMANTRA"."PKG_ACCRUAL_LM" 
AS

/******************************************************************************
   NAME:       RR_PKG_PTP
   PURPOSE:    All procedures commanly used for PTP module
   REVISIONS:

   Ver        Date        Author
   ---------  ----------  ---------------------------------------------------
   1.0        11/11/2010  Bhaskar Rampalli / Redrock Consulting - Initial version

   ******************************************************************************/

   v_package_name VARCHAR2(100) := 'PKG_ACCRUAL_LM';   
   
   PROCEDURE prc_fix_promo_type(p_user_id      NUMBER, 
                               p_promotion_id NUMBER);
   
   PROCEDURE prc_set_mdp_allocation(p_user_id      NUMBER, 
                                    p_promotion_id NUMBER);
   
   PROCEDURE prc_calc_accrual(p_user_id      NUMBER, 
                              p_promotion_id NUMBER);                            

   PROCEDURE prc_margin_support(p_user_id      NUMBER, 
                                p_promotion_id NUMBER);

   PROCEDURE prc_promo_accrual_sync(p_user_id      NUMBER, 
                                    p_promotion_id  NUMBER,
                                    p_sync_flag     NUMBER DEFAULT 3,
                                    p_sync_ms       NUMBER DEFAULT 0);

END;

/
